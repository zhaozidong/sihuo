//
//  TGCameraViewController.m
//  TGCameraViewController
//
//  Created by Bruno Tortato Furtado on 13/09/14.
//  Copyright (c) 2014 Tudo Gostoso Internet. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "TGCameraViewController.h"
#import "TGPhotoViewController.h"
#import "TGCameraSlideView.h"
#import "UIButton+Addition.h"
#import "ImageDisplayViewController.h"
#import "UIImage+IF.h"
#import "ImageListCell.h"
#import "QMSubmitButton.h"
#import "AlbumViewController.h"
#import "QMPhotoGuide.h"
#import "UserDefaultsUtils.h"
#import "UIImage+Additions.h"
#import "QMQAViewController.h"

@interface TGCameraViewController ()<UITableViewDataSource, UITableViewDelegate, ImageListCellDelegate>{
    UIImageView *_imageTips;
}

@property (strong, nonatomic) IBOutlet UIView *captureView;
@property (strong, nonatomic) IBOutlet UIImageView *topLeftView;
@property (strong, nonatomic) IBOutlet UIImageView *topRightView;
@property (strong, nonatomic) IBOutlet UIImageView *bottomLeftView;
@property (strong, nonatomic) IBOutlet UIImageView *bottomRightView;
@property (strong, nonatomic) IBOutlet UIButton *gridButton;
@property (strong, nonatomic) IBOutlet UIButton *toggleButton;
@property (strong, nonatomic) IBOutlet UIButton *shotButton;
@property (strong, nonatomic) IBOutlet UIButton *flashButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet TGCameraSlideView *slideUpView;
@property (strong, nonatomic) IBOutlet TGCameraSlideView *slideDownView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *imageListView;
@property (strong, nonatomic) UITableView *imageListTable; //显示已经拍下来的图片

@property (strong, nonatomic) TGCamera *camera;
@property (nonatomic) CGFloat beginPinchGestureScale;
@property (nonatomic) CGFloat effectiveScale;
@property (nonatomic) BOOL wasLoaded;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomRightCst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLeftCst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageListViewCst;
@property (nonatomic, strong) NSMutableArray *photoPathArr;
@property (weak, nonatomic) IBOutlet UIImageView *slideUpImageV;
@property (weak, nonatomic) IBOutlet UIImageView *slideDownImageV;
@property (weak, nonatomic) IBOutlet QMSubmitButton *makeSureButton;
@property (weak, nonatomic) IBOutlet UIButton *libraryButton;

@property (assign, nonatomic) int totalCount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topRightViewCst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLeftViewCst;

- (IBAction)closeTapped;
- (IBAction)gridTapped;
- (IBAction)flashTapped;
- (IBAction)shotTapped;
- (IBAction)toggleTapped;
- (IBAction)albumTapped:(id)sender;
- (IBAction)makeSureTapped:(id)sender;

- (IBAction)handlePinchGesture:(UIPinchGestureRecognizer *)recognizer;
- (IBAction)handleTapGesture:(UITapGestureRecognizer *)recognizer;

- (AVCaptureVideoOrientation)videoOrientationForDeviceOrientation:(UIDeviceOrientation)deviceOrientation;
- (void)zoomWithRecognizer:(UIPinchGestureRecognizer *)recognizer;

@end

const int kCameraBottomH = 90;

@implementation TGCameraViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.frame = CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight);
    _camera = [TGCamera cameraWithFlashButton:_flashButton];
    _effectiveScale = 1.;
    
    _captureView.backgroundColor = [UIColor clearColor];
    
    _topLeftView.transform = CGAffineTransformMakeRotation(0);
    _topRightView.transform = CGAffineTransformMakeRotation(M_PI_2);
    _bottomLeftView.transform = CGAffineTransformMakeRotation(-M_PI_2);
    _bottomRightView.transform = CGAffineTransformMakeRotation(M_PI_2*2);
    
    [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_cancelButton setButtonType:BT_LeftImageRightTitle interGap:5.f];
    _makeSureButton.titleLabel.font = [UIFont systemFontOfSize:15.f];
    
    if (kMainFrameWidth == 375) {
        _slideUpImageV.image = [UIImage imageNamed:@"CameraSlideUp_iphone6"];
        _slideDownImageV.image = [UIImage imageNamed:@"CameraSlideDown_iphone6"];
    }
    _slideUpView.frame = CGRectMake(0, 0, kMainFrameWidth, (kMainFrameWidth-kSlideImageGapHeight)/2+kSlideImageGapHeight);
    _slideDownView.frame = CGRectMake(0, 0, kMainFrameWidth, (kMainFrameWidth-kSlideImageGapHeight)/2+kSlideImageGapHeight);
    
    if (kCameraTopHeight == 0) {
        _topView.backgroundColor = [UIColor clearColor];
    }
    float height = kMainFrameHeight - kCameraTopHeight - kCameraBottomH - kMainFrameWidth;
    _bottomLeftCst.constant =
    _bottomRightCst.constant =
    _imageListViewCst.constant = height;
    [self.view setNeedsUpdateConstraints];
    [self.view layoutIfNeeded];
    
    //创建tableview
    self.imageListTable = [[UITableView alloc] initWithFrame:CGRectMake(kMainFrameWidth/2 - [ImageListCell cellHeight]/2, 300, [ImageListCell cellHeight], kMainFrameWidth) style:UITableViewStylePlain];
    self.imageListTable.backgroundColor = [UIColor clearColor];
    self.imageListTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.imageListTable.showsVerticalScrollIndicator = NO;
    self.imageListTable.delegate = self;
    self.imageListTable.dataSource = self;
    self.imageListTable.transform = CGAffineTransformMakeRotation(-M_PI/2);
    CGRect frame = self.imageListTable.frame;
    float tmpH = CGRectGetHeight(self.view.frame)-kCameraBottomH-kCameraTopHeight-kMainFrameWidth;
    frame.origin.y = CGRectGetHeight(self.view.frame)-CGRectGetHeight(frame)-kCameraBottomH-(tmpH-CGRectGetHeight(frame))/2;
    self.imageListTable.frame = frame;
    [self.view addSubview:self.imageListTable];
    [self.imageListTable registerNib:[ImageListCell nib] forCellReuseIdentifier:kImageListCellIdentifier];
    
    
    _imageTips=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photoTips"]];
    _imageTips.frame=CGRectMake((kMainFrameWidth-200)/2, kMainFrameHeight*0.73, 200, 30);
    _imageTips.userInteractionEnabled=YES;
    [self.view addSubview:_imageTips];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTips:)];
    [tap setNumberOfTapsRequired:1];
    [_imageTips addGestureRecognizer:tap];
    
    
    self.photoPathArr = [NSMutableArray arrayWithCapacity:self.totalCount];
    
    self.makeSureButton.hidden = YES;
    self.libraryButton.hidden = NO;
    
    self.totalCount = [self.delegate maxNumberOfPhotos];
    
    //添加照片
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addImageHandler:) name:kAddImageNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateImageHandler) name:kUpdateImageNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
//    [AppUtils hideStatusBar:YES viewController:self];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(deviceOrientationDidChangeNotification)
//                                                 name:UIDeviceOrientationDidChangeNotification
//                                               object:nil];
    
    _gridButton.enabled =
    _toggleButton.enabled =
    _shotButton.enabled =
    _flashButton.enabled = NO;
    self.topLeftViewCst.constant = kCameraTopHeight;
    self.topRightViewCst.constant = kCameraTopHeight;
    if (kCameraTopHeight == 0) {
        _topView.backgroundColor = [UIColor clearColor];
    }
    else{
        _topView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:1.0];
    }
    _imageListView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:1.0];
    
    [_camera startRunning];
    
    [TGCameraSlideView hideSlideUpView:_slideUpView slideDownView:_slideDownView atView:_captureView completion:^{
        _gridButton.enabled =
        _toggleButton.enabled =
        _shotButton.enabled =
        _flashButton.enabled = YES;
        if (kCameraTopHeight > 0) {
            _topView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
        }
        _imageListView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.8];
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [AppUtils hideStatusBar:NO viewController:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    [self deviceOrientationDidChangeNotification];
    [AppUtils hideStatusBar:YES viewController:self];
    
    if (_wasLoaded == NO) {
        _wasLoaded = YES;
        [_camera insertSublayerWithCaptureView:_captureView atRootView:self.view];
    }
    self.libraryButton.userInteractionEnabled = YES;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [_camera stopRunning];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)tapTips:(UIGestureRecognizer *)gestureRecognizer{
    QMQAViewController *qa=[[QMQAViewController alloc] initWithType:QMQATypeAddPhotos];
    [self.navigationController pushViewController:qa animated:YES];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Cell Delegate
- (void)deleteImageWithPath:(NSString *)photoPath
{
    NSInteger index = [self.photoPathArr indexOfObject:photoPath];
    if (index!=NSNotFound) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [self.photoPathArr removeObject:photoPath];
        [self.imageListTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.makeSureButton setTitle:[NSString stringWithFormat:@"确定 %ld/%d", (unsigned long)self.photoPathArr.count, self.totalCount] forState:UIControlStateNormal];
    }
    
    if (self.photoPathArr.count == 0) {
        self.makeSureButton.hidden = YES;
        self.libraryButton.hidden = NO;
    }
}

+ (CGSize)preferSize
{
    float scale = [UIScreen mainScreen].scale;
    return CGSizeMake(kMainFrameWidth*scale, (kMainFrameHeight-100)*scale);
    //    return CGSizeMake((kMainFrameHeight-100)*scale, kMainFrameWidth*scale);
}


-(void)editImageWithPath:(NSString *)photoPath{
    NSString *strPath;
    UIImage *image;
    if ([photoPath hasSuffix:@"_small.jpg"]) {//原图编辑
        NSString *strTemp=[photoPath substringToIndex:[photoPath  length]-[@"_small.jpg" length]];
        NSString *editPath=[NSString stringWithFormat:@"%@%@",strTemp,@"_edit.jpg"];
        NSString *orignialPath=[NSString stringWithFormat:@"%@%@",strTemp,@".jpg"];
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        if ([fileMgr fileExistsAtPath:editPath]) {
//            NSString *strTemp=[photoPath substringToIndex:[photoPath  length]-[@"_edit.jpg" length]];
//            strPath=[NSString stringWithFormat:@"%@%@",strTemp,@".jpg"];
            strPath=editPath;
            image=[UIImage imageWithContentsOfFile:strPath];
            
            ImageDisplayViewController *viewController = [[ImageDisplayViewController alloc]initWithImage:image andPath:strPath isEdit:YES];
            //      TGPhotoViewController *viewController = [TGPhotoViewController newWithDelegate:_delegate photo:photo];
            [self.navigationController pushViewController:viewController animated:YES];
            
        }else if([fileMgr fileExistsAtPath:orignialPath]){
//            strPath=[NSString stringWithFormat:@"%@%@",strTemp,@".jpg"];
            strPath=orignialPath;
            image = [UIImage imageWithContentsOfFile:strPath];
            //进行一次处理
//            CGSize size = [[self class]preferSize];
//            float w = size.width, h = size.height;
//            if ((image.size.width/w)<(image.size.height/h)) {
//                h = (w/image.size.width)*image.size.height;
//            }
//            else{
//                w = (h/image.size.height)*image.size.width;
//            }
//            
//            UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h), YES, [UIScreen mainScreen].scale);
//            CGRect scaledImageRect = CGRectMake( 0.0, 0.0, w, h);
//            [image drawInRect:scaledImageRect];
//            image = UIGraphicsGetImageFromCurrentImageContext();
//            UIGraphicsEndImageContext();
//            
//            float x = (image.size.width-size.width)/2;
//            float y = ((image.size.height-size.height)/2)+kCameraTopHeight*image.scale;
//            image = [image cropImageNoRotateWithBounds:CGRectMake(x, y, size.width, size.width)];
            
            ImageDisplayViewController *viewController = [[ImageDisplayViewController alloc]initWithImage:image andPath:strPath isEdit:NO];
            //      TGPhotoViewController *viewController = [TGPhotoViewController newWithDelegate:_delegate photo:photo];
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}


#pragma mark - 
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [ImageListCell cellHeight];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.photoPathArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ImageListCell *cell = (ImageListCell *)[tableView dequeueReusableCellWithIdentifier:kImageListCellIdentifier];
    cell.delegate = self;
    cell.photoPath = [self.photoPathArr objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark - 
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

#pragma mark -
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]]) {
        _beginPinchGestureScale = _effectiveScale;
    }
    
    return YES;
}

#pragma mark -
#pragma mark - Actions

- (IBAction)closeTapped
{
    if ([_delegate respondsToSelector:@selector(cameraDidCancel)]) {
        [_delegate cameraDidCancel];
    }
}

- (IBAction)gridTapped
{
    [_camera disPlayGridView];
}

- (IBAction)flashTapped
{
    [_camera changeFlashModeWithButton:_flashButton];
}

- (IBAction)shotTapped
{
    //统计拍照
    if(self.photoPathArr.count == 0) {
        [AppUtils trackCustomEvent:@"photo_btn_click_event" args:nil];
    }
    
    if (self.photoPathArr.count == self.totalCount) {
        [AppUtils showAlertMessage:@"不能再拍了哦~"];
        return;
    }
    _shotButton.enabled = NO;
    
    
    [_imageTips removeFromSuperview];
    

//    [TGCameraSlideView showSlideUpView:_slideUpView slideDownView:_slideDownView atView:_captureView completion:^{
    
        _topView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:1.0];
        _imageListView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:1.0];
        
//        UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    
//        AVCaptureVideoOrientation videoOrientation = [self videoOrientationForDeviceOrientation:deviceOrientation];
    
    
        [_camera takePhotoWithCaptureView:_captureView effectiveScale:_effectiveScale videoOrientation:AVCaptureVideoOrientationPortrait completion:^(UIImage *photo) {
            
            
//            ImageDisplayViewController *viewController = [[ImageDisplayViewController alloc]initWithImage:photo];
//            //      TGPhotoViewController *viewController = [TGPhotoViewController newWithDelegate:_delegate photo:photo];
//            [self.navigationController pushViewController:viewController animated:YES];
            
            
            
            
            //保存图片
            NSData *imgData = UIImageJPEGRepresentation(photo, 0.5);
            UInt64 t = (UInt64)([[NSDate date] timeIntervalSince1970]*1000);
            NSString *timeSp = [NSString stringWithFormat:@"%llu", t];
            [AppUtils saveData:imgData imgName:[NSString stringWithFormat:@"%@.jpg", timeSp]];
            
            float scale = [UIScreen mainScreen].scale;
            UIImage *resultImg = [photo thumbnailImage:CGSizeMake(70*scale, 70*scale)];
            imgData = UIImageJPEGRepresentation(resultImg, 0.6);
            NSString *smallImgPath = [AppUtils saveData:imgData imgName:[NSString stringWithFormat:@"%@_small.jpg", timeSp]];
            
            //添加已保存的图片到表格
            
            [self.photoPathArr addObject:smallImgPath];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.photoPathArr.count-1 inSection:0];
            [self.imageListTable insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            if (self.makeSureButton.hidden) {
                self.makeSureButton.hidden = NO;
                self.libraryButton.hidden = YES;
            }
            [self.makeSureButton setTitle:[NSString stringWithFormat:@"确定 %ld/%d", (unsigned long)self.photoPathArr.count, self.totalCount] forState:UIControlStateNormal];
            
            _shotButton.enabled = YES;
            
            if (![UserDefaultsUtils boolValueWithKey:@"photoGuideTakeAnother"] || ![UserDefaultsUtils boolValueWithKey:@"photoGuideTap"]) {
                QMPhotoGuide *photoGuide=[[QMPhotoGuide alloc] init];
                photoGuide.frame=CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight);
                [self.view addSubview:photoGuide];
            }
        }];
//    }];
    
}

- (IBAction)toggleTapped
{
    [_camera toogleWithFlashButton:_flashButton];
}

- (IBAction)albumTapped:(id)sender {
    //统计点击相册按钮的次数
    [AppUtils trackCustomEvent:@"click_album_event" args:nil];
    
    self.libraryButton.userInteractionEnabled = NO;
    AlbumViewController *controller = [[AlbumViewController alloc]init];
    controller.delegate = self;
    controller.totalCount = [self.delegate maxNumberOfPhotos];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)makeSureTapped:(id)sender {
    
    //统计群人进入发布页
    [AppUtils trackCustomEvent:@"enter_publish_page_event" args:nil];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cameraDidOKWithPhotos:)]) {
        [self.delegate cameraDidOKWithPhotos:self.photoPathArr];
    }
}

- (IBAction)handlePinchGesture:(UIPinchGestureRecognizer *)recognizer
{
    [self zoomWithRecognizer:recognizer];
}

- (IBAction)handleTapGesture:(UITapGestureRecognizer *)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:_captureView];
    [_camera focusView:_captureView inTouchPoint:touchPoint];
}



#pragma mark -
#pragma mark - Private methods

- (void)deviceOrientationDidChangeNotification
{
    UIDeviceOrientation orientation = [UIDevice.currentDevice orientation];
    NSInteger degress;
    
    switch (orientation) {
        case UIDeviceOrientationFaceUp:
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationUnknown:
            degress = 0;
            break;
            
        case UIDeviceOrientationLandscapeLeft:
            degress = 90;
            break;
            
        case UIDeviceOrientationFaceDown:
        case UIDeviceOrientationPortraitUpsideDown:
            degress = 180;
            break;
            
        case UIDeviceOrientationLandscapeRight:
            degress = 270;
            break;
    }
    
    CGFloat radians = degress * M_PI / 180;
    CGAffineTransform transform = CGAffineTransformMakeRotation(radians);
    
    [UIView animateWithDuration:.5f animations:^{
        _flashButton.transform =
        _toggleButton.transform = transform;
    }];
}

- (AVCaptureVideoOrientation)videoOrientationForDeviceOrientation:(UIDeviceOrientation)deviceOrientation
{
    AVCaptureVideoOrientation result = (AVCaptureVideoOrientation) deviceOrientation;
    
    switch (deviceOrientation) {
        case UIDeviceOrientationLandscapeLeft:
            result = AVCaptureVideoOrientationLandscapeRight;
            break;
            
        case UIDeviceOrientationLandscapeRight:
            result = AVCaptureVideoOrientationLandscapeLeft;
            break;
            
        default:
            break;
    }
    
    return result;
}

- (void)zoomWithRecognizer:(UIPinchGestureRecognizer *)recognizer
{
    BOOL allTouchesAreOnThePreviewLayer = YES;
    NSInteger numberOfTouches = [recognizer numberOfTouches];
    
    AVCaptureVideoPreviewLayer *previewLayer = [_camera previewLayer];
    
    for (NSInteger i = 0; i < numberOfTouches; i++) {
        CGPoint location = [recognizer locationOfTouch:i inView:_captureView];
        CGPoint convertedLocation = [previewLayer convertPoint:location fromLayer:previewLayer.superlayer];
        
        if ([previewLayer containsPoint:convertedLocation] == NO) {
            allTouchesAreOnThePreviewLayer = NO;
            break;
        }
    }
    
    if (allTouchesAreOnThePreviewLayer) {
        _effectiveScale = _beginPinchGestureScale * [recognizer scale];
        
        if (_effectiveScale < 1.) {
            _effectiveScale = 1.;
        }
        
        AVCaptureStillImageOutput *stillImageOutput = [_camera stillImageOutput];
        CGFloat maxScaleAndCropFactor = [[stillImageOutput connectionWithMediaType:AVMediaTypeVideo] videoMaxScaleAndCropFactor];
        
        if (_effectiveScale > maxScaleAndCropFactor) {
            _effectiveScale = maxScaleAndCropFactor;
        }
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:.025];
        [previewLayer setAffineTransform:CGAffineTransformMakeScale(_effectiveScale, _effectiveScale)];
        [CATransaction commit];
    }
}

#pragma mark -
#pragma mark - Notification Handler
- (void)addImageHandler:(NSNotification *)notification
{
    NSString *filePath = (NSString *)notification.object;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.photoPathArr.count inSection:0];
    [self.photoPathArr addObject:filePath];
    [self.imageListTable insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
    if (self.makeSureButton.hidden) {
        self.makeSureButton.hidden = NO;
        self.libraryButton.hidden = YES;
    }
    [self.makeSureButton setTitle:[NSString stringWithFormat:@"确定 %ld/%d", (unsigned long)self.photoPathArr.count, self.totalCount] forState:UIControlStateNormal];
}

- (void)updateImageHandler{
    [self.imageListTable reloadData];
}


#pragma mark - AlbumController Delegate
- (void)choosePhotosFromAlbum:(NSArray *)photos
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(cameraDidOKWithPhotos:)]) {
        [self.delegate cameraDidOKWithPhotos:photos];
    }
}

@end