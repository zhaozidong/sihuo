//
//  UIImage+FixOrientation.h
//  QMMM
//
//  Created by Derek.zhao on 14-12-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (FixOrientation)
- (UIImage *)fixOrientation;
- (UIImage *)fixOrientationInRect:(CGRect)newRect;

@end
