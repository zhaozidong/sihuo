//
//  TGCameraShot.m
//  TGCameraViewController
//
//  Created by Bruno Furtado on 15/09/14.
//  Copyright (c) 2014 Tudo Gostoso Internet. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

@import AssetsLibrary;
#import "TGCameraShot.h"
#import "UIImage+IF.h"

@implementation TGCameraShot

#pragma mark - Proper Size For Resizing Large Image
+ (CGSize)properSizeForResizingLargeImage:(UIImage *)originaUIImage {
    float originalWidth = originaUIImage.size.width;
    float originalHeight = originaUIImage.size.height;
    float smallerSide = 0.0f;
    float scalingFactor = 0.0f;
    CGSize size = [[self class]preferSize];
    if (originalWidth < originalHeight) {
        smallerSide = originalWidth;
        scalingFactor = size.width / smallerSide;
        return CGSizeMake(size.width, originalHeight*scalingFactor);
    } else {
        smallerSide = originalHeight;
        scalingFactor = size.height / smallerSide;
        return CGSizeMake(originalWidth*scalingFactor, size.height);
    }
}

+ (void)takePhotoCaptureView:(UIView *)captureView stillImageOutput:(AVCaptureStillImageOutput *)stillImageOutput effectiveScale:(NSInteger)effectiveScale videoOrientation:(AVCaptureVideoOrientation)videoOrientation completion:(void (^)(UIImage *))completion
{    
    AVCaptureConnection *videoConnection = nil;
    
    for (AVCaptureConnection *connection in [stillImageOutput connections]) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo]) {
                videoConnection = connection;
                break;
            }
        }
        
        if (videoConnection) {
            break;
        }
    }
    
    [videoConnection setVideoOrientation:videoOrientation];
    [videoConnection setVideoScaleAndCropFactor:effectiveScale];
    
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection
    completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
        if (imageDataSampleBuffer != NULL) {
            @autoreleasepool {
                NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                UIImage *image=[UIImage imageWithData:imageData];
                
//                UIImage *tmpImg = [UIImage imageWithData:imageData];
//                NSData *jpgImg = UIImageJPEGRepresentation(tmpImg, 0.5);
//                tmpImg = nil;
//                imageData = nil;
//                
//                UIImage *image = [UIImage imageWithData:jpgImg];
//                //进行一次处理
//                CGSize size = [[self class]preferSize];
//                float w = size.width, h = size.height;
//                if ((image.size.width/w)<(image.size.height/h)) {
//                    h = (w/image.size.width)*image.size.height;
//                }
//                else{
//                    w = (h/image.size.height)*image.size.width;
//                }
//                
//                UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h), YES, [UIScreen mainScreen].scale);
//                CGRect scaledImageRect = CGRectMake( 0.0, 0.0, w, h);
//                [image drawInRect:scaledImageRect];
//                image = UIGraphicsGetImageFromCurrentImageContext();
//                UIGraphicsEndImageContext();
//                
//                float x = (image.size.width-size.width)/2;
//                float y = ((image.size.height-size.height)/2)+kCameraTopHeight*image.scale;
//                NSLog(@"0000000000width=%f,height=%f",size.width,size.height);
//                
//                image = [image cropImageNoRotateWithBounds:CGRectMake(x, y, size.width, size.width)];

                
                completion(image);
            }
        }
    }];
}

+ (CGSize)preferSize
{
    float scale = [UIScreen mainScreen].scale;
    return CGSizeMake(kMainFrameWidth*scale, (kMainFrameHeight-100)*scale);
//    return CGSizeMake((kMainFrameHeight-100)*scale, kMainFrameWidth*scale);
}

@end
