// RDVTabBar.m
// RDVTabBarController
//
// Copyright (c) 2013 Robert Dimitrov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "RDVTabBar.h"
#import "RDVTabBarItem.h"
#import "UIButton+Addition.h"

@interface RDVTabBar ()

@property (nonatomic) CGFloat itemWidth;
@property (nonatomic) UIView *backgroundView;
@property (nonatomic) UIImageView *backgroundImage;
//@property (nonatomic) UILabel *lineLbl;
/**
 * 中间的button
 **/
@property (nonatomic) UIButton *centerButton;

@end

@implementation RDVTabBar

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInitialization];
    }
    return self;
}

- (id)init {
    return [self initWithFrame:CGRectZero];
}

- (void)commonInitialization {
    _backgroundView = [[UIView alloc] init];
    [self addSubview:_backgroundView];
//    _lineLbl = [[UILabel alloc] init];
//    _lineLbl.backgroundColor = [UIColor colorWithRed:216.f/255.f green:222.f/255.f blue:230.f/255.f alpha:1.0f];
//    _lineLbl.backgroundColor = UIColorFromRGB(0xd7d7d7);
//    _lineLbl.text = @"";
//    [_backgroundView addSubview:_lineLbl];
    [_backgroundView addSubview:[self backgroundImage]];
    [self setTranslucent:YES];
}

- (void)layoutSubviews {
    CGSize frameSize = self.frame.size;
    CGFloat minimumContentHeight = [self minimumContentHeight];
    
    [[self backgroundView] setFrame:CGRectMake(0, frameSize.height - minimumContentHeight, frameSize.width, frameSize.height)];
//    [self.lineLbl setFrame:CGRectMake(0, 0, frameSize.width, 0.5)];
    self.backgroundImage.frame = CGRectMake(0, 0, frameSize.width, frameSize.height);
    if (self.centerButton && [[self items]count]%2==0) {
        [self setItemWidth:roundf((frameSize.width - [self contentEdgeInsets].left - [self contentEdgeInsets].right - self.centerButton.frame.size.width) / [[self items] count])];
    }
    else{
        [self setItemWidth:roundf((frameSize.width - [self contentEdgeInsets].left -
                                   [self contentEdgeInsets].right) / [[self items] count])];
    }
    
    NSInteger index = 0;
    
    // Layout items
    for (RDVTabBarItem *item in [self items]) {
        CGFloat itemHeight = [item itemHeight];
        
        if (!itemHeight) {
            itemHeight = frameSize.height;
        }
        
        if (self.centerButton && [[self items]count]%2==0) {
            float x = 0.0f;
            if (index < [[self items]count]/2) {//左边的
                x = self.contentEdgeInsets.left + (index * self.itemWidth);
            }
            else{
                x = frameSize.width - self.contentEdgeInsets.right-([[self items]count]-index)*self.itemWidth;
            }
            [item setFrame:CGRectMake(x,
                                      roundf(frameSize.height - itemHeight) - self.contentEdgeInsets.top,
                                      self.itemWidth, itemHeight - self.contentEdgeInsets.bottom)];
        }
        else{
            [item setFrame:CGRectMake(self.contentEdgeInsets.left + (index * self.itemWidth),
                                      roundf(frameSize.height - itemHeight) - self.contentEdgeInsets.top,
                                      self.itemWidth, itemHeight - self.contentEdgeInsets.bottom)];
        }
        
        [item setNeedsDisplay];
        
        if (index == 3) {
            UIView *view = [item viewWithTag:100];
            if (view) {//小红点
                CGRect f = view.frame;
                f.origin.x = self.itemWidth/2+12.f;
                view.frame = f;
            }
        }
        
        index++;
    }
}

- (UIImageView *)backgroundImage
{
    if (!_backgroundImage) {
        UIImage *image = [UIImage imageNamed:@"tabbar_bg"];
        _backgroundImage = [[UIImageView alloc]initWithImage:[image stretchableImageWithLeftCapWidth:2 topCapHeight:2]];
        _backgroundImage.autoresizingMask = UIViewAutoresizingFlexibleWidth|
                          UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|
        UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
    }
    return _backgroundImage;
}

#pragma mark - Configuration

- (void)setItemWidth:(CGFloat)itemWidth {
    if (itemWidth > 0) {
        _itemWidth = itemWidth;
    }
}

- (void)setItems:(NSArray *)items {
    for (RDVTabBarItem *item in items) {
        [item removeFromSuperview];
    }
    
    _items = [items copy];
    for (RDVTabBarItem *item in items) {
        [item addTarget:self action:@selector(tabBarItemWasSelected:) forControlEvents:UIControlEventTouchDown];
        [item addTarget:self action:@selector(tabBarItemClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:item];
    }
}

- (void)setHeight:(CGFloat)height {
    [self setFrame:CGRectMake(CGRectGetMinX(self.frame), CGRectGetMinY(self.frame),
                              CGRectGetWidth(self.frame), height)];
}

- (CGFloat)minimumContentHeight {
    CGFloat minimumTabBarContentHeight = CGRectGetHeight([self frame]);
    
    for (RDVTabBarItem *item in [self items]) {
        CGFloat itemHeight = [item itemHeight];
        if (itemHeight && (itemHeight < minimumTabBarContentHeight)) {
            minimumTabBarContentHeight = itemHeight;
        }
    }
    
    return minimumTabBarContentHeight;
}

#pragma mark - Item selection

- (void)tabBarItemWasSelected:(id)sender {
    if ([[self delegate] respondsToSelector:@selector(tabBar:shouldSelectItemAtIndex:)]) {
        NSInteger index = [self.items indexOfObject:sender];
        if (![[self delegate] tabBar:self shouldSelectItemAtIndex:index]) {
            return;
        }
    }
    
    [self setSelectedItem:sender];
    
    if ([[self delegate] respondsToSelector:@selector(tabBar:didSelectItemAtIndex:)]) {
        NSInteger index = [self.items indexOfObject:self.selectedItem];
        [[self delegate] tabBar:self didSelectItemAtIndex:index];
    }
}

- (void)setSelectedItem:(RDVTabBarItem *)selectedItem {
    if (selectedItem == _selectedItem) {
        return;
    }
    [_selectedItem setSelected:NO];
    
    _selectedItem = selectedItem;
    [_selectedItem setSelected:YES];
}

-(void)tabBarItemClicked:(id)sender{
    if([[self delegate] respondsToSelector:@selector(tabBar:didSelectItemAtIndex:)]){
        NSInteger index = [self.items indexOfObject:sender];
        [[self delegate] tabBar:self didClickItemAtIndex:index];
        
    }
}

#pragma mark - Translucency

- (void)setTranslucent:(BOOL)translucent {
    _translucent = translucent;
    
    CGFloat alpha = (translucent ? 0.0 : 1.0);
    
    [_backgroundView setBackgroundColor:[UIColor colorWithRed:245/255.0
                                                        green:245/255.0
                                                         blue:245/255.0
                                                        alpha:alpha]];
}

#pragma mark - AddCenterView

-(void) addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage
{
    if (self.centerButton) {
        [self.centerButton removeFromSuperview];
    }
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    button.center = self.center;
    self.centerButton = button;
    [self addSubview:self.centerButton];
    [self.centerButton addTarget:self action:@selector(centerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat minimumContentHeight = [self minimumContentHeight];
    if (minimumContentHeight < buttonImage.size.height) {
        [self setHeight:buttonImage.size.height];
    }
}

- (void)setCenterButtonTitle:(NSString *)buttonTitle
{
    [self.centerButton setTitle:buttonTitle forState:UIControlStateNormal];
    [self.centerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.centerButton.titleLabel.font = [UIFont boldSystemFontOfSize:11.f];
    
    //设置button的title下移
    CGPoint buttonBoundsCenter = CGPointMake(CGRectGetMidX(self.centerButton.bounds), CGRectGetMidY(self.centerButton.bounds));
    // 找出titleLabel最的center
    CGPoint endTitleLabelCenter = CGPointMake(buttonBoundsCenter.x, CGRectGetHeight(self.bounds)-CGRectGetMidY(self.centerButton.titleLabel.bounds));
    // 取得titleLabel最初的center
    CGPoint startTitleLabelCenter = self.centerButton.titleLabel.center;
    // 设置titleEdgeInsets
    
    CGFloat titleEdgeInsetsTop = endTitleLabelCenter.y-startTitleLabelCenter.y;
    
    CGFloat titleEdgeInsetsLeft = endTitleLabelCenter.x - startTitleLabelCenter.x;
    
    CGFloat titleEdgeInsetsBottom = -titleEdgeInsetsTop;
    
    CGFloat titleEdgeInsetsRight = -titleEdgeInsetsLeft;
    
    float iosV = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (iosV >= 8.0) {
        self.centerButton.titleEdgeInsets = UIEdgeInsetsMake(titleEdgeInsetsTop-40, titleEdgeInsetsLeft, titleEdgeInsetsBottom, titleEdgeInsetsRight);
    }
    else{
        self.centerButton.titleEdgeInsets = UIEdgeInsetsMake(titleEdgeInsetsTop-30, titleEdgeInsetsLeft, titleEdgeInsetsBottom, titleEdgeInsetsRight);
    }
}

- (void)centerButtonAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(centerButtonAction)]) {
        [self.delegate centerButtonAction];
    }
}

@end
