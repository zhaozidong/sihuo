//
//  WithDrawInputCell.m
//  QMMM
//
//  Created by kingnet  on 14-9-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "WithDrawInputCell.h"
#import "WithDrawTextField.h"

NSString * const kWithDrawInputCellIdentifer = @"kWithDrawInputCellIdentifer";

@interface WithDrawInputCell ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet WithDrawTextField *textField;

@end
@implementation WithDrawInputCell
@synthesize text = _text;

+ (WithDrawInputCell *)inputCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"WithDrawInputCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.textField.delegate = self;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    UIImageView *rightV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"alipay_icon_small"]];
    self.textField.rightView = rightV;
    [self.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPlaceholder:(NSString *)placeholder
{
    self.textField.attributedPlaceholder = [AppUtils placeholderAttri:placeholder];
}

- (void)setIsShowRightView:(BOOL)isShowRightView
{
    if (isShowRightView) {
        self.textField.rightViewMode = UITextFieldViewModeAlways;
    }
    else{
        self.textField.rightViewMode = UITextFieldViewModeNever;
    }
}

- (NSString *)text
{
    if (self.textField.text == nil) {
        return @"";
    }
    return self.textField.text;
}

- (void)setText:(NSString *)text
{
    self.textField.text = text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

+ (CGFloat)rowHeight
{
    return 45.f;
}

- (void)setTextFieldEnabled:(BOOL)textFieldEnabled
{
    _textFieldEnabled = textFieldEnabled;
    self.textField.enabled = _textFieldEnabled;
}

- (void)setKeboardType:(UIKeyboardType)keboardType
{
    self.textField.keyboardType = keboardType;
}

- (void)textFieldDidChange:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputText:)]) {
        UITextField *field = (UITextField *)sender;
        [self.delegate inputText:field.text];
    }
}

@end
