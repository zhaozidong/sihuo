//
//  CloNetworkUtil.m
//  QMMM
//
//  Created by kingnet  on 14-9-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "CloNetworkUtil.h"

@implementation CloNetworkUtil

//初始化reachability
- (Reachability *)reachability{
    Reachability *r = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    return r;
}

//判断网络是否可用
- (BOOL)getNetWorkStatus{
    if ([[self reachability] currentReachabilityStatus] == NotReachable) {
        return NO;
    }else {
        return YES;
    }
}

/**
 获取网络类型
 return
 */
- (NSString *)getNetWorkType
{
    NSString *netWorkType;
    Reachability *reachability = [self reachability];
    switch ([reachability currentReachabilityStatus]) {
        case ReachableViaWiFi:   //Wifi网络
            netWorkType = @"wifi";
            break;
        case ReachableViaWWAN:  //无线广域网
            netWorkType = @"wwan";
            break;
        default:
            netWorkType = @"no";
            break;
    }
    return netWorkType;
}

@end


