//
//  OrderDataInfo.m
//  QMMM
//
//  Created by hanlu on 14-9-22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderDataInfo.h"
#import "NSString+Empty.h"
#import "FMResultSet.h"
#import "NSString+JSONCategories.h"
#import "NSDictionary+JSONCategories.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//简要订单信息

const NSInteger kRefundWaitDays = 10;

@implementation OrderDataInfo

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self && dict && [dict isKindOfClass:[NSDictionary class]]) {
        id temp = [dict objectForKey:@"id"];//
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _orderId = [dict objectForKey:@"id"];
        }
        
        temp = [dict objectForKey:@"goods_id"];//
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _goodsId = (uint64_t)[temp longLongValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _goodsId = [temp unsignedLongLongValue];
        }

        temp = [dict objectForKey:@"goods_desc"];//
        if(temp && [temp length] > 0) {
            _goodsDesc = temp;
        } else {
            _goodsDesc = @"";
        }
        
        temp = [dict objectForKey:@"photo"]; //
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _goodsImage = [dict objectForKey:@"photo"];
        } else {
            _goodsImage = @"";
        }
        
        temp = [dict objectForKey:@"remain_pay"]; //余额支付
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _balancePay = [temp floatValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _balancePay = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"third_pay"]; //第三方支付
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _aliPay = [temp floatValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _aliPay = [temp floatValue];
        }


        temp = [dict objectForKey:@"amount"]; //支付总金额
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _payAmount = [temp floatValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _payAmount = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"price"];//单价
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _goodsPrice = [temp floatValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _goodsPrice = [temp floatValue];
        }

        temp = [dict objectForKey:@"num"];//数量
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _buy_num = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _buy_num = [temp intValue];
        }
        
        temp = [dict objectForKey:@"payment_status"];//支付状态
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _paymentStatus = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _paymentStatus = [temp intValue];
        }
        
        temp = [dict objectForKey:@"trade_status"]; //订单状态
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _order_status = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _order_status = [temp intValue];
        }
        
        temp = [dict objectForKey:@"last_status"]; //最后订单停留的位置
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            _last_status = [temp intValue];
        } else if (temp && [temp isKindOfClass:[NSString class]]){
            _last_status = [temp intValue];
        }

        temp = [dict objectForKey:@"order_ts"];//买家下单时间
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _order_time = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _order_time = [temp intValue];
        }
        
        temp = [dict objectForKey:@"express_ts"]; //发货时间
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _express_time = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _express_time = [temp intValue];
        }
        
        temp = [dict objectForKey:@"user_id"]; //买家/卖家 id
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _user_Id = (uint64_t)[temp longLongValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _user_Id = [temp unsignedLongLongValue];
        }
        
        temp = [dict objectForKey:@"nickname"]; //卖家/买家 昵称
        if(temp && [temp length] > 0) {
            _user_nick = temp;
        } else {
            _user_nick = @"";
        }
        
        temp = [dict objectForKey:@"username"];
        if(temp && [temp length] > 0) {
            _user_name = temp;
        } else {
            _user_name = @"";
        }

        temp = [dict objectForKey:@"receiver"]; //收件人
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _recipients = [dict objectForKey:@"receiver"];
        } else {
            _recipients = @"";
        }
        
        temp = [dict objectForKey:@"mobile"];//联系电话
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _recipients_phone = [dict objectForKey:@"mobile"];
        } else {
            _recipients_phone = @"";
        }
        
//        temp = [dict objectForKey:@"city"];
//        if(temp && [temp length] > 0) {
//            _recipients_city = temp;
//        } else {
//            _recipients_city = @"";
//        }
        
        temp = [dict objectForKey:@"address"]; //收货地址
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _recipients_address = [dict objectForKey:@"address"];
        } else {
            _recipients_address = @"";
        }
        
        temp = [dict objectForKey:@"express_co_name"]; //快递公司名称
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _express_name = [dict objectForKey:@"express_co_name"];
        } else {
            _express_name = @"";
        }
        
        temp = [dict objectForKey:@"express_co"]; //快递公司编号
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            _express_co = [dict objectForKey:@"express_co"];
        }
        else{
            _express_co = @"";
        }
        
        temp = [dict objectForKey:@"express_no"]; //快递编号
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _express_num = [dict objectForKey:@"express_no"];
        } else {
            _express_num = @"";
        }
        
        temp = [dict objectForKey:@"comments"]; //评价
        if (temp && [temp isKindOfClass:[NSString class]]) {
            _comments = [dict objectForKey:@"comments"];
        }
        else{
            _comments = @"";
        }
        
        temp = [dict objectForKey:@"rate"]; //评论类型 1差评 2中评 3好评
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            _rate = [temp intValue];
        }
        else if(temp && [temp isKindOfClass:[NSString class]]){
            _rate = [temp intValue];
        }
        
        temp = [dict objectForKey:@"rate_ts"];//评价时间
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            _rate_ts = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            _rate_ts = [temp intValue];
        }
        
        temp = [dict objectForKey:@"express_fee"]; //运费
        if (temp && [temp isKindOfClass:[NSString class]]) {
            _freight = [temp floatValue];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            _freight = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"sys_ts"]; //系统时间
        if (temp && [temp isKindOfClass:[NSString class]]) {
            _sys_ts = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            _sys_ts = [temp intValue];
        }
        
        temp = [dict objectForKey:@"reject_reason"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            _rejectReason = temp;
        }
        else{
            _rejectReason = @"";
        }
        
        temp = [dict objectForKey:@"call"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            _mobile = temp;
        }
        else{
            _mobile = @"";
        }
        
        temp = [dict objectForKey:@"refund_amount"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            _refund_amount = [temp floatValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]) {
            _refund_amount = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"apply_ts"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            _apply_ts = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            _apply_ts = [temp intValue];
        }
        
    }
    return self;
}

@end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//简要订单信息

@implementation OrderSimpleInfo

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self && dict && [dict isKindOfClass:[NSDictionary class]]) {
        id temp = [dict objectForKey:@"id"];  //
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _order_id = [dict objectForKey:@"id"];
        }
        
        temp = [dict objectForKey:@"order_ts"];  //
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _order_pub_time = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _order_pub_time = [temp intValue];
        }

        
        temp = [dict objectForKey:@"goods_id"];  //
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _goods_id = (uint64_t)[temp longLongValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _goods_id = [temp unsignedLongLongValue];
        }
        
        temp = [dict objectForKey:@"goods_desc"];  //
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _goods_desc = temp;
        } else {
            _goods_desc = @"";
        }
        
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _goods_image = [dict objectForKey:@"photo"];
        } else {
            _goods_image = @"";
        }
        
        temp = [dict objectForKey:@"price"]; //
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _goods_price = [temp floatValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _goods_price = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"num"]; //
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _goods_num = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _goods_num = [temp intValue];
        }
        
        temp = [dict objectForKey:@"amount"]; //
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _payAmmount = [temp floatValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _payAmmount = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"receiver"]; //
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _recipients = temp;
        } else {
            _recipients = @"";
        }
        
        temp = [dict objectForKey:@"mobile"];//
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _cellPhone = temp;
        } else {
            _cellPhone = @"";
        }
        
        temp = [dict objectForKey:@"address"];//
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _recipients_address = temp;
        } else {
            _recipients_address = @"";
        }
        
        temp = [dict objectForKey:@"express_co"]; //快递公司编号
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            _express_co = [dict objectForKey:@"express_co"];
        }
        else{
            _express_co = @"";
        }
        
        temp = [dict objectForKey:@"express_co_name"]; //快递公司名称
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _express_name = [dict objectForKey:@"express_co_name"];
        } else {
            _express_name = @"";
        }
        
        temp = [dict objectForKey:@"express_no"]; //快递单号
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            _express_num = temp;
        } else {
            _express_num = @"";
        }

        temp = [dict objectForKey:@"payment_status"]; //
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _payment_status = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _payment_status = [temp intValue];
        }

        temp = [dict objectForKey:@"trade_status"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _order_status = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _order_status = [temp intValue];
        }
        
        temp = [dict objectForKey:@"express_fee"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            _freight = [temp floatValue];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            _freight = [temp floatValue];
        }
    }
    return self;
}

- (id)initWithOrderSimpleInfo:(OrderSimpleInfo *)info
{
    self = [super init];
    if(self) {
        self.order_id = info.order_id;
        self.order_pub_time = info.order_pub_time;
        self.goods_id = info.goods_id;
        self.goods_image = info.goods_image;
        self.goods_desc = info.goods_desc;
        self.goods_price = info.goods_price;
        self.goods_num = info.goods_num;
        self.payAmmount = info.payAmmount;
        self.recipients = info.recipients;
        self.cellPhone = info.cellPhone;
        self.recipients_address = info.recipients_address;
        self.express_name = info.express_name;
        self.express_num = info.express_num;
        self.express_co = info.express_co;
        self.payment_status = info.payment_status;
        self.order_status = info.order_status;
        self.freight = info.freight;
    }
    return self;
}

- (NSString *)toJSONString
{
    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
    [dict setObject:_order_id forKey:@"id"];
    [dict setObject:@(_order_pub_time) forKey:@"order_ts"];
    [dict setObject:@(_goods_id) forKey:@"goods_id"];
    [dict setObject:([_goods_desc length] > 0 ? _goods_desc : @"") forKey:@"goods_desc"];
    [dict setObject:([_goods_image length] > 0 ? _goods_image : @"") forKey:@"photo"];
    [dict setObject:@(_goods_price) forKey:@"price"];
    [dict setObject:@(_goods_num) forKey:@"num"];
    [dict setObject:@(_payAmmount) forKey:@"amount"];
    [dict setObject:([_recipients length] > 0 ? _recipients : @"") forKey:@"receiver"];
    [dict setObject:([_cellPhone length] > 0 ? _cellPhone : @"") forKey:@"mobile"];
    [dict setObject:([_recipients_address length] > 0 ? _recipients_address : @"") forKey:@"address"];
    [dict setObject:([_express_num length] > 0 ? _express_num : @"") forKey:@"express_no"];
    [dict setObject:@(_payment_status) forKey:@"payment_status"];
    [dict setObject:@(_order_status) forKey:@"trade_status"];
    [dict setObject:([_express_co length] > 0 ? _express_co : @"") forKey:@"express_co"];
    [dict setObject:([_express_name length] > 0 ? _express_name : @"") forKey:@"express_co_name"];
    [dict setObject:@(_freight) forKey:@"express_fee"];
    return [dict toString];
}

//- (id)initWithFMResultSet:(FMResultSet *)resultSet
//{
//    NSDictionary * dict = [resultSet stringForColumnIndex:1];
//    return
////    self = [super init];
////    if(self) {
////        self.gid = [resultSet unsignedLongLongIntForColumnIndex:0];
////        NSString * temp = [resultSet stringForColumnIndex:1];
////        self.picturelist = [[GoodsPictureList alloc] initWithArray:[temp toArray]];
////        self.desc = [resultSet stringForColumnIndex:2];
////        self.price = [resultSet doubleForColumnIndex:3];
////        self.orig_price = [resultSet doubleForColumnIndex:4];
////        self.cate_type = [resultSet intForColumnIndex:5];
////        self.city = [resultSet stringForColumnIndex:6];
////        self.msgAudio = [resultSet stringForColumnIndex:7];
////        self.msgAudioSeconds = (uint)[resultSet intForColumnIndex:8];
////        self.pub_time = [resultSet intForColumnIndex:9];
////        self.sellerUserId = [resultSet unsignedLongLongIntForColumnIndex:10];
////        self.sellerHeadIcon = [resultSet stringForColumnIndex:11];
////        self.sellerNick = [resultSet stringForColumnIndex:12];
////        temp = [resultSet stringForColumnIndex:13];
////        self.favorList = [[FavorList alloc] initWithArray:[temp toArray]];
////        self.favorCounts = [resultSet intForColumnIndex:14];
////        self.commentCounts = [resultSet intForColumnIndex:15];
////        self.isFavor = [resultSet boolForColumnIndex:16];
////    }
////    return self;
//}
//

@end
