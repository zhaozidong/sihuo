//
//  QMConfirmGoodsTitleCell.m
//  QMMM
//
//  Created by hanlu on 14-11-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMConfirmGoodsTitleCell.h"

NSString * const kQMConfirmGoodsTitleCellIdentifier = @"kQMConfirmGoodsTitleCellIdentifier";

@implementation QMConfirmGoodsTitleCell

+ (id)cellForConfirmGoodsTitle
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMConfirmGoodsTitleCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [_firstLabel setText:@"确认您已收到该商品"];
    [_firstLabel setTextColor:kDarkTextColor];
    
    [_secondLabel setText:@""];
    [_secondLabel setTextColor:kRedColor];
    
    [_thirdLabel setText:@"将汇入卖家账户"];
    [_thirdLabel setTextColor:kDarkTextColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGSize textSize = [_firstLabel.text sizeWithFont:_firstLabel.font];
    CGFloat start = self.contentView.frame.origin.x + (self.contentView.bounds.size.width - textSize.width - _flagImageView.image.size.width - 5) / 2;
    
    CGRect frame = _flagImageView.frame;
    frame.origin.x = start;
    _flagImageView.frame = frame;
    
    frame = _firstLabel.frame;
    frame.origin.x = _flagImageView.frame.origin.x + _flagImageView.frame.size.width + 5;
    _firstLabel.frame = frame;
}

@end
