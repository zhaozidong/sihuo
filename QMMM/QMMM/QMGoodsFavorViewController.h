//
//  QMGoodsFavorViewController.h
//  QMMM
//  在卖/已卖/已买的列表界面
//  Created by hanlu on 14-9-22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GoodDataInfo.h"

@class MyOrderList;

@class QMGoodsFootView;

@interface QMGoodsFavorViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView * tableView;

@property (nonatomic, strong) IBOutlet QMGoodsFootView * footView;

@property (nonatomic, strong) MyOrderList * myOrderList;

- (id)initWithControllerType:(EControllerType)type;

@end
