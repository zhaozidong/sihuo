//
//  SearchHistoryDao.h
//  QMMM
//
//  Created by kingnet  on 15-1-13.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseDao.h"

@interface SearchHistoryDao : BaseDao

+ (SearchHistoryDao *)sharedInstance;

@end
