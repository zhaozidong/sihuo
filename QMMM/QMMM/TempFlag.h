//
//  TempFlag.h
//  QMMM
//  放置一些需要经过几个界面来传标志的临时变量
//  Created by kingnet  on 15-1-15.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TempFlag : NSObject

extern NSString *const kHongBao;
extern NSString *const kBalance;
extern NSString * kChoice;

//从活动进入发布或正常发布
extern NSString * kPublishGoodsFrom; //choice  ""

//从红包进入或余额进入
extern NSString * kWithdrawFrom; //hongbao  balance

@end
