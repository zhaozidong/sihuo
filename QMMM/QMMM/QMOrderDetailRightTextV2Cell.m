//
//  QMOrderDetailRightTextV2Cell.m
//  QMMM
//
//  Created by hanlu on 14-11-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMOrderDetailRightTextV2Cell.h"

NSString * const kQMOrderDetailRightTextV2CellIdentifier = @"kQMOrderDetailRightTextV2CellIdentifier";

@implementation QMOrderDetailRightTextV2Cell

+ (id)cellForOrderDetailRightTextV2
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMOrderDetailRightTextV2Cell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
    [_nameLabel setTextColor:UIColorFromRGB(0x9198a3)];
    [_valueLabel setTextColor:UIColorFromRGB(0x9198a3)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
