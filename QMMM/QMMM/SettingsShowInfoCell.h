//
//  SettingsShowInfoCell.h
//  QMMM
//
//  Created by kingnet  on 14-11-5.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kSettingsShowInfoCellIdentifier;

@interface SettingsShowInfoCell : UITableViewCell

+ (NSArray *)items;

+ (CGFloat)cellHeight;

+ (UINib *)nib;

@property (nonatomic, strong) NSString *leftText;

@property (nonatomic, strong) NSString *rightText;

@property (nonatomic, assign) BOOL isShowNewIcon;

@end
