//
//  FriendInfo.h
//  QMMM
//
//  Created by hanlu on 14-11-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@class  FMResultSet;

@interface FriendInfo : NSObject

//uid
@property (nonatomic, assign) uint64_t uid;

//备注
@property (nonatomic, strong) NSString * remark;

//昵称
@property (nonatomic, strong) NSString *nickname;

//头像
@property (nonatomic, strong) NSString * icon;

//手机号码
@property (nonatomic, strong) NSString * cellphone;

//拼音
@property (nonatomic, strong) NSString * pinyin;

//展示的名字
@property (nonatomic, strong) NSString * showname;

//提示
@property (nonatomic, strong) NSString *tips;


- (id)initWithFMResultSet:(FMResultSet *)resultSet;

- (id)initWithDictionary:(NSDictionary *)dict;

- (void)updateRemark:(NSString *)remark;

@end

//section info
@interface FriendSectionInfo : NSObject

//letter: A~Z
@property (nonatomic, strong) NSString * letter;

//length
@property (nonatomic, assign) NSUInteger length;

//pos
@property (nonatomic, assign) NSUInteger pos;

- (id)initWithSection:(NSString *)letter pos:(NSUInteger)pos length:(NSUInteger)length;

@end
