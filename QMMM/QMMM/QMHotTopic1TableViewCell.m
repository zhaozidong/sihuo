//
//  QMHotTopic1TableViewCell.m
//  QMMM
//
//  Created by Derek on 15/3/17.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMHotTopic1TableViewCell.h"

#import "UIImageView+QMWebCache.h"


@implementation QMHotTopic1TableViewCell{
    NSArray *_arr;
}

+(QMHotTopic1TableViewCell *)hotTopic1{
    
    return [[[NSBundle mainBundle] loadNibNamed:@"QMHotTopic1TableViewCell" owner:self options:nil] lastObject];
}


- (void)awakeFromNib {
    // Initialization code
    
    _topicImg1.layer.borderWidth=0.5f;
    _topicImg1.layer.borderColor=UIColorFromRGB(0xe1e1e1).CGColor;
    _topicImg1.userInteractionEnabled=YES;
    UITapGestureRecognizer *tap1=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImg:)];
    tap1.numberOfTapsRequired=1;
    [_topicImg1 addGestureRecognizer:tap1];
    
    
    _topicImg2.layer.borderWidth=0.5f;
    _topicImg2.layer.borderColor=UIColorFromRGB(0xe1e1e1).CGColor;
    _topicImg2.userInteractionEnabled=YES;
    UITapGestureRecognizer *tap2=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImg:)];
    tap2.numberOfTapsRequired=1;
    [_topicImg2 addGestureRecognizer:tap2];
    
    _topicImg3.layer.borderWidth=0.5f;
    _topicImg3.layer.borderColor=UIColorFromRGB(0xe1e1e1).CGColor;
    _topicImg3.userInteractionEnabled=YES;
    UITapGestureRecognizer *tap3=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImg:)];
    tap3.numberOfTapsRequired=1;
    [_topicImg3 addGestureRecognizer:tap3];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateWithArr:(NSArray *)arr{
    _arr=arr;
    for (int i=0;i<=[arr count];i++) {
        for(UIImageView *imgView in self.contentView.subviews)
            if (imgView.tag==i+1) {
                SpecialTopicEntity *entity=[arr objectAtIndex:i];
                [imgView qm_setImageWithURL:entity.icon placeholderImage:nil completed:^(UIImage *image, NSError *error, QMImageCacheType cacheType, NSURL *imageURL) {
                    if (error) {
                        NSLog(@"热门图片加载失败！");
                    }
                }];
            }
    }
}

- (void)tapImg:(UIGestureRecognizer *)gestureRecognizer{
    if (_delegate && [_delegate respondsToSelector:@selector(didTapHotTopicCell:)]) {
        UIImageView *imgView=(UIImageView *)gestureRecognizer.view;
        SpecialTopicEntity *entity=[_arr objectAtIndex:imgView.tag-1];
        [_delegate didTapHotTopicCell:entity];
    }
}

@end
