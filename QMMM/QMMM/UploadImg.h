//
//  UploadImg.h
//  Channel
//
//  Created by deling on 12-11-27.
//  Copyright (c) 2012年 zn. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 上传图片的类
 **/
typedef void(^UploadBlock)(UIImage *img);

@interface UploadImg : NSObject<UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIViewController *viewController;
    UIPopoverController *popController;
    UIImage *image;
}
@property (nonatomic,retain) NSURLConnection* aSynConnection;
@property (nonatomic,retain) NSInputStream *inputStreamForFile;
@property (nonatomic, copy) UploadBlock uploadBlock;
@property (nonatomic, strong) UIImagePickerController *photoPickController;
/**
 弹出相册和照相机的视图
 **/
@property (strong, nonatomic) UIViewController *viewController;
/**
 用于ipad上弹出相册和相机的视图
 **/
@property (strong, nonatomic) UIPopoverController *popController;
/**
 通过相册或拍照得到的图片
 **/
@property (strong, nonatomic) UIImage *image;

/**
 上传图片到服务器
 @param vc 即本类的实例变量viewController
 **/
-(void)uploadImg:(UIViewController *)vc uploadBlock:(UploadBlock)uploadBlock;

@end
