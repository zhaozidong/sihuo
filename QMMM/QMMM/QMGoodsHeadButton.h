//
//  QMGoodsHeadButton.h
//  QMMM
//
//  Created by hanlu on 14-9-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QMGoodsHeadButtonDelegate;

@interface QMGoodsHeadButton : UIView {
    CGFloat _margin;
    CGFloat _roundCorner;
    NSString * _text;
    UILabel * _textLabel;
}

@property (nonatomic, assign) uint64_t userId;

@property (nonatomic, weak) id<QMGoodsHeadButtonDelegate> delegate;

@property (nonatomic, strong) UIImageView * imageView;

//设置头像
- (void)setImagePath:(NSString *)imagePath;

//小头像
- (void)setSmallImagePath:(NSString *)imagePath;

//设置圆角
- (void)setRoundCorner:(CGFloat)roundCorner;

//设置描边
- (void)setMargin:(CGFloat)margin;

//设置描边颜色
- (void)setMarginColor:(UIColor *)color;

//设置文字
- (void)setText:(NSString *)text;

//设置头像
- (void)setImage:(UIImage *)image;


@end

@protocol QMGoodsHeadButtonDelegate <NSObject>

@optional

- (void)didHeadButtonClicked:(uint64_t)userId;

@end