//
//  AddressListCell.m
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "AddressListCell.h"
#import "AddressEntity.h"

#define BUTTON_THRESHOLD 80

NSString * const kAddressCellIdentifier = @"kAddressCellIdentifier";

@interface AddressListCell (){
    
    addressEditMode _editMode;
}

@end
@implementation AddressListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.font = [UIFont systemFontOfSize:15.f];
        self.detailTextLabel.font = [UIFont systemFontOfSize:12.f];
        self.textLabel.textColor = kDarkTextColor;
        self.detailTextLabel.textColor = kGrayTextColor;
        self.detailTextLabel.numberOfLines = 0;
        self.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.revealDirection = RMSwipeTableViewCellRevealDirectionRight;
        self.animationType = RMSwipeTableViewCellAnimationTypeEaseOut;
        self.panElasticityStartingPoint = 2*BUTTON_THRESHOLD;
        //self.panElasticityStartingPoint = BUTTON_THRESHOLD;
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame=self.detailTextLabel.frame;
    if (_editMode==addressEditModeEdit) {
        frame.size.width=kMainFrameWidth-130;
    }else{
        frame.size.width=kMainFrameWidth-70;
    }
    self.detailTextLabel.frame=frame;

    frame = self.leftImgV.frame;
    frame.origin.x =self.detailTextLabel.frame.origin.x+self.detailTextLabel.frame.size.width+10;
//    frame.origin.x = CGRectGetWidth(self.contentView.frame)-20.f-frame.size.width;
    frame.origin.y = CGRectGetHeight(self.contentView.frame)/2 - frame.size.height/2;
    self.leftImgV.frame = frame;
    self.upView.frame = CGRectMake(0, 0, CGRectGetWidth(self.bounds), 1);
    self.bottomView.frame = CGRectMake(0, CGRectGetHeight(self.bounds)-1, CGRectGetWidth(self.bounds), 1);
//    [self.deleteButton setFrame:CGRectMake(CGRectGetMaxX(self.frame) - BUTTON_THRESHOLD, 0, BUTTON_THRESHOLD, CGRectGetHeight(self.contentView.frame))];
}

//-(UIButton*)deleteButton {
//    if (!_deleteButton) {
//        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_deleteButton setBackgroundColor:UIColorFromRGB(0xff5b5b)];
//        [_deleteButton setImage:[UIImage imageNamed:@"delAddress_btn"] forState:UIControlStateNormal];
////        [_deleteButton setBackgroundImage:[UIImage imageNamed:@"DeleteButtonBackground"] forState:UIControlStateNormal];
////        [_deleteButton setBackgroundImage:[UIImage imageNamed:@"DeleteButtonBackground"] forState:UIControlStateHighlighted];
////        [_deleteButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
////        [_deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
////        [_deleteButton setTitle:[AppUtils localizedCommonString:@"QM_Text_Delete"] forState:UIControlStateNormal];
//        [_deleteButton setFrame:CGRectMake(CGRectGetMaxX(self.frame) - BUTTON_THRESHOLD, 0, BUTTON_THRESHOLD, CGRectGetHeight(self.contentView.frame))];
//        [_deleteButton addTarget:self action:@selector(deleteAction) forControlEvents:UIControlEventTouchUpInside];
//        [_deleteButton setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
//    }
//    return _deleteButton;
//}

- (UIButton *)updateButton
{
    if (!_updateButton) {
        _updateButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_updateButton setBackgroundImage:[UIImage imageNamed:@"UpdateButtonBackground"] forState:UIControlStateNormal];
        [_updateButton setBackgroundImage:[UIImage imageNamed:@"UpdateButtonBackground"] forState:UIControlStateHighlighted];
        [_updateButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
        [_updateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_updateButton setTitle:[AppUtils localizedCommonString:@"QM_Text_Update"] forState:UIControlStateNormal];
        [_updateButton setFrame:CGRectMake(CGRectGetMaxX(self.frame) - 2*BUTTON_THRESHOLD, 0, BUTTON_THRESHOLD, CGRectGetHeight(self.contentView.frame))];
        [_updateButton addTarget:self action:@selector(updateAction) forControlEvents:UIControlEventTouchUpInside];
        [_updateButton setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
    }
    return _updateButton;
}

- (UIImageView *)leftImgV
{
    if (!_leftImgV) {
        UIImage *image = [UIImage imageNamed:@"select_press"];
        _leftImgV = [[UIImageView alloc]initWithImage:image];
        [self.contentView addSubview:_leftImgV];
    }
    return _leftImgV;
}

- (UIView *)upView
{
    if (!_upView) {
        _upView = [[UIView alloc]initWithFrame:CGRectZero];
        _upView.backgroundColor = UIColorFromRGB(0xff5b5b);
        [self.contentView addSubview:_upView];
    }
    return _upView;
}

- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]initWithFrame:CGRectZero];
        _bottomView.backgroundColor = UIColorFromRGB(0xff5b5b);
        [self.contentView addSubview:_bottomView];
    }
    return _bottomView;
}

//- (void)deleteAction
//{
//    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(cellDidDelegate:)]) {
//        [self.cellDelegate cellDidDelegate:self];
//    }
//}

- (void)updateAction
{
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(cellDidUpdate:)]) {
        [self.cellDelegate cellDidUpdate:self];
    }
}

//-(void)didStartSwiping {
//    [super didStartSwiping];
////    [self.backView addSubview:self.updateButton];
//    [self.backView addSubview:self.deleteButton];
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(AddressEntity *)entity withMode:(addressEditMode)editMode
{
    if (entity) {
        _editMode=editMode;
        self.textLabel.text = [NSString stringWithFormat:@"%@ %@", entity.consignee, entity.phone];
        if (entity.isDefault) {
            NSString *str = @"默认";
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@%@", str, entity.locationName, entity.detailAddress]];
            [string addAttribute:NSForegroundColorAttributeName value:kRedColor range:NSMakeRange(0, str.length)];
            self.detailTextLabel.attributedText = string;
        }
        else{
            self.detailTextLabel.text = [NSString stringWithFormat:@"%@%@", entity.locationName, entity.detailAddress];
        }
    }
}

+ (CGFloat)cellHeight:(NSString *)str
{
    CGSize maximumLabelSize = CGSizeMake(kMainFrameWidth-30, FLT_MAX);
    UIFont *font = [UIFont systemFontOfSize:12.f];
    CGSize expectedLabelSize = [str sizeWithFont:font constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = expectedLabelSize.height + 30;
    if (height < 55.f) {
        return 55.f;
    }
    else{
        return height;
    }
}

- (void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    self.leftImgV.hidden = !_isSelected;
    self.upView.hidden = !_isSelected;
    self.bottomView.hidden = !_isSelected;
}

-(void)resetContentView {
    [UIView animateWithDuration:0.15f
                     animations:^{
                         self.contentView.frame = CGRectOffset(self.contentView.bounds, 0, 0);
                     }
                     completion:^(BOOL finished) {
                         self.shouldAnimateCellReset = YES;
                         [self cleanupBackView];
                     }];
}

//-(void)cleanupBackView {
//    [super cleanupBackView];
//    [_deleteButton removeFromSuperview];
//    _deleteButton = nil;
//}

@end
