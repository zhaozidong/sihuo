//
//  ReliabilityView.m
//  QMMM
//
//  Created by kingnet  on 15-4-14.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "ReliabilityView.h"

@interface ReliabilityView ()
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;

@end
@implementation ReliabilityView

+ (ReliabilityView *)reliabilityView
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"ReliabilityView" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = UIColorFromRGB(0xeeeeee);
    self.tipLabel.numberOfLines = 2;
    self.tipLabel.textColor = UIColorFromRGB(0x898989);
    self.tipLabel.preferredMaxLayoutWidth = kMainFrameWidth-20;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.minimumLineHeight = 20.f;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.firstLineHeadIndent = 14.f;
    NSString *text = @"商品为什么会出现在这里呢？秘密就是他们的主人有较高的可信度，那什么是可信度呢？点击查看>>";
    NSNumber *underline = [NSNumber numberWithInt:NSUnderlineStyleSingle];
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:text];
    NSRange range = [text rangeOfString:@"点击查看>>"];
    [attributeStr addAttribute:NSUnderlineStyleAttributeName value:underline range:range];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:kRedColor range:range];
    [attributeStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, text.length)];
    self.tipLabel.attributedText = attributeStr;
}

+ (CGFloat)viewHeight
{
    return 50.f;
}

@end
