//
//  LRCheckCodeViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "LRCheckCodeViewController.h"
#import "QMTextField.h"
#import "QMSubmitButton.h"
#import "TimerButton.h"

@interface LRCheckCodeViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (strong, nonatomic) IBOutlet UITableViewCell *checkCodeCell;
@property (weak, nonatomic) IBOutlet QMTextField *checkCodeTf;
@property (strong, nonatomic) UITableView *tableView;
@property (weak, nonatomic) IBOutlet QMSubmitButton *submitBtn;
@property (weak, nonatomic) IBOutlet TimerButton *timerBtn;
@end

@implementation LRCheckCodeViewController

+ (CGFloat)viewHeight
{
    return 45*+14+76+kMainTopHeight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupViews];
    [self.checkCodeTf becomeFirstResponder];
    [self.timerBtn startTiming];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupViews
{
    self.view.frame = CGRectMake(0, 0, kMainFrameWidth, [[self class]viewHeight]);
    //加载需要的xib
    [[NSBundle mainBundle]loadNibNamed:@"LRCheckCodeView" owner:self options:0];
    
    //设置tableView
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStylePlain];
    _tableView.scrollEnabled = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.rowHeight = 45.f;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kBorderColor;
    _tableView.tableFooterView = self.footerView;
    [self.view addSubview:_tableView];
    
    self.footerView.backgroundColor = [UIColor clearColor];
    
    UIImageView *imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkCode_icon"]];
    self.checkCodeTf.leftView = imgV;
    self.checkCodeTf.attributedPlaceholder = [AppUtils placeholderAttri:@"请输入收到的验证码"];
    self.checkCodeTf.delegate = self;
    
    [self.submitBtn addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    self.submitBtn.enabled = NO;
    [self.submitBtn setTitle:self.submitBtnTitle forState:UIControlStateNormal];
    
    [self.timerBtn addTarget:self action:@selector(timerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)submitAction:(id)sender
{
    if (self.checkCodeTf.text == nil || self.checkCodeTf.text.length == 0) {
        [AppUtils showAlertMessage:@"请输入验证码"];
        return;
    }
    [AppUtils closeKeyboard];
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickSubmit:)]) {
        [self.delegate clickSubmit:self.checkCodeTf.text];
    }
}

- (void)timerAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickGetCheckCode)]) {
        [self.delegate clickGetCheckCode];
    }
}

- (void)stopTimer
{
    [self.timerBtn endTiming];
}

- (void)startTimer
{
    [self.timerBtn startTiming];
}

#pragma mark - 
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.checkCodeCell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 14.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - 
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self submitAction:nil];
    return YES;
}

- (void)textDidChange:(UITextField *)textField
{
    if (self.checkCodeTf.text!=nil && self.checkCodeTf.text.length>0) {
        self.submitBtn.enabled = YES;
    }
    else{
        self.submitBtn.enabled = NO;
    }
}

@end
