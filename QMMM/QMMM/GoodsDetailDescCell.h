//
//  GoodsDetailDescCell.h
//  QMMM
//
//  Created by Shinancao on 14/12/15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString * const kGoodsDetailDescCellIdentifier;

@class GoodEntity;
@interface GoodsDetailDescCell : UITableViewCell

+ (GoodsDetailDescCell *)goodsDetailDescCell;

- (void)updateUI:(GoodEntity *)entity;

@end
