/************************************************************
  *  * EaseMob CONFIDENTIAL 
  * __________________ 
  * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved. 
  *  
  * NOTICE: All information contained herein is, and remains 
  * the property of EaseMob Technologies.
  * Dissemination of this information or reproduction of this material 
  * is strictly forbidden unless prior written permission is obtained
  * from EaseMob Technologies.
  */

#import "ChatViewController.h"

#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "DXChatBarMoreView.h"
#import "DXRecordView.h"
#import "DXFaceView.h"
#import "EMChatViewCell.h"
#import "EMChatTimeCell.h"
#import "ChatSendHelper.h"
#import "MessageReadManager.h"
#import "MessageModelManager.h"
#import "LocationViewController.h"
//#import "ChatGroupDetailViewController.h"
//#import "UIViewController+HUD.h"
//#import "WCAlertView.h"
#import "NSDate+Category.h"
#import "DXMessageToolBar.h"
#import "UIImageView+QMWebCache.h"
#import "UserInfoHandler.h"
#import "UserEntity.h"
#import "UserBasicInfo.h"
#import "PersonPageViewController.h"
#import "MsgData.h"
#import "NSString+JSONCategories.h"
#import "QMOrderDetailViewController.h"
#import "GoodsDetailViewController.h"
#import "GoodEntity.h"
#import "QMPreviewGoodsMsgCell.h"
#import "NSString+JSONCategories.h"
#import "MsgHandle.h"

#import "KL_ImagesZoomController.h"

#import "FriendInfo.h"
#import "QMChatFraudCell.h"

#import "QMQAViewController.h"

#import "AppDelegate.h"
#import "UIScrollView+UzysCircularProgressPullToRefresh.h"

#define KPageCount 20

@interface ChatViewController ()<UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, IChatManagerDelegate, DXChatBarMoreViewDelegate, DXMessageToolBarDelegate, LocationViewDelegate, IDeviceManagerDelegate, UIGestureRecognizerDelegate, EMChatViewBaseDelegate, QMPreviewGoodsMsgDelegate,QMChatFraudCellDelegate>
{
    UIMenuController *_menuController;
    UIMenuItem *_copyMenuItem;
    UIMenuItem *_deleteMenuItem;
    NSIndexPath *_longPressIndexPath;
    
    NSInteger _recordingCount;
    
    dispatch_queue_t _messageQueue;
    
    BOOL _isScrollToBottom;
    
    NSMutableArray * _tempMessageList;
    NSMutableArray * _imageURLList; //存储聊天记录里出现的图片的url
    
    
    NSInteger indexFraud;
}

@property (nonatomic) BOOL isChatGroup;
@property (strong, nonatomic) EMGroup *chatGroup;

@property (strong, nonatomic) NSMutableArray *dataSource;//tableView数据源
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) DXMessageToolBar *chatToolBar;

@property (strong, nonatomic) UIImagePickerController *imagePicker;

@property (strong, nonatomic) MessageReadManager *messageReadManager;//message阅读的管理者
@property (strong, nonatomic) EMConversation *conversation;//会话管理者
@property (strong, nonatomic) NSDate *chatTagDate;

@property (nonatomic) BOOL isScrollToBottom;
@property (nonatomic) BOOL isPlayingAudio;

@property (nonatomic, strong) KL_ImagesZoomController *imageZoomView;

@property (strong, nonatomic) NSDate *lastFraudMessageDate;

@end

@implementation ChatViewController

- (instancetype)initWithChatter:(NSString *)chatter
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        _isPlayingAudio = NO;
        
        //根据接收者的username获取当前会话的管理者
        _conversation = [[EaseMob sharedInstance].chatManager conversationForChatter:chatter isGroup:_isChatGroup];
        
        uint64_t userId = (uint64_t)[_conversation.chatter longLongValue];
        UserBasicInfo * info = [[UserInfoHandler sharedInstance] getBasicUserInfo:userId];
        if((nil == info)|| (info && [[info showname] length] == 0)) {
            self.title =  _conversation.chatter;
        } else {
            self.title = [info showname];
        }
    }
    return self;
}

- (instancetype)initWithGroup:(EMGroup *)chatGroup
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        _isChatGroup = YES;
        _chatGroup = chatGroup;
        
        //根据接收者的username获取当前会话的管理者
        _conversation = [[EaseMob sharedInstance].chatManager conversationForChatter:chatGroup.groupId isGroup:_isChatGroup];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.view.backgroundColor = [UIColor colorWithRed:249.0/255.0 green:249.0/255.0 blue:249.0/255.0 alpha:1.0];
    self.view.backgroundColor = kBackgroundColor;

    _tempMessageList = [[NSMutableArray alloc] init];
    _imageURLList = [[NSMutableArray alloc]init];
    
    [[[EaseMob sharedInstance] deviceManager] addDelegate:self onQueue:nil];
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
    //注册为SDK的ChatManager的delegate
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(exitGroup) name:@"ExitGroup" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground) name:@"applicationDidEnterBackground" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processUserBasicInfoLoadedNtf:) name:kReloadChatUserListNotify object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processUpdateFriendRemarkNotify:) name:kUpdateFriendRemarkNotify object:nil];
    
    _messageQueue = dispatch_queue_create("easemob.com", NULL);
    _isScrollToBottom = YES;
//    //通过会话管理者获取已收发消息
//    
//    NSArray *chats = [_conversation loadNumbersOfMessages:KPageCount before:[_conversation latestMessage].timestamp + 1];
//    [self.dataSource addObjectsFromArray:[self sortChatSource:chats]];
    
    [self.view addSubview:self.tableView];
    [self setupBarButtonItem];
//    [self.tableView addSubview:self.slimeView];
    [self.view addSubview:self.chatToolBar];
    
    //将self注册为chatToolBar的moreView的代理
    if ([self.chatToolBar.moreView isKindOfClass:[DXChatBarMoreView class]]) {
        [(DXChatBarMoreView *)self.chatToolBar.moreView setDelegate:self];
    }
    
    //通过会话管理者获取已收发消息
//    [self loadMoreMessages];
    [self setupRefresh];
    
    //会话消息置为未读
    [_conversation markAllMessagesAsRead:YES];
    
    uint64_t userId = (uint64_t)[_conversation.chatter longLongValue];
    UserBasicInfo * info = [[UserInfoHandler sharedInstance] getBasicUserInfo:userId];
    if(!info || (info && [info.nickname length] == 0)) {
        [[UserInfoHandler sharedInstance] getUserBasicInfo:[NSArray arrayWithObject:@(userId)] success:nil failed:nil];
    }
    
    UIPanGestureRecognizer * panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardHidden)];
    panGestureRecognizer.delegate = self;
    [_tableView addGestureRecognizer:panGestureRecognizer];
}

- (void)setupBarButtonItem
{
//    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
//    [backButton setImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
//    [self.navigationItem setLeftBarButtonItem:backItem];
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(back)];
    
    if (_isChatGroup) {
        UIButton *detailButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
        [detailButton setImage:[UIImage imageNamed:@"group_detail"] forState:UIControlStateNormal];
        [detailButton addTarget:self action:@selector(showRoomContact:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:detailButton];
    }
    else{
//        UIButton *clearButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
//        [clearButton setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
//        [clearButton addTarget:self action:@selector(removeAllMessages:) forControlEvents:UIControlEventTouchUpInside];
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:clearButton];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_isScrollToBottom) {
        [self scrollViewToBottom:YES];
    }
    else{
        _isScrollToBottom = YES;
    }
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(oneTapHandler:) name:kShowImageOneTapNotity object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // 设置当前conversation的所有message为已读
    [_conversation markAllMessagesAsRead:YES];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kShowImageOneTapNotity object:nil];
}

- (void)dealloc
{
    _tableView.delegate = nil;
    _tableView.dataSource = nil;
    _tableView = nil;
    
    _chatToolBar.delegate = nil;
    _chatToolBar = nil;
    
    [[EaseMob sharedInstance].chatManager stopPlayingAudio];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    [[EaseMob sharedInstance].chatManager removeDelegate:self];
    [[[EaseMob sharedInstance] deviceManager] removeDelegate:self];
}

- (void)back
{
    //判断当前会话是否为空，若符合则删除该会话
    EMMessage *message = [_conversation latestMessage];
    if (message == nil) {
        [[EaseMob sharedInstance].chatManager removeConversationByChatter:_conversation.chatter deleteMessages:YES append2Chat:YES];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - helper
- (NSURL *)convert2Mp4:(NSURL *)movUrl {
    NSURL *mp4Url = nil;
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:movUrl options:nil];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:avAsset];
    
    if ([compatiblePresets containsObject:AVAssetExportPresetHighestQuality]) {
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]initWithAsset:avAsset
                                                                              presetName:AVAssetExportPresetHighestQuality];
        mp4Url = [movUrl copy];
        mp4Url = [mp4Url URLByDeletingPathExtension];
        mp4Url = [mp4Url URLByAppendingPathExtension:@"mp4"];
        exportSession.outputURL = mp4Url;
        exportSession.shouldOptimizeForNetworkUse = YES;
        exportSession.outputFileType = AVFileTypeMPEG4;
        dispatch_semaphore_t wait = dispatch_semaphore_create(0l);
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            switch ([exportSession status]) {
                case AVAssetExportSessionStatusFailed: {
                    NSLog(@"failed, error:%@.", exportSession.error);
                } break;
                case AVAssetExportSessionStatusCancelled: {
                    NSLog(@"cancelled.");
                } break;
                case AVAssetExportSessionStatusCompleted: {
                    NSLog(@"completed.");
                } break;
                default: {
                    NSLog(@"others.");
                } break;
            }
            dispatch_semaphore_signal(wait);
        }];
        int timeout = (int)dispatch_semaphore_wait(wait, DISPATCH_TIME_FOREVER);
        if (timeout) {
            NSLog(@"timeout.");
        }
        if (wait) {
            //dispatch_release(wait);
            wait = nil;
        }
    }
    
    return mp4Url;
}

#pragma mark - getter

- (NSMutableArray *)dataSource
{
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
    }
    
    return _dataSource;
}

#pragma mark - Pull Refresh

/**
 *  集成刷新控件
 */
- (void)setupRefresh
{
    //设置下拉刷新
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshActionHandler:^{
        [weakSelf loadMoreMessages];
    }];
    
    [self.tableView triggerPullToRefresh];
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - self.chatToolBar.frame.size.height) style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tableView.delegate = self;
        _tableView.dataSource = self;
//        _tableView.backgroundColor = [UIColor colorWithRed:249.0/255.0 green:249.0/255.0 blue:249.0/255.0 alpha:1.0];
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = kBackgroundColor;
        
        [_tableView registerClass:[QMChatFraudCell class] forCellReuseIdentifier:@"fraudcell"];
        
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        lpgr.minimumPressDuration = .5;
        [_tableView addGestureRecognizer:lpgr];
    }
    
    return _tableView;
}

- (DXMessageToolBar *)chatToolBar
{
    if (_chatToolBar == nil) {
        _chatToolBar = [[DXMessageToolBar alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - [DXMessageToolBar defaultHeight], self.view.frame.size.width, [DXMessageToolBar defaultHeight])];
        _chatToolBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
        _chatToolBar.delegate = self;
    }
    
    return _chatToolBar;
}

- (BOOL)isKeyBoardUp
{
    CGFloat defaultY = self.view.frame.size.height - [DXMessageToolBar defaultHeight];
    return _chatToolBar.frame.origin.y < defaultY ? YES : NO;
}

- (UIImagePickerController *)imagePicker
{
    if (_imagePicker == nil) {
        _imagePicker = [[UIImagePickerController alloc] init];
        _imagePicker.delegate = self;
    }
    
    return _imagePicker;
}

- (MessageReadManager *)messageReadManager
{
    if (_messageReadManager == nil) {
        _messageReadManager = [MessageReadManager defaultManager];
    }
    
    return _messageReadManager;
}

- (NSDate *)chatTagDate
{
    if (_chatTagDate == nil) {
        _chatTagDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:0];
    }
    
    return _chatTagDate;
}

- (NSDate *)lastFraudMessageDate{
    if (nil==_lastFraudMessageDate) {
        _lastFraudMessageDate= [NSDate dateWithTimeIntervalInMilliSecondSince1970:0];
    }
    return _lastFraudMessageDate;
}

-(BOOL)checkIsFraudMsg:(NSString *)textMessage{
    if (nil == textMessage) {
        return NO;
    }
    NSString *word=@"银行|卡号|工行|工商|建行|交行|交通|招行|招商|光大|农行|邮政|储蓄|中行|浦发|兴业|广发|花旗|华夏|中信|民生|信用社|银联|信用卡|ATM|取款机|支付宝|帐号|卡号|网银|借记卡|刷卡|网汇e|转账|汇款|打款|订金|定金|预支付|投资|理财|投钱|打钱|打款|借钱|利息|中奖|划钱|充值|到付|面交|货到付款|当面付|冲钱|充钱|抽奖|特等奖|.{1}等奖|奖金|报酬|参与奖|收益|代理|销量|先付|即时付款|保证金|存钱|再付|尾款|密码|手续费|代收|货款|放款|押金|钱打|付清|现金|资金|会员卡|全款|测试费|掏钱|掏邮费|掏运费|先款|分期|首付|收购|互换|互寄|互拍|先货|先发货|预付|先给|再给|存款|取款|存取|中介|收款|捐赠|网购|QQ|扣扣|微信|微店|微商|淘宝|闲鱼|旺旺|小交易|跳蚤市场|58同城|钱包|闲蛋|猎趣|余额宝|Q币|比特币|财付通|余额宝|口袋购物|天猫|链接|淘宝|二手|赶集|百姓网|快递|第三方|跳蚤街|\\d{9,11}|\\d{16,19}";
    
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:word options:NSRegularExpressionCaseInsensitive error:&error];
        NSTextCheckingResult *result = [regex firstMatchInString:textMessage options:0 range:NSMakeRange(0, [textMessage length])];
        if (result) {
            if ([textMessage substringWithRange:result.range]) {
                return YES;
            }
        }
        return NO;
}

-(BOOL) checkIsLinkFraudMsg:(NSString *)message{
    NSString *word=@"www|http|com|cn";
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:word options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *result = [regex firstMatchInString:message options:0 range:NSMakeRange(0, [message length])];
    if (result) {
        if ([message substringWithRange:result.range]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < [self.dataSource count]) {
        id obj = [self.dataSource objectAtIndex:indexPath.row];
        if ([obj isKindOfClass:[NSString class]]) {
            EMChatTimeCell *timeCell = (EMChatTimeCell *)[tableView dequeueReusableCellWithIdentifier:@"MessageCellTime"];
            if (timeCell == nil) {
                timeCell = [[EMChatTimeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MessageCellTime"];
                timeCell.backgroundColor = [UIColor clearColor];
                timeCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            [timeCell setDateText:(NSString *)obj];
            
            return timeCell;
        }
        else{
            MessageModel *model = (MessageModel *)obj;
            if([model isPreviewGoodsMsg]) {
                return [self cellForPreviewGoods:tableView cellForRowAtIndexPath:indexPath];
            }else if([model isFraudTextMsg]){
                return [self cellForFraud:tableView cellForRowAtIndexPath:indexPath messageType:EMT_RECEIVE_FRAUD_TEXT];
            }else if ([model isFraudLinkMsg]){
                return [self cellForFraud:tableView cellForRowAtIndexPath:indexPath messageType:EMT_RECEIVE_FRAUD_LINK];
            }
            else{
                return [self cellForOtherMsg:tableView cellForRowAtIndexPath:indexPath];
            }
        }
    }
    return nil;
}

- (UITableViewCell *)cellForPreviewGoods:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMPreviewGoodsMsgCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMPreviewGoodsMsgCellIdentifier];
    if(nil == cell) {
        cell = [QMPreviewGoodsMsgCell cellFroPreviewGoodsMsg];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    
    MessageModel *model = (MessageModel *)[self.dataSource objectAtIndex:indexPath.row];
    [cell updateUI:model];
    
    if(![_tempMessageList containsObject:model.messageId]) {
        [_tempMessageList addObject:model.messageId];
    }
    return cell;
}


- (UITableViewCell *)cellForFraud:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath messageType:(MsgType)type{
    QMChatFraudCell *cell=[tableView dequeueReusableCellWithIdentifier:@"fraudcell"];
    cell.delegate=self;
    MessageModel *model = (MessageModel *)[self.dataSource objectAtIndex:indexPath.row];
    if (type==EMT_RECEIVE_FRAUD_TEXT) {
        [cell updateWithTextMessageModel:model];
    }else{
        [cell updateWithLinkMessageModel:model];
    }
    return cell;
}

- (UITableViewCell *)cellForOtherMsg:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id obj = [self.dataSource objectAtIndex:indexPath.row];
    MessageModel *model = (MessageModel *)obj;
    NSString *cellIdentifier = [EMChatViewCell cellIdentifierForMessageModel:model];
    EMChatViewCell *cell = (EMChatViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[EMChatViewCell alloc] initWithMessageModel:model reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.messageModel = model;
    NSDictionary * dict = model.message.ext;
    
    if(dict && [dict objectForKey:@"tpl"]) {
        NSDictionary * argDict = nil;
        id temp = [dict objectForKey:@"arg"];
        if([temp isKindOfClass:[NSDictionary class]]) {
            argDict = temp;
        } else {
            argDict = [temp toDict];
        }
        if(argDict && [argDict count] > 0) {
            
            //商品描述10字显示限制
            NSString * goodsDesc = [argDict objectForKey:@"desc"];
            if([goodsDesc length] > 10) {
                goodsDesc = [goodsDesc substringToIndex:10];
                goodsDesc = [goodsDesc stringByAppendingString:@"..."];
            }
            
            NSRange keyRange = NSMakeRange(NSNotFound, 0);
            NSString * displayString = @"";
            int msgType = [[dict objectForKey:@"tpl"] intValue];
            if(msgType == EMT_GOODS_PAYED) {
                displayString = [NSString stringWithFormat:@"亲，您的商品\"%@\"已经被我拍下啦，快来给我安排发货吧~点此查看详情", goodsDesc];
                keyRange = [displayString rangeOfString:@"点此查看详情"];
            } else if(msgType == EMT_GOODS_DELIVERED) {
                displayString = [NSString stringWithFormat:@"亲，您拍下的商品\"%@\"已经发货啦，点此查看详情", goodsDesc];
                keyRange = [displayString rangeOfString:@"点此查看详情"];
            } else if(msgType == EMT_GOODS_RECEIVED) {
                displayString = [NSString stringWithFormat:@"亲，您的商品\"%@\"已经被我签收并评价啦~点此查看详情", goodsDesc];
                keyRange = [displayString rangeOfString:@"点此查看详情"];
            }
            
            if([displayString length] > 0) {
                [cell setAttributeString:displayString colorRange:keyRange Color:[UIColor redColor]];
            }
        }
    }
    
    if(model.isSender) {
        [cell.headView setSmallImageUrl:[UserInfoHandler sharedInstance].currentUser.avatar];
    } else {
        uint64_t userId = (uint64_t)[_conversation.chatter longLongValue];
        UserBasicInfo * userInfo = [[UserInfoHandler sharedInstance] getBasicUserInfo:userId];
        if(userInfo) {
            [cell.headView setSmallImageUrl:userInfo.icon];
        }
    }
    cell.delegate = self;
    
    return cell;
}


#pragma mark - Table view delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSObject *obj = [self.dataSource objectAtIndex:indexPath.row];
    if ([obj isKindOfClass:[NSString class]]) {
        return 40;
    } else {
        
        MessageModel *model = (MessageModel *)obj;
        if([model isPreviewGoodsMsg]) {
            return [QMPreviewGoodsMsgCell heightForPreviewGoodsMsg];
        } else {
            return [EMChatViewCell tableView:tableView heightForRowAtIndexPath:indexPath withObject:(MessageModel *)obj];
        }
    }
}

#pragma mark - GestureRecognizer

// 点击背景隐藏
-(void)keyBoardHidden
{
    [self.chatToolBar endEditing:YES];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)recognizer
{
	if (recognizer.state == UIGestureRecognizerStateBegan && [self.dataSource count] > 0) {
        CGPoint location = [recognizer locationInView:self.tableView];
        NSIndexPath * indexPath = [self.tableView indexPathForRowAtPoint:location];
        id object = [self.dataSource objectAtIndex:indexPath.row];
        if ([object isKindOfClass:[MessageModel class]]) {
            MessageModel *msgModel = (MessageModel *)object;
            if (![msgModel isPreviewGoodsMsg] && ![msgModel isFraudTextMsg] && ![msgModel isFraudLinkMsg]) { //不是预览商品的cell
                EMChatViewCell *cell = (EMChatViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                [cell becomeFirstResponder];
                _longPressIndexPath = indexPath;
                [self showMenuViewController:cell.bubbleView andIndexPath:indexPath messageType:cell.messageModel.type];
            }
        }
    }
}

- (void)didHeadImageClick:(MessageModel *)message
{
    uint64_t userId = 0;
    if(message.isSender) {
        userId = (uint64_t)[[UserInfoHandler sharedInstance].currentUser.uid longLongValue];
    } else {
        userId = (uint64_t)[_conversation.chatter longLongValue];
    }
    
    if (userId == 10000) { //如果是私货客服则不用点击看个人主页
        return;
    }
    
    PersonPageViewController *personController = [[PersonPageViewController alloc] initWithUserId:userId];
    [self.navigationController pushViewController:personController animated:YES];
}

- (void)didSelectContent:(MessageModel *)message
{
    NSDictionary * dict = message.message.ext;
    if(dict && [dict objectForKey:@"tpl"]) {
        NSDictionary * argDict = nil;
        id temp = [dict objectForKey:@"arg"];
        if([temp isKindOfClass:[NSDictionary class]]) {
            argDict = temp;
        } else {
            argDict = [temp toDict];
        }
        if(argDict && [argDict count] > 0) {

            NSString *orderId = [argDict objectForKey:@"oid"];
        
            int msgType = [[dict objectForKey:@"tpl"] intValue];
            if(msgType == EMT_GOODS_PAYED) {
                if([orderId length] > 0) {
                    QMOrderDetailViewController * viewController = [[QMOrderDetailViewController alloc] initWithData:YES orderId:orderId];
                    [self.navigationController pushViewController:viewController animated:YES];
                }
            } else if(msgType == EMT_GOODS_DELIVERED) {
                if([orderId length] > 0) {
                    QMOrderDetailViewController * viewController = [[QMOrderDetailViewController alloc] initWithData:NO orderId:orderId];
                    [self.navigationController pushViewController:viewController animated:YES];
                }
            } else if(msgType == EMT_GOODS_RECEIVED) {
                if([orderId length] > 0) {
                    QMOrderDetailViewController * viewController = [[QMOrderDetailViewController alloc] initWithData:YES orderId:orderId];
                    [self.navigationController pushViewController:viewController animated:YES];
                }
            }
        }
        
    }
}

- (void)didSelectGoodsContent:(MessageModel *)message
{
    NSDictionary * dict = message.message.ext;
    if(dict && [dict objectForKey:@"tpl"]) {
        int msgType = [[dict objectForKey:@"tpl"] intValue];
        if(msgType == EMT_GOODS_SEND) {
            NSDictionary * argDict = nil;
            id temp = [dict objectForKey:@"arg"];
            if([temp isKindOfClass:[NSDictionary class]]) {
                argDict = temp;
            } else {
                argDict = [temp toDict];
            }
            
             if(argDict && [argDict count] > 0) {
                 uint64_t goodsId = 0;
                 id tmp = [argDict objectForKey:@"gid"];
                 if([tmp isKindOfClass:[NSString class]]) {
                     goodsId = (uint64_t)[tmp longLongValue];
                 } else if([tmp isKindOfClass:[NSNumber class]]) {
                     goodsId = [tmp unsignedLongLongValue];
                 }
                 
                 if(goodsId) {
                     GoodsDetailViewController * viewController = [[GoodsDetailViewController alloc] initWithGoodsId:goodsId];
                     [self.navigationController pushViewController:viewController animated:YES];
                 }

             }
        }
    }
}



#pragma mark - UIResponder actions

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo
{
    MessageModel *model = [userInfo objectForKey:KMESSAGEKEY];
    if ([eventName isEqualToString:kRouterEventTextURLTapEventName]) {
        [self chatTextCellUrlPressed:[userInfo objectForKey:@"url"]];
    }
    else if ([eventName isEqualToString:kRouterEventAudioBubbleTapEventName]) {
        [self chatAudioCellBubblePressed:model];
    }
    else if ([eventName isEqualToString:kRouterEventImageBubbleTapEventName]){
        [self chatImageCellBubblePressed:model];
    }
    else if ([eventName isEqualToString:kRouterEventLocationBubbleTapEventName]){
        [self chatLocationCellBubblePressed:model];
    }
    else if([eventName isEqualToString:kResendButtonTapEventName]){
        EMChatViewCell *resendCell = [userInfo objectForKey:kShouldResendCell];
        MessageModel *messageModel = resendCell.messageModel;
        messageModel.status = eMessageDeliveryState_Delivering;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:resendCell];
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                              withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
        id <IChatManager> chatManager = [[EaseMob sharedInstance] chatManager];
        [chatManager asyncResendMessage:messageModel.message progress:nil];
    }else if([eventName isEqualToString:kRouterEventChatCellVideoTapEventName]){
        [self chatVideoCellPressed:model];
    } else if([eventName isEqualToString:kRouteGoodsTextTapEventName]) {
        [self didSelectContent:model];
    } else if([eventName isEqualToString:kRouteGoodsSendTapEventName]) {
        [self didSelectGoodsContent:model];
    }
}

//链接被点击
- (void)chatTextCellUrlPressed:(NSURL *)url
{
    NSString *absoluteStr = url.absoluteString;
    //跳转到商品详情
    if ([absoluteStr rangeOfString:@"/?c=html5&a=goods&goods_id="].location != NSNotFound) {
        //拿到goods_id的值
        NSArray *params = [absoluteStr componentsSeparatedByString:@"&"];
        if (params && params.count>0) {
            for (NSString *temp in params) {
                if ([temp hasPrefix:@"goods_id"]) {
                    NSString *goodsId = [[temp componentsSeparatedByString:@"="] lastObject];
                    //跳转
                    GoodsDetailViewController *controller = [[GoodsDetailViewController alloc]initWithGoodsId:(uint16_t)[goodsId longLongValue]];
                    [self.navigationController pushViewController:controller animated:YES];
                    return;
                }
            }
        }
    }
    if (url) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

// 语音的bubble被点击
-(void)chatAudioCellBubblePressed:(MessageModel *)model
{
    id <IEMFileMessageBody> body = [model.message.messageBodies firstObject];
    EMAttachmentDownloadStatus downloadStatus = [body attachmentDownloadStatus];
    if (downloadStatus == EMAttachmentDownloading) {
        [AppUtils showErrorMessage:@"正在下载语音，稍后点击"];
        return;
    }
    else if (downloadStatus == EMAttachmentDownloadFailure)
    {
        [AppUtils showErrorMessage:@"正在下载语音，稍后点击"];
        [[EaseMob sharedInstance].chatManager asyncFetchMessage:model.message progress:nil];
        
        return;
    }
    
    // 播放音频
    if (model.type == eMessageBodyType_Voice) {
        __weak ChatViewController *weakSelf = self;
        BOOL isPrepare = [self.messageReadManager prepareMessageAudioModel:model updateViewCompletion:^(MessageModel *prevAudioModel, MessageModel *currentAudioModel) {
            if (prevAudioModel || currentAudioModel) {
                [weakSelf.tableView reloadData];
            }
        }];
        
        if (isPrepare) {
            _isPlayingAudio = YES;
            __weak ChatViewController *weakSelf = self;
            [[[EaseMob sharedInstance] deviceManager] enableProximitySensor];
            [[EaseMob sharedInstance].chatManager asyncPlayAudio:model.chatVoice completion:^(EMError *error) {
                [weakSelf.messageReadManager stopMessageAudioModel];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.tableView reloadData];
                    
                    weakSelf.isPlayingAudio = NO;
//                    [[[EaseMob sharedInstance] deviceManager] disableProximitySensor];
                });
            } onQueue:nil];
        }
        else{
            _isPlayingAudio = NO;
        }
    }
}

// 位置的bubble被点击
-(void)chatLocationCellBubblePressed:(MessageModel *)model
{
    _isScrollToBottom = NO;
    LocationViewController *locationController = [[LocationViewController alloc] initWithLocation:CLLocationCoordinate2DMake(model.latitude, model.longitude)];
    [self.navigationController pushViewController:locationController animated:YES];
}

- (void)chatVideoCellPressed:(MessageModel *)model{
    __weak ChatViewController *weakSelf = self;
    id <IChatManager> chatManager = [[EaseMob sharedInstance] chatManager];
    [AppUtils showProgressMessage:@"正在获取视频"];
    [chatManager asyncFetchMessage:model.message progress:nil completion:^(EMMessage *aMessage, EMError *error) {
        [AppUtils dismissHUD];
        if (!error) {
            NSString *localPath = aMessage == nil ? model.localPath : [[aMessage.messageBodies firstObject] localPath];
            if (localPath && localPath.length > 0) {
                [weakSelf playVideoWithVideoPath:localPath];
            }
        }else{
            [AppUtils showErrorMessage:@"视频获取失败!"];
        }
    } onQueue:nil];
}

- (void)playVideoWithVideoPath:(NSString *)videoPath
{
    _isScrollToBottom = NO;
    NSURL *videoURL = [NSURL fileURLWithPath:videoPath];
    MPMoviePlayerViewController *moviePlayerController = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
    [moviePlayerController.moviePlayer prepareToPlay];
    moviePlayerController.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
    [self presentMoviePlayerViewControllerAnimated:moviePlayerController];
}

// 图片的bubble被点击
-(void)chatImageCellBubblePressed:(MessageModel *)model
{
    __weak ChatViewController *weakSelf = self;
    id <IChatManager> chatManager = [[EaseMob sharedInstance] chatManager];
    if ([model.messageBody messageBodyType] == eMessageBodyType_Image) {
        EMImageMessageBody *imageBody = (EMImageMessageBody *)model.messageBody;
        if (imageBody.thumbnailDownloadStatus == EMAttachmentDownloadSuccessed) {
//            [AppUtils showProgressMessage:@"正在获取大图"];
//            [chatManager asyncFetchMessage:model.message progress:nil completion:^(EMMessage *aMessage, EMError *error) {
//                [AppUtils dismissHUD];
//                if (!error) {
//                    NSString *localPath = aMessage == nil ? model.localPath : [[aMessage.messageBodies firstObject] localPath];
//                    if (localPath && localPath.length > 0) {
//                        weakSelf.isScrollToBottom = NO;
//                        [weakSelf showImages:@[localPath]];
//                        return ;
//                    }
//                }
//                [AppUtils showErrorMessage:@"大图获取失败!"];
//            } onQueue:nil];
            
            NSString *imagePath = model.isSender? model.localPath : model.remotePath;
            NSInteger index = [_imageURLList indexOfObject:imagePath];
            index = (index == NSNotFound ? 0 : index);
            [weakSelf showImages:_imageURLList selectIndex:index];
        }else{
            //获取缩略图
            [chatManager asyncFetchMessageThumbnail:model.message progress:nil completion:^(EMMessage *aMessage, EMError *error) {
                if (!error) {
                    [weakSelf reloadTableViewDataWithMessage:model.message];
                }else{
                    [AppUtils showErrorMessage:@"缩略图获取失败!"];
                }
                
            } onQueue:nil];
        }
    }else if ([model.messageBody messageBodyType] == eMessageBodyType_Video) {
        //获取缩略图
        EMVideoMessageBody *videoBody = (EMVideoMessageBody *)model.messageBody;
        if (videoBody.thumbnailDownloadStatus != EMAttachmentDownloadSuccessed) {
            [chatManager asyncFetchMessageThumbnail:model.message progress:nil completion:^(EMMessage *aMessage, EMError *error) {
                if (!error) {
                    [weakSelf reloadTableViewDataWithMessage:model.message];
                }else{
                    [AppUtils showErrorMessage:@"缩略图获取失败!"];
                }
            } onQueue:nil];
        }
    }
}

#pragma mark - IChatManagerDelegate

-(void)didSendMessage:(EMMessage *)message error:(EMError *)error;
{
    [self reloadTableViewDataWithMessage:message];
}

- (void)reloadTableViewDataWithMessage:(EMMessage *)message{
    __weak ChatViewController *weakSelf = self;
    dispatch_async(_messageQueue, ^{
        if ([weakSelf.conversation.chatter isEqualToString:message.conversationChatter])
        {
            for (int i = 0; i < weakSelf.dataSource.count; i ++) {
                id object = [weakSelf.dataSource objectAtIndex:i];
                if ([object isKindOfClass:[MessageModel class]]) {
                    EMMessage *currMsg = [weakSelf.dataSource objectAtIndex:i];
                    if ([message.messageId isEqualToString:currMsg.messageId]) {
                        MessageModel *cellModel = [MessageModelManager modelWithMessage:message];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [weakSelf.tableView beginUpdates];
                            [weakSelf.dataSource replaceObjectAtIndex:i withObject:cellModel];
                            [weakSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                            [weakSelf.tableView endUpdates];
                        });
                        break;
                    }
                }
            }
        }
    });
}

- (void)didMessageAttachmentsStatusChanged:(EMMessage *)message error:(EMError *)error{
    if (!error) {
        id<IEMFileMessageBody>fileBody = (id<IEMFileMessageBody>)[message.messageBodies firstObject];
        if ([fileBody messageBodyType] == eMessageBodyType_Image) {
            EMImageMessageBody *imageBody = (EMImageMessageBody *)fileBody;
            if ([imageBody thumbnailDownloadStatus] == EMAttachmentDownloadSuccessed)
            {
                [self reloadTableViewDataWithMessage:message];
            }
        }else if([fileBody messageBodyType] == eMessageBodyType_Video){
            EMVideoMessageBody *videoBody = (EMVideoMessageBody *)fileBody;
            if ([videoBody thumbnailDownloadStatus] == EMAttachmentDownloadSuccessed)
            {
                [self reloadTableViewDataWithMessage:message];
            }
        }else if([fileBody messageBodyType] == eMessageBodyType_Voice){
            if ([fileBody attachmentDownloadStatus] == EMAttachmentDownloadSuccessed)
            {
                [self reloadTableViewDataWithMessage:message];
            }
        }
        
    }else{
        
    }
}

- (void)didFetchingMessageAttachments:(EMMessage *)message progress:(float)progress{
    NSLog(@"didFetchingMessageAttachment: %f", progress);
}

-(void)didReceiveMessage:(EMMessage *)message
{
    if ([_conversation.chatter isEqualToString:message.conversationChatter]) {
        [self addChatDataToMessage:message];
    }
}

- (void)group:(EMGroup *)group didLeave:(EMGroupLeaveReason)reason error:(EMError *)error
{
    if (_isChatGroup && [group.groupId isEqualToString:_chatGroup.groupId]) {
        [self.navigationController popToViewController:self animated:NO];
        [self.navigationController popViewControllerAnimated:NO];
    }
}

- (void)didInterruptionRecordAudio
{
    [_chatToolBar cancelTouchRecord];
    
    // 设置当前conversation的所有message为已读
    [_conversation markAllMessagesAsRead:YES];
    
    [self stopAudioPlaying];
}

#pragma mark - EMChatBarMoreViewDelegate

- (void)moreViewPhotoAction:(DXChatBarMoreView *)moreView
{
    // 隐藏键盘
    [self keyBoardHidden];
    
    // 弹出照片选择
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
    [self presentViewController:self.imagePicker animated:YES completion:NULL];
}

- (void)moreViewTakePicAction:(DXChatBarMoreView *)moreView
{
    [self keyBoardHidden];
    
#if TARGET_IPHONE_SIMULATOR
    [AppUtils showAlertMessage:@"模拟器不支持拍照"];
#elif TARGET_OS_IPHONE
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
    [self presentViewController:self.imagePicker animated:YES completion:NULL];
#endif
}

- (void)moreViewLocationAction:(DXChatBarMoreView *)moreView
{
    // 隐藏键盘
    [self keyBoardHidden];
    
    LocationViewController *locationController = [[LocationViewController alloc] initWithNibName:nil bundle:nil];
    locationController.delegate = self;
    [self.navigationController pushViewController:locationController animated:YES];
}

- (void)moreViewVideoAction:(DXChatBarMoreView *)moreView{
    [self keyBoardHidden];
    
#if TARGET_IPHONE_SIMULATOR
    [self showHint:@"模拟器不支持录像"];
#elif TARGET_OS_IPHONE
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.imagePicker.mediaTypes = @[(NSString *)kUTTypeMovie];
    [self presentViewController:self.imagePicker animated:YES completion:NULL];
#endif
}

#pragma mark - LocationViewDelegate

-(void)sendLocationLatitude:(double)latitude longitude:(double)longitude andAddress:(NSString *)address
{
    EMMessage *locationMessage = [ChatSendHelper sendLocationLatitude:latitude longitude:longitude address:address toUsername:_conversation.chatter isChatGroup:_isChatGroup requireEncryption:NO];
    [self addChatDataToMessage:locationMessage];
}

#pragma mark - DXMessageToolBarDelegate
- (void)inputTextViewWillBeginEditing:(XHMessageTextView *)messageInputTextView{
    [_menuController setMenuItems:nil];
}

- (void)didChangeFrameToHeight:(CGFloat)toHeight
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rect = self.tableView.frame;
        rect.origin.y = 0;
        rect.size.height = self.view.frame.size.height - toHeight;
        self.tableView.frame = rect;
    }];
    [self scrollViewToBottom:YES];
}

- (void)didSendText:(NSString *)text
{
    if (text && text.length > 0) {
        [self sendTextMessage:text];
        
//        NSDictionary *params = @{@"ammount":@(100)};
//        [MTA trackCustomKeyValueEvent:@"1_test_event" props:params];
    }
}

/**
 *  按下录音按钮开始录音
 */
- (void)didStartRecordingVoiceAction:(UIView *)recordView
{
//    if (_isRecording) {
//        ++_recordingCount;
//        if (_recordingCount > 10)
//        {
//            _recordingCount = 0;
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"亲，已经戳漏了，随时崩溃给你看" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//            [alertView show];
//        }
//        else if (_recordingCount > 5) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"亲，手别抖了，快被戳漏了" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//            [alertView show];
//        }
//        return;
//    }
//    _isRecording = YES;
    
    DXRecordView *tmpView = (DXRecordView *)recordView;
    CGRect frame = tmpView.frame;
    frame.origin.y = self.view.bounds.origin.y + (self.view.bounds.size.height - tmpView.bounds.size.height - _chatToolBar.bounds.size.height) / 2;
    tmpView.frame = frame;
    [self.view addSubview:tmpView];
    [self.view bringSubviewToFront:recordView];
    
    NSError *error = nil;
    [[EaseMob sharedInstance].chatManager startRecordingAudioWithError:&error];
    if (error) {
        NSLog(@"开始录音失败");
    }
}

/**
 *  手指向上滑动取消录音
 */
- (void)didCancelRecordingVoiceAction:(UIView *)recordView
{
    [[EaseMob sharedInstance].chatManager asyncCancelRecordingAudioWithCompletion:nil onQueue:nil];
}

/**
 *  松开手指完成录音
 */
- (void)didFinishRecoingVoiceAction:(UIView *)recordView
{
    [[EaseMob sharedInstance].chatManager
     asyncStopRecordingAudioWithCompletion:^(EMChatVoice *aChatVoice, NSError *error){
         if (!error) {
             [self sendAudioMessage:aChatVoice];
         }else{
             if (error.code == EMErrorAudioRecordNotStarted) {
                 [AppUtils showErrorMessage:error.domain];

             } else {
                 [AppUtils showErrorMessage:error.domain];
             }
         }
         
     } onQueue:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) {
        NSURL *videoURL = info[UIImagePickerControllerMediaURL];
        [picker dismissViewControllerAnimated:NO completion:^{
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
            CGRect frame = appDelegate.viewController.view.frame;
            frame.size.height = kMainFrameHeight;
            appDelegate.viewController.view.frame = frame;
        }];
        
        // video url:
        // file:///private/var/mobile/Applications/B3CDD0B2-2F19-432B-9CFA-158700F4DE8F/tmp/capture-T0x16e39100.tmp.9R8weF/capturedvideo.mp4
        // we will convert it to mp4 format
        NSURL *mp4 = [self convert2Mp4:videoURL];
        NSFileManager *fileman = [NSFileManager defaultManager];
        if ([fileman fileExistsAtPath:videoURL.path]) {
            NSError *error = nil;
            [fileman removeItemAtURL:videoURL error:&error];
            if (error) {
                NSLog(@"failed to remove file, error:%@.", error);
            }
        }
        EMChatVideo *chatVideo = [[EMChatVideo alloc] initWithFile:[mp4 relativePath] displayName:@"video.mp4"];
        [self sendVideoMessage:chatVideo];
        
    }else{
        UIImage *orgImage = info[UIImagePickerControllerOriginalImage];
        [picker dismissViewControllerAnimated:YES completion:nil];
        [self sendImageMessage:orgImage];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:NO completion:^{
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        CGRect frame = appDelegate.viewController.view.frame;
        frame.size.height = kMainFrameHeight;
        appDelegate.viewController.view.frame = frame;
    }];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController1 animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    viewController1.navigationItem.title = [AppUtils localizedCommonString:@"QM_Text_ChoosePhoto"];
    if ([navigationController.viewControllers count]>1) {
        if (viewController1.navigationItem.leftBarButtonItems == nil) {
            viewController1.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(back:)];
        }
    }
    else{
        viewController1.navigationItem.leftBarButtonItems = nil;
    }
    viewController1.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(cancel:) title:[AppUtils localizedCommonString:@"QM_Button_Cancel"] size:CGSizeMake(44.f, 44.f) imageName:nil];
}

- (void)cancel:(id)sender
{
    [self.imagePicker dismissViewControllerAnimated:YES completion:nil];
}

- (void)back:(id)sender
{
    [self.imagePicker popViewControllerAnimated:YES];
}

#pragma mark - MenuItem actions

- (void)copyMenuAction:(id)sender
{
    // todo by du. 复制
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    if (_longPressIndexPath.row > 0) {
        MessageModel *model = [self.dataSource objectAtIndex:_longPressIndexPath.row];
        pasteboard.string = model.content;
    }
    
    _longPressIndexPath = nil;
}

- (void)deleteMenuAction:(id)sender
{
    if (_longPressIndexPath && _longPressIndexPath.row > 0) {
        MessageModel *model = [self.dataSource objectAtIndex:_longPressIndexPath.row];
        //如果是图片的信息则删除掉
        if ([model.messageBody messageBodyType] == eMessageBodyType_Image) {
            EMImageMessageBody *imageMessageBody = (EMImageMessageBody *)model.messageBody;
            if (model.isSender) {
                [_imageURLList removeObject:imageMessageBody.localPath];
            }
            else{
                [_imageURLList removeObject:imageMessageBody.remotePath];
            }
        }
        //删除掉
        NSMutableArray *messages = [NSMutableArray arrayWithObjects:model, nil];
        [_conversation removeMessage:model.message];
        NSMutableArray *indexPaths = [NSMutableArray arrayWithObjects:_longPressIndexPath, nil];
        if (_longPressIndexPath.row - 1 >= 0) {
            id nextMessage = nil;
            id prevMessage = [self.dataSource objectAtIndex:(_longPressIndexPath.row - 1)];
            if (_longPressIndexPath.row + 1 < [self.dataSource count]) {
                nextMessage = [self.dataSource objectAtIndex:(_longPressIndexPath.row + 1)];
            }
            if ((!nextMessage || [nextMessage isKindOfClass:[NSString class]]) && [prevMessage isKindOfClass:[NSString class]]) {
                [messages addObject:prevMessage];
                [indexPaths addObject:[NSIndexPath indexPathForRow:(_longPressIndexPath.row - 1) inSection:0]];
            }
        }
        [self.dataSource removeObjectsInArray:messages];
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    }
    
    _longPressIndexPath = nil;
}

#pragma mark - private

- (BOOL)canRecord
{
    __block BOOL bCanRecord = YES;
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending)
    {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        if ([audioSession respondsToSelector:@selector(requestRecordPermission:)]) {
            [audioSession performSelector:@selector(requestRecordPermission:) withObject:^(BOOL granted) {
                if (granted) {
                    bCanRecord = YES;
                } else {
                    bCanRecord = NO;
                }
            }];
        }
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    
    return bCanRecord;
}

- (void)stopAudioPlaying
{
    //停止音频播放及播放动画
    [[EaseMob sharedInstance].chatManager stopPlayingAudio];
    MessageModel *playingModel = [self.messageReadManager stopMessageAudioModel];
    
    NSIndexPath *indexPath = nil;
    if (playingModel) {
        indexPath = [NSIndexPath indexPathForRow:[self.dataSource indexOfObject:playingModel] inSection:0];
    }
    
    if (indexPath) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView beginUpdates];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            [self.tableView endUpdates];
        });
    }
}


- (void)loadMoreMessages
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(_messageQueue, ^{
        
        NSInteger currentCount = [weakSelf.dataSource count];
        EMMessage *latestMessage = [weakSelf.conversation latestMessage];
        NSTimeInterval beforeTime = 0;
        if (latestMessage) {
            beforeTime = latestMessage.timestamp + 1;
        }else{
            beforeTime = ([[NSDate date] timeIntervalSince1970]*1000) + 1;
        }

        NSMutableArray *chats = [[weakSelf.conversation loadNumbersOfMessages:(currentCount + KPageCount) before:beforeTime] mutableCopy];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableView stopRefreshAnimation];
        });
        
        NSString *selfId = [UserInfoHandler sharedInstance].currentUser.uid;
        if ([chats count] > currentCount) {
            NSMutableArray *arrInsert=[[NSMutableArray alloc] init];
            EMMessage *lastMessage=nil;
            
            for (int i=0; i<[chats count]; i++) {
                EMMessage *message1=[chats objectAtIndex:i];
                MessageModel *model = [MessageModelManager modelWithMessage:message1];
                if (model.content && ([self checkIsFraudMsg:model.content] || [self checkIsLinkFraudMsg:model.content])) {
                    if (((!lastMessage) || (lastMessage && (message1.timestamp-lastMessage.timestamp)>600000)) && (![message1.from isEqualToString:selfId]) && (![message1.from  isEqualToString:@"10000"])) {
                        NSDate *createDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:(NSTimeInterval)message1.timestamp];
                        self.lastFraudMessageDate=createDate;
                        NSDictionary *dict;
                        if ([self checkIsFraudMsg:model.content]) {
                            dict=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:i+1],@"pos" ,@"1",@"type",nil];
                        }else{
                            dict=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:i+1],@"pos" ,@"2",@"type",nil];
                        }
                        [arrInsert addObject:dict];
                    }
                    lastMessage=message1;
                }
            }
            
            if ([arrInsert count]>0) {
                int count=0;
                for (NSDictionary *dict in arrInsert) {
                    NSInteger pos=[[dict objectForKey:@"pos"] integerValue];
                    NSInteger type=[[dict objectForKey:@"type"] integerValue];
                    id obj=[chats objectAtIndex:pos-1+count];
                    if (obj && ![obj isKindOfClass:[NSString class]]) {
                        EMMessage *temp=(EMMessage *)obj;
                        EMMessage *ret;
                        if (type==1) {
                            ret=[self createFraudMessageWithType:EMT_RECEIVE_FRAUD_TEXT message:temp];
                        }else{
                            ret=[self createFraudMessageWithType:EMT_RECEIVE_FRAUD_LINK message:temp];
                        }
                        [chats insertObject:ret atIndex:pos+count];
                        count++;
                    }
                }
            }
            
            weakSelf.dataSource.array = [weakSelf sortChatSource:chats];

            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.tableView reloadData];
                [weakSelf.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[weakSelf.dataSource count] - currentCount - 1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
            });
        }
    });
}

//创建一条防诈骗信息
-(EMMessage *)createFraudMessageWithType:(MsgType)type message:(EMMessage *)msg{
    NSString *selfId = [UserInfoHandler sharedInstance].currentUser.uid;
    
    EMChatText *chatText = [[EMChatText alloc] initWithText:@""];
    EMTextMessageBody *textBody = [[EMTextMessageBody alloc] initWithChatObject:chatText];
    EMMessage *message = [[EMMessage alloc] initWithReceiver:selfId    bodies:@[textBody]];
    [message setFrom:selfId];
    [message setIsGroup:NO];
    [message setIsReadAcked:YES];
    [message setTo:selfId];
    [message setTimestamp:msg.timestamp+1];
    
    UInt64 t = (UInt64)([[NSDate date] timeIntervalSince1970]*1000);
    NSString *messageID = [NSString stringWithFormat:@"%llu", t];
    [message setMessageId:messageID];
    
    //扩展消息
    NSDictionary * dict;
    if (type==EMT_RECEIVE_FRAUD_TEXT) {//关键字诈骗信息
        dict= @{@"tpl":@(EMT_RECEIVE_FRAUD_TEXT), @"arg":@"系统提醒：请警惕任何要求你输入账号密码的行为，务必确认官方身份与链接。"};
    }else{
        dict= @{@"tpl":@(EMT_RECEIVE_FRAUD_LINK), @"arg":@"系统提醒：请警惕任何要求你输入账号密码的行为，务必确认官方身份与链接。"};
    }

    [message setExt:dict];
    
    return message;
}

- (NSArray *)sortChatSource:(NSArray *)array
{
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    if (array && [array count] > 0) {
        
        for (EMMessage *message in array) {
            NSDate *createDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:(NSTimeInterval)message.timestamp];
            NSTimeInterval tempDate = [createDate timeIntervalSinceDate:self.chatTagDate];
            if (tempDate > 60 || tempDate < -60 || (self.chatTagDate == nil)) {
                [resultArray addObject:[createDate formattedTime]];
                self.chatTagDate = createDate;
            }
            MessageModel *model = [MessageModelManager modelWithMessage:message];
            if (model) {
                [resultArray addObject:model];
                if ([model.messageBody messageBodyType] == eMessageBodyType_Image) {
                    EMImageMessageBody *imageMessageBody = (EMImageMessageBody *)model.messageBody;
                    if (model.isSender) {
                        [_imageURLList addObject:imageMessageBody.localPath];
                    }
                    else{
                        [_imageURLList addObject:imageMessageBody.remotePath];
                    }
                }
            }
        }
    }
    
    return resultArray;
}

-(NSMutableArray *)addChatToMessage:(EMMessage *)message
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    NSDate *createDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:(NSTimeInterval)message.timestamp];
    NSTimeInterval tempDate = [createDate timeIntervalSinceDate:self.chatTagDate];
    if (tempDate > 60 || tempDate < -60 || (self.chatTagDate == nil)) {
        [ret addObject:[createDate formattedTime]];
        self.chatTagDate = createDate;
    }
    NSString *selfId = [UserInfoHandler sharedInstance].currentUser.uid;
    MessageModel *model = [MessageModelManager modelWithMessage:message];
    if (model) {
        [ret addObject:model];
        NSTimeInterval temp2=[createDate timeIntervalSinceDate:self.lastFraudMessageDate];
        if ((temp2>600 || temp2 < -600 ||(self.lastFraudMessageDate == nil)) && (![message.from isEqualToString:selfId])&& (![message.from  isEqualToString:@"10000"])) {
            self.lastFraudMessageDate=createDate;
            EMMessage *msg;
            if (model.content && [self checkIsFraudMsg:model.content]) {
                msg=[self createFraudMessageWithType:EMT_RECEIVE_FRAUD_TEXT message:message];
            }else if (model.content && [self checkIsLinkFraudMsg:model.content]){
                msg=[self createFraudMessageWithType:EMT_RECEIVE_FRAUD_LINK message:message];
            }
            if (msg) {
                [ret addObject:[MessageModelManager modelWithMessage:msg]];
            }
        }
        
        if ([model.messageBody messageBodyType] == eMessageBodyType_Image) {
            EMImageMessageBody *imageMessageBody = (EMImageMessageBody *)model.messageBody;
            if (model.isSender) {
                [_imageURLList addObject:imageMessageBody.localPath];
            }
            else{
                [_imageURLList addObject:imageMessageBody.remotePath];
            }
        }
    }
    
    return ret;
}

-(void)addChatDataToMessage:(EMMessage *)message
{
    __weak ChatViewController *weakSelf = self;
    dispatch_async(_messageQueue, ^{
        NSArray *messages = [weakSelf addChatToMessage:message];
        NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < messages.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:weakSelf.dataSource.count+i inSection:0];
//            [indexPaths insertObject:indexPath atIndex:0];
            [indexPaths addObject:indexPath];
        }

        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableView beginUpdates];
            [weakSelf.dataSource addObjectsFromArray:messages];
            [weakSelf.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
            [weakSelf.tableView endUpdates];
            
            //            [weakSelf.tableView reloadData];
            
            [weakSelf.tableView scrollToRowAtIndexPath:[indexPaths lastObject] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        });
    });
}

- (void)scrollViewToBottom:(BOOL)animated
{
    if (self.tableView.contentSize.height > self.tableView.frame.size.height)
    {
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
        [self.tableView setContentOffset:offset animated:YES];
    }
}

- (void)showRoomContact:(id)sender
{
    [self.view endEditing:YES];
//    if (_isChatGroup && _chatGroup) {
//        ChatGroupDetailViewController *detailController = [[ChatGroupDetailViewController alloc] initWithGroup:_chatGroup];
//        detailController.title = _chatGroup.groupSubject;
//        [self.navigationController pushViewController:detailController animated:YES];
//    }
}

//- (void)removeAllMessages:(id)sender
//{
//    if (_dataSource.count == 0) {
//        [self showHint:@"消息已经清空"];
//        return;
//    }
//    
//    if ([sender isKindOfClass:[NSNotification class]]) {
//        NSString *groupId = (NSString *)[(NSNotification *)sender object];
//        if (_isChatGroup && [groupId isEqualToString:_conversation.chatter]) {
//            [_conversation removeAllMessages];
//            [_dataSource removeAllObjects];
//            [_tableView reloadData];
//            [self showHint:@"消息已经清空"];
//        }
//    }
//    else{
//        __weak typeof(self) weakSelf = self;
//        [WCAlertView showAlertWithTitle:@"提示"
//                                message:@"请确认删除"
//                     customizationBlock:^(WCAlertView *alertView) {
//                         
//                     } completionBlock:
//         ^(NSUInteger buttonIndex, WCAlertView *alertView) {
//             if (buttonIndex == 1) {
//                 [weakSelf.conversation removeAllMessages];
//                 [weakSelf.dataSource removeAllObjects];
//                 [weakSelf.tableView reloadData];
//             }
//         } cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//    }
//}

- (void)showMenuViewController:(UIView *)showInView andIndexPath:(NSIndexPath *)indexPath messageType:(MessageBodyType)messageType
{
    if (_menuController == nil) {
        _menuController = [UIMenuController sharedMenuController];
    }
    if (_copyMenuItem == nil) {
        _copyMenuItem = [[UIMenuItem alloc] initWithTitle:@"复制" action:@selector(copyMenuAction:)];
    }
    if (_deleteMenuItem == nil) {
        _deleteMenuItem = [[UIMenuItem alloc] initWithTitle:@"删除" action:@selector(deleteMenuAction:)];
    }
    
    if (messageType == eMessageBodyType_Text) {
        [_menuController setMenuItems:@[_copyMenuItem, _deleteMenuItem]];
    }
    else{
        [_menuController setMenuItems:@[_deleteMenuItem]];
    }
    
    [_menuController setTargetRect:showInView.frame inView:showInView.superview];
    [_menuController setMenuVisible:YES animated:YES];
}

- (void)exitGroup
{
    [self.navigationController popToViewController:self animated:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)applicationDidEnterBackground
{
    [_chatToolBar cancelTouchRecord];
    
    // 设置当前conversation的所有message为已读
    [_conversation markAllMessagesAsRead:YES];
}

- (void)processUserBasicInfoLoadedNtf:(NSNotification *)notification
{
    uint64_t userId = (uint64_t)[_conversation.chatter longLongValue];
    NSNumber * userObj = [notification.userInfo objectForKey:@"userId"];
    if(userObj && [userObj unsignedLongLongValue] == userId) {
        UserBasicInfo * info = [[UserInfoHandler sharedInstance] getBasicUserInfo:userId];
        if(info) {
            self.title = [info showname];
        }
        if(info && [info.icon length] > 0) {
            [_tableView reloadData];
        }
    }
}

#pragma mark - send message

- (void)sendGoodsMessage:(NSString *)extMessage
{
    EMMessage *tempMessage = [[MsgHandle shareMsgHandler] sendGoodsMsg:_conversation.chatter extData:extMessage];
    [self addChatDataToMessage:tempMessage];
}

-(void)sendTextMessage:(NSString *)textMessage
{
//    for (int i = 0; i < 100; i++) {
//        NSString *str = [NSString stringWithFormat:@"%@--%i", _conversation.chatter, i];
//        EMMessage *tempMessage = [ChatSendHelper sendTextMessageWithString:str toUsername:_conversation.chatter isChatGroup:_isChatGroup requireEncryption:NO];
//        [self addChatDataToMessage:tempMessage];
//    }
    
    EMMessage *tempMessage = [ChatSendHelper sendTextMessageWithString:textMessage toUsername:_conversation.chatter isChatGroup:_isChatGroup requireEncryption:NO];
    [self addChatDataToMessage:tempMessage];
}

-(void)sendImageMessage:(UIImage *)imageMessage
{
    EMMessage *tempMessage = [ChatSendHelper sendImageMessageWithImage:imageMessage toUsername:_conversation.chatter isChatGroup:_isChatGroup requireEncryption:NO];
    [self addChatDataToMessage:tempMessage];
}

-(void)sendAudioMessage:(EMChatVoice *)voice
{
    EMMessage *tempMessage = [ChatSendHelper sendVoice:voice toUsername:_conversation.chatter isChatGroup:_isChatGroup requireEncryption:NO];
    [self addChatDataToMessage:tempMessage];
}

-(void)sendVideoMessage:(EMChatVideo *)video
{
    EMMessage *tempMessage = [ChatSendHelper sendVideo:video toUsername:_conversation.chatter isChatGroup:_isChatGroup requireEncryption:NO];
    [self addChatDataToMessage:tempMessage];
}

#pragma mark - EMDeviceManagerProximitySensorDelegate

- (void)proximitySensorChanged:(BOOL)isCloseToUser{
    //如果此时手机靠近面部放在耳朵旁，那么声音将通过听筒输出，并将屏幕变暗（省电啊）
    if (isCloseToUser)//黑屏
    {
        // 使用耳机播放
        [[EaseMob sharedInstance].deviceManager switchAudioOutputDevice:eAudioOutputDevice_earphone];
    } else {
        // 使用扬声器播放
        [[EaseMob sharedInstance].deviceManager switchAudioOutputDevice:eAudioOutputDevice_speaker];
        if (!_isPlayingAudio) {
            [[[EaseMob sharedInstance] deviceManager] disableProximitySensor];
        }
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if([self isKeyBoardUp]) {
        return YES;
    }
    
    return NO;
}

#pragma mark - QMPreviewGoodsMsgDelegate

- (void)didSendToSeller:(NSString *)extJson
{
    if([extJson length] > 0) {
        [self sendGoodsMessage:extJson];
    }
}

#pragma mark - ClickImageCell
- (void)showImages:(NSArray *)urlArray selectIndex:(NSInteger)index
{
    self.navigationController.navigationBarHidden = YES;
    if (!self.imageZoomView) {
        self.imageZoomView = [[KL_ImagesZoomController alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height )imgViewSize:CGSizeZero];
    }
    self.imageZoomView.alpha = 0.0;
    [self.view addSubview:self.imageZoomView];
    ChatViewController __weak *weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        weakSelf.imageZoomView.alpha = 1.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [weakSelf.imageZoomView updateImageDate:urlArray smallImgs:nil selectIndex:index];
        }
    }];
}

#pragma mark - NSNotification Handler
- (void)oneTapHandler:(NSNotification *)notification
{
    self.navigationController.navigationBarHidden = NO;
    ChatViewController __weak *weakSelf = self;
    
    [UIView animateWithDuration:0.2 animations:^{
        weakSelf.imageZoomView.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [weakSelf.imageZoomView removeFromSuperview];
            weakSelf.imageZoomView = nil;
        }
    }];
}

//更改朋友的备注
- (void)processUpdateFriendRemarkNotify:(NSNotification *)notification
{
    FriendInfo *friendInfo = (FriendInfo *)notification.object;
    if (friendInfo.remark.length > 0) {
        self.navigationItem.title = friendInfo.remark;
    }
}

#pragma mark QMChatFraudCellDelegate
-(void)cellDidTapLink{
    QMQAViewController *qa=[[QMQAViewController alloc] initWithType:QMQATypePrivateTrade];
    [self.navigationController pushViewController:qa animated:YES];
}

@end
