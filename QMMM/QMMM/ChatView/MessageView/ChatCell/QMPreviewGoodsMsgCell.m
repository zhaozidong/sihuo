//
//  QMPreviewGoodsMsgCell.m
//  QMMM
//
//  Created by hanlu on 14-11-16.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMPreviewGoodsMsgCell.h"
#import "MessageModel.h"
#import "UIImageView+QMWebCache.h"
#import "NSString+JSONCategories.h"

NSString * const kQMPreviewGoodsMsgCellIdentifier = @"kQMPreviewGoodsMsgCellIdentifier";

@interface QMPreviewGoodsMsgCell () {
    NSString * _extString;
}

@end

@implementation QMPreviewGoodsMsgCell

+(id)cellFroPreviewGoodsMsg
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMPreviewGoodsMsgCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

+(CGFloat)heightForPreviewGoodsMsg
{
    return 112.0f;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _borderView.layer.borderWidth = 1;
    _borderView.layer.borderColor = kBorderColor.CGColor;
    
    _descLabel.backgroundColor = [UIColor clearColor];
    _descLabel.textColor = kDarkTextColor;
    
    _pricesLabel.backgroundColor = [UIColor clearColor];
    _pricesLabel.textColor = kRedColor;
    
    UIImage * image = [UIImage imageNamed:@"gray_border_btn_bkg"];
    UIImage * selectImage = [UIImage imageNamed:@"gray_border_btn_press_bkg"];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(10, 7, 10, 7)];
    selectImage = [selectImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 7, 10, 7)];
    [_sendLinkBtn setTitle:@"发送给卖家" forState:UIControlStateNormal];
    [_sendLinkBtn setTitleColor:kDarkTextColor forState:UIControlStateNormal];
    [_sendLinkBtn addTarget:self action:@selector(sendLinkBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_sendLinkBtn setBackgroundImage:image forState:UIControlStateNormal];
    [_sendLinkBtn setBackgroundImage:selectImage forState:UIControlStateHighlighted];
    
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
//    CGRect frame = _goodsImageView.frame;
//    frame.origin.x = _borderView.bounds.origin.x + LEFT_MARGIN;
//    frame.origin.y = _borderView.bounds.origin.y + TOP_MARGIN;
//    _goodsImageView.frame = frame;
//    _goodsImageView.frame = frame;
//    
//    frame = _descLabel.frame;
//    frame.origin.x = _goodsImageView.frame.origin.x + _goodsImageView.frame.size.width + LEFT_MARGIN;
//    frame.origin.y = _goodsImageView.frame.origin.y;
//    frame.size.width = self.bounds.size.width - (frame.origin.x + LEFT_MARGIN);
//    _descLabel.frame = frame;
//    
//    frame = _pricesLabel.frame;
//    frame.origin.x = _descLabel.frame.origin.x;
//    frame.origin.y = _descLabel.frame.origin.y + _descLabel.frame.size.height + 10;
//    frame.size.width = _descLabel.frame.size.width;
//    _pricesLabel.frame = frame;
//    
//    frame = _sendLinkBtn.frame;
//    frame.origin.x = _descLabel.frame.origin.x;
//    frame.origin.y = _pricesLabel.frame.origin.y + _pricesLabel.frame.size.height + 10;
//    _sendLinkBtn.frame = frame;
}

- (void)updateUI:(MessageModel *)model
{
    NSDictionary * dict = nil;
    id json = [model.message.ext objectForKey:@"arg"];
    if([json isKindOfClass:[NSDictionary class]]) {
        dict = json;
    } else {
        dict = [json toDict];
    }
    
    NSString *temp = [dict objectForKey:@"desc"];
    if([temp length] > 0) {
        _descLabel.text = temp;
    } else {
        _descLabel.text = @"";
    }
    
    CGFloat price = 0.0f;
    id obj = [dict objectForKey:@"price"];
    if([obj isKindOfClass:[NSString class]] && [obj length] > 0) {
        price = [obj floatValue];
    } else if([obj isKindOfClass:[NSNumber class]]) {
        price = [obj floatValue];
    }
    _pricesLabel.text = [NSString stringWithFormat:@"¥%.2f", price];
    
    temp = [dict objectForKey:@"pic"];
    NSString *tempUrl = [AppUtils thumbPath:temp sizeType:QMImageQuarterSize];
    [_goodsImageView qm_setImageWithURL:tempUrl placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_middle"]];
    
    _extString = json;
}

- (void)sendLinkBtnClick:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(didSendToSeller:)]) {
        [_delegate didSendToSeller:_extString];
    }
}

@end
