//
//  QMChatFraudCell.h
//  QMMM
//
//  Created by Derek on 15/3/19.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageModel.h"

static NSString *kQMChatFraudCell=@"fraudcell";

@protocol QMChatFraudCellDelegate <NSObject>

-(void)cellDidTapLink;

@end


@interface QMChatFraudCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblMessage;

@property (strong, nonatomic) id<QMChatFraudCellDelegate> delegate;

- (void)updateWithTextMessageModel:(MessageModel *)model;

- (void)updateWithLinkMessageModel:(MessageModel *)model;


@end
