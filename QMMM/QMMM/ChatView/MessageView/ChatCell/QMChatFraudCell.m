//
//  QMChatFraudCell.m
//  QMMM
//
//  Created by Derek on 15/3/19.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMChatFraudCell.h"


@implementation QMChatFraudCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[[[NSBundle mainBundle] loadNibNamed:@"QMChatFraudCell" owner:self options:nil] lastObject];
    self.contentView.backgroundColor=[UIColor clearColor];
    self.backgroundColor=[UIColor clearColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    _lblMessage.clipsToBounds=YES;
    _lblMessage.layer.cornerRadius=2.f;
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithTextMessageModel:(MessageModel *)model{
    
//    NSLog(@"content=%@",model.content);
    
    NSDictionary * dict = model.message.ext;
//    NSLog(@"dict2=%@",dict.description);
    _lblMessage.text=[dict objectForKey:@"arg"];
    
}

- (void)updateWithLinkMessageModel:(MessageModel *)model{
    NSDictionary * dict = model.message.ext;
    NSString *detail=[dict objectForKey:@"arg"];
    NSString *fraud=@"点击查看防骗指南";
    NSString *res=[NSString stringWithFormat:@"%@ %@",detail,fraud];
    NSMutableAttributedString *atr = [[NSMutableAttributedString alloc]initWithString:res];
    [atr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, detail.length)];
    [atr addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(detail.length+1, fraud.length)];
    [atr addAttribute:NSLinkAttributeName value:[NSURL URLWithString:@"www.sihuo.com"] range:NSMakeRange(detail.length+1, fraud.length)];
    
    if (_lblMessage) {
        _lblMessage.attributedText = atr;
        _lblMessage.userInteractionEnabled=YES;
        
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLink:)];
        tap.numberOfTapsRequired=1;
        tap.numberOfTouchesRequired=1;
        [_lblMessage addGestureRecognizer:tap];
        
    }
}

-(void)layoutSubviews{
    [super layoutSubviews];
}


-(void)didTapLink:(UITapGestureRecognizer *)gensture{
    if (_delegate && [_delegate respondsToSelector:@selector(cellDidTapLink)]) {
        [_delegate cellDidTapLink];
    }
}

@end
