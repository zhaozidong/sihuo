//
//  QMPreviewGoodsMsgCell.h
//  QMMM
//
//  Created by hanlu on 14-11-16.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class MessageModel;

@protocol QMPreviewGoodsMsgDelegate;

extern NSString * const kQMPreviewGoodsMsgCellIdentifier;

@interface QMPreviewGoodsMsgCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView * goodsImageView;

@property (nonatomic, strong) IBOutlet UILabel * descLabel;

@property (nonatomic, strong) IBOutlet UILabel * pricesLabel;

@property (nonatomic, strong) IBOutlet UIButton * sendLinkBtn;

@property (nonatomic, strong) IBOutlet UIView * borderView;

@property (nonatomic, weak) id<QMPreviewGoodsMsgDelegate> delegate;

+(id)cellFroPreviewGoodsMsg;

+(CGFloat)heightForPreviewGoodsMsg;

- (void)updateUI:(MessageModel *)model;

@end

@protocol QMPreviewGoodsMsgDelegate <NSObject>

- (void)didSendToSeller:(NSString *)extJson;

@end
