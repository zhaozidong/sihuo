/************************************************************
  *  * EaseMob CONFIDENTIAL 
  * __________________ 
  * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved. 
  *  
  * NOTICE: All information contained herein is, and remains 
  * the property of EaseMob Technologies.
  * Dissemination of this information or reproduction of this material 
  * is strictly forbidden unless prior written permission is obtained
  * from EaseMob Technologies.
  */

#import "EMChatTimeCell.h"

@implementation EMChatTimeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.imageViewBkg = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.imageViewBkg.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_imageViewBkg];
    
    _dateLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//    _dateLabel.textColor = [UIColor colorWithRed:126.0/255.0 green:126.0/255.0 blue:126.0/255.0 alpha:1.0];
    [_dateLabel setTextColor:[UIColor whiteColor]];
    _dateLabel.textAlignment = NSTextAlignmentCenter;
    _dateLabel.font = [UIFont systemFontOfSize:10.0];
    _dateLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_dateLabel];
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    _imageViewBkg.center=self.contentView.center;
    _dateLabel.center=self.contentView.center;
}

- (void)setDateText:(NSString *)date
{
    _dateLabel.text = date;
    
    UIImage * image = [UIImage imageNamed:@"time_bkg"];
    
    CGRect contentRect = self.contentView.bounds;
    CGSize textSize = [date sizeWithFont:_dateLabel.font constrainedToSize:contentRect.size];
    
    
    CGRect frame = _imageViewBkg.frame;
    frame.size.width = textSize.width + 20;
    frame.size.height = textSize.height + 10;
    if(frame.size.height < image.size.height) {
        frame.size.height = image.size.height;
    }
    frame.origin.x =  contentRect.origin.x + (contentRect.size.width - frame.size.width) / 2;
    frame.origin.y = contentRect.origin.y + (contentRect.size.height - frame.size.height) / 2;
    _imageViewBkg.frame = frame;
    
    
    frame = _dateLabel.frame;
    frame.origin.x = _imageViewBkg.frame.origin.x + 10;
    frame.origin.y = _imageViewBkg.frame.origin.y + 5;
    frame.size.width = textSize.width;
    frame.size.height = textSize.height;
    _dateLabel.frame = frame;
    
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(12, 13, 11, 13)];
    _imageViewBkg.image = image;
    
    [self setNeedsLayout];
}

@end
