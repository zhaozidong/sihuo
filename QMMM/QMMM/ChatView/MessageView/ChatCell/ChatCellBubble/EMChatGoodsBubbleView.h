//
//  EMChatGoodsBubbleView.h
//  QMMM
//
//  Created by hanlu on 14-11-16.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "EMChatBaseBubbleView.h"

extern NSString *const kRouteGoodsSendTapEventName ;

@interface EMChatGoodsBubbleView : EMChatBaseBubbleView

@property (nonatomic, strong) UIImageView * goodsImageView;

@property (nonatomic, strong) UILabel * descLabel;

@property (nonatomic, strong) UILabel * pricesLabel;

@end
