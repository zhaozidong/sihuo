//
//  EMChatGoodsBubbleView.m
//  QMMM
//
//  Created by hanlu on 14-11-16.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "EMChatGoodsBubbleView.h"
#import "MessageModel.h"
#import "UIImageView+QMWebCache.h"
#import "NSString+JSONCategories.h"

#define LEFT_MARGIN 8
#define TOP_MARGIN 10

NSString *const kRouteGoodsSendTapEventName = @"kRouteGoodsSendTapEventName";

@implementation EMChatGoodsBubbleView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        _goodsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        _goodsImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
        _goodsImageView.backgroundColor = [UIColor redColor];
        [self addSubview:_goodsImageView];
        
        _descLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 15)];
        _descLabel.backgroundColor = [UIColor clearColor];
        _descLabel.textColor = kDarkTextColor;
        _descLabel.font = [UIFont systemFontOfSize:15.0];
        [self addSubview:_descLabel];
        
        _pricesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 18)];
        _pricesLabel.backgroundColor = [UIColor clearColor];
        _pricesLabel.textColor = kRedColor;
        _pricesLabel.font = [UIFont systemFontOfSize:18.0];
        [self addSubview:_pricesLabel];
    }
    
    return self;
}

- (CGSize)sizeThatFits:(CGSize)size
{
    CGSize retSize = {200, 50};
    return CGSizeMake(retSize.width + BUBBLE_VIEW_PADDING * 2 + BUBBLE_ARROW_WIDTH, 2 * BUBBLE_VIEW_PADDING + retSize.height);
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = self.bounds;
    frame.size.width -= BUBBLE_ARROW_WIDTH;
    frame = CGRectInset(frame, BUBBLE_VIEW_PADDING, BUBBLE_VIEW_PADDING);
    if (self.model.isSender) {
        frame.origin.x = BUBBLE_VIEW_PADDING;
    }else{
        frame.origin.x = BUBBLE_VIEW_PADDING + BUBBLE_ARROW_WIDTH;
    }
    frame.origin.y = BUBBLE_VIEW_PADDING;
    
    CGRect rect = _goodsImageView.frame;
    rect.origin.x = frame.origin.x;
    rect.origin.y = frame.origin.y;
    _goodsImageView.frame = rect;
    
    rect = _descLabel.frame;
    rect.origin.x = _goodsImageView.frame.origin.x + _goodsImageView.frame.size.width + LEFT_MARGIN;
    rect.origin.y = _goodsImageView.frame.origin.y;
    rect.size.width = frame.size.width - rect.origin.x;
    _descLabel.frame = rect;
    
    rect = _pricesLabel.frame;
    rect.origin.x = _descLabel.frame.origin.x;
    rect.origin.y = _descLabel.frame.origin.y + _descLabel.frame.size.height + 10;
    rect.size.width = _descLabel.frame.size.width;
    _pricesLabel.frame = rect;
}

- (void)setModel:(MessageModel *)model
{
    [super setModel:model];
    
    NSDictionary * dict = nil;
    NSString * json = [model.message.ext objectForKey:@"arg"];
    if([json isKindOfClass:[NSDictionary class]]) {
        dict = (NSDictionary*)json;
    } else {
        dict = [json toDict];
    }
    
    NSString *temp = [dict objectForKey:@"desc"];
    if([temp length] > 0) {
        _descLabel.text = temp;
    } else {
        _descLabel.text = @"";
    }
    
    CGFloat price = 0.0f;
    id obj = [dict objectForKey:@"price"];
    if([obj isKindOfClass:[NSString class]] && [obj length] > 0) {
        price = [obj floatValue];
    } else if([obj isKindOfClass:[NSNumber class]]) {
        price = [obj floatValue];
    }
    _pricesLabel.text = [NSString stringWithFormat:@"¥%.2f", price];
    
    temp = [dict objectForKey:@"pic"];
    NSString *tempUrl = [AppUtils thumbPath:temp sizeType:QMImageQuarterSize];
    [_goodsImageView qm_setImageWithURL:tempUrl placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_middle"]];
}

-(void)bubbleViewPressed:(id)sender
{
    [self routerEventWithName:kRouteGoodsSendTapEventName userInfo:@{KMESSAGEKEY:self.model}];
}

+(CGFloat)heightForBubbleWithObject:(MessageModel *)object
{
    return 2 * BUBBLE_VIEW_PADDING + 50;
}

@end
