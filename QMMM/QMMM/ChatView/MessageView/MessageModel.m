/************************************************************
  *  * EaseMob CONFIDENTIAL 
  * __________________ 
  * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved. 
  *  
  * NOTICE: All information contained herein is, and remains 
  * the property of EaseMob Technologies.
  * Dissemination of this information or reproduction of this material 
  * is strictly forbidden unless prior written permission is obtained
  * from EaseMob Technologies.
  */

#import "MessageModel.h"
#import "MsgData.h"

@implementation MessageModel

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        //code
    }
    
    return self;
}

- (void)dealloc{
    
}

- (BOOL)isPreviewGoodsMsg
{
    BOOL ret = NO;
    
    NSDictionary * dict = self.message.ext;
    if(dict && [dict objectForKey:@"tpl"]) {
        int msgType = [[dict objectForKey:@"tpl"] intValue];
        ret = (msgType == EMT_CUSTOM_GOODS) ? YES : NO;
    }
    
    return ret;
}

- (BOOL)isGoodsSendMsg
{
    BOOL ret = NO;
    
    NSDictionary * dict = self.message.ext;
    if(dict && [dict objectForKey:@"tpl"]) {
        int msgType = [[dict objectForKey:@"tpl"] intValue];
        ret = (msgType == EMT_GOODS_SEND) ? YES : NO;
    }
    
    return ret;
}

- (BOOL)isFraudTextMsg{
    BOOL ret = NO;
    NSDictionary * dict = self.message.ext;
    if(dict && [dict objectForKey:@"tpl"]) {
        int msgType = [[dict objectForKey:@"tpl"] intValue];
        ret = (msgType == EMT_RECEIVE_FRAUD_TEXT) ? YES : NO;
    }
    return ret;
}

- (BOOL)isFraudLinkMsg{
    BOOL ret = NO;
    NSDictionary * dict = self.message.ext;
    if(dict && [dict objectForKey:@"tpl"]) {
        int msgType = [[dict objectForKey:@"tpl"] intValue];
        ret = (msgType == EMT_RECEIVE_FRAUD_LINK) ? YES : NO;
    }
    return ret;
}

@end
