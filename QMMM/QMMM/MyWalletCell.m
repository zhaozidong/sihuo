//
//  MyWalletCell.m
//  QMMM
//
//  Created by kingnet  on 14-11-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MyWalletCell.h"


NSString *const kMyWalletIdentifier = @"kMyWalletIdentifier";

@interface MyWalletCell ()
@property (weak, nonatomic) IBOutlet UILabel *leftTextLbl;
@property (weak, nonatomic) IBOutlet UIImageView *leftIcon;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;

@end

@implementation MyWalletCell


+ (UINib *)nib
{
    return [UINib nibWithNibName:@"MyWalletCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setLeftText:(NSString *)leftText
{
    self.leftTextLbl.text = leftText;
}

- (void)setIconStr:(NSString *)iconStr
{
    UIImage *image = [UIImage imageNamed:iconStr];
    self.leftIcon.image = image;
}

- (void)setMoney:(float)money
{
    self.moneyLbl.text = [NSString stringWithFormat:@"¥ %.2f", money];
}

+ (CGFloat)cellHeight
{
    return 45.f;
}

@end
