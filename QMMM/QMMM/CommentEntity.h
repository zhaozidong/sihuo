//
//  CommentEntity.h
//  QMMM
//
//  Created by kingnet  on 14-9-19.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseEntity.h"

@interface CommentEntity : BaseEntity

- (id)initWithDictionary:(NSDictionary *)dict;

- (NSString *)description;

//评论id
@property (nonatomic, assign) uint64_t cid;

//商品id
@property (nonatomic, assign) uint64_t gid;

//用户id
@property (nonatomic, assign) uint64_t cUserId;

//回复的评论的id
@property (nonatomic, assign) uint64_t pid;

//父级评论的人的id
@property (nonatomic, assign) uint64_t puid;

//评论内容
@property (nonatomic, copy) NSString *comments;

//评论时间
@property (nonatomic, assign) int comments_ts;

//评论人昵称
@property (nonatomic, copy) NSString *cNickname;

//父级评论人的昵称
@property (nonatomic, copy) NSString *pnickname;

@end
