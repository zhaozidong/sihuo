//
//  Friendhandle.h
//  QMMM
//
//  Created by hanlu on 14-10-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseHandler.h"

@class MsgData;
@class FriendInfo;

@interface Friendhandle : BaseHandler

@property (nonatomic, strong, readonly) NSMutableArray * friendList;

@property (nonatomic, strong, readonly) NSMutableArray * friendSectionList;

+ (Friendhandle *)shareFriendHandle;

+ (void)freeFriendHandle;

//获取好友列表
- (void)getFriendList;
- (void)getLocalFriendList;

//邀请好友
- (void)inviteFriend:(NSString *)friendList isInviteAll:(BOOL)isInviteAll success:(SuccessBlock)success failed:(FailedBlock)failed;

//接受好友
- (void)acceptFriend:(uint64_t)fid token:(NSString *)token context:(contextBlock)context;

//更新好友
- (void)updateFriend:(contextBlock)context friendInfo:(FriendInfo *)friendInfo;

//加载通讯录列表
- (void)loadContact:(contextBlock)context;

//获取本地好友消息列表
- (void)getLocalFriendMsgList:(contextBlock)context;

//获取新好友未读消息数
- (void)getFriendUnreadMsgCount:(contextBlock)context;

//更新好友消息的状态
- (void)updateLocalFriendMsgStatus:(contextBlock)context msgData:(MsgData *)msgData;

//修改好友备注
- (void)modifyFriendRemark:(contextBlock)context friendId:(uint64_t)fid remark:(NSString *)remark;

//更新好友备注
- (void)updateFriendRemark:(FriendInfo *)friendInfo;

//获取可能认识的人
- (void)getProbablyKnowPeople:(contextBlock)context;

//邀请全部好友
- (void)inviteAllFriends:(SuccessBlock)success failed:(FailedBlock)failed;

@end
