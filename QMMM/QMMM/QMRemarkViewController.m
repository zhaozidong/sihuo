//
//  QMRemarkViewController.m
//  QMMM
//
//  Created by hanlu on 14-11-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMRemarkViewController.h"
#import "QMRemarkContactCell.h"
#import "FriendInfo.h"
#import "Friendhandle.h"

@interface QMRemarkViewController ()<UITableViewDataSource, UITableViewDelegate> {
    FriendInfo * _friendInfo;
}

@end

@implementation QMRemarkViewController

- (id)initWithFriendInfo:(FriendInfo *)info
{
    self = [super initWithNibName:@"QMRemarkViewController" bundle:nil];
    if(self) {
        _friendInfo = info;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"备注信息";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backBtnClick:)];
    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(okBtnClick:) title:nil size:CGSizeMake(44, 44) imageName:@"navRight_finish_btn"];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    if(kIOS7) {
        self.tableView.separatorInset = UIEdgeInsetsZero;
    }
    self.tableView.separatorColor = kBorderColor;
    self.tableView.backgroundColor = kBackgroundColor;

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = @"备注信息";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backBtnClick:)];
    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(okBtnClick:) title:nil size:CGSizeMake(44, 44) imageName:@"navRight_finish_btn"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backBtnClick:(id)sender
{
    [AppUtils dismissHUD];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)okBtnClick:(id)sender
{
    QMRemarkContactCell * cell = (QMRemarkContactCell*)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    NSString * remark = cell.textField.text;
    NSString *text = [remark stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (remark == nil || remark.length == 0 || text.length ==0) {
        [AppUtils showAlertMessage:@"备注不能为空"];
        return;
    }
    
    [cell.textField resignFirstResponder];
    
    [AppUtils showProgressMessage:@"正在处理..."];
    
    [[Friendhandle shareFriendHandle] modifyFriendRemark:^(id result) {
        [AppUtils showSuccessMessage:@"修改备注成功"];
        
        //数据源
        [_friendInfo updateRemark:remark];
        [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateFriendRemarkNotify object:_friendInfo];
        
        //update db
        [[Friendhandle shareFriendHandle] updateFriendRemark:_friendInfo];
        
        //back
        [self performSelector:@selector(backBtnClick:) withObject:nil afterDelay:1.0];
    } friendId:_friendInfo.uid remark:remark];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMRemarkContactCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMRemarkContactCellIdentifier];
    if(nil == cell) {
        cell = [QMRemarkContactCell cellForRemarkContact];
    }
    if (_friendInfo.showname.length == 0) {
        [cell.textField becomeFirstResponder];
    }
    else{
        cell.textField.text = _friendInfo.showname;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

@end
