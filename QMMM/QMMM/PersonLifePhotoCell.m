//
//  PersonLifePhotoCell.m
//  QMMM
//
//  Created by kingnet  on 14-11-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PersonLifePhotoCell.h"
#import "UIImageView+QMWebCache.h"

NSString * const kPersonLifePhotoCellIdentifier = @"kPersonLifePhotoCellIdentifier";

@interface PersonLifePhotoCell ()

@property (weak, nonatomic) IBOutlet UIImageView *photoImageV;
@property (weak, nonatomic) IBOutlet UIImageView *addPhotoImageV;

@end

@implementation PersonLifePhotoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle]loadNibNamed:@"PersonLifePhotoCell" owner:self options:0] lastObject];
    if (self) {
        self.frame = frame;
        self.photoImageV.contentMode = UIViewContentModeScaleAspectFill;
    }
    return self;
}

- (void)setFilePath:(NSString *)filePath
{
    _filePath = filePath;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:_filePath]) {
        UIImage *tmpImage = [UIImage imageWithContentsOfFile:_filePath];
        self.photoImageV.image = tmpImage;
    }
    else{
        NSString *smallPath = [AppUtils thumbPath:_filePath sizeType:QMImageHalfSize];
        [self.photoImageV qm_setImageWithURL:smallPath placeholderImage:nil];
    }
}

- (void)setShowAddBtn:(BOOL)showAddBtn
{
    _showAddBtn = showAddBtn;
    self.addPhotoImageV.hidden = !_showAddBtn;
    self.photoImageV.hidden = showAddBtn;
}

@end
