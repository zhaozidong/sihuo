//
//  NSArray+JSONCategories.h
//  SDKDemo
//
//  Created by kingnet  on 14-7-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (JSONCategories)
- (NSString*)toJSON;
@end
