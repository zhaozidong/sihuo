//
//  QMGoodsImageCellV2.h
//  QMMM
//
//  Created by hanlu on 14-10-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QMGoodsBaseTableViewCell.h"

#import "ImageBrowerView.h"

@class StrikeoutLineLabel;
@class QMGoodsAudioView;
@class GoodEntity;
@class QMPagePhotoView;
@class ImageBrowerView;

extern NSString * const kQMGoodsImageCellV2Indentifier;

@interface QMGoodsNewPrecentView : UIView

@property (nonatomic, strong) UIImageView * backgroundView;

@property (nonatomic, strong) UILabel * textLabel;

- (void)setText:(NSString *)text;

@end

@interface QMGoodsImageCellV2 : QMGoodsBaseTableViewCell<ImageBrowerViewDelegate> {
    BOOL _showFull;
    UIImageView *imageSellout;
    BOOL _isPreView;
    NSArray *_arrLocalPath;
    NSString *_localAudioPath;
}

//@property (nonatomic, strong) IBOutlet QMPagePhotoView * pageControlView;
@property (weak, nonatomic) IBOutlet ImageBrowerView *imageBrowerView;

@property (nonatomic, strong) IBOutlet QMGoodsAudioView * audioView;

@property (nonatomic, strong) IBOutlet UILabel * priceLabel;

@property (nonatomic, strong) IBOutlet StrikeoutLineLabel * origPriceLabel;

//@property (nonatomic, strong) IBOutlet QMGoodsNewPrecentView * newsPrecentView;

@property (nonatomic, strong) IBOutlet UILabel * numLabel;

@property (nonatomic, weak) id<ImageBrowerViewDelegate> delegate;

+ (id)cellAwakeFromNib;

- (void)updateUI:(GoodEntity *)goodsInfo;

- (void)setShowFull:(BOOL)showFull;

- (void)updateUI:(GoodEntity *)goodsInfo localImgPath:(NSArray *)arrPath localAudioPath:(NSString *)audioPath;

@end
