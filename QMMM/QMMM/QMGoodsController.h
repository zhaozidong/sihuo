//
//  QMGoodsController.h
//  QMMM
//
//  Created by hanlu on 14-9-18.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@class GoodEntity;

@protocol QMGoodsControllerDelegate;

@interface QMGoodsController : NSObject<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong, readonly) GoodEntity * goodsInfo;

@property (nonatomic, weak) id<QMGoodsControllerDelegate> delegate;

@property (nonatomic, assign) BOOL showFull;

- (id)initWithGoodsInfo:(GoodEntity *)goodsInfo;

- (void)setGoodsInfo:(GoodEntity *)goodsInfo;

@end

@protocol QMGoodsControllerDelegate <NSObject>

@optional

//点击商品图像
- (void)didGoodsImageClick:(QMGoodsController *)controller curIndex:(NSInteger)index;

//点击音频
- (void)didGoodsAudioClick:(QMGoodsController *)controller;

//点击用户头像
- (void)didGoodsHeadImageClick:(QMGoodsController *)controller userId:(uint64_t)userId;

//点击赞
- (void)didGoodsFavorClick:(QMGoodsController *)controller;

//点击评论
- (void)didGoodsCommentClick:(QMGoodsController *)controller;

//点击分享
- (void)didGoodsShareClick:(QMGoodsController *)controller;

@end
