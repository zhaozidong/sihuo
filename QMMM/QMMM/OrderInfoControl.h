//
//  OrderInfoControl.h
//  QMMM
//
//  Created by kingnet  on 14-9-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@class GoodEntity, AddressEntity;

@protocol OrderInfoControlDelegate<NSObject>

- (void)changeGoodsNum;

@end

@interface OrderInfoControl : NSObject

@property (nonatomic, strong) NSArray *cellsArray;
@property (nonatomic, strong) GoodEntity *goodsEntity;
@property (nonatomic, strong) AddressEntity *addressEntity;

@property (nonatomic, assign) int goodsNum;

@property (nonatomic, weak) id<OrderInfoControlDelegate> delegate;

- (float)actualPay;

- (void)setNum:(int)num;

- (void)calculatePrice;

+ (NSInteger)rowCount;

+ (CGFloat)cellHeightWithIndex:(NSIndexPath *)indexPath;

@end
