//
//  QMPullRefreshView.h
//  tongtong
//
//  Created by Liao Feng on 13-10-8.
//  Copyright (c) 2013年 SNDA. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 下拉刷新状态
 */
typedef enum {
    ePullRefreshStateNone = 0,              ///< 无
    ePullRefreshStatePullForRefresh = 1,    ///< 下拉可以刷新
    ePullRefreshStateReleaseToRefresh = 2,  ///< 松开以刷新
    ePullRefreshStateRefreshing = 3,        ///< 正在刷新...
} ePullRefreshState;

@protocol QMPullRefreshViewDelegate;

@interface QMPullRefreshView : UIView

@property (nonatomic, retain) IBOutlet UILabel * textLabel;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * indicatorView;
@property (nonatomic, retain) IBOutlet UIImageView * arrowImageView;

@property (nonatomic, assign) IBOutlet id<QMPullRefreshViewDelegate> delegate;

@property (nonatomic, assign) NSInteger state;

- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;

+ (QMPullRefreshView *)refreshView;

@end


@protocol QMPullRefreshViewDelegate <NSObject>

- (void)didPullRefresh:(QMPullRefreshView *)view;

@end
