//
//  GoodsListDescCell.h
//  QMMM
//
//  Created by Shinancao on 14/12/14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

NSString * const kGoodsListDescCellIdentifier;

@class GoodEntity;
@interface GoodsListDescCell : UITableViewCell

+ (GoodsListDescCell *)goodsListDescCell;

- (void)updateUI:(GoodEntity *)entity;

+ (CGFloat)heightForGoodsListCell:(GoodEntity *)entity;

- (void)maxLine:(NSUInteger)number;

- (void)updateUI:(GoodEntity *)entity isPreview:(BOOL)isPrev;

@end
