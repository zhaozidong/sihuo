//
//  UserProtocolViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-6.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "UserProtocolViewController.h"
#import "BaseHandler.h"
#import "APIConfig.h"
#import "UIWebView+Additions.h"

@interface UserProtocolViewController ()<UIWebViewDelegate>
@property (nonatomic, strong) UIWebView *webView;
@end

@implementation UserProtocolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"用户协议";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.view addSubview:self.webView];
    
    NSString *urlStr = [BaseHandler requestUrlWithPath:API_USER_PROTOCOL];
    
    [self.webView loadRemoteUrl:urlStr];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, kMainTopHeight, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-kMainTopHeight)];
        _webView.delegate = self;
        _webView.multipleTouchEnabled = NO;
        _webView.scalesPageToFit = YES;
    }
    return _webView;
}

#pragma mark - UIWebView Delegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [AppUtils showProgressMessage:@"加载中"];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [AppUtils dismissHUD];
}

@end
