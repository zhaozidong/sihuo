//
//  MineUserInfoCell.m
//  QMMM
//
//  Created by kingnet  on 15-1-19.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "MineUserInfoCell.h"
#import "UserEntity.h"
#import "UserDataInfo.h"
#import "QMRoundHeadView.h"

@interface MineUserInfoCell ()<QMRoundHeadViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet QMRoundHeadView *headView;
@property (weak, nonatomic) IBOutlet UILabel *siihuoNumLbl;

@end

@implementation MineUserInfoCell

- (void)awakeFromNib {
    // Initialization code
    self.nameLbl.backgroundColor = [UIColor clearColor];
    self.nameLbl.textColor = kDarkTextColor;
    self.nameLbl.text = @"";
    self.headView.headViewStyle = QMRoundHeadViewLarge;
    self.headView.delegate = self;
    self.siihuoNumLbl.text = @"";
    self.siihuoNumLbl.textColor = kDarkTextColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (MineUserInfoCell *)userInfoCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"MineUserInfoCell" owner:self options:0];
    return [array lastObject];
}

+ (CGFloat)cellHeight
{
    return 90.f;
}

- (void)updateUI:(UserEntity *)entity
{
    self.nameLbl.text = entity.nickname;
    [self.headView setImageUrl:entity.avatar];
    self.siihuoNumLbl.text = [NSString stringWithFormat:@"私货号：%@", entity.uid];
}


- (void)clickHeadViewWithUid:(uint64_t)userId
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapAvatar)]) {
        [self.delegate didTapAvatar];
    }
}
@end
