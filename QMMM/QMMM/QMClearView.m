//
//  QMClearView.m
//  QMMM
//
//  Created by Shinancao on 14/10/26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMClearView.h"

@implementation QMClearView

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"QMClearView" bundle:nil];
}

@end
