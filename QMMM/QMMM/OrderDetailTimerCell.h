//
//  OrderDetailTimerCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailCellDelegate.h"

@class OrderDataInfo;

extern NSString * const kOrderDetailTimerCellIdentifier;

@interface OrderDetailTimerCell : UITableViewCell

@property (nonatomic, weak) id<OrderDetailCellDelegate> delegate;

+ (UINib *)nib;

- (void)updateUI:(OrderDataInfo *)orderInfo;

+ (CGFloat)cellHeight;

@end
