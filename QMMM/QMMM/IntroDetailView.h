//
//  IntroDetailView.h
//  QMMM
//
//  Created by Derek on 15/1/8.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodEntity.h"
@interface IntroDetailView : UIView



-(id)initWithGoodEntity:(GoodEntity *)entity;

-(id)initWithDict:(NSDictionary *)dictInfo;

@end
