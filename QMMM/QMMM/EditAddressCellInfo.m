//
//  EditAddressCellInfo.m
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "EditAddressCellInfo.h"

@interface EditAddressCellInfo ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *buyerNameTf;
@property (weak, nonatomic) IBOutlet UITextField *buyerPhoneTf;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UITextField *detailAddressTf;

@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
@end
@implementation EditAddressCellInfo
@synthesize buyerName = _buyerName;
@synthesize buyerPhone = _buyerPhone;
@synthesize address = _address;
@synthesize detailAddress = _detailAddress;
@synthesize isDefault = _isDefault;

- (id)init
{
    self = [super init];
    if (self) {
        self.cellsArray = [[NSBundle mainBundle]loadNibNamed:@"EditAddressCells" owner:self options:0];
        self.addressLbl.text = @"";
        self.addressLbl.backgroundColor = [UIColor clearColor];
        self.buyerNameTf.delegate = self;
        self.buyerPhoneTf.delegate = self;
        self.detailAddressTf.delegate = self;
        self.buyerNameTf.attributedPlaceholder = [AppUtils placeholderAttri:@"收货人姓名"];
        self.buyerPhoneTf.attributedPlaceholder = [AppUtils placeholderAttri:@"手机号码"];
        self.detailAddressTf.attributedPlaceholder = [AppUtils placeholderAttri:@"详细地址"];
        self.detailAddressTf.textColor = kDarkTextColor;
        self.buyerPhoneTf.textColor = kDarkTextColor;
        self.buyerNameTf.textColor = kDarkTextColor;
    }
    return self;
}

+ (CGFloat)cellHeight
{
    return 50.f;
}

#pragma mark - Setter
- (void)setBuyerName:(NSString *)buyerName
{
    _buyerName = buyerName;
    self.buyerNameTf.text = _buyerName;
}

- (void)setBuyerPhone:(NSString *)buyerPhone
{
    _buyerPhone = buyerPhone;
    self.buyerPhoneTf.text = _buyerPhone;
}

- (void)setAddress:(NSString *)address
{
    _address = address;
    self.addressLbl.text = _address;
}

- (void)setDetailAddress:(NSString *)detailAddress
{
    _detailAddress = detailAddress;
    self.detailAddressTf.text = _detailAddress;
}

- (void)setIsDefault:(BOOL)isDefault
{
    _isDefault = isDefault;
    self.checkBtn.selected = _isDefault;
}

#pragma mark - Getter
- (NSString *)buyerName
{
    if (self.buyerNameTf.text == nil) {
        _buyerName = @"";
    }
    else{
        _buyerName = self.buyerNameTf.text;
    }
    return _buyerName;
}

- (NSString *)buyerPhone
{
    if (self.buyerPhoneTf.text == nil) {
        _buyerPhone = @"";
    }
    else{
        _buyerPhone = self.buyerPhoneTf.text;
    }
    return _buyerPhone;
}

- (NSString *)address
{
    if (self.addressLbl.text == nil) {
        _address = @"";
    }
    else{
        _address = self.addressLbl.text;
    }
    return _address;
}

- (BOOL)isDefault
{
    _isDefault = self.checkBtn.selected;
    return _isDefault;
}

- (NSString *)detailAddress
{
    if (self.detailAddressTf.text == nil) {
        _detailAddress = @"";
    }
    else{
        _detailAddress = self.detailAddressTf.text;
    }
    return _detailAddress;
}
- (IBAction)checkAction:(id)sender {
    self.checkBtn.selected = !self.checkBtn.selected;
}

#pragma mark - UITextFieldd Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.buyerNameTf) {
        self.editingIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    }
    else if (textField == self.buyerPhoneTf){
        self.editingIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    }
    else if (textField == self.detailAddressTf){
        self.editingIndexPath = [NSIndexPath indexPathForRow:1 inSection:1];
    }
}

@end
