//
//  GoodsRecordCell.h
//  QMMM
//
//  Created by kingnet  on 14-11-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SoundBlock)(NSString *audioPath, int audioTime);

@protocol GoodsRecordCellDelegate <NSObject>

- (BOOL)canRecord;

@end

@interface GoodsRecordCell : UITableViewCell

@property (nonatomic, copy) SoundBlock soundBlock;

@property (nonatomic, copy) NSString *audioPath; //更新时

@property (nonatomic, assign) int audioTime; //更新时

@property (nonatomic, weak) id<GoodsRecordCellDelegate> delegate;

+ (GoodsRecordCell *)goodsRecordCell;

+ (CGFloat)cellHeight;

- (void)updateUI:(int)audioTime audioPath:(NSString *)audioPath;

- (void)stopPlaying;

@end
