//
//  GoodsImageTileView.m
//  QMMM
//
//  Created by kingnet  on 14-9-17.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsImageTileView.h"
#import "UIImageView+AFNetworking.h"
#import "MACircleProgressIndicator.h"
#import "NSString+Empty.h"

@interface GoodsImageTileView ()
@property (nonatomic) UIImageView *imageView;
@property (nonatomic, strong) MACircleProgressIndicator *progress; //下载进度条
@end
@implementation GoodsImageTileView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.backgroundColor = [UIColor clearColor];
    
    self.imageView = [[UIImageView alloc]initWithFrame:self.bounds];
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.imageView];
    
    self.progress = [[MACircleProgressIndicator alloc]initWithFrame:CGRectMake(0, 0, 42, 42)];
    self.progress.value = 0.0;
    self.progress.center = self.center;
    self.progress.color = [UIColor whiteColor];
    [self addSubview:self.progress];
    self.progress.hidden = YES;
}

- (void)setImage:(UIImage *)image
{
    self.imageView.image = image;
}

- (void)setImageURL:(NSURL *)imageURL
{
    if (imageURL && ![NSString isEmptyWithSting:imageURL.absoluteString]) {
        self.progress.hidden = NO;
        GoodsImageTileView __weak *weakSelf = self;
        NSURLRequest *request = [NSURLRequest requestWithURL:imageURL];
        [self.imageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            weakSelf.progress.hidden = YES;
            if (image) {
                weakSelf.imageView.image = image;
            }
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            weakSelf.progress.hidden = YES;
            NSLog(@"download image error:%@", error.description);
        } downloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            weakSelf.progress.value = (float) totalBytesRead/totalBytesExpectedToRead;
        }];
    }
}

@end
