//
//  QMBorderView.m
//  QMMM
//
//  Created by kingnet  on 14-9-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMBorderView.h"
#import <QuartzCore/QuartzCore.h>

@implementation QMBorderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.layer.cornerRadius = 0;
    self.layer.borderWidth = 1.f;
    self.layer.borderColor = [UIColorFromRGB(0xcdcdcd) CGColor];
    self.layer.masksToBounds = YES;
    self.backgroundColor = [UIColor whiteColor];
}

@end
