//
//  TopicGoodsForSearchViewController.h
//  QMMM
//  推荐商品列表，走搜索接口
//  Created by kingnet  on 15-3-19.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@interface TopicGoodsForSearchViewController : BaseViewController

- (id)initWithTitle:(NSString *)title requestURL:(NSString *)requestURL;

@end
