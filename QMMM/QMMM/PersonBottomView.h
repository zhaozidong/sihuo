//
//  PersonBottomView.h
//  QMMM
//
//  Created by kingnet  on 15-1-27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PersonBottomViewDelegate <NSObject>

- (void)didTapChat; //点击聊天按钮

- (void)didTapAddFriend; //点击加好友

@end

@class PersonRelation;

@interface PersonBottomView : UIView

@property (nonatomic, weak) id<PersonBottomViewDelegate> delegate;

- (void)updateUI:(PersonRelation *)relation;

+ (PersonBottomView *)bottomView;

+ (CGFloat)viewHeight;

- (void)showWithCompletion:(void(^)(void))completion;

- (void)dismissWithCompletion:(void(^)(void))completion;

@end
