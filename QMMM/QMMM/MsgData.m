//
//  MsgData.m
//  QMMM
//
//  Created by hanlu on 14-11-3.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MsgData.h"
#import "FMResultSet.h"

@implementation MsgData

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        id tmp = [dict objectForKey:@"tpl"];
        if([tmp isKindOfClass:[NSString class]]) {
            _msgType = [tmp intValue];
        } else if([tmp isKindOfClass:[NSNumber class]]) {
            _msgType = [tmp intValue];
        }
        
        tmp = [dict objectForKey:@"arg"];
        if([tmp isKindOfClass:[NSString class]]) {
            _extData = tmp;
        } else {
            _extData = @"";
        }
        
        tmp = [dict objectForKey:@"ts"];
        if([tmp isKindOfClass:[NSString class]]) {
            _msgTime = [tmp intValue];
        } else if([tmp isKindOfClass:[NSNumber class]]) {
            _msgTime = [tmp intValue];
        }
        
        tmp = [dict objectForKey:@"msg"];
        if([tmp isKindOfClass:[NSString class]]) {
            _msgContent = tmp;
        }
        
        tmp = [dict objectForKey:@"pos"];
        if([tmp isKindOfClass:[NSString class]]) {
            _msgPos = [tmp intValue];
        } else if([tmp isKindOfClass:[NSNumber class]]) {
            _msgPos = [tmp intValue];
        }
        
        tmp = [dict objectForKey:@"id"];
        if([tmp isKindOfClass:[NSString class]]) {
            _msgId = tmp;
        } else {
            _msgId = @"";
        }
        
        _unRead = YES;
    }
    return self;
}

- (id)initWithFMResult:(FMResultSet *)resultSet {
    self = [super init];
    if(self && resultSet) {
        self.msgId = [resultSet stringForColumnIndex:0];
        self.msgType = (MsgType)[resultSet intForColumnIndex:1];
        self.msgContent = [resultSet stringForColumnIndex:2];
        self.msgTime = [resultSet intForColumnIndex:3];
        self.extData = [resultSet stringForColumnIndex:4];
        self.msgPos = [resultSet intForColumnIndex:5];
        self.unRead = [resultSet boolForColumnIndex:6];
    }
    return self;
}

@end
