//
//  QMTextField.m
//  QMMM
//
//  Created by kingnet  on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMTextField.h"
#import <QuartzCore/QuartzCore.h>

@implementation QMTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.clipsToBounds = YES;
    self.leftViewMode = UITextFieldViewModeAlways;
    self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.font = [UIFont systemFontOfSize:15.f];
    self.borderStyle = UITextBorderStyleNone;
    self.backgroundColor = [UIColor whiteColor];
    self.textColor = kDarkTextColor;
    
    //加上下面的线
//    CALayer *lineLayer = [CALayer layer];
//    lineLayer.frame = CGRectMake(0, CGRectGetHeight(self.frame)-1.f, CGRectGetWidth(self.frame), 1.f);
//    lineLayer.backgroundColor = [UIColorFromRGB(0xc4b7b8) CGColor];
//    [self.layer addSublayer:lineLayer];
    
    //添加clear button
    UIImage *imgClear = [UIImage imageNamed:@"clearText_btn"];
    UIButton *clearButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [clearButton setImage:imgClear forState:UIControlStateNormal];
    [clearButton addTarget:self action:@selector(clearText:) forControlEvents:UIControlEventTouchUpInside];
    self.rightView = clearButton;
}

- (CGRect)leftViewRectForBounds:(CGRect)bounds
{
    CGRect textRect = [super leftViewRectForBounds:bounds];
    textRect.origin.x = 25;
    return textRect;
}

- (CGRect)rightViewRectForBounds:(CGRect)bounds
{
    CGRect rect = [super rightViewRectForBounds:bounds];
    rect.origin.x = CGRectGetWidth(self.bounds)-rect.size.width-15;
    return rect;
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    CGRect rect = [super placeholderRectForBounds:bounds];
    rect.origin.x = CGRectGetMaxX(self.leftView.frame)+12.f;
    return rect;
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    CGRect rect = [super textRectForBounds:bounds];
    rect.origin.x = CGRectGetMaxX(self.leftView.frame)+12.f;
    return rect;
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    CGRect rect = [super editingRectForBounds:bounds];
    rect.origin.x = CGRectGetMaxX(self.leftView.frame)+12.f;
    if (self.rightView) {
        rect.size.width = rect.size.width - CGRectGetWidth(self.rightView.frame) - CGRectGetWidth(self.leftView.frame);
    }
    return rect;
}

- (BOOL)becomeFirstResponder
{
    BOOL ret = YES;
    ret = [super becomeFirstResponder];
    if (ret && ![self.text isEqualToString:@""]) {
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    else{
        self.rightViewMode = UITextFieldViewModeNever;
    }
    return ret;
}

- (BOOL)resignFirstResponder
{
    BOOL ret = YES ;
    ret = [super resignFirstResponder] ;
    if( ret )
        self.rightViewMode = UITextFieldViewModeNever;
    return ret ;
}
- (void) clearText:(id)sender
{
    self.text = @"";
    self.rightViewMode = UITextFieldViewModeNever;
}

@end
