//
//  TimerButton.m
//  QMMM
//
//  Created by kingnet  on 14-9-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "TimerButton.h"

@interface TimerButton ()
@property (nonatomic, strong) NSTimer *timer;

@end

static int counter = 60; //计时器

@implementation TimerButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    UIImage *bgImg = [[UIImage imageNamed:@"checkCode_btn"] stretchableImageWithLeftCapWidth:2.f topCapHeight:2.f];
    UIImage *disableImg = [[UIImage imageNamed:@"checkCode_btn_disable"] stretchableImageWithLeftCapWidth:2 topCapHeight:2];
    [self setBackgroundImage:bgImg forState:UIControlStateNormal];
    [self setBackgroundImage:disableImg forState:UIControlStateDisabled];
    [self.titleLabel setFont:[UIFont systemFontOfSize:15.f]];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitle:[AppUtils localizedPersonString:@"QM_Text_CheckCode"] forState:UIControlStateNormal];
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.minimumScaleFactor = .1f;
}

- (void)resetTimer
{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    counter = 60;
}

- (void)startTiming
{
    counter = 60;
    self.enabled = NO;
    NSString *title = [NSString stringWithFormat:[AppUtils localizedPersonString:@"QM_Text_ReCheckCode"], counter];
    [self setTitle:title forState:UIControlStateDisabled];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(handleTimer) userInfo:nil repeats:YES];
}

- (void)handleTimer
{
    counter--;
    if (counter <=0) {
        [self endTiming];
    }
    else{
        NSString *title = [NSString stringWithFormat:[AppUtils localizedPersonString:@"QM_Text_ReCheckCode"], counter];
        [self setTitle:title forState:UIControlStateDisabled];
    }
}

- (void)endTiming
{
    [self resetTimer];
    self.enabled = YES;
}

- (void)dealloc
{
    [self resetTimer];
}


@end
