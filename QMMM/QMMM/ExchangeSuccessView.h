//
//  ExchangeSuccessView.h
//  QMMM
//
//  Created by kingnet  on 15-3-26.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ExchangeSuccessViewDelegate <NSObject>

- (void)didTapInvite;

- (void)didTapExchange;

@end
@interface ExchangeSuccessView : UIView

@property (nonatomic, weak) id<ExchangeSuccessViewDelegate> delegate;

@property (nonatomic, assign) float hongbaoMoney; //红包的余额

+ (ExchangeSuccessView *)successView;

+ (CGFloat)viewHeight;

@end
