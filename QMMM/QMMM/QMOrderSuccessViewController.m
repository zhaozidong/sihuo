//
//  QMOrderSuccessViewController.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMOrderSuccessViewController.h"
#import <AlipaySDK/AlipaySDK.h>
#import "MyOrderInfo.h"
#import "QMOrderDetailViewController.h"
#import "QMPayFailViewController.h"
#import "QMPaySuccessViewController.h"

@interface QMOrderSuccessViewController ()<QMPayOrderDelegate>{
    float _sihuoPay;
    float _aliPay;
    NSString *_orderId;
}

@property (weak, nonatomic) IBOutlet UIButton *payButton;
@end

@implementation QMOrderSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title=@"订单已生成";
    self.edgesForExtendedLayout =UIRectEdgeNone;
    
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    float total=_sihuoPay+_aliPay;
    _lblTotal.text=[NSString stringWithFormat:@"￥%.2f",total];
    _lblSiHuoPay.text=[NSString stringWithFormat:@"￥%.2f",_sihuoPay];
    _lblAliPay.text=[NSString stringWithFormat:@"￥%.2f",_aliPay];
    if (_aliPay > 0) {
        [self.payButton setTitle:@"去支付宝" forState:UIControlStateNormal];
    }
    else{
        [self.payButton setTitle:@"确认支付" forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)setOrderId:(NSString *)orderId sihuoPay:(float)sihuoPay andAliPay:(float)aliPay{
    _sihuoPay=sihuoPay;
    _aliPay=aliPay;
    _orderId=orderId;

}

- (IBAction)payForOrder:(id)sender {//去支付
    if (_orderId) {
        MyOrderInfo *orderInfo=[[MyOrderInfo alloc] initWithOrderId:_orderId andRemain:_sihuoPay];
        orderInfo.delegate=self;
        [orderInfo payOrder];
    }else{
        DLog(@"支付参数为空");
    }
}

-(void)paySuccessForOrderId:(NSString *)orderId{
    if (orderId) {
        QMPaySuccessViewController *paySuccess=[[QMPaySuccessViewController alloc] initWithOrderId:orderId];
        [self.navigationController pushViewController:paySuccess animated:YES];
    }else{
        DLog(@"订单id为空");
    }
}

-(void)payFail{
    QMPayFailViewController *payFail=[[QMPayFailViewController alloc] initWithNibName:@"QMPayFailViewController" bundle:nil];
    [payFail setOrderId:_orderId sihuoPay:_sihuoPay andAliPay:_aliPay];
    [self.navigationController pushViewController:payFail animated:YES];
}


- (IBAction)viewOrder:(id)sender {//查看订单
    if (_orderId) {
        QMOrderDetailViewController *orderDetail=[[QMOrderDetailViewController alloc] initWithData:NO orderId:_orderId];
        [self.navigationController pushViewController:orderDetail animated:YES];
    }else{
        DLog(@"订单id为空");
    }
}

- (void)backAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
