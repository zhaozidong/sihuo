//
//  QMSegmentedControl.h
//  QMMM
//
//  Created by kingnet  on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMSegmentedControl : UISegmentedControl

- (id)initWithItems:(NSArray *)items target:(id)target selector:(SEL)selector;

@end
