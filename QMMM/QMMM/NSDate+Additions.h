//
//  NSDate+Additions.h
//  QMMM
//
//  Created by hanlu on 14-9-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate(Addition)

+ (NSInteger)secondsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;

@end
