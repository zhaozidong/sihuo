//
//  QMMsgV2ViewController.m
//  QMMM
//
//  Created by hanlu on 14-11-10.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMMsgV2ViewController.h"
#import "UserInfoHandler.h"
#import "QMContactCell.h"
#import "QMChatListCell.h"
#import "ChatViewController.h"
#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import "QMFavorViewController.h"
#import "QMCommentViewController.h"
#import "MsgHandle.h"
#import "AppDelegate.h"
#import "UIScrollView+UzysCircularProgressPullToRefresh.h"

@interface QMMsgV2ViewController ()<UITableViewDataSource, UITableViewDelegate, IChatManagerDelegate> {
    NSMutableArray      *_dataSource;
    BOOL                _bLoadedBasicUserInfo;
}

@end

@implementation QMMsgV2ViewController

- (void)initTableView
{
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if(kIOS7) {
        _tableView.separatorInset = UIEdgeInsetsZero;
    }
    _tableView.backgroundColor = kBackgroundColor;
    
    if (self.rdv_tabBarController.tabBar.translucent) {
        UIEdgeInsets insets = UIEdgeInsetsMake(0,
                                               0,
                                               CGRectGetHeight(self.rdv_tabBarController.tabBar.frame),
                                               0);
        
        self.tableView.contentInset = insets;
        self.tableView.scrollIndicatorInsets = insets;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.frame = CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight);
    self.tableView.frame = self.view.frame;
    
    self.navigationItem.title = [AppUtils localizedProductString:@"QM_Text_Msg_title"];
    
    [self initTableView];
    
    [self setupRefresh];
    
    if(![[EaseMob sharedInstance].chatManager isLoggedIn]) {
    } else {
        [self setupUnreadMessageCount];
    }

    [[UserInfoHandler sharedInstance] loadUserBasicInfo:nil failed:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processReloadChatUserListNotify:) name:kReloadChatUserListNotify object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processNetworkErrorNotify:) name:kNotNetworkErrorNotity object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processRefreshMsgListNotify:) name:kRefreshMsgListNotify object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processGetMsgListFailedNotify:) name:kGetMsgListFailedNotify object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUnreadMessagesCountChanged:) name:kUnreadMessagesCountChangedNotity object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConversationList:) name:kUpdateConversationListNotify object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedReceiveOfflineMessages:) name:kFinishedReceiveOfflineMessagesNotify object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processModifyRemarkNotify:) name:kUpdateFriendRemarkNotify object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processDidReceiveMessageNotify:) name:kDidReceiveMessageNotify object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.navigationController.viewControllers.count < 2) {
        //是当前界面
        [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
    }
    
    [self refreshDataSource];
    [self loadChatUserList];
    
    [self refreshFavorAndCommentUnreadCount];
}

-(void)viewWillDisappear:(BOOL)animated
{
    if (self.navigationController.viewControllers.count > 1) {
        //是当前界面
        [[self rdv_tabBarController] setTabBarHidden:YES animated:YES];
    }
    
    [super viewWillDisappear:animated];
}

- (void)loadChatUserList
{
    if(!_bLoadedBasicUserInfo && _dataSource) {
        NSMutableArray * reqlist = [[NSMutableArray alloc] init];
        NSArray * array = [NSArray arrayWithArray:_dataSource];
        for (EMConversation * session in array) {
            [reqlist addObject:@((uint64_t)[session.chatter longLongValue])];
        }
        if([reqlist count] > 0) {
            [[UserInfoHandler sharedInstance] getUserBasicInfo:reqlist success:nil failed:nil];
        } else {
            [self performSelector:@selector(reloadChatUserList) withObject:nil afterDelay:0.1];
        }
        
        _bLoadedBasicUserInfo = TRUE;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2 + _dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * retCell = nil;
    
    if(0 == indexPath.section || 1 == indexPath.section) {
        QMContactCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMContactCellIdentifier];
        if(nil == cell) {
            cell = [QMContactCell cellForContact];
        }
        
        if(0 == indexPath.section) {
            cell.contactLabel.text = [AppUtils localizedProductString:@"QM_Text_comment"];
            cell.headImageView.image = [UIImage imageNamed:@"commentNtf_bkg"];
        } else {
            cell.contactLabel.text = [AppUtils localizedProductString:@"QM_Text_favor"];
            cell.headImageView.image = [UIImage imageNamed:@"favorNtf_bkg"];
        }
        
        retCell = cell;
        
    } else {
        QMChatListCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMChatListCellIdentifier];
        if(nil == cell) {
            cell = [QMChatListCell cellForChatList];
        }
        
        EMConversation *conversation = [_dataSource objectAtIndex:indexPath.section - 2];
        [cell UpdateUI:conversation];
        
        retCell = cell;

    }
    return retCell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(0 == indexPath.section) {
        QMCommentViewController * viewController = [[QMCommentViewController alloc] initWithNibName:@"QMCommentViewController" bundle:nil];
        [self.navigationController pushViewController:viewController animated:YES];
    } else if(1 == indexPath.section) {
        QMFavorViewController * viewController = [[QMFavorViewController alloc] initWithNibName:@"QMFavorViewController" bundle:nil];
        [self.navigationController pushViewController:viewController animated:YES];
    } else {
        EMConversation *conversation = [_dataSource objectAtIndex:indexPath.section - 2];
        ChatViewController *chatController = [[ChatViewController alloc] initWithChatter:conversation.chatter];
        [self.navigationController pushViewController:chatController animated:YES];
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section <= 1) {
        return NO;
    } else {
        EMConversation *conversation = [_dataSource objectAtIndex:indexPath.section - 2];
        uint64_t userId = (uint64_t)[conversation.chatter longLongValue];
        if (userId == 10000) {
            return NO;
        }
        else{
            return YES;
        }
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSIndexSet * deleteIndexPath = [NSIndexSet indexSetWithIndex:indexPath.section];
        
        //删除数据库
        EMConversation *converation = [_dataSource objectAtIndex:(indexPath.section - 2)];
        
        [[EaseMob sharedInstance].chatManager removeConversationByChatter:converation.chatter deleteMessages:NO append2Chat:YES];
        
        //删除数据源
        [_dataSource removeObjectAtIndex:(indexPath.section - 2)];
        
        //删除UI
         [self.tableView deleteSections:deleteIndexPath withRowAnimation:UITableViewRowAnimationLeft];
    }
}

#pragma mark - Pull Refresh


- (void)setupRefresh
{
    //设置下拉刷新
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshActionHandler:^{
        [weakSelf headerRereshing];
    }];
}

- (void)headerRereshing
{
    [self refreshDataSource];
    
    _bLoadedBasicUserInfo = NO;
    [self loadChatUserList];
    
    [[MsgHandle shareMsgHandler] getMsgList];
}

- (void)stopRefresh
{
    if ([_tableView showPullToRefresh]) {
        [_tableView stopRefreshAnimation];
    }
}

#pragma mark - 消息变化 通知

//聊天列表变化通知
- (void)didUpdateConversationList:(NSNotification *)notification
{
    [self refreshDataSource];
}

// 未读消息数量变化回调
-(void)didUnreadMessagesCountChanged:(NSNotification *)notification
{
    [self refreshDataSource];
}

////离线消息启动
//- (void)willReceiveOfflineMessages:(NSNotification *)notification{
//    NSLog(@"开始接收离线消息");
//}

//离线消息通知
- (void)didFinishedReceiveOfflineMessages:(NSNotification *)notification{
    NSLog(@"离线消息接收成功");
    [self refreshDataSource];
    
    //获取用户基本信息
    [self checkChatUserList];
}

//收到聊天消息
- (void)processDidReceiveMessageNotify:(NSNotification *)notification
{
    EMMessage *message = (EMMessage *)notification.object;
    //获取用户基本信息
    uint64_t userId = (uint64_t)[message.from longLongValue];
    if(![[UserInfoHandler sharedInstance] getBasicUserInfo:userId]) {
        [[UserInfoHandler sharedInstance] getUserBasicInfo:[NSArray arrayWithObject:@(userId)] success:nil failed:nil];
    }
}

- (void)checkChatUserList
{
    if(_dataSource) {
        NSMutableArray * reqlist = [[NSMutableArray alloc] init];
        NSArray * array = [NSArray arrayWithArray:_dataSource];
        for (EMConversation * session in array) {
            uint64_t userId = (uint64_t)[session.chatter longLongValue];
            if(![[UserInfoHandler sharedInstance] getBasicUserInfo:userId]) {
                [reqlist addObject:@(userId)];
            }
        }
        
        if([reqlist count] > 0) {
            [[UserInfoHandler sharedInstance] getUserBasicInfo:reqlist success:nil failed:nil];
        }
    }
}

-(void)setupUnreadMessageCount
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kFlushMsgUnreadCountNotify object:nil];
}

#pragma mark - public

- (NSMutableArray *)loadDataSource
{
    NSMutableArray *ret = nil;
    NSArray *conversations = [[EaseMob sharedInstance].chatManager conversations];
    NSArray* sorte = [conversations sortedArrayUsingComparator:
                      ^(EMConversation *obj1, EMConversation* obj2){
                          EMMessage *message1 = [obj1 latestMessage];
                          EMMessage *message2 = [obj2 latestMessage];
                          if(message1.timestamp > message2.timestamp) {
                              return(NSComparisonResult)NSOrderedAscending;
                          }else {
                              return(NSComparisonResult)NSOrderedDescending;
                          }
                      }];
    ret = [[NSMutableArray alloc] initWithArray:sorte];
    return ret;
}

-(void)refreshDataSource
{
    _dataSource = [self loadDataSource];
    [_tableView reloadData];
}

-(void)reloadChatUserList
{
    [self stopRefresh];
    
    [_tableView reloadData];
}

#pragma mark - NSNotification

//主动从服务器上请求回来时会调用
- (void)processReloadChatUserListNotify:(NSNotification *)notification
{
    [self reloadChatUserList];
}

- (void)processNetworkErrorNotify:(NSNotification *)notification
{
    [self stopRefresh];
}
//评论和点赞的刷新
- (void)processRefreshMsgListNotify:(NSNotification *)notification
{
    [self refreshFavorAndCommentUnreadCount];
}

//拉取消息失败
- (void)processGetMsgListFailedNotify:(NSNotification *)notification
{
    [self stopRefresh];
    
    NSString *str = (NSString *)notification.object;
    [AppUtils showErrorMessage:str];
}

- (void)processRecieveChatMsgNotify:(NSNotification *)notification
{
    
}

- (void)refreshFavorAndCommentUnreadCount
{
    QMContactCell * favorCell = (QMContactCell*)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    QMContactCell * commentCell = (QMContactCell*)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    __weak QMContactCell * weakFavorCell = favorCell;
    __weak QMContactCell * weakCommentCell = commentCell;
    
    [[MsgHandle shareMsgHandler] getFavorAndCommentUnreadCount:^(id result) {
        if(result && [result isKindOfClass:[NSDictionary class]]) {
            NSDictionary * dict = result;
            
            int favorUnread = [[dict objectForKey:@"favor"] intValue];
            int commentUnread = [[dict objectForKey:@"comment"] intValue];
            
            [weakFavorCell setUnreadCount:favorUnread];
            [weakCommentCell setUnreadCount:commentUnread];
        }
    }];
}

- (void)processModifyRemarkNotify:(NSNotification *)notification
{
    [self headerRereshing];
}

@end
