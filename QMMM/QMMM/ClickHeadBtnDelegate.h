//
//  ClickHeadBtnDelegate.h
//  QMMM
//
//  Created by kingnet  on 14-10-11.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ClickHeadBtnDelegate <NSObject>

- (void)didTapHeadButton:(uint64_t)userId;

@end
