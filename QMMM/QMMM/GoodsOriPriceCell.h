//
//  GoodsOriPriceCell.h
//  QMMM
//
//  Created by kingnet  on 15-3-2.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsOriPriceCell : UITableViewCell

+ (GoodsOriPriceCell *)oriPriceCell;

@property (nonatomic, assign) float price;

@end
