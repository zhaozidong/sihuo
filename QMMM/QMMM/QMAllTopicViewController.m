//
//  QMAllTopicViewController.m
//  QMMM
//
//  Created by Derek on 15/3/18.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMAllTopicViewController.h"
#import "GoodsHandler.h"
#import "ActivityInfo.h"
#import "QMAllTopicCell.h"
#import "ActivityDetailViewController.h"

@interface QMAllTopicViewController ()<UITableViewDataSource,UITableViewDelegate,QMAllTopicDelegate>{
    UITableView *_tableView;
    NSArray *_arrData;
}

@end

@implementation QMAllTopicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"专题";
    
    self.navigationItem.leftBarButtonItems=[AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight) style:UITableViewStyleGrouped];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    
    [_tableView registerClass:[QMAllTopicCell class] forCellReuseIdentifier:@"topiccell"];
    
    
    [self.view addSubview:_tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    __weak typeof(self) weakSelf=self;
    [[GoodsHandler shareGoodsHandler] getAllTopic:^(id obj) {
        if (obj && [obj isKindOfClass:[NSMutableArray class]]) {
            _arrData=obj;
            [weakSelf reloadAllData];
        }
    } failed:^(id obj) {
        NSLog(@"加载所有专题失败！");
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)backAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)reloadAllData{
    [_tableView reloadData];
}


#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_arrData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QMAllTopicCell *cell=[tableView dequeueReusableCellWithIdentifier:@"topiccell"];
    cell.delegate=self;
    BannerEntity *entity=[_arrData objectAtIndex:indexPath.section];
    [cell updateUI:entity];
    return cell;
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kMainFrameWidth*0.43;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1.f;
}



-(void)didTapTopicImg:(BannerEntity *)entity{
    //跳转到指定的专题
    [AppUtils trackCustomEvent:@"click_banner_event" args:nil];
    ActivityDetailViewController *controller = [[ActivityDetailViewController alloc]initWithEntity:entity];
    [self.navigationController pushViewController:controller animated:YES];
}


@end
