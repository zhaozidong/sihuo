//
//  QMFriendRelationViewController.m
//  QMMM
//
//  Created by Derek on 15/1/26.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMFriendRelationViewController.h"
#import "QMFriendsRelationCell.h"
#import "PersonPageViewController.h"

@interface QMFriendRelationViewController ()<UITableViewDataSource,UITableViewDelegate,QMFriendsRelationCellDelgate>{
    NSArray *_arrUserInfo;
}

@end


@implementation QMFriendRelationViewController

-(instancetype)initWithUserInfo:(NSArray *)arrUserInfo{
    self=[super init];
    if (self) {
        _arrUserInfo=arrUserInfo;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"朋友关系";
//    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
//        self.edgesForExtendedLayout=UIRectEdgeNone;
//    }
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight) style:UITableViewStyleGrouped];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kBorderColor;
    [self.view addSubview:_tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark -UITableViewDataSource

- (NSInteger)numberOfSections{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrUserInfo count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QMFriendsRelationCell *cell=[tableView dequeueReusableCellWithIdentifier:kQMFriendsRelationCell];
    if (!cell) {
        cell=[QMFriendsRelationCell cellAwakeFromNIB];
    }
    
    UserDataInfo *info=[_arrUserInfo objectAtIndex:indexPath.row];
    [cell updateUIWithUserInfo:info];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}

#pragma mark -UITableViewDelegate

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserDataInfo *info=[_arrUserInfo objectAtIndex:indexPath.row];
    PersonPageViewController *person=[[PersonPageViewController alloc] initWithUserId:[info.uid longLongValue]];
    [self.navigationController pushViewController:person animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 14.f;
}

#pragma mark QMFriendsRelationCellDelgate

-(void)clickCellWithUserId:(uint64_t)uid{//点击头像
    PersonPageViewController *person=[[PersonPageViewController alloc] initWithUserId:uid];
    [self.navigationController pushViewController:person animated:YES];
}

@end
