//
//  QMModifyPriceViewController.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMModifyPriceViewController.h"
#import "QMHttpClient.h"
#import "BaseHandler.h"
#import "QMDataCenter.h"
#import "GoodsDataHelper.h"

@interface QMModifyPriceViewController (){
        NSString *_orderId;
        float _total;
        blkComplete _complete;
}

@end

@implementation QMModifyPriceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title=@"修改价格";
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout =UIRectEdgeNone;
    }
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(submitAction:) title:nil size:CGSizeMake(44, 44) imageName:@"navRight_finish_btn"];
    
    NSString *strPrice=[NSString stringWithFormat:@"该订单的原价为￥%.2f\n   请输入修改后的总价",_total];
    _lblPrice.text=strPrice;
    _lblPrice.numberOfLines = 2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)backAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)submitAction:(id)sender{//确认按钮
    [AppUtils showProgressMessage:@"正在提交"];
    if (_txtPrice.text && ![_txtPrice.text isEqualToString:@""]) {
        if ([[_txtPrice.text substringFromIndex:1] length]>0) {
            float finalPrice=[[_txtPrice.text substringFromIndex:1] floatValue];
            NSString *strPath=[NSString stringWithFormat:@"/?c=order&a=changeAmount"];
            NSString *URL=[BaseHandler requestUrlWithPath:strPath];
            NSDictionary *params = @{@"id":_orderId, @"amount":@(finalPrice)};
            [[QMHttpClient defaultClient] requestWithPath:URL method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSError *error = nil;
                id result = [self parseData:responseObject error:&error];
                if (error) {
                    [AppUtils showErrorMessage:[error.userInfo objectForKey:kServerErrorTip]];
                    return;
                }
                [AppUtils showSuccessMessage:@"修改成功"];
                if(result){
                    if(_complete){
                        _complete(finalPrice);
                    }
                    
                    //更新数据库
                    GoodsDataHelper *goodsHelp = [[QMDataCenter sharedDataCenter]goodsDataHelper];
                    [goodsHelp asyncUpdateOrderPrice:nil bSeller:YES orderId:_orderId payAmount:finalPrice];
                    //通知其他界面修改payAmount
                    NSDictionary *dict = @{@"orderId":_orderId, @"finalPrice":@(finalPrice)};
                    [[NSNotificationCenter defaultCenter]postNotificationName:kUpdateOrderPriceNotify object:nil userInfo:dict];
                    [self.navigationController popViewControllerAnimated:YES];
                }else{
                    [AppUtils showErrorMessage:@"修改价格失败"];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                DLog(@"修改价格失败");
            }];
        }
    }
}

-(void)setOrderId:(NSString *)orderId totalPay:(float)total completeBlock:(blkComplete)complete{
    _orderId=orderId;
    _total=total;
    _complete=complete;
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [_txtPrice becomeFirstResponder];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [_txtPrice resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string{
    BOOL isHaveDot = NO;
    if ([textField.text rangeOfString:@"."].location != NSNotFound) {
        isHaveDot = YES;
    }
    //如果输入的是小数点
    if ([string isEqualToString:@"."]) {
        //第一位是小数点时不可以
        if (textField.text == nil || [textField.text isEqualToString:@""]) {
            return NO;
        }
        else{
            //如果输入过小数点则不能再输入了
            if (isHaveDot) {
                return NO;
            }
        }
    }
    else{
        if (![textField.text hasPrefix:@"￥"]) {
            textField.text=[NSString stringWithFormat:@"￥%@",string];
            return NO;
        }
    }
    
    return YES;
}

- (id)parseData:(NSData *)data error:(NSError *__autoreleasing *)error
{
    NSError *err = nil;
    
    //    NSDictionary *dataDic = [MPMessagePackReader readData:data error:&err];
    NSString *resString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    DLog(@"restring:%@", resString);
    
    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
    
    if (err) {//解析出错
        if (error) {
            
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey:err.description, kServerErrorTip:[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            *error = [NSError errorWithDomain:err.domain code:err.code userInfo:userInfo];
        }
        return nil;
    }
    else{
        if (![dataDic isKindOfClass:[NSDictionary class]]) {//不是NSDictionary
            if (error) {
                NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"http response not a nsdictionary", kServerErrorTip:[AppUtils localizedCommonString:@"QM_Alert_Server"]};
                *error = [NSError errorWithDomain:kAppErrorDomain code:QMNotDictionaryError userInfo:userInfo];
            }
            return nil;
        }
        else{
            if ([[dataDic objectForKey:@"c"] intValue] == 0) {//
                id result = [dataDic objectForKey:@"d"];
                return result;
            }
            else{//返回错误提示
                if (error) {
                    NSString *tip = [NSString stringWithFormat:@"%@(%@)", [dataDic objectForKey:@"d"], [dataDic objectForKey:@"c"]];
                    NSDictionary *userInfo = @{NSLocalizedDescriptionKey:@"http response c != 0", kServerErrorTip:tip};
                    *error = [NSError errorWithDomain:kAppErrorDomain code:QMCNotZeroError userInfo:userInfo];
                }
                return nil;
            }
        }
    }
}

@end
