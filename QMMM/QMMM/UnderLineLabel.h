//
//  UnderLineLabel.h
//  QMMM
//  带下划线的label
//  Created by kingnet  on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnderLineLabel : UILabel

- (void)addTouchWithTarget:(id)target selector:(SEL)selector;

@end
