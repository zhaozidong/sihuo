//
//  QMGoodsAudioView.h
//  QMMM
//
//  Created by hanlu on 14-9-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QAudioPlayer.h"


@protocol QMGoodsAudioDelegate;

@class GoodEntity;

@interface QMGoodsAudioView : UIView

@property (nonatomic, strong) UILabel * seconds;

@property (nonatomic, strong) UIImageView * audioImageView;

@property (nonatomic, strong) UIImageView * backgroundView;

//@property (nonatomic, weak) id<QMGoodsAudioDelegate> delegate;

- (void)updateUI:(GoodEntity *)goodsInfo;

-(void)updateWithLocalPath:(NSString *)localPath andAudioTime:(NSInteger)audioTime;

- (void)didPlayAudioStop:(NSDictionary *)audioParams;

@end

@protocol QMGoodsAudioDelegate <NSObject>

- (void)didAudioClicked;

@end
