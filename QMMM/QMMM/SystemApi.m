//
//  SystemApi.m
//  QMMM
//
//  Created by kingnet  on 14-10-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "SystemApi.h"
#import "QMHttpClient.h"
#import "APIConfig.h"

@implementation SystemApi

- (void)getAppVersion:(contextBlock)context
{
    __block contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_CHECK_VERSION];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"get new version error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------get new version fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });
}

- (void)submitFeedback:(NSString *)content type:(int)type context:(contextBlock)context
{
    __block contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_SUBMIT_FEEDBACK];
    
    NSDictionary *params = @{@"content":content, @"type":@(type)};
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"submit feedback error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------submit feedback fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });
}


- (void)reportDevInfoWithContext:(contextBlock)context{
    
    __block NSString *path = [[self class]requestUrlWithPath:API_REPORT_DEVINFO];
    __block contextBlock bContext = context;
    
    NSMutableDictionary *params=[[AppUtils systemInfo] mutableCopy];
    [params setObject:@"ios" forKey:@"mainref"];
    [params setObject:kAppChannel forKey:@"subref"];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"report deviceid error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"report deviceid error:%@", error.description);
        }];
    });
}


- (void)reportUseTime:(NSString *)time Context:(contextBlock)context{
    
    __block NSString *path = [[self class]requestUrlWithPath:API_REPORT_USETIME];
    __block contextBlock bContext = context;
    
    NSMutableDictionary *params=[[AppUtils systemInfo] mutableCopy];
    [params setObject:@"ios" forKey:@"mainref"];
    [params setObject:kAppChannel forKey:@"subref"];
    [params setObject:time forKey:@"sec"];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"report use time error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"report use time error:%@", error.description);
        }];
    });

}


- (void)reportCurrentPage:(NSString *)page contex:(contextBlock)context{
    
    __block NSString *path = [[self class]requestUrlWithPath:API_REPORT_REGISTER];
    __block contextBlock bContext = context;
    
    NSMutableDictionary *params=[[AppUtils systemInfo] mutableCopy];
    [params setObject:@"ios" forKey:@"mainref"];
    [params setObject:kAppChannel forKey:@"subref"];
    [params setObject:page forKey:@"step"];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"report current page error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"report current page error:%@", error.description);
        }];
    });
}

@end
