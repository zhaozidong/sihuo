//
//  OrderDetailAddressCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderDetailAddressCell.h"
#import "OrderDataInfo.h"

NSString * const kOrderDetailAddressCellIdentifier = @"kOrderDetailAddressCellIdentifier";

@interface OrderDetailAddressCell ()
@property (weak, nonatomic) IBOutlet UILabel *recieverLbl; //收货人
@property (weak, nonatomic) IBOutlet UILabel *phoneLbl; //手机号
@property (weak, nonatomic) IBOutlet UILabel *addressLbl; //详细地址

@end

@implementation OrderDetailAddressCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"OrderDetailAddressCell" bundle:nil];
}

+ (OrderDetailAddressCell *)addressCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"OrderDetailAddressCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.recieverLbl.textColor = kDarkTextColor;
    self.recieverLbl.text = @"";
    self.phoneLbl.text = @"";
    self.phoneLbl.textColor = kDarkTextColor;
    self.addressLbl.textColor = kDarkTextColor;
    self.addressLbl.text = @"";
    self.addressLbl.numberOfLines = 0;
    self.addressLbl.preferredMaxLayoutWidth = kMainFrameWidth-20;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight
{
    return 60.f;
}

- (void)updateUI:(OrderDataInfo *)orderInfo
{
    self.recieverLbl.text = [NSString stringWithFormat:@"收货人：%@", orderInfo.recipients];
    self.phoneLbl.text = orderInfo.recipients_phone;
    self.addressLbl.text = [NSString stringWithFormat:@"收货地址：%@", orderInfo.recipients_address];
}

@end
