//
//  QMGoodsActionCell.h
//  QMMM
//
//  Created by 韩芦 on 14-9-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QMGoodsBaseTableViewCell.h"

NSString * const kQMGoodsActionCellIndentifier;

@protocol QMGoodsActionDelegate;

@class GoodEntity;

@interface QMGoodsActionCell : QMGoodsBaseTableViewCell

@property (nonatomic, strong) IBOutlet UIButton * favorBtn;

@property (nonatomic, strong) IBOutlet UIButton * commentBtn;

@property (nonatomic, strong) IBOutlet UIButton * shareBtn;

@property (nonatomic, strong) IBOutlet UILabel * lineSeprator;

@property (nonatomic, strong) IBOutlet UILabel * leftLineSeprator;

@property (nonatomic, strong) IBOutlet UILabel * rightLineSeprator;

@property (nonatomic, strong) IBOutlet UIImageView * roundBkgView;

@property (nonatomic, weak) id<QMGoodsActionDelegate> delegate;

+ (id)cellAwakeFromNib;

+ (NSUInteger)heightForGoodsActionCell:(GoodEntity *)goodsInfo;

- (void)updateUI:(GoodEntity *)goodsInfo showFull:(BOOL)showFull;

@end

@protocol QMGoodsActionDelegate <NSObject>

@required

- (void)didFavorBtnClicked;
- (void)didCommentBtnClicked;
- (void)didShareBtnClicked;

@end
