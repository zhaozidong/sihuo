//
//  QMFilterSubViewController.h
//  QMMM
//
//  Created by kingnet  on 15-3-17.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol QMFilterSubViewControllerDelegate <NSObject>

@optional
//选择了table的某一行
- (void)tableViewSelectedAtIndex:(NSInteger)selectedIndex;

@end
@interface QMFilterSubViewController : NSObject

@property (nonatomic, strong, readonly) UITableView *tableView;

@property (nonatomic, weak) id <QMFilterSubViewControllerDelegate> delegate;

//刷新列表
- (void)reloadTableWithData:(NSArray *)data selectedIndex:(NSInteger)selectedIndex;

@end
