//
//  TTIconMenuBar.m
//  AWIconSheet
//
//  Created by hanlu on 14-1-13.
//  Copyright (c) 2014年 Narcissus. All rights reserved.
//

#import "TTIconMenuBar.h"

const int numPerRow = 4;

@implementation IconMenuItemData

- (id)initWithName:(NSString *)aImageName title:(NSString *)aTitleName
{
    self = [super init];
    if(self) {
        _imageName = [[NSString alloc] initWithString:aImageName];
        _titleName = [[NSString alloc] initWithString:aTitleName];
    }
    return self;
}

- (void)dealloc
{
    self.imageName = nil;
    self.titleName = nil;
    //[super dealloc];
}

@end

@implementation TTIconMenuItem

+(CGFloat)heightForMenuItem
{
    return 320.0f/3.0;
}

+(CGFloat)widthForMenuItem
{
    return 320.0f/numPerRow;
}

- (id)init
{
    self = [super initWithFrame:CGRectMake(0, 0, [[self class] widthForMenuItem], [[self class] heightForMenuItem])];
    if(self) {
        [self setTitleColor:kDarkTextColor forState:UIControlStateNormal];
        [self.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    }
    return self;
}

@end


@implementation TTIconMenuBar

@synthesize cancelBtn = _cancelBtn;
@synthesize iconMenuBarDelegate = _iconMenuBarDelegate;

- (id)initWithIconMenuBarDelegate:(id<TTIconMenuBarDelegate>)delgate itemCount:(NSUInteger)count
{
    int row = (int)count / numPerRow;
    int col = count % numPerRow;
    if(col != 0) {
        row ++;
    }
    
    CGFloat height = row * [TTIconMenuItem heightForMenuItem] + 75.0f;
    self = [super initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, height)];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
        UIImageView * imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        UIImage * image = [UIImage imageNamed:@"share_bkg"];
        image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(8, 8, 8, 8)];
        imageView.image = image;
        [self addSubview:imageView];
        
        image = [UIImage imageNamed:@"btn_cancel"];
        UIImage * highImage = [UIImage imageNamed:@"btn_cancel_press"];
        image = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        highImage = [highImage stretchableImageWithLeftCapWidth:highImage.size.width/2 topCapHeight:highImage.size.height/2];
        
        _cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 282, 43)];
        [_cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelBtn setBackgroundImage:image forState:UIControlStateNormal];
        [_cancelBtn setBackgroundImage:highImage forState:UIControlStateHighlighted];
        [_cancelBtn addTarget:self action:@selector(_dismissModalView) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_cancelBtn];
    }
    return self;
}

- (void)show
{
    [self cleanMenuItem];
    
    int meunItemCount = [_iconMenuBarDelegate numberOfItemsInMenuBar];
    for (int i = 0; i < meunItemCount; ++i) {
        TTIconMenuItem * cell = [_iconMenuBarDelegate menuItemForMenuAtIndex:i];
        cell.tag = 100 + i;
        [cell addTarget:self action:@selector(menuItemClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:cell];
        
        int row = i / numPerRow;
        int column = i % numPerRow;
        
        CGRect frame= cell.frame;
//        frame.origin.x = column * [[cell class] widthForMenuItem];
        frame.origin.x  =(kMainFrameWidth-numPerRow*[[cell class] widthForMenuItem])/(numPerRow+1)*(column+1)+column*[[cell class] widthForMenuItem];
        frame.origin.y = row * [[cell class] heightForMenuItem];
        cell.frame = frame;
    }
    
    [self _presentModelView];
}

- (void)dismiss
{
    [self _dismissModalView];
}

- (void)menuItemClick:(TTIconMenuItem *)menuitem
{
    if(menuitem && menuitem.currentTitle) {
        [self dismiss];
        
        if(menuitem && _iconMenuBarDelegate && [_iconMenuBarDelegate respondsToSelector:@selector(didTapOnItemAtIndex:)]) {
            [_iconMenuBarDelegate didTapOnItemAtIndex:menuitem.index];
        }
    }
}

- (void)_presentModelView
{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    if (![keywindow.subviews containsObject:self]) {
        // Calulate all frames
        CGRect sf = self.frame;
        CGRect vf = keywindow.frame;
        CGRect f  = CGRectMake(0, vf.size.height-sf.size.height, vf.size.width, sf.size.height);
        
        // Add semi overlay
        UIView * overlay = [[UIView alloc] initWithFrame:keywindow.bounds];
        overlay.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
        
        UIControl * ss = [[UIControl alloc] initWithFrame:keywindow.bounds];
        [overlay addSubview:ss];
        [keywindow addSubview:overlay];
        [ss addTarget:self action:@selector(didTapOverlay:) forControlEvents:UIControlEventTouchUpInside];

        
        // Begin overlay animation
        [UIView animateWithDuration:0.3 animations:^{
            ss.alpha = 0.5;
        }];
        
        // Present view animated
        self.frame = CGRectMake(0, vf.size.height, vf.size.width, sf.size.height);
        [keywindow addSubview:self];
        self.layer.shadowColor = [[UIColor blackColor] CGColor];
        self.layer.shadowOffset = CGSizeMake(0, -2);
        self.layer.shadowRadius = 5.0;
        self.layer.shadowOpacity = 0.8;
        [UIView animateWithDuration:0.3 animations:^{
            self.frame = f;
        }];
    }
}

- (void)didTapOverlay:(id)sender
{
    [self _dismissModalView];
}

- (void)_dismissModalView
{
    UIWindow * keywindow = [[UIApplication sharedApplication] keyWindow];
    UIView * modal = [keywindow.subviews objectAtIndex:keywindow.subviews.count-1];
    UIView * overlay = [keywindow.subviews objectAtIndex:keywindow.subviews.count-2];
    [UIView animateWithDuration:0.3 animations:^{
        modal.frame = CGRectMake(0, keywindow.frame.size.height, modal.frame.size.width, modal.frame.size.height);
    } completion:^(BOOL finished) {
        [overlay removeFromSuperview];
        [modal removeFromSuperview];
    }];
    
    // Begin overlay animation
    UIImageView * ss = (UIImageView*)[overlay.subviews objectAtIndex:0];
    [UIView animateWithDuration:0.3 animations:^{
        ss.alpha = 1;
    }];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect bounds = self.bounds;
    
    CGRect frame = _cancelBtn.frame;
    frame.origin.y = bounds.size.height - _cancelBtn.frame.size.height - 20;
    frame.origin.x = (bounds.size.width - _cancelBtn.frame.size.width) / 2;
    _cancelBtn.frame = frame;
}

- (void)cleanMenuItem
{
    int meunItemCount = [_iconMenuBarDelegate numberOfItemsInMenuBar];
    for (int i = 0; i < meunItemCount; ++i) {
        UIView * view = [self viewWithTag:100+i];
        if(view) {
            [view removeFromSuperview];
        }
    }
}

- (void)removeFromSuperview
{
    [self cleanMenuItem];
    [super removeFromSuperview];
}

- (void)dealloc
{
    [self cleanMenuItem];
    self.cancelBtn = nil;
    //[super dealloc];
}

@end
