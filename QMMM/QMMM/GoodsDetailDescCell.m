//
//  GoodsDetailDescCell.m
//  QMMM
//
//  Created by Shinancao on 14/12/15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsDetailDescCell.h"
#import "GoodEntity.h"

NSString * const kGoodsDetailDescCellIdentifier = @"kGoodsDetailDescCellIdentifier";
@interface GoodsDetailDescCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imageV;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidthCst;
@end
@implementation GoodsDetailDescCell

+ (GoodsDetailDescCell *)goodsDetailDescCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"GoodsDetailDescCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.descLbl.text = @"";
    self.descLbl.textColor = kDarkTextColor;
    self.descLbl.font = [UIFont systemFontOfSize:14.f];
    self.descLbl.numberOfLines = 0;
    self.descLbl.backgroundColor = [UIColor clearColor];
    self.descLbl.textAlignment = NSTextAlignmentLeft;
    self.descLbl.lineBreakMode = NSLineBreakByWordWrapping;
    self.descLbl.preferredMaxLayoutWidth = kMainFrameWidth - 32;
    self.imageV.image = [[UIImage imageNamed:@"descUsage_bg"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(GoodEntity *)entity
{
    if (entity) {
        NSString *strUsage=[[GoodEntity usageDesc] objectAtIndex:entity.isNew];
        NSString * displayname = [NSString stringWithFormat:@"%@ %@", strUsage, entity.desc];
        
        //属性字符串设置属性
        NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:displayname];
        [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.0] range:NSMakeRange(0, [strUsage length]+1)];
        [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange([strUsage length]+1, [attributeString length]-([strUsage length]+1))];
        [attributeString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x99a0aa) range:NSMakeRange(0, [strUsage length]+1)];
        
        self.descLbl.attributedText = attributeString;
        
        NSMutableAttributedString * attributeString1 = [[NSMutableAttributedString alloc] initWithString:strUsage];
        [attributeString1 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.0] range:NSMakeRange(0, [strUsage length])];
        
        [attributeString1 addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x99a0aa) range:NSMakeRange(0, strUsage.length)];
        //width - index - margin
        CGRect rect = [attributeString1 boundingRectWithSize:CGSizeMake(100, 17) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine context:nil];
        CGFloat width = rect.size.width;
        self.imageViewWidthCst.constant = width+4;
        [self.contentView layoutIfNeeded];
    }
}


@end
