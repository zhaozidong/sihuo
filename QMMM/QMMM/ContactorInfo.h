//
//  ContactorInfo.h
//  QMMM
//
//  Created by hanlu on 14-10-21.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@class FMResultSet;

@interface ContactorInfo : NSObject

@property (nonatomic, strong) NSString * cellPhone;

@property (nonatomic, strong) NSString * name;

- (id)initWithFMResult:(FMResultSet *)resultSet;

@end
