//
//  QMContactHelper.h
//  QMMM
//
//  Created by hanlu on 14-10-21.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@class FMDatabase;
@class ContactorInfo;

@interface QMContactHelper : NSObject {
    NSThread * _workThread;
}

//数据库工作线程
- (void)setWorkThread:(NSThread *)aThread;

- (NSArray *)loadContactList:(FMDatabase *)database;
- (void)addContactList:(FMDatabase *)database list:(NSArray *)userList;
- (void)updateContact:(FMDatabase *)database userInfo:(ContactorInfo *)userInfo;
- (void)deleteContact:(FMDatabase *)database cellPhone:(NSString *)cellPhone;
- (void)deleteAllContact:(FMDatabase *)database;

- (void)asyncLoadContactList:(contextBlock)context;
- (void)asyncAddContactList:(contextBlock)context list:(NSArray *)userList;
- (void)asyncUpdateContact:(contextBlock)context userInfo:(ContactorInfo *)userInfo;
- (void)asyncDeleteContact:(contextBlock)context cellPhone:(NSString *)cellPhone;
- (void)asyncDeleteAllContact:(contextBlock)context;

@end
