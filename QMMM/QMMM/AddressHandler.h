//
//  AddressHandler.h
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseHandler.h"

@class AddressEntity;

@interface AddressHandler : BaseHandler

/**
 查询收货地址
 **/
- (void)getAddressList:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 新增收货地址
 **/
- (void)addNewAddress:(AddressEntity *)entity success:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 更新收货地址
 **/
- (void)updateAddress:(AddressEntity *)entity success:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 删除收货地址
 **/
- (void)deleteAddress:(AddressEntity *)entity success:(SuccessBlock)success failed:(FailedBlock)failed;
@end
