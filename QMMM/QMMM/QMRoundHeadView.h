//
//  QMRoundHeadView.h
//  QMMM
//
//  Created by kingnet  on 14-10-22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    QMRoundHeadViewSmall,
    QMRoundHeadViewLarge
}QMRoundHeadViewStyle;

@protocol QMRoundHeadViewDelegate <NSObject>

- (void)clickHeadViewWithUid:(uint64_t)userId;

@end

@interface QMRoundHeadView : UIView
{
    UIImageView *imageView_;
}

@property (nonatomic, assign) uint64_t userId;

@property (nonatomic, weak) id<QMRoundHeadViewDelegate> delegate;

@property (nonatomic, strong) NSString *imageUrl; //加载大头像

@property (nonatomic, strong) NSString *smallImageUrl; //加载小头像

@property (nonatomic, strong) UIImage *image; //直接赋予image

@property (nonatomic, assign) QMRoundHeadViewStyle headViewStyle;

@property (nonatomic, assign) BOOL clickEnable; //是否可点击，默认是可以的

- (void)setCornerRadius:(float)cornerRadius;

@end
