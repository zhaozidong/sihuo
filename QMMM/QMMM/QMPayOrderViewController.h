//
//  QMPayOrderViewController.h
//  QMMM
//
//  Created by Derek.zhao on 14-12-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMPayOrderViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblRemainPay;
@property (strong, nonatomic) IBOutlet UILabel *lblAliPay;
@property (strong, nonatomic) IBOutlet UILabel *lblTotal;

- (void)setOrderId:(NSString *)orderId sihuoPay:(float)sihuoPay andAliPay:(float)aliPay;

- (void)setOrderId:(NSString *)orderId totalPay:(float)totalPay;

@end
