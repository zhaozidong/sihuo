//
//  SettingsShowInfoCell.m
//  QMMM
//
//  Created by kingnet  on 14-11-5.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "SettingsShowInfoCell.h"
#import "NewIcon.h"

NSString * const kSettingsShowInfoCellIdentifier = @"kSettingsShowInfoCellIdentifier";

@interface SettingsShowInfoCell ()
@property (weak, nonatomic) IBOutlet UILabel *leftTextLbl;
@property (weak, nonatomic) IBOutlet UILabel *rightTextLbl;
@property (weak, nonatomic) IBOutlet NewIcon *redNewIcon;

@end

@implementation SettingsShowInfoCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"SettingsShowInfoCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.leftTextLbl.text = @"";
    self.leftTextLbl.textColor = kDarkTextColor;
    self.rightTextLbl.textColor = UIColorFromRGB(0x9198a3);
    self.rightTextLbl.text = @"";
    self.redNewIcon.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}

+ (NSArray *)items
{
    return @[@"当前版本", @"清除缓存"];
}

+ (CGFloat)cellHeight
{
    return 45.f;
}

- (void)setLeftText:(NSString *)leftText
{
    self.leftTextLbl.text = leftText;
}

- (void)setRightText:(NSString *)rightText
{
    self.rightTextLbl.text = rightText;
}

- (void)setIsShowNewIcon:(BOOL)isShowNewIcon
{
    self.redNewIcon.hidden = !isShowNewIcon;
}

@end
