//
//  ContactHandle.h
//  QMMM
//
//  Created by hanlu on 14-10-21.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactHandle : NSObject

//@property (nonatomic, strong, readonly) NSMutableDictionary * contactDict;

+ (ContactHandle *)shareContactInstance;

- (void)uploadContactList;

@end
