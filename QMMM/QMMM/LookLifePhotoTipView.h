//
//  LookLifePhotoTipView.h
//  QMMM
//
//  Created by kingnet  on 15-1-29.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LookLifePhotoTipView : UIView

+ (LookLifePhotoTipView *)photoTipView;

- (void)showInView:(UIView *)inView;

- (void)dismissWithCompletion:(void(^)(void))completion;

@end
