//
//  PersonFriendsCell.h
//  QMMM
//
//  Created by kingnet  on 14-11-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kPersonFriendsCellIdentifier;

@class UserDataInfo;
@interface PersonFriendsCell : UICollectionViewCell

- (void)updateUI:(UserDataInfo *)userInfo;

@end
