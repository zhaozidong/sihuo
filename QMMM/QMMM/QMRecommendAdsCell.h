//
//  QMRecommendAdsCell.h
//  QMMM
//
//  Created by Derek on 15/1/4.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityInfo.h"
@protocol QMRecommendAdsDelegate <NSObject>

-(void)didAdsClick:(BannerEntity *)entity;

@end







@interface QMRecommendAdsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAds;

@property (weak, nonatomic) id<QMRecommendAdsDelegate> delegate;

@property (strong, nonatomic) NSMutableArray *arrView;

+ (id)cellForGoodsAds;

-(void)updateUiWithArr:(NSArray *)arrInfo;


@end
