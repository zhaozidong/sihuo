//
//  QMFillExpressNumCell2.m
//  QMMM
//
//  Created by Shinancao on 14/11/22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMFillExpressNumCell2.h"

NSString * const kQMFillExpressNumCell2Identifier = @"kQMFillExpressNumCell2Identifier";

@interface QMFillExpressNumCell2 ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end

@implementation QMFillExpressNumCell2

+ (QMFillExpressNumCell2 *)cell2ForFillExpressNum
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"QMFillExpressNumCell2" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.textField.attributedPlaceholder = [AppUtils placeholderAttri:@"请填写快递单号"];
    self.textField.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSString *)expressNum
{
    if (self.textField.text == nil) {
        return @"";
    }
    return self.textField.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
