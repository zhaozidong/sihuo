//
//  QMFriendViewController.h
//  QMMM
//
//  Created by hanlu on 14-10-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMFriendViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView * tableView;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, assign) BOOL slideToInvite; //是否需要定位到邀请好友

@end
