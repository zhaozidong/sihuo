//
//  QMHttpClient.m
//  QMMM
//
//  Created by kingnet  on 14-8-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMHttpClient.h"
#import "Reachability.h"
#import "QMHTTPResponseSerializer.h"
#import "AppUtils.h"
#import "UserDefaultsUtils.h"
#import "APIConfig.h"
#import "VersionInfo.h"

@interface QMHttpClient ()
{
    BOOL isAlertShow_;
}

@property(nonatomic, strong) AFHTTPRequestOperationManager *manager;

@end

@implementation QMHttpClient

- (id)init
{
    self = [super init];
    if (self) {
        self.manager = [AFHTTPRequestOperationManager manager];
        self.manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        self.manager.responseSerializer = [QMHTTPResponseSerializer serializer];
        self.manager.requestSerializer.timeoutInterval = 20; //4秒既请求超时
        NSString *userAgent = [NSString stringWithFormat:@"Sihuo v%@ (iphone; iOS %@)", [VersionInfo currentVer], [[UIDevice currentDevice]systemVersion]];
        [self.manager.requestSerializer setValue:userAgent forHTTPHeaderField:@"User-Agent"];
    }
    return self;
}

+ (QMHttpClient *)defaultClient
{
    static QMHttpClient *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)saveHost:(NSDictionary *)headerFields
{
    NSString *host = [headerFields objectForKey:@"Cdnhost"];
    if (host && ![host isEqualToString:@""] && ![host isKindOfClass:[NSNull class]]) {
        [UserDefaultsUtils saveValue:host forKey:kCdnhost];
    }
    else{
        host = [UserDefaultsUtils valueWithKey:kCdnhost];
        if (!host) {
            host = [@"http://" stringByAppendingString:[AppUtils serverHost]];
            [UserDefaultsUtils saveValue:host forKey:kCdnhost];
        }
    }
}

- (void)requestWithPath:(NSString *)url method:(NSInteger)method parameters:(id)parameters prepareExecute:(PrepareExecuteBlock)prepare success:(void (^)(AFHTTPRequestOperation *, id))success failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure
{
    //请求的URL
    DLog(@"Request path:%@", url);
    
    //判断网络状况（有链接：执行请求；无链接：弹出提示）
    if ([self isConnectionAvailable]) {
        isAlertShow_ = NO;
        
        //预处理（显示加载信息啥的）
        if (prepare) {
            prepare();
        }
        
        //加载cookie
        [AppUtils loadCookies];
        
        switch (method) {
            case QMHttpRequestGet:
            {
                [self.manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    [self saveHost:operation.response.allHeaderFields];
                    if (success) {
                        success(operation, responseObject);
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    if (failure) {
                        failure(operation,error);
                    }
                }];
            }
//                [self.manager GET:url parameters:parameters success:success failure:failure];
                break;
            case QMHttpRequestPost:
            {
                [self.manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    [self saveHost:operation.response.allHeaderFields];
                    if (success) {
                        success(operation, responseObject);
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    if (failure) {
                        failure(operation, error);
                    }
                }];
            }
//                [self.manager POST:url parameters:parameters success:success failure:failure];
                break;
            case QMHttpRequestPut:
            {
                [self.manager PUT:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    [self saveHost:operation.response.allHeaderFields];
                    if (success) {
                        success(operation, responseObject);
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    if (failure) {
                        failure(operation, error);
                    }
                }];
            }
//                [self.manager PUT:url parameters:parameters success:success failure:failure];
                break;
            case QMHttpRequestDelete:
            {
                [self.manager DELETE:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    [self saveHost:operation.response.allHeaderFields];
                    if (success) {
                        success(operation, responseObject);
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    if (failure) {
                        failure(operation, error);
                    }
                }];
            }
//                [self.manager DELETE:url parameters:parameters success:success failure:failure];
                break;
            default:
                break;
        }
    }
    else{
        if (!isAlertShow_) {
            isAlertShow_ = YES;
            [self showExceptionDialog];
        }
        if (failure) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey:[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            NSError *error = [NSError errorWithDomain:kAppErrorDomain code:QMNotNetworkError userInfo:userInfo];
            failure(nil, error);
        }
        //发出网络异常通知广播
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotNetworkErrorNotity object:nil];
    }
}

- (void)requestWithInHEAD:(NSString *)url parameters:(NSDictionary *)parameters success:(void (^)(AFHTTPRequestOperation *))success failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure
{
    if ([self isConnectionAvailable]) {
        //加载cookie
        [AppUtils loadCookies];
        
        [self.manager HEAD:url parameters:parameters success:success failure:failure];
    }
}

- (BOOL)isConnectionAvailable
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    if (!didRetrieveFlags)
    {
        DLog(@"Error. Could not recover network reachability flags");
        return NO;
    }
    BOOL isReachable = ((flags & kSCNetworkFlagsReachable) != 0);
    BOOL needsConnection = ((flags & kSCNetworkFlagsConnectionRequired) != 0);
    return (isReachable && !needsConnection) ? YES : NO;
}

//弹出网络错误提示框
- (void)showExceptionDialog
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:[AppUtils localizedCommonString:@"QM_Tip_Title"]
                                    message:[AppUtils localizedCommonString:@"QM_Alert_Network"]
                                   delegate:nil
                          cancelButtonTitle:[AppUtils localizedCommonString:@"QM_Button_OK"]
                          otherButtonTitles:nil] show];
    });
}

- (void)uploadFileWithPath:(NSString *)path data:(NSData *)data contentType:(NSString *)contentType success:(void (^)(AFHTTPRequestOperation *, id))success failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))uploadProgressBlock
{
    //加载cookie
    [AppUtils loadCookies];
    
    NSInputStream *inputStreamForFile = [NSInputStream inputStreamWithData:data];
    NSURL *serverURL = [NSURL URLWithString:path];
    // 上传大小
    NSMutableURLRequest *request;
    request = [NSMutableURLRequest requestWithURL:serverURL];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBodyStream:inputStreamForFile];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%ld", (unsigned long)[data length]] forHTTPHeaderField:@"Content-Length"];
 
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [QMHTTPResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:success failure:failure];
    if (uploadProgressBlock) {
        [op setUploadProgressBlock:uploadProgressBlock];
    }
    
    [self.manager.operationQueue addOperation:op];
}

- (void)downloadFileWithPath:(NSString *)path savename:(NSString *)name success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    DLog(@"downloadFile path:%@", path);
    
    if ([self isConnectionAvailable]) {
        //加载cookie
        [AppUtils loadCookies];
        
        NSURLRequest *downloadRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:path]];
        
        AFHTTPRequestOperation* op = [[AFHTTPRequestOperation alloc] initWithRequest:downloadRequest];
        op.outputStream = [NSOutputStream outputStreamToFileAtPath:name append:NO];
        [op setCompletionBlockWithSuccess:success failure:failure];
        
        [self.manager.operationQueue addOperation:op];
    }
}

@end
