//
//  QMInviteContactCell.h
//  QMMM
//
//  Created by hanlu on 14-10-31.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMInviteContactCellIdentifier;

@class ContactInfo;

@protocol QMInviteContactDelegate;

@interface QMInviteContactCell : UITableViewCell {
    ContactInfo * _contactInfo;
}

@property (nonatomic, strong) IBOutlet UILabel * nameLabel;

@property (nonatomic, strong) IBOutlet UIButton * inviteBtn;

@property (nonatomic, weak) id<QMInviteContactDelegate> delegate;

+ (id)cellForInviteContact;

- (void)updateUI:(ContactInfo *)info;

- (void)updateUIWithContact:(ContactInfo *)info andSearchInfo:(NSString *)strSearch;

@end

@protocol  QMInviteContactDelegate <NSObject>

- (void)didInviteContactClick:(ContactInfo *)info;

@end
