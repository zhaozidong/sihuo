//
//  KeyWordEntity.h
//  QMMM
//  搜索的关键对象
//  Created by kingnet  on 15-1-13.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseEntity.h"

@class FMResultSet;

@interface KeyWordEntity : BaseEntity

@property (nonatomic, assign) int wordId; //关键字id

@property (nonatomic, copy) NSString *keyWord; //关键字

- (id)initWithFMResultSet:(FMResultSet *)resultSet;

@end
