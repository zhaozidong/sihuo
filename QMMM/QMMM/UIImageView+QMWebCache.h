//
//  UIImageView+QMWebCache.h
//  QMMM
//
//  Created by kingnet  on 14-10-7.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, QMImageCacheType) {
    /**
     * The image wasn't available the SDWebImage caches, but was downloaded from the web.
     */
    QMImageCacheTypeNone,
    /**
     * The image was obtained from the disk cache.
     */
    QMImageCacheTypeDisk,
    /**
     * The image was obtained from the memory cache.
     */
    QMImageCacheTypeMemory
};

typedef void(^QMWebImageCompletionBlock)(UIImage *image, NSError *error, QMImageCacheType cacheType, NSURL *imageURL);

typedef void(^QMWebImageDownloaderProgressBlock)(NSInteger receivedSize, NSInteger expectedSize);

@interface UIImageView(QMWebCache)

- (void)qm_setImageWithURL:(NSString *)URLStr placeholderImage:(UIImage *)placeholder completed:(QMWebImageCompletionBlock)completedBlock;

- (void)qm_setImageWithURL:(NSString *)URLStr placeholderImage:(UIImage *)placeholder;

- (void)qm_setImageWithURL:(NSString *)URLStr placeholderImage:(UIImage *)placeholder progress:(QMWebImageDownloaderProgressBlock)progressBlock completed:(QMWebImageCompletionBlock)completedBlock;


@end
