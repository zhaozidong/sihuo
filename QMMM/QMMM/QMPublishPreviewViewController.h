//
//  QMPublishPreviewViewController.h
//  QMMM
//
//  Created by Derek on 15/2/28.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"
#import "GoodEntity.h"

typedef void(^UpdateGoodsBlock)(GoodEntity *goodsEntity);

@interface QMPublishPreviewViewController : BaseViewController

@property (nonatomic, copy) UpdateGoodsBlock updateGoodsBlock; //更新商品后的回调

- (id)initWithGoodsEntity:(GoodEntity *)goodEntity localImgPath:(NSArray *)arrImgPath localAudioPath:(NSString *)localAudioPath editType:(QMEditGoodsType)editType;

@end
