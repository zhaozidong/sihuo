//
//  QMNewFriendViewController.m
//  QMMM
//
//  Created by hanlu on 14-10-31.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMNewFriendViewController.h"
#import "QMNewFriendCell.h"
#import "MsgHandle.h"
#import "Friendhandle.h"
#import "MsgData.h"
#import "NSString+JSONCategories.h"
#import "NSDictionary+JSONCategories.h"
#import "FriendInfo.h"
#import "QMFailPrompt.h"
#import "PersonPageViewController.h"

@interface QMNewFriendViewController () <UITableViewDataSource, UITableViewDelegate, QMNewFriendDelegate> {
    NSMutableArray * _friendMsgList;
}
@property (nonatomic, strong) QMFailPrompt *failPrompt; //没有时的提示

@end

@implementation QMNewFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = kBackgroundColor;
    self.navigationItem.title = @"新的朋友";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backBtnClick:)];
    
    _friendMsgList = [[NSMutableArray alloc] init];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    
//    if(kIOS7) {
//        self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
//        self.tableView.separatorInset = UIEdgeInsetsZero;
//    }
    [self setExtraCellLineHidden:self.tableView];
    
    self.failPrompt =[[QMFailPrompt alloc] initWithFailType:QMFailTypeNOData labelText:@"还没有收到新的好友请求" buttonType:QMFailButtonTypeNone buttonText:nil];
    self.failPrompt.frame=CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processRefreshMsgListNotify:) name:kRefreshMsgListNotify object:nil];
    
    [self refreshFriendMsgList:YES];
    
    [[MsgHandle shareMsgHandler] updateMsgUnreadStatus:nil msgType:EMT_FRIEND unread:NO];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [AppUtils dismissHUD];
    [[NSNotificationCenter defaultCenter] postNotificationName:kFlushMsgUnreadCountNotify object:nil];
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)refreshFriendMsgList:(BOOL)bGetNew
{
    __weak MsgHandle * msgHandler = [MsgHandle shareMsgHandler];
    __weak UITableView * weakTableView = self.tableView;
    __weak NSMutableArray * weakFriendMsgList = _friendMsgList;
    __weak typeof(self) weakSelf = self;
    
    [[Friendhandle shareFriendHandle] getLocalFriendMsgList:^(id result) {
        @synchronized(weakFriendMsgList) {
            [weakFriendMsgList removeAllObjects];
            [weakFriendMsgList addObjectsFromArray:result];
        }
        
        if (weakFriendMsgList.count == 0) {
            //添加占位图
            if (nil == weakSelf.failPrompt.superview) {
                weakTableView.hidden = YES;
                [weakSelf.view addSubview:weakSelf.failPrompt];
            }
        }
        else{
            if (weakSelf.failPrompt.superview) {
                [weakSelf.failPrompt removeFromSuperview];
            }
            weakTableView.hidden = NO;
        }
        
        [weakTableView reloadData];
        
        if(bGetNew) {
            [msgHandler getMsgList];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backBtnClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - NSNotification

- (void)processRefreshMsgListNotify:(NSNotification *)notification
{
    [self refreshFriendMsgList:NO];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_friendMsgList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMNewFriendCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMNewFriendCellIdentifier];
    if(nil == cell) {
        cell = [QMNewFriendCell cellForNewFriend];
    }
    cell.delegate = self;
    
    MsgData * msgData = [_friendMsgList objectAtIndex:indexPath.row];
    [cell updateUI:msgData];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MsgData * msgData = [_friendMsgList objectAtIndex:indexPath.row];
    NSDictionary *dict = [msgData.extData toDict];
    uint64_t userId = 0;
    //收到好友请求的消息
    if (msgData.msgType == EMT_INVITE_FRIEND) {
        id tmp = [dict objectForKey:@"req_uid"];
        if([tmp isKindOfClass:[NSString class]]) {
            userId = (uint64_t)[tmp longLongValue];
        } else if([tmp isKindOfClass:[NSNumber class]]) {
            userId = [tmp unsignedLongLongValue];
        }
    }
    else{//同意添加好友,通讯录好友加入私货
        id tmp = [dict objectForKey:@"uid"];
        if([tmp isKindOfClass:[NSString class]]) {
            userId = (uint64_t)[tmp longLongValue];
        } else if([tmp isKindOfClass:[NSNumber class]]) {
            userId = [tmp unsignedLongLongValue];
        }
    }
    
    PersonPageViewController *controller = [[PersonPageViewController alloc]initWithUserId:userId];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - QMNewFriendDelegate

- (void)didAgreeBtnClick:(MsgData *)info
{
    if(info && info.msgType == EMT_INVITE_FRIEND) {
        NSDictionary * dict = [info.extData toDict];
        
        uint64_t userId = 0;
        id tmp = [dict objectForKey:@"req_uid"];
        if([tmp isKindOfClass:[NSString class]]) {
            userId = (uint64_t)[tmp longLongValue];
        } else if([tmp isKindOfClass:[NSNumber class]]) {
            userId = [tmp unsignedLongLongValue];
        }
        
        tmp = [dict objectForKey:@"token"];
        
        __weak MsgData * weakMsgData = info;
        __weak NSMutableArray * weakFriendMsgList = _friendMsgList;
        __weak UITableView * weakTableView = self.tableView;
        [[Friendhandle shareFriendHandle] acceptFriend:userId token:tmp context:^(id result) {
            if([result boolValue]) {
                NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:[weakMsgData.extData toDict]];
                [dict setObject:@(1) forKey:@"status"];
                NSString * string = [dict toString];
                weakMsgData.extData = string;

                //DB
                [[Friendhandle shareFriendHandle] updateLocalFriendMsgStatus:nil msgData:weakMsgData];
                
                //UI
                NSUInteger row = [weakFriendMsgList indexOfObject:weakMsgData];
                NSIndexPath * indexPath = [NSIndexPath indexPathForRow:row inSection:0];
                [weakTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                
                //refresh friend list
                [[NSNotificationCenter defaultCenter] postNotificationName:kFriendListChangedNotify object:nil];
                
            } else {
                [AppUtils showAlertMessage:@"接受好友失败"];
            }
        }];
    }
}

-(void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
}

@end
