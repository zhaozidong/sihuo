//
//  AppDelegate.m
//  QMMM
//
//  Created by kingnet  on 14-8-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "AppDelegate.h"
#import "RDVTabBarItem.h"
#import "QMGoodsViewController.h"
#import "GoodsHandler.h"
#import "MineViewController.h"
#import "UserDefaultsUtils.h"
#import "QMMsgV2ViewController.h"
#import "UserInfoHandler.h"
#import "QMDatabaseHelper.h"
#import "QMDataCenter.h"
#import "UserEntity.h"
#import "ContactHandle.h"
#import "FillUserInfoViewController.h"
#import "PersonPageViewController.h"
#import "XGPush.h"
#import "XGSetting.h"
#import "MsgHandle.h"
#import "MsgData.h"
#import <SDWebImage/SDImageCache.h>
#import "BackNavigationController.h"
#import "FinanceHandler.h"
#import "SystemHandler.h"
#import "VersionInfo.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "IntroduceViewController.h"
#import "QMGetLocation.h"
#import "QMRecommendViewController.h"
#import <AlipaySDK/AlipaySDK.h>
#import <FIR/FIR.h>
#import "TempFlag.h"
#import "QMIntroDetailViewController.h"
#import "EditGoodsPage1ViewController.h"
#import "SharedManager.h"
#import "HongBaoAlertView.h"
#import "ActivityDetailViewController.h"
#import "BaseHandler.h"
#import "APIConfig.h"
//for mac
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>
//for idfa
#import <AdSupport/AdSupport.h>


#define TAG_MSG_FLAG 100

//两次提示的默认间隔
static const CGFloat kDefaultPlaySoundInterval = 3.0;

@interface AppDelegate ()<UIAlertViewDelegate,IChatManagerDelegate> {
    __block VersionInfo *verInfo_;
    UINavigationController *lastNav;
}

@property (strong, nonatomic) NSDate *lastPlaySoundDate;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [SharedManager config];
    
#ifndef DEBUG
    if (!kIsTestRelease) {
        [MTA startWithAppkey:@"IQD4ID8P7D5X"];
        [[MTAConfig getInstance]setChannel:kAppChannel];
        [self umengTrack];
    }
#endif
    
    [FIR handleCrashWithKey:@"fb4c39344b0963ada6026397662c68e2"];
    
    if (![UserDefaultsUtils boolValueWithKey:@"reportDeviceInfo"]) {//上报设备信息和idfa
        [[SystemHandler sharedInstance] reportDeviceInfo];
    }
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self initEaseMob:application didFinishLaunchingWithOptions:launchOptions];

    //判断是否登录
    BOOL isLogin = [UserDefaultsUtils boolValueWithKey:kIsLogin];
    
    if (isLogin) {
        //创建当前用户数据库
        [self startCurDatabase];
        [self startApp];
    }
    else{
        //统计进入到引导页的次数
        [AppUtils trackCustomEvent:@"entry_introduce_event" args:nil];
        
        if (![UserDefaultsUtils boolValueWithKey:@"firstLoad"]) {
            IntroduceViewController *introController = [[IntroduceViewController alloc]init];
            BackNavigationController *navController = [[BackNavigationController alloc]initWithRootViewController:introController];
            self.window.rootViewController = navController;
        }else{
            QMIntroDetailViewController *introDetail = [[QMIntroDetailViewController alloc]init];
            BackNavigationController *navController = [[BackNavigationController alloc]initWithRootViewController:introDetail];
            self.window.rootViewController = navController;
        }
    }
    
    [self.window makeKeyAndVisible];
    
    [self customizeInterface];
    
    //判断程序是不是由推送服务完成的
    [self handleRemoteNotification:launchOptions];
    
    [self checkVersion];
    
    return YES;
}

- (void)customizeInterface{
    UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
    NSDictionary *textAttributes = @{
                                     NSFontAttributeName: [UIFont boldSystemFontOfSize:20],
                                     NSForegroundColorAttributeName: [UIColor whiteColor],
                                     };
    if(kIOS7) {
        [navigationBarAppearance setBarTintColor:UIColorFromRGB(0xff3232)];
    } else {
        [navigationBarAppearance setTintColor:UIColorFromRGB(0xfb5553)];
    }
    [navigationBarAppearance setTitleTextAttributes:textAttributes];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil]setTintColor:[UIColor whiteColor]];
}

- (void)startCurDatabase
{
    //创建当前用户的数据库
    NSString *uid = [UserDefaultsUtils valueWithKey:kUserId];
    [[QMDatabaseHelper sharedInstance] createDatabase:uid];
}

- (void)startApp
{
    self.viewController = nil;
    self.window.rootViewController = nil;
    //判断nickname有没有
    UserEntity *entity = [UserInfoHandler sharedInstance].currentUser;
    if (entity.nickname == nil || [entity.nickname isEqualToString:@""] || entity.nickname.length == 0) {
        FillUserInfoViewController *controller = [[FillUserInfoViewController alloc]init];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:controller];
        self.window.rootViewController = navController;
    }
    else{
        //启动数据库线程
        [[QMDataCenter sharedDataCenter] start];
        
        //setup tabbar
        [self setupViewControllers];
        
        //登陆环信消息服务器
        [self loginEaseMsgServer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FlushMsgBadge:) name:kFlushMsgUnreadCountNotify object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeToTabOne:) name:kPublishGoodsOKNotify object:nil]; //当发布了一个商品以后，自动切换到首页
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newerHongBao:) name:kNewerHongbaoNotify object:nil];
        
        self.window.rootViewController = self.viewController;
        
        //注册还信代理
        [self registerChatDelegate];
        
        //登录成功之后，注册远程通知
        [self initXGForRemoteNotification];
        
        //检测是否有系统消息未读
        [[MsgHandle shareMsgHandler] getMsgList];
        [self FlushMsgBadge:nil];
        
        //同步通讯录
//        [[ContactHandle shareContactInstance] uploadContactList];
        
        //上报当前位置
        [self submitCurrentLoc];
    }
}

- (void)logout
{
    //清空所有的key
    [UserDefaultsUtils saveValue:@"0" forKey:@"firstRecommendKey"];
    [UserDefaultsUtils saveValue:@"0" forKey:@"firstFriendKey"];
    
    [GoodsHandler freeGoodsHandler];
    
    //设置登录的标识
    [UserDefaultsUtils saveBoolValue:NO withKey:kIsLogin];
    
    //请求服务器注销当前用户
    [[UserInfoHandler sharedInstance]userLogout:nil failed:nil];
    
    //清除当前的账户信息
    [FinanceHandler sharedInstance].financeUserInfo = nil;
    
    //停止数据库线程
    [[QMDataCenter sharedDataCenter] stop];
    
    //环信消息服务器注销当前用户
    [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:YES completion:^(NSDictionary *info, EMError *error) {
        if (error) {
#ifdef DEBUG
            [AppUtils showAlertMessage:[NSString stringWithFormat:@"注销环信失败:%lu", error.errorCode]];
#endif
            //再次尝试注销当前用户
            [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:NO];
        }
    } onQueue:nil];
    
    //移除环信的delegate
    [self unregisterChatDelegate];
    
    //注销推送
    [XGPush unRegisterDevice];
    
    //清除cookie
    [AppUtils clearCookies];
    
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    
    self.viewController = nil;
    self.window.rootViewController = nil;
    
    //统计进入到引导页的次数
    [AppUtils trackCustomEvent:@"entry_introduce_event" args:nil];
    
//    IntroduceViewController *introController = [[IntroduceViewController alloc]init];
//    BackNavigationController *navController = [[BackNavigationController alloc]initWithRootViewController:introController];
    
    QMIntroDetailViewController *introDetail = [[QMIntroDetailViewController alloc]init];
    BackNavigationController *navController = [[BackNavigationController alloc]initWithRootViewController:introDetail];
    self.window.rootViewController = navController;
}

#pragma mark - registerChatDelegate

- (void)registerChatDelegate {
    [self unregisterChatDelegate];
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
}

- (void)unregisterChatDelegate {
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
}

- (void)setupViewControllers {
    //首页
    QMGoodsViewController * goodsViewController = [[QMGoodsViewController alloc] initWithListType:EGLT_FRIEND];
    UIViewController *firstNavigationController = [[BackNavigationController alloc]
                                                   initWithRootViewController:goodsViewController];
    
    //精选
//    QMGoodsViewController * recommendController = [[QMGoodsViewController alloc] initWithListType:EGLT_RECOMMEND];
    
    QMRecommendViewController *recommendController=[[QMRecommendViewController alloc] init];
    UIViewController *secondNavigationController = [[BackNavigationController alloc]initWithRootViewController:recommendController];
    
    //消息
    UIViewController * viewController = [[QMMsgV2ViewController alloc] initWithNibName:@"QMMsgV2ViewController" bundle:nil];
    UIViewController *thirdNavigationController = [[BackNavigationController alloc] initWithRootViewController:viewController];
    
    //我的
    MineViewController *fouthViewController = [[MineViewController alloc]init];
    UIViewController *fouthNavigationController = [[BackNavigationController alloc]initWithRootViewController:fouthViewController];
    
    RDVTabBarController *tabBarController = [[RDVTabBarController alloc] init];
    [tabBarController setViewControllers:@[firstNavigationController, secondNavigationController, thirdNavigationController, fouthNavigationController]];
    
    self.viewController = tabBarController;
    self.viewController.delegate = self;
    
    [self customizeTabBarForController:tabBarController];
}

- (void)customizeTabBarForController:(RDVTabBarController *)tabBarController {
    NSArray *tabBarItemImages = @[@"first", @"second", @"third", @"fouth"];
    NSArray *titles = @[@"私货", @"发现", @"消息", @"我的"];
    NSInteger index = 0;
    for (RDVTabBarItem *item in [[tabBarController tabBar] items]) {
        //去掉title
        item.title = [titles objectAtIndex:index];
        [item setItemHeight:50]; //设置item的最小高度
        [item setBackgroundSelectedImage:nil withUnselectedImage:nil];
        UIImage *selectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",
                                                      [tabBarItemImages objectAtIndex:index]]];
        UIImage *unselectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_normal",
                                                        [tabBarItemImages objectAtIndex:index]]];
        [item setFinishedSelectedImage:selectedimage withFinishedUnselectedImage:unselectedimage];
        
        if(index == 3) {
            UIImageView * imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"msg_tips_bkg"]];
            CGRect frame = item.frame;
            frame.origin.x = 0;
            frame.origin.y = 6;
            frame.size = imageView.frame.size;
            imageView.frame = frame;
            imageView.tag = TAG_MSG_FLAG;
            imageView.hidden = YES;
            
            [item addSubview:imageView];
        }
        
        index++;
    }
    
    UIImage *image = [UIImage imageNamed:@"centerButton_normal"];
    UIImage *highlightImage = [UIImage imageNamed:@"centerButton_highlight"];
    [[tabBarController tabBar]addCenterButtonWithImage:image highlightImage:highlightImage];
    [[tabBarController tabBar]setCenterButtonTitle:@"发布"];
}


//- (void)tabBarController:(RDVTabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{

//    if(!lastTime){
//        lastTime=[NSDate date];
//        return;
//    }
//    NSDate *thisTime=[NSDate date];
//    NSTimeInterval interval=[thisTime timeIntervalSinceDate:lastTime];
//    lastTime=thisTime;
//    NSLog(@"timerInterval=%.f",interval);
//    if(interval <1){
//        NSLog(@"scrolltotop");
//        if([viewController isKindOfClass:[UINavigationController class]]){
//            UINavigationController *nav=(UINavigationController *)viewController;
//            UIViewController *vi=[nav.viewControllers objectAtIndex:0];
//            NSDictionary *dictInfo=@{@"viewController":vi};
//            [[NSNotificationCenter defaultCenter] postNotificationName:kScrollToTopNotify object:nil userInfo:dictInfo];
//        }
//    }
//}


- (void)tabBarController:(RDVTabBarController *)tabBarController didClickItemAtIndex:(UIViewController *)viewController{

    if([viewController isKindOfClass:[UINavigationController class]]){
        UINavigationController *nav=(UINavigationController *)viewController;
        UIViewController *vi=[nav.viewControllers objectAtIndex:0];
        NSDictionary *dictInfo=@{@"viewController":vi};
        [[NSNotificationCenter defaultCenter] postNotificationName:kScrollToTopNotify object:nil userInfo:dictInfo];
    }
}


//初始化环信
- (void)initEaseMob:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    //注册远程推送类型
//#if !TARGET_IPHONE_SIMULATOR
//    UIRemoteNotificationType notificationTypes = UIRemoteNotificationTypeBadge |
//    UIRemoteNotificationTypeSound |
//    UIRemoteNotificationTypeAlert;
//    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:notificationTypes];
//#endif
    
    //SDK注册 APNS文件的名字, 需要与后台上传证书时的名字一一对应
    NSString *apnsCertName = nil;
#ifdef DEBUG
    //电脑编译
    apnsCertName = @"aps_development_com_kingnet_qmmm";
#else
    if (kIsTestRelease) { //给tester的
        apnsCertName = @"aps_production_com_kingnet_qmmm";
    }
    else if([[[NSBundle mainBundle] bundleIdentifier] isEqualToString:@"com.kingnet.sihuo"]){ //给inHouse的
        apnsCertName = @"aps_production_com_kingnet_sihuo";
    }
    else{ //appstore + 越狱
        apnsCertName = @"aps_production_com_sihuo_sihuo";
    }
#endif

//注册环信用户群 kingnet#qmmm测试用户 kingnet#sihuo正式用户
#ifdef DEBUG
    [[EaseMob sharedInstance] registerSDKWithAppKey:@"kingnet#qmmm" apnsCertName:apnsCertName];
#else
    if (kIsTestRelease) {
       [[EaseMob sharedInstance] registerSDKWithAppKey:@"kingnet#qmmm" apnsCertName:apnsCertName];
    }
    else{
       [[EaseMob sharedInstance] registerSDKWithAppKey:@"kingnet#sihuo" apnsCertName:apnsCertName];
    }
#endif
    
#ifdef DEBUG
    [[EaseMob sharedInstance] enableUncaughtExceptionHandler];
#endif
    //以下一行代码的方法里实现了自动登录，异步登录，需要监听[didLoginWithInfo: error:]
    //demo中此监听方法在MainViewController中
    [[EaseMob sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    //注册为SDK的ChatManager的delegate (及时监听到申请和通知)
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
}

//登陆环信消息服务器
- (void)loginEaseMsgServer
{
    NSString *uid = [UserDefaultsUtils valueWithKey:kUserId];
    NSString *pwd = [UserDefaultsUtils valueWithKey:kMsgServerPwd];
 
    [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:uid
                                                        password:pwd
                                                      completion:
     ^(NSDictionary *loginInfo, EMError *error) {
         if (loginInfo && !error) {
             [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@YES];
             [[NSNotificationCenter defaultCenter] postNotificationName:kFlushMsgUnreadCountNotify object:nil];
             //环信更换了database，需将数据导入到新的数据库
             EMError *error = [[EaseMob sharedInstance].chatManager importDataToNewDatabase];
             if (!error) {
                 error = [[EaseMob sharedInstance].chatManager loadDataFromDatabase];
                 if (error) {
                     DLog(@"数据库迁移失败:%@", error.description);
                 }
             }
             else{
                 DLog(@"数据库迁移失败:%@", error.description);
             }
         }else {
             switch (error.errorCode) {
                 case EMErrorServerNotReachable:
#ifdef DEBUG
                     [AppUtils showAlertMessage:@"连接服务器失败!"];
#else
                     if (kIsTestRelease) {
                         [AppUtils showAlertMessage:@"连接服务器失败!"];
                     }
#endif
                     break;
                 case EMErrorServerAuthenticationFailure:
#ifdef DEBUG
                     [AppUtils showAlertMessage:@"用户名或密码错误!"];
#else
                     if (kIsTestRelease) {
                         [AppUtils showAlertMessage:@"用户名或密码错误!"];
                     }
#endif
                     break;
                 case EMErrorServerTimeout:
#ifdef DEBUG
                     [AppUtils showAlertMessage:@"连接服务器超时!"];
#else
                     if (kIsTestRelease) {
                         [AppUtils showAlertMessage:@"连接服务器超时!"];
                     }
#endif
                     break;
                 default:
#ifdef DEBUG
                     [AppUtils showAlertMessage:[NSString stringWithFormat:@"登录失败!:%lu", error.errorCode]];
#else
                     if (kIsTestRelease) {
                         [AppUtils showAlertMessage:[NSString stringWithFormat:@"登录失败!:%lu", error.errorCode]];
                     }
                     else{
                         DLog(@"登录失败!")
                     }
#endif
                     break;
             }
         }
     } onQueue:nil];
}

- (void)initXGForRemoteNotification
{
#ifdef DEBUG
    //com.kingnet.qmmm
    [XGPush startApp:2200046807 appKey:@"I969ABB54DZC"];
#else
    //com.kingnet.qmmm
    if (kIsTestRelease) {
        [XGPush startApp:2200046807 appKey:@"I969ABB54DZC"];
    }
    //com.sihuo.sihuo
    else{
        [XGPush startApp:2200089421 appKey:@"IUAT77K4F64B"];
    }
#endif
    void (^successCallback)(void) = ^(void){
        //如果变成需要注册状态
        if(![XGPush isUnRegisterStatus])
        {
            [self registerPushNotify];
        }
    };
    [XGPush initForReregister:successCallback];
    [XGPush setAccount:[UserInfoHandler sharedInstance].currentUser.uid];
}

- (void)registerPushNotify
{
#if !TARGET_IPHONE_SIMULATOR
    UIApplication *application = [UIApplication sharedApplication];
    //iOS8的
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    //iOS8以下的
    else{
        UIRemoteNotificationType notificationTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:notificationTypes];
    }
#endif
}

//注册远程通知
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [[EaseMob sharedInstance] application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    
    NSString * deviceTokenStr = [XGPush registerDevice:deviceToken];
    NSLog(@"------------------------%@", deviceTokenStr);
//    TTAlertNoTitle(deviceTokenStr);
    
    [[UserInfoHandler sharedInstance] registerDeviceToken:deviceTokenStr];
}

//注册远程通知失败
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {

    [[EaseMob sharedInstance] application:application didFailToRegisterForRemoteNotificationsWithError:error];
    
    NSString *str = [NSString stringWithFormat: @"Error: %@",error];
    
    NSLog(@"%@",str);
    
//    TTAlertNoTitle(str);
}

//收到远程通知
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[EaseMob sharedInstance] application:application didReceiveRemoteNotification:userInfo];
    
    [XGPush handleReceiveNotification:userInfo];
  
    [self handleRemoteNotification:userInfo];
}

//收到本地通知
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [[EaseMob sharedInstance] application:application didReceiveLocalNotification:notification];
    
//    //notification是发送推送时传入的字典信息
//    [XGPush localNotificationAtFrontEnd:notification userInfoKey:@"clockID" userInfoValue:@"myid"];
    
    //删除推送列表中的这一条
    [XGPush delLocalNotification:notification];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [[EaseMob sharedInstance] applicationWillResignActive:application];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"applicationDidEnterBackground" object:nil];

    [[EaseMob sharedInstance] applicationDidEnterBackground:application];
    
    NSDate *thisTime=[NSDate date];
    NSDate *lastTime=[UserDefaultsUtils valueWithKey:@"beginTime"];
    
    NSTimeInterval interval=[thisTime timeIntervalSinceDate:lastTime];
    NSString *strTime=[NSString stringWithFormat:@"%u",(uint)interval];
    [[SystemHandler sharedInstance] reportUseTime:strTime];
    
    //计算未读的数量
    [[MsgHandle shareMsgHandler] checkLocalSystemMsgUnreadCount:^(id result) {
        //环信聊天消息未读数
        NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
        NSUInteger unreadCount = 0;
        for (EMConversation *conversation in conversations) {
            unreadCount += conversation.unreadMessagesCount;
        }
     
        if([result count] > 0) {
            NSUInteger favorUnreadCount = [[result objectForKey:@"favor"] unsignedIntegerValue];
            unreadCount += favorUnreadCount;
            NSUInteger friendUnreadCount = [[result objectForKey:@"friend"] unsignedIntegerValue];
            unreadCount += friendUnreadCount;
            NSInteger walletUnreadCount = [[result objectForKey:@"wallet"] unsignedIntegerValue];
            unreadCount += walletUnreadCount;
            NSInteger tradeUnreadCount = [[result objectForKey:@"trade"] unsignedIntegerValue];
            unreadCount += tradeUnreadCount;
        }
        //设置badgenum
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:unreadCount];
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[EaseMob sharedInstance] applicationWillEnterForeground:application];
    
    //app切换到前台时，检测是否有新系统消息，有则通知UI
    if([UserDefaultsUtils boolValueWithKey:kIsLogin]) {
        [[MsgHandle shareMsgHandler] getMsgList];
    }
    
    //设置badgenum
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    //    [UserDefaultsUtils valueWithKey:@"beginTime"];
    NSDate *date=[NSDate date];
    [UserDefaultsUtils saveValue:date forKey:@"beginTime"];
    
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
    
    [[EaseMob sharedInstance] applicationDidBecomeActive:application];
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_8_0

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings NS_AVAILABLE_IOS(8_0)
{
    if(notificationSettings.types == UIUserNotificationTypeBadge) {
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
}

//按钮点击事件回调
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler{
    if([identifier isEqualToString:@"ACCEPT_IDENTIFIER"]){
        NSLog(@"ACCEPT_IDENTIFIER is clicked");
    }
    
    completionHandler();
}

#endif



- (void)applicationWillTerminate:(UIApplication *)application
{
    [[EaseMob sharedInstance] applicationWillTerminate:application];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation NS_AVAILABLE_IOS(4_2)
{
    //跳转支付宝钱包进行支付，需要将支付宝钱包的支付结果回传给SDK
    if ([url.host isEqualToString:@"safepay"]) {
        [[AlipaySDK defaultService]
         processOrderWithPaymentResult:url
         standbyCallback:^(NSDictionary *resultDic) {
//             NSLog(@"result = %@", resultDic);
             if(self.orderInfo && resultDic){
                if ([[resultDic objectForKey:@"resultStatus"] isEqualToString:@"9000"] || [[resultDic objectForKey:@"resultStatus"] isEqualToString:@"4000"]) {
                    [_orderInfo notifyForSuccessWithParams:resultDic];
                }
             }
         }];
    }else{
        return [WXApi handleOpenURL:url delegate:self];
    }
    return YES;
}

#pragma mark - Configure UMeng
- (void)umengTrack {
        [MobClick setCrashReportEnabled:NO]; // 如果不需要捕捉异常，注释掉此行
//    [MobClick setLogEnabled:YES];  // 打开友盟sdk调试，注意Release发布时需要注释掉此行,减少io消耗
    [MobClick setAppVersion:[VersionInfo currentVer]]; //参数为NSString * 类型,自定义app版本信息，如果不设置，默认从CFBundleVersion里取
    //
    [MobClick startWithAppkey:UMENG_APPKEY reportPolicy:(ReportPolicy) BATCH channelId:kAppChannel];
    //   reportPolicy为枚举类型,可以为 REALTIME, BATCH,SENDDAILY,SENDWIFIONLY几种
    //   channelId 为NSString * 类型，channelId 为nil或@""时,默认会被被当作@"App Store"渠道
    
    //      [MobClick checkUpdate];   //自动更新检查, 如果需要自定义更新请使用下面的方法,需要接收一个(NSDictionary *)appInfo的参数
    //    [MobClick checkUpdateWithDelegate:self selector:@selector(updateMethod:)];
    
    [MobClick updateOnlineConfig];  //在线参数配置
    
    [MobClick setEncryptEnabled:YES];  //设置日志加密
    
    //    1.6.8之前的初始化方法
    //    [MobClick setDelegate:self reportPolicy:REALTIME];  //建议使用新方法
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onlineConfigCallBack:) name:UMOnlineConfigDidFinishedNotification object:nil];
    
    //统计链接来源
    NSString * deviceName = [[[UIDevice currentDevice] name] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString * mac = [self macString];
    NSString * idfa = [self idfaString];
    NSString * idfv = [self idfvString];
    NSString * urlString = [NSString stringWithFormat:@"http://log.umtrack.com/ping/%@/?devicename=%@&mac=%@&idfa=%@&idfv=%@", UMENG_APPKEY, deviceName, mac, idfa, idfv];
    [NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL: [NSURL URLWithString:urlString]] delegate:nil];
}

- (void)onlineConfigCallBack:(NSNotification *)note {
    
    NSLog(@"online config has fininshed and note = %@", note.userInfo);
}

#pragma mark - push

- (void)didBindDeviceWithError:(EMError *)error
{
    if (error) {
        [AppUtils showAlertMessage:@"消息推送与设备绑定失败"];
    }
}

#pragma mark - RDVTabBarControllerDelegate
- (void)didTapCenterButton
{
    kPublishGoodsFrom = @"";
    
    //统计发布按钮点击
    [AppUtils trackCustomEvent:@"publish_btn_click_event" args:nil];
    
    TGCameraNavigationController *navigationController = [TGCameraNavigationController newWithCameraDelegate:self];
    [self.viewController presentViewController:navigationController animated:YES completion:nil];
}

//- (BOOL)tabBarController:(RDVTabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
//{
//    NSInteger index = [tabBarController.viewControllers indexOfObject:viewController];
//    if (index == 1 || index == 2 || index == 3) {
//        BOOL isLogin = [UserDefaultsUtils boolValueWithKey:kIsLogin];
//        if (!isLogin) {
//            LoginAndRegisterViewController *controller = [[LoginAndRegisterViewController alloc]init];
//            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:controller];
//            [self.viewController presentViewController:navController animated:YES completion:nil];
//            return NO;
//        }
//        else{
//            return YES;
//        }
//    }
//    return YES;
//}

- (void)tabBarController:(RDVTabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if (tabBarController.selectedIndex == 3) {
        //选择我的tab
        [[MsgHandle shareMsgHandler]updateMsgUnreadStatus:nil msgType:EMT_TRADE unread:NO];
        RDVTabBarItem * item = self.viewController.tabBar.items[3];
        UIView * view = [item viewWithTag:TAG_MSG_FLAG];
        view.hidden = YES;
    }
}

#pragma mark -
#pragma mark - TGCameraDelegate

- (void)cameraDidTakePhoto:(UIImage *)image
{
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidCancel
{
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
}

- (int)maxNumberOfPhotos
{
    return kGoodsPhotoLimitNum;
}

- (void)cameraWillTakePhoto
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)cameraDidOKWithPhotos:(NSArray *)photoArr
{
    if (photoArr.count == 0) {
        [AppUtils showAlertMessage:@"至少选择一张图片才能发布哦~"];
        return;
    }
    
    [self.viewController dismissViewControllerAnimated:YES completion:^{
        EditGoodsPage1ViewController *controller = [[EditGoodsPage1ViewController alloc]initWithType:QMEditNewGoods];
        controller.beforPhotos = photoArr;
        BackNavigationController *navController = [[BackNavigationController alloc]initWithRootViewController:controller];
        [self.viewController presentViewController:navController animated:YES completion:nil];
    }];
}

#pragma mark - WXApiDelegate

-(void) onReq:(BaseReq*)req
{
    
}

-(void) onResp:(BaseResp*)resp
{
    
}

#pragma mark - NSNotification
- (void)changeToTabOne:(NSNotification *)notification
{
    [self.viewController setSelectedIndex:0];
}

//标tabbar上的小红点
- (void)FlushMsgBadge:(NSNotification *)notification
{
    [[MsgHandle shareMsgHandler] checkLocalSystemMsgUnreadCount:^(id result) {
        //环信聊天消息未读数
        NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
        NSUInteger unreadCount = 0;
        for (EMConversation *conversation in conversations) {
            unreadCount += conversation.unreadMessagesCount;
        }
        
        NSUInteger friendUnreadCount = 0;
        NSUInteger walletUnreadCount = 0;
        NSUInteger tradeUnreadCount = 0;
        if([result count] > 0) {
            NSUInteger favorUnreadCount = [[result objectForKey:@"favor"] unsignedIntegerValue];
            friendUnreadCount = [[result objectForKey:@"friend"] unsignedIntegerValue];
            walletUnreadCount = [[result objectForKey:@"wallet"] unsignedIntegerValue];
            tradeUnreadCount = [[result objectForKey:@"trade"] unsignedIntegerValue];
            unreadCount += favorUnreadCount;
        }
        
        RDVTabBarItem * item = self.viewController.tabBar.items[2];
        if(unreadCount > 0) {
            [item setBadgeValue:[NSString stringWithFormat:@"%ld",(long)unreadCount]];
        } else {
            [item setBadgeValue:@""];
        }
        
        item = self.viewController.tabBar.items[3];
        UIView * view = [item viewWithTag:TAG_MSG_FLAG];
        if((friendUnreadCount > 0 || walletUnreadCount || tradeUnreadCount) && self.viewController.selectedIndex != 3) {
            [view setHidden:NO];
        }
        
        if (self.viewController.selectedIndex == 3) {
            //当前在我的界面，置交易消息已读
            [[MsgHandle shareMsgHandler]updateMsgUnreadStatus:nil msgType:EMT_TRADE unread:NO];
        }

    }];
}

#pragma mark - 消息变化
//聊天列表变化通知
- (void)didUpdateConversationList:(NSArray *)conversationList
{
    [[NSNotificationCenter defaultCenter]postNotificationName:kUpdateConversationListNotify object:conversationList];
}

// 未读消息数量变化回调
-(void)didUnreadMessagesCountChanged
{
    //环信过来新的消失，通知消息界面刷新
    [[NSNotificationCenter defaultCenter]postNotificationName:kUnreadMessagesCountChangedNotity object:nil];
    [self FlushMsgBadge:nil];
}

//离线消息通知
- (void)didFinishedReceiveOfflineMessages:(NSArray *)offlineMessages
{
    [[NSNotificationCenter defaultCenter]postNotificationName:kFinishedReceiveOfflineMessagesNotify object:nil];
    [self FlushMsgBadge:nil];
}

//收到消息
- (void)didReceiveMessage:(EMMessage *)message
{
    [[NSNotificationCenter defaultCenter]postNotificationName:kDidReceiveMessageNotify object:message];
    BOOL needShowNotification = message.isGroup ? [self needShowNotification:message.conversationChatter] : YES;
    if (needShowNotification) {
#if !TARGET_IPHONE_SIMULATOR
        
        BOOL isAppActivity = [[UIApplication sharedApplication] applicationState] == UIApplicationStateActive;
        if (!isAppActivity) {
            [self showNotificationWithMessage:message];
        }else {
            [self playSoundAndVibration];
        }
#endif
    }
}

- (BOOL)needShowNotification:(NSString *)fromChatter
{
    BOOL ret = YES;
    NSArray *igGroupIds = [[EaseMob sharedInstance].chatManager ignoredGroupIds];
    for (NSString *str in igGroupIds) {
        if ([str isEqualToString:fromChatter]) {
            ret = NO;
            break;
        }
    }
    
    if (ret) {
        EMPushNotificationOptions *options = [[EaseMob sharedInstance].chatManager pushNotificationOptions];
        
        do {
            if (options.noDisturbing) {
                NSDate *now = [NSDate date];
                NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute
                                                                               fromDate:now];
                
                NSInteger hour = [components hour];
                //        NSInteger minute= [components minute];
                
                NSUInteger startH = options.noDisturbingStartH;
                NSUInteger endH = options.noDisturbingEndH;
                if (startH>endH) {
                    endH += 24;
                }
                
                if (hour>=startH && hour<=endH) {
                    ret = NO;
                    break;
                }
            }
        } while (0);
    }
    
    return ret;
}

- (void)playSoundAndVibration{
    NSTimeInterval timeInterval = [[NSDate date]
                                   timeIntervalSinceDate:self.lastPlaySoundDate];
    if (timeInterval < kDefaultPlaySoundInterval) {
        //如果距离上次响铃和震动时间太短, 则跳过响铃
        NSLog(@"skip ringing & vibration %@, %@", [NSDate date], self.lastPlaySoundDate);
        return;
    }
    
    //保存最后一次响铃时间
    self.lastPlaySoundDate = [NSDate date];
    
    // 收到消息时，播放音频
    [[EaseMob sharedInstance].deviceManager asyncPlayNewMessageSound];
    // 收到消息时，震动
    [[EaseMob sharedInstance].deviceManager asyncPlayVibration];
}

- (void)showNotificationWithMessage:(EMMessage *)message
{
    EMPushNotificationOptions *options = [[EaseMob sharedInstance].chatManager pushNotificationOptions];
    //发送本地推送
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate date]; //触发通知的时间
    
    if (options.displayStyle == ePushNotificationDisplayStyle_messageSummary) {
        id<IEMMessageBody> messageBody = [message.messageBodies firstObject];
        NSString *messageStr = nil;
        switch (messageBody.messageBodyType) {
            case eMessageBodyType_Text:
            {
                messageStr = ((EMTextMessageBody *)messageBody).text;
            }
                break;
            case eMessageBodyType_Image:
            {
                messageStr = @"[图片]";
            }
                break;
            case eMessageBodyType_Location:
            {
                messageStr = @"[位置]";
            }
                break;
            case eMessageBodyType_Voice:
            {
                messageStr = @"[音频]";
            }
                break;
            case eMessageBodyType_Video:{
                messageStr = @"[视频]";
            }
                break;
            default:
                break;
        }
        
        NSString *title = message.from;
        if (message.isGroup) {
            NSArray *groupArray = [[EaseMob sharedInstance].chatManager groupList];
            for (EMGroup *group in groupArray) {
                if ([group.groupId isEqualToString:message.conversationChatter]) {
                    title = [NSString stringWithFormat:@"%@(%@)", message.groupSenderName, group.groupSubject];
                    break;
                }
            }
        }
        
        notification.alertBody = [NSString stringWithFormat:@"%@:%@", title, messageStr];
    }
    else{
        notification.alertBody = @"您有一条新消息";
    }
    
    //此处便于调试
    //notification.alertBody = [[NSString alloc] initWithFormat:@"[本地]%@", notification.alertBody];
    
    notification.alertAction = @"打开";
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    //发送通知
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    //    UIApplication *application = [UIApplication sharedApplication];
    //    application.applicationIconBadgeNumber += 1;
}


//- (void)enableRemoteNotification:(BOOL)enable
//{
//#if !TARGET_IPHONE_SIMULATOR
//    if(enable) {
//        UIRemoteNotificationType notificationTypes = UIRemoteNotificationTypeBadge |
//        UIRemoteNotificationTypeSound |
//        UIRemoteNotificationTypeAlert;
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:notificationTypes];
//    } else {
//        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
//    }
//#endif
//}

#pragma mark - handle remote notification

- (void)handleRemoteNotification:(NSDictionary *)launchOptions
{
    if(nil == launchOptions) return;
    //清除所有通知(包含本地通知)
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
   
    //是否远程启动
    if([launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"]) {
         //启动后直接跳转到指定页面
        NSDictionary* pushDict = [launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
//        NSString * string = [NSString stringWithFormat:@"%@", pushDict];
//        TTAlertNoTitle(string);
        [self jumpToPage:pushDict];
    } else {
        //本地启动
        UIApplicationState state = [UIApplication sharedApplication].applicationState;
        if(state == UIApplicationStateActive) {
            if([launchOptions objectForKey:@"f"]) { //
                [[MsgHandle shareMsgHandler] getMsgList];
            }
        } else {
            [self jumpToPage:launchOptions];
        }
    }
}

- (void)jumpToPage:(NSDictionary *)userInfo
{
    //跳转消息界面
    [self.viewController setSelectedIndex:2];
    
    //拉取消息
    [[MsgHandle shareMsgHandler] getMsgList];
}

#pragma mark - Get Current Location
- (void)submitCurrentLoc
{
    [QMGetLocation getCurrentPoint:^(CLLocation *currentLoc, NSError *error) {
        if (nil != error) {
            CLLocationCoordinate2D coordinate = currentLoc.coordinate;
            [[UserInfoHandler sharedInstance]submitLocation:[NSString stringWithFormat:@"%f,%f", coordinate.latitude, coordinate.longitude] success:nil failed:nil];
        }
    }];
}

#pragma mark - ChechVerson
- (void)checkVersion
{
    __weak typeof(self) weakSelf = self;
    [[SystemHandler sharedInstance] checkVersion:^(id obj) {
        [AppUtils dismissHUD];
        verInfo_ = (VersionInfo *)obj;
        [weakSelf treatUpdateTip];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)treatUpdateTip
{
    BOOL haveNew = [VersionInfo isHaveNewVer:verInfo_.version];
    if (haveNew) {
        //拉取之前的提示记录
        NSDictionary *tipLog = [UserDefaultsUtils valueWithKey:kVersionUpdateTip];
        id temp = [tipLog objectForKey:verInfo_.version];
        if (!temp || [temp boolValue]==NO) {
            //该版本的更新没有提示过，则提示并做记录
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"检测到新版本V%@", verInfo_.version] message:verInfo_.desc delegate:self cancelButtonTitle:@"再看看" otherButtonTitles:@"马上更新", nil];
            alertView.tag = 9999;
            [alertView show];
            
            //记录下本次的提示
            NSMutableDictionary *mbDic = [NSMutableDictionary dictionaryWithDictionary:tipLog];
            [mbDic setObject:[NSNumber numberWithBool:YES] forKey:verInfo_.version];
            [UserDefaultsUtils saveValue:mbDic forKey:kVersionUpdateTip];
        }
        
        //tab上的小红点的提示
//        NSDictionary *redPointLog = [UserDefaultsUtils valueWithKey:kVersionRedPonitTip];
//        temp = [redPointLog objectForKey:verInfo_.version];
//        if ((!temp || [temp boolValue]==NO) && self.viewController.selectedIndex!=3) {//第一次展示该版本或者没有读过
//            //‘我的’上面标小红点
//            RDVTabBarItem * item = self.viewController.tabBar.items[3];
//            UIView * view = [item viewWithTag:TAG_MSG_FLAG];
//            view.hidden = NO;
//        }
    }
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex) {
        return ;
    }
    else{
        if (alertView.tag == 9999) {
            //跳到appstore下载
            [self gotoAppStore];
        }
        else if (alertView.tag == 9998){
            [self logout];
        }
    }
}

- (void)gotoAppStore
{
//    //跳到APPStore
//    NSString *appstoreUrl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/cn/app/jie-zou-da-shi/id%@?mt=8", kAppleID];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appstoreUrl]];
    //跳转到safari
    NSString *url = verInfo_.downloadUrl;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

#pragma mark - 
#pragma mark - IChatManagerDelegate 登录状态变化
- (void)didLoginFromOtherDevice
{
    [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:NO completion:^(NSDictionary *info, EMError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"你的账号已在其他设备登录"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"确定",nil];
        alertView.tag = 9998;
        [alertView show];
    } onQueue:nil];
}

#pragma mark -
#pragma mark - For UMeng
- (NSString * )macString{
    int mib[6];
    size_t len;
    char *buf;
    unsigned char *ptr;
    struct if_msghdr *ifm;
    struct sockaddr_dl *sdl;
    
    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;
    
    if ((mib[5] = if_nametoindex("en0")) == 0) {
        printf("Error: if_nametoindex error\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 1\n");
        return NULL;
    }
    
    if ((buf = malloc(len)) == NULL) {
        printf("Could not allocate memory. error!\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 2");
        free(buf);
        return NULL;
    }
    
    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)LLADDR(sdl);
    NSString *macString = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                           *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    free(buf);
    
    return macString;
}

- (NSString *)idfaString {
    
    NSBundle *adSupportBundle = [NSBundle bundleWithPath:@"/System/Library/Frameworks/AdSupport.framework"];
    [adSupportBundle load];
    
    if (adSupportBundle == nil) {
        return @"";
    }
    else{
        
        Class asIdentifierMClass = NSClassFromString(@"ASIdentifierManager");
        
        if(asIdentifierMClass == nil){
            return @"";
        }
        else{
            
            //for no arc
            //ASIdentifierManager *asIM = [[[asIdentifierMClass alloc] init] autorelease];
            //for arc
            ASIdentifierManager *asIM = [[asIdentifierMClass alloc] init];
            
            if (asIM == nil) {
                return @"";
            }
            else{
                
                if(asIM.advertisingTrackingEnabled){
                    return [asIM.advertisingIdentifier UUIDString];
                }
                else{
                    return [asIM.advertisingIdentifier UUIDString];
                }
            }
        }
    }
}

- (NSString *)idfvString
{
    if([[UIDevice currentDevice] respondsToSelector:@selector( identifierForVendor)]) {
        return [[UIDevice currentDevice].identifierForVendor UUIDString];
    }
    
    return @"";
}

#pragma mark -
#pragma mark - HongBao
- (void)newerHongBao:(NSNotification *)notify
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kNewerHongbaoNotify object:nil];
    int money = 0;
    id temp = notify.object;
    if (temp && [temp isKindOfClass:[NSNumber class]]) {
        money = [temp intValue];
    }
    else if (temp && [temp isKindOfClass:[NSString class]]){
        money = [temp intValue];
    }
    
    if (money > 0) {
        HongBaoAlertView *hongbaoAlert = [[HongBaoAlertView alloc] initWithMoney:money];
        __weak typeof(self) weakSelf = self;
        hongbaoAlert.alertBlock = ^(void){
            ActivityDetailViewController *controller = [[ActivityDetailViewController alloc]initWithUrl:[BaseHandler requestUrlWithPath:API_HONGBAO_EXCHANGE_LIST]];
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:controller];
            [weakSelf.viewController presentViewController:navController animated:YES completion:nil];
        };
        dispatch_time_t delaySeconds = dispatch_time(DISPATCH_TIME_NOW, 0.5*NSEC_PER_SEC);
        dispatch_after(delaySeconds, dispatch_get_main_queue(), ^{
            [hongbaoAlert show];
        });
    }
}

@end
