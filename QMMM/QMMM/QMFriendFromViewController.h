//
//  QMFriendFromViewController.h
//  QMMM
//
//  Created by Derek on 15/1/26.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"
#import "UserDataInfo.h"


@interface QMFriendFromViewController : BaseViewController


@property (nonatomic, strong)UITableView *tableView;

@property (nonatomic, strong)PersonPageUserInfo *userInfo;


-(id)initWithUserInfo:(PersonPageUserInfo *)userInfo;

@end
