//
//  NSString+JSONCategories.h
//  QMMM
//
//  Created by hanlu on 14-9-16.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JSONCategories)

-(NSArray *)toArray;

- (NSDictionary *)toDict;

@end
