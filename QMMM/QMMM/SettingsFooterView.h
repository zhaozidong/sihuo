//
//  SettingsFooterView.h
//  QMMM
//
//  Created by kingnet  on 14-11-5.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsFooterView : UIView

@property (nonatomic, strong) NSString *curVersion; //当前的版本号

+ (SettingsFooterView *)footerView;

@end
