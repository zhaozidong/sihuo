//
//  QMIntroDetailViewController.m
//  QMMM
//
//  Created by Derek on 15/1/9.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMIntroDetailViewController.h"
#import "IntroDetailView.h"
#import "GoodsHandler.h"
#import "IntroDetailView.h"
#import "CycleScrollView.h"
#import "GoodEntity.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "SystemHandler.h"
#import "QAudioPlayer.h"

@interface QMIntroDetailViewController (){
//    UIScrollView *scroll;
//    NSMutableArray *arrView;
    CycleScrollView *cycleScroll;
    
}

@property(strong,nonatomic)IntroDetailView *introDetail;

@end

@implementation QMIntroDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.edgesForExtendedLayout =UIRectEdgeNone;
    [self.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    self.view.backgroundColor=kBackgroundColor;
    
    if (!kISSmall || IS_IPHONE_5) {
        UILabel *lblTitle=[[UILabel alloc] init];
        lblTitle.frame=CGRectMake(0, 30, kMainFrameWidth, 40);
        lblTitle.backgroundColor=[UIColor clearColor];
        lblTitle.textAlignment=NSTextAlignmentCenter;
        lblTitle.textColor=UIColorFromRGB(0xaeb8c7);
        lblTitle.font=[UIFont systemFontOfSize:20.0f];
        lblTitle.text=@"私货里正在卖的好东西";
        //    [lblTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.view addSubview:lblTitle];
    }
    
    [self setUpButtons];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"intro" ofType:@"plist"];
    NSArray *array = [[NSArray alloc] initWithContentsOfFile:plistPath];
    [self setUpScrollWithArray:array];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[QAudioPlayer sharedInstance] stopPlay];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setUpButtons{
    
    UIImage *image = [UIImage imageNamed:@"submit_btn"];
    image = [image stretchableImageWithLeftCapWidth:7 topCapHeight:7];
    
    UIButton *btnRegister=[[UIButton alloc] init];
    btnRegister.frame=CGRectMake(30,kMainFrameHeight-30-40, kMainFrameWidth/2-30-15, 40);
    [btnRegister setBackgroundImage:image forState:UIControlStateNormal];
    btnRegister.layer.cornerRadius=10.0f;
    [btnRegister setTitle:@"注册" forState:UIControlStateNormal];
    [btnRegister addTarget:self action:@selector(btnDidRegister:) forControlEvents:UIControlEventTouchUpInside];
//    [btnRegister setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:btnRegister];
    
    
    UIButton *btnLogin=[[UIButton alloc] init];
    btnLogin.frame=CGRectMake(kMainFrameWidth/2+15,kMainFrameHeight-30-40, btnRegister.frame.size.width, 40);
    [btnLogin setBackgroundImage:image forState:UIControlStateNormal];
    [btnLogin setTitle:@"登录" forState:UIControlStateNormal];
    btnLogin.layer.cornerRadius=10.0f;
//    [btnLogin setTranslatesAutoresizingMaskIntoConstraints:NO];
    [btnLogin addTarget:self action:@selector(btnDidLogin:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnLogin];
    
//    NSArray *registerConstraints1=[NSLayoutConstraint constraintsWithVisualFormat:@"V:[btnRegister(==40)]-30-|"
//                                                                 options:0
//                                                                 metrics:nil
//                                                                   views:NSDictionaryOfVariableBindings(btnRegister)];
//    
//    NSArray *registerConstraints2=[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-14-[btnRegister]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btnRegister)];
//    
//    
//    [self.view addConstraints:registerConstraints1];
//    [self.view addConstraints:registerConstraints2];
//    
//    
//    NSArray *loginConstraints1=[NSLayoutConstraint constraintsWithVisualFormat:@"V:[btnLogin(btnRegister)]-30-|" options:NSLayoutFormatAlignAllTop metrics:nil views:NSDictionaryOfVariableBindings(btnLogin,btnRegister)];
//    
//    NSArray *loginConstraints2=[NSLayoutConstraint constraintsWithVisualFormat:@"H:[btnRegister]-14-[btnLogin(btnRegister)]-14-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btnRegister,btnLogin)];
//    [self.view addConstraints:loginConstraints1];
//    [self.view addConstraints:loginConstraints2];
    
    
}

-(void)btnDidLogin:(id)sender{
    [AppUtils trackCustomEvent:@"login_event" args:nil];
    LoginViewController *controller = [[LoginViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)btnDidRegister:(id)sender{
    //点击注册按钮
    [[SystemHandler sharedInstance] reportRegisterStep:@"reg_btn"];
    [AppUtils trackCustomEvent:@"register_event" args:nil];
    RegisterViewController *controller = [[RegisterViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)setUpScrollWithArray:(NSArray *)arr{
//    for (int i=0;i<[arr count];i++ ) {
//        NSDictionary *dict=[arr objectAtIndex:i];
//        IntroDetailView *intro=[[IntroDetailView alloc] initWithDict:dict];
////        CGRect frame=CGRectMake(i*kMainFrameWidth, 0, kMainFrameWidth, scroll.frame.size.height);
////        intro.backgroundColor=[UIColor colorWithRed:i*30/255.0f green:i*30/255.0f blue:i*30/255.0f alpha:1.0f];
////        intro.frame=frame;
////        [scroll addSubview:intro];
//        
//
//    }
    if (kISSmall && !IS_IPHONE_5) {
        cycleScroll=[[CycleScrollView alloc]initWithFrame:CGRectMake(0, 20, kMainFrameWidth, kMainFrameHeight-120)];
    }else if(IS_IPHONE_5){
        cycleScroll=[[CycleScrollView alloc]initWithFrame:CGRectMake(0, 70, kMainFrameWidth, kMainFrameHeight-170)];
    }else{
        cycleScroll=[[CycleScrollView alloc]initWithFrame:CGRectMake(0, 90, kMainFrameWidth, kMainFrameHeight-120-100)];
    }
    
    cycleScroll.backgroundColor=kBackgroundColor;
    
    NSMutableArray *arrView=[NSMutableArray arrayWithCapacity:10];
    for (int i=0; i<[arr count]; i++) {
//        GoodEntity *entity=(GoodEntity *)[arr objectAtIndex:i];
        NSDictionary *dictInfo=[arr objectAtIndex:i];
//        IntroDetailView *intro=[[IntroDetailView alloc] initWithGoodEntity:entity];
        IntroDetailView *intro=[[IntroDetailView alloc] initWithDict:dictInfo];
        CGRect frame=CGRectMake(i*kMainFrameWidth, 0, kMainFrameWidth, cycleScroll.frame.size.height);
//        intro.backgroundColor=[UIColor colorWithRed:i*30/255.0f green:i*30/255.0f blue:i*30/255.0f alpha:1.0f];
        intro.backgroundColor=[UIColor clearColor];
        intro.frame=frame;
        [arrView addObject:intro];
    }

    
    cycleScroll.fetchContentViewAtIndex=^(NSInteger pageIndex){
        [[QAudioPlayer sharedInstance] stopPlay];
        return arrView[pageIndex];
    };
    
    cycleScroll.totalPagesCount=^NSInteger(void){
        return [arr count];
    };
    
    cycleScroll.TapActionBlock=^(NSInteger pageIndex){
//        NSLog(@"点击了第%ld个",(long)pageIndex);
    };
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [cycleScroll scrollToNext];
    });
    
    [self.view addSubview:cycleScroll];
}
@end
