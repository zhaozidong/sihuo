//
//  QMTopNavView.m
//  QMMM
//
//  Created by kingnet  on 14-10-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMTopNavView.h"
#import "QMLineLabel.h"
#import "UIButton+Addition.h"

@interface QMTopNavView()
@property (nonatomic, copy) NSArray *titlesArr;
@property (nonatomic) UILabel *redLbl; //底部的线
@end
@implementation QMTopNavView
@synthesize selectedIndex = _selectedIndex;

static const int kBaseButtonTag = 100;
const CGFloat kAnimationSpeed = 0.20;

- (id)initWithFrame:(CGRect)frame titles:(NSArray *)titles images:(NSArray *)images
{
    self = [super initWithFrame:frame];
    if (self) {
        self.titlesArr = titles;
        self.backgroundColor = [UIColor whiteColor];
        
        int i=0;
        float w = frame.size.width/[self.titlesArr count];
        for (NSString *s in self.titlesArr) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setFrame:CGRectMake(i*w, 0, w, frame.size.height)];
            [button setBackgroundColor:[UIColor clearColor]];
            [button setTitle:s forState:UIControlStateNormal];
            [button setTag:kBaseButtonTag+i];
            [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
            button.titleLabel.font = [UIFont systemFontOfSize:15.f];
            [button setTitleColor:kLightBlueColor forState:UIControlStateNormal];
            [button setTitleColor:kDarkTextColor forState:UIControlStateSelected];
            
            if (images) {
                UIImage *image = [UIImage imageNamed:images[i]];
                [button setImage:image forState:UIControlStateNormal];
                [button setButtonType:BT_LeftImageRightTitle interGap:5.f];
            }
    
            [self addSubview:button];
            i++;
        }
        
//        QMLineLabel *topLineLbl = [[QMLineLabel alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, 1.f)];
//        [self addSubview:topLineLbl];
        QMLineLabel *bottomLineLbl = [[QMLineLabel alloc]initWithFrame:CGRectMake(0, frame.size.height-1, frame.size.width, 1.f)];
        [self addSubview:bottomLineLbl];
        
        self.redLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height-2.f, w, 2.f)];
        self.redLbl.text = @"";
        self.redLbl.backgroundColor = UIColorFromRGB(0xf5292c);
        [self addSubview:self.redLbl];
        [self selectButton:0];
    }
    return self;
}

- (void)buttonAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if (button.selected) {
        return;
    }
    
    NSInteger index = button.tag-kBaseButtonTag;
    [self selectButton:index];
}

- (void)selectButton:(NSInteger)index
{
    for (UIButton *btn in self.subviews) {
        if ([btn isKindOfClass:[UIButton class]]) {
            btn.selected = NO;
        }
    }
    
    UIButton *button = (UIButton *)[self viewWithTag:kBaseButtonTag+index];
    button.selected = YES;
    self.selectedIndex = index;
    [self onButton:button];
}

- (void)onButton:(UIButton *)button
{
    [UIView animateWithDuration:kAnimationSpeed animations:^{
        CGPoint point = self.redLbl.center;
        self.redLbl.center = CGPointMake(button.center.x, point.y);
    } completion:^(BOOL finished) {
        if (finished) {
            if ([self.delegate respondsToSelector:@selector(filterButtonIndex:)]) {
                [self.delegate filterButtonIndex:button.tag-kBaseButtonTag];
            }
        }
    }];
}

- (void)setTitleNum:(int)num atIndex:(NSInteger)index
{
    UIButton *button = (UIButton *)[self viewWithTag:kBaseButtonTag+index];
    NSString *string = [NSString stringWithFormat:@"%@(%d)", button.titleLabel.text, num];
    [button setTitle:string forState:UIControlStateNormal];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    _selectedIndex = selectedIndex;
    UIButton *button = (UIButton *)[self viewWithTag:kBaseButtonTag+selectedIndex];
    [UIView animateWithDuration:kAnimationSpeed animations:^{
        CGPoint point = self.redLbl.center;
        self.redLbl.center = CGPointMake(button.center.x, point.y);
    } completion:^(BOOL finished) {
    }];
}

@end
