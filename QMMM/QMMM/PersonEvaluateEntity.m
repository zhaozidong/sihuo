//
//  PersonEvaluateEntity.m
//  QMMM
//
//  Created by kingnet  on 14-11-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PersonEvaluateEntity.h"

@implementation PersonEvaluateEntity

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        id temp = [dict objectForKey:@"goods_id"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.goodsId = [temp longLongValue];
        }
        
        temp = [dict objectForKey:@"photo"];
        if (temp && [temp isKindOfClass:[NSArray class]]) {
            if ([temp count] > 0) {
                self.photo = [temp objectAtIndex:0];
            }
        }
        
        temp = [dict objectForKey:@"id"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.orderId = [temp longLongValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]) {
            self.orderId = [temp longLongValue];
        }
        
        temp = [dict objectForKey:@"rate_ts"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.comment_ts = [temp intValue];
        }
        
        temp = [dict objectForKey:@"comments"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            self.comments = [dict objectForKey:@"comments"];
        }

        temp = [dict objectForKey:@"nickname"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            self.nickName = [dict objectForKey:@"nickname"];
        }
        
        temp = [dict objectForKey:@"rate"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.rate = [temp intValue];
        }
    }
    return self;
}

- (NSString *)getRateImgName
{
    switch (self.rate) {
        case 1:
            return @"comment_bad";
        case 2:
            return @"comment_normal";
        case 3:
            return @"comment_good";
        default:
            return @"";
    }
}

@end
