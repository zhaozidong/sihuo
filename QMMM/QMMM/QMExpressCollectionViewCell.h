//
//  QMExpressCollectionViewCell.h
//  QMMM
//
//  Created by hanlu on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMExpressCollectionViewCellIdentifier;

@protocol QMExpressCallBtnDelegate ;

@interface QMExpressCollectionViewCell : UICollectionViewCell {
    NSDictionary * _infoDict;
    NSString * _phoneNum;
}

@property (nonatomic, strong) UIImageView *bkgImageView;

@property (nonatomic, strong) UILabel * title;

@property (nonatomic, strong) UIImageView * imageView;

@property (nonatomic, strong) UIImageView * flagImageView;

@property (nonatomic, strong) UIImageView * selectImageView;

@property (nonatomic, strong) UILabel * expressPhoneNum;

@property (nonatomic, strong) UILabel * sepratorLine;

@property (nonatomic, strong) UIButton * callBtn;

@property (nonatomic, weak) id<QMExpressCallBtnDelegate> delegate;

- (void)updateUI:(NSDictionary *)dict;

@end

@protocol QMExpressCallBtnDelegate <NSObject>

- (void)didExpressCallBtnClick:(NSString *)phoneNum;

@end
