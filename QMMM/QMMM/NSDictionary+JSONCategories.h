//
//  NSDictionary+JSONCategories.h
//  SDKDemo
//
//  Created by kingnet  on 14-7-11.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSONCategories)
+(NSDictionary*)dictionaryWithContentsOfJSONURLString:
(NSString*)urlAddress;
-(NSData*)toJSON;
-(NSString *)toString;
@end
