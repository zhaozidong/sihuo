//
//  ChooseCityView.h
//  QMMM
//  选择城市，省市区三级联动
//  Created by kingnet  on 14-9-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ChooseCityBlock)(int provinceID, int cityID, int districtID, NSString *result);

@interface ChooseCityView : UIView

@property (nonatomic, copy) ChooseCityBlock chooseCityBlock;

+ (void)showInView:(UIView *)containerView provinceId:(int)provinceId cityId:(int)cityId  distritId:(int)distritId completion:(ChooseCityBlock)competion;

@end
