//
//  NoCommentTipCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-10.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "NoCommentTipCell.h"

@interface NoCommentTipCell ()

@property (weak, nonatomic) IBOutlet UILabel *tipLbl;
@end
@implementation NoCommentTipCell

+ (NoCommentTipCell *)tipCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"NoCommentTipCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.tipLbl.textColor = kLightBlueColor;
    self.tipLbl.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTip:(NSString *)tip
{
    if (tip && tip.length > 0) {
        self.tipLbl.text = tip;
    }
}

@end
