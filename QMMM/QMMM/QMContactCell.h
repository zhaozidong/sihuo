//
//  QMContactCell.h
//  QMMM
//
//  Created by hanlu on 14-10-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMContactCellIdentifier;

@class QMNumView;

@interface QMContactCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView * headImageView;

@property (nonatomic, strong) IBOutlet UILabel * contactLabel;

@property (nonatomic, strong) IBOutlet QMNumView * numView;

@property (nonatomic, strong) IBOutlet UIImageView * moreImageView;

+ (id)cellForContact;

- (void)setUnreadCount:(NSUInteger)num;

@end

