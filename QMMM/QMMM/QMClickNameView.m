//
//  QMClickNameVIew.m
//  QMMM
//
//  Created by kingnet  on 14-10-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMClickNameVIew.h"

@implementation QMClickNameView
@synthesize height = _height;
@synthesize width = _width;

static const int kLabelIncent = 5;
- (id)init
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self _initView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self _initView];
    }
    return self;
}

- (void)_initView
{
    self.backgroundColor = [UIColor clearColor];
    label_ = [[UILabel alloc]initWithFrame:CGRectZero];
    label_.font = [UIFont systemFontOfSize:13.f];
    label_.backgroundColor = [UIColor clearColor];
    label_.textColor = UIColorFromRGB(0xfe595a);
    label_.numberOfLines = 1;
    label_.lineBreakMode = NSLineBreakByTruncatingMiddle;
    [self addSubview:label_];
    
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer * tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nameLabelClicked)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGestureRecognizer];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (label_.text) {
        CGSize size = [label_.text sizeWithFont:label_.font];
        CGRect frame = self.frame;
        if (_height == 0) {
            _height = 40.f;
        }
        if (_width == 0) {
            self.frame = CGRectMake(frame.origin.x, frame.origin.y, size.width+kLabelIncent*2, _height);
            label_.frame = CGRectMake(kLabelIncent, (_height-size.height)/2.0, size.width, size.height);
        }
        else{
            self.frame = CGRectMake(frame.origin.x, frame.origin.y, _width, _height);
            label_.frame = CGRectMake(kLabelIncent, (_height-size.height)/2.0, _width-2*kLabelIncent, size.height);
        }
    }
}

- (void)setName:(NSString *)name
{
    _name = name;
    label_.text = _name;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (float)width
{
    _width = self.frame.size.width;
    return _width;
}

- (void)setWidth:(float)width
{
    _width = width;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (float)minWidth
{
    CGSize size = [@"..." sizeWithFont:label_.font];
    return size.width;
}

- (void)nameLabelClicked
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickNameViewWithUid:)]) {
        [self.delegate clickNameViewWithUid:self.userId];
    }
}

@end
