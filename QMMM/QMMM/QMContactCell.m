//
//  QMContactCell.m
//  QMMM
//
//  Created by hanlu on 14-10-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMContactCell.h"
#import "QMNumView.h"

NSString * const kQMContactCellIdentifier = @"kQMContactCellIdentifier";

@implementation QMContactCell

+ (id)cellForContact
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMContactCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _numView.hidden = YES;
    
    self.headImageView.layer.cornerRadius = self.headImageView.bounds.size.width / 2;
    self.headImageView.layer.masksToBounds = YES;
    
    self.moreImageView.image = [UIImage imageNamed:@"arrow_more"];
    self.moreImageView.hidden = NO;
    
    [_contactLabel setTextColor:UIColorFromRGB(0x4f4f4f)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUnreadCount:(NSUInteger)num
{
    [_numView setNum:num];
    
    [self setNeedsLayout];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.contentView.frame = self.bounds;
    
    CGRect frame = _headImageView.frame;
    frame.origin.y = self.contentView.frame.origin.y + (self.contentView.frame.size.height - _headImageView.frame.size.height) / 2;
    _headImageView.frame = frame;
    
    CGSize textSize = [_contactLabel.text sizeWithFont:_contactLabel.font];
    frame = _contactLabel.frame;
    frame.origin.y = self.contentView.frame.origin.y;
    frame.size.height = self.contentView.frame.size.height;
    frame.size.width = textSize.width;
    _contactLabel.frame = frame;
    
    frame = _numView.frame;
    frame.origin.x = _contactLabel.frame.origin.x + _contactLabel.frame.size.width + 5;
    frame.origin.y = self.contentView.frame.origin.y + (self.contentView.frame.size.height - _numView.frame.size.height) / 2;
    _numView.frame = frame;
    
    frame = _moreImageView.frame;
    frame.origin.x = self.contentView.frame.origin.x + self.contentView.frame.size.width - _moreImageView.frame.size.width - 8;
    frame.origin.y = self.contentView.frame.origin.y + (self.contentView.frame.size.height - _moreImageView.frame.size.height) / 2;
    _moreImageView.frame = frame;
}

@end
