//
//  QMFilterConditionView.h
//  QMMM
//
//  Created by kingnet  on 15-3-17.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class QMFilterConditionView;
@protocol QMFilterConditionViewDelegate <NSObject>

- (void)conditionView:(QMFilterConditionView *)conditionView conditions:(NSArray *)conditions priceMin:(float)priceMin priceMax:(float)priceMax;

@end
@interface QMFilterConditionView : UIView

@property (nonatomic, weak) id<QMFilterConditionViewDelegate> delegate;

+ (QMFilterConditionView *)filterConditionView;

+ (CGFloat)viewHeight;

//清空view上之前的选择
- (void)clearView;

@end
