//
//  UserProtocolView.h
//  QMMM
//
//  Created by kingnet  on 14-11-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserProtocolViewDelegate <NSObject>

- (void)clickUserProtocol;

@end

@interface UserProtocolView : UIView

@property (nonatomic, assign) BOOL isCheckUserPro; //是否勾选上了用户协议

@property (nonatomic, weak) id<UserProtocolViewDelegate> delegate;

+ (UserProtocolView *)protocolView;

+ (CGSize)viewSize;

@end
