//
//  QMClickNameVIew.h
//  QMMM
//
//  Created by kingnet  on 14-10-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QMClickNameViewDelegate <NSObject>

- (void)clickNameViewWithUid:(uint64_t)userId;

@end

@interface QMClickNameView : UIView
{
    UILabel *label_;
}

@property (nonatomic, strong) NSString *name; //设置姓名

@property (nonatomic, assign) uint64_t userId; //用户id

@property (nonatomic, assign) float height;

@property (nonatomic, assign) float width;

@property (nonatomic, weak) id<QMClickNameViewDelegate> delegate;

- (float)minWidth;

@end
