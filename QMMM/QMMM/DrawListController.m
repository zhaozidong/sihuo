//
//  DrawListController.m
//  QMMM
//
//  Created by kingnet  on 14-9-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "DrawListController.h"
#import "DrawListCell.h"
#import "FinanceDataInfo.h"
#import "FinanceHandler.h"
#import "MJRefresh.h"
#import "FinanceDao.h"
#import "MJRefreshConst.h"
#import "QMFailPrompt.h"
#import "TempFlag.h"
#import "WithdrawController.h"
#import "DrawListTopView.h"
#import "HongbaoRulesViewController.h"
#import "UIScrollView+UzysCircularProgressPullToRefresh.h"

@interface DrawListController ()<UITableViewDataSource, UITableViewDelegate, DrawListTopViewDelegate>
@property (nonatomic) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *drawList;
@property (nonatomic, strong) QMFailPrompt *failPrompt;
@property (nonatomic, strong) DrawListTopView *topView;

@end

@implementation DrawListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([kWithdrawFrom isEqualToString:kBalance]) {
        self.navigationItem.title = @"私货现金";
    }
    else{
        self.navigationItem.title = @"红包";
    }
    
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(withdrawAction:) title:@"提现" size:CGSizeMake(44.f, 44.f) imageName:nil];
    
    if ([kWithdrawFrom isEqualToString:kBalance]) {
        NSMutableArray *array = [[FinanceDao sharedInstance]queryAll];
        self.drawList = [NSMutableArray arrayWithArray:array];
    }
    else{
        self.drawList = [NSMutableArray array];
    }
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.topView];
    
    [self updateTopView];
    
    NSString *tip = @"";
    if ([kWithdrawFrom isEqualToString:kBalance]) {
        tip = @"还没有收支明细";
    }
    else{
        tip = @"还没有收到过红包";
    }
    self.failPrompt =[[QMFailPrompt alloc]initWithFailType:QMFailTypeNOData labelText:tip buttonType:QMFailButtonTypeNone buttonText:nil];
    self.failPrompt.frame=CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(processWithdrawSuccess:) name:kWithdrawSuccessNotify object:nil];
    
    [self setupRefresh];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  
    [self updateTopView];
}

- (void)withdrawAction:(id)sender
{
    if ([kWithdrawFrom isEqualToString:kBalance]) {
        //提现
        WithdrawController *controller = [[WithdrawController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else{//红包提现
        FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
        if (financeInfo.hongbao < 100) {
            NSString *tip = [NSString stringWithFormat:@"红包余额还差%.2f元就能提现了，邀请好友可获得更多红包哟~", (100-financeInfo.hongbao)];
            [AppUtils showAlertMessage:tip];
        }
        else{
            WithdrawController *controller = [[WithdrawController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

- (void)updateTopView
{
    FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
    if ([kWithdrawFrom isEqualToString:kBalance]) {
        [self.topView updateUI:financeInfo isBalance:YES];
    }
    else{
        [self.topView updateUI:financeInfo isBalance:NO];
    }
}

#pragma mark - Pull Refresh

/**
 *  集成刷新控件
 */
- (void)setupRefresh
{
    //添加下拉refresh header
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshActionHandler:^{
        [weakSelf headerRereshing];
    }];
    
    [self.tableView addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRereshing];
    }];
    
    [self.tableView triggerPullToRefresh];
    
}

/**
 移除footerView
 **/
- (void)removeFooterView
{
    if (self.tableView.footer.isRefreshing) {
        [self.tableView.footer endRefreshing];
    }
    [self.tableView removeFooter];
}

- (void)headerRereshing
{
    DrawListController __weak * weakSelf = self;
    
    [[FinanceHandler sharedInstance]getDrawList:0 success:^(id obj) {
        NSArray *array = (NSArray *)obj;
        //停止刷新
        if ([weakSelf.tableView showPullToRefresh]) {
            [weakSelf.tableView stopRefreshAnimation];
        }
        
        //更新界面
        [weakSelf.drawList removeAllObjects];
        [weakSelf.drawList addObjectsFromArray:array];
        if (weakSelf.drawList.count == 0) {
            weakSelf.tableView.hidden = YES;
            if (nil == weakSelf.failPrompt.superview) {
                [weakSelf.view insertSubview:weakSelf.failPrompt belowSubview:self.topView];
            }
        }
        else{
            weakSelf.tableView.hidden = NO;
            if (weakSelf.failPrompt.superview) {
                [weakSelf.failPrompt removeFromSuperview];
            }
        }
        [weakSelf.tableView reloadData];
    } failed:^(id obj) {
        [weakSelf.tableView.footer endRefreshing];
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)footerRereshing
{
    FinanceDataInfo *dataInfo = [self.drawList lastObject];
    
    DrawListController __weak * weakSelf = self;
    [[FinanceHandler sharedInstance]getDrawList:dataInfo._id success:^(id obj) {
        NSArray *array = (NSArray *)obj;
        //停止刷新
        [weakSelf.tableView.footer endRefreshing];
        if (array.count == 0) {
            [weakSelf removeFooterView];
        }
        
        //更新界面
//        NSMutableArray *mbArray = [NSMutableArray array];
        [weakSelf.drawList addObjectsFromArray:array];
//        for (NSInteger i=0; i<array.count; i++) {
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:count+i inSection:0];
//            [mbArray addObject:indexPath];
//        }
//        [weakSelf.tableView insertRowsAtIndexPaths:mbArray withRowAnimation:UITableViewRowAnimationNone];
        [weakSelf.tableView reloadData];
//        NSInteger count = weakSelf.drawList.count;
//        [weakSelf.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    } failed:^(id obj) {
        [weakSelf.tableView.footer endRefreshing];
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

#pragma mark - Get Data
- (void)requestData:(int)startId resultBlock:(void(^)(void))resultBlock
{
    DrawListController __weak *weakSelf = self;
    [[FinanceHandler sharedInstance]getDrawList:startId success:^(id obj) {
        resultBlock();
        [weakSelf reloadWithData:(NSArray *)obj];
    } failed:^(id obj) {
        resultBlock();
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)reloadWithData:(NSArray *)data
{
    [self.drawList addObjectsFromArray:data];
    [self.tableView reloadData];
}

#pragma mark - UIView
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kMainTopHeight+80.f, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-(kMainTopHeight+25.f)) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.rowHeight = [DrawListCell rowHeight];
        _tableView.allowsSelection = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
    }
    return _tableView;
}

- (DrawListTopView *)topView
{
    if (!_topView) {
        _topView = [DrawListTopView topView];
        _topView.frame = CGRectMake(0, kMainTopHeight, CGRectGetWidth(self.view.frame), [DrawListTopView viewHeight]);
        _topView.backgroundColor = [UIColor whiteColor];
        if ([kWithdrawFrom isEqualToString:kHongBao]) {
            _topView.delegate = self;
        }
    }
    return _topView;
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.drawList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DrawListCell *cell = (DrawListCell *)[tableView dequeueReusableCellWithIdentifier:kDrawListCellIdentifier];
    if (!cell) {
        cell = [DrawListCell drawListCell];
    }
    FinanceDataInfo *dataInfo = [self.drawList objectAtIndex:indexPath.row];
    [cell updateUI:dataInfo];
    return cell;
}

#pragma mark - UITableView DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 14.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - TopViewDelegate
- (void)didTapRulesBtn
{
    HongbaoRulesViewController *controller = [[HongbaoRulesViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - NSNotification Handler
- (void)processWithdrawSuccess:(NSNotification *)notifaction
{
    [self headerRereshing];
}

@end
