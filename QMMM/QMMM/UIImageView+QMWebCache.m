//
//  UIImageView+QMWebCache.m
//  QMMM
//
//  Created by kingnet  on 14-10-7.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "UIImageView+QMWebCache.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation UIImageView(QMWebCache)

- (void)qm_setImageWithURL:(NSString *)URLStr placeholderImage:(UIImage *)placeholder completed:(QMWebImageCompletionBlock)completedBlock
{
    NSURL *url = [NSURL URLWithString:[AppUtils getAbsolutePath:URLStr]];
    [self sd_setImageWithURL:url placeholderImage:placeholder options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (completedBlock) {
            switch (cacheType) {
                case SDImageCacheTypeDisk:
                    completedBlock(image, error, QMImageCacheTypeDisk, imageURL);
                    break;
                case SDImageCacheTypeMemory:
                    completedBlock(image, error, QMImageCacheTypeDisk, imageURL);
                    break;
                default:
                    completedBlock(image, error, QMImageCacheTypeNone, imageURL);
                    break;
            }
        }
    }];
}

- (void)qm_setImageWithURL:(NSString *)URLStr placeholderImage:(UIImage *)placeholder
{
    NSURL *url = [NSURL URLWithString:[AppUtils getAbsolutePath:URLStr]];
    [self sd_setImageWithURL:url placeholderImage:placeholder options:SDWebImageRetryFailed];
}

- (void)qm_setImageWithURL:(NSString *)URLStr placeholderImage:(UIImage *)placeholder progress:(QMWebImageDownloaderProgressBlock)progressBlock completed:(QMWebImageCompletionBlock)completedBlock
{
    NSURL *url = [NSURL URLWithString:[AppUtils getAbsolutePath:URLStr]];
    [self sd_setImageWithURL:url placeholderImage:placeholder options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        if (progressBlock) {
            progressBlock(receivedSize, expectedSize);
        }
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if(completedBlock){
            switch (cacheType) {
                case SDImageCacheTypeDisk:
                    completedBlock(image, error, QMImageCacheTypeDisk, imageURL);
                    break;
                case SDImageCacheTypeMemory:
                    completedBlock(image, error, QMImageCacheTypeMemory, imageURL);
                    break;
                default:
                    completedBlock(image, error, QMImageCacheTypeNone, imageURL);
                    break;
            }
        }
    }];
}

@end
