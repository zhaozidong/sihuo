//
//  QMCategoryCellDetail.m
//  QMMM
//
//  Created by Derek on 15/3/17.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMCategoryCellDetail.h"
#import "UIImageView+QMWebCache.h"

@implementation QMCategoryCellDetail


-(id)initWithFrame:(CGRect)frame{
    self=[[[NSBundle mainBundle] loadNibNamed:@"QMCategoryCellDetail" owner:self options:nil] lastObject];
//    self.contentView.backgroundColor=[UIColor greenColor];
    return self;
}


- (void)awakeFromNib {
    // Initialization code

}

-(void)updateWithCategory:(CategoryEntity *)category{
    _lblTitle.text=category.name;
    
    [_imgView qm_setImageWithURL:category.icon placeholderImage:nil completed:^(UIImage *image, NSError *error, QMImageCacheType cacheType, NSURL *imageURL) {
        if (error) {
            NSLog(@"分类图片加载失败！");
        }
    }];
}


-(void)layoutSubviews{
    [super layoutSubviews];
    
    
//    CGPoint point=self.contentView.center;
//    _imgView.center=point;
//
//    _lblTitle.center=CGPointMake(point.x, point.y+40);
    
    _imgView.frame=CGRectMake((self.contentView.frame.size.width-43)/2, 5, 43, 43);
    
    
    
//    if (IS_IPHONE_5 && IS_IPHONE_6) {
//        _imgView.frame=CGRectMake(0, 0, 43, 43);
//    }else if(IS_IPHONE_6P){
//
//    }
    
//    _lblTitle.frame=CGRectMake((self.contentView.frame.size.width-CGRectGetWidth(_imgView.frame))/2, CGRectGetMaxY(_imgView.frame), CGRectGetWidth(_imgView.frame), 20);
    
    _lblTitle.frame=CGRectMake(0,CGRectGetMaxY(_imgView.frame),CGRectGetWidth(self.contentView.frame),20);

}


@end
