//
//  QMSegmentedControl.m
//  QMMM
//
//  Created by kingnet  on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMSegmentedControl.h"

@implementation QMSegmentedControl

- (id)initWithItems:(NSArray *)items target:(id)target selector:(SEL)selector
{
    self = [super initWithItems:items];
    if (self) {
        self.frame = CGRectMake(0, 0, 200.f, 30.f);
        self.tintColor = kRedColor;
        self.selectedSegmentIndex = 0;
        self.segmentedControlStyle = UISegmentedControlStylePlain;
        [self addTarget:target action:selector forControlEvents:UIControlEventValueChanged];
    }
    return self;
}

@end
