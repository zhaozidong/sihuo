//
//  NSString+JSONCategories.m
//  QMMM
//
//  Created by hanlu on 14-9-16.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@implementation NSString (JSONCategories)

- (NSArray*)toArray
{
    NSError* error = nil;
    NSData * data = [self dataUsingEncoding:NSUTF8StringEncoding];
    id object = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if([object isKindOfClass:[NSArray class]]) {
        return object;
    } else {
        return nil;
    }
}

- (NSDictionary *)toDict
{
    NSError* error = nil;
    NSData * data = [self dataUsingEncoding:NSUTF8StringEncoding];
    id object = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if([object isKindOfClass:[NSDictionary class]]) {
        return object;
    } else {
        return nil;
    }

}
//
//NSString* json = nil;
//NSError* error = nil;
//NSData *data = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
//json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//return json;

@end