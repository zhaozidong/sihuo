//
//  PersonPageHandler.m
//  QMMM
//
//  Created by kingnet  on 14-10-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PersonPageHandler.h"
#import "PersonPageApi.h"
#import "SoldOutGoodsEntity.h"
#import "GoodDataInfo.h"
#import "UserDataInfo.h"
#import "PersonEvaluateEntity.h"

@interface PersonPageHandler ()
{
    PersonPageApi *personPageApi_;
}
@end
@implementation PersonPageHandler

- (instancetype)init
{
    self = [super init];
    if (self) {
        personPageApi_ = [[PersonPageApi alloc]init];
    }
    return self;
}

+ (PersonPageHandler *)sharedInstance
{
    static PersonPageHandler *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)getSoldOutGoods:(uint64_t)goodsId uid:(NSString *)uid success:(SuccessBlock)success failed:(FailedBlock)failed
{
    NSString *s = @"";
    if (goodsId > 0) {
        s = [NSString stringWithFormat:@"%qu", goodsId];
    }
    
    [personPageApi_ getSoldOutGoods:s uid:uid context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSArray *dataArr = resultDic[@"d"];
            NSMutableArray *models = [NSMutableArray arrayWithCapacity:dataArr.count];
            for (NSDictionary *dic in dataArr) {
                SoldOutGoodsEntity *entity = [[SoldOutGoodsEntity alloc]initWithDictionary:dic];
                [models addObject:entity];
            }
            if (success) {
                success(models);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)getSellingGoods:(uint64_t)goodsId uid:(NSString *)uid success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [personPageApi_ getSellingGoods:goodsId uid:uid context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = [resultDic objectForKey:@"d"];
            NSMutableArray *mbArray = [NSMutableArray array];
            NSArray *dataArr = [dataDic objectForKey:@"list"];
            for (NSDictionary *dic in dataArr) {
                GoodsSimpleInfo *goodsInfo = [[GoodsSimpleInfo alloc]initWithDictionary:dic];
                [mbArray addObject:goodsInfo];
            }
            NSDictionary *dict = @{@"models": mbArray, @"pageNum":[dataDic objectForKey:@"page_num"]};
            if (success) {
                success(dict);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
    
}

- (void)getUserInfo:(NSString *)uid success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [personPageApi_ getUserInfo:uid context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = [resultDic objectForKey:@"d"];
            PersonPageUserInfo *userInfo = [[PersonPageUserInfo alloc]initWithDictionary:dataDic];
            if (success) {
                success(userInfo);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)addWatch:(NSString *)uid follow:(int)follow success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [personPageApi_ addWatch:uid follow:follow context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] ==0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)getWatchNumAndFlyNum:(NSString *)uid success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [personPageApi_ getWatchNumAndFlyNum:uid context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            NSDictionary *dataDic = resultDic[@"d"];
            int influence = [[dataDic objectForKey:@"influence"] intValue];
            int reliability = [[dataDic objectForKey:@"reliability"]intValue];
            NSString *influenceBeat = [dataDic objectForKey:@"influence_beat"];
            NSString *reliabilityBeat = [dataDic objectForKey:@"reliability_beat"];
            PersonInfluence *personInfluce = [[PersonInfluence alloc]initWithInfluence:influence reliability:reliability influenceBeat:influenceBeat reliabilityBeat:reliabilityBeat uid:uid];
            if (success) {
                success(personInfluce);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)addFriendId:(NSString *)uid success:(SuccessBlock)success failed:(FailedBlock)failed
{
    //统计添加好友次数
    [AppUtils trackCustomEvent:@"add_friend_event" args:nil];
    [personPageApi_ addFriendId:uid context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)getEvaluateList:(NSString *)uid orderId:(uint64_t)orderId success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [personPageApi_ getEvaluateList:uid orderId:orderId context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            NSArray *evaluates = resultDic[@"d"];
            NSMutableArray *models = [NSMutableArray array];
            for (NSDictionary *dic in evaluates) {
                PersonEvaluateEntity *entity = [[PersonEvaluateEntity alloc]initWithDictionary:dic];
                [models addObject:entity];
            }
            if (success) {
                success(models);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

@end
