//
//  NSString+Empty.m
//  QMMM
//
//  Created by kingnet  on 14-9-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "NSString+Empty.h"

@implementation NSString (Empty)

+ (BOOL)isEmptyWithSting:(NSString *)string
{
    return (!string || [string isKindOfClass:[NSNull class]]);
}

@end
