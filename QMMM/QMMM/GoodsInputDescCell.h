//
//  GoodsInputDescCell.h
//  QMMM
//
//  Created by kingnet  on 14-11-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonCellDelegate.h"

@protocol GoodsInputDescCellDelegate <NSObject>

//描述框失去焦点时
- (void)didEndEditing;

@end
@interface GoodsInputDescCell : UITableViewCell

+ (GoodsInputDescCell *)goodsInputDescCell;

@property (nonatomic, weak) id<CommonCellDelegate> commonDelegate;

@property (nonatomic, weak) id<GoodsInputDescCellDelegate> delegate;

@property (nonatomic, strong) NSString *desc;

@property (nonatomic, strong) NSIndexPath *indexPath;

+ (CGFloat)cellHeight;


-(void)setPlaceHolder:(NSString *)placeHolder andMaxLength:(NSUInteger)maxLength;


@end
