//
//  FinanceDataInfo.h
//  QMMM
//
//  Created by kingnet  on 14-9-27.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseEntity.h"

@class FMResultSet;

@interface FinanceDataInfo : BaseEntity

@property (nonatomic, assign) int _id;
@property (nonatomic, assign) int64_t uid; //用户id
@property (nonatomic, copy) NSString *desc; //交易描述
@property (nonatomic, assign) float amount; //账户余额
@property (nonatomic, copy) NSString *orderId; //账单id
@property (nonatomic, copy) NSString *payOrderId; //
@property (nonatomic, strong) NSString *comments;
@property (nonatomic, assign) int type;
@property (nonatomic, assign) int status;
@property (nonatomic, assign) int ts; //交易时间

- (instancetype)initWithDictionary:(NSDictionary *)dict;

- (id)initWithFMResultSet:(FMResultSet *)rs;

@end

@interface FinanceUserInfo : BaseEntity

@property (nonatomic, assign) float amount; //账户余额
@property (nonatomic, assign) float hongbao; //红包
@property (nonatomic, copy) NSString *payee; //收款人名称
@property (nonatomic, copy) NSString *accountNo; //收款人帐号

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
