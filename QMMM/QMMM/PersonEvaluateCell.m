//
//  PersonEvaluateCell.m
//  QMMM
//
//  Created by kingnet  on 14-11-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PersonEvaluateCell.h"
#import "PersonEvaluateEntity.h"
#import "UIImageView+QMWebCache.h"

NSString * const kPersonEvaluateCellIdentifier = @"kPersonEvaluateCellIdentifier";

@interface PersonEvaluateCell ()
@property (weak, nonatomic) IBOutlet UIImageView *goodsImageV;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *evaluateLbl;
@property (weak, nonatomic) IBOutlet UIImageView *leftImgV;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineHeightCst;
@end

@implementation PersonEvaluateCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"PersonEvaluateCell" bundle:nil];
}

+ (PersonEvaluateCell *)evaluateCell
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"PersonEvaluateCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.timeLbl.text = @"";
    self.timeLbl.textColor = kGrayTextColor;
    self.nameLbl.textColor = kRedColor;
    self.nameLbl.text = @"";
    self.evaluateLbl.text = @"";
    self.evaluateLbl.textColor = kDarkTextColor;
    self.evaluateLbl.numberOfLines = 0;
    self.evaluateLbl.preferredMaxLayoutWidth = kMainFrameWidth-108-10;
    UIImage *image= [UIImage imageNamed:@"goodsPlaceholder_middle"];
    self.goodsImageV.image = image;
    self.lineHeightCst.constant = 0.5;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(oneTapAction:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGesture];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(PersonEvaluateEntity *)evaluate
{
    if (_evaluate != evaluate) {
        _evaluate = evaluate;
        
        //刷新界面
        self.nameLbl.text = _evaluate.nickName;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_evaluate.comment_ts];
        self.timeLbl.text = [AppUtils stringFromDate:date];
        self.evaluateLbl.text = _evaluate.comments;
        
        self.leftImgV.image = [UIImage imageNamed:[_evaluate getRateImgName]];
        
        NSString *imageUrl = [AppUtils thumbPath:_evaluate.photo sizeType:QMImageQuarterSize];
        UIImage *image= [UIImage imageNamed:@"goodsPlaceholder_middle"];
        [self.goodsImageV qm_setImageWithURL:imageUrl placeholderImage:image];
    }
}

- (void)oneTapAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapEvaluateCell:)]) {
        [self.delegate didTapEvaluateCell:_evaluate];
    }
}

@end
