//
//  QMFilterItem.h
//  QMMM
//
//  Created by kingnet  on 15-3-16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMFilterItem : UIView

@property (nonatomic, copy) NSString * title;
@property (nonatomic, assign) NSInteger index;

+ (QMFilterItem *)filterItem;
//添加点击事件
- (void)addClickTarget:(id)target action:(SEL)action;
//旋转箭头
- (void)rotateArrowUp:(BOOL)isUp;
@end
