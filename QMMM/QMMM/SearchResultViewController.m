//
//  SearchResultViewController.m
//  QMMM
//
//  Created by kingnet  on 15-1-14.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "SearchResultViewController.h"
#import "MJRefreshConst.h"
#import "MJRefresh.h"
#import "GoodsDetailViewController.h"
#import "GoodsHandler.h"
#import "GoodEntity.h"
#import "UserInfoHandler.h"
#import "QMFailPrompt.h"
#import "GoodsItemCell.h"
#import "GoodsItem.h"

@interface SearchResultViewController ()<UITableViewDataSource, UITableViewDelegate, GoodsItemDelegate>

@property (nonatomic, strong) NSMutableArray *goodsItems; //存qmgoodscontroller
@property (nonatomic, assign) int numberOfItemsPerRow;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation SearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    self.goodsItems = [NSMutableArray array];
    
    self.numberOfItemsPerRow = 2;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self setupRefresh];
}

- (void)setControllerTitle:(NSString *)controllerTitle
{
    self.navigationItem.title = controllerTitle;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    [self.goodsItems removeAllObjects];
    self.goodsItems = nil;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.allowsSelection = NO;
        [_tableView registerClass:[GoodsItemCell class] forCellReuseIdentifier:kGoodsItemCellIdentifier];
        [self.view addSubview:_tableView];
        
        //布局
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[table]-(0)-|" options:0 metrics:0 views:@{@"table":_tableView}]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-(0)-[table]-(0)-|" options:0 metrics:0 views:@{@"table":_tableView}]];
    }
    return _tableView;
}

#pragma mark - Pull Refresh

/**
 *  集成刷新控件
 */
- (void)setupRefresh
{
    __weak typeof(self) weakSelf = self;
    [self.tableView addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRereshing];
    }];
}

//上拉
- (void)footerRereshing
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(loadMoreData)]) {
        [self.delegate loadMoreData];
    }
}

- (void)endFooterRereshing
{
    if (self.tableView.footer.isRefreshing) {
        [self.tableView.footer endRefreshing];
    }
}

- (void)addGoods:(NSArray *)array
{
    if (array.count>0) {
        for (GoodsSimpleInfo *goodsInfo in array) {
            GoodsItem *goodsItem = [GoodsItem goodsItem];
            goodsItem.goodsInfo = goodsInfo;
            goodsItem.delegate = self;
            [self.goodsItems addObject:goodsItem];
        }
        [self.tableView reloadData];
    }
}

- (void)removeAllGoods
{
    [self.goodsItems removeAllObjects];
    [self.tableView reloadData];
    self.tableView.scrollsToTop = YES;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.goodsItems.count > 0) {
        NSInteger nr = 1;
        if (self.goodsItems.count % self.numberOfItemsPerRow == 0) {
            nr = self.goodsItems.count / self.numberOfItemsPerRow;
        }
        else{
            nr = self.goodsItems.count / self.numberOfItemsPerRow +1;
        }
        return nr;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.goodsItems.count > 0) {
        return [GoodsItem itemSize].height + [GoodsItem margin];
    }

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.goodsItems.count >0) {
        GoodsItemCell *cell = (GoodsItemCell *)[tableView dequeueReusableCellWithIdentifier:kGoodsItemCellIdentifier];
        cell.items = [self itemsForRowAtIndexPath:indexPath];
        return cell;
    }
    return nil;
}

- (NSArray *)itemsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:self.numberOfItemsPerRow];
    
    NSUInteger startIndex = indexPath.row * self.numberOfItemsPerRow,
    endIndex = startIndex + self.numberOfItemsPerRow - 1;
    if (startIndex < self.goodsItems.count)
    {
        if (endIndex > self.goodsItems.count - 1)
            endIndex = self.goodsItems.count - 1;
        
        for (NSUInteger i = startIndex; i <= endIndex; i++)
        {
            [items addObject:(self.goodsItems)[i]];
        }
    }
    
    return items;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(topSectionView)]) {
        UIView *sectionView = [self.delegate topSectionView];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), CGRectGetHeight(sectionView.frame)+[GoodsItem margin])];
        view.backgroundColor = [UIColor whiteColor];
        [view addSubview:sectionView];
        return view;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(topSectionView)]) {
        return [self.delegate topSectionView].frame.size.height + [GoodsItem margin];
    }
    return [GoodsItem margin];
}

#pragma mark - GoodsItemDelegate
- (void)clickGoodsItem:(GoodsSimpleInfo *)goodsInfo
{
    GoodsDetailViewController *controller = [[GoodsDetailViewController alloc]initWithGoodsId:goodsInfo.gid];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
