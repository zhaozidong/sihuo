//
//  PersonNumIntroCell.h
//  QMMM
//
//  Created by kingnet  on 15-1-22.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class PersonPageUserInfo;

@class PersonInfluence;


@protocol PersonNumIntroDelegate <NSObject>

-(void)credibilityClicked;

-(void)influenceClicked;

-(void)tradingVolumeClicked;

-(void)goodRateClicked;

@end



@interface PersonNumIntroCell : UITableViewCell

@property(nonatomic, weak) id<PersonNumIntroDelegate> delegate;

+ (PersonNumIntroCell *)numIntroCell;

+ (CGFloat)cellHeight;

- (void)updateUI:(PersonPageUserInfo *)person;

- (IBAction)btnDidCredibility:(id)sender;

- (IBAction)btnDidInfluence:(id)sender;

- (IBAction)btnDidTradingVolume:(id)sender;

- (IBAction)btnDidClickGoodRate:(id)sender;


@end
