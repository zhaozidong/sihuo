//
//  QMFilterConditionView.m
//  QMMM
//
//  Created by kingnet  on 15-3-17.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMFilterConditionView.h"
#import "GoodEntity.h"
#import "UITextField+Additions.h"

//每块Lable
@interface ToggledLabel : UILabel
@property (nonatomic, assign) BOOL selected;
@end
@implementation ToggledLabel
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.userInteractionEnabled = YES;
    self.font = [UIFont systemFontOfSize:15.f];
    self.layer.borderWidth = 0.5;
    self.selected = NO;
    //添加点击手势
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickLblAction:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGesture];
}

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    if (_selected) {
        self.textColor = kRedColor;
        self.layer.borderColor = [kRedColor CGColor];
    }
    else{
        self.textColor = kDarkTextColor;
        self.layer.borderColor = [kBorderColor CGColor];
    }
}

- (void)clickLblAction:(UITapGestureRecognizer *)sender
{
    self.selected = !self.selected;
}

@end

@interface QMFilterConditionView()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *fromPriceTf;
@property (weak, nonatomic) IBOutlet UITextField *toPriceTf;

@end
@implementation QMFilterConditionView

+ (QMFilterConditionView *)filterConditionView
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"QMFilterConditionView" owner:self options:0];
    return [array lastObject];
}

+ (CGFloat)viewHeight
{
    return 295.f;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    NSArray *descArr = [GoodEntity usageDesc];
    for (int i=0; i<[descArr count]; i++) {
        UILabel *label = (UILabel *)[self viewWithTag:100+i];
        label.text = descArr[i];
    }
    self.fromPriceTf.delegate = self;
    self.toPriceTf.delegate = self;
    self.fromPriceTf.textColor = kDarkTextColor;
    self.toPriceTf.textColor = kDarkTextColor;
    self.fromPriceTf.layer.borderColor = [kBorderColor CGColor];
    self.fromPriceTf.layer.borderWidth = 0.5;
    self.toPriceTf.layer.borderColor = [kBorderColor CGColor];
    self.toPriceTf.layer.borderWidth = 0.5;
}

- (void)clearView
{
    self.fromPriceTf.text = @"";
    self.toPriceTf.text = @"";
    NSArray *descArr = [GoodEntity usageDesc];
    for (int i=0; i<[descArr count]; i++) {
        ToggledLabel *label = (ToggledLabel *)[self viewWithTag:100+i];
        label.selected = NO;
    }
}

- (IBAction)clickBtnAction:(id)sender {
    //获取价格
    float priceMin, priceMax;
    NSString *priceMinStr = self.fromPriceTf.text;
    //没有填最低价格
    if (nil == priceMinStr || priceMinStr.length == 0) {
        priceMin = -1;
    }
    else{
        priceMin = [priceMinStr floatValue];
        if (priceMin <= 0 ) {
            [AppUtils showAlertMessage:@"最低价格错误，需大于0"];
            return;
        }
    }
    //没有填最高价格
    NSString *priceMaxStr = self.toPriceTf.text;
    if (nil == priceMaxStr || priceMaxStr.length == 0) {
        priceMax = -1;
    }
    else{
        priceMax = [priceMaxStr floatValue];
        if (priceMax <= 0 ) {
            [AppUtils showAlertMessage:@"最高价格错误，需大于0"];
            return;
        }
    }
    
    if (priceMin!=-1 && priceMax!=-1 && priceMax<priceMin) {
        [AppUtils showAlertMessage:@"最高价格需大于最低价格"];
        return;
    }
    
    //获取选择的新旧程度
    NSArray *descArr = [GoodEntity usageDesc];
    NSMutableArray *conditions = [@[]mutableCopy];
    for (int i=0; i<[descArr count]; i++) {
        ToggledLabel *label = (ToggledLabel *)[self viewWithTag:100+i];
        if (label.selected) {
            [conditions addObject:[@(i)stringValue]];
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(conditionView:conditions:priceMin:priceMax:)]) {
        [self.delegate conditionView:self conditions:conditions priceMin:priceMin priceMax:priceMax];
    }
}

#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return [textField shouldChangeCharactersInRange:range replacementString:string];
}

@end
