//
//  SearchResultViewController.h
//  QMMM
//  只用于拉取商品、展示商品
//  Created by kingnet  on 15-1-14.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@class SearchResultViewController;
@protocol SearchResultViewCtlDelegate <NSObject>

- (void)loadMoreData;

@optional
- (UIView *)topSectionView; //最顶部的section view

@end
@interface SearchResultViewController : BaseViewController

@property (nonatomic, strong) NSString *controllerTitle;

@property (nonatomic, weak) id <SearchResultViewCtlDelegate> delegate;

- (void)endFooterRereshing;

//元素为GoodsSimpleInfo
- (void)addGoods:(NSArray *)array;

- (void)removeAllGoods;

@end
