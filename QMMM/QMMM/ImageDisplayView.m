//
//  ImageDisplayView.m
//  QMMM
//
//  Created by Shinancao on 14-9-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "ImageDisplayView.h"
#import "InstaFilters.h"
#import "UIImage+IF.h"
#import "UIButton+Addition.h"

#define kFilterImageViewTag 9999
#define kFilterCellHeight 72.0f 
#define kBlueDotImageViewOffset 25.0f
#define kBlueDotAnimationTime 0.2f
#define kFilterImageViewContainerViewTag 9998

@interface ImageDisplayView ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) IFVideoCamera *videoCamera;
@property (nonatomic, unsafe_unretained) IFFilterType currentType;
@property (nonatomic) UITableView *filtersTableView;
@property (nonatomic, strong) UIImageView *blueDotImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *reTakePhotoButton; //重拍按钮
@property (weak, nonatomic) IBOutlet UIButton *makeSureButton; //确定按钮
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewCst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightCst;

@end
@implementation ImageDisplayView

//必须要这样初始化，否则在init方法用到的frame都不一定是准确的
- (id)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"ImageDisplayView" owner:self options:nil] lastObject];
    if (self) {
        self.frame = frame;
        [self.reTakePhotoButton setTitle:@"返回" forState:UIControlStateNormal];
        [self.reTakePhotoButton setButtonType:BT_LeftImageRightTitle interGap:5.f];
        [self.makeSureButton setTitle:@"确定" forState:UIControlStateNormal];
        [self.makeSureButton setButtonType:BT_LeftImageRightTitle interGap:5.f];
//        self.videoCamera = [[IFVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPresetPhoto cameraPosition:AVCaptureDevicePositionBack highVideoQuality:YES];
//        self.videoCamera.gpuImageView.frame = CGRectMake(0, 0, kMainFrameWidth, kMainFrameWidth);
//        
//        self.topViewHeightCst.constant = kCameraTopHeight;
//        self.scrollViewCst.constant = kMainFrameWidth;
//        self.scrollView.bounces = NO;
//        self.scrollView.contentSize = CGSizeMake(kMainFrameWidth, kMainFrameWidth);
//        [self.scrollView addSubview:self.videoCamera.gpuImageView];
        
        self.filtersTableView = [[UITableView alloc] initWithFrame:CGRectMake(kMainFrameWidth/2 - 36, 300, 72, kMainFrameWidth) style:UITableViewStylePlain];
        self.filtersTableView.backgroundColor = [UIColor clearColor];
        self.filtersTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.filtersTableView.showsVerticalScrollIndicator = NO;
        self.filtersTableView.delegate = self;
        self.filtersTableView.dataSource = self;
        self.filtersTableView.transform	= CGAffineTransformMakeRotation(-M_PI/2);
        self.blueDotImageView = [[UIImageView alloc] initWithFrame:CGRectMake(-3, kBlueDotImageViewOffset + 4, 21, 11)];
        self.blueDotImageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"glCameraSelectedFilter" ofType:@"png"]];
        self.blueDotImageView.transform = CGAffineTransformMakeRotation(-M_PI/2);
        [self.filtersTableView addSubview:self.blueDotImageView];
        [self addSubview:self.filtersTableView];
        _currentType = 0;
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = self.filtersTableView.frame;
    float tmpH = CGRectGetHeight(self.frame)-kMainFrameWidth-kCameraTopHeight-90;
    frame.origin.y = CGRectGetHeight(self.frame)-CGRectGetHeight(frame)-90.f-(tmpH-CGRectGetHeight(frame))/2;
    self.filtersTableView.frame = frame;
}

- (void)setImage:(UIImage *)image
{
    //这个image要进行一个处理
    _image=nil;
    _image = image;
    
    self.videoCamera=nil;
    self.videoCamera = [[IFVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPresetPhoto cameraPosition:AVCaptureDevicePositionBack highVideoQuality:YES];
    self.videoCamera.gpuImageView.frame = CGRectMake(0, 0, kMainFrameWidth, kMainFrameWidth);
    
    self.topViewHeightCst.constant = kCameraTopHeight;
    self.scrollViewCst.constant = kMainFrameWidth;
    self.scrollView.bounces = NO;
    self.scrollView.contentSize = CGSizeMake(kMainFrameWidth, kMainFrameWidth);
    [self.scrollView addSubview:self.videoCamera.gpuImageView];
    
    self.videoCamera.rawImage = _image;
    [self.videoCamera switchFilter:_currentType];
}

- (IBAction)backAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapBack)]) {
        [self.delegate didTapBack];
    }
}

- (IBAction)finishAction:(id)sender {
    if (_deleteBeforeSave && _photoPath) {//先删除再保存
        @autoreleasepool {
            [self deleteFileAtPath:_photoPath];
            [self saveCurrentImage];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(finishUpdateImageFile)]) {
                [self.delegate finishUpdateImageFile];
            }
        }
    }else{//直接保存
        //写入文件
        @autoreleasepool {
            UIImage *resultImg = self.videoCamera.filterImage;
            NSData *imgData = UIImageJPEGRepresentation(resultImg, 0.9);
            UInt64 t = (UInt64)([[NSDate date] timeIntervalSince1970]*1000);
            NSString *timeSp = [NSString stringWithFormat:@"%llu", t];
            [self saveData:imgData imgName:[NSString stringWithFormat:@"%@.jpg", timeSp]];
            
            float scale = [UIScreen mainScreen].scale;
            resultImg = [resultImg scaleWithSize:CGSizeMake(70*scale, 70*scale)];
            imgData = UIImageJPEGRepresentation(resultImg, 1.0);
            NSString *smallImgPath = [self saveData:imgData imgName:[NSString stringWithFormat:@"%@_small.jpg", timeSp]];
            if (self.delegate && [self.delegate respondsToSelector:@selector(finishWithImageFile:)]) {
                [self.delegate finishWithImageFile:smallImgPath];
            }
        }
    }
}

-(BOOL)saveCurrentImage{

    NSString *fileName;
    NSString *smallPath;
    if ([_photoPath hasSuffix:@"_edit.jpg"]) {//已编辑过的图片
        NSString *strTemp=[_photoPath lastPathComponent];
        fileName=[strTemp substringToIndex:[strTemp length]-[@"_edit.jpg" length]];
        smallPath=[_photoPath stringByReplacingOccurrencesOfString:@"_edit.jpg" withString:@"_small.jpg"];
    }else if ([_photoPath hasSuffix:@".jpg"]) {//原图
        NSString *strTemp=[_photoPath lastPathComponent];
        fileName=[strTemp substringToIndex:[strTemp length]-4];
        smallPath=[_photoPath stringByReplacingOccurrencesOfString:@".jpg" withString:@"_small.jpg"];
    }
    NSLog(@"fileName=%@",fileName);
    
    UIImage *resultImg = self.videoCamera.filterImage;
    NSData *imgData = UIImageJPEGRepresentation(resultImg, 0.6);
    NSString *imagePath=[self saveData:imgData imgName:[NSString stringWithFormat:@"%@%@",fileName,@"_edit.jpg"]];
    
    NSLog(@"smallPath=%@",smallPath);
    
    [self deleteFileAtPath:smallPath];//small image
    float scale = [UIScreen mainScreen].scale;
    resultImg = [resultImg scaleWithSize:CGSizeMake(70*scale, 70*scale)];
    imgData = UIImageJPEGRepresentation(resultImg, 1.0);
    NSString *smallImgPath = [self saveData:imgData imgName:[NSString stringWithFormat:@"%@_small.jpg", fileName]];

    if (imagePath && smallImgPath) {
        return YES;
    }else{
        return NO;
    }
    return NO;
}


-(BOOL)deleteFileAtPath:(NSString *)strFilePath{
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    BOOL bRet = [fileMgr fileExistsAtPath:strFilePath];
    if (bRet) {
        NSError *err;
        [fileMgr removeItemAtPath:strFilePath error:&err];
        if (err) {
            NSLog(@"删除文件失败");
            return NO;
        }else{
            NSLog(@"删除文件成功");
            return YES;
        }
    }else{
        NSLog(@"要删除的文件不存在");
    }
    return YES;
}


#pragma mark - Save Image
-(NSString *)getTempPath:(NSString*)filename{
    
    NSString *tempPath = NSTemporaryDirectory();
    
    return [tempPath stringByAppendingPathComponent:filename];
}

//返回path
- (NSString *)saveData:(NSData *)imgData imgName:(NSString *)imgName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dirPath = [self getTempPath:@"cacheImg"];
    BOOL isDir = NO;
    BOOL isDirExist = [fileManager fileExistsAtPath:dirPath isDirectory:&isDir];
    if (!(isDirExist && isDir)) {
        isDirExist = [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    if (isDirExist) {
        NSString *filePath = [self getTempPath:[NSString stringWithFormat:@"cacheImg/%@", imgName]];
        if([fileManager createFileAtPath:filePath contents:imgData attributes:nil]){
            return filePath;
        }
        else{
            return @"";
        }
    }
    else{
        return @"";
    }
}

#pragma mark - Filters TableView Delegate & Datasource methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kFilterCellHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGRect cellRect = [tableView rectForRowAtIndexPath:indexPath];
    CGRect tempRect = self.blueDotImageView.frame;
    tempRect.origin.y = cellRect.origin.y + kBlueDotImageViewOffset;
    
    [UIView animateWithDuration:kBlueDotAnimationTime animations:^() {
        self.blueDotImageView.frame = tempRect;
    }completion:^(BOOL finished){
        // do nothing
    }];
    
    self.currentType = (int)[indexPath row];
    
    if (self.currentType==0) {//旋转90°，保存图片
        if (_delegate && [_delegate respondsToSelector:@selector(rotateImage:)]) {

            
            UIImage *resultImg = self.videoCamera.filterImage;
            [_delegate rotateImage:resultImg];
        }
        return;
    }else if(self.currentType==1){//原图
        if (_delegate && [_delegate respondsToSelector:@selector(restoreOrginalImage)]) {
            _currentType=0;
            [_delegate restoreOrginalImage];
        }
        return;
    }
    
    [self.videoCamera switchFilter:(int)[indexPath row]-1];

    
    if (([indexPath row] != [[[tableView indexPathsForVisibleRows] objectAtIndex:0] row]) && ([indexPath row] != [[[tableView indexPathsForVisibleRows] lastObject] row])) {
        
        return;
    }
    
    if ([indexPath row] == [[[tableView indexPathsForVisibleRows] objectAtIndex:0] row]) {
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    } else {
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *filtersTableViewCellIdentifier = @"filtersTableViewCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: filtersTableViewCellIdentifier];
    UIImageView *filterImageView;
    UIView *filterImageViewContainerView;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:filtersTableViewCellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        filterImageView = [[UIImageView alloc] initWithFrame:CGRectMake(7.5, -7.5, 57, 72)];
        filterImageView.transform = CGAffineTransformMakeRotation(M_PI/2);
        filterImageView.tag = kFilterImageViewTag;
        
        filterImageViewContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 7, 57, 72)];
        filterImageViewContainerView.tag = kFilterImageViewContainerViewTag;
        [filterImageViewContainerView addSubview:filterImageView];
        
        [cell.contentView addSubview:filterImageViewContainerView];
    } else {
        filterImageView = (UIImageView *)[cell.contentView viewWithTag:kFilterImageViewTag];
    }
    
    switch ([indexPath row]) {
        case 0: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterRotate"];
            break;
        }
        case 1: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileNormal"];

            break;
        }
        case 2: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileAmaro"];
            
            break;
        }
        case 3: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileRise"];
            
            break;
        }
        case 4: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileHudson"];
            
            break;
        }
        case 5: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileXpro2"];
            
            break;
        }
        case 6: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileSierra"];
            
            break;
        }
        case 7: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileLomoFi"];
            
            break;
        }
        case 8: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileEarlybird"];
            
            break;
        }
        case 9: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileSutro"];
            
            break;
        }
        case 10: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileToaster"];
            
            break;
        }
        case 11: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileBrannan"];
            
            break;
        }
        case 12: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileInkwell"];
            
            break;
        }
        case 13: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileWalden"];
            
            break;
        }
        case 14: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileHefe"];
            
            break;
        }
        case 15: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileValencia"];
            
            break;
        }
        case 16: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileNashville"];
            
            break;
        }
        case 17: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTile1977"];
            
            break;
        }
        case 18: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileLordKelvin"];
           
            break;
        }
            
        default: {
            filterImageView.image = [UIImage imageNamed:@"DSFilterTileNormal"];
            
            break;
        }
    }
    
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 19;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

@end
