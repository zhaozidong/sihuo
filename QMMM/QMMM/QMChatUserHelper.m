//
//  QMChatUserHelper.m
//  QMMM
//
//  Created by hanlu on 14-10-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMChatUserHelper.h"
#import "QMSqlite.h"
#import "UserBasicInfo.h"
#import "QMDatabaseHelper.h"

@implementation QMChatUserHelper

- (void)setWorkThread:(NSThread *)aThread
{
    @synchronized(self) {
        if (_workThread != aThread) {
            _workThread = aThread;
        }
    }
}

- (NSArray *)loadChatUserList:(FMDatabase *)database
{
    NSMutableArray * list = [[NSMutableArray alloc] initWithCapacity:1];
    
    if(database) {
        @autoreleasepool {
            FMResultSet * resultSet = [database executeQuery:@"SELECT * FROM qmChatUserList;"];
            while ([resultSet next]) {
                UserBasicInfo * info = [[UserBasicInfo alloc] initWithFMResultSet:resultSet];
                [list addObject:info];
            }
        }
    }
    
    return list;

}

- (void)addChatUserList:(FMDatabase *)database list:(NSArray *)userList
{
    if(database && [userList count] > 0) {
         NSArray * array = [NSArray arrayWithArray:userList];
        for (UserBasicInfo * data in array) {
            [self updateChatUser:database userInfo:data];
        }
    }
}


- (void)updateChatUser:(FMDatabase *)database userInfo:(UserBasicInfo *)userInfo
{
    if(database && userInfo) {
        [database executeUpdateWithFormat:@"REPLACE INTO qmChatUserList (userId, nickname, icon, extData, remark) VALUES (%qu, %@, %@, %@, %@);", userInfo.userId, userInfo.nickname, userInfo.icon, @"", userInfo.remark];
    }
}

- (void)deleteChatUser:(FMDatabase *)database userId:(uint64_t)userId
{
    if(database && userId) {
        [database executeUpdateWithFormat:@"DELETE FROM qmChatUserList WHERE userId = %qu", userId];
    }
}

- (void)asyncLoadChatUserList:(contextBlock)context
{
    if(_workThread && context) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:bContext, nil];
        [self performSelector:@selector(threadLoadChatUserList:)
                     onThread:_workThread
                   withObject:array
                waitUntilDone:NO];
    }
    else {
        if (context) {
            context(nil);
        }
    }
}
- (void)threadLoadChatUserList:(NSArray *)params
{
    if(params && [params count] > 0) {
        contextBlock context = [params objectAtIndex:0];
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        NSArray * result = [self loadChatUserList:helper.database];
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(result);
            });
        }
    }

}

- (void)asyncAddChatUserList:(contextBlock)context list:(NSArray *)userList
{
    if(_workThread && userList) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:userList, bContext, nil];
        [self performSelector:@selector(threadAddChatUserList:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadAddChatUserList:(NSArray *)params
{
    if([params count] >= 1) {
        NSArray * data = [params objectAtIndex:0];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self addChatUserList:helper.database list:data];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}

- (void)asyncUpdateChatUser:(contextBlock)context userInfo:(UserBasicInfo *)userInfo
{
    if(_workThread && userInfo) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:userInfo, bContext, nil];
        [self performSelector:@selector(threadUpdateChatUser:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadUpdateChatUser:(NSArray *)params
{
    if(params && [params count] >= 1) {
        UserBasicInfo * userInfo = (UserBasicInfo *)[params objectAtIndex:0];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self updateChatUser:helper.database userInfo:userInfo];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
        
    }
}

- (void)asyncDeleteChatUser:(contextBlock)context userId:(uint64_t)userId
{
    if(_workThread && userId) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(userId), bContext, nil];
        [self performSelector:@selector(threadDeleteChatUser:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
-(void)threadDeleteChatUser:(NSArray *)params
{
    if(params && [params count] > 0) {
        uint64_t userId = [[params objectAtIndex:0] unsignedLongLongValue];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self deleteChatUser:helper.database userId:userId];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
        
    }
}

@end
