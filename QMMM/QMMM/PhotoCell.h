//
//  PhotoCell.h
//  QMMM
//
//  Created by Shinancao on 14-9-21.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kPhotoCellIdentifier;

typedef void(^PhotoCellBlock)(void);

@interface PhotoCell : UICollectionViewCell

@property (nonatomic, strong) UIImage *image;

@property (nonatomic, copy) NSString *filePath;

//是否出现添加按钮
@property (nonatomic, assign) BOOL isHaveAddBtn;

//是否是主图
@property (nonatomic, assign) BOOL isMainPhoto;

@property (nonatomic, copy) PhotoCellBlock addPhotoBlock;

@property (nonatomic, assign) float progress; //上传进度

+(CGSize)cellSize;

- (void)prepareUpload; //准备上传

- (void)uploadSuccess; //上传成功

- (void)uploadFailed; //上传失败

@end
