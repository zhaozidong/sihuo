//
//  OrderDetailRefundForSellerCell.m
//  QMMM
//
//  Created by kingnet  on 15-3-12.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "OrderDetailRefundForSellerCell.h"
#import "OrderDataInfo.h"
#import "NSDate+Category.h"

@interface OrderDetailRefundForSellerCell ()
@property (weak, nonatomic) IBOutlet UIButton *rejectBtn;

@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;

@property (weak, nonatomic) IBOutlet UILabel *tipLbl;

@end

@implementation OrderDetailRefundForSellerCell

+ (OrderDetailRefundForSellerCell *)refundForSellderCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"OrderDetailRefundForSellerCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.tipLbl.text = @"";
    self.tipLbl.numberOfLines = 0;
    self.tipLbl.preferredMaxLayoutWidth = kMainFrameWidth-54.f;
    UIImage * image = [UIImage imageNamed:@"gray_border_btn_bkg"];
    [self.rejectBtn setBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(10, 7, 10, 7)] forState:UIControlStateNormal];
    [self.rejectBtn setTitleColor:kGrayTextColor forState:UIControlStateNormal];
    [self.rejectBtn addTarget:self action:@selector(rejectAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage * image1 = [UIImage imageNamed:@"border_btn_bkg"];
    [self.agreeBtn setBackgroundImage:[image1 resizableImageWithCapInsets:UIEdgeInsetsMake(10, 7, 10, 7)] forState:UIControlStateNormal];
    [self.agreeBtn setTitleColor:kRedColor forState:UIControlStateNormal];
    [self.agreeBtn addTarget:self action:@selector(agreeAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}

- (void)rejectAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapRejectRefund)]) {
        [self.delegate didTapRejectRefund];
    }
}

- (void)agreeAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapAgreeRefund)]) {
        [self.delegate didTapAgreeRefund];
    }
}

- (void)updateUI:(OrderDataInfo *)orderInfo
{
    NSInteger dayNum = [[NSDate dateWithTimeIntervalSince1970:orderInfo.apply_ts] distanceInDaysToDate:[NSDate dateWithTimeIntervalSince1970:orderInfo.sys_ts]];
    NSString *days = [NSString stringWithFormat:@"%ld", (kRefundWaitDays-dayNum)];
    NSString *tips = [NSString stringWithFormat:@"买家申请退款，退款金额为：¥%.2f元。你还有%@天来处理请求，逾期将自动退款给买家。", orderInfo.refund_amount, days];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:tips];
    NSRange range = [tips rangeOfString:days options:NSBackwardsSearch];
    [attributedStr addAttributes:@{NSForegroundColorAttributeName:kRedColor} range:range];
    self.tipLbl.attributedText = attributedStr;
}

@end
