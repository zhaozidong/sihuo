//
//  QMSmartFreight.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMSmartFreight.h"

@implementation QMSmartFreight{
    UIImageView *imageView;
    UIButton *btnClose;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(id)init{
    self=[super init];
    if (self) {
        
        UIImage *img=[UIImage imageNamed:@"smartFreight"];
        imageView=[[UIImageView alloc] init];
        imageView.frame=CGRectMake(0, 0, img.size.width, img.size.height);
        imageView.image=img;
        [self addSubview:imageView];
        
//        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImageView:)];
//        [singleTap setNumberOfTapsRequired:1];
//        [self addGestureRecognizer:singleTap];
        
        
//        btnClose=[[UIButton alloc] init];
//        btnClose.frame=CGRectMake(0, 0, 30, 30);
//        [btnClose setImage:[UIImage imageNamed:@"smartFreight_close"] forState:UIControlStateNormal];
//        [btnClose addTarget:self action:@selector(btnDidClose:) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:btnClose];
        
        
//        self.backgroundColor=[UIColor clearColor];
//        self.backgroundColor=[UIColor colorWithRed:0.56f green:0.56f blue:0.56f alpha:0.5];
//        self.layer.opacity=0.5;
        
    }
    return self;
}

- (void)tapImageView:(UIGestureRecognizer *)gestureRecognizer{
    [self removeFromSuperview];
}

//- (void)btnDidClose:(id)sender{
//    
//    self.transform = CGAffineTransformIdentity;
//    [UIView animateWithDuration:0.30 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
//        self.transform = CGAffineTransformMakeScale(0.01, 0.01);
//    } completion:^(BOOL finished){
//        self.hidden = YES;
//        [self removeFromSuperview];
//    }];
////    [self removeFromSuperview];
//
//}


-(void)layoutSubviews{
    CGRect frame=imageView.frame;
    frame.origin.x=0;
    frame.origin.y =0;
    frame.size.width=kMainFrameWidth;
    frame.size.height=kMainFrameHeight;
    imageView.frame=frame;
//    btnClose.center=CGPointMake(imageView.frame.origin.x+imageView.frame.size.width-15, imageView.frame.origin.y+15);
}

@end
