//
//  ChangePwdCellControl.m
//  QMMM
//
//  Created by kingnet  on 14-10-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "ChangePwdCellControl.h"
#import "TimerButton.h"
#import "QMTextField.h"

@interface ChangePwdCellControl ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet QMTextField *oldPwdTf;
@property (weak, nonatomic) IBOutlet QMTextField *pwdTf;

@end

@implementation ChangePwdCellControl

- (instancetype)init
{
    if (self = [super init]) {
        self.cellsArray = [[NSBundle mainBundle]loadNibNamed:@"ChangePwdCells" owner:self options:0];
        self.oldPwdTf.delegate = self;
        self.pwdTf.delegate = self;
        UIImageView *pwdImageV1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"pwd_icon"]];
        self.oldPwdTf.leftView = pwdImageV1;
        UIImageView *pwdImageV2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"pwd_icon"]];
        self.pwdTf.leftView = pwdImageV2;
        self.oldPwdTf.attributedPlaceholder = [AppUtils placeholderAttri:@"请输入旧密码"];
        self.pwdTf.attributedPlaceholder = [AppUtils placeholderAttri:@"请输入新密码"];
    }
    return self;
}

- (NSString *)pwd
{
    if (self.pwdTf.text == nil) {
        return @"";
    }
    return self.pwdTf.text;
}

- (NSString *)oldPwd
{
    if (self.oldPwdTf.text == nil) {
        return @"";
    }
    return self.oldPwdTf.text;
}

+ (CGFloat)rowHeight
{
    return 45.f;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


@end
