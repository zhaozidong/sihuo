//
//  NSThread+TTAdditions.m
//  ttnew
//
//  Copyright 2011年 SNDA Corporation. All rights reserved.
//

#import "NSThread+TTAdditions.h"

@implementation NSThread (TTAdditions)

- (id)threadValueForKey:(id)key
{
    NSMutableDictionary *dict = [[NSThread currentThread] threadDictionary];
    return [dict objectForKey:key];
}

- (void)setThreadValueForKey:(id)key value:(id)value
{
    NSMutableDictionary *dict = [[NSThread currentThread] threadDictionary];
    if (value)
        [dict setObject:value forKey:key];
    else
        [dict removeObjectForKey:key];
        
}

- (void)removeThreadValueForKey:(id)key
{
    NSMutableDictionary *dict = [[NSThread currentThread] threadDictionary];
    [dict removeObjectForKey:key];
}

- (const void *)threadValueForCFKey:(const void *)key;
{
    if (!key) 
        return nil;
    
    CFMutableDictionaryRef dict = (__bridge CFMutableDictionaryRef)[[NSThread currentThread] threadDictionary];
    return CFDictionaryGetValue(dict, key);
        
}

- (void)setThreadValueForCFKey:(const void *)key value:(const void *)value
{
    if (!key)
        return;
    
    CFMutableDictionaryRef dict = (__bridge CFMutableDictionaryRef)[[NSThread currentThread] threadDictionary];
    if (value)
        CFDictionarySetValue(dict, key, value);
    else
        CFDictionaryRemoveValue(dict, key);
}

- (void)removeThreadValueForCFKey:(const void *)key
{
    if (!key)
        return;
    
    CFMutableDictionaryRef dict = (__bridge CFMutableDictionaryRef)[[NSThread currentThread] threadDictionary];
    CFDictionaryRemoveValue(dict, key);
}

@end
