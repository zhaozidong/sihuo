//
//  AddressApi.h
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseHandler.h"

@interface AddressApi : BaseHandler

/**
 查询收货地址
 **/
- (void)getAddressList:(contextBlock)context;

/**
 新增收货地址
 **/
- (void)addNewAddress:(int)province
                 city:(int)city
             district:(int)district
              address:(NSString *)address   //详细地址
                 name:(NSString *)name   //收货人姓名
               mobile:(NSString *)mobile
            isDefault:(BOOL)isDefault
              context:(contextBlock)context;

/**
 更新收货地址
 **/
- (void)updateAddress:(int)id_
             province:(int)province
                 city:(int)city
             district:(int)district
              address:(NSString *)address   //详细地址
                 name:(NSString *)name   //收货人姓名
               mobile:(NSString *)mobile
            isDefault:(BOOL)isDefault
              context:(contextBlock)context;

/**
 删除收货地址
 **/
- (void)deleteAddress:(int)id_  //收货地址
              context:(contextBlock)context;

@end
