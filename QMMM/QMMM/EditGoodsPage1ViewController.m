//
//  EditGoodsPage1ViewController.m
//  QMMM
//
//  Created by kingnet  on 15-2-27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "EditGoodsPage1ViewController.h"
#import "TGCameraNavigationController.h"

#import "ButtonAccessoryView.h"
#import "GoodsPhotoBoxCell.h"
#import "GoodsInputDescCell.h"
#import "GoodsPriceCell.h"
#import "GoodsRecordCell.h"
#import "QMSubmitButton.h"
#import "GoodsHandler.h"
#import "QMQAViewController.h"

@interface EditGoodsPage1ViewController ()<UITableViewDataSource, UITableViewDelegate, GoodsPhotoCellDelegate, TGCameraDelegate, UIAlertViewDelegate>
{
    __block NSString *audioPath_;
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ButtonAccessoryView *accessoryView;
@property (nonatomic, strong) GoodsPhotoBoxCell *goodsPhotoBoxCell;
@property (nonatomic, strong) GoodsInputDescCell *goodsInputDescCell;
@property (nonatomic, strong) GoodsPriceCell *goodsPriceCell;
@property (nonatomic, strong) GoodsRecordCell *goodsRecordCell;

@property (nonatomic, strong) EditGoodsPage2ViewController *page2Controller;
@property (nonatomic, strong) GoodEntity *editGoods;
@property (nonatomic, strong) NSString *audioLocalPath;

@end

@implementation EditGoodsPage1ViewController

- (id)initWithType:(QMEditGoodsType)type
{
    self= [super init];
    if (self) {
        self.controllerType = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.controllerType == QMEditNewGoods) {
        self.navigationItem.title = @"发布商品";
    }
    else{
        self.navigationItem.title = @"编辑商品";
    }
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    self.navigationItem.rightBarButtonItems=[AppUtils createRightButtonWithTarget:self selector:@selector(tipsAction:) title:nil size:CGSizeMake(50, 50) imageName:@"btnTips"];
    
    audioPath_ = @"";
    _audioLocalPath = @"";
    
    _editGoods = [[GoodEntity alloc]init];
    
    [self setupViews];
    
    if (self.controllerType == QMUpdateGoods) {
        //拉取商品信息
        [self loadProductInfo];
    }
}

- (void)backAction:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:[AppUtils localizedCommonString:@"QM_Alert_Discard"] message:nil delegate:self cancelButtonTitle:[AppUtils localizedCommonString:@"QM_Button_Discard"] otherButtonTitles:[AppUtils localizedCommonString:@"QM_Button_GoOn"], nil];
    alertView.tag = 9999;
    [alertView show];
}

-(void)tipsAction:(id)sender{
    
    QMQAViewController *qa=[[QMQAViewController alloc] initWithType:QMQATypeDescribing];
    [self.navigationController pushViewController:qa animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.beforPhotos && self.beforPhotos.count>0) {
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.beforPhotos];
        [self addGoodsPhotos:array];
        self.beforPhotos = nil;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self registerObserver];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self removeObserver];
    [self.goodsRecordCell stopPlaying];
}

- (void)registerObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processKeyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)removeObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [nc removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)processKeyboardWillHide:(NSNotification *)notification
{
    self.canTapCloseKeyboard = NO;
    NSTimeInterval animationDuration = 0 ;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    [UIView animateWithDuration:animationDuration animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0;
        self.tableView.frame = f;
    }];
}

- (void)processKeyboardWillShow:(NSNotification *)notification
{
    self.canTapCloseKeyboard = YES;
    NSTimeInterval animationDuration = 0 ;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    [UIView animateWithDuration:animationDuration animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -20;
        self.tableView.frame = f;
    }];
}

- (void)setupViews
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStylePlain];
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kBorderColor;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.allowsSelection = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    [self.view addSubview:_tableView];
    
    //footer
    _tableView.tableFooterView = [self footerView];
    
    //键盘收回按钮
    _accessoryView = [[ButtonAccessoryView alloc]init];
    _accessoryView.originalFrame = CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds)-CGRectGetWidth(_accessoryView.frame)-5.f,CGRectGetHeight(self.view.frame), CGRectGetWidth(_accessoryView.frame), CGRectGetHeight(_accessoryView.frame));
    [self.view addSubview:_accessoryView];
}

- (void)submitAction:(id)sender
{
    if (self.controllerType == QMEditNewGoods) {
        [AppUtils trackCustomEvent:@"submit_publishGoodsBtn_clicked" args:nil];
    }
    
    //校验各项信息是否填完整了
    if ([self.goodsPhotoBoxCell.photoArray count]==0) {
        [AppUtils showAlertMessage:[AppUtils localizedProductString:@"QM_Alert_PublishGoods_NoPhoto"]];
        return;
    }
    
    if ([self.goodsInputDescCell.desc isEqualToString:@""]) {
        [AppUtils showAlertMessage:[AppUtils localizedProductString:@"QM_Alert_PublishGoods_NoDesc"]];
        return;
    }
    
    if (self.goodsPriceCell.price == 0.0) {
        [AppUtils showAlertMessage:[AppUtils localizedProductString:@"QM_Alert_PublishGoods_NoPrice"]];
        return;
    }
    
    if (NO == [self.goodsPhotoBoxCell isImgAllUploaded]) {
        [AppUtils showAlertMessage:@"稍等一下，还有图片在上传中"];
        return;
    }
    
    //拿到上传图片后得到的key
    GoodsPictureList *picList = [[GoodsPictureList alloc]initWithArray:[self.goodsPhotoBoxCell keysArray]];
    _editGoods.picturelist = picList;
    _editGoods.desc = self.goodsInputDescCell.desc;
    _editGoods.price = self.goodsPriceCell.price;
    _editGoods.msgAudioSeconds = self.goodsRecordCell.audioTime;
    if (_editGoods.msgAudioSeconds > 0) {
        //录音
        _editGoods.msgAudio = audioPath_;
    }
    else{
        _editGoods.msgAudio = @"";
    }
    
    if (nil == _page2Controller) {
        _page2Controller = [[EditGoodsPage2ViewController alloc]initWithType:self.controllerType goodsEntity:_editGoods];
        if (self.controllerType == QMUpdateGoods) {
            _page2Controller.updateGoodsBlock = self.updateGoodsBlock;
        }
    }
    if (_editGoods.msgAudioSeconds > 0) {
        _page2Controller.audioPath = _audioLocalPath;
    }
    [_page2Controller setGoodsEntity:_editGoods];
    _page2Controller.goodsPhotoArr = [self.goodsPhotoBoxCell bigImgLocalPath];
    [self.navigationController pushViewController:_page2Controller animated:YES];
}

- (UIView *)footerView
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_tableView.frame), 40)];
    view.backgroundColor = [UIColor clearColor];
    QMSubmitButton *submitBtn = [[QMSubmitButton alloc]initWithFrame:view.bounds];
    [view addSubview:submitBtn];
    submitBtn.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *views = NSDictionaryOfVariableBindings(submitBtn);
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[submitBtn(40)]-(0)-|" options:0 metrics:0 views:views];
    [view addConstraints:constraints];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:submitBtn attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:290]];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:submitBtn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    
    if (self.controllerType == QMEditNewGoods) {
        [submitBtn setTitle:@"确认其他信息并发布" forState:UIControlStateNormal];
    }
    else{
        [submitBtn setTitle:@"确认其他信息" forState:UIControlStateNormal];
    }
    [submitBtn addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    return view;
}

- (GoodsPhotoBoxCell *)goodsPhotoBoxCell
{
    if (!_goodsPhotoBoxCell) {
        _goodsPhotoBoxCell = [GoodsPhotoBoxCell goodsPhotoBoxCell];
        _goodsPhotoBoxCell.delegate = self;
        _goodsPhotoBoxCell.isShowMainPic=YES;
    }
    return _goodsPhotoBoxCell;
}

- (GoodsInputDescCell *)goodsInputDescCell
{
    if (!_goodsInputDescCell) {
        _goodsInputDescCell = [GoodsInputDescCell goodsInputDescCell];
        [_goodsInputDescCell setPlaceHolder:@"描述下购买的时间地点，商品的品牌、型号、材质、磨损维修情况等" andMaxLength:140];
    }
    return _goodsInputDescCell;
}

- (GoodsPriceCell *)goodsPriceCell
{
    if (!_goodsPriceCell) {
        _goodsPriceCell = [GoodsPriceCell goodsPriceCell];
    }
    return _goodsPriceCell;
}

- (GoodsRecordCell *)goodsRecordCell
{
    if (!_goodsRecordCell) {
        _goodsRecordCell = [GoodsRecordCell goodsRecordCell];
        __weak typeof(self) weakSelf = self;
        _goodsRecordCell.soundBlock = ^(NSString *audioPath, int audioTime){
            //上传音频
            weakSelf.audioLocalPath = audioPath;
            NSData *data = [NSData dataWithContentsOfFile:audioPath];
            [weakSelf uploadRecord:data];
        };
    }
    return _goodsRecordCell;
}

#pragma mark - UITableViewDatasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return [GoodsPhotoBoxCell cellHeight];
    }
    else if (indexPath.row == 1){
        if (IS_IPHONE_4) {
            return (kMainFrameHeight-kMainTopHeight-[GoodsPhotoBoxCell cellHeight]-[GoodsPriceCell cellHeight]-50-[GoodsRecordCell cellHeight]);
        }
        else if (IS_IPHONE_6P) {
            return [GoodsInputDescCell cellHeight]+100;
        }
        else if (IS_IPHONE_6) {
            return [GoodsInputDescCell cellHeight]+50;
        }
        else{
            return [GoodsInputDescCell cellHeight];
        }
    }
    else if (indexPath.row == 2){
        return [GoodsPriceCell cellHeight];
    }
    else if (indexPath.row == 3){
        float height = (kMainFrameHeight-kMainTopHeight-[GoodsPhotoBoxCell cellHeight]-[GoodsInputDescCell cellHeight]-[GoodsPriceCell cellHeight]-60);
        if (IS_IPHONE_4) {
            return [GoodsRecordCell cellHeight];
        }
        else if (IS_IPHONE_6P) {
            return height-100;
        }
        else if (IS_IPHONE_6) {
            return height-50;
        }
        else{
            return height;
        }
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return self.goodsPhotoBoxCell;
    }
    else if (indexPath.row == 1){
        return self.goodsInputDescCell;
    }
    else if (indexPath.row == 2){
        return self.goodsPriceCell;
    }
    else if (indexPath.row == 3){
        return self.goodsRecordCell;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 3) {
        cell.backgroundColor = UIColorFromRGB(0xededed);
    }
    else{
        cell.backgroundColor = [UIColor whiteColor];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Cell Delegate
- (void)showCamera
{
    TGCameraNavigationController *navigationController = [TGCameraNavigationController newWithCameraDelegate:self];
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark -
#pragma mark - TGCameraDelegate

- (void)cameraDidTakePhoto:(UIImage *)image
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraWillTakePhoto
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (int)maxNumberOfPhotos
{
    return kGoodsPhotoLimitNum - (int)self.goodsPhotoBoxCell.photoArray.count;
}

- (void)cameraDidOKWithPhotos:(NSArray *)photoArr
{
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        [weakSelf addGoodsPhotos:photoArr];
    }];
}

#pragma mark - Upload Data
- (void)addGoodsPhotos:(NSArray *)photos
{
    NSMutableArray *array = [NSMutableArray arrayWithArray:photos];
    [self.goodsPhotoBoxCell addPhotos:array isServerPhoto:NO];
}

- (void)uploadRecord:(NSData *)data
{
    [[GoodsHandler shareGoodsHandler]upLoadAudio:data success:^(id obj) {
        audioPath_ = (NSString *)obj;
    } failed:^(id obj) {
        DLog(@"upload audio error:%@", obj);
    }];
}

//更新商品信息处用到
- (void)loadProductInfo
{
    __weak typeof(self) weakSelf = self;
    [AppUtils showProgressMessage:@"加载中"];
    [[GoodsHandler shareGoodsHandler]getGoodsInfoWithId:self.goodsId success:^(id obj) {
        [AppUtils dismissHUD];
        
        GoodEntity *entity = (GoodEntity *)obj;
        //音频路径
        audioPath_ = entity.msgAudio;
        [weakSelf.editGoods assignValue:entity];
        
        //下载音频
        [[GoodsHandler shareGoodsHandler]downloadAudio:entity.msgAudio complete:nil];
        
        [weakSelf updateUI:entity];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)updateUI:(GoodEntity *)entity
{
    //图片
    NSMutableArray *array = [NSMutableArray arrayWithArray:entity.picturelist.rawArray];
    [self.goodsPhotoBoxCell addPhotos:array isServerPhoto:YES];
    //价格
    self.goodsPriceCell.price = entity.price;
    //描述
    self.goodsInputDescCell.desc = entity.desc;
    //录音
    [self.goodsRecordCell updateUI:entity.msgAudioSeconds audioPath:entity.msgAudio];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 9999) {
        if (buttonIndex==0) {
            [super backAction:nil];
        }
    }
}

@end
