//
//  GoodsDescriptionCell.h
//  QMMM
//
//  Created by kingnet  on 14-9-18.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsDetailBaseCell.h"

@class GoodEntity;

extern NSString * const kGoodsDescriptionCellIdentifier;

@interface GoodsDescriptionCell : GoodsDetailBaseCell

+ (GoodsDescriptionCell *)cellAwakeFromNib;

- (void)updateUI:(GoodEntity *)goodEntity;

@end
