//
//  OrderDetailGoodsDescCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderDetailGoodsDescCell.h"
#import "OrderDataInfo.h"
#import "UIImageView+QMWebCache.h"

NSString * const kOrderDetailGoodsDescCellIdentifier = @"kOrderDetailGoodsDescCellIdentifier";

@interface OrderDetailGoodsDescCell ()
@property (weak, nonatomic) IBOutlet UIImageView *goodsImgV;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *numLbl;

@end

@implementation OrderDetailGoodsDescCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"OrderDetailGoodsDescCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.descLbl.textColor = kDarkTextColor;
    self.priceLbl.textColor = kDarkTextColor;
    self.numLbl.textColor = kDarkTextColor;
    self.descLbl.numberOfLines = 3;
    self.descLbl.preferredMaxLayoutWidth = kMainFrameWidth-120;
    self.descLbl.text = @"";
    self.priceLbl.text = @"";
    self.numLbl.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(OrderDataInfo *)orderInfo
{
    self.descLbl.text = orderInfo.goodsDesc;
    self.priceLbl.text = [NSString stringWithFormat:@"价格：%.2f", orderInfo.goodsPrice];
    self.numLbl.text = [NSString stringWithFormat:@"数量：%d", orderInfo.buy_num];
    NSString * imagePath = [AppUtils thumbPath:orderInfo.goodsImage sizeType:QMImageQuarterSize];
    [self.goodsImgV qm_setImageWithURL:imagePath placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_middle"]];
}

+ (CGFloat)cellHeight
{
    return 110.f;
}

@end
