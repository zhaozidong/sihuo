//
//  QMSelectExpressViewController.m
//  QMMM
//
//  Created by hanlu on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMSelectExpressViewController.h"
#import "QMSelectExpressCell.h"
#import "QMFillExpressNumCell.h"
#import "QMFillExpressNumCell2.h"
#import "OrderDataInfo.h"
#import "GoodsHandler.h"
#import "QMSelectExpressCoViewController.h"
#import "BackNavigationController.h"

@interface QMSelectExpressViewController ()<UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UIAlertViewDelegate> {
    BOOL _keyboardVisible;
    NSString * _orderId;
    __weak UIViewController * _popViewController;
    __block NSString *_expressCo; //快递公司名称
}

@property (nonatomic, strong) UITableView * tableView;

@property (nonatomic, strong) QMFillExpressNumCell *expressNumCell;

@property (nonatomic, strong) QMFillExpressNumCell2 *expressNumCell2;

@end

@implementation QMSelectExpressViewController

- (id)initWithOrderId:(NSString *)orderId
{
    self = [super init];
    if(self) {
        _orderId = orderId;
    }
    return self;
}

- (void)setPopViewController:(UIViewController *)viewController
{
    _popViewController = viewController;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)initNavBar
{
    self.title = [AppUtils localizedProductString:@"QM_Text_Fill_Express"];
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(confirmDeliverGoods:) title:nil size:CGSizeMake(44, 44) imageName:@"navRight_finish_btn"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initNavBar];
    
    [self.view addSubview:self.tableView];
    
    _expressCo = @"";
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 90+14+kMainTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.scrollEnabled = NO;
    }
    return _tableView;
}

- (QMFillExpressNumCell *)expressNumCell
{
    if(!_expressNumCell){
        _expressNumCell = [QMFillExpressNumCell cellForFillExpressNum];
    }
    return _expressNumCell;
}

- (QMFillExpressNumCell2 *)expressNumCell2
{
    if (!_expressNumCell2) {
        _expressNumCell2 = [QMFillExpressNumCell2 cell2ForFillExpressNum];
    }
    return _expressNumCell2;
}

- (void)returnViewController
{
    if(_popViewController) {
        [self.navigationController popToViewController:_popViewController animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)confirmDeliverGoods:(id)sender
{
    [AppUtils closeKeyboard];
    
    if ([_expressCo isEqualToString:@""] ) {
        [AppUtils showAlertMessage:@"请选择快递公司"];
        return;
    }
    
    NSString *expressNum = self.expressNumCell2.expressNum;
    
    if ([expressNum isEqualToString:@""]) {
        [AppUtils showAlertMessage:@"请填写快递单号"];
        return;
    }
    
    if (![AppUtils checkExpressNumber:expressNum]) {
        [AppUtils showAlertMessage:@"快递单号格式不正确"];
        return;
    }
    
    [AppUtils showProgressMessage:@"正在发货处理..."];
    
    __weak typeof(self) weakSelf = self;
    [[GoodsHandler shareGoodsHandler] deliverGoods:_orderId expressCo:_expressCo express_num:expressNum success:^(id result) {
        [AppUtils dismissHUD];
        [weakSelf deliverGoodsFinish];
//        [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshOrderState object:_orderId];
    } failed:^(id obj) {
       
        [AppUtils showErrorMessage:(NSString *)obj];
        
        //[self performSelector:@selector(returnViewController) withObject:nil afterDelay:2.0];
    }];
}

- (void)deliverGoodsFinish
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:[AppUtils localizedProductString:@"QM_Alert_DeliverGoodsOK"] message:nil delegate:self cancelButtonTitle:[AppUtils localizedCommonString:@"QM_Button_IKnow"] otherButtonTitles:nil];
    alertView.tag = 1000;
    [alertView show];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        return self.expressNumCell;
    }
    else if (indexPath.row == 1){
        return self.expressNumCell2;
    }
    return nil;
}

#pragma mark - UITableViewDeletegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        QMSelectExpressCoViewController * viewController = [[QMSelectExpressCoViewController alloc] initWithNibName:@"QMSelectExpressCoViewController" bundle:nil];
        __weak typeof(self) weakSelf = self;
        viewController.selectBlock = ^(NSString *expressCo, NSString *expressName){
            _expressCo = expressCo;
            [weakSelf.expressNumCell setExpressName:expressName];
        };
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 14.f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)backAction:(id)sender
{
    NSString *expressNum = self.expressNumCell2.expressNum;
    if ([expressNum isEqualToString:@""]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = (BackNavigationController *)self.navigationController;
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[AppUtils localizedCommonString:@"QM_Alert_Discard"] message:nil delegate:self cancelButtonTitle:[AppUtils localizedCommonString:@"QM_Button_Discard"] otherButtonTitles:[AppUtils localizedCommonString:@"QM_Button_GoOn"], nil];
        alert.tag = 1001;
        [alert show];
    }
}

#pragma mark - 手势返回
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer == self.navigationController.interactivePopGestureRecognizer) {
        [self backAction:nil];
        return NO;
    }
    return YES;
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex == buttonIndex) {
        
        self.navigationController.interactivePopGestureRecognizer.delegate = (BackNavigationController *)self.navigationController;
        [self.navigationController popViewControllerAnimated:self];
    }
}

@end
