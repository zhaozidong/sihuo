//
//  QMFillExpressNumCell.m
//  QMMM
//
//  Created by Shinancao on 14/11/22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMFillExpressNumCell.h"

NSString * const kQMFillExpressNumCellIdentifier = @"kQMFillExpressNumCellIdentifier";

@interface QMFillExpressNumCell ()
@property (weak, nonatomic) IBOutlet UILabel *expressLbl;//选择的

@end
@implementation QMFillExpressNumCell

+ (id)cellForFillExpressNum
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMFillExpressNumCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib {
    // Initialization code
    self.expressLbl.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setExpressName:(NSString *)expressName
{
    self.expressLbl.text = expressName;
}

@end
