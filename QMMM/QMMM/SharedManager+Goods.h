//
//  SharedManager+Goods.h
//  QMMM
//
//  Created by kingnet  on 15-2-28.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "SharedManager.h"

@class GoodEntity;
@interface SharedManager (Goods)

- (void)sharedWithType:(QMSharedType)type goodsEntity:(GoodEntity *)goodsEntity;

@end
