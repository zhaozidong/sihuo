//
//  GoodsPriceCell.m
//  QMMM
//
//  Created by kingnet  on 15-3-2.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "GoodsPriceCell.h"

@interface GoodsPriceCell ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end
@implementation GoodsPriceCell
@synthesize price = _price;

+ (GoodsPriceCell *)goodsPriceCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"GoodsPriceCell" owner:self options:0];
    return [array lastObject];
}

+ (CGFloat)cellHeight
{
    return 45.f;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.textField.font = [UIFont systemFontOfSize:15.f];
    self.textField.textColor = kDarkTextColor;
    self.textField.attributedPlaceholder = [AppUtils placeholderAttri:@"给商品定个合理的价格(包含运费)"];
    self.textField.keyboardType = UIKeyboardTypeDecimalPad;
    self.textField.delegate = self;
}

- (void)setPrice:(float)price
{
    NSString *text = [NSString stringWithFormat:@"%.2f", price];
    self.textField.text = text;
}

- (float)price
{
    NSString *text = self.textField.text;
    if (text && text.length >0) {
        return [text floatValue];
    }
    else{
        return 0.0;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    BOOL isHaveDot = NO;
    if ([textField.text rangeOfString:@"."].location != NSNotFound) {
        isHaveDot = YES;
    }
    
    //如果输入的是小数点
    if ([string isEqualToString:@"."]) {
        //第一位是小数点时不可以
        if (textField.text == nil || [textField.text isEqualToString:@""]) {
            return NO;
        }
        else{
            //如果输入过小数点则不能再输入了
            if (isHaveDot) {
                return NO;
            }
        }
    }
    
    //如果输入过小数点，则最多输入11位，没有输入过小数点，则最多输入8位
    NSUInteger tempLength = 8;
    if (isHaveDot) {
        tempLength = 11;
    }
    
    if (newLength <= tempLength) {
        //判读有小数点时
        if (isHaveDot) {
            NSRange ran=[textField.text rangeOfString:@"."];
            NSInteger tt=range.location-ran.location;
            if (tt<=2) {
                return YES;
            }
            else{
                return NO;
            }
        }
        else{
            return YES;
        }
    }
    //输入长度不合法
    else{
        return NO;
    }
}

@end
