//
//  UIGoodsFilterView.h
//  QMMM
//
//  Created by hanlu on 14-9-19.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum _EDefaultSortType {
    EST_DEFAULT,
    EST_ASC,
    EST_DESC,
}EDefaultSortType;

@interface QMGoodsFilterView : UIView<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView * tableView;

@property (nonatomic, strong) IBOutlet UITextField * startTextField;

@property (nonatomic, strong) IBOutlet UITextField * endTextField;

//@property (nonatomic, strong) IBOutlet UIButton * btnOk;

- (void)releaseFocus;

- (EDefaultSortType)sortType;

@end
