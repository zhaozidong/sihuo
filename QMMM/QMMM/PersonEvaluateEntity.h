//
//  PersonEvaluateEntity.h
//  QMMM
// 个人主页的评价列表
//  Created by kingnet  on 14-11-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseEntity.h"

@interface PersonEvaluateEntity : BaseEntity

@property (nonatomic, assign) uint64_t goodsId;

@property (nonatomic, copy) NSString *comments; //评价内容

@property (nonatomic, assign) int comment_ts; //评价时间

@property (nonatomic, strong) NSString *photo; //商品图片

@property (nonatomic, assign) uint64_t orderId; //订单id

@property (nonatomic, copy) NSString *nickName; //买家昵称

@property (nonatomic, assign) int rate; //1差评 2中评 3好评

- (id)initWithDictionary:(NSDictionary *)dict;

- (NSString *)getRateImgName;

@end
