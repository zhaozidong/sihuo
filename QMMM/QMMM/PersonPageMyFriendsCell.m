//
//  PersonPageMyFriendsCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-11.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PersonPageMyFriendsCell.h"

@interface PersonPageMyFriendsCell (){
    PersonPageUserInfo *_userInfo;
}
@property (weak, nonatomic) IBOutlet UILabel *friendsTipLbl;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;

@end

@implementation PersonPageMyFriendsCell

+ (PersonPageMyFriendsCell *)myFriendsCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"PersonPageMyFriendsCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    [self.chatBtn setTitle:@"私聊" forState:UIControlStateNormal];
    [self.chatBtn setTitleColor:UIColorFromRGB(0x5381db) forState:UIControlStateNormal];
    UIImage *image1 = [UIImage imageNamed:@"blue_border_btn_bkg"];
    [self.chatBtn setBackgroundImage:[image1 stretchableImageWithLeftCapWidth:10 topCapHeight:8] forState:UIControlStateNormal];
    self.friendsTipLbl.textColor = UIColorFromRGB(0x9198a3);
    self.friendsTipLbl.text = @"你们是朋友";
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(friendsLabelDidClick)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [self.friendsTipLbl addGestureRecognizer:tapGesture];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight
{
    return 100.f;
}

- (IBAction)chatAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickChat)]) {
        [self.delegate clickChat];
    }
}

-(void)updateUIWithUserInfo:(PersonPageUserInfo *)userInfo{
    _userInfo=userInfo;
    if (userInfo.relation.isFromApp) {
        self.friendsTipLbl.text=@"TA来自私货";
    }else{
        self.friendsTipLbl.text=@"TA来自你的通讯录>";
    }
}

-(void)friendsLabelDidClick{
    if (!_userInfo.relation.isFromApp && _delegate && [_delegate respondsToSelector:@selector(clickFriendsFrom:)]) {
        [_delegate clickFriendsFrom:_userInfo];
    }
}
@end
