//
//  NoPasteTextField.m
//  QMMM
//
//  Created by kingnet  on 15-4-2.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "NoPasteTextField.h"

@implementation NoPasteTextField

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(paste:)) {
        return NO;
    }
    return [super canPerformAction:action withSender:sender];
}

@end
