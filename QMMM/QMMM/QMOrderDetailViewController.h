//
//  QMOrderDetailViewController.h
//  QMMM
//  订单详情界面
//  Created by hanlu on 14-9-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMOrderDetailViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView * tableView;

- (id)initWithData:(BOOL)bseller orderId:(NSString *)orderId;

@end
