//
//  QMGoodsController.m
//  QMMM
//
//  Created by hanlu on 14-9-18.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsController.h"
#import "GoodEntity.h"
#import "QMGoodsActionCell.h"
#import "QMGoodsTitleCell.h"
#import "QMGoodsFavorlistCell.h"
#import "QMGoodsAudioView.h"
#import "QMGoodsImageCellV2.h"
#import "QMPagePhotoView.h"
#import "QMGoodsLocationCell.h"
#import "GoodsListDescCell.h"
#import "GoodsDetailDescCell.h"

@interface QMGoodsController () <QMGoodsAudioDelegate, QMGoodsActionDelegate, ImageBrowerViewDelegate, QMRoundHeadViewDelegate,RelationViewTitleDelegate> {
    NSUInteger  _rowTotalCount;
    
    NSInteger _HeaderIndex; //头部title
    NSInteger _ImageIndex;  //图片
    NSInteger _TextIndex;   //文本描述
    NSInteger _ActionIndex; //action
    NSInteger _LikeListIndex;//赞列表
    NSInteger _CommentCount, _CommentIndexStart, _CommentIndexEnd;//评论
    NSInteger _LocationIndex; //时间和地点
}

@property (nonatomic, strong) GoodsListDescCell *descSizeCell;

@property (nonatomic, strong) GoodsDetailDescCell *detailDescCell;

@end

@implementation QMGoodsController

@synthesize goodsInfo = _goodsInfo;
@synthesize delegate = _delegate;

- (id)init
{
    self = [super init];
    if (self) {
        _showFull = NO;
    }
    return self;
}

- (id)initWithGoodsInfo:(GoodEntity *)goodsInfo
{
    self = [self init];
    if(self) {
        [self setGoodsInfo:goodsInfo];
    }
    return self;
}

- (void)setGoodsInfo:(GoodEntity *)goodsInfo
{
    if(_goodsInfo != goodsInfo) {
        _goodsInfo = goodsInfo;
        
        _goodsInfo.desc = [_goodsInfo.desc stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        _rowTotalCount = 0;
    }
}

- (void)setShowFull:(BOOL)showFull
{
    if (_showFull != showFull) {
        _showFull = showFull;
        _rowTotalCount = 0;
    }
}

- (GoodsListDescCell *)descSizeCell
{
    if (!_descSizeCell) {
        _descSizeCell = [GoodsListDescCell goodsListDescCell];
        _descSizeCell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _descSizeCell.frame = CGRectMake(0, 0, kMainFrameWidth-16, CGRectGetHeight(_descSizeCell.frame));
    }
    return _descSizeCell;
}

- (GoodsDetailDescCell *)detailDescCell
{
    if (!_detailDescCell) {
        _detailDescCell = [GoodsDetailDescCell goodsDetailDescCell];
        _detailDescCell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _detailDescCell.frame = CGRectMake(0, 0, kMainFrameWidth, CGRectGetHeight(_detailDescCell.frame));
    }
    return _detailDescCell;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //是否计算索引
    if (_rowTotalCount > 0)
        return _rowTotalCount;
    
    //初始化
    _HeaderIndex = -1;
    _ImageIndex = -1;
    _TextIndex = -1;
    _ActionIndex = -1;
    _LikeListIndex = -1;
    _CommentIndexStart = -1;
    _CommentIndexEnd = -1;
    _CommentCount = 0;
    _LocationIndex = -1;
    
    // 头像、昵称
    NSInteger result = 0;
    _HeaderIndex = result;
    result ++;
    
    //Image
    _ImageIndex = result;
    result ++;
    
    //Text
    _TextIndex = result;
    result ++;
    
    //Location
    if (_showFull) {
        _LocationIndex = result;
        result ++;
    }
    
    //Action
    _ActionIndex = result;
    result ++;
    
//    if(_showFull) {
//        
//        //favor
//        _LikeListIndex = result;
//        result ++;
//        
//        //comment
//        _CommentCount = _goodsInfo.commentCounts;
//        if (_CommentCount > 0) {
//            _CommentIndexStart = result;
//            result += _CommentCount;
//            _CommentIndexEnd = result - 1;
//        }
//    }
    
    _rowTotalCount = result;
    
    return _rowTotalCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    
    if(row == _HeaderIndex) {
        return [self cellforGoodsTitle:tableView cellForRowAtIndexPath:indexPath];
    }
    
    if(row == _ImageIndex) {
        return [self cellforGoodsImage:tableView cellForRowAtIndexPath:indexPath];
    }
    
    if(row == _TextIndex) {
        return [self cellforGoodsText:tableView cellForRowAtIndexPath:indexPath];
    }
    
    if(row == _ActionIndex) {
         return [self cellforGoodsAction:tableView cellForRowAtIndexPath:indexPath];
    }
    
    if (row == _LocationIndex) {
        return [self cellforGoodsLocation:tableView cellForRowAtIndexPath:indexPath];
    }
    
//    if(row == _LikeListIndex) {
//        return [self cellforGoodsFavorList:tableView cellForRowAtIndexPath:indexPath];
//    }
    
//    if(row >= _CommentIndexStart && row <= _CommentIndexEnd) {
//        return [self cellforGoodsComment:tableView row:row - _CommentIndexStart];
//    }
    
    return nil;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = indexPath.row;

    if(_HeaderIndex == row) {
        return [self heightForGoodsTitleCell:_goodsInfo];
    }
    
    if(_ImageIndex == row) {
        return [self heightForGoodsImageCell:_goodsInfo];
    }
    
    if(_TextIndex == row) {
        return [self heightForGoodsDescCell:_goodsInfo];
    }
    
    if(_ActionIndex == row) {
        return [self heightForGoodsActionCell:_goodsInfo];
    }
    
    if (_LocationIndex == row) {
        return [self heightForGoodsLocationCell:_goodsInfo];
    }
//    if(_LikeListIndex == row) {
//        return [self heightForGoodsFavorlistCell:_goodsInfo];
//    }
//    
//    if(row >= _CommentIndexStart && row <= _CommentIndexEnd) {
//        return [self heightForGoodsCommentCell:_goodsInfo];
//    }
    
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1 || indexPath.row == 2) {
        //跳到详情界面
        [_delegate didGoodsImageClick:self curIndex:0];
    }
}

#pragma mark - cell

- (UITableViewCell *)cellforGoodsLocation:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMGoodsLocationCell * cell = [tableView dequeueReusableCellWithIdentifier:kGoodsLocationCellIdentifier];
    if (nil == cell) {
        cell = [QMGoodsLocationCell locationCell];
    }
    [cell updateUI:_goodsInfo];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (UITableViewCell *)cellforGoodsTitle:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMGoodsTitleCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMGoodsTitleCellIndentifier];
    if(nil == cell) {
        cell = [QMGoodsTitleCell cellAwakeFromNib];
    }
    cell.forbidIndent = _showFull ? YES : NO;
    cell.delegate = self;
    cell.delegateRelation=self;
    [cell updateUI:_goodsInfo showFull:_showFull];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (UITableViewCell *)cellforGoodsImage:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMGoodsImageCellV2 * cell = [tableView dequeueReusableCellWithIdentifier:kQMGoodsImageCellV2Indentifier];
    if(nil == cell) {
        cell = [QMGoodsImageCellV2 cellAwakeFromNib];
    }
    cell.forbidIndent = _showFull ? YES : NO;
    cell.delegate = self;
    [cell setShowFull:_showFull];
    [cell updateUI:_goodsInfo];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (UITableViewCell *)cellforGoodsText:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_showFull) {
        [self.detailDescCell updateUI:_goodsInfo];
        [self.detailDescCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return self.detailDescCell;
    }
    else{
        GoodsListDescCell *cell = [tableView dequeueReusableCellWithIdentifier:kGoodsListDescCellIdentifier];
        if (nil == cell) {
            cell = [GoodsListDescCell goodsListDescCell];
        }
        [cell updateUI:_goodsInfo];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
}

- (UITableViewCell *)cellforGoodsAction:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMGoodsActionCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMGoodsActionCellIndentifier];
    if(nil == cell) {
        cell = [QMGoodsActionCell cellAwakeFromNib];
    }
    cell.forbidIndent =  _showFull ? YES : NO;
    [cell setDelegate:self];
    [cell updateUI:_goodsInfo showFull:_showFull];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

//- (UITableViewCell *)cellforGoodsFavorList:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    QMGoodsFavorlistCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMGoodsFavorlistCellIndentifier];
//    
//    [cell setDelegate:self];
//    [cell updateUI:_goodsInfo];
//    
//    return cell;
//}

- (UITableViewCell *)cellforGoodsComment:(UITableView *)tableView row:(NSInteger)row
{
    static NSString * identifier = @"identifier";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.contentView.backgroundColor = [UIColor redColor];
    
    return cell;
}

#pragma mark - cell height
- (CGFloat)heightForGoodsLocationCell:(GoodEntity *)goodsInfo
{
    return [QMGoodsLocationCell cellHeight];
}

- (CGFloat)heightForGoodsTitleCell:(GoodEntity *)goodsInfo
{
    return 50;
}

- (CGFloat)heightForGoodsDescCell:(GoodEntity *)goodsInfo
{
    if (_showFull) {
        [self.detailDescCell updateUI:_goodsInfo];
        [self.detailDescCell setNeedsLayout];
        [self.detailDescCell layoutIfNeeded];
        CGFloat height = [self.detailDescCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
        height += 5;
        return height;
    }
    else{
        [self.descSizeCell updateUI:_goodsInfo];
        [self.descSizeCell setNeedsLayout];
        [self.descSizeCell layoutIfNeeded];
        CGFloat height = [self.descSizeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
        height += 5;
        return height;
    }
}

- (CGFloat)heightForGoodsImageCell:(GoodEntity *)goodsInfo
{
//    NSUInteger height = 320;
//    if(_goodsInfo.orig_price > 0) {
//        return height + 58 - 4;
//    } else {
//        return height + 52 - 4;
//    }
    
    
//    if(_showFull) {
//        return 320;
//    } else if(_goodsInfo.orig_price > 0) {
//        return 320;
//    } else {
//        return 270 + 30;
//    }
    
//    NSLog(@"mainFrameWidth=%f",kMainFrameWidth);
//    NSLog(@"is Full=%d",_showFull);
    
    if (0 != goodsInfo.msgAudioSeconds) {//有声音
//        return 320;
        if (!_showFull) {//有边框
            return (kMainFrameWidth-2*15)+40*2;
        }else{
            return (kMainFrameWidth-2*15)+46*2;
        }
        //return (kMainFrameWidth-2*15)+43*2;
    }else{//无声音
//        return 305;
        if (!_showFull) {//有边框
            return (kMainFrameWidth-3*15)+40*2;
        }else{
            return (kMainFrameWidth-3*15)+46*2;
        }
        //return (kMainFrameWidth-3*15)+40*2;
    }
}

- (CGFloat)heightForGoodsActionCell:(GoodEntity *)goodsInfo
{
    return 36;
}

- (CGFloat)heightForGoodsFavorlistCell:(GoodEntity *)goodsInfo
{
    return 38;
}

- (CGFloat)heightForGoodsCommentCell:(GoodEntity *)goodsInfo
{
    return 50;
}

#pragma mark - QMGoodsHeadButtonDelegate

- (void)clickHeadViewWithUid:(uint64_t)userId
{
    if(_delegate && [_delegate respondsToSelector:@selector(didGoodsHeadImageClick:userId:)]) {
        [_delegate didGoodsHeadImageClick:self userId:userId];
    }
}


#pragma RelationViewTitleDelegate
-(void)clickRelationTitleWithId:(NSString *)friendId{
    uint64_t userId=[friendId intValue];
    if(_delegate && [_delegate respondsToSelector:@selector(didGoodsHeadImageClick:userId:)]) {
        [_delegate didGoodsHeadImageClick:self userId:userId];
    }
}

#pragma mark - QMGoodsAudioDelegate

- (void)didAudioClicked
{
    if(_delegate && [_delegate respondsToSelector:@selector(didGoodsAudioClick:)]) {
        [_delegate didGoodsAudioClick:self];
    }
}

#pragma mark - QMGoodsActionDelegate

- (void)didFavorBtnClicked
{
    //如果已经赞过就不做处理了
    if (self.goodsInfo.isFavor) {
        return;
    }
    if(_delegate && [_delegate respondsToSelector:@selector(didGoodsFavorClick:)]) {
        [_delegate didGoodsFavorClick:self];
    } 
}
- (void)didCommentBtnClicked
{
    if(_delegate && [_delegate respondsToSelector:@selector(didGoodsCommentClick:)]) {
        [_delegate didGoodsCommentClick:self];
    }
}

- (void)didShareBtnClicked
{
    if(_delegate && [_delegate respondsToSelector:@selector(didGoodsShareClick:)]) {
        [_delegate didGoodsShareClick:self];
    }
}

- (void)clickeImageAtIndex:(NSInteger)index
{
    if(_delegate && [_delegate respondsToSelector:@selector(didGoodsImageClick:curIndex:)]) {
        [_delegate didGoodsImageClick:self curIndex:index];
    }
}

#pragma mark - QMGoodsDescDelegate

- (void)didGoodsDesclick
{
    if(_delegate && [_delegate respondsToSelector:@selector(didGoodsImageClick:curIndex:)]) {
        [_delegate didGoodsImageClick:self curIndex:0];
    }
}

@end
