//
//  QMHttpClient.h
//  QMMM
//  HTTP网络请求
//  Created by kingnet  on 14-8-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

//HTTP REQUEST METHOD TYPE
typedef NS_ENUM(NSInteger, QMHttpRequestType){
    QMHttpRequestGet,
    QMHttpRequestPost,
    QMHttpRequestDelete,
    QMHttpRequestPut,
};

/**
 *  请求开始前预处理Block
 */
typedef void(^PrepareExecuteBlock)(void);

/**************** QMHttpClient  ***************/
@interface QMHttpClient : NSObject

+ (QMHttpClient *)defaultClient;
/**
 *  HTTP请求（GET、POST、DELETE、PUT）
 *
 *  @param path
 *  @param method     RESTFul请求类型
 *  @param parameters 请求参数
 *  @param prepare    请求前预处理块
 *  @param success    请求成功处理块
 *  @param failure    请求失败处理块
 **/
- (void)requestWithPath:(NSString *)url
                 method:(NSInteger)method
             parameters:(id)parameters
         prepareExecute:(PrepareExecuteBlock)prepare
                success:(void(^)(AFHTTPRequestOperation *operation, id responseObject))success
                failure:(void(^)(AFHTTPRequestOperation *operation, NSError *error))failure;

/**
 *  HTTP请求（HEAD）
 *
 *  @param path
 *  @param parameters
 *  @param success
 *  @param failure
 */
- (void)requestWithInHEAD:(NSString *)url
               parameters:(NSDictionary *)parameters
                  success:(void(^)(AFHTTPRequestOperation *operation))success
                  failure:(void(^)(AFHTTPRequestOperation *operation, NSError *error))failure;

//判断当前网络状态
- (BOOL)isConnectionAvailable;

/**
 上传单个文件
 @param path
 @param data 文件data
 @param contentType request的Content-Type
 @param success
 @param failure
 @param uploadProgressBlock 上传进度
 **/
- (void)uploadFileWithPath:(NSString *)path data:(NSData *)data contentType:(NSString *)contentType success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))uploadProgressBlock;

/**
 下载单个文件
 @param path
 @param success
 @param failure
 **/
- (void)downloadFileWithPath:(NSString *)path savename:(NSString *)name success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

@end
