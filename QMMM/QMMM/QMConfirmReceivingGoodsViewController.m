//
//  QMConfirmReceivingGoodsViewController.m
//  QMMM
//
//  Created by hanlu on 14-9-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMConfirmReceivingGoodsViewController.h"
#import "QMOrderDetailRightTextV2Cell.h"
#import "QMConfirmReceivingGoodsView.h"
#import "QMOrderDetailPayAmountCell.h"
#import "QMConfirmGoodsTitleCell.h"
#import "OrderDataInfo.h"
#import "GoodsHandler.h"

@interface QMConfirmReceivingGoodsViewController () <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate> {
    BOOL _keyboardVisible;
    NSString * _orderId;
    OrderDataInfo * _orderInfo;
    __weak UIViewController * _popViewController;
}

@end

@implementation QMConfirmReceivingGoodsViewController

- (id)initWithOrderId:(NSString *)orderId
{
    self = [super initWithNibName:@"QMConfirmReceivingGoodsViewController" bundle:nil];
    if (self) {
        // Custom initialization
        _orderId = orderId;
    }
    return self;
}

- (id)initWithOrderInfo:(OrderDataInfo *)orderData
{
    self = [super initWithNibName:@"QMConfirmReceivingGoodsViewController" bundle:nil];
    if (self) {
        // Custom initialization
        _orderInfo = orderData;
    }
    return self;
}

- (void)setPopViewController:(UIViewController *)viewController
{
    _popViewController = viewController;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)initNavBar
{
    self.title = [AppUtils localizedProductString:@"QM_Text_Accept_Goods"];
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(btnBackClick:)];
    
//    UILabel * titlelabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
//    titlelabel.backgroundColor = [UIColor clearColor];
//    titlelabel.textColor = kRedColor;
//    titlelabel.textAlignment = NSTextAlignmentCenter;
//    titlelabel.text = [AppUtils localizedProductString:@"QM_Text_Confirm_Receive_Goods"];
//    [self.navigationItem setTitleView:titlelabel];
}

- (void)registerObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processKeyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(processkeyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initNavBar];
    
    [self registerObserver];
    
    if(kIOS7) {
        _tableView.separatorInset = UIEdgeInsetsZero;
    }
    _tableView.separatorColor = UIColorFromRGB(0xd8dee6);
    _tableView.backgroundColor = kBackgroundColor;
    _tableView.tableFooterView = _footView;
    [_footView.confirmBtn addTarget:self action:@selector(confirmBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIPanGestureRecognizer * swipeGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    swipeGestureRecognizer.delegate = self;
    [_footView.commentTextView addGestureRecognizer:swipeGestureRecognizer];
    
    if(nil == _orderInfo) {
        
         _tableView.hidden = YES;
        
        [AppUtils showProgressMessage:@""];
        
        [[GoodsHandler shareGoodsHandler] getOrderDetail:2 orderId:_orderId success:^(id result) {
            [AppUtils dismissHUD];
            @synchronized(_orderInfo) {
                _orderInfo = result;
            }
            [_tableView setHidden:NO];
            [_tableView reloadData];

        } failed:^(id result) {
            if(result && [result isKindOfClass:[NSString class]]) {
                [AppUtils showErrorMessage:result];
            } else {
                [AppUtils showErrorMessage:@"获取订单详情失败"];
            }
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = [AppUtils localizedProductString:@"QM_Text_Accept_Goods"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)btnBackClick:(id)sender
{
    [AppUtils dismissHUD];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)returnViewController
{
    if(_popViewController) {
        [self.navigationController popToViewController:_popViewController animated:YES];
    }
}

- (void)confirmBtnClick:(id)sender
{
    if(-1 == _footView.selectIndex) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:nil message:@"请选择评价等级" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    NSString * comment = _footView.commentTextView.text;
    if([comment isEqualToString:@""]) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:nil message:@"请填写评价内容" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    [ _footView.commentTextView resignFirstResponder];
    
    [AppUtils showProgressMessage:@"正在处理..."];
    
    [[GoodsHandler shareGoodsHandler] doScores:_orderInfo.orderId scores:_footView.selectIndex comment:comment success:^(id result) {
        
        [AppUtils showSuccessMessage:@"确认收货完成"];
        
//        [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshOrderState object:_orderId];
        [self.navigationController popViewControllerAnimated:YES];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowCommentsNotify object:nil];
    
    } failed:^(id result) {
        [AppUtils showErrorMessage:(NSString *)result];
        
        //[self performSelector:@selector(returnViewController) withObject:nil afterDelay:2.0];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(_orderInfo) {
        return 1;
    } else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = nil;
    
    if(0 == indexPath.section) {
        
        QMConfirmGoodsTitleCell * titleCell = [tableView dequeueReusableCellWithIdentifier:kQMConfirmGoodsTitleCellIdentifier];
        if(nil == titleCell) {
            titleCell = [QMConfirmGoodsTitleCell cellForConfirmGoodsTitle];
        }
        titleCell.secondLabel.text = [NSString stringWithFormat:@"¥%.2f", _orderInfo.payAmount];
        
        cell = titleCell;
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDeletegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(_controllerType == ECT_ORDER_MGR || _controllerType == ECT_FAVOR_GOODS) {
//        OrderSimpleInfo * info = [_dataArray objectAtIndex:indexPath.section];
//        QMOrderDetailViewController * viewController = [[QMOrderDetailViewController alloc] initWithOrderId:info.order_id ordertime:info.order_pub_time];
//        [self.navigationController pushViewController:viewController animated:YES];
//    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(0 == indexPath.row) {
        return 128;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 14;
}

#pragma mark - UIKeyboard

- (void)processKeyboardWillHide:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0 ;
    UIViewAnimationCurve animationCurve ;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    CGRect newFrame = _tableView.frame ;
    newFrame.origin.y = 0;
    
    [UIView animateWithDuration:animationDuration animations:^{
        _tableView.frame = newFrame;
    }];
    
    _keyboardVisible = NO;
}

- (void)processKeyboardWillShow:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0 ;
    UIViewAnimationCurve animationCurve ;
    CGRect keyboardEndFrame = CGRectZero;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    //注意:当contentSize大于屏幕高度时，要首先将tableview滚动到最低端再计算键盘弹起后tableView上移的距离，否则会出现异常
    [_tableView setContentOffset:CGPointMake(0, _tableView.contentSize.height - _tableView.bounds.size.height) animated:NO];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:self.view];
    CGRect filterViewFrame = [_footView convertRect:_footView.commentTextView.frame toView:self.view];
    
    if(keyboardFrame.origin.y < filterViewFrame.origin.y + filterViewFrame.size.height) {
        CGFloat moveHeight =  filterViewFrame.origin.y + filterViewFrame.size.height - keyboardEndFrame.origin.y;
        CGFloat moveMaxHeight = filterViewFrame.origin.y;
        if(kIOS7) {
            moveMaxHeight -= 64;
        }
        if(moveHeight > moveMaxHeight) {
            moveHeight = moveMaxHeight;
        }
        CGRect newFrame = _tableView.frame;
        newFrame.origin.y -= moveHeight;
        
        [UIView animateWithDuration:animationDuration animations:^{
            _tableView.frame = newFrame;
        }];
    }
    
    _keyboardVisible = YES;
}


- (void)processkeyboardWillChangeFrame:(NSNotification *)notification
{
    if(!_keyboardVisible) {
        return;
    }
    
    NSTimeInterval animationDuration = 0 ;
    UIViewAnimationCurve animationCurve ;
    CGRect keyboardEndFrame = CGRectZero;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    //注意:当contentSize大于屏幕高度时，要首先将tableview滚动到最低端再计算键盘弹起后tableView上移的距离，否则会出现异常
    [_tableView setContentOffset:CGPointMake(0, _tableView.contentSize.height - _tableView.bounds.size.height) animated:NO];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:self.view];
    CGRect filterViewFrame = [_footView convertRect:_footView.commentTextView.frame toView:self.view];
    
    if(keyboardFrame.origin.y < filterViewFrame.origin.y + filterViewFrame.size.height) {
        CGFloat moveHeight =  filterViewFrame.origin.y + filterViewFrame.size.height - keyboardEndFrame.origin.y;
        CGFloat moveMaxHeight = filterViewFrame.origin.y;
        if(kIOS7) {
            moveMaxHeight -= 64;
        }
        if(moveHeight > moveMaxHeight) {
            moveHeight = moveMaxHeight;
        }
        CGRect newFrame = _tableView.frame;
        newFrame.origin.y -= moveHeight;
        
        [UIView animateWithDuration:animationDuration animations:^{
            _tableView.frame = newFrame;
        }];
    } else {
        CGFloat moveHeight =  keyboardEndFrame.origin.y - filterViewFrame.origin.y - filterViewFrame.size.height;
        CGRect newFrame = _tableView.frame;
        newFrame.origin.y += moveHeight;
        [UIView animateWithDuration:animationDuration animations:^{
            _tableView.frame = newFrame;
        }];
    }
    
    _keyboardVisible = YES;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
//    if([touch.view isKindOfClass:[QMConfirmReceivingGoodsView class]] && _keyboardVisible) {
//        return YES;
//    }
    
    if(_keyboardVisible) {
        return YES;
    }
    
    return NO;
    
}

- (void)swipe:(UIGestureRecognizer *)recognizer
{
    if([_footView.commentTextView isFirstResponder]) {
        [_footView.commentTextView resignFirstResponder];
    }
}

@end
