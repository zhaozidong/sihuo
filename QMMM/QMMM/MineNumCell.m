//
//  MineNumCell.m
//  QMMM
//
//  Created by kingnet  on 15-3-19.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "MineNumCell.h"
#import "UserDataInfo.h"

@interface MineNumCell ()
@property (weak, nonatomic) IBOutlet UILabel *flyNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *influenceLbl;
@property (weak, nonatomic) IBOutlet UIView *flyView;

@property (weak, nonatomic) IBOutlet UIView *influenceView;
@end

@implementation MineNumCell

+ (MineNumCell *)mineNumCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"MineNumCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.flyNumLbl.textColor = kRedColor;
    self.influenceLbl.textColor = kRedColor;
    self.flyNumLbl.text = @"0";
    self.influenceLbl.text = @"0";
    self.flyView.userInteractionEnabled = YES;
    self.influenceView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapFlyAction:)];
    tapGesture1.numberOfTapsRequired = 1;
    tapGesture1.numberOfTouchesRequired = 1;
    [self.flyView addGestureRecognizer:tapGesture1];
    
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapInfluenceAction:)];
    tapGesture2.numberOfTapsRequired = 1;
    tapGesture2.numberOfTouchesRequired = 1;
    [self.influenceView addGestureRecognizer:tapGesture2];
}

- (void)updateNum:(PersonInfluence *)influence
{
    _personInfluence = influence;
    if (_personInfluence) {
        self.flyNumLbl.text = [@(_personInfluence.reliability)stringValue];
        self.influenceLbl.text = [@(_personInfluence.influence)stringValue];
    }
}

- (void)tapFlyAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapFlyNumLbl:flyBeat:)]) {
        if (self.personInfluence) {
            [self.delegate didTapFlyNumLbl:[NSString stringWithFormat:@"%d", self.personInfluence.reliability] flyBeat:self.personInfluence.reliabilityBeat];
        }
        else{
            [self.delegate didTapFlyNumLbl:nil flyBeat:nil];
        }
    }
}

- (void)tapInfluenceAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapInfluenceNumLbl:influenceBeat:)]) {
        if (self.personInfluence) {
            [self.delegate didTapInfluenceNumLbl:[NSString stringWithFormat:@"%d", self.personInfluence.influence] influenceBeat:self.personInfluence.influenceBeat];
        }
        else{
            [self.delegate didTapInfluenceNumLbl:nil influenceBeat:nil];
        }
    }
}

@end
