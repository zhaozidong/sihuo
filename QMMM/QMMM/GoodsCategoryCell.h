//
//  GoodsCategoryCell.h
//  QMMM
//
//  Created by kingnet  on 15-1-14.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * kGoodsCategoryCellIdentifier;

@class QMGoodsCateInfo;

@interface GoodsCategoryCell : UITableViewCell

+ (UINib *)nib;

- (void)updateUI:(QMGoodsCateInfo *)category;

+ (CGFloat)cellHeight;

@end
