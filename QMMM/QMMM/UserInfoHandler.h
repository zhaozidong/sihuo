//
//  UserInfoHandler.h
//  QMMM
//
//  Created by kingnet  on 14-9-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseHandler.h"
#import "UserEntity.h"

@class UserEntity;
@class UserBasicInfo;

@interface UserInfoHandler : BaseHandler

@property (nonatomic, strong) UserEntity *currentUser;

+ (UserInfoHandler *)sharedInstance;

- (UserBasicInfo *)getBasicUserInfo:(uint64_t)userId;

/**
 上传生活照的data
 **/
- (void)uploadLifePhotoData:(NSData *)imgData
        uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))uploadProgressBlock
                    success:(SuccessBlock)success
                     failed:(FailedBlock)failed;

/**
 上传头像的data
 **/
- (void)uploadAvatarData:(NSData *)imgData
                 success:(SuccessBlock)success
                  failed:(FailedBlock)failed;

/**
 上传生活照的key
 **/
- (void)uploadLifePhotos:(NSArray *)photos
                 success:(SuccessBlock)success
                  failed:(FailedBlock)failed;

/**
 更新个人信息
 **/
- (void)updateUserInfo:(UserEntity *)entity
               success:(SuccessBlock)success
                failed:(FailedBlock)failed;


/**
 上报当前的位置
 **/
- (void)submitLocation:(NSString *)location
               success:(SuccessBlock)success
                failed:(FailedBlock)failed;

/**
 上传通讯录
 **/
- (void)uploadContacts:(NSArray *)contacts
               success:(SuccessBlock)success
                failed:(FailedBlock)failed;

/**
 登录
 **/
- (void)executeLoginWithUserName:(NSString *)userName
                        password:(NSString *)password
                         success:(SuccessBlock)success
                          failed:(FailedBlock)failed;

/**
 注册
 **/
- (void)executeRegisterWithMobile:(NSString *)mobile
                         password:(NSString *)password
                        checkCode:(NSString *)checkCode
                           avatar:(NSString *)avatar
                         nickname:(NSString *)nickname
                          success:(SuccessBlock)success
                           failed:(FailedBlock)failed;

/**
 修改密码
 **/
- (void)executeChangePwdWithPwd:(NSString *)pwd
                         oldPwd:(NSString *)oldPwd
                        success:(SuccessBlock)success
                         failed:(FailedBlock)failed;

/**
 修改密码时发送短信
 **/
- (void)sendSMSWithSuccess:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 找回密码
 **/
- (void)executeFindPwdWithNewPwd:(NSString *)pwd
                       checkCode:(NSString *)checkCode
                        phoneNum:(NSString *)phoneNum
                         success:(SuccessBlock)success
                          failed:(FailedBlock)failed;

/**
 注册设备token
 **/
- (void)registerDeviceToken:(NSString *)deviceToken;

/**
 获取用户基本信息
 **/
- (void)getUserBasicInfo:(NSArray *)userlist
                 success:(SuccessBlock)success
                  failed:(FailedBlock)failed;

/**
 加载用户基本信息
 **/
- (void)loadUserBasicInfo:(SuccessBlock)sucess
                   failed:(FailedBlock)failed;

/**
 上传个人生活照
 **/
- (void)batchUploadLifePhoto:(NSArray *)filePaths
                     success:(SuccessBlock)success
                      failed:(FailedBlock)failed;

/**
 获取待收货数和待发货数
 **/
- (void)getOrderStateNum:(SuccessBlock)success
                  failed:(FailedBlock)failed;

/**
 退出登录
 **/
- (void)userLogout:(SuccessBlock)success
            failed:(FailedBlock)failed;

/**
 校验验证码
 **/
- (void)smsVerify:(NSString *)type
           mobile:(NSString *)mobile
          captcha:(NSString *)captcha
          success:(SuccessBlock)success
           failed:(FailedBlock)failed;

/**
 获取新手红包
 **/
- (void)newerHongBao:(SuccessBlock)success
              failed:(FailedBlock)failed;

@end
