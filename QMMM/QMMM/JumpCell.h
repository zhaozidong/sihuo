//
//  JumpCell.h
//  QMMM
//
//  Created by kingnet  on 14-9-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kJumpCellIdentifier;

@interface JumpCell : UITableViewCell

@property (nonatomic, assign) int num;
@property (nonatomic, strong) NSIndexPath *indexPath;

+ (UINib *)nib;
- (void)updateUI:(NSIndexPath *)indexPath;
+ (CGFloat)rowHeight;

@end
