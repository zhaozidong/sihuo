//
//  EditGoodsPage1ViewController.h
//  QMMM
//
//  Created by kingnet  on 15-2-27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"
#import "GoodEntity.h"
#import "EditGoodsPage2ViewController.h"

@interface EditGoodsPage1ViewController : BaseViewController

@property (nonatomic, assign) QMEditGoodsType controllerType;

@property (nonatomic, strong) NSArray *beforPhotos; //在未进入该界面之前的得到的照片

@property (nonatomic, assign) uint64_t goodsId; //商品id 更新处用到

@property (nonatomic, copy) UpdateGoodsBlock updateGoodsBlock; //更新商品后的回调

- (id)initWithType:(QMEditGoodsType)type;

@end
