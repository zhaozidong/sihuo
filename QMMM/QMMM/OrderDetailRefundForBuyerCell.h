//
//  OrderDetailRefundForBuyerCell.h
//  QMMM
//
//  Created by kingnet  on 15-3-11.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailCellDelegate.h"

@class OrderDataInfo;
@interface OrderDetailRefundForBuyerCell : UITableViewCell

@property (nonatomic, weak) id <OrderDetailCellDelegate> delegate;

+ (OrderDetailRefundForBuyerCell *)refundForBuyerCell;

- (void)updateUI:(OrderDataInfo *)orderInfo;

@end
