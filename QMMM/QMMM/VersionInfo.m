//
//  VersionInfo.m
//  QMMM
//
//  Created by kingnet  on 14-10-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "VersionInfo.h"

@implementation VersionInfo

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        id temp = [dict objectForKey:@"ver"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            self.version = [dict objectForKey:@"ver"];
        }
        
        temp = [dict objectForKey:@"desc"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            self.desc = [dict objectForKey:@"desc"];
        }
        else{
            self.desc = @"";
        }
        
        temp = [dict objectForKey:@"ios"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.downloadUrl = [dict objectForKey:@"ios"];
        }
    }
    return self;
}

+ (BOOL)isHaveNewVer:(NSString *)ver
{
    NSString *curVer = [[self class]currentVer];
    if ([ver isEqualToString:curVer]) {
        return NO;
    }
    else
        return YES;
}

+ (NSString *)currentVer
{
//    NSString *curVer = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    NSString *curVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    return curVer;
}

@end
