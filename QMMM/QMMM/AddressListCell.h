//
//  AddressListCell.h
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMSwipeTableViewCell.h"

typedef NS_ENUM(NSUInteger, addressEditMode) {
    addressEditModeView,
    addressEditModeEdit
};

extern NSString * const kAddressCellIdentifier;

@class AddressEntity, AddressListCell;

@protocol AddressListCellDelegate <NSObject>

- (void)cellDidDelegate:(AddressListCell *)cell;

- (void)cellDidUpdate:(AddressListCell *)cell;

@end
@interface AddressListCell : RMSwipeTableViewCell
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) UIButton *updateButton;
@property (nonatomic, strong) UIImageView *leftImgV;
@property (nonatomic, strong) UIView *upView; //选中时上面的线
@property (nonatomic, strong) UIView *bottomView; //选中时下面的线
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, weak) id<AddressListCellDelegate> cellDelegate;

+ (CGFloat)cellHeight:(NSString *)str;

- (void)updateUI:(AddressEntity *)entity withMode:(addressEditMode)editMode;
-(void)resetContentView;

@end
