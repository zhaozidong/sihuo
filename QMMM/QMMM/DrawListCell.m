//
//  DrawListCell.m
//  QMMM
//
//  Created by kingnet  on 14-9-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "DrawListCell.h"
#import "FinanceDataInfo.h"

NSString * const kDrawListCellIdentifier = @"kDrawListCellIdentifier";

@interface DrawListCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *amountLbl;

@property (weak, nonatomic) IBOutlet UILabel *statusLbl;
@property (weak, nonatomic) IBOutlet UIImageView *leftImgV;
@end

@implementation DrawListCell

+ (DrawListCell *)drawListCell
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"DrawListCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.titleLbl.text = @"";
    self.timeLbl.text = @"";
    self.amountLbl.text = @"";
    self.statusLbl.text = @"";
    self.titleLbl.textColor = kDarkTextColor;
    self.timeLbl.textColor = kLightBlueColor;
    self.amountLbl.textColor = kDarkTextColor;
    self.amountLbl.font = [UIFont boldSystemFontOfSize:12];
    self.statusLbl.textColor = kLightBlueColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(FinanceDataInfo *)dataInfo
{
    if (dataInfo) {
        self.titleLbl.text = dataInfo.desc;
        self.amountLbl.text = [NSString stringWithFormat:@"%.2f", dataInfo.amount];
        switch (dataInfo.type) {
            case 1://交易完成
            {
                self.statusLbl.text = @"交易完成";
            }
                break;
            case 2://卖家退款
            {
                self.statusLbl.text = @"卖家退款";
            }
                break;
            case 3://卖家退款
            {
                self.statusLbl.text = @"卖家退款";
            }
                break;
            case 4://用户提款
            {
                if (dataInfo.status == 1) {//提现中
                    self.statusLbl.text = @"提现中";
                    self.statusLbl.textColor = kYellowTextColor;
                }
                else{
                    self.statusLbl.text = @"提现成功";
                }
            }
                break;
            case 5://余额付款
            {
                self.statusLbl.text = @"交易完成";
            }
                break;
            case 6://邀请好友红包
            {
                self.statusLbl.text = @"领取成功";
            }
                break;
            case 7://红包提现
            {
                if (dataInfo.status == 1) {
                    self.statusLbl.text = @"提现中";
                    self.statusLbl.textColor = kYellowTextColor;
                }
                else{
                    self.statusLbl.text = @"提现成功";
                }
            }
                break;
            case 8://注册红包
            {
                self.statusLbl.text = @"注册红包";
            }
                break;
            default:
                break;
        }
        
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:dataInfo.ts];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSString *dateStr = [formatter stringFromDate:date];
        self.timeLbl.text = dateStr;
        
        if (dataInfo.amount < 0) {
            self.leftImgV.image = [UIImage imageNamed:@"moneyOut_icon"];
        }
        else{
            self.leftImgV.image = [UIImage imageNamed:@"moneyIn_icon"];
        }
    }
}

+ (CGFloat)rowHeight
{
    return 55.f;
}

@end
