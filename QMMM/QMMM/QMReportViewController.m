//
//  QMReportViewController.m
//  QMMM
//
//  Created by Derek on 15/3/10.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMReportViewController.h"
#import "GoodsInputDescCell.h"
#import "GoodsPhotoBoxCell.h"
#import "TGCameraNavigationController.h"
#import "ReportHandler.h"
#import "ButtonAccessoryView.h"

@interface QMReportViewController ()<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,TGCameraDelegate,GoodsPhotoCellDelegate,UIGestureRecognizerDelegate>{
    QMReportType _reportType;

    NSString *_userId;
    NSString *_goodsId;
}
@property(nonatomic, strong)GoodsInputDescCell *descCell;
@property(nonatomic, strong)GoodsPhotoBoxCell *photoCell;

@property(nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong) ButtonAccessoryView *accessoryView;

@end

@implementation QMReportViewController

-(id)initWithUserId:(NSString *)userId{
    self=[super init];
    if (self) {
        _userId=userId;
        _reportType=QMReportTypePerson;
    }
    return self;
}

-(id)initWithGoodsId:(NSString *)goodsId andUserId:(NSString *)userId{
    self=[super init];
    if (self) {
        _goodsId=goodsId;
        _reportType=QMReportTypeGoods;
        _userId=userId;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"举报";
    self.view.backgroundColor=[UIColor whiteColor];
    
//    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
//        [self setEdgesForExtendedLayout:UIRectEdgeNone];
//    }
    
    self.navigationItem.leftBarButtonItems=[AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(submit:) title:@"提交" size:CGSizeMake(44.f, 44.f) imageName:nil];
    
    
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, 450) style:UITableViewStylePlain];
//    _tableView.contentInset=UIEdgeInsetsMake(-35, 0, 0, 0);
    _tableView.backgroundColor=[UIColor whiteColor];
    _tableView.userInteractionEnabled=YES;
//    _tableView.scrollEnabled=NO;
    _tableView.allowsSelection = NO;
    
    _tableView.delegate=self;
    _tableView.dataSource=self;
    
    [self.view addSubview:_tableView];
    
    
    UISwipeGestureRecognizer *swipGesture=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipTable:)];
    swipGesture.delegate=self;
    swipGesture.direction=UISwipeGestureRecognizerDirectionDown;
    [_tableView addGestureRecognizer:swipGesture];
    
    
    //键盘收回按钮
    _accessoryView = [[ButtonAccessoryView alloc]init];
    _accessoryView.originalFrame = CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds)-CGRectGetWidth(_accessoryView.frame)-5.f,CGRectGetHeight(self.view.frame), CGRectGetWidth(_accessoryView.frame), CGRectGetHeight(_accessoryView.frame));
    [self.view addSubview:_accessoryView];
    
    [self registerObserver];

}

-(void)dealloc{
    [self removeObserver];
}

-(void)swipTable:(UISwipeGestureRecognizer *)recognizer{
    [AppUtils closeKeyboard];
}


- (void)registerObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processKeyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)removeObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [nc removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)processKeyboardWillHide:(NSNotification *)notification
{
    self.canTapCloseKeyboard = NO;
    NSTimeInterval animationDuration = 0 ;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    [UIView animateWithDuration:animationDuration animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0;
        self.tableView.frame = f;
    }];
}

- (void)processKeyboardWillShow:(NSNotification *)notification
{
    self.canTapCloseKeyboard = YES;
    NSTimeInterval animationDuration = 0 ;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    [UIView animateWithDuration:animationDuration animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -20;
        self.tableView.frame = f;
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)backAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)submit:(id)sender{
    if (!_descCell.desc || [_descCell.desc isEqualToString:@""]) {
        [AppUtils showAlertMessage:@"你还没有填写举报理由，不能举报"];
        return;
    }
    if (!_photoCell.isImgAllUploaded) {
        [AppUtils showAlertMessage:@"图片还有上传完成，不能提交"];
        return;
    }
    
    ReportInfo *_reportInfo;
    if (_reportType==QMReportTypePerson) {
        _reportInfo=[[ReportInfo alloc] initWithUserId:_userId reason:_descCell.desc photoKey:_photoCell.keysArray];
    }else if(_reportType==QMReportTypeGoods){
        _reportInfo=[[ReportInfo alloc] initWithGoodsId:_goodsId userId:_userId reaseon:_descCell.desc photoKey:_photoCell.keysArray];
    }
    
    [AppUtils showProgressMessage:@"正在提交信息"];
    [[ReportHandler sharedInstance] reportByInfo:_reportInfo success:^(id obj) {
        [AppUtils showSuccessMessage:@"举报成功"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    } failure:^(id obj) {
        [AppUtils showSuccessMessage:@"举报失败"];
    }];
}

#pragma mark UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        if (!_photoCell) {
            _photoCell=[GoodsPhotoBoxCell goodsPhotoBoxCell];
        }
        _photoCell.delegate=self;
        _photoCell.isShowMainPic=NO;
        _photoCell.selectionStyle=UITableViewCellSelectionStyleNone;
        return _photoCell;
    }else if(indexPath.row==1){
        if (!_descCell) {
            _descCell=[GoodsInputDescCell goodsInputDescCell];
        }
        [_descCell setPlaceHolder:@"请填写举报的理由（必填）" andMaxLength:500];
        return _descCell;
    }
    return nil;
}


#pragma mark UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return 100.0f;
    }else if(indexPath.row==1){
        return 250.0f;
//        return [GoodsInputDescCell cellHeight];
    }
    return 200.0f;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

#pragma mark - Cell Delegate
- (void)showCamera
{
    TGCameraNavigationController *navigationController = [TGCameraNavigationController newWithCameraDelegate:self];
    [self presentViewController:navigationController animated:YES completion:nil];
}


#pragma mark -
#pragma mark - TGCameraDelegate
- (void)cameraDidTakePhoto:(UIImage *)image
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraWillTakePhoto
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (int)maxNumberOfPhotos
{
    return 8;
}

- (void)cameraDidOKWithPhotos:(NSArray *)photoArr
{
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        [weakSelf addGoodsPhotos:photoArr];
    }];
}

#pragma mark - Upload Data
- (void)addGoodsPhotos:(NSArray *)photos
{
    NSMutableArray *array = [NSMutableArray arrayWithArray:photos];
    [self.photoCell addPhotos:array isServerPhoto:NO];
}



@end
