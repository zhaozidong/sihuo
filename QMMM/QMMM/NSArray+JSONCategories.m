//
//  NSArray+JSONCategories.m
//  SDKDemo
//
//  Created by kingnet  on 14-7-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "NSArray+JSONCategories.h"

@implementation NSArray (JSONCategories)

- (NSString*)toJSON
{
    NSString* json = nil;
    NSError* error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
    json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return json;
}

@end
