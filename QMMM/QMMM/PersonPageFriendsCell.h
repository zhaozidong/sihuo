//
//  PersonPageFriendsCell.h
//  QMMM
//
//  Created by kingnet  on 14-11-10.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PersonPageFriendsCellDelegate <NSObject>

- (void)clickAddFriend;

- (void)clickFriendsList:(NSArray *)arrList;

- (void)clickChat; //点击聊天按钮

@end
@interface PersonPageFriendsCell : UITableViewCell

@property (nonatomic, weak) id<PersonPageFriendsCellDelegate> delegate;

- (void)updateUI:(NSArray *)friends;

+ (PersonPageFriendsCell *)friendsCell;

+ (CGFloat)cellHeight;

@end
