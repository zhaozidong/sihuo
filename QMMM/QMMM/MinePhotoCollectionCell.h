//
//  MinePhotoCollectionCell.h
//  QMMM
//
//  Created by kingnet  on 14-9-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kMinePhotoCellIdentifier;

@protocol MinePhotoCollectionCellDelegate <NSObject>

//在上传完一种图片后，执行该方法添加一个key
//filePath为_small后缀
- (void)addPhotoKey:(NSString *)key filePath:(NSString *)filePath;

@end

@interface MinePhotoCollectionCell : UICollectionViewCell

@property (nonatomic, assign) BOOL isLastOne; //最后一张放加button

@property (nonatomic, strong) NSString *filePath; //图片路径，可能是本地的也可能是服务器上的

@property (nonatomic, assign) BOOL isUploading; //是否正在上传

@property (nonatomic, assign) BOOL isUploadOK; //是否上传成功

@property (nonatomic, weak) id<MinePhotoCollectionCellDelegate> delegate;

+ (CGSize)cellSize;

- (void)uploadPhoto;

@end
