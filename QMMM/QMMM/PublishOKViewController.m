//
//  PublishOKViewController.m
//  QMMM
//
//  Created by kingnet  on 14-10-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PublishOKViewController.h"
#import "PublishSuccessView.h"
#import "GoodEntity.h"
#import "QMFriendViewController.h"
#import "GoodsDetailViewController.h"
#import "PersonPageHandler.h"
#import "UserEntity.h"
#import "UserInfoHandler.h"
#import "UserDataInfo.h"
#import "UserRightsManager.h"
#import "SharedManager+Goods.h"

@interface PublishOKViewController ()<PublishSuccessViewDelegate>
{
    BOOL isFirstLoad_;
}
@property (nonatomic) PublishSuccessView *publishOKView;
@end

@implementation PublishOKViewController

- (instancetype)initWithGoodsEntity:(GoodEntity *)entity
{
    if (self = [super init]) {
        self.goodsEntity = [[GoodEntity alloc]initWithGoodsEntity:entity];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"发布成功";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    [self setupViews];
    
    isFirstLoad_ = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backAction:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:kPublishGoodsOKNotify object:nil];
    [super backAction:sender];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!isFirstLoad_) {
        isFirstLoad_ = YES;
        //加载人数
        UserEntity *curUser = [[UserInfoHandler sharedInstance]currentUser];
        __weak typeof(self) weakSelf = self;
        [[PersonPageHandler sharedInstance]getWatchNumAndFlyNum:curUser.uid success:^(id obj) {
            PersonInfluence *influence = (PersonInfluence *)obj;
            [weakSelf.publishOKView setToNum:influence.influence];
        } failed:^(id obj) {
            
        }];
    }
    
    [[UserRightsManager sharedInstance]showNotifyTip:self];
}

- (void)setupViews
{
    _publishOKView = [PublishSuccessView publishView];
    _publishOKView.delegate = self;
    _publishOKView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_publishOKView];
    
    NSDictionary *dict = NSDictionaryOfVariableBindings(_publishOKView);
    CGFloat topH = kMainTopHeight;
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topH-[_publishOKView]" options:0 metrics:@{@"topH":@(topH)} views:dict];
    [self.view addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-(0)-[_publishOKView]-(0)-|" options:0 metrics:0 views:dict];
    [self.view addConstraints:constraints];
}

#pragma mark - PublishSuccessViewDelegate
//分享到微信
- (void)shareToWeiXin
{
    [[SharedManager sharedInstance]sharedWithType:QMSharedTypeWeiXin goodsEntity:self.goodsEntity];
}

//分享到朋友圈
- (void)shareToWeiXinFriends
{
    [[SharedManager sharedInstance]sharedWithType:QMSharedTypeWeiXinFriends goodsEntity:self.goodsEntity];
}

//分享到微博
- (void)shareToWeiBo
{
    [[SharedManager sharedInstance]sharedWithType:QMSharedTypeSinaWeiBo goodsEntity:self.goodsEntity];
}

//分享到qq空间
- (void)shareToQQZone
{
    [[SharedManager sharedInstance]sharedWithType:QMSharedTypeQZone goodsEntity:self.goodsEntity];
}

- (void)copyUrl
{
    [[SharedManager sharedInstance]sharedWithType:QMSharedTypeCopyText goodsEntity:self.goodsEntity];
}

- (void)inviteMoreFriends
{
    QMFriendViewController * viewController = [[QMFriendViewController alloc] initWithNibName:@"QMFriendViewController" bundle:nil];
    viewController.slideToInvite = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)checkGoods
{
    GoodsDetailViewController *controller = [[GoodsDetailViewController alloc]initWithGoodsId:self.goodsEntity.gid];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
