//
//  LRInputViewController.h
//  QMMM
//  登录、注册、找回密码处的输入框与提交按钮
//  Created by kingnet  on 14-11-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@protocol LRInputViewControllerDelegate <NSObject>

@optional
- (void)clickSubmitBtn:(NSString *)phoneNum pwd:(NSString *)pwd;

- (void)clickGetCheckCode:(NSString *)phoneNum;

- (void)clickSubmitBtn:(NSString *)phoneNum pwd:(NSString *)pwd checkCode:(NSString *)checkCode;

- (void)viewShouldMoveY:(CGFloat)Y;

@end
@interface LRInputViewController : BaseViewController

+ (CGFloat)viewHeight:(BOOL)isLoginView;

@property (nonatomic, copy) NSString *submitBtnTitle;

@property (nonatomic, weak) id<LRInputViewControllerDelegate> delegate;

@property (nonatomic, assign) UIReturnKeyType returnKeyType;

@property (nonatomic, copy) NSString *pwdPlaceholder;

@property (nonatomic, assign) BOOL isLoginView; //标识是否是登录界面用

@property (nonatomic, assign) BOOL isRegisterView; //标识是否是注册界面用

- (void)stopTimer;

- (void)startTimer;

@end
