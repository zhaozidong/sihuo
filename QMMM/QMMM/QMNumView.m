//
//  QMNumViewV2.m
//  QMMM
//
//  Created by hanlu on 14-10-31.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMNumView.h"

@implementation QMNumView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    _bkgImageView = [[UIImageView alloc] init];
    [self addSubview:_bkgImageView];
    
    _numLabel = [[UILabel alloc] init];
    [_numLabel setFont:[UIFont systemFontOfSize:10.0]];
    [_numLabel setTextColor:[UIColor whiteColor]];
    [_numLabel setTextAlignment:NSTextAlignmentCenter];
    [_numLabel setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_numLabel];
    
    [self setHidden:YES];
}

- (void)setNum:(NSUInteger)num
{
    if(num > 0) {
        if(num <= 99) {
            [self.numLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)num]];
        } else {
            self.numLabel.text = @"99+";
        }
        CGSize textSize;
        if (kIOS7) {
            textSize = [self.numLabel.text sizeWithAttributes:@{NSFontAttributeName:self.numLabel.font}];
        }
        else{
            textSize = [self.numLabel.text sizeWithFont:self.numLabel.font];
        }
       
        textSize.width += 14;
        CGRect frame = self.frame;
        frame.size.width = textSize.width;
        frame.origin.x = self.frame.origin.x + self.frame.size.width - frame.size.width;
        self.frame = frame;
        
        [self setHidden:NO];
    } else {
        self.numLabel.text = @"";
        if (self.isRedDot) {
            CGRect frame = self.frame;
            frame.size = CGSizeMake(8.f, 8.f);
            frame.origin.x = self.frame.origin.x + self.frame.size.width - frame.size.width;
            self.frame = frame;
            [self setHidden:NO];
        }
        else{
            [self setHidden:YES];
        }
    }
    [self setNeedsLayout];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect bounds = self.bounds;
    _bkgImageView.frame = bounds;
    _numLabel.frame = bounds;
    
    UIImage * image = [UIImage imageNamed:@"unread_bkg"];
    if (NO == self.isRedDot) {
        image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(7, 7, 7, 7)];
    }
    
    _bkgImageView.image = image;
}

- (CGFloat)width
{
    return CGRectGetWidth(self.frame);
}

@end
