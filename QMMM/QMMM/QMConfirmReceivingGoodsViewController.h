//
//  QMConfirmReceivingGoodsViewController.h
//  QMMM
//
//  Created by hanlu on 14-9-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class QMConfirmReceivingGoodsView;

@class OrderDataInfo;

@interface QMConfirmReceivingGoodsViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView * tableView;

@property (nonatomic, strong) IBOutlet QMConfirmReceivingGoodsView * footView;

- (id)initWithOrderId:(NSString *)orderId;

- (id)initWithOrderInfo:(OrderDataInfo *)orderData;

- (void)setPopViewController:(UIViewController *)viewController;

@end
