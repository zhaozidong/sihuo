//
//  SpecialGoodsViewController.m
//  QMMM
//
//  Created by kingnet  on 14-12-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "SpecialGoodsViewController.h"
#import "SpecialGoodsTopCell.h"
#import "ActivityInfo.h"
#import "GoodsHandler.h"
#import "MJRefreshConst.h"
#import "MJRefresh.h"
#import "QMGoodsController.h"
#import "QAudioPlayer.h"
#import "GoodsDetailViewController.h"
#import "PersonPageViewController.h"
#import "QMShareViewContrller.h"
#import "GoodEntity.h"
#import "UserInfoHandler.h"
#import "UserEntity.h"
#import "QMFailPrompt.h"
#import "UIScrollView+UzysCircularProgressPullToRefresh.h"

@interface SpecialGoodsViewController ()<UITableViewDataSource, UITableViewDelegate, QMGoodsControllerDelegate>
{
    __block uint64_t topKey_; //下拉的key
    __block uint64_t lastKey_; //上拉的key
    QMShareViewContrller *_shareViewController;
}

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) SpecialGoodsTopCell *topCell;

@property (nonatomic, strong) ActivityEntity *activity; //专题活动

@property (nonatomic, strong) NSMutableArray *dataList; //存qmgoodscontroller

@end

@implementation SpecialGoodsViewController

- (id)initWithActivity:(ActivityEntity *)activity
{
    self = [super init];
    if (self) {
        if (activity) {
            self.activity = [[ActivityEntity alloc]initWithEntity:activity];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.activity) {
        self.navigationItem.title = self.activity.title;
    }
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    topKey_ = 0;
    lastKey_ = 0;
    
    self.dataList = [NSMutableArray array];
    
    NSNotificationCenter * nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processGoodsUpdateFavorNotify:) name:kGoodsUpdateFavorNotify object:nil];
    [nc addObserver:self selector:@selector(processGoodsUpdateCommentNotify:) name:kAddGoodsCommentNotify object:nil];
    
    if (self.activity) {
        [self.view addSubview:self.tableView];
        [self setupRefresh];
    }
    else{
        QMFailPrompt *prompt =[[QMFailPrompt alloc]initWithFailType:QMFailTypeLoadFail labelText:@"哎呀，加载失败了"    buttonType:QMFailButtonTypeNone buttonText:nil];
        prompt.frame = CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
        [self.view addSubview:prompt];
        [AppUtils showAlertMessage:[AppUtils localizedCommonString:@"QM_Alert_Network"]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[QAudioPlayer sharedInstance] stopPlay];
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (SpecialGoodsTopCell *)topCell
{
    if (!_topCell) {
        _topCell = [SpecialGoodsTopCell topCell];
        _topCell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _topCell.frame = CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), CGRectGetHeight(_topCell.frame));
        [_topCell updateUI:self.activity];
    }
    return _topCell;
}

#pragma mark - Pull Refresh

/**
 *  集成刷新控件
 */
- (void)setupRefresh
{
    __weak typeof(self) weakSelf = self;
    [self.tableView addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRereshing];
    }];
    self.tableView.footer.automaticallyRefresh = YES;
    [self.tableView addPullToRefreshActionHandler:^{
        [weakSelf headerRereshing];
    }];
    [self.tableView triggerPullToRefresh];
}

//下拉
- (void)headerRereshing
{
    [self getGoodsList:1 key:topKey_];
}

//上拉
- (void)footerRereshing
{
    [self getGoodsList:0 key:lastKey_];
}

//拉取商品列表
- (void)getGoodsList:(uint)type key:(uint64_t)key
{
    __weak typeof(self) weakSelf = self;
    [[GoodsHandler shareGoodsHandler]getSpecialGoodsList:type key:key tag:self.activity.tag success:^(id obj) {
        NSDictionary *dataDic = (NSDictionary *)obj;
        NSString *s = [dataDic objectForKey:@"topKey"];
        if (nil != s) {
            topKey_ = [s longLongValue];
        }

        s = [dataDic objectForKey:@"lastKey"];
        if (nil != s) {
            lastKey_ = [s longLongValue];
        }
        
        NSArray *dataArr = [dataDic objectForKey:@"list"];
        [weakSelf reloadUI:dataArr type:type];
        
    } failed:^(id obj) {
        if ([weakSelf.tableView showPullToRefresh]) {
            [weakSelf.tableView stopRefreshAnimation];
        }
        
        if ([weakSelf.tableView.footer isRefreshing]) {
            [weakSelf.tableView.footer endRefreshing];
        }
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)reloadUI:(NSArray *)dataArr type:(int)type
{
    if ([self.tableView showPullToRefresh]) {
        [self.tableView stopRefreshAnimation];
    }
    
    if ([self.tableView.footer isRefreshing]) {
        [self.tableView.footer endRefreshing];
    }
    
    if (type == 1) { //下拉
        if (dataArr.count >0) {
            for (int i=0; i<self.dataList.count; i++) {
                // 先清空所有delegate
                QMGoodsController *goodsController = [self.dataList objectAtIndex:i];
                goodsController.delegate = self;
            }
            
            [self.dataList removeAllObjects];
        }
    }
    
    @synchronized(self.dataList){
        for (int i=0; i<dataArr.count; i++) {
            GoodEntity *goodsEntity = [dataArr objectAtIndex:i];
            QMGoodsController *controller = [[QMGoodsController alloc]initWithGoodsInfo:goodsEntity];
            controller.delegate = self;
            [self.dataList addObject:controller];
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1 + self.dataList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    if (self.dataList.count > 0) {
        QMGoodsController * controller = [self.dataList objectAtIndex:section-1];
        return [controller tableView:tableView numberOfRowsInSection:section];
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        [self.topCell setNeedsLayout];
        [self.topCell layoutIfNeeded];
        CGFloat height = [self.topCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
        height += 1;
        return height;
    }
    else{
        if (self.dataList.count > 0) {
            QMGoodsController * controller = [self.dataList objectAtIndex:indexPath.section-1];
            return [controller tableView:tableView heightForRowAtIndexPath:indexPath];
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return self.topCell;
    }
    else{
        if (self.dataList.count >0) {
            UITableViewCell *cell = nil;
            QMGoodsController *controller = [self.dataList objectAtIndex:indexPath.section-1];
            cell = [controller tableView:tableView cellForRowAtIndexPath:indexPath];
            return cell;
        }
    }
    return nil;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section > 0) {
        QMGoodsController * controller = [self.dataList objectAtIndex:indexPath.section-1];
        [controller tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
}

#pragma mark - Notification Handler
- (void)processGoodsUpdateFavorNotify:(NSNotification *)notification
{
    NSDictionary * dict = notification.userInfo;
    NSNumber * temp = [dict objectForKey:@"goodsId"];
    for (int i=0; i<self.dataList.count; i++) {
        QMGoodsController *goodsController = [self.dataList objectAtIndex:i];
        if ([temp longLongValue] == goodsController.goodsInfo.gid) {
            uint64_t userId = (uint64_t)[[UserInfoHandler sharedInstance].currentUser.uid longLongValue];
            [goodsController.goodsInfo addFavor:userId];
            
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:3 inSection:i+1];
            [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        }
    }
}

- (void)processGoodsUpdateCommentNotify:(NSNotification *)notification
{
    NSDictionary * dict = notification.userInfo;
    NSNumber * temp = [dict objectForKey:@"goodsId"];
    for (int i=0; i<self.dataList.count; i++) {
        QMGoodsController *goodsController = [self.dataList objectAtIndex:i];
        if ([temp longLongValue] == goodsController.goodsInfo.gid) {
            GoodEntity *goodsEntity = goodsController.goodsInfo;
            goodsEntity.commentCounts++;
            
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:3 inSection:i+1];
            [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        }
    }
}

#pragma mark - QMGoodsControllerDelegate

//点击商品图像
- (void)didGoodsImageClick:(QMGoodsController *)controller curIndex:(NSInteger)index
{
    GoodsDetailViewController *viewController = [[GoodsDetailViewController alloc] initWithGoodsId:controller.goodsInfo.gid];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

//点击用户头像
- (void)didGoodsHeadImageClick:(QMGoodsController *)controller userId:(uint64_t)userId
{
    PersonPageViewController *personController = [[PersonPageViewController alloc]initWithUserId:userId];
    [self.navigationController pushViewController:personController animated:YES];
}

//点击赞
- (void)didGoodsFavorClick:(QMGoodsController *)controller
{
    [[GoodsHandler shareGoodsHandler] addFavor:controller.goodsInfo.gid sellerId:controller.goodsInfo.sellerUserId];
}

//点击评论
- (void)didGoodsCommentClick:(QMGoodsController *)controller
{
    GoodsDetailViewController *viewController = [[GoodsDetailViewController alloc] initWithGoodsId:controller.goodsInfo.gid];
    viewController.needScrollToComments = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

//点击分享
- (void)didGoodsShareClick:(QMGoodsController *)controller
{
    if(nil == _shareViewController) {
        _shareViewController = [[QMShareViewContrller alloc] init];
    }
    [_shareViewController setGoodsInfo:controller.goodsInfo];
    [_shareViewController show];
}

@end
