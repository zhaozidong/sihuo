//
//  ImageBrowerView.h
//  QMMM
//
//  Created by kingnet  on 14-10-22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageBrowerViewDelegate <NSObject>
//点击图片时执行
- (void)clickeImageAtIndex:(NSInteger)index;

@end
@interface ImageBrowerView : UIView<UITableViewDelegate, UITableViewDataSource>
{
    UITableView *tableView_;
    UIPageControl *pageControl_;
    UIView *pageBackgroundView_;
}

@property (nonatomic, weak) id<ImageBrowerViewDelegate> delegate;

@property (nonatomic, strong) NSArray *imgs; //图片的url

- (void)updateImages:(NSArray *)images selectIndex:(NSInteger)index;

@end
