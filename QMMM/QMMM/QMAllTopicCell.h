//
//  QMAllTopicCell.h
//  QMMM
//
//  Created by Derek on 15/3/19.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityInfo.h"


typedef void(^tapBlock)(id);


@protocol QMAllTopicDelegate <NSObject>

-(void)didTapTopicImg:(BannerEntity *)entity;

@end


@interface QMAllTopicCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgView;

@property (nonatomic, weak) id<QMAllTopicDelegate> delegate;


+ (QMAllTopicCell *)allTopicCell;

- (void)updateUI:(BannerEntity *)entity;


@end
