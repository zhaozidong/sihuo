//
//  QMSystemMsgHelper.h
//  QMMM
//
//  Created by hanlu on 14-10-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MsgData.h"

@class FMDatabase;

@interface QMMsgHelper : NSObject {
    NSThread * _workThread;
}

//数据库工作线程
- (void)setWorkThread:(NSThread *)aThread;

- (NSArray *)loadMsglist:(FMDatabase *)database type:(MsgBigType)msgType;
//- (NSArray *)loadMsglist:(FMDatabase *)database type:(MsgBigType)msgType fromId:(uint64_t)fromId count:(uint)count;
- (void)addMsgList:(FMDatabase *)database list:(NSArray *)dataList;
- (void)deleteMsgList:(FMDatabase *)database type:(MsgBigType)msgType;
- (void)deleteMsgById:(FMDatabase *)database msgId:(NSString *)msgId;
- (void)updateMsgUnreadStatusByType:(FMDatabase *)database type:(MsgBigType)msgType unRead:(BOOL)unRead;
- (void)updateMsgUnreadStatusById:(FMDatabase *)database msgId:(NSString *)msgId unRead:(BOOL)unRead;

- (NSDictionary *)getMsgUnreadCount:(FMDatabase *)database;
- (NSDictionary *)getMsgUnreadCountList:(FMDatabase *)database;
- (uint)getFriendMsgUnreadCount:(FMDatabase *)database;
- (void)updateFriendMsgStatus:(FMDatabase *)database msgId:(NSString *)msgId newExtData:(NSString *)newExtData;
- (void)asyncGetWalletUnreadCount:(contextBlock)context;

- (void)asyncLoadMsgList:(contextBlock)context type:(MsgBigType)msgType;
//- (void)asyncLoadMsgList:(contextBlock)context type:(MsgBigType)msgType fromId:(uint64_t)fromId count:(uint)count;
- (void)asyncAddMsgList:(contextBlock)context list:(NSArray *)datalist;
- (void)asyncDeleteMsgByType:(contextBlock)context type:(MsgBigType)msgType;
- (void)asyncDeleteMsgById:(contextBlock)context msgId:(NSString *)msgId;
- (void)asyncUpdateMsgUnreadStatusByType:(contextBlock)context type:(MsgBigType)msgType unRead:(BOOL)unRead;
- (void)asyncUpdateMsgUnreadStatusById:(contextBlock)context msgId:(NSString *)msgId unRead:(BOOL)unRead;
- (void)asyncUpdateFriendMsgStatus:(contextBlock)context msgId:(NSString *)msgId newExtData:(NSString *)newExtData;
- (void)asyncGetMsgUnreadCount:(contextBlock)context;
- (void)asyncGetMsgUnreadCountList:(contextBlock)context;
- (void)asyncGetFriendMsgUnreadCount:(contextBlock)context;

@end
