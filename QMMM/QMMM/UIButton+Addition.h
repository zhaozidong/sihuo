//
//  UIButton+Addition.h
//  QMMMM
//

#import <UIKit/UIKit.h>

typedef enum _ttButtonType
{
    BT_LeftImageRightTitle      = 0,    // image在左，title在右
    BT_RightTitleLeftImage      = 1,    // image在右，title在左
    BT_UpImageBottomTitle       = 2,    // image在上，title在下
    BT_BottomImageUpTitle       = 3,    // image在下，title在上
    
    BT_Max
}TT_BUTTON_TYPE;

@interface UIButton (Addition)
- (void)setButtonType:(TT_BUTTON_TYPE)buttonType;
- (void)setButtonType:(TT_BUTTON_TYPE)buttonType interGap:(CGFloat)gap;
@end
