//
//  MsgData.h
//  QMMM
//
//  Created by hanlu on 14-11-3.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@class FMResultSet;

typedef NS_ENUM(NSUInteger, MsgType)
{
    EMT_UNKNOWN = 0,
    EMT_GOODS_PAYED = 11,   //商品被拍下
    EMT_GOODS_DELIVERED = 12, //商品已发货
    EMT_GOODS_RECEIVED = 13, //商品评价
    EMT_GOODS_SEND     = 14, //发送宝贝消息
    EMT_INVITE_FRIEND = 21, //申请加为好友
    EMT_ARGEE_FRIEND = 22,  //同意添加好友
    EMT_CONTACT_COMING = 23,//通讯录好友加入私货
    EMT_GOODS_FAVOR = 31,   //商品赞
    EMT_GOODS_COMMENT = 32, //商品评论
    EMT_GOODS_REPLY_COMMENT = 33,//回去评论
    EMT_SELLER_GET_MONEY = 37, //交易完成，卖家收到钱
    
    EMT_CUSTOM_GOODS = 10000,   //宝贝消息
    
    EMT_GOODS_PRICE_CHANGE          = 10001, //商品被卖家修改了价格
    EMT_BUYER_JUDGE_GOOD            = 10002, //买家评价了卖家的商品并签收
    EMT_CHAT_MESSAGE_INCOMING       = 10003, //有聊天信息发送
    EMT_FRIEND_PUBLISH_NEW_GOOD     = 10004, //朋友xxx发布了新商品
    
    EMT_RECEIVE_FRAUD_TEXT=10005, //关键字诈骗
    EMT_RECEIVE_FRAUD_LINK=10006 //链接诈骗信息
};



typedef enum _msgBigType {
    EMT_NONE = 0,
    EMT_FRIEND = 1,
    EMT_FAVOR = 2,
    EMT_COMMENT = 3,
    EMT_WALLET = 4,
    EMT_TRADE = 5,
}MsgBigType;


@interface MsgData : NSObject

//消息Id
@property (nonatomic, strong) NSString* msgId;

//消息类型
@property (nonatomic, assign) MsgType msgType;

//消息内容
@property (nonatomic, strong) NSString * msgContent;

//消息时间
@property (nonatomic, assign) int msgTime;

//消息附加信息
@property (nonatomic, strong) NSString * extData;

//消息位置:暂时没用
@property (nonatomic, assign) uint msgPos;

//消息是否未读
@property (nonatomic, assign) BOOL unRead;

- (id)initWithDictionary:(NSDictionary *)dict;

- (id)initWithFMResult:(FMResultSet *)resultSet;

@end