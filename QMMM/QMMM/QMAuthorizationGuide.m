//
//  QMAuthorizationGuide.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMAuthorizationGuide.h"

@interface QMAuthorizationGuide(){
    UIImageView *_imageView;
}


@end






@implementation QMAuthorizationGuide

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithType:(QMAuthorizationGuideType)type{
    self=[super init];
    if (self) {
        _imageView=[[UIImageView alloc] init];
        UIImage *image;
        switch (type) {
            case QMAuthorizationGuideTypeAddressBook:
                image=[UIImage imageNamed:@"authorContact"];
                break;
            default:
                break;
        }

        _imageView.image=image;
        [self addSubview:_imageView];
        
        
        self.backgroundColor=[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.3];
    }
    return self;
}

-(void)layoutSubviews{
    _imageView.frame=CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    _imageView.center=self.center;
}




@end
