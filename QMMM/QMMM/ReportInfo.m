//
//  ReportInfo.m
//  QMMM
//
//  Created by Derek on 15/3/10.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "ReportInfo.h"

@implementation ReportInfo

-(id)initWithUserId:(NSString *)userID reason:(NSString *)reason photoKey:(NSArray *)key{
    self=[super init];
    if (self) {
        self.userId=userID;
        self.reason=reason;
        self.photoKey=key;
        self.type=QMReportTypePerson;
    }
    return self;
    
}

-(id)initWithGoodsId:(NSString *)goodsId userId:(NSString *)userId reaseon:(NSString *)reason photoKey:(NSArray *)key{
    self=[super init];
    if (self) {
        self.goodsID=goodsId;
        self.reason=reason;
        self.photoKey=key;
        self.type=QMReportTypeGoods;
        self.userId=userId;
    }
    return self;
}


@end
