//
//  WaitBuyGoodsTipView.h
//  QMMM
//
//  Created by kingnet  on 15-1-9.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WaitBuyGoodsTipView : UIView

@property (nonatomic, strong) NSString *tipStr;

- (void)showWithMaxY:(float)maxY inView:(UIView *)inView completion:(void(^)(void))completion;

- (void)dismisWithCompletion:(void(^)(void))completion;

+ (WaitBuyGoodsTipView *)tipView;

+ (CGFloat)viewHeight;

@end
