//
//  AddAddressCell.m
//  QMMM
//
//  Created by kingnet  on 14-9-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "AddAddressCell.h"

@implementation AddAddressCell

+ (AddAddressCell *)addAddressCell
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"AddAddressCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight
{
    return 44.f;
}

@end
