//
//  KNThread.m
//  QMMM
//
//  Created by 韩芦 on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//
#import "KNThread.h"

@implementation KNThread

- (id)init
{
    self = [super init];
    if (self) {
        delegate = nil;
        [self performSelector:@selector(threadStart) onThread:self withObject:nil waitUntilDone:NO];
    }
    
    return self;
}

- (id)initWithDelegate:(id<KNThreadDelegate>)anDelegate
{
    self = [self init];
    if (self) {
        delegate = anDelegate;
    }
    return self;
}

- (id<KNThreadDelegate>)delegate
{
    return delegate;
}

- (void)setDelegate:(id<KNThreadDelegate>)anDelegate
{
    delegate = anDelegate;
}

- (void)threadStart
{
    if (delegate && [delegate respondsToSelector:@selector(threadStart:)]) 
        [delegate threadStart:self];
}

- (void)threadKill
{
    if (delegate && [delegate respondsToSelector:@selector(threadStop:)]) {
        [delegate threadStop:self];
        delegate = nil;
    }

    [self cancel];
    CFRunLoopStop(CFRunLoopGetCurrent());
}

- (void)kill:(BOOL)waitDone
{
    if ([NSThread currentThread] == self)
        [self threadKill];
    else {
        if ((![self isCancelled]) || ([self isExecuting])) {
            [self cancel];
            [self performSelector:@selector(threadKill) 
                         onThread:self 
                       withObject:nil
                    waitUntilDone:waitDone];
        }
    }
}

- (void)main
{
    @autoreleasepool {
        BOOL running = YES;
        
        // 启动RunLoop
        do {
            CFRunLoopRun();
            running = ![[NSThread currentThread] isCancelled];
        }
        while (running);
    }
    [NSThread exit];
}

@end
