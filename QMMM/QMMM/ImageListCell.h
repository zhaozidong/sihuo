//
//  ImageListCell.h
//  QMMM
//
//  Created by kingnet  on 14-10-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kImageListCellIdentifier;

@protocol ImageListCellDelegate <NSObject>

- (void)deleteImageWithPath:(NSString *)photoPath;

- (void)editImageWithPath:(NSString *)photoPath;

@end
@interface ImageListCell : UITableViewCell

@property (nonatomic, strong) UIImage *photo;

@property (nonatomic, strong) NSString *photoPath;

@property (nonatomic, weak) id<ImageListCellDelegate> delegate;

+ (UINib *)nib;

+ (CGFloat)cellHeight;

@end
