//
//  MineInfoCellControl.h
//  QMMM
//
//  Created by kingnet  on 14-9-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QMRoundHeadView.h"

@class UserEntity;

@interface MineInfoCellControl : NSObject

@property (nonatomic, strong) NSArray *cellsArray;

@property (nonatomic, strong) NSIndexPath *editingIndexPath;

@property (nonatomic, strong) NSString *nickName;

@property (nonatomic, strong) NSString *email;

@property (nonatomic, weak) id<QMRoundHeadViewDelegate> delegate;

+ (CGFloat)rowHeightWithIndexPath:(NSIndexPath *)indexPath;

- (void)updateUI:(UserEntity *)entity;

- (void)updateUserAvarta:(UIImage *)image;

@end
