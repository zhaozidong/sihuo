//
//  QMProbablyKnowViewController.m
//  QMMM
//
//  Created by Derek on 15/1/21.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMProbablyKnowViewController.h"
#import "Friendhandle.h"
#import "FriendInfo.h"
#import "QMProbablyKnowCell.h"
#import "PersonPageViewController.h"
#import "QMFailPrompt.h"
#import "QMFriendViewController.h"
#import "PersonPageHandler.h"

@interface QMProbablyKnowViewController ()<QMAddNewFriendDelegate,QMFailPromptDelegate>
@property (nonatomic, strong)NSArray *arrDataSource;
@end

@implementation QMProbablyKnowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title=@"可能认识的人";
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout =UIRectEdgeNone;
    }
    
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backBtnClick:)];
    
    [self getPeopleData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)backBtnClick:(id)sender
{
    [AppUtils dismissHUD];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getPeopleData{
    [AppUtils showProgressMessage:@"正在加载"];
    __weak typeof(self) weakSelf=self;
    [[Friendhandle shareFriendHandle] getProbablyKnowPeople:^(id result) {
        [AppUtils dismissHUD];
        if (result && [result isKindOfClass:[NSArray class]]) {
            NSArray *arrRes=(NSArray *)result;
            if (arrRes.count>0) {
                weakSelf.arrDataSource=result;
                [weakSelf.tableView reloadData];
            }else{
                [weakSelf addPlaceholder];
            }
        }
    }];
}

-(void)addPlaceholder{
    _tableView.hidden=YES;
    
    QMFailPrompt *failPrompt=[[QMFailPrompt alloc] initWithFailType:QMFailTypeNOData labelText:@"这里没有人，朋友们都在等待你的邀请" buttonType:QMFailButtonTypeNormal buttonText:@"马上邀请"];
    failPrompt.frame=CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
    failPrompt.delegate=self;
    [self.view addSubview:failPrompt];

}
-(void) placeHolderButtonClick:(id)sender{
    QMFriendViewController * viewController = [[QMFriendViewController alloc] initWithNibName:@"QMFriendViewController" bundle:nil];
    viewController.slideToInvite = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrDataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FriendInfo *friendInfo=[self.arrDataSource objectAtIndex:indexPath.row];
    
    QMProbablyKnowCell *cell=[tableView dequeueReusableCellWithIdentifier:QMPROBABLY];
    if (!cell) {
        cell=[QMProbablyKnowCell cellForProbablyKnow];
    }
    
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }

    
    cell.delegate=self;
    [cell updateWithFriendInfo:friendInfo];

    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FriendInfo *friendInfo=[self.arrDataSource objectAtIndex:indexPath.row];
    PersonPageViewController *controller = [[PersonPageViewController alloc]initWithUserId:friendInfo.uid];
    [self.navigationController pushViewController:controller animated:YES];

}

-(void)addNewFriend:(FriendInfo *)friendInfo{//添加朋友
    //统计添加好友次数
    [AppUtils trackCustomEvent:@"add_friend_event" args:nil];
    [AppUtils showProgressMessage:@"正在发送"];
    [[PersonPageHandler sharedInstance]addFriendId:[NSString stringWithFormat:@"%llu",friendInfo.uid] success:^(id obj) {
        [AppUtils showSuccessMessage:@"发送成功"];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

@end
