//
//  SellingGoodsControlCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kSellingGoodsControlCellIdentifier;

@class GoodsSimpleInfo;

@protocol SellingGoodsControlCellDelegate <NSObject>

- (void)didTapDeleteBtn:(GoodsSimpleInfo *)goodsInfo;

- (void)didTapDropBtn:(GoodsSimpleInfo *)goodsInfo;

- (void)didTapEditBtn:(GoodsSimpleInfo *)goodsInfo;

@end

@interface SellingGoodsControlCell : UITableViewCell

@property (nonatomic, weak) id<SellingGoodsControlCellDelegate> delegate;

+ (UINib *)nib;

- (void)updateUI:(GoodsSimpleInfo *)goodsInfo;

+ (CGFloat)cellHeight;

@end
