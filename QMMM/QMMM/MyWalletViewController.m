//
//  MyWalletViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MyWalletViewController.h"
#import "WithDrawHeadView.h"
#import "MyWalletViewController.h"
#import "MyWalletCell.h"
#import "WithdrawController.h"
#import "DrawListController.h"
#import "FinanceHandler.h"
#import "FinanceDataInfo.h"
#import "MsgHandle.h"
#import "TempFlag.h"
#import "UserDefaultsUtils.h"
#import "ChatViewController.h"

@interface MyWalletViewController ()<UITableViewDataSource, UITableViewDelegate,UIAlertViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) WithDrawHeadView *headView;
@property (nonatomic, strong) NSArray *leftTextArr;
@property (nonatomic, strong) NSArray *leftIconArr;
@end

@implementation MyWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"我的钱包";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    self.leftTextArr = @[@"私货现金", @"红包"];
    self.leftIconArr = @[@"moneyIcon_icon", @"hongbao_icon"];
    [self.view addSubview:self.tableView];
    
    [self getFinanceInfo];
    
    //将消息更新为已读
    [[MsgHandle shareMsgHandler] updateMsgUnreadStatus:nil msgType:EMT_WALLET unread:NO];
}

-(void)backAction:(id)sender{
    FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
    float money=financeInfo.amount;
    if (![UserDefaultsUtils boolValueWithKey:IS_GIVE_COMMENTS] && money>0) {
        [UserDefaultsUtils saveBoolValue:YES withKey:IS_GIVE_COMMENTS];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"您的第一笔交易已经完成啦!求赏个五星好评吧，咱给程序员买点肉。" delegate:self cancelButtonTitle:@"残忍拒绝" otherButtonTitles:@"马上好评",@"我要吐槽", nil];
        [alert show];
    }else{
        [super backAction:sender];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {//残忍拒绝

    }else if(buttonIndex==1){//马上好评
        NSString *strURL=@"https://itunes.apple.com/cn/app/si-huo-hao-you-jian-xian-zhi/id955693333?l=en&mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strURL]];
    }else if (buttonIndex==2){//立即吐槽
        ChatViewController *chatController  = [[ChatViewController alloc] initWithChatter:@"10000"];
        [self.navigationController pushViewController:chatController animated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
    self.headView.money = financeInfo.amount+financeInfo.hongbao;
    [self.tableView reloadData];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:kFlushMsgUnreadCountNotify object:nil];
}

#pragma mark - UIView
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.rowHeight = [MyWalletCell cellHeight];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
        [_tableView registerNib:[MyWalletCell nib] forCellReuseIdentifier:kMyWalletIdentifier];
        _tableView.tableHeaderView = self.headView;
    }
    return _tableView;
}

- (WithDrawHeadView *)headView
{
    if (!_headView){
        _headView = [WithDrawHeadView headView];
        _headView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), [WithDrawHeadView headViewHeight]);
    }
    return _headView;
}

#pragma mark - Methods
- (void)getFinanceInfo
{
    __weak typeof(self) weakSelf = self;
    [AppUtils showProgressMessage:@"加载中"];
    [[FinanceHandler sharedInstance]freeFinanceInfo];
    [[FinanceHandler sharedInstance]getFinanceInfo:^(id obj) {
        [AppUtils dismissHUD];
        FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
        weakSelf.headView.money = (financeInfo.amount + financeInfo.hongbao);
        [weakSelf.tableView reloadData];
    } failed:^(id obj) {
        
    }];
}

#pragma mark - UITableVew DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.leftTextArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyWalletCell *cell = (MyWalletCell *)[tableView dequeueReusableCellWithIdentifier:kMyWalletIdentifier];
    cell.leftText = [self.leftTextArr objectAtIndex:indexPath.row];
    cell.iconStr = [self.leftIconArr objectAtIndex:indexPath.row];
    FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
    if (indexPath.row == 0) {
        cell.money = financeInfo.amount;
    }
    else if (indexPath.row == 1){
        cell.money = financeInfo.hongbao;
    }
    return cell;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        kWithdrawFrom = kBalance;
    }
    else{
        kWithdrawFrom = kHongBao;
    }
    DrawListController *controller = [[DrawListController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}


@end
