//
//  QMSqlite.m
//  QMMM
//
//  Created by 韩芦 on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMSqlite.h"

@implementation FMDatabase (TTAdditions)

- (int)userDatabaseVersion
{
    return [self intForQuery:@"PRAGMA user_version;"];
}

- (void)setUserDatabaseVersion:(int)version
{
    [self executeUpdate:[NSString stringWithFormat:@"PRAGMA user_version = %d;", version]];
}

- (BOOL)isEmpty
{
    return [self intForQuery:@"select count(*) from sqlite_master where type='table';"] == 0;
}

@end