//
//  SellingGoodsControlCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "SellingGoodsControlCell.h"
#import "GoodDataInfo.h"

NSString * const kSellingGoodsControlCellIdentifier = @"kSellingGoodsControlCellIdentifier";

@interface SellingGoodsControlCell ()
{
    GoodsSimpleInfo *goodsInfo_;
}
@property (weak, nonatomic) IBOutlet UIButton *editGoodsBtn;
@property (weak, nonatomic) IBOutlet UIButton *dropGoodsBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteGoodsBtn;

@end

@implementation SellingGoodsControlCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"SellingGoodsControlCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    [self.editGoodsBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [self.dropGoodsBtn setTitle:@"" forState:UIControlStateNormal];
    [self.deleteGoodsBtn setTitle:@"删除" forState:UIControlStateNormal];
    UIImage * image = [UIImage imageNamed:@"gray_border_btn_bkg"];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(10, 7, 10, 7)];
    [self.editGoodsBtn setBackgroundImage:image forState:UIControlStateNormal];
    [self.dropGoodsBtn setBackgroundImage:image forState:UIControlStateNormal];
    [self.deleteGoodsBtn setBackgroundImage:image forState:UIControlStateNormal];
    [self.editGoodsBtn setTitleColor:kGrayTextColor forState:UIControlStateNormal];
    [self.dropGoodsBtn setTitleColor:kGrayTextColor forState:UIControlStateNormal];
    [self.deleteGoodsBtn setTitleColor:kGrayTextColor forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(GoodsSimpleInfo *)goodsInfo
{
    if (goodsInfo.status == 0) {
        [self.dropGoodsBtn setTitle:@"上架" forState:UIControlStateNormal];
    }
    else if (goodsInfo.status == 1){
        [self.dropGoodsBtn setTitle:@"下架" forState:UIControlStateNormal];
    }
    goodsInfo_ = goodsInfo;
}

+ (CGFloat)cellHeight
{
    return 45.f;
}

- (IBAction)deleteGoodsAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapDeleteBtn:)]) {
        [self.delegate didTapDeleteBtn:goodsInfo_];
    }
}

- (IBAction)dropGoodsAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapDropBtn:)]) {
        [self.delegate didTapDropBtn:goodsInfo_];
    }
}

- (IBAction)editGoodsAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapEditBtn:)]) {
        [self.delegate didTapEditBtn:goodsInfo_];
    }
}

@end
