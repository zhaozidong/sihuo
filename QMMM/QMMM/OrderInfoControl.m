//
//  OrderInfoControl.m
//  QMMM
//
//  Created by kingnet  on 14-9-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderInfoControl.h"
#import "GoodEntity.h"
#import "AddressEntity.h"
#import "FinanceDataInfo.h"

@interface OrderInfoControl ()
{
    float freight_; //运费
}
@property (weak, nonatomic) IBOutlet UITextField *numTf; //添加数量
@property (weak, nonatomic) IBOutlet UILabel *nameLbl; //姓名 电话
@property (weak, nonatomic) IBOutlet UILabel *cityLbl; //省市区
@property (weak, nonatomic) IBOutlet UILabel *addressLbl; //详细地址
@property (weak, nonatomic) IBOutlet UILabel *priceLbl; //需要支付的价格
@property (weak, nonatomic) IBOutlet UILabel *stockLbl;//库存
@property (weak, nonatomic) IBOutlet UILabel *payLbl; //实际支付的价格
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *lessBtn;
@property (weak, nonatomic) IBOutlet UILabel *freightLbl;

@end

//static const float kFeeMoney = 0.025;

@implementation OrderInfoControl

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cellsArray = [[NSBundle mainBundle] loadNibNamed:@"OrderCells" owner:self options:0];
        self.nameLbl.text = @"";
        self.cityLbl.text = @"";
        self.addressLbl.text = @"";
        self.numTf.text = @"1";
        self.payLbl.text = @"";
        self.priceLbl.text = @"";
        self.priceLbl.textColor = kRedColor;
        self.priceLbl.textColor = kRedColor;
        self.payLbl.textColor = kRedColor;
        self.freightLbl.text = @"";
    }
    return self;
}

+ (NSInteger)rowCount
{
    return 6;
}

+ (CGFloat)cellHeightWithIndex:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2) {
        return 66.f;
    }
    return 45.f;
}

-(void)setGoodsEntity:(GoodEntity *)goodsEntity
{
    if (goodsEntity) {
        _goodsEntity = goodsEntity;
        self.stockLbl.text = [NSString stringWithFormat:@"库存：%d", (_goodsEntity.number-_goodsEntity.lockNum)];
        self.priceLbl.text = [NSString stringWithFormat:@"¥ %.2f", _goodsEntity.price];
        freight_ = 20.f;
        if (_goodsEntity.freight == 0) {
            freight_ = 0.f;
        }
        else if(_goodsEntity.freight == -1){
            if (_addressEntity) {
                freight_ = [AppUtils freightForAddress:_addressEntity.locationName];
            }
        }
        else if(_goodsEntity.freight > 0){ //自定义运费
            freight_ = _goodsEntity.freight;
        }
        if (freight_==0) {
            self.freightLbl.text = @"包邮";
        }else{
            self.freightLbl.text = [NSString stringWithFormat:@"+¥ %.2f", freight_];
        }
        
        self.payLbl.text = [NSString stringWithFormat:@"¥ %.2f", freight_+_goodsEntity.price];
        [self setNum:1];
    }
}

- (void)setAddressEntity:(AddressEntity *)addressEntity
{
    _addressEntity = addressEntity;
    if (addressEntity) {
        self.nameLbl.text = [NSString stringWithFormat:@"%@ %@", _addressEntity.consignee, _addressEntity.phone];
        self.cityLbl.text = _addressEntity.locationName;
        self.addressLbl.text = _addressEntity.detailAddress;
        if (_goodsEntity && _goodsEntity.freight == -1) {
            freight_ = [AppUtils freightForAddress:_addressEntity.locationName];
            self.freightLbl.text = [NSString stringWithFormat:@"+¥ %.2f", freight_];
        }
    }
    else{
        self.nameLbl.text = @"";
        self.cityLbl.text = @"";
        self.addressLbl.text = @"";
    }

    float totalPrice = self.goodsEntity.price * [self goodsNum];
    self.payLbl.text = [NSString stringWithFormat:@"¥ %.2f", freight_+totalPrice];
}

- (void)setNum:(int)num
{
    self.numTf.text = [NSString stringWithFormat:@"%d", num];
    self.addBtn.enabled = (num < _goodsEntity.number-_goodsEntity.lockNum);
    self.lessBtn.enabled = (num > 1);
}

- (IBAction)lessButtonAction:(id)sender {
    int num = [self.numTf.text intValue];
    num--;
    [self setNum:num];
    [self calculatePrice];
}
- (IBAction)addButtonAction:(id)sender {
    int num = [self.numTf.text intValue];
    num++;
    [self setNum:num];
    [self calculatePrice];
}

- (int)goodsNum
{
    return [self.numTf.text intValue];
}

- (void)calculatePrice
{
    float totalPrice = self.goodsEntity.price * [self goodsNum];
    self.priceLbl.text = [NSString stringWithFormat:@"%.2f", totalPrice];
    self.payLbl.text = [NSString stringWithFormat:@"¥ %.2f", totalPrice+freight_];
    if (self.delegate && [self.delegate respondsToSelector:@selector(changeGoodsNum)]) {
        [self.delegate changeGoodsNum];
    }
}

- (float)actualPay
{
    float totalPrice = self.goodsEntity.price * [self goodsNum];
    return totalPrice + freight_;
}

@end
