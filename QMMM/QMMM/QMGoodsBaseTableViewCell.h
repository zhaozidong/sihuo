//
//  QMGoodsBaseTableViewCell.h
//  QMMM
//
//  Created by hanlu on 14-9-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMGoodsBaseTableViewCell : UITableViewCell

+(NSUInteger)indentForCell;

//是否禁止缩进
@property (nonatomic, assign) BOOL forbidIndent;

@end
