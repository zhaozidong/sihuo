//
//  MyOrderInfo.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MyOrderInfo.h"
#import "QMHttpClient.h"
#import "APIConfig.h"
#import "DataSigner.h"
#import <AlipaySDK/AlipaySDK.h>
#import "AppDelegate.h"
#import "GoodsDataHelper.h"
#import "QMDataCenter.h"
#import "OrderDataInfo.h"

@interface MyOrderInfo(){
    float _remainPay;
    NSString *_orderId;
}
//数据库操作相关
@property (readonly, strong) GoodsDataHelper * goodsHelper;

@end

@implementation MyOrderInfo

-(id)initWithOrderId:(NSString *)orderId andRemain:(float)remainPay{
    self=[super init];
    if (self) {
        _remainPay=remainPay;
        _orderId=orderId;
        _goodsHelper = [QMDataCenter sharedDataCenter].goodsDataHelper;
    }
    return  self;
}


-(NSString *)getOrderStringWithId:(NSString *)orderId productName:(NSString *)name description:(NSString *)description price:(float)price notifyURL:(NSString *)URL{
    
    Order *order=[[Order alloc] init];
    order.partner=PAY_PARTNER;
    order.seller=PAY_SELLER;
    
    order.tradeNO=orderId;
    order.productName=name;
    order.productDescription=description;
    order.amount=[NSString stringWithFormat:@"%.2f",price];
    order.notifyURL=URL;

    order.service = @"mobile.securitypay.pay";
    order.paymentType = @"1";
    order.inputCharset = @"utf-8";
    order.itBPay = @"30m";
    order.showUrl = @"m.alipay.com";
    
    //将商品信息拼接成字符串
    NSString *orderSpec = [order description];
//    NSLog(@"orderSpec = %@",orderSpec);
    
    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner(PAY_PRIVATEKEY);
    NSString *signedString = [signer signString:orderSpec];
    
    //将签名成功字符串格式化为订单字符串,请严格按照该格式
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        return  orderString;
    }
    return nil;
}

-(void)notifyForSuccessWithParams:(NSDictionary *)dictParams{
    
    NSString *status=[dictParams objectForKey:@"resultStatus"];
    if ([status isEqualToString:@"9000"]) {
        [AppUtils trackCustomEvent:@"purchase_success_event" args:nil];
        
        if (_delegate && [_delegate respondsToSelector:@selector(paySuccessForOrderId:)]) {
            [_delegate paySuccessForOrderId:_orderId];
        }
        //数据库更新
        [_goodsHelper asyncUpdateOrderStatus:nil bSeller:NO orderId:_orderId status:QMOrderStatusPaid];
        
        //已付款待发货
//        NSDictionary * dict = @{@"orderId":_orderId, @"status":@(QMOrderStatusPaid)};
//        [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateOrderStatusNotify object:nil userInfo:dict];
    }else{
        if (_delegate && [_delegate respondsToSelector:@selector(payFail:)]) {
            NSString *errorTip = [dictParams objectForKey:@"errorTip"];
            [_delegate payFail:errorTip];
        }
    }
}


-(void)payOrder{
    __block NSString * url = [[self class] httpsRequestUrlWithPath:API_PAY_ORDER];
    __weak __typeof(self)weakSelf = self;
    NSDictionary *params = @{@"id":_orderId, @"remain_pay":@(_remainPay),@"payment_type":@(1)};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result && [result isKindOfClass:[NSDictionary class]]) {
                NSString *orderId =[result objectForKey:@"id"];
                NSString *productName=[result objectForKey:@"desc"];
                NSString *notify_url=[result objectForKey:@"notify_url"];
                weakSelf.callBackURL=[result objectForKey:@"callback_url"];
                float price=[[result objectForKey:@"third_pay"] floatValue];
                if (!orderId || !productName || !notify_url || !price) {
//                    TTAlert(@"系统繁忙，请稍后再试！");
                    NSDictionary *dic = @{@"resultStatus":@"9000"};
                    [weakSelf notifyForSuccessWithParams:dic];
                    return;
                }
                NSString *orderString=[self getOrderStringWithId:orderId productName:productName description:productName price:price notifyURL:notify_url];
                AppDelegate *appDelegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
                appDelegate.orderInfo=weakSelf;
                
                if (orderString) {
                    [[AlipaySDK defaultService] payOrder:orderString fromScheme:@"sihuoAliPay" callback:^(NSDictionary *resultDic) {//只处理状态为9000(成功)和4000(失败)的情况
                        if (resultDic) {
                            if ([[resultDic objectForKey:@"resultStatus"] isEqualToString:@"9000"] || [[resultDic objectForKey:@"resultStatus"] isEqualToString:@"4000"]) {
                                    [weakSelf notifyForSuccessWithParams:resultDic];
                            }
                        }
                    }];
                }
            }
            else{
                NSDictionary *resultDic = @{@"resultStatus":@"1000", @"errorTip":error.userInfo[kServerErrorTip]};
                [weakSelf notifyForSuccessWithParams:resultDic];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"faild to get order information");
        }];
    });
}

@end