//
//  ImageBrowerCell.h
//  QMMM
//
//  Created by kingnet  on 14-10-21.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kImageBrowerCellIdty;

@class YLProgressBar;
@interface ImageBrowerCell : UITableViewCell
{
    UIImageView *imageView_;
}

@property (nonatomic, strong) NSString *imageName; //图片的下载路径

@property (nonatomic, assign) CGSize size;

@property (nonatomic, strong) YLProgressBar *progressBar;

- (void)downloadNextImage:(NSString *)imageName;

@end
