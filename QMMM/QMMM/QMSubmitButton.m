//
//  QMSubmitButton.m
//  QMMM
//
//  Created by kingnet  on 14-9-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMSubmitButton.h"

@implementation QMSubmitButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    UIImage *normalImg = [[UIImage imageNamed:@"submit_btn"] stretchableImageWithLeftCapWidth:7.f topCapHeight:7.f];
    [self setBackgroundImage:normalImg forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont systemFontOfSize:20.f]];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.titleLabel.shadowColor = [UIColor clearColor];
    UIImage *disableImg = [[UIImage imageNamed:@"submit_btn_disable"] stretchableImageWithLeftCapWidth:7.f topCapHeight:7.f];
    [self setBackgroundImage:disableImg forState:UIControlStateDisabled];
}

@end
