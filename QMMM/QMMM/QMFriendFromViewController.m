//
//  QMFriendFromViewController.m
//  QMMM
//
//  Created by Derek on 15/1/26.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMFriendFromViewController.h"

@interface QMFriendFromViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>

@end

@implementation QMFriendFromViewController

-(id)initWithUserInfo:(PersonPageUserInfo *)userInfo{
    self=[super init];
    if (self) {
        self.userInfo=userInfo;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"朋友来源";
//    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
//        self.edgesForExtendedLayout=UIRectEdgeNone;
//    }
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight) style:UITableViewStyleGrouped];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kBorderColor;
    [self.view addSubview:_tableView];
    
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -UITableViewDataSource

- (NSInteger)numberOfSections{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=nil;
    cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"cell"];
    }
    cell.textLabel.textColor=[UIColor grayColor];
    cell.textLabel.textAlignment=NSTextAlignmentLeft;
    cell.textLabel.font=[UIFont systemFontOfSize:18.0f];
    cell.textLabel.text=@"手机";
    
    NSString *strTemp=[NSString stringWithFormat:@"%@ (%@)",self.userInfo.userInfo.nickname,self.userInfo.userInfo.mobile];
    cell.detailTextLabel.textColor=[UIColor grayColor];
    cell.detailTextLabel.font=[UIFont systemFontOfSize:18.0f];
    cell.detailTextLabel.textAlignment=NSTextAlignmentLeft;
    cell.detailTextLabel.text=strTemp;
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}


#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.userInfo.userInfo.mobile) {
        NSString *strTemp=[NSString stringWithFormat:@"%@",self.userInfo.userInfo.mobile];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:nil message:strTemp delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"拨打", nil];
        [alert show];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 14.f;
}

#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (1==buttonIndex) {
        NSString *strTemp=[NSString stringWithFormat:@"tel://%@",self.userInfo.userInfo.mobile];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strTemp]];
    }
}


@end
