//
//  QMGoodsBaseTableViewCell.m
//  QMMM
//
//  Created by hanlu on 14-9-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsBaseTableViewCell.h"

@implementation QMGoodsBaseTableViewCell

+(NSUInteger)indentForCell
{
    return 16;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.backgroundView != nil) {
        CGRect frame = self.backgroundView.frame;
        frame = CGRectInset(frame, 4.0f - frame.origin.x, 0.0f);
        self.backgroundView.frame = frame;
        
        if (self.selectedBackgroundView != nil)
            self.selectedBackgroundView.frame = frame;
    }
    
    if(self.contentView != nil) {
        CGRect frame = self.contentView.frame;
        frame.origin.x = 0;
        frame.size.width = [UIScreen mainScreen].bounds.size.width;
        if(!self.forbidIndent) {
            NSInteger indent = [[self class] indentForCell]; //缩进10px
            frame = CGRectInset(frame, indent/2, 0.0f);
        }
        self.contentView.frame = frame;
    }
    
    self.backgroundColor = [UIColor clearColor];
}


@end
