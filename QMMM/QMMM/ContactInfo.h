//
//  ContactInfo.h
//  QMMM
//
//  Created by hanlu on 14-10-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AddressBook/AddressBook.h>

@interface ContactInfo : NSObject

@property (nonatomic, strong) NSString * personName;

@property (nonatomic, strong) NSString * pinyinPersonName;

@property (nonatomic, strong) NSMutableArray * phoneList;

- (id)initWithRecordRef:(ABRecordRef)record;

@end
