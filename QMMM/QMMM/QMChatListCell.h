//
//  QMChatListCell.h
//  QMMM
//
//  Created by hanlu on 14-10-3.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QMRoundHeadView.h"

extern NSString * const kQMChatListCellIdentifier;

@class QMNumView;

@interface QMChatListCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * nameLabel;

@property (nonatomic, strong) IBOutlet UILabel * contentLabel;


@property (weak, nonatomic) IBOutlet QMRoundHeadView *headView;

@property (nonatomic, strong) IBOutlet UILabel * timeLabel;

@property (nonatomic, strong) IBOutlet QMNumView * numView;

@property (nonatomic, strong) IBOutlet UIImageView * unreadImageView;

+ (id)cellForChatList;

+ (CGFloat)heightForChatList;

- (void)UpdateUI:(EMConversation *)data;

@end
