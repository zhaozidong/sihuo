//
//  UserDao.h
//  QMMM
//
//  Created by kingnet  on 14-9-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseDao.h"
#import "UserEntity.h"

@interface UserDao : BaseDao

+ (UserDao *)sharedInstance;

- (BOOL)update:(BaseEntity *)entity propertyName:(NSString *)propertyName;

@end
