//
//  GoodDataInfo.h
//  QMMM
//
//  Created by 韩芦 on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BaseEntity.h"

//赞
@interface FavorInfo : NSObject

//用户ID
@property (nonatomic, assign) uint64_t uid;

//用户头像
@property (nonatomic, strong) NSString * uicon;

//点赞时间
@property (nonatomic, assign) int64_t time;

//点赞人的昵称
@property (nonatomic, strong) NSString *nickName;

- (NSDictionary *)dict;

@end

//赞列表
@interface FavorList : NSObject {
    NSMutableDictionary * _dict;
}

@property (nonatomic, strong) NSMutableArray * favorArray;

- (id)initWithArray:(NSArray *)rawdata;

- (void)addFavor:(FavorInfo *)info;

- (void)deleteFavor:(uint64_t)uid;

- (void)deleteFavorList;

- (BOOL)hasFarvor:(uint64_t)uid;

- (NSString *)getSaveString;

- (BOOL)isEmpty;

@end

//图片附件
@interface GoodsPictureList : NSObject

@property (nonatomic, strong) NSMutableArray* rawArray;

- (id)initWithArray:(NSArray *)rawArray;

- (NSString *)getSaveString;

- (BOOL)isEmpty;

@end

//简要商品信息
@interface GoodsSimpleInfo : BaseEntity

@property (nonatomic, assign) uint64_t gid;

@property (nonatomic, strong) NSString * desc;

@property (nonatomic, strong) NSString * photo;

@property (nonatomic, assign) float price;

@property (nonatomic, assign) int publish_time;

@property (nonatomic, assign) int update_time;

@property (nonatomic, assign) int num;

@property (nonatomic, assign) CGFloat orig_price;

@property (nonatomic, assign) int status;

@property (nonatomic, assign) int trust;

- (id)initWithDictionary:(NSDictionary *)dict;

- (void)assignValue:(GoodsSimpleInfo *)info;

- (NSDictionary *)toDictionary;

@end

typedef enum _EControllerType
{
    ECT_ORDER_MGR, //已卖掉的
    ECT_GOODS_MGR, //在卖的商品
    ECT_BUYED_GOODS, //买到的商品
    ECT_FAVOR_GOODS, //赞过的商品
}EControllerType;

