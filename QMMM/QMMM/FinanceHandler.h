//
//  FinanceHandler.h
//  QMMM
//
//  Created by kingnet  on 14-9-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseHandler.h"

@class FinanceUserInfo;

@interface FinanceHandler : BaseHandler

//记录账户信息
@property (nonatomic, strong) FinanceUserInfo *financeUserInfo;

+ (FinanceHandler *)sharedInstance;

/**
 获取账户信息
 **/
- (void)getFinanceInfo:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 取现
 **/
- (void)drawMoney:(NSString *)pwd
            money:(float)money  //取现金额
            payee:(NSString *)payee
        accountNo:(NSString *)accountNo
          success:(SuccessBlock)success
           failed:(FailedBlock)failed;

/**
 获得取现列表
 **/
- (void)getDrawList:(int)startId
            success:(SuccessBlock)success
             failed:(FailedBlock)failed;

/**
 设置账户
 **/
- (void)setAccount:(NSString *)accountNo
       accountName:(NSString *)accountName
           success:(SuccessBlock)success
            failed:(FailedBlock)failed;

/**
 更改帐号
 **/
- (void)changeAccountNo:(NSString *)accountNo
            accountName:(NSString *)accountName
           oldAccountNo:(NSString *)oldAccountNo
         oldAccountName:(NSString *)oldAccountName
                success:(SuccessBlock)success
                 failed:(FailedBlock)failed;

/**
 释放当前的financeInfo
 **/
- (void)freeFinanceInfo;

@end
