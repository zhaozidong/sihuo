//
//  FinanceHandler.m
//  QMMM
//
//  Created by kingnet  on 14-9-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "FinanceHandler.h"
#import "FinanceApi.h"
#import "FinanceDataInfo.h"
#import "FinanceDao.h"
#import "TempFlag.h"

@interface FinanceHandler ()
{
    FinanceApi *financeApi_;
}
@end
@implementation FinanceHandler

- (id)init
{
    if (self = [super init]) {
        financeApi_ = [[FinanceApi alloc]init];
    }
    return self;
}

+ (FinanceHandler *)sharedInstance
{
    static FinanceHandler *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)getFinanceInfo:(SuccessBlock)success failed:(FailedBlock)failed
{
    if (self.financeUserInfo != nil) {
        if (success) {
            success(self.financeUserInfo);
        }
    }
    else{
        FinanceHandler __weak * weakSelf = self;
        [financeApi_ getFinanceInfo:^(id resultDic) {
            if ([resultDic[@"s"] intValue] == 0) {
                FinanceUserInfo *info = [[FinanceUserInfo alloc]initWithDictionary:resultDic[@"d"]];
                weakSelf.financeUserInfo = info;
                if (success) {
                    success(kHTTPTreatSuccess);
                }
            }
            else{
                if (failed) {
                    failed(resultDic[@"d"]);
                }
            }
        }];
    }
}

- (void)freeFinanceInfo
{
    self.financeUserInfo = nil;
}

- (void)drawMoney:(NSString *)pwd money:(float)money payee:(NSString *)payee accountNo:(NSString *)accountNo success:(SuccessBlock)success failed:(FailedBlock)failed
{
    FinanceHandler __weak * weakSelf = self;
    [financeApi_ drawMoney:pwd money:money payee:payee accountNo:accountNo context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = resultDic[@"d"];
            if (success) {
                if ([kWithdrawFrom isEqualToString:kBalance]) {
                    weakSelf.financeUserInfo.amount = [dataDic[@"amount"] floatValue];
                }
                else{
                    weakSelf.financeUserInfo.hongbao = [dataDic[@"amount"] floatValue];
                }
                if ([weakSelf.financeUserInfo.accountNo isEqualToString:@""]) {
                    weakSelf.financeUserInfo.accountNo = accountNo;
                }
                if ([weakSelf.financeUserInfo.payee isEqualToString:@""]) {
                    weakSelf.financeUserInfo.payee = payee;
                }
                [[NSNotificationCenter defaultCenter]postNotificationName:kWithdrawSuccessNotify object:nil];
                success(dataDic);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)getDrawList:(int)startId success:(SuccessBlock)success failed:(FailedBlock)failed
{
    int top = 0;
    if (startId == 0) {
        top = 1;
    }
    [financeApi_ getDrawList:startId top:top context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            if ([kWithdrawFrom isEqualToString:kBalance]) {
                if (startId == 0) {
                    //如果是下拉则清空本地的所有记录
                    [[FinanceDao sharedInstance]removeAll];
                }
            }
            
            //得到最新的数据
            NSArray *dataArr = (NSArray *)(resultDic[@"d"]);
            NSMutableArray *models = [[NSMutableArray alloc] initWithCapacity:dataArr.count];
            for (NSDictionary *dic in dataArr) {
                FinanceDataInfo *dataInfo = [[FinanceDataInfo alloc]initWithDictionary:dic];
                [models addObject:dataInfo];
                if ([kWithdrawFrom isEqualToString:kBalance]) {
                    //插入数据库
                    [[FinanceDao sharedInstance]insert:dataInfo];
                }
            }
            if (success) {
                success(models);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)setAccount:(NSString *)accountNo accountName:(NSString *)accountName success:(SuccessBlock)success failed:(FailedBlock)failed
{
    FinanceHandler __weak * weakSelf = self;
    [financeApi_ setAccount:accountNo accountName:accountName oldAccountNo:nil oldAccountName:nil context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            weakSelf.financeUserInfo.payee = accountName;
            weakSelf.financeUserInfo.accountNo = accountNo;
            if (success) {
                success(kHTTPTreatSuccess);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)changeAccountNo:(NSString *)accountNo accountName:(NSString *)accountName oldAccountNo:(NSString *)oldAccountNo oldAccountName:(NSString *)oldAccountName success:(SuccessBlock)success failed:(FailedBlock)failed
{
    FinanceHandler __weak * weakSelf = self;
    [financeApi_ setAccount:accountNo accountName:accountName oldAccountNo:oldAccountNo oldAccountName:oldAccountName context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            NSDictionary *dataDic = [resultDic objectForKey:@"d"];
            FinanceUserInfo *tmpInfo = [[FinanceUserInfo alloc]initWithDictionary:dataDic];
            weakSelf.financeUserInfo.payee = tmpInfo.payee;
            weakSelf.financeUserInfo.accountNo = tmpInfo.accountNo;
            weakSelf.financeUserInfo.amount = tmpInfo.amount;
            if (success) {
                success(kHTTPTreatSuccess);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

@end
