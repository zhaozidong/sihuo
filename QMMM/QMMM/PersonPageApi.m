//
//  PersonPageApi.m
//  QMMM
//
//  Created by kingnet  on 14-10-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PersonPageApi.h"
#import "APIConfig.h"
#import "QMHttpClient.h"

@implementation PersonPageApi

- (void)getSoldOutGoods:(NSString *)goodsId uid:(NSString *)uid context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:[NSString stringWithFormat:API_SOLDOUT_GOODS, uid, goodsId]];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"soldout goods list error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"soldout goods list error:%@", error.description);
        }];
        
    });
}

- (void)getSellingGoods:(uint64_t)goodsId uid:(NSString *)uid context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_SELLING_GOODS];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"uid":uid, @"id":@(goodsId)};
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
        
    });
}

- (void)getUserInfo:(NSString *)uid context:(contextBlock)context
{
    NSString *urlS = [NSString stringWithFormat:API_PERSON_PAGE_INFO, uid, @"uid,nickname,avatar,mobile,life_photo,positive,neutral,negative,remark"];
    __block NSString *path = [[self class]httpsRequestUrlWithPath:urlS];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"soldout goods list error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"soldout goods list error:%@", error.description);
        }];
        
    });
}

- (void)addWatch:(NSString *)uid follow:(int)follow context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_ADD_WATCH];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"fid":uid, @"follow":@(follow)};
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"addwatch error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"addwatch error:%@", error.description);
        }];
        
    });
}

- (void)getWatchNumAndFlyNum:(NSString *)uid context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_INFLUENCE_NUM];
    
    __block contextBlock bContext = context;
    NSDictionary *params = @{@"uid":uid};
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"getInfluence error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"getInfluence error:%@", error.description);
        }];
        
    });
}

- (void)addFriendId:(NSString *)uid context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_ADD_FRIEND];
    
    __block contextBlock bContext = context;
    NSDictionary *params = @{@"fid":uid};
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"addFriend error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"addFriend error:%@", error.description);
        }];
        
    });
}

- (void)getEvaluateList:(NSString *)uid orderId:(uint64_t)orderId context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_EVALUATE_LIST];
    
    __block contextBlock bContext = context;
    NSDictionary *params = @{@"uid":uid, @"id":@(orderId)};
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"getEvaluateList error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"getEvaluateList error:%@", error.description);
        }];
        
    });
}

@end
