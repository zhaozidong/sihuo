//
//  QMGoodsViewController.h
//  QMMM
//
//  Created by 韩芦 on 14-9-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

#import "MyGoodsList.h"

@class  QMGoodsFootView;

@interface QMGoodsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView * tableView;

@property (nonatomic, strong) IBOutlet QMGoodsFootView * footView;

- (id)initWithListType:(GoodsListType)listType;

@end
