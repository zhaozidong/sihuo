//
//  QMInputAlert.m
//  QMMM
//
//  Created by kingnet  on 14-9-27.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMInputAlert.h"
#import "UIPlaceHolderTextView.h"
#import "UITextField+Additions.h"
#import "NoPasteTextField.h"

@interface QMInputAlert ()<UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet NoPasteTextField *textField;
@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *textView;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@property (nonatomic) UIControl *backgroundControl;

@end

@implementation QMInputAlert

- (instancetype)init
{
    self = [[[NSBundle mainBundle]loadNibNamed:@"QMInputAlert" owner:self options:0] lastObject];
    if (self) {
        [self registerObserver];
    }
    return self;
}

- (void)dealloc
{
    [self removeObserver];
}

- (void)registerObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processKeyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)processKeyboardWillHide:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0 ;
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    [UIView animateWithDuration:animationDuration animations:^{
        UIWindow *window = [[UIApplication sharedApplication]keyWindow];
        CGRect f = self.frame;
        f.origin.y = CGRectGetHeight(window.frame)/2 - CGRectGetHeight(f)/2;
        self.frame = f;
    }];
}

- (void)processKeyboardWillShow:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0 ;
    CGRect keyboardEndFrame = CGRectZero;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    if (CGRectGetMaxY(self.frame) > CGRectGetMinY(keyboardEndFrame)) {
        CGFloat differ = (CGRectGetMaxY(self.frame) - CGRectGetMinY(keyboardEndFrame));
        [UIView animateWithDuration:animationDuration animations:^{
            CGRect f = self.frame;
            f.origin.y = f.origin.y - differ;
            self.frame = f;
        }];
    }
}

- (void)removeObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [nc removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [nc removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    UIImage *bgImg = [UIImage imageNamed:@"inputAlert_bg"];
    self.backgroundImageView.image = [bgImg stretchableImageWithLeftCapWidth:6 topCapHeight:6];
    self.textField.text = @"";
    self.textField.placeholder = @"";
    self.textField.delegate = self;
    self.textField.textColor = kDarkTextColor;
    UIImage *cancelImg = [UIImage imageNamed:@"cancel_btn"];
    [self.cancelBtn setBackgroundImage:[cancelImg stretchableImageWithLeftCapWidth:5 topCapHeight:6] forState:UIControlStateNormal];
    UIImage *okImg = [UIImage imageNamed:@"ok_btn"];
    [self.okBtn setBackgroundImage:[okImg stretchableImageWithLeftCapWidth:5 topCapHeight:6] forState:UIControlStateNormal];
    UIImage *tfBg = [UIImage imageNamed:@"inputNum_bg"];
    [self.textField setBackground:[tfBg stretchableImageWithLeftCapWidth:2 topCapHeight:2]];
    
    self.textView.placeholder = @"";
    self.textView.placeholderTextColor = kPlaceholderColor;
    self.textView.delegate = self;
    self.textView.hidden = YES;
    self.textView.layer.borderWidth = 0.5;
    self.textView.layer.borderColor = [kBorderColor CGColor];
    self.textView.textColor = kDarkTextColor;
    self.textView.font = [UIFont systemFontOfSize:15.f];
}

+ (void)showWithPlaceholder:(NSString *)placeholder alertType:(QMInputAlertType)alertType delegate:(id<QMInputAlertDelegate>)delegate
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    QMInputAlert *alert = [[QMInputAlert alloc]init];
    alert.alertType = alertType;
    alert.placeholder = placeholder;
    alert.delegate = delegate;
    alert.backgroundControl.alpha = 0.0;
    CGRect frame = alert.frame;
    frame.origin.y = CGRectGetHeight(window.frame);
    frame.origin.x = CGRectGetWidth(window.frame)/2 - CGRectGetWidth(frame)/2;
    alert.frame = frame;
    [window addSubview:alert.backgroundControl];
    [window addSubview:alert];
    
    [UIView animateWithDuration:0.3 animations:^{
        alert.backgroundControl.alpha = 0.7;
        CGRect rect = alert.frame;
        rect.origin.y = CGRectGetHeight(window.frame)/2.f-CGRectGetHeight(alert.frame)/2.f;
        alert.frame = rect;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundControl.alpha = 0.0;
        UIWindow *window = [[UIApplication sharedApplication]keyWindow];
        CGRect rect = self.frame;
        rect.origin.y = CGRectGetHeight(window.frame);
        self.frame = rect;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.backgroundControl removeFromSuperview];
            [self removeFromSuperview];
        }
    }];
}

- (UIControl *)backgroundControl
{
    if (!_backgroundControl) {
        _backgroundControl = [[UIControl alloc]initWithFrame:[UIScreen mainScreen].bounds];
        _backgroundControl.backgroundColor = [UIColor blackColor];
    }
    return _backgroundControl;
}

- (void)setPlaceholder:(NSString *)placeholder
{
    if (placeholder) {
        if (_alertType == QMInputAlertTypeText) {
            self.textView.placeholder = placeholder;
        }
        else{
            self.textField.attributedPlaceholder = [AppUtils placeholderAttri:placeholder];
        }
    }
}

- (void)setAlertType:(QMInputAlertType)alertType
{
    _alertType = alertType;
    switch (alertType) {
        case QMInputAlertTypeSecurity:
        {
            self.textField.secureTextEntry = YES;
        }
            break;
        case QMInputAlertTypeFloat:
        {
            self.textField.keyboardType = UIKeyboardTypeDecimalPad;
        }
            break;
        case QMInputAlertTypeText:
        {
            self.textField.hidden = YES;
            [self.textField removeFromSuperview];
            self.textView.hidden = NO;
            CGRect frame = self.frame;
            frame.size.height = 200;
            self.frame = frame;
        }
            break;
        default:
            break;
    }
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return [textField shouldChangeCharactersInRange:range replacementString:string];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    NSString *toBeString = textView.text;
    NSString *lang = textView.textInputMode.primaryLanguage; // 键盘输入模式
    
    if ([lang isEqualToString:@"zh-Hans"]) { // 简体中文输入，包括简体拼音，健体五笔，简体手写
        
        UITextRange *selectedRange = [textView markedTextRange];
        
        //获取高亮部分
        
        UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
        
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        
        if (!position) {
            
            if (toBeString.length > kRejectRefundLimitNum) {
                
                textView.text = [toBeString substringToIndex:kRejectRefundLimitNum];
                [AppUtils showErrorMessage:[NSString stringWithFormat:@"最多可输入%d个字", kRejectRefundLimitNum]];
            }
        }
        
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        else{
            
        }
    }
    
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    
    else{
        
        if (toBeString.length > kRejectRefundLimitNum) {
            
            textView.text = [toBeString substringToIndex:kRejectRefundLimitNum];
            [AppUtils showErrorMessage:[NSString stringWithFormat:@"最多可输入%d个字", kRejectRefundLimitNum]];
        }
    }
}

#pragma mark - UIButton Action
- (IBAction)okAction:(id)sender {
    if (_alertType == QMInputAlertTypeText) {
        [self.textView resignFirstResponder];
    }
    else{
        [self.textField resignFirstResponder];
    }
    
    BOOL flag = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(okWithInputStr:inputAlert:)]) {
        NSString *str = (_alertType == QMInputAlertTypeText)?self.textView.text:self.textField.text;
        if (str == nil) {
            flag = [self.delegate okWithInputStr:@"" inputAlert:self];
        }
        else{
            flag = [self.delegate okWithInputStr:str inputAlert:self];
        }
    }
    if (flag) {
        [self dismiss];
    }
}
- (IBAction)cancelAction:(id)sender {
    if (_alertType == QMInputAlertTypeText) {
        [self.textView resignFirstResponder];
    }
    else{
        [self.textField resignFirstResponder];
    }
    
    [self dismiss];
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelInput)]) {
        [self.delegate cancelInput];
    }
}

@end
