//
//  BuyHongBaoGoodsViewController.h
//  QMMM
//  购买红包商品的下单界面
//  Created by kingnet  on 15-3-27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@interface BuyHongBaoGoodsViewController : BaseViewController

- (id)initWithNum:(int)num price:(float)price goodsId:(int)goodsId;

@end
