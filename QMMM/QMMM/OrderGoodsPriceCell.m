//
//  OrderGoodsPriceCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderGoodsPriceCell.h"
#import "OrderDataInfo.h"

NSString * const kOrderGoodsPriceCellIdentifier = @"kOrderGoodsPriceCellIdentifier";

@interface OrderGoodsPriceCell ()

@property (weak, nonatomic) IBOutlet UILabel *label;

@end
@implementation OrderGoodsPriceCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"OrderGoodsPriceCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.label.textColor = kDarkTextColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(OrderSimpleInfo *)orderInfo
{
    self.label.text = [NSString stringWithFormat:@"价格：¥%.2f  数量：%d", orderInfo.goods_price, orderInfo.goods_num];
}

+ (CGFloat)cellHeight
{
    return 30.f;
}

@end
