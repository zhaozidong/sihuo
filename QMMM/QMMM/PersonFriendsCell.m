//
//  PersonFriendsCell.m
//  QMMM
//
//  Created by kingnet  on 14-11-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PersonFriendsCell.h"
#import "UserDataInfo.h"
#import "QMRoundHeadView.h"

NSString * const kPersonFriendsCellIdentifier = @"kPersonFriendsCellIdentifier";

@interface PersonFriendsCell ()

@property (weak, nonatomic) IBOutlet QMRoundHeadView *headView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@end

@implementation PersonFriendsCell

- (id)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle]loadNibNamed:@"PersonFriendsCell" owner:self options:0] lastObject];
    if (self) {
        self.headView.headViewStyle = QMRoundHeadViewSmall;
        self.nameLbl.textColor = kDarkTextColor;
        self.headView.userInteractionEnabled = NO;
    }
    return self;
}

- (void)updateUI:(UserDataInfo *)userInfo
{
    [self.headView setSmallImageUrl:userInfo.avatar];
    self.nameLbl.text = userInfo.nickname;
}

@end
