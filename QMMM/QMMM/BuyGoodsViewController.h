//
//  BuyGoodsViewController.h
//  QMMM
//
//  Created by kingnet  on 14-9-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@class GoodEntity;

@interface BuyGoodsViewController : BaseViewController

- (instancetype)initWithGoodsID:(uint64_t)gid;


@end
