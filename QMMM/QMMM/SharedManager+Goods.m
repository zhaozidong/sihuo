//
//  SharedManager+Goods.m
//  QMMM
//
//  Created by kingnet  on 15-2-28.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "SharedManager+Goods.h"
#import "GoodEntity.h"
#import "UserEntity.h"
#import "BaseHandler.h"
#import "UserInfoHandler.h"

@implementation SharedManager (Goods)

- (void)sharedWithType:(QMSharedType)type goodsEntity:(GoodEntity *)goodsEntity
{
    UserEntity *user = [[UserInfoHandler sharedInstance]currentUser];
    NSString *sellerUid = [NSString stringWithFormat:@"%qu", goodsEntity.sellerUserId];
    NSString * webpageUrl = [NSString stringWithFormat:@"/?c=html5&a=goods&goods_id=%qu", goodsEntity.gid];
    webpageUrl = [BaseHandler requestUrlWithPath:webpageUrl];
    NSString *goodsDesc = goodsEntity.desc;
    if (goodsDesc.length > 20) {
        goodsDesc = [goodsDesc substringToIndex:20];
    }
    switch (type) {
        case QMSharedTypeWeiXin:
        {
            //分享标题与描述
            NSString * title = @"";
            NSString * desc = @"";
            if ([user.uid isEqualToString:sellerUid]) {
                //是自己的东西
                title = @"我有个闲置物品想转手，求戳求带走！";
            }
            else{
                title = @"哇塞！这个闲置物品还能更便宜吗！赶紧下单拍吧！";
            }
            desc = [NSString stringWithFormat:@"%.2f转让%@", goodsEntity.price,goodsEntity.desc];
            [self sharedWithType:QMSharedTypeWeiXin title:title content:desc description:@"" image:[AppUtils getAbsolutePath:goodsEntity.picturelist.rawArray[0]] url:webpageUrl];
        }
            break;
        case QMSharedTypeWeiXinFriends:
        {
            NSString * title = @"";
            if ([user.uid isEqualToString:sellerUid]) {
                //是自己的东西
                title = @"这里有一只落单的小萌物，快把它带走吧！";
            }
            else{
                title = @"咦，我发现了一个落单的小可怜，你愿意带它回家吗？";
            }
            [self sharedWithType:QMSharedTypeWeiXinFriends title:title content:@"" description:@"" image:[AppUtils getAbsolutePath:goodsEntity.picturelist.rawArray[0]] url:webpageUrl];
        }
            break;
        case QMSharedTypeCopyText:
        {
            [self sharedWithType:QMSharedTypeCopyText title:@"" content:webpageUrl description:@"" image:@"" url:@""];
            [AppUtils showSuccessMessage:@"商品的链接已复制"];
        }
            break;
        case QMSharedTypeSinaWeiBo:
        {
            NSString *content = @"";
            if ([user.uid isEqualToString:sellerUid]) {
                content = [NSString stringWithFormat:@"#淘私货#哇塞，这件宝贝绝对超值！\"%@\"，只要%.2f元，赶快下单抢吧？抢购地址：%@（分享自@私货）", goodsDesc, goodsEntity.price, webpageUrl];
            }
            else{
                content = [NSString stringWithFormat:@"#淘私货#呜呜呜，这里有一只落单的小可怜，\"%@\"，只要%.2f元，你愿意把它带回家吗？购买地址：%@（分享自@私货）", goodsDesc, goodsEntity.price, webpageUrl];
            }
            
            [self sharedWithType:QMSharedTypeSinaWeiBo title:@"" content:content description:@"" image:[AppUtils getAbsolutePath:goodsEntity.picturelist.rawArray[0]] url:webpageUrl];
        }
            break;
        case QMSharedTypeQZone:
        {
            NSString *title = @"";
            NSString *desc = @"";
            if ([user.uid isEqualToString:sellerUid]) {
                title = @"【跪求】我想转手一个闲置物品，求围观！";
                desc = [NSString stringWithFormat:@"我正在私货上转手\"%@\"，只要%.2f元。求带走，求帮扩！", goodsDesc,goodsEntity.price];
            }
            else{
                title = @"【呜】这只小萌物好可怜，带它回家吧！";
                desc = [NSString stringWithFormat:@"我在私货上发现了一只小萌物，\"%@\"，只要%.2f元。你愿意带它回家吗？", goodsDesc,goodsEntity.price];
            }
            [self sharedWithType:QMSharedTypeQZone title:title content:@"" description:desc image:[AppUtils getAbsolutePath:goodsEntity.picturelist.rawArray[0]] url:webpageUrl];
        }
            break;
        default:
            break;
    }
}

@end
