//
//  QMPullRefreshView.m
//  tongtong
//
//  Created by Liao Feng on 13-10-8.
//  Copyright (c) 2013年 SNDA. All rights reserved.
//

#import "QMPullRefreshView.h"
#import <QuartzCore/QuartzCore.h>

#define FLIP_ANIMATION_DURATION 0.2f

@implementation QMPullRefreshView
{
    BOOL _selfFrameValid;
    CGRect _selfFrame;
}

@synthesize textLabel = _textLabel;
@synthesize indicatorView = _indicatorView;
@synthesize arrowImageView = _arrowImageView;

@synthesize state = _state;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (QMPullRefreshView *)refreshView
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"QMPullRefreshView" owner:self options:0];
    return [array lastObject];
}

- (void)dealloc
{
    self.textLabel = nil;
    self.indicatorView = nil;
    self.arrowImageView = nil;
}

- (void)setState:(NSInteger)state
{
    if (_state != state) {
        
        switch (state) {
            case ePullRefreshStateRefreshing:
                [_arrowImageView layer].transform = CATransform3DIdentity;
                _arrowImageView.hidden = YES;
                
                _textLabel.text = @"正在刷新...";
                _indicatorView.hidden = NO;
                [_indicatorView startAnimating];
                break;
                
            case ePullRefreshStateReleaseToRefresh:
                [UIView beginAnimations:nil context:nil];
                [_arrowImageView layer].transform = CATransform3DMakeRotation((M_PI / 180.0) * 180.0f, 0.0f, 0.0f, 1.0f);
                _arrowImageView.hidden = NO;
                [UIView setAnimationDuration:FLIP_ANIMATION_DURATION];
                [UIView commitAnimations];

                _textLabel.text = @"松开以刷新";
                _indicatorView.hidden = YES;
                [_indicatorView stopAnimating];
                break;
            
            case ePullRefreshStatePullForRefresh:
            default:
                [UIView beginAnimations:nil context:nil];
                if (_state == ePullRefreshStateReleaseToRefresh) {
                    [_arrowImageView layer].transform = CATransform3DIdentity;
                }
                else
                    [_arrowImageView layer].transform = CATransform3DIdentity;
                _arrowImageView.hidden = NO;
                [UIView setAnimationDuration:FLIP_ANIMATION_DURATION];
                [UIView commitAnimations];
                
                _textLabel.text = @"下拉可以刷新";
                _indicatorView.hidden = YES;
                [_indicatorView stopAnimating];
                break;
        }

        _state = state;
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (!_selfFrameValid) {
        _selfFrame = [self convertRect:self.bounds toView:scrollView];
        _selfFrameValid = YES;
    }
    
    if (_state != ePullRefreshStateRefreshing) {
        CGPoint offset = scrollView.contentOffset;
        if(kIOS7) {
            offset.y += 64;
        }
        if (offset.y < _selfFrame.origin.y) {
            self.state = ePullRefreshStateReleaseToRefresh;
        }
        else if (offset.y < (_selfFrame.origin.y + _selfFrame.size.height)) {
            self.state = ePullRefreshStatePullForRefresh;
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    CGPoint offset = scrollView.contentOffset;
    if(kIOS7) {
         offset.y += 64;
    }
    if (offset.y < _selfFrame.origin.y) {
        if (_delegate) {
            [_delegate didPullRefresh:self];
        }
    }
    
    _selfFrameValid = NO;
}

@end
