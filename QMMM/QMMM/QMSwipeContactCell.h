//
//  QMSwipeContactCell.h
//  QMMM
//
//  Created by hanlu on 14-11-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RMSwipeTableViewCell.h"
#import "QMRoundHeadView.h"

@class FriendInfo;

@protocol QMSwipeContactDelegate;

extern NSString * const kQMSwipeContactCellIdentifier;

@interface QMSwipeContactCell : RMSwipeTableViewCell

//@property (nonatomic, strong) IBOutlet UIImageView * headImageView;

@property (weak, nonatomic) IBOutlet QMRoundHeadView *headView;
@property (nonatomic, strong) IBOutlet UILabel * contactLabel;

@property (nonatomic, strong) UIButton * remarkButton;

@property (nonatomic, weak) id<QMSwipeContactDelegate> contactDelegate;

+ (id)cellForSwipeContact;

- (void)updateUI:(FriendInfo *)friendInfo;

- (void)updateUIWithFriend:(FriendInfo *)friendInfo andSearchString:(NSString *)searchString;

- (void)resetContentView;

- (FriendInfo *)getFriendInfo;

@end

@protocol QMSwipeContactDelegate <NSObject>

- (void)didRemarkBtnClick:(QMSwipeContactCell *)cell;

@end
