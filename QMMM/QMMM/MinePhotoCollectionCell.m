//
//  MinePhotoCollectionCell.m
//  QMMM
//
//  Created by kingnet  on 14-9-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MinePhotoCollectionCell.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+IF.h"
#import "UIImageView+QMWebCache.h"
#import "UploadProgress.h"
#import "UserInfoHandler.h"
#import "UIImage+FixOrientation.h"
#import "MinePhotoCell.h"

NSString * const kMinePhotoCellIdentifier = @"kMinePhotoCellIdentifier";

@interface MinePhotoCollectionCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imgV;
@property (weak, nonatomic) IBOutlet UploadProgress *progressView;

@end

@implementation MinePhotoCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle]loadNibNamed:@"MinePhotoCollectionCell" owner:self options:0] lastObject];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.layer.borderColor = UIColorFromRGB(0xd8dee6).CGColor;
        self.layer.masksToBounds = YES;
        self.layer.borderWidth = 1.f;
        self.layer.cornerRadius = 8.f;
        self.progressView.hidden = YES;
        self.progressView.cornerRadius = 8.f;
        self.isUploadOK = YES;
        self.isUploading = NO;
    }
    return self;
}

- (void)setIsLastOne:(BOOL)isLastOne
{
    _isLastOne = isLastOne;
    if (_isLastOne) {
        self.imgV.image = [UIImage imageNamed:@"addPhoto_btn"];
    }
}

- (void)setFilePath:(NSString *)filePath
{
    _filePath = filePath;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:_filePath]) {
        [self uploadPhoto];
    }
    else{
        self.progressView.hidden = YES;
        UIImage *placeholder = [UIImage imageNamed:@"goodsPlaceholder_small"];
        NSString *smallPath = [AppUtils thumbPath:_filePath sizeType:QMImageQuarterSize];
        [self.imgV qm_setImageWithURL:smallPath placeholderImage:placeholder];
    }
}

+ (CGSize)cellSize
{
//    return CGSizeMake(70.f, 70.f);
    float w = (kMainFrameWidth-[MinePhotoCell itemBorder]*5)/4;
    return CGSizeMake(w, w);
}

- (NSData *)getImgData:(NSString *)filePath
{
    NSData *data = nil;
    NSString *bigS= [filePath stringByReplacingOccurrencesOfString:@"_small" withString:@""];
    NSString *editPic=[filePath stringByReplacingOccurrencesOfString:@"_small" withString:@"_edit"];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    if ([fileMgr fileExistsAtPath:bigS]){
        UIImage *image=[[UIImage imageWithContentsOfFile:bigS] fixOrientation];
        if ([self fileSizeAtPath:bigS]>1) {
            data=UIImageJPEGRepresentation(image, 0.1);
        }else{
            data=UIImageJPEGRepresentation(image, 0.6);
        }
    }else if ([fileMgr fileExistsAtPath:editPic]){
        UIImage *image=[[UIImage imageWithContentsOfFile:editPic] fixOrientation];
        if ([self fileSizeAtPath:editPic]>1) {
            data=UIImageJPEGRepresentation(image, 0.1);
        }else{
            data=UIImageJPEGRepresentation(image, 0.6);
        }
    }
    return data;
}

- (float) fileSizeAtPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize]/(1024.0*1024);
    }
    return 0;
}

- (void)uploadPhoto
{
    UIImage *tmpImage = [UIImage imageWithContentsOfFile:_filePath];
    self.imgV.image = tmpImage;
    NSData *data = [self getImgData:_filePath];
    if (data) {
        //上传该张图片
        self.progressView.hidden = NO;
        self.progressView.value = 0.f;
        self.isUploading = YES;
        self.progressView.isFailed = NO;
        
        __weak typeof(self) weakSelf = self;
        [[UserInfoHandler sharedInstance]uploadLifePhotoData:data uploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
            
            weakSelf.progressView.value = (float) totalBytesWritten/totalBytesExpectedToWrite;
            
        } success:^(id obj) {
            weakSelf.progressView.value = 1.0;
            weakSelf.progressView.hidden = YES;
            weakSelf.isUploading = NO;
            weakSelf.isUploadOK = YES;
            
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(addPhotoKey:filePath:)]) {
                [weakSelf.delegate addPhotoKey:(NSString *)obj filePath:weakSelf.filePath];
            }
        } failed:^(id obj) {
            weakSelf.progressView.value = 0.0;
            weakSelf.progressView.isFailed = YES;
            weakSelf.isUploading = NO;
            weakSelf.isUploadOK = NO;
        }];
    }
    else{
        DLog(@"获取图片失败");
    }
}

@end
