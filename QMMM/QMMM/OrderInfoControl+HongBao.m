//
//  OrderInfoControl+HongBao.m
//  QMMM
//
//  Created by kingnet  on 15-3-27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "OrderInfoControl+HongBao.h"

@implementation OrderInfoControl (HongBao)

- (void)setLabelText
{
    UITableViewCell *numCell = [self.cellsArray objectAtIndex:0];
    UILabel *label1 = (UILabel *)[numCell viewWithTag:100];
    label1.text = @"兑换数量";
    UITableViewCell *totalPriceCell = [self.cellsArray objectAtIndex:3];
    UILabel *label2 = (UILabel *)[totalPriceCell viewWithTag:200];
    label2.text = @"兑换总价";
    UITableViewCell *payCell = [self.cellsArray objectAtIndex:5];
    UILabel *label3 = (UILabel *)[payCell viewWithTag:300];
    label3.text = @"红包支付：";
}

- (void)resetGoodsNum
{
    [self setNum:self.goodsNum-1];
    [self calculatePrice];
}

@end
