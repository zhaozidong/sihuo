//
//  Constants.h
//  QMMM
//  系统常量
//  Created by kingnet  on 14-8-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>


//APP INFO
//#define VERSION @"1.0"

#define kFontName @"HelveticaNeue"
#define kAppKey @"97134cfeaafea1d8adfaa4d6515a9672"
#define kAppErrorDomain @"com.kingnet.sihuo"
#define kAppChannel @"test"//app渠道来源
#define UMENG_APPKEY @"551529b4fd98c522500000bf"

#define kISSmall ([[UIScreen mainScreen]bounds].size.height > 568 ? NO:YES)//4寸及以下屏幕

#define IS_IPHONE_4 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )480 ) < DBL_EPSILON )
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define kIOS7  ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7 ? YES:NO)//iOS7及以后的系统

#define IS_IOS7 (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1 && floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1)
#define IS_IOS8  ([[[UIDevice currentDevice] systemVersion] compare:@"8" options:NSNumericSearch] != NSOrderedAscending)


//SIZE
#define kMainFrameHeight  ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7 ? [ [UIScreen mainScreen]bounds].size.height : [ [UIScreen mainScreen]applicationFrame].size.height)

#define kMainFrameWidth ([[UIScreen mainScreen]applicationFrame].size.width)

#define kMainTopHeight (kIOS7 ? 64.f:0.f)

#define kSideBorder 10.f
#define kCameraTopHeight ([[UIScreen mainScreen]bounds].size.height > 480 ? 50.f:0.0f)
#define kSlideImageGapHeight 24.f
#define kDefaultGoodsSize (20)
#define kDefaultProgressSize 44.f

// NSError userInfo key that will contain response data
#define HTTPResponseSerializerWithDataKey @"HTTPResponseSerializerWithDataKey"
#define kUserId @"uid"
#define kIsLogin @"isLogin" //是否在登录状态
#define kCdnhost @"Cdnhost" //静态文件Host
#define kMsgServerPwd @"msgServerPwd" //登录还信服务器的密码
#define kVersionUpdateTip @"versionUpdateTip" //每次版本更新只提示一次
#define kGoodsCategory @"goodsCategory" //商品分类
#define kEverShowNotifyTip @"everShowNotifyTip" //是否提示过打开通知提醒
#define kVersionRedPonitTip @"versionRedPonitTip" //每次版本更新时在标记小红点

// NSNotification
#define kUpdateImageNotification @"UpdateImageNotification"//add by zhaozd
#define kAddImageNotification @"AddImageNotification"
#define kDeleteImageNotification @"DeleteImageNotification"
#define kShowPickerViewNotification @"ShowPickerViewNotification"
#define kGoodsListLoadedNotify @"kGoodsListLoadedNotify"
#define kGoodsListLoadStateNotify @"kGoodsListLoadStateNotify"
#define kGoodsAudioPlayNotify @"kGoodsAudioPlayNotify"
#define kGoodsAudioStopNotify @"kGoodsAudioStopNotify"
#define kGoodsFilterConditionNotify @"kGoodsFilterConditionNotify"
#define kGoodsUpdateFavorNotify @"kGoodsUpdateFavorNotify"
#define kServerGoodsNumUpdateNotify @"kServerGoodsNumUpdateNotify" //add by zhaozd
#define kRefreshOrderState @"kRefreshOrderState"
#define kMyCacheListLoadedNotify @"kMyCacheListLoadedNotify"
#define kMyCacheListLoadStateNotify @"kMyCacheListLoadStateNotify"
#define kFinanceChangedNotify @"kFinanceChangedNotify"
#define kMineInfoTextChangeNotify @"kMineInfoTextChangeNotify"
#define kUserInfoUpdateNotify @"kUserInfoUpdateNotify"
#define kShowImageOneTapNotity @"kShowImageOneTapNotity"
#define kLoadSystemMsgListNotify @"kLoadSystemMsgListNotify"
#define kSelectExpressNameNotify @"kSelectExpressNameNotify"
#define kScrollToTopNotify  @"kScrollToTopNotify"

#define kFlushMsgUnreadCountNotify @"kFlushMsgUnreadCountNotify"
#define kDeleteGoodsNotify @"kDeleteGoodsNotify"

#define kUpdateGoodsSimpleInfoNotify @"kUpdateGoodsSimpleInfoNotify"
#define kUpdateOrderStatusNotify @"kUpdateOrderStatusNotify"
#define kSwitchPageNotify @"kSwitchPageNotify"
#define kReloadSystemMsgListNotify @"kReloadSystemMsgListNotify"
#define kReloadChatUserListNotify @"kReloadChatUserListNotify"
#define kNotNetworkErrorNotity @"kNotNetworkErrorNotity"
#define kFriendListLoadedNotify @"kFriendListLoadedNotify"
#define kFriendListChangedNotify @"kFriendListChangedNotify"
//有人申请加为好友
#define kRefreshMsgListNotify @"kRefreshMsgListNotify"
//钱包消息
#define kWalletMsgNotify @"kWalletMsgNotify"
//交易消息
#define kTradeMsgNotify @"kTradeMsgNotify"

#define kUpdateFriendRemarkNotify @"kUpdateFriendRemarkNotify"
#define kAddGoodsCommentNotify @"kAddGoodsCommentNotify"
#define kPublishGoodsOKNotify @"kPublishGoodsOKNotify"
//消息列表拉取消息失败
#define kGetMsgListFailedNotify @"kGetMsgListFailedNotify"
//收到环信消息
#define kUnreadMessagesCountChangedNotity @"kUnreadMessagesCountChangedNotity"
//聊天列表变化通知
#define kUpdateConversationListNotify @"kUpdateConversationListNotify"
//离线消息通知
#define kFinishedReceiveOfflineMessagesNotify @"kFinishedReceiveOfflineMessagesNotify"
//加载朋友列表失败
#define kFriendListLoadedFailedNotify @"kFriendListLoadedFailedNotify"
//收到消息
#define kDidReceiveMessageNotify @"kDidReceiveMessageNotify"
//删除订单完成
#define kDeleteOrderOKNotify @"kDeleteOrderOKNotify"
//修改价格
#define kUpdateOrderPriceNotify @"kUpdateOrderPriceNotify"
//提现成功
#define kWithdrawSuccessNotify @"kWithdrawSuccessNotify"
//显示评价
#define kShowCommentsNotify @"kShowCommentsNotify"
//收到新手红包的通知
#define kNewerHongbaoNotify @"kNewerHongbaoNotify"
//刷新Html界面
#define kReloadHtmlNotify @"kReloadHtmlNotify"
//刷新列表
#define kUpdateOrderListStatusNotify @"kUpdateOrderListStatusNotify"

//METHOD
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//COLOR
#define kBackgroundColor UIColorFromRGB(0xececec) //软件的背景颜色
#define kTitleColor UIColorFromRGB(0xfe595a)
#define kGrayTextColor UIColorFromRGB(0x7e7e7e)
#define kLightGrayTextColor UIColorFromRGB(0xbfbfbf)
#define kRedColor UIColorFromRGB(0xf5292c) //软件通用的红色
#define kBorderColor UIColorFromRGB(0xd8dee6) //边框颜色
#define kDarkTextColor UIColorFromRGB(0x4f4f4f) //软件通用的黑色
#define kCellSelectedColor UIColorFromRGB(0xd9d9d9) //UITableViewCell的选中颜色
#define kGrayCellColor UIColorFromRGB(0xededed)
#define kPlaceholderColor UIColorFromRGB(0xb7bfc9) //placeholder的颜色
#define kBlueColor UIColorFromRGB(0x819fe0)
#define kLightBlueColor UIColorFromRGB(0x9198a3) //在订单详情页与发布页用到的text color
#define kGreenTextColor UIColorFromRGB(0x097e00)
#define kYellowTextColor UIColorFromRGB(0xff9122)

//Word Limit
#define kWordLimitNum 140
//GoodsPhoto Limit
#define kGoodsPhotoLimitNum 10
//PersonPhoto Limit
#define kPersonPhotoLimitNum 8
//RejectRefund Reason Limit
#define kRejectRefundLimitNum 70

//database current version
#define CURRENT_DB_VERSION (7)

#define kAppleID @"955693333"

//my global queue
#define kMyGlobalQueueId "com.kingnet.sihuo"

#define PAY_PARTNER @"2088711776731084"
#define PAY_SELLER @"zhifubao@sihuo.com"

#define PAY_PRIVATEKEY @"MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMq+sFEXj08d+qIv5L7r0Biy0noYeD5+9Um5B5keBKKgRw1dDv4gEfMr8REqOVp76Foyvr/rYXpk4fLGioGVr3TxZLsvmBTV6eHsIK4q7Ao3DJVnClXowFF2BYgjkaq5xKGuswQ3s10AgFvvsWn8h5YN+x9pMRJJ1Qlc4z6+ynyDAgMBAAECgYAtIefkFWeMiDLr+ddk/N+VL/GUxm+2EdMOt0bOuhT0EtPeULHutjrotAZ0+L5NuzmNIrC9zmsjsONvhOz+TdEBxNeLjAJK4gu2VurJKRnnCwoT7/EZI2gP+IWLFmJFy8JoBbOK+Ez0tBG3M/PVLB81tsndV+Y3B7VpfO2Hft/xsQJBAO4IRdEaIQmELxtQC3zd+4vy9t/Dja4qufHHwewk9IW17CQjWkKwD7EhGC9/nc0fdmKULMZoib7vyVOVIBnM5BkCQQDaDIXfHiUizt8Ks65H3/mhRXiFMA6IXmYBQguQvCVdsa0iVjyfHo2agMaJsQO5RT4CVvWxenLI+H8LGD3WOJj7AkEA5x3vNC2LoGLRHlLlJJOg12zMWc3VNiPUMbdESlZDecR/CUOuMwwh4FmC0zO082K2HtdCdk8wV80ZDOfBqGtxmQJBANMWrn76Ou9AoBJHMmAohwzGKU+e32LLKCrtmi6qxe23dSm5nIRaWrGJGyrvL/CrBMXVE5OWPAss7ih6dlqtfgMCQHJ+tRbqc4lK2OSUP4P+iQGs8MqmatLXJu6JuqjWYxENIfQDxY6YtR3x5fXpXyPXaG3kHhXDUKIEp5V0BGwHkDQ="

//判断在release环境下是否是test的
#define kIsTestRelease ([[[NSBundle mainBundle] bundleIdentifier] isEqualToString:@"com.kingnet.qmmm"])

typedef void (^contextBlock)(id);

@interface Constants : NSObject

@end
