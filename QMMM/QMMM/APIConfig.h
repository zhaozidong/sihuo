//
//  APIConfig.h
//  QMMM
//  配置请求的
//  Created by kingnet  on 14-8-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

/***************SERVER API***************/

//登录
#define API_LOGIN @"/?c=user&a=login"
//注册
#define API_REGISTER @"/?c=user&a=register"
//短信
#define API_SMS @"/?c=sms&a=send"
//修改密码
#define API_CHANGE_PWD @"/?c=user&a=changePassword"
//更新昵称
#define API_UPDATE_NICKNAME @"/?c=user&a=updateNickname"
//找回密码
#define API_FIND_PWD @"/?c=user&a=recoveryPassword"
//上传头像
#define API_UPLOAD_AVARTA @"/?c=user&a=updateAvatar"
//上传图片 type为 goods或avatar
#define API_UPLOAD_FILE @"/?c=uploadAsset&a=image&type=%@"
//商品列表
//#define URL_RECOMMEND_GOODS_LIST @"/?c=goods&a=getGoodsList"
#define URL_RECOMMEND_GOODS_LIST @"/?c=goods&a=getClassList"
//上传录音
#define API_UPLOAD_AUDIO @"/?c=uploadAsset&a=audio"
//发布商品
#define API_RELEASE_GOODS @"/?c=goods&a=publish"
//上报错误日志
#define API_REORD_ERROR @"/?c=stat&a=clientError"
//商品详情
#define API_GOODS_DETAIL @"/?c=goods&a=getDetails"
//评论列表
#define API_COMMENT_LIST @"/?c=comment&a=getList"
//赞
#define API_ADD_FAVOR @"/?c=goods&a=praise"
//发表评论
#define API_SEND_COMMENT @"/?c=comment&a=add"
//搜索
#define API_SEARCH_GOODS @"/?c=search"
//获取商品分类
#define API_GOODS_CATEGORY @"/?c=category&a=getList&format=tree"
//订单列表
#define API_ORDER_LIST @"/?c=pay&a=getTradeList"
//订单详情
#define API_ORDER_DETAIL @"/?c=pay&a=getOrderDetails"
//卖家发货
#define API_DELIVER_GOODS @"/?c=pay&a=shipOrder"
//买家确认收货，并打分
#define API_DO_SCORES @"/?c=pay&a=rateOrder"
//赞列表
#define API_FAVOR_LIST @"/?c=goods&a=getPraise"
//删除评论
#define API_DELETE_COMMIT @"/?c=Comment&a=delete"
//已购买商品列表
#define API_BOUGHT_GOODS_LIST @"/?c=goods&a=getMyGoods"
//系统消息列表
#define API_SYSTEM_CHAT_LIST @"/?c=message&a=getList"
//删除消息
#define API_DELETE_SYSTEM_MSG @"/?c=message&a=del"

//查询收获地址
#define API_ADDRESS_LIST @"/?c=consignee&a=getAll"
//新增地址
#define API_NEW_ADDRESS @"/?c=consignee&a=add"
//更新收货地址
#define API_UPDATE_ADDRESS @"/?c=consignee&a=update"
//删除收货地址
#define API_DELETE_ADDRESS @"/?c=consignee&a=delete&id=%d"

//获取余额
#define API_USER_FINANCE @"/?c=finance&a=getUserFinanceInfo"
//提现
#define API_DRAW_MONEY @"/?c=finance&a=userDrawMoney"
//红包提现
#define API_HONGBAO_DRAW_MONEY @"/?c=HongBao&a=userDrawMoney"
//提现列表
#define API_DRAW_LIST @"/?c=finance&a=getBalanceList"
//红包提现列表
#define API_HONGBAO_DRAW_LIST @"/?c=HongBao&a=getBalanceList"
//上传生活照
#define API_LIFE_PHOTOS @"/?c=user&a=updateLifePhoto"
//更新用户信息
#define API_UPDATE_USERINFO @"/?c=user&a=updateProfile"
//设置提现账户
#define API_DRAW_ACCOUNT @"/?c=finance&a=updatePayeeInfo"
//更新商品信息处拉取商品信息
#define API_GOODS_INFO @"/?c=goods&a=getGoodsInfo"
//更新商品信息
#define API_UPDATE_GOODS @"/?c=goods&a=setGoods"
//上传通讯录
#define API_UPLOAD_CONTACTS @"/?c=friend&a=uploadContacts"
//获取百度经纬度
#define API_BAIDU_COORDS @"http://api.map.baidu.com/geoconv/v1/?from=1&ak=Zf1CgyTr4YiC2RWsE39T21jj&sn=iIjiBubmANj7qet0mESjRmuIWq1rsUz1"
//获取详细地理位置
#define API_DETAIL_LOC @"http://api.map.baidu.com/geocoder/v2/?ak=Zf1CgyTr4YiC2RWsE39T21jj&output=json"
//更新商品状态
#define API_GOODS_STATUS @"/?c=goods&a=updateGoods"
//订单提交之后的页面
#define API_AFTER_ORDER @"?c=pay&a=saveOrder&goods_id=%qu&payment_type=%d&num=%d&address_id=%d&remain_pay_amount=%.2f"

//创建订单
#define API_CREATE_ORDER @"/?c=pay&a=saveOrder"

//支付订单
#define API_PAY_ORDER @"/?c=order&a=payOrder"


//设备token
#define API_REGISTER_DEVICE_TOKEN @"/?c=user&a=updateDeviceToken"
//点击关注
#define API_WATCH @"/?c=friend&a=follow"
//获取已售出的商品-个人主页
#define API_SOLDOUT_GOODS @"/?c=pay&a=soldGoodsList&uid=%@&goods_id=%@"

//获取正在售的商品—个人主页
#define API_SELLING_GOODS @"/?c=goods&a=getMyHomeGoodsList"

//获取个人主页的个人信息
#define API_PERSON_PAGE_INFO @"/?c=user&a=home&uid=%@&fields=%@"

//关注
#define API_ADD_WATCH @"/?c=friend&a=follow"

//共同好友
#define API_COMMON_FRIENDS @"/?c=user&a=prettyCoFriends&uid=%@"

//获取用户信息
#define API_GET_USER_BASIC_INFO @"/?c=user&a=profile"

//检测新版本
#define API_CHECK_VERSION @"/?c=version&a=ios"

//意见反馈
#define API_SUBMIT_FEEDBACK @"/?c=feedback&a=index"

//用户协议
#define API_USER_PROTOCOL @"/?c=index&a=registerAgreement"

//上报设备信息
#define API_REPORT_DEVINFO @"/?c=TongJi&a=deviceActive"

//统计使用时间
#define API_REPORT_USETIME @"/?c=TongJi&a=retention"

//统计注册
#define API_REPORT_REGISTER @"/?c=TongJi&a=register"

////////////////////////////////////////////////////////////////////////////////////////////////////
//好友相关接口

//好友列表
#define API_GET_FRIEND_LIST @"/?c=friend&a=getList&fields=%@"

//邀请好友
#define API_INVITE_FRIEND @"/?c=friend&a=invite"

//接受好友
#define API_ACCEPT_FRIEND @"/?c=friend&a=friendAccept"

//修改好友备注
#define API_MODIFY_FRIEND_REMARK @"/?c=friend&a=setFriendRemark"

//查看物流信息
#define API_CHECK_EXPRESSINFO @"/?c=express&a=detail"

//获取影响力与靠谱度
#define API_INFLUENCE_NUM @"/?c=friend&a=getInfluence"

//获取待收货数和待确认收货数
#define API_ORDERSTATE_NUM @"/?c=pay&a=getTodoNum"

//申请加为好友
#define API_ADD_FRIEND @"/?c=friend&a=friendRequest"

//获取评价列表
#define API_EVALUATE_LIST @"/?c=pay&a=getRateList"

//退出登录
#define API_LOGOUT @"/?c=user&a=logout"

//创建订单
#define API_SUBMIT_ORDER @"/?c=order&a=newOrder"

//删除订单
#define API_DELETE_ORDER @"/?c=Order&a=deleteOrder"

//校验验证码
#define API_VERIFY_CHECKCODE @"/?c=sms&a=verify"

//取消订单
#define API_CANCEL_ORDER @"/?c=order&a=cancelOrder"

//获取活动内容
#define API_GET_ACTIVITYINFO @"/?c=Goods&a=getActivitiesInfo"

//获取所有往期专题
#define API_GET_ALLTOPIC @"/?c=topic&a=getList"

//获取专题页的商品列表
#define API_GET_SPECIALGOODSLIST @"/?c=goods&a=getClassList"

//获取引导页商品列表
#define API_GET_INTROGOODS @"/?c=RollInfo&a=rollInfo"

//获取商品所属的分类
#define API_GET_CATEGORY @"/?c=category&a=getAutoCategory"

//获取可能认识的人
#define API_GET_PROBABLYKNOW @"/?c=friend&a=mayKnow&fields=uid,nickname,avatar"

//红包细则
#define API_HONGBAO_RULES @"/?c=hongBao&a=rules"

//举报
#define API_REPORT @"/?c=Report&a=report"


//////////////////////////////////////////////////////////
//卖家关闭订单并退款
#define API_ORDER_SELLERREFUND @"/?c=PayRefund&a=sellerRefund"
//买家申请退款
#define API_ORDER_APPLYREFUND @"/?c=PayRefund&a=applyRefund"
//卖家同意退款
#define API_ORDER_ACCEPTREFUND @"/?c=PayRefund&a=acceptRefund"
//卖家拒绝退款
#define API_ORDER_REJECTREFUND @"/?c=PayRefund&a=rejectRefund"

////////////////////////////////////////////////////////
//红包处的邀请web界面
#define API_HONGBAO_INVITE @"/?c=HongBao&a=invite"
//可兑换的商品列表
#define API_HONGBAO_EXCHANGE_LIST @"/?c=HongBao&a=goodsList"
//拉取新手红包的数量
#define API_HONGBAO_MONEY @"/?c=HongBao&a=getRegisterHongBaoMoney"
//提交兑换商品的订单
#define API_HONGBAO_BUY_GOODS @"/?c=HongBao&a=exchangeGoods"


@interface APIConfig : NSObject

@end
