//
//  QMFilterTableViewCell.h
//  QMMM
//
//  Created by kingnet  on 15-3-17.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMFilterTableViewCellIdentifier;

@interface QMFilterTableViewCell : UITableViewCell

@property (nonatomic, copy) NSString * text;

@property (nonatomic, assign) BOOL choosed;

+ (UINib *)nib;

@end
