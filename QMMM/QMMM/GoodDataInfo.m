//
//  GoodDataInfo.m
//  QMMM
//
//  Created by 韩芦 on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodDataInfo.h"
#import "NSArray+JSONCategories.h"

@implementation FavorInfo

- (NSDictionary *)dict
{
    return [NSDictionary dictionaryWithObjectsAndKeys:@(_uid), @"uid", _uicon, @"img",nil];
}

@end

@implementation FavorList

- (id)init
{
    self = [super init];
    if(self) {
        _dict = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (id)initWithArray:(NSArray *)rawdata
{
    self = [super init];
    if(self) {
        _favorArray = [[NSMutableArray alloc] init];
         _dict = [[NSMutableDictionary alloc] init];
        
        for (NSDictionary * dict in rawdata) {
            FavorInfo * info = [[FavorInfo alloc] init];
            
            id temp = [dict objectForKey:@"uid"];
            if(temp && [temp isKindOfClass:[NSString class]]) {
                info.uid = (uint64_t)[temp longLongValue];
            } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
                info.uid = [temp unsignedLongLongValue];
            }
            
            temp = [dict objectForKey:@"img"];
            if(temp && [temp isKindOfClass:[NSString class]]) {
                info.uicon = [dict objectForKey:@"img"];
            } else {
                info.uicon = @"";
            }
            
            temp = [dict objectForKey:@"nickName"];
            if (temp && [temp isKindOfClass:[NSString class]]) {
                info.nickName = [dict objectForKey:@"nickName"];
            }
            else{
                info.nickName = @"";
            }
            
            [_favorArray addObject:info];
            [_dict setObject:info forKey:@(info.uid)];
        }
    }
    return self;
}


- (BOOL)hasFarvor:(uint64_t)uid
{
    return [_dict objectForKey:@(uid)] != nil ? YES : NO;
}

- (void)addFavor:(FavorInfo *)info
{
    if(![self hasFarvor:info.uid]) {
        [_favorArray insertObject:info atIndex:0];
//        [_favorArray addObject:info];
        [_dict setObject:info forKey:@(info.uid)];
    }
}

- (void)deleteFavor:(uint64_t)uid
{
    if([self hasFarvor:uid]) {
        FavorInfo * info = [_dict objectForKey:@(uid)];
        [_favorArray removeObject:info];
        [_dict removeObjectForKey:@(uid)];
    }
}

- (void)deleteFavorList
{
    [_dict removeAllObjects];
    [_favorArray removeAllObjects];
}

- (NSString *)getSaveString
{
    if(_favorArray && [_favorArray count] > 0) {
        NSMutableArray * array = [[NSMutableArray alloc] init];
        for (FavorInfo * info in _favorArray) {
            [array addObject:[info dict]];
        }
        return [array toJSON];
    }
    
    return @"";
}

- (BOOL)isEmpty
{
    return [_favorArray count] > 0 ? NO : YES;
}

@end

@implementation GoodsPictureList

- (id)initWithArray:(NSArray *)rawArray
{
    self = [super init];
    if (self) {
        _rawArray = [NSMutableArray arrayWithArray:rawArray];
    }
    return self;
}

- (NSString *)getSaveString
{
    if(_rawArray && [_rawArray count] > 0) {
        return [_rawArray toJSON];
    }
    return @"";
}

- (BOOL)isEmpty
{
    if(_rawArray && [_rawArray count] > 0) {
        return NO;
    } else {
        return YES;
    }
}

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////
//简要商品信息

@implementation GoodsSimpleInfo

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self && dict && [dict isKindOfClass:[NSDictionary class]]) {
        id temp = [dict objectForKey:@"id"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _gid = (uint64_t)[temp longLongValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _gid = [temp unsignedLongLongValue];
        }
        
        temp = [dict objectForKey:@"desc"];
        if(temp && [temp isKindOfClass:[NSString class]] && [temp length] > 0) {
            _desc = temp;
        } else {
            _desc = @"";
        }
        
        
        temp = [dict objectForKey:@"photo"];
        if(temp && [temp isKindOfClass:[NSString class]] && [temp length] > 0) {
            _photo = [dict objectForKey:@"photo"];
        } else {
            _photo = @"";
        }
        
        temp = [dict objectForKey:@"price"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _price = [temp floatValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _price = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"pub_ts"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _publish_time = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _publish_time = [temp intValue];
        }
        
        temp = [dict objectForKey:@"update_ts"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _update_time = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _update_time = [temp intValue];
        }
        
        temp = [dict objectForKey:@"num"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _num = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _num = [temp intValue];
        }

        temp = [dict objectForKey:@"ori_price"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _orig_price = [temp floatValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _orig_price = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"status"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            _status = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            _status = [temp intValue];
        }
        
        temp = [dict objectForKey:@"trust"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            _trust = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            _trust = [temp intValue];
        }
    }
    return self;
}

- (void)assignValue:(GoodsSimpleInfo *)info
{
    self.gid = info.gid;
    self.desc = info.desc;
    self.photo = info.photo;
    self.price = info.price;
    self.publish_time = info.publish_time;
    self.update_time = info.update_time;
    self.num = info.num;
    self.orig_price = info.orig_price;
    self.status = info.status;
    self.trust = info.trust;
}

- (NSDictionary *)toDictionary
{
    NSDictionary * dict = @{@"id":@(self.gid), @"desc":self.desc, @"photo":self.photo, @"price":@(self.price), @"pub_ts":@(self.publish_time), @"update_ts":@(self.update_time), @"num":@(self.num), @"status":@(self.status), @"ori_price":@(self.orig_price), @"trust":@(self.trust)};
    return dict;
}

@end

