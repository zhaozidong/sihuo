//
//  QMBlankCell.m
//  QMMM
//
//  Created by hanlu on 14-10-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMBlankCell.h"

NSString * const kQMBlankCellIdentifier = @"kQMBlankCellIdentifier";

@implementation QMBlankCell

+ (id)cellForBlank
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMBlankCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Initialization code
    
    self.backgroundColor = [UIColor clearColor];
    
    _tipsLabel.text = @"";
    _tipsLabel.preferredMaxLayoutWidth = kMainFrameWidth;
    _tipsLabel.textColor = [UIColor lightGrayColor];
    _tipsLabel.numberOfLines = 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTips:(NSString *)tips
{
    _tipsLabel.text = tips;
}

- (void)setFailGoodsType:(QMFailGoodsType)failGoodsType
{
    switch (failGoodsType) {
        case QMFailTypeNOSellingGoods:
        {
            _defaultImageView.image = [UIImage imageNamed:@"errorLoadFail"];
            _tipsLabel.text = @"暂无商品，快去发布一个吧";
        }
            break;
        case QMFailTypeNOBoughtGoods:
        {
            _defaultImageView.image = [UIImage imageNamed:@"errorLoadFail"];
            _tipsLabel.text = @"一件商品都没有？\n赶快买！买！买！！！";
        }
            break;
        case QMFailTypeNOSoldGoods:
        {
            _defaultImageView.image = [UIImage imageNamed:@"errorLoadFail"];
            _tipsLabel.text = @"你的商品还没有卖出去";
        }
            break;
        default:
            break;
    }
}

@end
