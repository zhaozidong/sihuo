//
//  QMFavorMsgCell.m
//  QMMM
//
//  Created by hanlu on 14-10-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMFavorMsgCell.h"
#import "MsgData.h"
#import "NSString+JSONCategories.h"
#import "UIImageView+QMWebCache.h"

#define MARGIN 10

NSString * const kQMFavorMsgCellIdentifier = @"kQMFavorMsgCellIdentifier";

@implementation QMFavorMsgCell

+ (id)cellForFavorMsg
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMFavorMsgCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
    [_timeLabel setTextColor:UIColorFromRGB(0x9198a3)];
    [_nameLabel setTextColor:UIColorFromRGB(0xfe595A)];
    [_favorLabel setTextColor:kDarkTextColor];
    [_descLabel setTextColor:UIColorFromRGB(0x9198a3)];
    
    [_descLabel setNumberOfLines:0];
    
    _flagImageView.image = [UIImage imageNamed:@"text_prefix_bkg"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(MsgData *)data
{
    NSDictionary * dict = [data.extData toDict];
    
    NSString * temp = [dict objectForKey:@"nickname"];
    if([temp length] > 0) {
        _nameLabel.text = temp;
    } else {
        _nameLabel.text = @"";
    }
    
    _timeLabel.text = [AppUtils stringFromDate:[NSDate dateWithTimeIntervalSince1970:data.msgTime]];
    _favorLabel.text = @"赞了你的宝贝";
    
    temp = [dict objectForKey:@"pic"];
    if([temp length] > 0) {
        NSString *tempUrl = [AppUtils thumbPath:temp sizeType:QMImageQuarterSize];
        [_goodsImageView qm_setImageWithURL:tempUrl placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_middle"]];
    } else {
        _goodsImageView.image = [UIImage imageNamed:@"goodsPlaceholder_middle"];
    }
    
    _descLabel.text = @"";
//    _descLabel.backgroundColor = [UIColor redColor];
    
    temp = [dict objectForKey:@"goods_desc"];
    if([temp length] > 0) {
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init] ;
        [paragraphStyle setFirstLineHeadIndent:26];
        [paragraphStyle setLineSpacing:6.0];
        [paragraphStyle setLineBreakMode:NSLineBreakByCharWrapping];
        
        //属性字符串设置属性
        NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:temp];
        [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.0] range:NSMakeRange(0, [attributeString length])];
        [attributeString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x9198a3) range:NSMakeRange(0, [attributeString length])];
        [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attributeString length])];
        
        //textSize
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat margin = MARGIN * 2  + 90 + 15;
        CGRect rect = [attributeString boundingRectWithSize:CGSizeMake(width - margin, 10000) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        if(rect.size.height > 44) {
            rect.size.height = 50;
        }
        CGRect frame = _descLabel.frame;
        frame.size.height = rect.size.height;
        _descLabel.frame = frame;
        
        [paragraphStyle setLineBreakMode:NSLineBreakByTruncatingTail];
        
        _descLabel.attributedText = attributeString;
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = _flagImageView.frame;
    frame.origin.x = _descLabel.frame.origin.x;
    frame.origin.y = _descLabel.frame.origin.y + 3;
    _flagImageView.frame = frame;
}

@end
