//
//  DrawListTopView.h
//  QMMM
//
//  Created by kingnet  on 15-2-5.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class FinanceUserInfo;

@protocol DrawListTopViewDelegate <NSObject>

- (void)didTapRulesBtn;

@end

@interface DrawListTopView : UIView

@property (nonatomic, weak) id<DrawListTopViewDelegate> delegate;

+ (DrawListTopView *)topView;

- (void)updateUI:(FinanceUserInfo *)info isBalance:(BOOL)isBalance;

+ (CGFloat)viewHeight;

@end
