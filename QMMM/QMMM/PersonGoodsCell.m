//
//  PersonGoodsCell.m
//  QMMM
//
//  Created by kingnet  on 14-11-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PersonGoodsCell.h"
#import "PersonGoodsItem.h"

NSString * const kPersonGoodsCellIdentifier = @"kPersonGoodsCellIdentifier";

@implementation PersonGoodsCell
@synthesize items = _items;

- (id)initWithItems:(NSArray *)items
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kPersonGoodsCellIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.items = items;
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setItems:(NSArray *)items
{
    @synchronized(self)
    {
        if (_items != items) {
            _items = items;
            for (UIView *view in [self.contentView subviews]) {
                [view removeFromSuperview];
            }
            
            for (PersonGoodsItem *goodsItem in _items) {
                [self.contentView addSubview:goodsItem];
            }
            [self setItemPosition];
        }
    }
}

- (NSArray *)items
{
    NSArray *array = nil;
    
    @synchronized (self)
    {
        array = _items;
    }
    
    return array;
}

- (void)setItemPosition
{
    self.frame = CGRectMake(0, 0, self.frame.size.width, [PersonGoodsItem itemSize].height+[PersonGoodsItem margin]);
    self.contentView.frame = self.bounds;
    float x = [PersonGoodsItem margin];
    for (PersonGoodsItem *goodsItem in _items) {
        goodsItem.translatesAutoresizingMaskIntoConstraints = NO;
        NSDictionary *views = NSDictionaryOfVariableBindings(goodsItem);
        NSDictionary *metrics = @{@"margin":@([PersonGoodsItem margin])};
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[goodsItem]-margin-|" options:0 metrics:metrics views:views]];
        metrics = @{@"x":@(x), @"w":@([PersonGoodsItem itemSize].width)};
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-x-[goodsItem(w)]" options:0 metrics:metrics views:views]];
        x += [PersonGoodsItem itemSize].width + [PersonGoodsItem margin];
    }
}

@end
