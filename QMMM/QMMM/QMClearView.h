//
//  QMClearView.h
//  QMMM
//
//  Created by Shinancao on 14/10/26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMClearView : UIView

+ (UINib *)nib;

@end
