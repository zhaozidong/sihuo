//
//  QMPhotoGuide.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMPhotoGuide.h"
#import "UserDefaultsUtils.h"

@interface  QMPhotoGuide ()<UIGestureRecognizerDelegate>{
    UIImageView *_imageView1;
    UIImageView *_imageView2;
}
@end


@implementation QMPhotoGuide

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)init{
    self=[super init];
    if (self) {
        BOOL isShowImageView1=[UserDefaultsUtils boolValueWithKey:@"photoGuideTakeAnother"];
        UIImage *img;
        if (!isShowImageView1) {
            _imageView1=[[UIImageView alloc] init];
            img=[UIImage imageNamed:@"photoGuideTakeAnother"];
            _imageView1.frame=CGRectMake(0, 0, img.size.width, img.size.height);
            _imageView1.image=img;
            
//            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImageView1:)];
//            [singleTap setNumberOfTapsRequired:1];
//            singleTap.delegate=self;
//            [_imageView1 addGestureRecognizer:singleTap];
            [self addSubview:_imageView1];
        }
        
        BOOL isShowImageView2=[UserDefaultsUtils boolValueWithKey:@"photoGuideTap"];
        if (!isShowImageView2) {
            _imageView2=[[UIImageView alloc] init];
            img=[UIImage imageNamed:@"photoGuideTap"];
            _imageView2.frame=CGRectMake(0, 0, img.size.width, img.size.height);
            _imageView2.image=img;
            

//            UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImageView2:)];
//            [singleTap2 setNumberOfTapsRequired:1];
//            singleTap2.delegate=self;
//            [_imageView2 addGestureRecognizer:singleTap2];
            [self addSubview:_imageView2];
        }
        UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSelf:)];
        [singleTap2 setNumberOfTapsRequired:1];
        singleTap2.delegate=self;
        [self addGestureRecognizer:singleTap2];
        self.backgroundColor=[UIColor clearColor];
//        self.layer.opacity=0.8f;
    }
    return self;
}

//- (void)tapImageView1:(UIGestureRecognizer *)gestureRecognizer{
//    [UserDefaultsUtils saveBoolValue:YES withKey:@"photoGuideTakeAnother"];
//    [self removeFromSuperview];
//}
//
//- (void)tapImageView2:(UIGestureRecognizer *)gestureRecognizer{
//    [UserDefaultsUtils saveBoolValue:YES withKey:@"photoGuideTap"];
//    [self removeFromSuperview];
//}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]]) {
    }
    
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if(touch.view == self || touch.view == _imageView1 || touch.view == _imageView2)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


- (void)tapSelf:(UIGestureRecognizer *)gestureRecognizer{
    [UserDefaultsUtils saveBoolValue:YES withKey:@"photoGuideTakeAnother"];
    [UserDefaultsUtils saveBoolValue:YES withKey:@"photoGuideTap"];
    [self removeFromSuperview];
}

-(void)layoutSubviews{
    if (kISSmall) {//小尺寸屏幕
        _imageView1.center=CGPointMake(kMainFrameWidth*0.65, kMainFrameHeight*0.8);//再拍一张
        _imageView2.center=CGPointMake(kMainFrameWidth*0.35, kMainFrameHeight*0.7);//
    }else{//大屏
        _imageView1.center=CGPointMake(kMainFrameWidth*0.6, kMainFrameHeight*0.84);//再拍一张
        _imageView2.center=CGPointMake(kMainFrameWidth*0.3, kMainFrameHeight*0.7);//
    }
}


@end
