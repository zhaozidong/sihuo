//
//  QMGoodsFavorlistCell.m
//  QMMM
//
//  Created by 韩芦 on 14-9-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsFavorlistCell.h"
#import "GoodEntity.h"
#import "GoodDataInfo.h"
#import "QMClickNameView.h"

#define MARGIN 8

NSString * const kQMGoodsFavorlistCellIndentifier = @"kQMGoodsFavorlistCellIndentifier";

@interface QMGoodsFavorlistCell ()<QMClickNameViewDelegate> {
    NSMutableArray * _headBtnArray;
    NSMutableArray *nameViewArray_;
}

@property (weak, nonatomic) IBOutlet UIImageView *praiseIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgTopHeightCst;

@end

@implementation QMGoodsFavorlistCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"QMGoodsFavorlistCell" bundle:nil];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(GoodEntity *)goodsInfo
{
    NSArray * array =[NSArray arrayWithArray:goodsInfo.favorList.favorArray];
    for (QMClickNameView *view in nameViewArray_) {
        [view removeFromSuperview];
    }
    [nameViewArray_ removeAllObjects];
    if (nameViewArray_ == nil) {
        nameViewArray_ = [NSMutableArray array];
    }
    
    int lineCount=1; //放置的行数
    float maxWidth = kMainFrameWidth - 2*MARGIN - 16 -15*2;
    float totalW = 0, startX = CGRectGetMaxX(self.praiseIcon.frame)+10;
    if (array && array.count >0) {
        for (FavorInfo *info in array) {
            QMClickNameView *nameView = [[QMClickNameView alloc]init];
            nameView.userId = info.uid;
            if (info == [array lastObject]) {
                nameView.name = info.nickName;
            }
            else{
                nameView.name = [NSString stringWithFormat:@"%@,", info.nickName];
            }
        
            nameView.delegate = self;
            nameView.height = [[self class]cellHeight];
            if (nameView.width > maxWidth) { //如果一个名字就超过了一行，则只显示一行
                nameView.width = maxWidth;
            }
            
            if ((totalW+nameView.width) > maxWidth) {
                lineCount++; //换行
                totalW = 0;
            }
            
            //在界面上添加view
            [self.contentView addSubview:nameView];
            nameView.frame = CGRectMake(totalW+startX, (lineCount -1)*[[self class]cellHeight], nameView.width, nameView.height);
            [nameViewArray_ addObject:nameView];
            
            totalW += nameView.width;
        }
    }
    
    //如果只有一行重新排布控件的的高度
    if (lineCount<=1) {
        totalW = CGRectGetMaxX(self.praiseIcon.frame)+10;
        for (QMClickNameView *view in nameViewArray_) {
            view.height = 36.f;
            view.frame = CGRectMake(totalW, 0, view.width, view.height);
            totalW += view.width;
        }
    }
    else{
        self.imgTopHeightCst.constant = 5.f;
        [self layoutIfNeeded];
    }
}

+ (CGFloat)heightWithGoodsEntity:(GoodEntity *)goodsInfo
{
    NSArray * array =[NSArray arrayWithArray:goodsInfo.favorList.favorArray];

    int lineCount=1; //放置的行数
    float maxWidth = kMainFrameWidth - 2*MARGIN - 16 -15*2; //去掉空格等宽度剩下的可以排控件的距离
    float totalW = 0;
    if (array && array.count >0) {
        for (FavorInfo *info in array) {
            QMClickNameView *nameView = [[QMClickNameView alloc]init];
            if (info == [array lastObject]) {
                nameView.name = info.nickName;
            }
            else{
                nameView.name = [NSString stringWithFormat:@"%@,", info.nickName];
            }
            
            nameView.height = [[self class]cellHeight];
            if (nameView.width > maxWidth) { //如果一个名字就超过了一行，则只显示一行
                nameView.width = maxWidth;
            }

            if ((totalW+nameView.width) > maxWidth) {
                lineCount++; //换行
                totalW = 0;
            }
            totalW += nameView.width;
        }
    }
    if (lineCount <= 1) {
        return 36.f;
    }
    else{
        return lineCount * [[self class]cellHeight];
    }
}

+ (CGFloat)cellHeight
{
    return 25.f;
}

#pragma mark - QMNameView Delegate
- (void)clickNameViewWithUid:(uint64_t)userId
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickNameViewWithUid:)]) {
        [self.delegate clickNameViewWithUid:userId];
    }
}

@end
