//
//  QMFriendsRelationCell.h
//  QMMM
//
//  Created by Derek on 15/1/27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QMRoundHeadView.h"
#import "UserDataInfo.h"

static NSString *kQMFriendsRelationCell=@"QMFriendsRelationCell";


@protocol QMFriendsRelationCellDelgate <NSObject>

-(void)clickCellWithUserId:(uint64_t)uid;

@end




@interface QMFriendsRelationCell : UITableViewCell<QMRoundHeadViewDelegate>

@property (strong, nonatomic) IBOutlet QMRoundHeadView *headView;

@property (strong, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) id<QMFriendsRelationCellDelgate> delgate;


+ (QMFriendsRelationCell *)cellAwakeFromNIB;

-(void)updateUIWithUserInfo:(UserDataInfo *)userInfo;


@end
