//
//  QMGoodCateParser.h
//  QMMM
//
//  Created by hanlu on 14-9-19.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface QMGoodsCateInfo : BaseEntity

@property (nonatomic, assign) NSUInteger cateId; //类别id

@property (nonatomic, strong) NSString * name; //类别名称

@property (nonatomic, strong) NSString * desc; //类别描述

@property (nonatomic, strong) NSString * iconURL; //icon路径

- (id)initWithDictionary:(NSDictionary *)dict;

@end

@interface QMGoodsCateParser : NSObject

@property (nonatomic, strong) NSMutableArray * catesList;

- (BOOL)loadGoodsCateXml;

@end
