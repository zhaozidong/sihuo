//
//  GoodsDetailToolBar.m
//  QMMM
//
//  Created by kingnet  on 14-9-18.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsDetailToolBar.h"
#import "QMSubmitButton.h"
#import "UIButton+Addition.h"

@interface GoodsDetailToolBar ()

@property (weak, nonatomic) IBOutlet QMSubmitButton *submitBtn; //在库存为0时不可用
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;

@end
@implementation GoodsDetailToolBar

+ (GoodsDetailToolBar *)toolBar
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"GoodsDetailToolBar" owner:self options:nil];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.chatBtn setTitle:@"私聊" forState:UIControlStateNormal];
    [self.chatBtn setImage:[UIImage imageNamed:@"chat"] forState:UIControlStateNormal];
    [self.chatBtn setButtonType:BT_LeftImageRightTitle interGap:8.f];
    [self.chatBtn setTitleColor:kDarkTextColor forState:UIControlStateNormal];
}

- (void)setCanPurchase:(BOOL)canPurchase
{
    _canPurchase = canPurchase;
    self.submitBtn.enabled = _canPurchase;
}

- (void)setCanChat:(BOOL)canChat
{
    _canChat = canChat;
    self.chatBtn.hidden = !_canChat;
}

- (IBAction)chatAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapChat)]) {
        [self.delegate didTapChat];
    }
}

- (IBAction)purchaseAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapPurchase)]) {
        [self.delegate didTapPurchase];
    }
}

@end
