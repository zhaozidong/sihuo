//
//  NSString+URLEncode.h
//  QMMM
//
//  Created by kingnet  on 14-9-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URLEncode)

- (NSString *)urlencode;

@end
