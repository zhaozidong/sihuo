//
//  QMFavorViewController.m
//  QMMM
//
//  Created by hanlu on 14-11-11.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMFavorViewController.h"
#import "MsgHandle.h"
#import "QMFavorMsgCell.h"
#import "GoodsDetailViewController.h"
#import "NSString+JSONCategories.h"
#import "QMFailPrompt.h"

@interface QMFavorViewController () <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray * _dataSource;
    BOOL _isBlankPage;
}

@end

@implementation QMFavorViewController

- (void)initNavBar
{
    self.title = [AppUtils localizedProductString:@"QM_Text_favor"];
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backBtnClick:)];
}

- (void)initTableView
{
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    if(kIOS7) {
        self.tableView.separatorInset = UIEdgeInsetsZero;
    }
    self.tableView.backgroundColor = kBackgroundColor;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initNavBar];
    
    [self initTableView];
    
    _dataSource = [[NSMutableArray alloc] init];
    
    __weak UITableView * weakTableView = self.tableView;
    __weak NSMutableArray * weakDataSource = _dataSource;
    __weak QMFavorViewController *weakSelf=self;
    [[MsgHandle shareMsgHandler] loadLocalMsgList:^(id result) {
        [weakDataSource removeAllObjects];
        if([result count] > 0) {
            [weakDataSource addObjectsFromArray:result];
        }
        
        if([weakDataSource count] == 0) {
            weakTableView.hidden=YES;
            _isBlankPage = YES;
            [weakSelf showPromptViewWithType:QMFailTypeNOData andText:@"还没有人赞过你的商品"];
            
        }else{  //消息数量为0就不加载数据
            [weakTableView reloadData];
        }
        
    } msgType:EMT_FAVOR];
    
    [[MsgHandle shareMsgHandler] updateMsgUnreadStatus:nil msgType:EMT_FAVOR unread:NO];
}

-(void)showPromptViewWithType:(QMFailType)failType andText:(NSString *)text{//显示提示信息
    QMFailPrompt *fail=[[QMFailPrompt alloc] initWithFailType:failType labelText:text buttonType:QMFailButtonTypeNone buttonText:nil];
    fail.frame=CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
    [self.view addSubview:fail];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    _tableView.delegate = nil;
    _tableView.dataSource = nil;
    [_dataSource removeAllObjects];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)backBtnClick:(id)sender
{
//    _tableView.delegate = nil;
//    _tableView.dataSource = nil;
//    [_dataSource removeAllObjects];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:kFlushMsgUnreadCountNotify object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(_isBlankPage) {
        return 0;
    } else {
        return [_dataSource count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * retCell = nil;
    if(!_isBlankPage) {
        QMFavorMsgCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMFavorMsgCellIdentifier];
        if(nil == cell) {
            cell = [QMFavorMsgCell cellForFavorMsg];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        MsgData * msgdata = [_dataSource objectAtIndex:indexPath.section];
        [cell updateUI:msgdata];
        
        retCell = cell;
    }
    
    return retCell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_isBlankPage) {
        if(kIOS7) {
            return _tableView.bounds.size.height - 64;
        } else {
            return _tableView.bounds.size.height;
        }
    } else {
        return 110;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(_isBlankPage) {
        return CGFLOAT_MIN;
    } else {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(!_isBlankPage) {
        MsgData * msgdata = [_dataSource objectAtIndex:indexPath.section];
        NSDictionary * dict = [msgdata.extData toDict];
        id  obj = [dict objectForKey:@"gid"];
        uint64_t goodsId = 0;
        if([obj isKindOfClass:[NSString class]]) {
            goodsId = (uint64_t)[obj longLongValue];
        } else if([obj isKindOfClass:[NSNumber class]]) {
            goodsId = [obj unsignedLongLongValue];
        }
        
        if(goodsId) {
            GoodsDetailViewController * viewController = [[GoodsDetailViewController alloc] initWithGoodsId:goodsId];
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

@end
