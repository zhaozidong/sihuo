//
//  MineViewController.m
//  QMMM
//
//  Created by kingnet  on 14-9-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MineViewController.h"
#import "JumpCell.h"
#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import "QMGoodsFavorViewController.h"
#import "MineInfoViewController.h"
#import "UserInfoHandler.h"
#import "UserEntity.h"
#import "QMFriendViewController.h"
#import "SettingsViewController.h"
#import "MyWalletViewController.h"
#import "PersonPageHandler.h"
#import "UserDataInfo.h"
#import "PersonPageViewController.h"
#import "Friendhandle.h"
#import "CXAlertView.h"
#import "MsgHandle.h"
#import "MineUserInfoCell.h"
#import "UploadImg.h"
#import "SystemHandler.h"
#import "VersionInfo.h"
#import "UserDefaultsUtils.h"
#import "MineNumCell.h"

@interface MineViewController ()<UITableViewDataSource, UITableViewDelegate, MineUserInfoCellDelegate, MineNumCellDelegate>

{
    __block NSMutableDictionary *keysAndFilePath_;
    __block int salerNum_;
    __block int buyerNum_;
    __block int friendsNum_;
    __block int walletNum_;
    __block int versionNum_;
    UploadImg *uploadImg_;
    VersionInfo *verInfo_;
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) MineUserInfoCell *userInfoCell;
@property (nonatomic, strong) MineNumCell *numCell;

@end

@implementation MineViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"我";
    
    [self.view addSubview:self.tableView];
    
    if (self.rdv_tabBarController.tabBar.translucent) {
        UIEdgeInsets insets = UIEdgeInsetsMake(0,
                                               0,
                                               CGRectGetHeight(self.rdv_tabBarController.tabBar.frame),
                                               0);
        
        self.tableView.contentInset = insets;
        self.tableView.scrollIndicatorInsets = insets;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userInfoUpdate:) name:kUserInfoUpdateNotify object:nil];
    //有人申请加为好友
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getFriendsNum) name:kFriendListChangedNotify object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getWalletNum) name:kWalletMsgNotify object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getOrderStatus) name:kTradeMsgNotify object:nil];
    
    salerNum_ = 0;
    buyerNum_ = 0;
    friendsNum_ = 0;
    walletNum_ = 0;
    versionNum_ = 0;
    [self userInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.navigationController.viewControllers.count < 2) {
        //是当前界面
        [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (self.navigationController.viewControllers.count > 1) {
        //是当前界面
        [[self rdv_tabBarController] setTabBarHidden:YES animated:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self getOrderStatus];
    [self getFriendsNum];
    [self getInfluenceNum];
    [self getWalletNum];
//    [self getVersion];
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)userInfoUpdate:(NSNotification *)notification
{
    [self userInfo];
}

#pragma mark - Data
- (void)getFriendsNum
{
    __weak typeof(self) weakSelf = self;
    [[Friendhandle shareFriendHandle] getFriendUnreadMsgCount:^(id result) {
        friendsNum_ = [result intValue];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
        [weakSelf.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

- (void)userInfo
{
    UserEntity *entity = [UserInfoHandler sharedInstance].currentUser;
    [self.userInfoCell updateUI:entity];
}

- (void)getInfluenceNum
{
    __weak typeof(self) weakSelf = self;
    UserEntity *entity = [UserInfoHandler sharedInstance].currentUser;
    [[PersonPageHandler sharedInstance]getWatchNumAndFlyNum:entity.uid success:^(id obj) {
        PersonInfluence *influence = (PersonInfluence *)obj;
        [weakSelf.numCell updateNum:influence];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)getOrderStatus
{
    __weak typeof(self) weakSelf = self;
    [[UserInfoHandler sharedInstance]getOrderStateNum:^(id obj) {
        PersonOrderStatusNum *status = (PersonOrderStatusNum *)obj;
        salerNum_ = status.salerNum;
        buyerNum_ = status.buyerNum;
    
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:2];
        NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:2 inSection:2];
        [weakSelf.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, indexPath2, nil] withRowAnimation:UITableViewRowAnimationNone];
    } failed:^(id obj) {
        
    }];
}

- (void)getWalletNum
{
    __weak typeof(self) weakSelf = self;
    [[MsgHandle shareMsgHandler]getWalletUnreadCount:^(id result) {
        walletNum_ = [result intValue];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:1];
        [weakSelf.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

- (void)getVersion
{
    __weak typeof(self) weakSelf = self;
    [[SystemHandler sharedInstance]checkVersion:^(id obj) {
        verInfo_ = (VersionInfo *)obj;
        BOOL haveNew = [VersionInfo isHaveNewVer:verInfo_.version];
        if (haveNew) {
            id temp = [UserDefaultsUtils valueWithKey:kVersionRedPonitTip];
            if (!temp || [temp objectForKey:verInfo_.version]==NO) {
                versionNum_ = 1;
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:3];
                [weakSelf.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
        }
    } failed:^(id obj) {
        
    }];
}

#pragma mark - View

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
        [_tableView registerNib:[JumpCell nib] forCellReuseIdentifier:kJumpCellIdentifier];
    }
    return _tableView;
}

- (MineUserInfoCell *)userInfoCell
{
    if (!_userInfoCell) {
        _userInfoCell = [MineUserInfoCell userInfoCell];
        _userInfoCell.delegate = self;
    }
    return _userInfoCell;
}

- (MineNumCell *)numCell
{
    if (!_numCell) {
        _numCell = [MineNumCell mineNumCell];
        _numCell.delegate = self;
    }
    return _numCell;
}

#pragma mark - HeadView Delegate
- (void)didTapAvatar
{
    //上传头像
    if (!uploadImg_) {
        uploadImg_ = [[UploadImg alloc]init];
    }
    
    [uploadImg_ uploadImg:self uploadBlock:^(UIImage *img) {
        //上传
        NSData *data = UIImageJPEGRepresentation(img, 0.9);
        
        [[UserInfoHandler sharedInstance] uploadAvatarData:data success:^(id obj) {
            NSString *key = (NSString *)obj;
            UserEntity *curUser = [UserInfoHandler sharedInstance].currentUser;
            UserEntity *entity = [[UserEntity alloc]initWithUserEntity:curUser];
            entity.avatar = key;
            [[UserInfoHandler sharedInstance]updateUserInfo:entity success:^(id obj) {
                
            } failed:^(id obj) {
                [AppUtils showErrorMessage:(NSString *)obj];
            }];
        } failed:^(id obj) {
            [AppUtils showErrorMessage:(NSString *)obj];
        }];
    }];
}

- (void)didTapFlyNumLbl:(NSString *)flyNum flyBeat:(NSString *)flyBeat
{
    if (flyNum == nil || flyBeat == nil) {
        [AppUtils showAlertMessage:@"获取影响力和可信度失败了--!"];
        return;
    }
 
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 250, 60)];
    label.numberOfLines = 0;
    label.font = [UIFont systemFontOfSize:14.f];
    label.numberOfLines = NSLineBreakByWordWrapping;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    NSString *tipStr = [NSString stringWithFormat:[AppUtils localizedPersonString:@"QM_Alert_My_Reliability"], flyBeat];
    NSRange range = [tipStr rangeOfString:flyBeat];
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:tipStr];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xf5292c) range:NSMakeRange(range.location, [flyBeat length]+1)];
    range = [tipStr rangeOfString:flyNum];
    label.attributedText = attributeStr;
    
    CXAlertView *alertView = [[CXAlertView alloc]initWithTitle:@"可信度" contentView:label cancelButtonTitle:nil];
    [alertView addButtonWithTitle:[AppUtils localizedCommonString:@"QM_Button_Invite"] type:CXAlertViewButtonTypeDefault handler:^(CXAlertView *alertView, CXAlertButtonItem *button) {
        [alertView dismiss];
        [self performSelector:@selector(testTipAlert) withObject:nil afterDelay:0.5];
    }];
    [alertView show];
}

- (void)didTapInfluenceNumLbl:(NSString *)influenceNum influenceBeat:(NSString *)influenceBeat
{
    if (influenceNum == nil || influenceBeat == nil) {
        [AppUtils showAlertMessage:@"获取影响力和可信度失败了--!"];
        return;
    }
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 250, 60)];
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:14.f];
    label.numberOfLines = NSLineBreakByWordWrapping;
    label.textColor = [UIColor blackColor];
    NSString *tipStr = [NSString stringWithFormat:[AppUtils localizedPersonString:@"QM_Alert_My_Influence"], influenceBeat];
    NSRange range = [tipStr rangeOfString:influenceBeat];
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:tipStr];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xf5292c) range:NSMakeRange(range.location, [influenceBeat length]+1)];
    label.attributedText = attributeStr;
    
    CXAlertView *alertView = [[CXAlertView alloc]initWithTitle:@"影响力" contentView:label cancelButtonTitle:nil];
    [alertView addButtonWithTitle:[AppUtils localizedCommonString:@"QM_Button_Invite"] type:CXAlertViewButtonTypeDefault handler:^(CXAlertView *alertView, CXAlertButtonItem *button) {
        [alertView dismiss];
        [self performSelector:@selector(testTipAlert) withObject:nil afterDelay:0.2];
    }];
    [alertView show];
}

- (void)testTipAlert
{
    QMFriendViewController * viewController = [[QMFriendViewController alloc] initWithNibName:@"QMFriendViewController" bundle:nil];
    viewController.slideToInvite = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1 || section == 2) {
        return 3;
    }
    else if (section == 0){
        return 2;
    }
    else if (section == 3){
        return 1;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return [MineUserInfoCell cellHeight];
    }
    return [JumpCell rowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return self.userInfoCell;
        }
        else{
            return self.numCell;
        }
    }
    else{
        JumpCell *cell = (JumpCell *)[tableView dequeueReusableCellWithIdentifier:kJumpCellIdentifier];
        [cell updateUI:indexPath];
        if (indexPath.section == 2 && indexPath.row == 1) {
            cell.num = salerNum_; //待发货
        }
        else if (indexPath.section == 2 && indexPath.row == 2){
            cell.num = buyerNum_; //待收货
        }
        else if (indexPath.section == 1 && indexPath.row == 1){
            cell.num = friendsNum_;
        }
        else if (indexPath.section == 1 && indexPath.row == 2){
            cell.num = walletNum_;
        }
        else if (indexPath.section == 3 && indexPath.row == 0){
            cell.num = versionNum_;
        }
        return cell;
    }
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) { //资料编辑
            MineInfoViewController *controller = [[MineInfoViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
    else if (indexPath.section == 1) {
        if (indexPath.row == 0) {//我的主页
            UserEntity *entity = [[UserInfoHandler sharedInstance]currentUser];
            PersonPageViewController *controller = [[PersonPageViewController alloc]initWithUserId:[entity.uid longLongValue]];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (indexPath.row == 1){ //我的朋友
            QMFriendViewController * viewController = [[QMFriendViewController alloc] initWithNibName:@"QMFriendViewController" bundle:nil];
            
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if (indexPath.row == 2){//我的钱包
            MyWalletViewController *viewController = [[MyWalletViewController alloc]init];
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
    else if (indexPath.section == 2) {
        if (indexPath.row == 0){//在卖的商品
            QMGoodsFavorViewController * viewController = [[QMGoodsFavorViewController alloc] initWithControllerType:ECT_GOODS_MGR];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if (indexPath.row == 1){//已卖掉的宝贝
            QMGoodsFavorViewController * viewController = [[QMGoodsFavorViewController alloc] initWithControllerType:ECT_ORDER_MGR];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if (indexPath.row == 2){//已买到的商品
            QMGoodsFavorViewController * viewController = [[QMGoodsFavorViewController alloc] initWithControllerType:ECT_BUYED_GOODS];
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
    else if (indexPath.section == 3){
//        if (verInfo_ && [VersionInfo isHaveNewVer:verInfo_.version]) {
//            //设置为已读
//            NSMutableDictionary *mbDic = [NSMutableDictionary dictionaryWithDictionary:[UserDefaultsUtils valueWithKey:kVersionRedPonitTip]];
//            [mbDic setObject:[NSNumber numberWithBool:YES] forKey:verInfo_.version];
//            [UserDefaultsUtils saveValue:mbDic forKey:kVersionRedPonitTip];
//            versionNum_ = 0;
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:3];
//            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
//        }
        SettingsViewController *controller = [[SettingsViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 14.f;
    }
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 14.f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
