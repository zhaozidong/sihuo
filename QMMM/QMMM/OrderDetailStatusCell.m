//
//  OrderDetailStatusCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderDetailStatusCell.h"
#import "OrderDataInfo.h"

NSString * const kOrderDetailStatusCellIdentifier = @"kOrderDetailStatusCellIdentifier";

@interface OrderDetailStatusCell ()
@property (weak, nonatomic) IBOutlet UIImageView *statusImageV;

@end
@implementation OrderDetailStatusCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"OrderDetailStatusCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(OrderDataInfo *)orderInfo
{
    if (orderInfo.order_status == QMOrderStatusClosed || orderInfo.order_status == QMOrderStatusWaitPay || orderInfo.order_status == QMOrderStatusTimeout) {
        self.statusImageV.image = [UIImage imageNamed:@"order_closed_bkg"];
    }
    else if (orderInfo.order_status == QMOrderStatusPaid){
        self.statusImageV.image = [UIImage imageNamed:@"order_payed_bkg"];
    }
    else if (orderInfo.order_status == QMOrderStatusDelivery){
        self.statusImageV.image = [UIImage imageNamed:@"order_delivered_bkg"];
    }
    else if (orderInfo.order_status == QMOrderStatusFinished){
        self.statusImageV.image = [UIImage imageNamed:@"order_finish_bkg"];
    }
}

+ (CGFloat)cellHeight
{
    return 123.f;
}

@end
