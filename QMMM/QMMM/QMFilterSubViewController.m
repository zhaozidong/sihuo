//
//  QMFilterSubViewController.m
//  QMMM
//
//  Created by kingnet  on 15-3-17.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMFilterSubViewController.h"
#import "QMFilterTableViewCell.h"


@interface QMFilterSubViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *dataArr_;  //tableview的数据，元素要求为NSString类型
    NSInteger selectedIndex_; //选择的index
}

@end

@implementation QMFilterSubViewController
@synthesize tableView = _tableView;

- (id)init
{
    self = [super init];
    if (self) {
        dataArr_ = [@[]mutableCopy];
    }
    return self;
}

- (void)reloadTableWithData:(NSArray *)data selectedIndex:(NSInteger)selectedIndex
{
    if (data) {
        [dataArr_ removeAllObjects];
        [dataArr_ addObjectsFromArray:data];
        selectedIndex_ = selectedIndex;
        [self.tableView reloadData];
    }
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[QMFilterTableViewCell nib] forCellReuseIdentifier:kQMFilterTableViewCellIdentifier];
    }
    return _tableView;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArr_.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMFilterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kQMFilterTableViewCellIdentifier forIndexPath:indexPath];
    cell.text = [dataArr_ objectAtIndex:indexPath.row];
    cell.choosed = (selectedIndex_==indexPath.row);
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(tableViewSelectedAtIndex:)]) {
        [self.delegate tableViewSelectedAtIndex:indexPath.row];
    }
}

@end
