//
//  PersonPageHeadView.m
//  QMMM
//
//  Created by kingnet  on 14-11-10.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PersonPageHeadView.h"
#import "UserDataInfo.h"
#import "QMRoundHeadView.h"

@interface PersonPageHeadView ()<QMRoundHeadViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *nicknameLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerSuperViewHCst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerSuperViewTopCst;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageTopCst;
@property (weak, nonatomic) IBOutlet QMRoundHeadView *roundHeadView;
@property (weak, nonatomic) IBOutlet UIButton *friendBtn;

@property (nonatomic, strong) PersonPageUserInfo *personInfo;

@end

@implementation PersonPageHeadView

+ (PersonPageHeadView *)headView
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"PersonPageHeadView" owner:self options:0];
    return [array lastObject];
}

+ (CGFloat)headViewHeight
{
    return 205;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.nicknameLbl.text = @"";
    self.roundHeadView.headViewStyle = QMRoundHeadViewLarge;
    self.roundHeadView.delegate = self;
    [self.friendBtn setTitle:@"" forState:UIControlStateNormal];
    self.friendBtn.hidden = YES;
}

- (void)setTouching:(BOOL)touching
{
    _touching = touching;
}

-(void)setOffsetY:(float)y
{
    _offsetY = y;
    
    UIView* bannerSuper = _img_banner.superview;
    CGRect bframe = bannerSuper.frame;
    if(y<0)
    {
        self.bannerSuperViewTopCst.constant = y;
        self.bannerSuperViewHCst.constant = -y + [[self class]headViewHeight];
        self.imageTopCst.constant = 0;
    }
    else{
        if(bframe.origin.y != 0)
        {
            self.bannerSuperViewTopCst.constant = 0;
            self.bannerSuperViewHCst.constant = [[self class]headViewHeight];
        }
        if(y<self.bannerSuperViewHCst.constant)
        {
            self.imageTopCst.constant = 0.5*y;
        }

        self.nicknameLbl.alpha = 1-1/[[self class]headViewHeight]*y*2;
        self.roundHeadView.alpha = 1-1/[[self class]headViewHeight]*y*2;
    }
}

- (void)updateUI:(UserDataInfo *)userDataInfo
{
    //设置用户名
    if (userDataInfo.remark.length > 0 && userDataInfo.nickname.length > 0) {
        NSString *s = [NSString stringWithFormat:@"%@ (%@)", userDataInfo.remark, userDataInfo.nickname];
        NSMutableAttributedString *attriStr = [[NSMutableAttributedString alloc]initWithString:s];
        
        [attriStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15.f] range:NSMakeRange(s.length-2-userDataInfo.nickname.length, userDataInfo.nickname.length+2)];
        self.nicknameLbl.attributedText = attriStr;
    }
    else{
        if (userDataInfo.remark.length > 0) {
            self.nicknameLbl.text = userDataInfo.remark;
        }
        else if (userDataInfo.nickname.length > 0) {
            self.nicknameLbl.text = userDataInfo.nickname;
        }
    }
    
    //设置头像
    [self.roundHeadView setImageUrl:userDataInfo.avatar];
    self.roundHeadView.userId = [userDataInfo.uid longLongValue];
}

//设置好友button
- (void)updateFriendButton:(PersonRelation *)relation
{
    if (relation.isFriends) {
        //一度好友
        self.friendBtn.hidden = NO;
        if (relation.isFromApp) {
            [self.friendBtn setTitle:@"TA来自私货" forState:UIControlStateNormal];
            self.friendBtn.enabled = NO;
        }
        else{
            [self.friendBtn setTitle:@"TA来自通讯录>" forState:UIControlStateNormal];
            [self.friendBtn addTarget:self action:@selector(clickFriendBtn:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    else if (relation.is2ndFriends){
        //二度好友
        NSArray *array = relation.friendsArr;
        if (array.count > 0) {
            NSString *tipStr = @"";
            if (array.count == 1) {
                UserDataInfo *user = [array objectAtIndex:0];
                tipStr = [NSString stringWithFormat:@"TA是%@的朋友>", user.nickname];
            }
            else{
                UserDataInfo *user = [array objectAtIndex:0];
                tipStr = [NSString stringWithFormat:@"TA是%@等%d人的朋友>", user.nickname, (int)array.count];
            }
            self.friendBtn.hidden = NO;
            [self.friendBtn setTitle:tipStr forState:UIControlStateNormal];
            [self.friendBtn addTarget:self action:@selector(clickFriendBtn:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
}

#pragma mark - RoundHeadView Delegate
- (void)clickHeadViewWithUid:(uint64_t)userId
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapHeadView)]) {
        [self.delegate didTapHeadView];
    }
}

- (void)clickFriendBtn:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapFriendBtn)]) {
        [self.delegate didTapFriendBtn];
    }
}

@end
