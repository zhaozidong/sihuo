//
//  ChooseCityView.m
//  QMMM
//
//  Created by kingnet  on 14-9-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "ChooseCityView.h"

#define FirstComponent 0
#define SubComponent 1
#define ThirdComponent 2
#define ViewHeight 206

@interface ChooseCityView ()<UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic) UIPickerView *pickView;
@property (nonatomic) UIControl *control;
@property (nonatomic, strong) NSArray *pickerArray;
@property (nonatomic, strong) NSArray *subPickerArray;
@property (nonatomic, strong) NSArray *thirdPickerArray;

@property (nonatomic, strong) NSArray *provinceArr;
@property (nonatomic, strong) NSDictionary *cityDic;
@property (nonatomic, strong) NSDictionary *townDic;

@end

@implementation ChooseCityView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setupArray];
        [self setupViews];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupArray];
        [self setupViews];
    }
    return self;
}

+ (void)showInView:(UIView *)containerView provinceId:(int)provinceId cityId:(int)cityId distritId:(int)distritId completion:(ChooseCityBlock)competion
{
    [AppUtils closeKeyboard];
    
    ChooseCityView *chooseView = [[ChooseCityView alloc]initWithFrame:CGRectMake(0, kMainFrameHeight, kMainFrameWidth, ViewHeight)];
    chooseView.chooseCityBlock = [competion copy];
    chooseView.control.frame = containerView.bounds;
    chooseView.control.center = containerView.center;
    [containerView addSubview:chooseView.control];
    [containerView addSubview:chooseView];
    
    [UIView animateWithDuration:.3f animations:^{
        CGFloat h = kMainFrameHeight;
        chooseView.frame = CGRectMake(0, h-ViewHeight, kMainFrameWidth, ViewHeight);
    } completion:^(BOOL finished) {
        if (provinceId > 0) {//如果之前已经选择了省市区
            //自动定位到那一行
            [chooseView chooseWithProvinceId:provinceId cityId:cityId distritId:distritId];
        }
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:.3f animations:^{
        self.frame = CGRectMake(0, kMainFrameHeight, kMainFrameWidth, ViewHeight);
    } completion:^(BOOL finished) {
        [self.control removeFromSuperview];
        [self removeFromSuperview];
        self.pickerArray = nil;
        self.subPickerArray = nil;
        self.thirdPickerArray = nil;
    }];
}

- (void)setupArray
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSData *cityData = [[NSData alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"city" ofType:@"json"]];
        self.cityDic = [NSJSONSerialization JSONObjectWithData:cityData options:NSJSONReadingMutableLeaves error:nil];
        NSData *provinceData = [[NSData alloc]initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"province" ofType:@"json"]];
        self.provinceArr = [NSJSONSerialization JSONObjectWithData:provinceData options:NSJSONReadingMutableLeaves error:nil];
        NSData *townData = [[NSData alloc]initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"town" ofType:@"json"]];
        self.townDic = [NSJSONSerialization JSONObjectWithData:townData options:NSJSONReadingMutableLeaves error:nil];
        
        self.pickerArray = self.provinceArr;
        
        int _id = [[[self.provinceArr objectAtIndex:0] objectForKey:@"id"] intValue];
        self.subPickerArray = [self.cityDic objectForKey:[NSString stringWithFormat:@"%d", _id]];
        _id = [[[self.subPickerArray objectAtIndex:0] objectForKey:@"id"] intValue];
        self.thirdPickerArray = [self.townDic objectForKey:[NSString stringWithFormat:@"%d", _id]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.pickView reloadAllComponents];
        });

    });
}

- (void)setupViews
{
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:[self pickView]];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_pickView);
    //设置位置
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[_pickView]-0-|" options:0 metrics:0 views:views];
    [self addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_pickView(162)]-0-|" options:0 metrics:0 views:views];
    [self addConstraints:constraints];
    
    //顶部的操作栏
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = UIColorFromRGB(0x595959);
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:view];
    
    views = NSDictionaryOfVariableBindings(_pickView, view);
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view(44)]-0-[_pickView]" options:0 metrics:0 views:views];
    [self addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[view]-0-|" options:0 metrics:0 views:views];
    [self addConstraints:constraints];
    
    UIButton *finishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [finishBtn setImage:[UIImage imageNamed:@"navRight_finish_btn"] forState:UIControlStateNormal];
    finishBtn.translatesAutoresizingMaskIntoConstraints = NO;
    [finishBtn addTarget:self action:@selector(finishAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:finishBtn];
    
    [view addConstraint:[NSLayoutConstraint constraintWithItem:finishBtn attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.f constant:0.f]];
    views = NSDictionaryOfVariableBindings(finishBtn);
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[finishBtn(40)]-10-|" options:0 metrics:0 views:views]];
    
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setImage:[UIImage imageNamed:@"xxx_btn"] forState:UIControlStateNormal];
    closeBtn.translatesAutoresizingMaskIntoConstraints = NO;
    [closeBtn addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:closeBtn];
    
    [view addConstraint:[NSLayoutConstraint constraintWithItem:closeBtn attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.f constant:0.f]];
    views = NSDictionaryOfVariableBindings(closeBtn);
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-10-[closeBtn(40)]" options:0 metrics:0 views:views]];
}

- (UIPickerView *)pickView
{
    if (!_pickView) {
        _pickView = [[UIPickerView alloc]init];
        _pickView.delegate = self;
        _pickView.dataSource = self;
        _pickView.showsSelectionIndicator = YES;
        _pickView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _pickView;
}

- (UIControl *)control
{
    if (!_control) {
        _control = [[UIControl alloc]init];
        _control.backgroundColor = [UIColor clearColor];
        [_control addTarget:self action:@selector(tapControl:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _control;
}

- (void)dealloc
{
    self.pickerArray = nil;
    self.subPickerArray = nil;
    self.thirdPickerArray = nil;
    self.provinceArr = nil;
    self.townDic = nil;
    self.cityDic = nil;
}

- (void)chooseWithProvinceId:(int)provinceId cityId:(int)cityId distritId:(int)distritId
{
    int firstRow = 0, secondRow = 0, thirdRow = 0;
    for(int i=0; i< self.provinceArr.count; i++) {
        NSDictionary *province = [self.provinceArr objectAtIndex:i];
        if ([[province objectForKey:@"id"] intValue] == provinceId) {
            firstRow = i;
            break;
        }
    }
    
    self.subPickerArray = [self.cityDic objectForKey:[NSString stringWithFormat:@"%d", provinceId]];
    for (int i=0; i< self.subPickerArray.count; i++) {
        NSDictionary *city = [self.subPickerArray objectAtIndex:i];
        if ([[city objectForKey:@"id"] intValue] == cityId) {
            secondRow = i;
            break;
        }
    }
    
    self.thirdPickerArray = [self.townDic objectForKey:[NSString stringWithFormat:@"%d", cityId]];
    for (int i=0; i< self.thirdPickerArray.count; i++) {
        NSDictionary *town = [self.thirdPickerArray objectAtIndex:i];
        if ([[town objectForKey:@"id"] intValue] == distritId) {
            thirdRow = i;
            break;
        }
    }
    
    //设置选中的行
    [self.pickView selectRow:firstRow inComponent:0 animated:NO];
    [self.pickView reloadComponent:1];
    [self.pickView selectRow:secondRow inComponent:1 animated:NO];
    [self.pickView reloadComponent:2];
    [self.pickView selectRow:thirdRow inComponent:2 animated:NO];
}

- (void)tapControl:(id)sender
{
    self.chooseCityBlock(0, 0, 0, @"");
    [self dismiss];
}

- (void)finishAction:(id)sender
{
    NSDictionary *dicP = [self.pickerArray objectAtIndex:[self.pickView selectedRowInComponent:FirstComponent]];
    NSDictionary *dicC = [self.subPickerArray objectAtIndex:[self.pickView selectedRowInComponent:SubComponent]];
    NSDictionary *dicD = [self.thirdPickerArray objectAtIndex:[self.pickView selectedRowInComponent:ThirdComponent]];
    
    NSString *province = [dicP objectForKey:@"name"];
    NSString *city = [dicC objectForKey:@"name"];
    if ([city isEqualToString:@"县"] || [city isEqualToString:@"市辖区"] || [city isEqualToString:@"省直辖"]) {
        city = @"";
    }
    
    NSString *district;
    if ([dicD objectForKey:@"name"]) {
        district =[dicD objectForKey:@"name"];
    }else{
        district=@"";
    }
//    NSString *district =[dicD objectForKey:@"name"];
    
    NSString *result = [NSString stringWithFormat:@"%@%@%@", province, city, district];
    
    int provinceID = [[dicP objectForKey:@"id"] intValue];
    int cityID = [[dicC objectForKey:@"id"] intValue];
    int districtID = [[dicD objectForKey:@"id"] intValue];

    //看看需要什么数据
    self.chooseCityBlock(provinceID, cityID, districtID, result);
    [self dismiss];
}

- (void)closeAction:(id)sender
{
    [self tapControl:nil];
}

#pragma mark - UIPickView datasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component==FirstComponent) {
        return [self.pickerArray count];
    }
    if (component==SubComponent) {
        return [self.subPickerArray count];
    }
    if (component==ThirdComponent) {
        return [self.thirdPickerArray count];
    }
    return 0;
}

#pragma mark - UIPickerView delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component==FirstComponent) {
        return [[self.pickerArray objectAtIndex:row] objectForKey:@"name"];
    }
    if (component==SubComponent) {
        return [[self.subPickerArray objectAtIndex:row] objectForKey:@"name"];
    }
    if (component==ThirdComponent) {
        return [[self.thirdPickerArray objectAtIndex:row] objectForKey:@"name"];
    }
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //处理数据源的切换
    if (component == FirstComponent) {
        int _id = [[[self.provinceArr objectAtIndex:row] objectForKey:@"id"] intValue];
        self.subPickerArray = [self.cityDic objectForKey:[NSString stringWithFormat:@"%d", _id]];
        _id = [[[self.subPickerArray objectAtIndex:0] objectForKey:@"id"] intValue];
        self.thirdPickerArray = [self.townDic objectForKey:[NSString stringWithFormat:@"%d", _id]];
        
        [pickerView selectedRowInComponent:1];
        [pickerView reloadComponent:1];
        [pickerView selectedRowInComponent:2];
    }
    if (component == SubComponent) {
        int _id = [[[self.subPickerArray objectAtIndex:row] objectForKey:@"id"] intValue];
        self.thirdPickerArray = [self.townDic objectForKey:[NSString stringWithFormat:@"%d", _id]];
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }
    
    [pickerView reloadComponent:2];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component==FirstComponent) {
        return 90.0;
    }
    if (component==SubComponent) {
        return 100.0;
    }
    if (component==ThirdComponent) {
        return 120.0;
    }
    return 0;
}

@end
