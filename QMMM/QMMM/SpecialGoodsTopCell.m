//
//  SpecialGoodsTopCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "SpecialGoodsTopCell.h"
#import "ActivityInfo.h"
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/SDWebImageManager.h>

@interface SpecialGoodsTopCell ()
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;

@property (weak, nonatomic) IBOutlet UILabel *descLbl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgVHeightCst;

@end

@implementation SpecialGoodsTopCell

+ (SpecialGoodsTopCell *)topCell
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"SpecialGoodsTopCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.descLbl.text = @"";
    self.descLbl.textColor = kDarkTextColor;
    self.descLbl.numberOfLines = 0;
    self.descLbl.preferredMaxLayoutWidth = kMainFrameWidth - 16;
    self.topImageView.image = [UIImage imageNamed:@""];
    CGRect frame = self.frame;
    frame.size.height = [[self class]imageHeight]+6+8+20;
    self.frame = frame;
    self.contentView.frame = self.bounds;
    self.imgVHeightCst.constant = [[self class]imageHeight];
    [self layoutIfNeeded];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(ActivityEntity *)entity
{
    self.descLbl.text = entity.desc;
    NSString *imgUrl = [AppUtils getAbsolutePath:entity.bigImgSrc];
    UIImage *cachedImg = [[SDImageCache sharedImageCache]imageFromDiskCacheForKey:imgUrl];
    if (cachedImg) {
        self.topImageView.image = cachedImg;
    }
    else{
        UIImage *placeholder = [UIImage imageNamed:@"goodsPlaceholder_big"];
        self.topImageView.image = placeholder;
        __weak __typeof(self) weakSelf = self;
        [[SDWebImageManager sharedManager]downloadImageWithURL:[NSURL URLWithString:imgUrl] options:SDWebImageProgressiveDownload | SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if (finished) {
                if ([imageURL.absoluteString isEqualToString:imgUrl]) {
                    if(image){
                        weakSelf.topImageView.alpha = 0.0;
                        [UIView transitionWithView:weakSelf.topImageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                            ;
                            weakSelf.topImageView.image = image;
                            weakSelf.topImageView.alpha = 1.0;
                        } completion:nil];
                    }
                    else{
                        weakSelf.topImageView.image = [UIImage imageNamed:@"goodImgFailed_big"];
                    }
                }
            }
        }];
    }
}

+ (CGFloat)imageHeight
{
    float w = kMainFrameWidth;
    return w*200/320;
}

@end
