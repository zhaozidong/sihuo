//
//  SearchHistoryViewController.m
//  QMMM
//
//  Created by kingnet  on 15-1-13.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "SearchHistoryViewController.h"
#import "SearchHistoryCell.h"
#import "SearchHistoryDao.h"
#import "KeyWordEntity.h"

@interface SearchHistoryViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *keyWordsList;
@property (strong, nonatomic) IBOutlet UITableViewCell *clearCell;

@property (strong, nonatomic) IBOutlet UITableViewCell *blankCell;
@end

@implementation SearchHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.keyWordsList = [NSMutableArray array];
    NSArray *array = [[SearchHistoryDao sharedInstance] queryAll];
    for (KeyWordEntity *entity in array) {
        [self.keyWordsList addObject:entity];
    }
    
    [[NSBundle mainBundle]loadNibNamed:@"SearchHistoryOtherCells" owner:self options:0];
    
    [self.view addSubview:self.tableView];
    
    if (self.keyWordsList.count == 0) {
        self.tableView.separatorColor = kBackgroundColor;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [self.keyWordsList removeAllObjects];
    self.keyWordsList = nil;
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight-kMainTopHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
        [_tableView registerNib:[SearchHistoryCell nib] forCellReuseIdentifier:kSearchHistoryCellIdentifier];
        _tableView.rowHeight = [SearchHistoryCell cellHeight];
    }
    return _tableView;
}

#pragma mark -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.keyWordsList.count == 0) {
        return 1;
    }
    return self.keyWordsList.count+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.keyWordsList.count == 0) {
        return 100;
    }
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.keyWordsList.count == 0) {
        return self.blankCell;
    }
    if (indexPath.row == self.keyWordsList.count) {
        return self.clearCell;
    }
    else{
        SearchHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:kSearchHistoryCellIdentifier];
        KeyWordEntity *entity = [self.keyWordsList objectAtIndex:indexPath.row];
        [cell updateUI:entity];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (self.keyWordsList.count == 0) {
        cell.backgroundColor = [UIColor clearColor];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.keyWordsList.count > 0) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        if (indexPath.row == self.keyWordsList.count) {
            [self clearHistory];
        }
        else{
            if (self.delegate && [self.delegate respondsToSelector:@selector(selectRow:)]) {
                KeyWordEntity *entity = [self.keyWordsList objectAtIndex:indexPath.row];
                [self.delegate selectRow:entity];
            }
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(closeKeyBorad)]) {
        [self.delegate closeKeyBorad];
    }
}

- (void)clearHistory
{
    //清除历史记录
    [[SearchHistoryDao sharedInstance]removeAll];
    [self.keyWordsList removeAllObjects];
    self.tableView.separatorColor = kBackgroundColor;
    [self.tableView reloadData];
}

#pragma mark -

- (void)showInView:(UIView *)view
{
    self.view.frame = CGRectMake(0, kMainFrameHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
    [view addSubview:self.view];
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = kMainTopHeight;
        self.view.frame = frame;
    }];
}

- (void)showInViewTop:(UIView *)view
{
    self.view.frame = CGRectMake(0, kMainFrameHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
    [view addSubview:self.view];
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = 0;
        self.view.frame = frame;
    }];
}

- (void)dismissWithCompletion:(void(^)(void))completion
{
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = kMainFrameHeight;
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
            if (completion) {
                completion();
            }
        }
    }];
}

@end
