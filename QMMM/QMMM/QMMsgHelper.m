//
//  QMSystemMsgHelper.m
//  QMMM
//
//  Created by hanlu on 14-10-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMMsgHelper.h"
#import "QMSqlite.h"
#import "MsgData.h"
#import "QMDatabaseHelper.h"
#import "NSString+JSONCategories.h"
#import "NSDictionary+JSONCategories.h"

@implementation QMMsgHelper

- (void)setWorkThread:(NSThread *)aThread
{
    @synchronized(self) {
        if (_workThread != aThread) {
            _workThread = aThread;
        }
    }
}

- (NSString *)getMsgTypeList:(MsgBigType)msgType
{
    __autoreleasing NSString * ret = nil;
    
    if(msgType == EMT_FRIEND) {
        ret = @"21,22,23";
    } else if(msgType == EMT_FAVOR) {
        ret = @"31";
    } else if(msgType == EMT_COMMENT) {
        ret = @"32,33";
    } else if (msgType == EMT_WALLET) {
        ret = @"37";
    } else if (msgType == EMT_TRADE) {
        ret = @"11,12,13";
    } else {
        ret = @"";
    }
    
    return ret;
}

- (NSArray *)loadMsglist:(FMDatabase *)database type:(MsgBigType)msgType
{
    NSMutableArray * list = [[NSMutableArray alloc] initWithCapacity:1];
    
    if(database) {
        
        @autoreleasepool {
            
            NSString * stringMsgType = [self getMsgTypeList:msgType];
            NSString * sql = [NSString stringWithFormat:@"SELECT * FROM qmMsgList WHERE msgtype IN (%@) ORDER BY msgtime DESC;", stringMsgType];
            FMResultSet * resultSet = [database executeQuery:sql];
            while ([resultSet next]) {
                if([[NSThread currentThread] isCancelled]) {
                    break;
                }
                
                MsgData * dataInfo = [[MsgData alloc] initWithFMResult:resultSet];
                [list addObject:dataInfo];
            }
        }
    }
    
    return list;
    
}

//- (NSArray *)loadMsglist:(FMDatabase *)database type:(MsgBigType)msgType fromId:(uint64_t)fromId count:(uint)count
//{
//    NSMutableArray * list = [[NSMutableArray alloc] initWithCapacity:1];
//    
//    if(database && count > 0) {
//        
//        @autoreleasepool {
//            
//            NSString * stringMsgType = [self getMsgTypeList:msgType];
//            
//            int getCount = 0; //实际获取到的条数
//            FMResultSet * resultSet = nil;
//            if(fromId != 0) {
//                // count + 1 多取一条用来判断是否已经到结尾
//                resultSet = [database executeQueryWithFormat:@"SELECT * FROM qmMsgList WHERE id < %qu AND msgtype IN (%@) ORDER BY id DESC LIMIT %u;", fromId, stringMsgType, count + 1];
//            } else {
//                resultSet = [database executeQueryWithFormat:@"SELECT * FROM qmMsgList WHERE msgtype IN (%@) ORDER BY id DESC;", stringMsgType];
//            }
//            
//            while ([resultSet next]) {
//                getCount++;
//                if (getCount > count)
//                    break;
//                
//                if([[NSThread currentThread] isCancelled]) {
//                    getCount = count + 1;
//                    break;
//                }
//                
//                MsgData * dataInfo = [[MsgData alloc] initWithFMResult:resultSet];
//                [list addObject:dataInfo];
//            }
//            
//            // 如果没有获取到指定的数量，说明已经没有更多数据了，最后一项加null表示数据到头
//            if (count > 0 && getCount <= count)
//                [list addObject:[NSNull null]];
//        }
//    }
//    
//    return list;
//}

- (void)addMsgList:(FMDatabase *)database list:(NSArray *)dataList
{
    if(database && [dataList count] > 0) {
        NSArray * array = [NSArray arrayWithArray:dataList];
        for (MsgData * data in array) {
            
            FMResultSet * result = [database executeQueryWithFormat:@"SELECT * FROM qmMsgList WHERE id = %@;", data.msgId];
            if([result next]) {//已经有当前id的数据,则先删除,保证界面中只有一条
                [database executeQueryWithFormat:@"DELETE * FROM qmMsgList WHERE id = %@;", data.msgId];
//                continue;
            }
            
            NSString * extData = data.extData;
            
            if(data.msgType == EMT_INVITE_FRIEND) {
                NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:[data.extData toDict]];
                [dict setObject:@(0) forKey:@"status"];
                extData = [dict toString];
            }
            
            [database executeUpdateWithFormat:@"INSERT INTO qmMsgList (id, msgtype, msgcontent, msgtime, extdata, msgpos, unread) VALUES (%@, %u, %@, %d, %@, %u, %d);", data.msgId, data.msgType, data.msgContent, data.msgTime, extData, data.msgPos, data.unRead];
        }
    }
}

- (void)deleteMsgList:(FMDatabase *)database type:(MsgBigType)msgType
{
    if(database) {
        NSString * stringMsgType = [self getMsgTypeList:msgType];
        NSString * sql = [NSString stringWithFormat:@"DELETE FROM qmMsgList WHERE msgType IN (%@);", stringMsgType];
        [database executeUpdate:sql];
    }
}

- (void)deleteMsgById:(FMDatabase *)database msgId:(NSString *)msgId
{
    if(database && [msgId length] > 0) {
        [database executeUpdateWithFormat:@"DELETE FROM qmMsgList WHERE id = %@", msgId];
    }
}

- (void)updateMsgUnreadStatusByType:(FMDatabase *)database type:(MsgBigType)msgType unRead:(BOOL)unRead
{
    if(database) {
        NSString * stringMsgType = [self getMsgTypeList:msgType];
        NSString * sql = [NSString stringWithFormat:@"UPDATE qmMsgList SET unread = %d WHERE msgtype IN (%@);", (int)unRead, stringMsgType];
        [database executeUpdate:sql];
    }
}

- (void)updateMsgUnreadStatusById:(FMDatabase *)database msgId:(NSString *)msgId unRead:(BOOL)unRead
{
    if(database && [msgId length] > 0) {
        [database executeUpdateWithFormat:@"UPDATE qmMsgList SET unread = %d WHERE id = %@", (int)unRead, msgId];
    }
}

- (void)updateFriendMsgStatus:(FMDatabase *)database msgId:(NSString *)msgId newExtData:(NSString *)newExtData
{
    if(database && newExtData && [msgId length] > 0) {
        [database executeUpdateWithFormat:@"UPDATE qmMsgList SET extdata = %@ WHERE id = %@", newExtData, msgId];
    }
}

- (uint)getFriendMsgUnreadCount:(FMDatabase *)database
{
    uint ret = 0;
    if(database) {
        NSString * sql = @"SELECT COUNT(*) FROM qmMsgList WHERE unread = 1 AND msgtype IN (21, 22, 23);";
        FMResultSet * result = [database executeQuery:sql];
        if([result next]) {
            ret = [result intForColumnIndex:0];
        }
    }
    return ret;
}

- (int)getWalletUnreadCount:(FMDatabase *)database
{
    int ret = 0;
    if(database) {
        NSString * sql = @"SELECT COUNT(*) FROM qmMsgList WHERE unread = 1 AND msgtype = 37;";
        FMResultSet * result = [database executeQuery:sql];
        if([result next]) {
            ret = [result intForColumnIndex:0];
        }
    }
    return ret;
}

- (NSDictionary *)getMsgUnreadCountList:(FMDatabase *)database
{
    __autoreleasing NSDictionary * dict = nil;
    if(database) {
        int favorUnread = 0;
        int commentUnread = 0;
        
        NSString * stringMsgType = [self getMsgTypeList:EMT_FAVOR];
        NSString * sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM qmMsgList WHERE unread = 1 AND msgtype IN (%@);", stringMsgType];
        FMResultSet * result = [database executeQuery:sql];
        if([result next]) {
            favorUnread = [result intForColumnIndex:0];
        }
        
        stringMsgType = [self getMsgTypeList:EMT_COMMENT];
        sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM qmMsgList WHERE unread = 1 AND msgtype IN (%@);", stringMsgType];
        result = [database executeQuery:sql];
        if([result next]) {
            commentUnread = [result intForColumnIndex:0];
        }
        
        dict = @{@"favor":@(favorUnread), @"comment":@(commentUnread)};
    }
    return dict;
}

- (NSDictionary *)getMsgUnreadCount:(FMDatabase *)database
{
    __autoreleasing NSDictionary * dict = nil;
    if(database) {
        int friendUnreadCount = 0;
        int favorUnreadCount = 0;
        int walletUnreadCount = 0;
        int tradeUnreadCount = 0;
        
        NSString * sql = @"SELECT COUNT(*) FROM qmMsgList WHERE unread = 1 AND msgtype IN (31, 32, 33);";
        FMResultSet * result = [database executeQuery:sql];
        if([result next]) {
            favorUnreadCount = [result intForColumnIndex:0];
        }
        
        sql = @"SELECT COUNT(*) FROM qmMsgList WHERE unread = 1 AND msgtype IN (21, 22, 23);";
        result = [database executeQuery:sql];
        if([result next]) {
            friendUnreadCount = [result intForColumnIndex:0];
        }
        
        sql = @"SELECT COUNT(*) FROM qmMsgList WHERE unread = 1 AND msgtype = 37;";
        result = [database executeQuery:sql];
        if([result next]) {
            walletUnreadCount = [result intForColumnIndex:0];
        }
        
        sql = @"SELECT COUNT(*) FROM qmMsgList WHERE unread = 1 AND msgtype IN (11, 12, 13);";
        result = [database executeQuery:sql];
        if([result next]) {
            tradeUnreadCount = [result intForColumnIndex:0];
        }
        
        dict = @{@"favor":@(favorUnreadCount), @"friend":@(friendUnreadCount), @"wallet":@(walletUnreadCount), @"trade":@(tradeUnreadCount)};

    }
    return dict;
}

- (void)asyncLoadMsgList:(contextBlock)context type:(MsgBigType)msgType
{
    if(_workThread) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(msgType), bContext, nil];
        [self performSelector:@selector(threadLoadAllMsgList:)
                     onThread:_workThread
                   withObject:array
                waitUntilDone:NO];
    }
    else {
        if (context) {
            context(nil);
        }
    }
}
- (void)threadLoadAllMsgList:(NSArray *)params
{
    if(params && [params count] > 0) {
        MsgBigType type = [[params objectAtIndex:0] unsignedIntValue];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        NSArray * result = [self loadMsglist:helper.database type:type];
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(result);
            });
        }
    }
}

//- (void)asyncLoadMsgList:(contextBlock)context type:(MsgBigType)msgType fromId:(uint64_t)fromId count:(uint)count
//{
//    if(_workThread) {
//        __strong contextBlock bContext = context;
//        NSArray * array = [NSArray arrayWithObjects:@(msgType), @(fromId), @(count), bContext, nil];
//        [self performSelector:@selector(threadLoadMsgList:)
//                     onThread:_workThread
//                   withObject:array
//                waitUntilDone:NO];
//    }
//    else {
//        if (context) {
//            context(nil);
//        }
//    }
//}
//- (void)threadLoadMsgList:(NSArray *)params
//{
//    if(params && [params count] >= 3) {
//        MsgBigType    type = [[params objectAtIndex:0] unsignedIntValue];
//        uint64_t fromId = [[params objectAtIndex:1] unsignedLongLongValue];
//        uint count = [[params objectAtIndex:2] unsignedIntValue];
//        contextBlock context = nil;
//        if([params count] > 3) {
//            context = [params objectAtIndex:3];
//        }
//        
//        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
//        NSArray * result = [self loadMsglist:helper.database type:type fromId:fromId count:count];
//        if(context) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                context(result);
//            });
//        }
//    }
//}

- (void)asyncAddMsgList:(contextBlock)context list:(NSArray *)datalist
{
    if(_workThread && datalist) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:datalist, bContext, nil];
        [self performSelector:@selector(threadAddMsgList:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadAddMsgList:(NSArray *)params
{
    if([params count] >= 1) {
        NSArray * data = [params objectAtIndex:0];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self addMsgList:helper.database list:data];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}

- (void)asyncUpdateMsgUnreadStatusByType:(contextBlock)context type:(MsgBigType)msgType unRead:(BOOL)unRead
{
    if(_workThread && msgType) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(msgType), @(unRead), bContext, nil];
        [self performSelector:@selector(threadUpdateMsgUnreadStatusByType:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadUpdateMsgUnreadStatusByType:(NSArray *)params
{
    if(params && [params count] >= 2) {
        MsgBigType msgType = [[params objectAtIndex:0] unsignedIntValue];
        BOOL unRead = [[params objectAtIndex:1] boolValue];
        contextBlock context = nil;
        if([params count] > 2) {
            context = [params objectAtIndex:2];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self updateMsgUnreadStatusByType:helper.database type:msgType unRead:unRead];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
        
    }

}

- (void)asyncUpdateMsgUnreadStatusById:(contextBlock)context msgId:(NSString *)msgId unRead:(BOOL)unRead
{
    if(_workThread && [msgId length] > 0) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:msgId, @(unRead), bContext, nil];
        [self performSelector:@selector(threadUpdateMsgUnreadStatusById:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadUpdateMsgUnreadStatusById:(NSArray *)params
{
    if(params && [params count] >= 2) {
        NSString* msgId = [params objectAtIndex:0];
        BOOL unRead = [[params objectAtIndex:1] boolValue];
        contextBlock context = nil;
        if([params count] > 2) {
            context = [params objectAtIndex:2];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self updateMsgUnreadStatusById:helper.database msgId:msgId unRead:unRead];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
        
    }
}

- (void)asyncUpdateFriendMsgStatus:(contextBlock)context msgId:(NSString *)msgId newExtData:(NSString *)newExtData
{
    if(_workThread && [msgId length] > 0 && newExtData) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:msgId, newExtData, bContext, nil];
        [self performSelector:@selector(threadUpdateFriendMsgStatus:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadUpdateFriendMsgStatus:(NSArray *)params
{
    if(params && [params count] >= 2) {
        NSString * msgId = [params objectAtIndex:0];
        NSString * extData = [params objectAtIndex:1];
        contextBlock context = nil;
        if([params count] > 2) {
            context = [params objectAtIndex:2];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self updateFriendMsgStatus:helper.database msgId:msgId newExtData:extData];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
        
    }

}

- (void)asyncDeleteMsgById:(contextBlock)context msgId:(NSString *)msgId
{
    if(_workThread && [msgId length] > 0) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:msgId, bContext, nil];
        [self performSelector:@selector(threadDeletemMsgById:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
-(void)threadDeletemMsgById:(NSArray *)params
{
    if(params && [params count] > 0) {
        NSString * msgId = [params objectAtIndex:0];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self deleteMsgById:helper.database msgId:msgId];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
        
    }
}

- (void)asyncDeleteMsgByType:(contextBlock)context type:(MsgBigType)msgType
{
    if(_workThread) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(msgType), bContext, nil];
        [self performSelector:@selector(threadDeletemMsgByType:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
-(void)threadDeletemMsgByType:(NSArray *)params
{
    if(params && [params count] > 0) {
        MsgBigType msgType = [[params objectAtIndex:0] unsignedIntValue];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self deleteMsgList:helper.database type:msgType];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
        
    }

}

- (void)asyncGetMsgUnreadCount:(contextBlock)context
{
    if(_workThread && context) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:bContext, nil];
        [self performSelector:@selector(threaGetMsgUnreadCount:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}

-(void)threaGetMsgUnreadCount:(NSArray *)params
{
    if(params && [params count] > 0) {
        contextBlock context  =  context = [params objectAtIndex:0];
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        __block NSDictionary * dict = [self getMsgUnreadCount:helper.database];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(dict);
            });
        }
        
    }
}

- (void)asyncGetMsgUnreadCountList:(contextBlock)context
{
    if(_workThread && context) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:bContext, nil];
        [self performSelector:@selector(threaGetMsgUnreadCountList:) onThread:_workThread withObject:array waitUntilDone:NO];
    }

}
-(void)threaGetMsgUnreadCountList:(NSArray *)params
{
    if(params && [params count] > 0) {
        contextBlock context  = [params objectAtIndex:0];
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        __block NSDictionary * retDict = [self getMsgUnreadCountList:helper.database];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(retDict);
            });
        }
        
    }
}

- (void)asyncGetFriendMsgUnreadCount:(contextBlock)context
{
    if(_workThread && context) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:bContext, nil];
        [self performSelector:@selector(threaGetFriendMsgUnreadCount:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
-(void)threaGetFriendMsgUnreadCount:(NSArray *)params
{
    if(params && [params count] > 0) {
        contextBlock context  =  context = [params objectAtIndex:0];
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        uint ret = [self getFriendMsgUnreadCount:helper.database];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(ret));
            });
        }
        
    }
}

- (void)asyncGetWalletUnreadCount:(contextBlock)context
{
    if (_workThread && context) {
        __strong contextBlock bContext = context;
        NSArray *array = [NSArray arrayWithObjects:bContext, nil];
        [self performSelector:@selector(threadGetWalletUnreadCount:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}

- (void)threadGetWalletUnreadCount:(NSArray *)params
{
    if (params && [params count] > 0) {
        contextBlock context = context = [params objectAtIndex:0];
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        uint ret = [self getWalletUnreadCount:helper.database];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(ret));
            });
        }
    }
}

@end
