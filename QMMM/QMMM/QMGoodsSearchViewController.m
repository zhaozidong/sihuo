//
//  QMGoodsSearchViewController.m
//  QMMM
//
//  Created by kingnet  on 15-1-12.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMGoodsSearchViewController.h"
#import "SearchHistoryViewController.h"
#import "GoodsHandler.h"
#import "SearchResultViewController.h"
#import "SearchHistoryDao.h"
#import "KeyWordEntity.h"
#import "QMFilterView.h"
#import "QMFilterItem.h"
#import "QMFilterSubViewController.h"
#import "QMFilterConditionView.h"
#import "ButtonAccessoryView.h"
#import "QMGoodsCateInfo.h"
#import "QMGetLocation.h"
#import "GoodDataInfo.h"
#import "GoodEntity.h"

#import "QMFailPrompt.h"

@interface QMGoodsSearchViewController ()<UISearchDisplayDelegate, UISearchBarDelegate, SearchHistoryViewControllerDelegate, QMFilterViewDataSource, QMFilterSubViewControllerDelegate, QMFilterConditionViewDelegate, SearchResultViewCtlDelegate>

{
    SearchHistoryViewController *historyViewCtl_;
    SearchResultViewController *searchResultViewCtl_;
    UITextField *textField_;
    UIButton *cancelButton_;
    
    NSInteger firstSelIndex_; //第一个块选择的index
    NSInteger secondSelIndex_; //第二块选择的index
    NSMutableArray *firstDataArr_;
    NSMutableArray *secondDataArr_;
    NSMutableArray *filterTitleArr_;
    
    __block NSMutableDictionary *filterDict_; //搜索的条件
    NSArray *goodsCateArr_; //商品的分类
    
    __block NSString *location_; //纬度,经度
    
    __block int offset_; //分页用的
}

@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic, strong) NSMutableArray *cateList;

@property (nonatomic, strong) QMFilterView *filterView;
@property (nonatomic, strong) QMFilterSubViewController *filterSubViewCtl;
@property (nonatomic, strong) QMFilterConditionView *conditionView;
@property (nonatomic, strong) ButtonAccessoryView *accessoryView;

@property (nonatomic, strong) QMFailPrompt *failPromt;  //错误提示

@end

@implementation QMGoodsSearchViewController

- (id)init
{
    self = [super init];
    if (self) {
        filterDict_ = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id)initWithCategoryId:(int)categoryId
{
    self = [self init];
    if (self) {
        if (categoryId > 0) {
            [filterDict_ setObject:[@(categoryId)stringValue] forKey:kSearchGoodsCategory];
        }
    }
    return self;
}

- (id)initWithKeyWords:(NSString *)keyWords
{
    self = [self init];
    if (self) {
        if (nil!=keyWords) {
            [filterDict_ setObject:keyWords forKey:kSearchGoodsWords];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.titleView = self.searchBar;
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    searchResultViewCtl_ = [[SearchResultViewController alloc]init];
    [self addChildViewController:searchResultViewCtl_];
    [searchResultViewCtl_ didMoveToParentViewController:searchResultViewCtl_];
    searchResultViewCtl_.view.frame = CGRectMake(0, kMainTopHeight+[QMFilterView viewHeight], CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-(kMainTopHeight+[QMFilterView viewHeight]));
    searchResultViewCtl_.delegate = self;
    [self.view addSubview:searchResultViewCtl_.view];
    
    [self setupFilter];
    
    [self.view addSubview:self.filterView];
    
    //键盘收回按钮
    _accessoryView = [[ButtonAccessoryView alloc]init];
    _accessoryView.originalFrame = CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds)-CGRectGetWidth(_accessoryView.frame)-5.f,CGRectGetHeight(self.view.frame), CGRectGetWidth(_accessoryView.frame), CGRectGetHeight(_accessoryView.frame));
    [self.view addSubview:_accessoryView];
    
    [self getCategory];
    
    //开始搜索
    [self getSearchResult];
    
    //获取地理位置
    [QMGetLocation getCurrentPoint:^(CLLocation *currentLoc, NSError *error) {
        if (!error) {
            CLLocationCoordinate2D coordinate = currentLoc.coordinate;
            location_ = [NSString stringWithFormat:@"%f,%f", coordinate.latitude, coordinate.longitude];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupFilter
{
    self.filterSubViewCtl = [[QMFilterSubViewController alloc]init];
    self.filterSubViewCtl.delegate = self;
    
    firstDataArr_ = [@[@"全部分类"]mutableCopy];
    secondDataArr_ = [@[@"最新发布",@"折扣最大", @"离我最近", @"价格最低"]mutableCopy];
    
    firstSelIndex_ = 0;
    secondSelIndex_ = 0;
    
    filterTitleArr_ = [@[]mutableCopy];
    [filterTitleArr_ addObject:[firstDataArr_ objectAtIndex:firstSelIndex_]];
    [filterTitleArr_ addObject:[secondDataArr_ objectAtIndex:secondSelIndex_]];
    [filterTitleArr_ addObject:@"筛选"];
}

- (void)getCategory
{
    __weak typeof(self) weakSelf = self;
    //先从本地拉取一次，没有的话再从服务器上拉取
    [[GoodsHandler shareGoodsHandler]getCategoryFromLocal:^(id obj) {
        [weakSelf setGoodsCates:(NSArray *)obj];
    } failed:^(id obj) {
        [[GoodsHandler shareGoodsHandler]getCategory:^(id obj) {
            [weakSelf setGoodsCates:(NSArray *)obj];
        } failed:^(id obj) {
            
        }];
    }];
}

- (void)setGoodsCates:(NSArray *)array
{
    goodsCateArr_ = array;
    int cateId = 0;
    if ([filterDict_ objectForKey:kSearchGoodsCategory]) {
        cateId = [[filterDict_ objectForKey:kSearchGoodsCategory] intValue];
    }
    int i=1;
    for (QMGoodsCateInfo * cateInfo in goodsCateArr_) {
        [firstDataArr_ addObject:cateInfo.name];
        if (cateId == cateInfo.cateId) {
            firstSelIndex_ = i;
        }
        i++;
    }
    
    if (firstSelIndex_ > 0) {
        NSString *string = [firstDataArr_ objectAtIndex:firstSelIndex_];
        [filterTitleArr_ replaceObjectAtIndex:0 withObject:string];
        [self.filterView reloadItemAtIndex:0];
    }
}

//从服务器拉取搜索结果
- (void)getSearchResult
{
    offset_ = 0;
    [filterDict_ setObject:[@(offset_)stringValue] forKey:kSearchGoodsOffset];
    [AppUtils showProgressMessage:@"加载中"];
    [self loadSearchGoods];
}

//加载更多处用
- (void)loadSearchGoods
{
    __weak typeof(self) weakSelf = self;
    [[GoodsHandler shareGoodsHandler]searchGoods:filterDict_ success:^(id obj) {
        [AppUtils dismissHUD];
        NSDictionary *retDic = (NSDictionary *)obj;
        [weakSelf reloadUI:retDic[@"list"]];
        offset_ += [retDic[@"offset"] intValue];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        [searchResultViewCtl_ endFooterRereshing];
    }];
}

- (void)reloadUI:(NSArray *)dataArr
{
    NSMutableArray *goodsArr = [NSMutableArray array];
    for (GoodEntity *goods in dataArr) {
        GoodsSimpleInfo *goodsInfo = [[GoodsSimpleInfo alloc]init];
        goodsInfo.gid = goods.gid;
        goodsInfo.photo = goods.picturelist.rawArray[0];
        goodsInfo.desc = goods.desc;
        goodsInfo.price = goods.price;
        [goodsArr addObject:goodsInfo];
    }
   
    if (offset_ == 0) {
        [searchResultViewCtl_ removeAllGoods];
        if (_failPromt) {
            [_failPromt removeFromSuperview];
            _failPromt = nil;
        }
    }
    [searchResultViewCtl_ addGoods:goodsArr];
    
    if (dataArr.count == 0) {
        //第一次重新拉取，没有商品时
        if (offset_ == 0) {
            if (nil == _failPromt) {
                _failPromt = [[QMFailPrompt alloc]initWithFailType:QMFailTypeNOData labelText:@"没有找到相关商品~" buttonType:QMFailButtonTypeNone buttonText:nil];
                _failPromt.frame = CGRectMake(0, kMainTopHeight+[QMFilterView viewHeight], kMainFrameWidth, kMainFrameHeight-kMainTopHeight-[QMFilterView viewHeight]);
                [self.view insertSubview:_failPromt aboveSubview:searchResultViewCtl_.view];
            }
        }
    }
    
    [searchResultViewCtl_ endFooterRereshing];
}

#pragma mark - init View
- (QMFilterConditionView *)conditionView
{
    if (!_conditionView) {
        _conditionView = [QMFilterConditionView filterConditionView];
        _conditionView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), [QMFilterConditionView viewHeight]);
        _conditionView.delegate = self;
    }
    return _conditionView;
}

- (QMFilterView *)filterView
{
    if (!_filterView) {
        _filterView = [[QMFilterView alloc] initWithFrame:CGRectMake(0, kMainTopHeight, CGRectGetWidth(self.view.frame), [QMFilterView viewHeight])];
        _filterView.dataSource = self;
    }
    return _filterView;
}

- (UISearchBar *)searchBar
{
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 44)];
        _searchBar.delegate = self;

        UIImage* clearImg = [UIImage imageNamed:@"search_bg"];
        clearImg = [clearImg stretchableImageWithLeftCapWidth:7 topCapHeight:5];
        [_searchBar setBackgroundImage:clearImg];
        [_searchBar setSearchFieldBackgroundImage:clearImg forState:UIControlStateNormal];
        [_searchBar setBackgroundColor:[UIColor clearColor]];
        [_searchBar setImage:[UIImage imageNamed:@"search_icon"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
        [_searchBar setImage:[UIImage imageNamed:@"searchClearDel_icon"] forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
        
        for (UIView *subView in _searchBar.subviews) {
            for (UIView *view in subView.subviews) {
                if ([view isKindOfClass:[UITextField class]]) {
                    textField_ = (UITextField *)view;
                }
                if ([view isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                    [view removeFromSuperview];
                }
            }
        }
        
        if (textField_) {
            UIFont *font = [UIFont systemFontOfSize:15];
            NSDictionary *attrDic = @{NSForegroundColorAttributeName:UIColorFromRGB(0xffd4d4), NSFontAttributeName:font};
            NSAttributedString *attrStr = [[NSAttributedString alloc]initWithString:@"输入关键字搜索商品" attributes:attrDic];
            textField_.attributedPlaceholder = attrStr;
            textField_.textColor = [UIColor whiteColor];
        }
        
        if ([filterDict_ objectForKey:kSearchGoodsWords]) {
            _searchBar.text = [filterDict_ objectForKey:kSearchGoodsWords];
        }
    }
    return _searchBar;
}

#pragma mark - QMFilterView Delegate
- (NSInteger)numberOfItemForFilterView:(QMFilterView *)filterView
{
    return [filterTitleArr_ count];
}

- (NSString *)filterView:(QMFilterView *)filterView titleForItem:(QMFilterItem *)filterItem
{
    return [filterTitleArr_ objectAtIndex:filterItem.index];
}

- (UIView *)filterView:(QMFilterView *)filterView viewAtIndex:(NSUInteger)index
{
    if (0 == index) {
         [self.filterSubViewCtl reloadTableWithData:firstDataArr_ selectedIndex:firstSelIndex_];
         return self.filterSubViewCtl.tableView;
    }
    else if (1 == index){
        [self.filterSubViewCtl reloadTableWithData:secondDataArr_ selectedIndex:secondSelIndex_];
        return self.filterSubViewCtl.tableView;
    }
    else{
        
        return self.conditionView;
    }
}

#pragma mark - QMFilterSubViewControllerDelegate
- (void)tableViewSelectedAtIndex:(NSInteger)selectedIndex
{
    //分类
    if (self.filterView.selectedIndex == 0) {
        firstSelIndex_ = selectedIndex;
        NSString *string = [firstDataArr_ objectAtIndex:selectedIndex];
        [filterTitleArr_ replaceObjectAtIndex:0 withObject:string];
        if (selectedIndex == 0) {
            [filterDict_ setObject:[@(selectedIndex)stringValue] forKey:kSearchGoodsCategory];
        }
        else{
            QMGoodsCateInfo *cateInfo = [goodsCateArr_ objectAtIndex:selectedIndex-1];
            [filterDict_ setObject:[@(cateInfo.cateId)stringValue] forKey:kSearchGoodsCategory];
        }
    }
    //排序
    else if (self.filterView.selectedIndex == 1){
        secondSelIndex_ = selectedIndex;
        NSString *string = [secondDataArr_ objectAtIndex:selectedIndex];
        [filterTitleArr_ replaceObjectAtIndex:1 withObject:string];
        if (selectedIndex == 0) {//最新发布
            [filterDict_ setObject:@"pub_ts_desc" forKey:kSearchGoodsSort];
            [filterDict_ removeObjectForKey:kSearchGoodsPoint];
            [filterDict_ removeObjectForKey:kSearchGoodsDistance];
            [filterDict_ removeObjectForKey:kSearchGoodsDiscount];
            [AppUtils trackCustomEvent:@"search_sort_publush" args:nil];
        }
        else if (selectedIndex == 1){//折扣最大
            [filterDict_ setObject:@"desc" forKey:kSearchGoodsDiscount];
            [filterDict_ removeObjectForKey:kSearchGoodsSort];
            [filterDict_ removeObjectForKey:kSearchGoodsPoint];
            [filterDict_ removeObjectForKey:kSearchGoodsDistance];
            [AppUtils trackCustomEvent:@"search_sort_discount" args:nil];
        }
        else if (selectedIndex == 2){//离我最近
            [filterDict_ setObject:@"asc" forKey:kSearchGoodsDistance];
            if (location_) {
                [filterDict_ setObject:location_ forKey:kSearchGoodsPoint];
            }
            [filterDict_ removeObjectForKey:kSearchGoodsSort];
            [filterDict_ removeObjectForKey:kSearchGoodsDiscount];
            [AppUtils trackCustomEvent:@"search_sort_distance" args:nil];
        }
        else if (selectedIndex == 3){//价格最低
            [filterDict_ setObject:@"price_asc" forKey:kSearchGoodsSort];
            [filterDict_ removeObjectForKey:kSearchGoodsPoint];
            [filterDict_ removeObjectForKey:kSearchGoodsDistance];
            [filterDict_ removeObjectForKey:kSearchGoodsDiscount];
            [AppUtils trackCustomEvent:@"search_sort_price" args:nil];
        }
    }
    [self.filterView reloadItemAtIndex:self.filterView.selectedIndex];
    __weak typeof(self) weakSelf = self;
    [self.filterView dismissWithCompletion:^{
        [weakSelf getSearchResult];
    }];
}

#pragma mark - QMFilterConditionViewDelegate
- (void)conditionView:(QMFilterConditionView *)conditionView conditions:(NSArray *)conditions priceMin:(float)priceMin priceMax:(float)priceMax
{
    __weak typeof(self) weakSelf = self;
    [self.filterView dismissWithCompletion:^{
        if (priceMin!=-1) {
            NSString *minStr = [NSString stringWithFormat:@"%.2f", priceMin];
            [filterDict_ setObject:minStr forKey:kSearchGoodsPriceMin];
        }
        else{
            [filterDict_ removeObjectForKey:kSearchGoodsPriceMin];
        }
        if (priceMax!=-1) {
            NSString *maxStr = [NSString stringWithFormat:@"%.2f", priceMax];
            [filterDict_ setObject:maxStr forKey:kSearchGoodsPriceMax];
        }
        else{
            [filterDict_ removeObjectForKey:kSearchGoodsPriceMax];
        }
        if ([conditions count]>0) {
            NSString *str = [conditions componentsJoinedByString:@","];
            [filterDict_ setObject:str forKey:kSearchGoodsCondition];
        }
        else{
            [filterDict_ removeObjectForKey:kSearchGoodsCondition];
        }
        [weakSelf getSearchResult];
    }];
}

#pragma mark - SearchResultViewCtlDelegate
- (void)loadMoreData
{
    [filterDict_ setObject:[@(offset_)stringValue] forKey:kSearchGoodsOffset];
   
    [self loadSearchGoods];
}

#pragma mark - UISearchBar Delegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [self.filterView dismissWithCompletion:nil];
    
    [searchBar setShowsCancelButton:YES animated:YES];
    self.navigationItem.leftBarButtonItems = nil;
    self.navigationItem.hidesBackButton = YES;
    
    if(kIOS7) {
        UIView *topView = _searchBar.subviews[0];
        for (UIView *subView in topView.subviews) {
            if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
                cancelButton_ = (UIButton*)subView;
                [cancelButton_ setTitle:[AppUtils localizedProductString:@"QM_Text_Cancel"] forState:UIControlStateNormal];
            }
        }
    } else {
        for(UIView *subView in _searchBar.subviews){
            if([subView isKindOfClass:[UIButton class]]){
                cancelButton_ = (UIButton*)subView;
                [cancelButton_ setTitle:[AppUtils localizedProductString:@"QM_Text_Cancel"] forState:UIControlStateNormal];
            }
        }
    }
    
    //显示搜索历史记录
    if (nil == historyViewCtl_) {
        historyViewCtl_ = [[SearchHistoryViewController alloc]init];
        historyViewCtl_.delegate = self;
        [self addChildViewController:historyViewCtl_];
        [historyViewCtl_ didMoveToParentViewController:self];
        [historyViewCtl_ showInView:self.view];
    }

    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    //取消搜索
    [searchBar setShowsCancelButton:NO animated:YES];
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    [textField_ resignFirstResponder];
    
    //如果关键字被清掉了则移除params中的words
    NSString *temp = [searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (temp.length == 0) {
        [filterDict_ removeObjectForKey:kSearchGoodsWords];
    }
    [historyViewCtl_ dismissWithCompletion:^{
        historyViewCtl_ = nil;
    }];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    //点击搜索按钮
    NSString *searchText = _searchBar.text;
    
    [self searchBarCancelButtonClicked:_searchBar];
    
    [self searchWithWords:searchText];
    
    //存储搜索记录
    KeyWordEntity *entity = [[KeyWordEntity alloc]init];
    entity.keyWord = searchText;
    [[SearchHistoryDao sharedInstance]insert:entity];
}

#pragma mark - 
- (void)closeKeyBorad
{
    [textField_ resignFirstResponder];
    cancelButton_.enabled = YES;
}

//选中搜索历史的某一行
- (void)selectRow:(KeyWordEntity *)entity
{
    _searchBar.text = entity.keyWord;
    [self searchBarCancelButtonClicked:_searchBar];
    
    [self searchWithWords:entity.keyWord];
}

//搜索关键字
- (void)searchWithWords:(NSString *)keyWords
{
    //将所有筛选条件都还原
    [filterDict_ removeAllObjects];
    [filterDict_ setObject:keyWords forKey:kSearchGoodsWords];
    firstSelIndex_ = 0;
    secondSelIndex_ = 0;
    NSString *string = [firstDataArr_ objectAtIndex:firstSelIndex_];
    [filterTitleArr_ replaceObjectAtIndex:0 withObject:string];
    NSString *string1 = [secondDataArr_ objectAtIndex:secondSelIndex_];
    [filterTitleArr_ replaceObjectAtIndex:1 withObject:string1];
    
    [self.filterView reloadItemAtIndex:0];
    [self.filterView reloadItemAtIndex:1];
    
    [self.conditionView clearView];
    
    [self getSearchResult];
}

@end
