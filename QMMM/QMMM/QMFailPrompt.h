//
//  QMFailPrompt.h
//  QMMM
//
//  Created by Derek.zhao on 14-12-9.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum{
    QMFailTypeNoNetWork=0,
    QMFailTypeNOData,
    QMFailTypeLoadFail
} QMFailType;

typedef enum {
    QMErrorShowTypeLabel=0,
    QMErrorShowTypeButton
}QMErrorShowType;

typedef NS_ENUM(NSInteger, QMFailButtonType) {
    QMFailButtonTypeNone,
    QMFailButtonTypeNormal,
    QMFailButtonTypeFail
};


@protocol QMFailPromptDelegate <NSObject>

//-(void)ReloadButtonClick:(UIButton *)sender;
@optional;
-(void) placeHolderButtonClick:(id)sender;

@end

@interface QMFailPrompt : UIView

@property(nonatomic,weak) id<QMFailPromptDelegate> delegate;

//-(id)initWithFailType:(QMFailType)failType andLabelText:(NSString *)failMessage andHasButton:(BOOL)hasButton;

-(id)initWithFailType:(QMFailType)failType labelText:(NSString *)labelText buttonType:(QMFailButtonType)buttonType buttonText:(NSString *)buttonText;


@end
