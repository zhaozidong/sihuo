//
//  Friendhandle.m
//  QMMM
//
//  Created by hanlu on 14-10-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "Friendhandle.h"
#import <AddressBook/AddressBook.h>
#import "FriendInfo.h"
#import "ContactInfo.h"
#import "FriendApi.h"
#import "QMFriendHelper.h"
#import "QMDataCenter.h"
#import "QMMsgHelper.h"
#import "APAddressBook.h"
#import "APContact.h"

@interface Friendhandle () {
    FriendApi * _friendApi;
    QMFriendHelper * _friendHelper;
    QMMsgHelper * _msgHelper;
    
}

@property (nonatomic, strong)  NSMutableArray * friendList;

@property (nonatomic, strong)  NSMutableArray * friendSectionList;

@end

@implementation Friendhandle

static Friendhandle* _shareInstance = NULL;

static unichar getFirstLetter(NSString *str) {
    unichar result = 0;
    if (str && ([str length] > 0)) {
        result = [str characterAtIndex:0];
    }// if
    return result;
}

static unichar uppercaseLetter(unichar c) {
    // 将小写字母转换为大写
    unichar result = c;
    if (((c >= 97) && (c <= 122))) {
        result = result - (97 - 65);
    }// if
    return result;
}

static BOOL isLetterChar(unichar c) {
    // 判断首字符是否在 a-z 97-122 A-Z 65-90的范围内
    return ((c >= 97) && (c <= 122)) || ((c >= 65) && (c <= 90));
}

static BOOL isFirstLetter(NSString *str) {
    return isLetterChar(getFirstLetter(str));
}

static NSInteger compareFirstLetter(unichar ac, unichar bc) {
    NSInteger result = NSOrderedSame;
    if (ac < bc) {
        result = NSOrderedAscending;
    } else {
        if (ac > bc) {
            result = NSOrderedDescending;
        }
    }
    return result;
}

static NSInteger pinyinSortFriend(id uiPerson1, id uiPerson2, void *reverse) {
    // 用于排序装着FriendInfo的NSMutableArray
    // 1. 按照拼音首字母A-Z a-z排序其它的往后排
    unichar ac = uppercaseLetter(getFirstLetter([uiPerson1 pinyin]));
    unichar bc = uppercaseLetter(getFirstLetter([uiPerson2 pinyin]));
    NSInteger result = compareFirstLetter(ac, bc);
    BOOL pa = isLetterChar(ac);
    BOOL pb = isLetterChar(bc);
    // NSOrderedAscending = -1, NSOrderedSame, NSOrderedDescending
    if ((pa == YES) && (pb == NO)) {
        result = NSOrderedAscending;
    } else {
        if ((pa == NO) && (pb == YES)) {
            result = NSOrderedDescending;
        } else {
            // 2. 拼音首字母顺序一样的按原文顺序排
            if (NSOrderedSame == result) {
                // 原文英文字母排前面，之后是其它字符顺序
                BOOL oa = isFirstLetter([uiPerson1 showname]);
                BOOL ob = isFirstLetter([uiPerson2 showname]);
                if ((oa == YES) && (ob == NO)) {
                    result = NSOrderedAscending;
                } else {
                    if ((oa == NO) && (ob == YES)) {
                        result = NSOrderedDescending;
                    } else {
                        // 按全拼排序
                        result =[[uiPerson1 pinyin] compare:[uiPerson2 pinyin] options:(NSNumericSearch | NSWidthInsensitiveSearch)];
                        if (NSOrderedSame == result) {
                            // 全拼一样的再按中文顺序排
                            //result = [[[uiPerson1 person] personName] localizedCompare:[[uiPerson2 person] personName]];
                            //result = [[[uiPerson1 person] personName] compare:[[uiPerson2 person] personName]];
                            // 搜索数字例如：'xy1' < 'xy2' < 'xy4' < '13' < 'xy23'
                            result = [[uiPerson1 showname] compare:[uiPerson2 showname] options:(NSNumericSearch | NSWidthInsensitiveSearch)];
                            if (NSOrderedSame == result) {
                                // 3. 名称完全一样的再按personId排
                                //                                int personId1 = [[uiPerson1 person] personId];
                                //                                int personId2 = [[uiPerson2 person] personId];
                                //                                if (personId1 > personId2) {
                                //                                    result = NSOrderedAscending;
                                //                                } else {
                                //                                    if (personId1 < personId2) {
                                //                                        result = NSOrderedDescending;
                                //                                    }// if
                                //                                }// if
                                result = NSOrderedAscending;
                            }// if
                        }
                    }// if
                }// if
            }// if
        }// if
    }// if
    return result;
}

static NSInteger pinyinSortContact(id uiPerson1, id uiPerson2, void *reverse) {
    // 用于排序装着ContactInfo的NSMutableArray
    // 1. 按照拼音首字母A-Z a-z排序其它的往后排
    unichar ac = uppercaseLetter(getFirstLetter([uiPerson1 pinyinPersonName]));
    unichar bc = uppercaseLetter(getFirstLetter([uiPerson2 pinyinPersonName]));
    NSInteger result = compareFirstLetter(ac, bc);
    BOOL pa = isLetterChar(ac);
    BOOL pb = isLetterChar(bc);
    // NSOrderedAscending = -1, NSOrderedSame, NSOrderedDescending
    if ((pa == YES) && (pb == NO)) {
        result = NSOrderedAscending;
    } else {
        if ((pa == NO) && (pb == YES)) {
            result = NSOrderedDescending;
        } else {
            // 2. 拼音首字母顺序一样的按原文顺序排
            if (NSOrderedSame == result) {
                // 原文英文字母排前面，之后是其它字符顺序
                BOOL oa = isFirstLetter([uiPerson1 personName]);
                BOOL ob = isFirstLetter([uiPerson2 personName]);
                if ((oa == YES) && (ob == NO)) {
                    result = NSOrderedAscending;
                } else {
                    if ((oa == NO) && (ob == YES)) {
                        result = NSOrderedDescending;
                    } else {
                        // 按全拼排序
                        result =[[uiPerson1 pinyinPersonName] compare:[uiPerson2 pinyinPersonName] options:(NSNumericSearch | NSWidthInsensitiveSearch)];
                        if (NSOrderedSame == result) {
                            // 全拼一样的再按中文顺序排
                            //result = [[[uiPerson1 person] personName] localizedCompare:[[uiPerson2 person] personName]];
                            //result = [[[uiPerson1 person] personName] compare:[[uiPerson2 person] personName]];
                            // 搜索数字例如：'xy1' < 'xy2' < 'xy4' < '13' < 'xy23'
                            result = [[uiPerson1 personName] compare:[uiPerson2 personName] options:(NSNumericSearch | NSWidthInsensitiveSearch)];
                            if (NSOrderedSame == result) {
                                // 3. 名称完全一样的再按personId排
                                //                                int personId1 = [[uiPerson1 person] personId];
                                //                                int personId2 = [[uiPerson2 person] personId];
                                //                                if (personId1 > personId2) {
                                //                                    result = NSOrderedAscending;
                                //                                } else {
                                //                                    if (personId1 < personId2) {
                                //                                        result = NSOrderedDescending;
                                //                                    }// if
                                //                                }// if
                                result = NSOrderedAscending;
                            }// if
                        }
                    }// if
                }// if
            }// if
        }// if
    }// if
    return result;
}

+ (Friendhandle *)shareFriendHandle
{
    @synchronized([Friendhandle class]) {
        if (nil == _shareInstance) {
            _shareInstance = [[Friendhandle alloc] init];
        }
        return _shareInstance;
    }
}

+ (void)freeFriendHandle
{
    if(_shareInstance != nil) {
        _shareInstance = nil;
    }
}

- (id)init
{
    self = [super init];
    if(self) {
        _friendList = [[NSMutableArray alloc] init];
        _friendSectionList = [[NSMutableArray alloc] initWithCapacity:30];
        
        _friendApi = [[FriendApi alloc] init];
        
        _friendHelper = [QMDataCenter sharedDataCenter].friendHelper;
        
        _msgHelper = [QMDataCenter sharedDataCenter].msgHelper;
    }
    return self;
}

- (void)dealloc
{
    [_friendList removeAllObjects];
    [_friendSectionList removeAllObjects];
}

//获取好友列表
- (void)getFriendList
{
    [self getFriendListFromServer];
}

//获取本地好友列表
- (void)getLocalFriendList
{
    //清空
    [_friendList removeAllObjects];
    [_friendSectionList removeAllObjects];
    
    //防止循环引用
    __weak NSMutableArray  *  weakFriendList = _friendList;
    __weak NSMutableArray * weakFriendSectionList = _friendSectionList;
    __weak Friendhandle * weakFriendHandle = self;
    
    [_friendHelper asyncLoadFriendList:^(id result) {
        if([result isKindOfClass:[NSArray class]] && [result count] > 0) {
            
            __block NSMutableArray * resultArray = [[NSMutableArray alloc] initWithArray:result];
            
            //好友排序与好友字母节可能比较耗时，再次异步调用，
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                
                NSMutableArray * strongFriendList = (NSMutableArray*)weakFriendList;
                NSMutableArray * strongFriendSectionList = (NSMutableArray*)weakFriendSectionList;
                
                [strongFriendList addObjectsFromArray:resultArray];
                
                //sort
                [strongFriendList sortUsingFunction:pinyinSortFriend context:nil];
                
                //build section
                [weakFriendHandle buildPinYinSections:strongFriendSectionList sortList:strongFriendList];
                
                //UI
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:kFriendListLoadedNotify object:nil];
                    
                    [weakFriendHandle getFriendListFromServer];
                });
            });
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kFriendListLoadedNotify object:nil];
            
            [weakFriendHandle getFriendListFromServer];
        }
        
        
    }];
}

//从服务端获取好友列表
- (void)getFriendListFromServer
{
    //防止循环引用
    __weak NSMutableArray  *  weakFriendList = _friendList;
    __weak NSMutableArray * weakFriendSectionList = _friendSectionList;
    __weak Friendhandle * weakFriendHandle = self;
    __weak QMFriendHelper * weakFriendHelper = _friendHelper;
    
    [_friendApi getFriendList:^(id result) {
        if ([result[@"s"] intValue] == 0) {
            
            //清空
            [weakFriendList removeAllObjects];
            [weakFriendSectionList removeAllObjects];
            
            NSArray *dataArray = result[@"d"];
            if([dataArray count] > 0) {
                
                __block NSMutableArray * resultArray = [[NSMutableArray alloc] initWithArray:dataArray];
                
                //好友排序与好友字母节可能比较耗时，再次异步调用，
                dispatch_async(dispatch_get_global_queue(0, 0), ^{
                    
                    NSMutableArray * strongFriendList = (NSMutableArray*)weakFriendList;
                    NSMutableArray * strongFriendSectionList = (NSMutableArray*)weakFriendSectionList;
                    
                    for (NSDictionary * dict in resultArray) {
                        FriendInfo * info = [[FriendInfo alloc] initWithDictionary:dict];
                        [strongFriendList addObject:info];
                    }
                    
                    if([strongFriendList count] > 0) {
                        //sort
                        [strongFriendList sortUsingFunction:pinyinSortFriend context:nil];
                        
                        //build section
                        [weakFriendHandle buildPinYinSections:strongFriendSectionList sortList:strongFriendList];
                        
                        //DB
                        [weakFriendHelper asyncDeleteAllFriend:nil];
                        [weakFriendHelper asyncAddFriendList:nil list:strongFriendList];
                    }
                    
                    //UI
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:kFriendListLoadedNotify object:nil];
                    });
                });
                
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:kFriendListLoadedNotify object:nil];
            }
        }
        else{
            [[NSNotificationCenter defaultCenter] postNotificationName:kFriendListLoadedFailedNotify object:result[@"d"]];
        }
    }];

}

//加载通讯录列表
- (void)loadContact:(contextBlock)context
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        CFErrorRef error = nil;
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
        if(addressBook) {
            __block NSMutableArray * contactArray = [[NSMutableArray alloc] init];
            
            CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
            CFIndex personCount = CFArrayGetCount(allPeople);
            for (CFIndex i = 0; i < personCount; i ++) {
                ABRecordRef record =  CFArrayGetValueAtIndex(allPeople, i);
                ContactInfo * info = [[ContactInfo alloc] initWithRecordRef:record];
                if([info.phoneList count] > 0) {
                    [contactArray addObject:info];
                }
            }
            CFRelease(allPeople);
            CFRelease(addressBook);
           
            //sort
            [contactArray sortUsingFunction:pinyinSortContact context:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if(context) {
                    context(contactArray);
                }
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(context) {
                    context(nil);
                }
            });
        }
    });
}

-(void)buildPinYinSections:(NSMutableArray *)sections sortList:(NSArray *)sortList {
    [sections removeAllObjects];
    
    int i, pos = 0;
    FriendSectionInfo *lastSection;
    unichar firstChar, priorFirstChar;
    priorFirstChar = 0;
    if ([sortList count] > 0) {
        priorFirstChar = uppercaseLetter(getFirstLetter([[sortList objectAtIndex:0] pinyin]));
        pos = 0;
    }
    static NSString *kJin = @"#";
    for (i = 1; i < [sortList count]; i++) {
        // 得到第一个拼音字符
        firstChar = uppercaseLetter(getFirstLetter([[sortList objectAtIndex:i] pinyin]));
        if (firstChar != priorFirstChar) {
            lastSection = [[FriendSectionInfo alloc] initWithSection:[NSString stringWithFormat:@"%c", priorFirstChar] pos:pos length:(i - pos)];
            [sections addObject:lastSection];
            priorFirstChar = firstChar;
            pos = i;
        }// if
        if (!isLetterChar(firstChar)) {
            // 由于排过序剩下的全部是#
            break;
        }// if
    }// for
    if (([sortList count] - pos) > 0) {
        if (isLetterChar(priorFirstChar)) {
            lastSection = [[FriendSectionInfo alloc] initWithSection:[NSString stringWithFormat:@"%c", priorFirstChar] pos:pos length:([sortList count] - pos)];
            [sections addObject:lastSection];
        } else {
            lastSection = [[FriendSectionInfo alloc] initWithSection:kJin pos:pos length:([sortList count] - pos)];
            [sections addObject:lastSection];
        }// if
    }// if
}

- (void)inviteFriend:(NSString *)friendList isInviteAll:(BOOL)isInviteAll success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_friendApi inviteFriend:friendList isInviteAll:isInviteAll context:^(id result) {
        if ([result[@"s"] intValue] == 0) {
            id obj = result[@"d"];
            if (success) {
                success(obj);
            }
        } else {
            id obj = result[@"d"];
            if (failed) {
                failed(obj);
            }
        }
    }];
}

//获取好友消息
- (void)getLocalFriendMsgList:(contextBlock)context
{
    [_msgHelper asyncLoadMsgList:^(id result) {
        if(context) {
            context(result);
        }
    } type:EMT_FRIEND];
}

//获取新好友未读消息数
- (void)getFriendUnreadMsgCount:(contextBlock)context
{
    [_msgHelper asyncGetFriendMsgUnreadCount:^(id result) {
        if(context) {
            context(result);
        }
    }];
}

//接受好友
- (void)acceptFriend:(uint64_t)fid token:(NSString *)token context:(contextBlock)context
{
    [_friendApi acceptFriend:fid token:token context:^(id result) {
        if ([result[@"s"] intValue] == 0) {
            id obj = result[@"d"];
            if(context) {
                context(obj);
            }
        } else {
            if(context) {
                context(@(NO));
            }
        }

    }];
}

//更新好友
- (void)updateFriend:(contextBlock)context friendInfo:(FriendInfo *)friendInfo
{
    if(friendInfo) {
        [_friendHelper asyncUpdateFriend:^(id result) {
            if(context) {
                context(friendInfo);
            }
        } userInfo:friendInfo];
    }
}

//更新好友消息的状态
- (void)updateLocalFriendMsgStatus:(contextBlock)context msgData:(MsgData *)msgData
{
    if(msgData.msgType == EMT_INVITE_FRIEND) {
        [_msgHelper asyncUpdateFriendMsgStatus:nil msgId:msgData.msgId newExtData:msgData.extData];
    }
}

//修改好友备注
- (void)modifyFriendRemark:(contextBlock)context friendId:(uint64_t)fid remark:(NSString *)remark
{
    [_friendApi modifyFriendRemark:fid remark:remark context:^(id result) {
        if ([result[@"s"] intValue] == 0) {
            id obj = result[@"d"];
            if(context) {
                context(obj);
            }
        } else {
            if(context) {
                context(@(NO));
            }
        }
    }];
}

//更新好友备注
- (void)updateFriendRemark:(FriendInfo *)friendInfo
{
    [_friendHelper asyncUpdateFriend:nil userInfo:friendInfo];
}

//获取可能认识的人
- (void)getProbablyKnowPeople:(contextBlock)context{
    __block NSMutableArray *arrFriend=[NSMutableArray arrayWithCapacity:5];
    [_friendApi getProbablyKnowList:^(id result) {
        if (result && [result isKindOfClass:[NSArray class]]) {
            for (NSDictionary *dictInfo in result) {
                FriendInfo *friend=[[FriendInfo alloc] initWithDictionary:dictInfo];
                [arrFriend addObject:friend];
            }
        }
        if(context) {
            context(arrFriend);
        }
    }];
}

- (void)inviteAllFriends:(SuccessBlock)success failed:(FailedBlock)failed
{
    switch ([APAddressBook access]) {
        case APAddressBookAccessUnknown:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (failed) {
                    failed(@"unknown");
                }
            });
        }
            break;
        case APAddressBookAccessDenied:
        {
            //拒绝访问通讯录
            dispatch_async(dispatch_get_main_queue(), ^{
                if (failed) {
                    failed(@"denied");
                }
            });
        }
            break;
        case APAddressBookAccessGranted:
        {
            //获取所有好友
            [_friendApi getFriendList:^(id result) {
                if ([result[@"s"] intValue] == 0) {
                    NSArray *dataArr = [result objectForKey:@"d"];
                    NSMutableArray *friendsArr = [NSMutableArray array];
                    NSMutableArray *friendsPhoneList = [NSMutableArray array];
                    for (NSDictionary *dict in dataArr) {
                        FriendInfo *info = [[FriendInfo alloc]initWithDictionary:dict];
                        [friendsArr addObject:info];
                        [friendsPhoneList addObject:info.cellphone];
                    }
                    
                    //读通讯录
                    APAddressBook *addressBook = [[APAddressBook alloc]init];
                    addressBook.fieldsMask = APContactFieldDefault;
                    addressBook.filterBlock = ^BOOL(APContact *contact){
                        return contact.phones.count > 0;
                    };
                    
                    [addressBook loadContactsOnQueue:dispatch_get_global_queue(0, 0) completion:^(NSArray *contacts, NSError *error) {
                        if (!error) {
                            NSMutableArray *inviteList = [NSMutableArray array];
                            for (APContact *info in contacts) {
                                for (NSString *cellString in info.phones) {
                                    NSString *tempStr = [[[cellString stringByReplacingOccurrencesOfString:@"+86" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
                                    if (![friendsPhoneList containsObject:tempStr]) {
                                        [inviteList addObject:cellString];
                                    }
                                }
                            }
                            
                            //发送邀请
                            if (inviteList.count > 0) {
                                NSString *phone = [inviteList componentsJoinedByString:@","];
                                [_friendApi inviteFriend:phone isInviteAll:YES context:^(id result) {
                                    if ([result[@"s"] intValue] == 0) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            if (success) {
                                                success(kHTTPTreatSuccess);
                                            }
                                        });
                                    }
                                    else{
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            if (failed) {
                                                failed(result[@"d"]);
                                            }
                                        });
                                    }
                                }];
                            }
                            else{
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    if (failed) {
                                        failed(@"暂无联系人可邀请");
                                    }
                                });
                            }
                        }
                        else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (failed) {
                                    failed(@"读取通讯录失败");
                                }
                            });
                        }
                    }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (failed) {
                            failed(result[@"d"]);
                        }
                    });
                }
            }];
        }
            break;
    }
}

@end
