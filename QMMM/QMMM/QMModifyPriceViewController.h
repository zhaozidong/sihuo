//
//  QMModifyPriceViewController.h
//  QMMM
//
//  Created by Derek.zhao on 14-12-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^blkComplete)(float);

@interface QMModifyPriceViewController : UIViewController

@property(nonatomic,assign)float price;

@property (strong, nonatomic) IBOutlet UITextField *txtPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;

-(void)setOrderId:(NSString *)orderId totalPay:(float)total completeBlock:(blkComplete)complete;


@end
