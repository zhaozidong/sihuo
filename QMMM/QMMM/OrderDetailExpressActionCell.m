//
//  OrderDetailExpressActionCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderDetailExpressActionCell.h"
#import "OrderDataInfo.h"

NSString * const kOrderDetailExpressActionCellIdentifier = @"kOrderDetailExpressActionCellIdentifier";

@interface OrderDetailExpressActionCell ()
@property (weak, nonatomic) IBOutlet UIButton *expressCallBtn;
@property (weak, nonatomic) IBOutlet UIButton *checkExpressBtn;

@end

@implementation OrderDetailExpressActionCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"OrderDetailExpressActionCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    UIImage * image = [UIImage imageNamed:@"gray_border_btn_bkg"];
    [self.expressCallBtn setBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(10, 7, 10, 7)] forState:UIControlStateNormal];
    [self.expressCallBtn setTitleColor:kGrayTextColor forState:UIControlStateNormal];
    
    UIImage * image1 = [UIImage imageNamed:@"border_btn_bkg"];
    [self.checkExpressBtn setBackgroundImage:[image1 resizableImageWithCapInsets:UIEdgeInsetsMake(10, 7, 10, 7)] forState:UIControlStateNormal];
    [self.checkExpressBtn setTitleColor:kRedColor forState:UIControlStateNormal];
    
    [self.expressCallBtn addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.checkExpressBtn addTarget:self action:@selector(checkExpressAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight
{
    return 45.f;
}

- (void)callAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapCallExpress)]) {
        [self.delegate didTapCallExpress];
    }
}

- (void)checkExpressAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapCheckExpress)]) {
        [self.delegate didTapCheckExpress];
    }
}

@end
