//
//  QMChatListCell.m
//  QMMM
//
//  Created by hanlu on 14-10-3.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMChatListCell.h"
#import "NSDate+Category.h"
#import "ConvertToCommonEmoticonsHelper.h"
#import "UIImageView+QMWebCache.h"
#import "UserInfoHandler.h"
#import "UserBasicInfo.h"
#import "QMNumView.h"
#import "NSString+JSONCategories.h"
#import "MsgData.h"
#import "MessageModel.h"
#import "MessageModelManager.h"

NSString * const kQMChatListCellIdentifier = @"kQMChatListCellIdentifier";

@implementation QMChatListCell

+ (id)cellForChatList
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMChatListCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

+ (CGFloat)heightForChatList
{
    return 55;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Initialization code
    
    [_headView setHeadViewStyle:QMRoundHeadViewSmall];
    _headView.clickEnable = NO;
    
    [_timeLabel setTextColor:UIColorFromRGB(0x9198a3)];
    [_nameLabel setTextColor:kDarkTextColor];
    [_contentLabel setTextColor:UIColorFromRGB(0x9198a3)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)UpdateUI:(EMConversation *)data
{
    uint64_t userId = (uint64_t)[data.chatter longLongValue];
    UserBasicInfo * userInfo = [[UserInfoHandler sharedInstance] getBasicUserInfo:userId];
    if(nil == userInfo) {
        _nameLabel.text = data.chatter;
    } else {
        [_headView setImageUrl:userInfo.icon];
        if([[userInfo showname] length] > 0) {
            _nameLabel.text = [userInfo showname];
        } else {
            _nameLabel.text = data.chatter;
        }
    }
    _contentLabel.text = [self subTitleMessageByConversation:data];
    _timeLabel.text = [self lastMessageTimeByConversation:data];
    [_numView setNum:[self unreadMessageCountByConversation:data]];
    [_unreadImageView setHidden:YES];
}

// 得到最后消息时间
-(NSString *)lastMessageTimeByConversation:(EMConversation *)conversation
{
    NSString *ret = @"";
    EMMessage *lastMessage = [conversation latestMessage];;
    if (lastMessage) {
        ret = [NSDate formattedTimeFromTimeInterval:lastMessage.timestamp];
    }
    
    return ret;
}

// 得到未读消息条数
- (NSInteger)unreadMessageCountByConversation:(EMConversation *)conversation
{
    NSInteger ret = 0;
    ret = conversation.unreadMessagesCount;
    
    return  ret;
}

// 得到最后消息文字或者类型
-(NSString *)subTitleMessageByConversation:(EMConversation *)conversation
{
    NSString *ret = @"";
    EMMessage *lastMessage = [conversation latestMessage];
    if (lastMessage) {
        id<IEMMessageBody> messageBody = lastMessage.messageBodies.lastObject;
        switch (messageBody.messageBodyType) {
            case eMessageBodyType_Image:{
                ret = @"[图片]";
            } break;
            case eMessageBodyType_Text:{
                MessageModel *msgModel = [MessageModelManager modelWithMessage:lastMessage];
                if ([msgModel isGoodsSendMsg]) {
                    ret = @"[商品]";
                }
                else{
                    NSString * msgText = [self getTextString:lastMessage.ext];
                    if([msgText length] > 0) {
                        ret = msgText;
                    } else {
                        NSString *didReceiveText = [ConvertToCommonEmoticonsHelper
                                                    convertToSystemEmoticons:((EMTextMessageBody *)messageBody).text];
                        ret = didReceiveText;
                    }
                }
            } break;
            case eMessageBodyType_Voice:{
                ret = @"[声音]";
            } break;
            case eMessageBodyType_Location: {
                ret = @"[位置]";
            } break;
            case eMessageBodyType_Video: {
                ret = @"[视频]";
            } break;
            default: {
            } break;
        }
    }
    
    return ret;
}

- (NSString *)getTextString:(NSDictionary *)dict
{
    NSString * displayString = @"";
    
    if(dict && [dict objectForKey:@"tpl"]) {
        NSDictionary * argDict = nil;
        id temp = [dict objectForKey:@"arg"];
        
        if([temp isKindOfClass:[NSDictionary class]]) {
            argDict = temp;
        } else if([temp isKindOfClass:[NSString class]]){
            argDict = [temp toDict];
        }
        else if ([temp isKindOfClass:[NSData class]]){
            NSError *error = nil;
            argDict = [NSJSONSerialization JSONObjectWithData:temp options:kNilOptions error:&error];
            if (error) {
                argDict = nil;
            }
        }
        if(argDict && [argDict count] > 0) {
            
            //商品描述10字显示限制
            NSString * goodsDesc = [argDict objectForKey:@"desc"];
            if([goodsDesc length] > 10) {
                goodsDesc = [goodsDesc substringToIndex:10];
                goodsDesc = [goodsDesc stringByAppendingString:@"..."];
            }
            
            NSString * displayString = @"";
            int msgType = [[dict objectForKey:@"tpl"] intValue];
            if(msgType == EMT_GOODS_PAYED) {
                displayString = [NSString stringWithFormat:@"亲，您的商品\"%@\"已经被我拍下啦，快来给我安排发货吧~点此查看详情", goodsDesc];
            } else if(msgType == EMT_GOODS_DELIVERED) {
                displayString = [NSString stringWithFormat:@"亲，您拍下的商品\"%@\"已经发货啦，点此查看详情", goodsDesc];
            } else if(msgType == EMT_GOODS_RECEIVED) {
                displayString = [NSString stringWithFormat:@"亲，您的商品\"%@\"已经被我签收并评价啦~点此查看详情", goodsDesc];
            }
        }
    }
    
    return displayString;
}

@end
