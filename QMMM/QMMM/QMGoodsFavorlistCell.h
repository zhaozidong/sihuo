//
//  QMGoodsFavorlistCell.h
//  QMMM
//
//  Created by 韩芦 on 14-9-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QMGoodsBaseTableViewCell.h"
#import "QMClickNameView.h"

@class GoodEntity;

extern NSString * const kQMGoodsFavorlistCellIndentifier;

@interface QMGoodsFavorlistCell : UITableViewCell

@property (nonatomic, weak) id<QMClickNameViewDelegate> delegate;

+ (UINib *)nib;

- (void)updateUI:(GoodEntity *)goodsInfo;

+ (CGFloat)cellHeight;

+ (CGFloat)heightWithGoodsEntity:(GoodEntity *)goodsInfo;

@end
