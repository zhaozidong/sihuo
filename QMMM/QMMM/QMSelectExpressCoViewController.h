//
//  QMSelectExpressCoViewController.h
//  QMMM
//  选择快递公司界面
//  Created by hanlu on 14-10-9.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SelectExpressCoBlock)(NSString *expressCo, NSString *expressName);

@interface QMSelectExpressCoViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView * tableView;

@property (nonatomic, copy) SelectExpressCoBlock selectBlock;

@end
