//
//  GoodsInputDescCell.m
//  QMMM
//
//  Created by kingnet  on 14-11-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsInputDescCell.h"
#import "NSString+Empty.h"
#import "UIPlaceHolderTextView.h"

@interface GoodsInputDescCell ()<UITextViewDelegate>{
    NSUInteger _maxLength;
}

@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *textView;

@end

@implementation GoodsInputDescCell
@synthesize desc = _desc;

+ (GoodsInputDescCell *)goodsInputDescCell
{
    
    return [[[NSBundle mainBundle]loadNibNamed:@"GoodsInputDescCell" owner:self options:0] lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    [self.textView setPlaceholderTextColor:kPlaceholderColor];
    self.textView.textColor = kDarkTextColor;
    self.textView.font = [UIFont systemFontOfSize:15.f];
    self.textView.enablesReturnKeyAutomatically = YES;
    self.textView.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight
{
    return 125.f;
}

-(void)setPlaceHolder:(NSString *)placeHolder andMaxLength:(NSUInteger)maxLength{
    self.textView.placeholder =placeHolder;
    _maxLength=maxLength;
}


- (NSString *)desc
{
    if ([NSString isEmptyWithSting:self.textView.text]) {
        return @"";
    }
    return self.textView.text;
}

- (void)setDesc:(NSString *)desc
{
    self.textView.text = desc;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *new = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if(new.length > _maxLength){
        if (![text isEqualToString:@""]) {
            return NO;
        }
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    NSString *toBeString = textView.text;
    NSString *lang = textView.textInputMode.primaryLanguage; // 键盘输入模式

    if ([lang isEqualToString:@"zh-Hans"]) { // 简体中文输入，包括简体拼音，健体五笔，简体手写
        
        UITextRange *selectedRange = [textView markedTextRange];
        
        //获取高亮部分
        
        UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
        
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        
        if (!position) {
            
            if (toBeString.length > _maxLength) {
                
                textView.text = [toBeString substringToIndex:_maxLength];
                [AppUtils showErrorMessage:[NSString stringWithFormat:@"最多可输入%ld个字", _maxLength]];
            }
        }
        
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        else{
            
        }
    }
    
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    
    else{
        
        if (toBeString.length > _maxLength) {
            
            textView.text = [toBeString substringToIndex:_maxLength];
            [AppUtils showErrorMessage:[NSString stringWithFormat:@"最多可输入%ld个字", _maxLength]];
        }
    }
}

@end
