//
//  GoodsDataHelper.m
//  QMMM
//
//  Created by 韩芦 on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsDataHelper.h"
#import "Constants.h"
#import "FMDatabaseAdditions.h"
#import "QMSqlite.h"
#import "GoodEntity.h"
#import "NSArray+JSONCategories.h"
#import "OrderDataInfo.h"
#import "NSString+JSONCategories.h"
#import "NSDictionary+JSONCategories.h"

@interface GoodsDataHelper () {
    NSThread * _workThread;
}

@end

@implementation GoodsDataHelper

- (void)setWorkThread:(NSThread *)aThread
{
    @synchronized(self) {
        if (_workThread != aThread) {
            _workThread = aThread;
        }
    }
}

- (NSString *)getGoodsTableName:(uint)type
{
    if(0 == type) {
        return @"qmRecommendGoods";
    } else {
        return @"qmFriendGoods";
    }
}

//获取精品列表
- (NSArray *)loadRecommendList:(FMDatabase *)database type:(uint)type count:(uint)count
{
    return [self loadRecommendList:database type:type fromId:0 count:count];
}

- (NSArray *)loadRecommendList:(FMDatabase *)database type:(uint)type fromId:(uint64_t)fromId count:(uint)count
{
    NSMutableArray * list = [[NSMutableArray alloc] initWithCapacity:1];
    
    if(database && count > 0) {
        
//        NSString * sql = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE id < ? ORDER BY id DESC LIMIT ?;", tableName];
//        resultSet = [database executeQuery:sql, @(fromId), @(count + 1)];
        
        @autoreleasepool {
            NSUInteger time = 0;
            if(fromId != 0) {
                NSString * sql = [NSString stringWithFormat:@"SELECT pub_time from %@ WHERE id = ?;", [self getGoodsTableName:type]];
                FMResultSet * resultSet = [database executeQuery:sql, @(fromId)];
                if([resultSet next]) {
                    time = (NSUInteger)[resultSet intForColumnIndex:0];
                }
            }
            
            int getCount = 0; //实际获取到的条数
            FMResultSet * resultSet = nil;
            if(time != 0) {
                 // count + 1 多取一条用来判断是否已经到结尾
                NSString * sql = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE pub_time <= ? AND id != ? ORDER BY pub_time DESC LIMIT ?;", [self getGoodsTableName:type]];
                resultSet = [database executeQuery:sql, @(time), @(fromId), @(count + 1)];
            } else {
                NSString * sql = [NSString stringWithFormat:@"SELECT * FROM %@ ORDER BY pub_time DESC LIMIT ?;", [self getGoodsTableName:type]];
                resultSet = [database executeQuery:sql, @(count + 1)];
            }
            
            while ([resultSet next]) {
                getCount++;
                if (getCount > count)
                    break;
                
                if([[NSThread currentThread] isCancelled]) {
                    getCount = count + 1;
                    break;
                }
                
                GoodEntity * goodInfo = [[GoodEntity alloc] initWithFMResultSet:resultSet];
                [list addObject:goodInfo];
            }
            
            // 如果没有获取到指定的数量，说明已经没有更多数据了，最后一项加null表示数据到头
            if (count > 0 && getCount <= count)
                [list addObject:[NSNull null]];
        }
    }
    
    return list;

}

- (void)updateRecommendList:(FMDatabase *)database type:(uint)type newList:(NSArray *)newRecommendList
{
    if(database && [newRecommendList count] > 0) {
        
        //clear all
        NSString * sql = [NSString stringWithFormat:@"DELETE FROM %@;", [self getGoodsTableName:type]];
        [database executeUpdate:sql];
        
        //add new list
        for (GoodEntity * data in newRecommendList) {
            
            NSString * pictures = [data.picturelist getSaveString];
            NSString * favors = [data.favorList getSaveString];
            
            NSString * sql = [NSString stringWithFormat:@"INSERT INTO %@(id, picture, desc, price, orig_price, cate_type, city, msgAudio, msgAudioSecond, pub_time, sellerUserId, sellerHeadIcon, sellerNick, favorlist, favorCounts, commentCounts, isFavor, isFriend, whoseFriend,friendId,num,freight,isSpecialGoods) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?);", [self getGoodsTableName:type]];
            [database executeUpdate:
             sql,
             @(data.gid),
             pictures,
             data.desc,
             @(data.price),
             @(data.orig_price),
             @(data.cate_type),
             data.city,
             data.msgAudio,
             @(data.msgAudioSeconds),
             @(data.pub_time),
             @(data.sellerUserId),
             data.sellerHeadIcon,
             data.sellerNick,
             favors,
             @(data.favorCounts),
             @(data.commentCounts),
             @(data.isFavor),
             @(data.is_friend),
             data.whose_friend,
             data.friendId,
             @(data.number),
             @(data.freight),
             @(data.isSpecialGoods)];

//            [database executeUpdateWithFormat:@"INSERT INTO qmRecommendGoods (id, picture, desc, price, orig_price, cate_type, city, msgAudio, msgAudioSecond, pub_time, sellerUserId, sellerHeadIcon, sellerNick, favorlist, favorCounts,commentCounts,isFavor) VALUES (%qu, %@, %@, %f, %f, %d, %@, %@, %u, %d, %qu, %@, %@, %@, %d, %d, %d);",
//                data.gid,
//                pictures,
//                data.desc,
//                data.price,
//                data.orig_price,
//                data.cate_type,
//                data.city,
//                data.msgAudio,
//                data.msgAudioSeconds,
//                data.pub_time,
//                data.sellerUserId,
//                data.sellerHeadIcon,
//                data.sellerNick,
//                favors,
//                data.favorCounts,
//                data.commentCounts,
//                data.isFavor];
        }
    }
}

- (void)updateRecommendGoodsForFavor:(FMDatabase *)database goodsInfo:(GoodEntity *)info
{
    if(database && info) {
        NSString * favors = [info.favorList getSaveString];
        NSString * sql = [NSString stringWithFormat:@"UPDATE qmRecommendGoods SET isFavor = ?, favorCounts = ?, favorlist = ? WHERE id = ?;"];
        NSString * sql2 = [NSString stringWithFormat:@"UPDATE qmFriendGoods SET isFavor = ?, favorCounts = ?, favorlist = ? WHERE id = ?;"];
        [database executeUpdate:sql, @(info.isFavor), @(info.favorCounts), favors, @(info.gid)];
        [database executeUpdate:sql2, @(info.isFavor), @(info.favorCounts), favors, @(info.gid)];
    }
}

- (void)updateGoodsListForComment:(FMDatabase *)database goodsInfo:(GoodEntity *)info
{
    if(database && info) {
        NSString * sql = [NSString stringWithFormat:@"UPDATE qmRecommendGoods SET commentCounts = ? WHERE id = ?;"];
        NSString * sql2 = [NSString stringWithFormat:@"UPDATE qmFriendGoods SET commentCounts = ? WHERE id = ?;"];
        [database executeUpdate:sql, @(info.commentCounts), @(info.gid)];
        [database executeUpdate:sql2, @(info.commentCounts), @(info.gid)];
    }
}

- (void)addRecommendList:(FMDatabase *)database type:(uint)type list:(NSArray *)dataList
{
    if(database && [dataList count] > 0) {
        NSArray * array = [NSArray arrayWithArray:dataList];
        
         //add new list
        for (GoodEntity * data in array) {
            
            NSString * pictures = [data.picturelist getSaveString];
            NSString * favors = [data.favorList getSaveString];
            
            NSString * sql = [NSString stringWithFormat:@"INSERT INTO %@(id, picture, desc, price, orig_price, cate_type, city, msgAudio, msgAudioSecond, pub_time, sellerUserId, sellerHeadIcon, sellerNick, favorlist, favorCounts, commentCounts, isFavor, isFriend, whoseFriend,friendId,num,fNum,usage,status,update_time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?, ?, ?, ?);", [self getGoodsTableName:type]];
            [database executeUpdate:
             sql,
             @(data.gid),
             pictures,
             data.desc,
             @(data.price),
             @(data.orig_price),
             @(data.cate_type),
             data.city,
             data.msgAudio,
             @(data.msgAudioSeconds),
             @(data.pub_time),
             @(data.sellerUserId),
             data.sellerHeadIcon,
             data.sellerNick,
             favors,
             @(data.favorCounts),
             @(data.commentCounts),
             @(data.isFavor),
             @(data.is_friend),
             data.whose_friend,
             data.friendId,
             @(data.number),
             @(data.fNum),
             @(data.isNew),
             @(data.status),
             @(data.update_time)];

//            [database executeUpdateWithFormat:@"INSERT INTO qmRecommendGoods (id, picture, desc, price, orig_price, cate_type, city, msgAudio, msgAudioSecond, pub_time, sellerUserId, sellerHeadIcon, sellerNick, favorlist, favorCounts,commentCounts,isFavor) VALUES (%qu, %@, %@, %f, %f, %d, %@, %@, %u, %d, %qu, %@, %@, %@, %d, %d, %d);",
//             data.gid,
//             pictures,
//             data.desc,
//             data.price,
//             data.orig_price,
//             data.cate_type,
//             data.city,
//             data.msgAudio,
//             data.msgAudioSeconds,
//             data.pub_time,
//             data.sellerUserId,
//             data.sellerHeadIcon,
//             data.sellerNick,
//             favors,
//             data.favorCounts,
//             data.commentCounts,
//             data.isFavor];
        }
    }
}

- (void)deleteGoodsById:(FMDatabase *)database type:(uint)type goodsId:(uint64_t)goodsId
{
    if(database) {
        NSString * sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE id = %qu;", [self getGoodsTableName:type], goodsId];
        [database executeUpdate:sql];
    }
}

- (void)deleteAllRecommendList:(FMDatabase *)database type:(uint)type
{
    if(database) {
        NSString * sql = [NSString stringWithFormat:@"DELETE FROM %@;", [self getGoodsTableName:type]];
        [database executeUpdate:sql];
    }
}

- (void)asyncLoadRecommendList:(contextBlock)context type:(uint)type count:(uint)count
{
    
    if(_workThread) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(type), @(count), bContext, nil];
        [self performSelector:@selector(threadLoadRecommendList:)
                     onThread:_workThread
                   withObject:array
                waitUntilDone:NO];
    }
    else {
        if (context) {
            context(nil);
        }
    }
}
- (void)threadLoadRecommendList:(NSArray *)params
{
    if(params && [params count] >= 2) {
        uint type = [[params objectAtIndex:0] unsignedIntValue];
        uint count = [[params objectAtIndex:1] unsignedIntValue];
        contextBlock context = nil;
        if([params count] > 2) {
            context = [params objectAtIndex:2];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        NSArray * result = [self loadRecommendList:helper.database type:type count:count];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(result);
            });
        }
    }
}

- (void)asyncLoadRecommendList:(contextBlock)context type:(uint)type fromId:(uint64_t)fromId count:(uint)count
{
    if(_workThread && count > 0) {
        __strong contextBlock bContext = context; //注意：
        NSArray * array = [NSArray arrayWithObjects:@(type), @(fromId), @(count), bContext, nil];
        [self performSelector:@selector(threadLoadRecommendListFromId:)
                     onThread:_workThread
                   withObject:array
                waitUntilDone:NO];
    } else {
        if(context) {
            context(nil);
        }
    }
}
- (void)threadLoadRecommendListFromId:(NSArray *)params
{
    if(params && [params count] >= 3) {
        uint type = [[params objectAtIndex:0] unsignedIntValue];
        uint64_t fromId = [[params objectAtIndex:1] unsignedLongLongValue];
        uint    count = [[params objectAtIndex:2] unsignedIntValue];
        contextBlock context = nil;
        if([params count] > 3) {
            context = (contextBlock)[params objectAtIndex:3];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        NSArray * result = [self loadRecommendList:helper.database type:type fromId:fromId count:count];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(result);
            });
        }
    }
}

- (void)asyncUpdateRecomendList:(contextBlock)context type:(uint)type newList:(NSArray*)newRecommendList
{
    if(_workThread && newRecommendList) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(type), newRecommendList, bContext, nil];
        [self performSelector:@selector(threadUpdateRecommendList:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadUpdateRecommendList:(NSArray *)params
{
    if([params count] >= 2) {
        uint type = [[params objectAtIndex:0] unsignedIntValue];
        NSArray * data = [params objectAtIndex:1];
        contextBlock context = nil;
        if([params count] > 2) {
            context = [params objectAtIndex:2];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self updateRecommendList:helper.database type:type newList:data];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}

- (void)asyncAddRecommendList:(contextBlock)context type:(uint)type list:(NSArray *)datalist
{
    if(_workThread && datalist) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(type), datalist, bContext, nil];
        [self performSelector:@selector(threadAddRecommendList:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadAddRecommendList:(NSArray *)params
{
    if([params count] >= 2) {
        uint type = [[params objectAtIndex:0] unsignedIntValue];
        NSArray * data = [params objectAtIndex:1];
        contextBlock context = nil;
        if([params count] > 2) {
            context = [params objectAtIndex:2];
        }

        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self addRecommendList:helper.database type:type list:data];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}

- (void)asyncDeleteGoodsById:(contextBlock)context type:(uint)type goodsId:(uint64_t)goodsId
{
    if(_workThread) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(type), @(goodsId), bContext, nil];
        [self performSelector:@selector(threadDeleteGoodsById:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadDeleteGoodsById:(NSArray *)params
{
    if([params count] >= 2) {
        uint type = [[params objectAtIndex:0] unsignedIntValue];
        uint64_t goodsId = [[params objectAtIndex:1] unsignedLongLongValue];
        contextBlock context = nil;
        if([params count] > 2) {
            context = [params objectAtIndex:2];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self deleteGoodsById:helper.database type:type goodsId:goodsId];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }

}

- (void)asyncDeleteAllRecommendList:(contextBlock)context type:(uint)type
{
    if(_workThread) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(type), bContext, nil];
        [self performSelector:@selector(threadDeleteAllRecommendList:) onThread:_workThread withObject:array waitUntilDone:NO];
    }

}
- (void)threadDeleteAllRecommendList:(NSArray *)params
{
    if([params count] >= 1) {
        uint type = [[params objectAtIndex:0] unsignedIntValue];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self deleteAllRecommendList:helper.database type:type];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}

- (void)asyncUpdateRecommendGoodsForFavor:(contextBlock)context goodsInfo:(GoodEntity *)info
{
    if(_workThread) {
        __strong contextBlock  bContext = context;
        NSArray * array = [NSArray arrayWithObjects:info, bContext, nil];
        [self performSelector:@selector(threadUpdateRecommendListByFavor:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadUpdateRecommendListByFavor:(NSArray *)params
{
    if(params && [params count] >= 1) {
        GoodEntity * info = [params objectAtIndex:0];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:2];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self updateRecommendGoodsForFavor:helper.database goodsInfo:info];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}

- (void)asyncUpdateGoodsForComment:(contextBlock)context goodsInfo:(GoodEntity *)info
{
    if (_workThread) {
        __strong contextBlock  bContext = context;
        NSArray * array = [NSArray arrayWithObjects:info, bContext, nil];
        [self performSelector:@selector(threadUpdateGoodsListByComment:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}

- (void)threadUpdateGoodsListByComment:(NSArray *)params
{
    if(params && [params count] >= 1) {
        GoodEntity * info = [params objectAtIndex:0];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:2];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self updateGoodsListForComment:helper.database goodsInfo:info];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSString *)getTableName:(EControllerType)type
{
    NSString * tableName = nil;
    if(type == ECT_ORDER_MGR) {
        tableName = @"qmOrderList";
    } else if(type == ECT_BUYED_GOODS) {
        tableName = @"qmBoughtGoodsList";
    } else if(type == ECT_GOODS_MGR) {
        tableName = @"qmPubGoodsList";
    } else if(type == ECT_FAVOR_GOODS) {
        tableName = @"qmFavorList";
    }
    return tableName;
}

- (NSArray *)loadMylist:(FMDatabase *)database type:(EControllerType)type fromId:(NSString *)fromId count:(uint)count
{
    NSMutableArray * list = [[NSMutableArray alloc] initWithCapacity:1];
    
    if(database && count > 0) {
        
        @autoreleasepool {
            
            NSString * tableName = [self getTableName:type];
            
            int getCount = 0; //实际获取到的条数
            FMResultSet * resultSet = nil;
            if([fromId intValue]!= 0) {
                // count + 1 多取一条用来判断是否已经到结尾
                NSString * sql = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE id < ? ORDER BY id DESC LIMIT ?;", tableName];
                resultSet = [database executeQuery:sql, fromId, @(count + 1)];
            } else {
                NSString * sql = [NSString stringWithFormat:@"SELECT * FROM %@ ORDER BY id DESC LIMIT ?;", tableName];
                resultSet = [database executeQuery:sql, @(count + 1)];
            }
            
            while ([resultSet next]) {
                getCount++;
                if (getCount > count)
                    break;
                
                if([[NSThread currentThread] isCancelled]) {
                    getCount = count + 1;
                    break;
                }
                
                NSString * data = [resultSet stringForColumnIndex:1];
                NSDictionary * dict = [data toDict];
                if(type == ECT_ORDER_MGR || type == ECT_BUYED_GOODS) {
                    OrderSimpleInfo * info = [[OrderSimpleInfo alloc] initWithDictionary:dict];
                    [list addObject:info];
                } else {
                    GoodsSimpleInfo * info = [[GoodsSimpleInfo alloc] initWithDictionary:dict];
                    [list addObject:info];
                }
            }
            
            // 如果没有获取到指定的数量，说明已经没有更多数据了，最后一项加null表示数据到头
            if (count > 0 && getCount <= count)
                [list addObject:[NSNull null]];
        }
    }
    
    return list;
}

//- (NSArray *)loadGoodslist:(FMDatabase *)database timeStamp:(int)timestamp count:(uint)count
//{
//    NSMutableArray * list = [[NSMutableArray alloc] initWithCapacity:1];
//    
//    if(database && count > 0) {
//        
//        @autoreleasepool {
//            
//            int getCount = 0; //实际获取到的条数
//            FMResultSet * resultSet = nil;
//            if(timestamp != 0) {
//                // count + 1 多取一条用来判断是否已经到结尾
//                NSString * sql = @"SELECT * FROM qmPubGoodsList WHERE updatetime < ? ORDER BY updatetime DESC LIMIT ?;";
//                resultSet = [database executeQuery:sql, @(timestamp), @(count + 1)];
//            } else {
//                NSString * sql = @"SELECT * FROM qmPubGoodsList ORDER BY updatetime DESC LIMIT ?;";
//                resultSet = [database executeQuery:sql, @(count + 1)];
//            }
//            
//            while ([resultSet next]) {
//                getCount++;
//                if (getCount > count)
//                    break;
//                
//                if([[NSThread currentThread] isCancelled]) {
//                    getCount = count + 1;
//                    break;
//                }
//                
//                NSString * data = [resultSet stringForColumnIndex:2];
//                NSDictionary * dict = [data toDict];
//                GoodsSimpleInfo * info = [[GoodsSimpleInfo alloc] initWithDictionary:dict];
//                [list addObject:info];
//            }
//            
//            // 如果没有获取到指定的数量，说明已经没有更多数据了，最后一项加null表示数据到头
//            if (count > 0 && getCount <= count)
//                [list addObject:[NSNull null]];
//        }
//    }
//    
//    return list;
//}

- (void)addMyList:(FMDatabase *)database type:(EControllerType)type list:(NSArray *)dataList
{
    if(database && [dataList count] > 0) {
        
        NSArray * array = [NSArray arrayWithArray:dataList];
        NSString * tableName = [self getTableName:type];
        
        for (NSDictionary * dict in array) {
            id temp = nil;
            if(type ==  ECT_ORDER_MGR || type == ECT_BUYED_GOODS) {
                temp = [dict objectForKey:@"order_id"];
                
                NSString * json = [dict toString];
                NSString * sql = [NSString stringWithFormat:@"INSERT INTO %@ (id, data) VALUES(?, ?);", tableName];
                [database executeUpdate:sql, (NSString *)temp, json];
            } else {
                temp = [dict objectForKey:@"id"];
                uint64_t uuid = 0;
                if([temp isKindOfClass:[NSString class]]) {
                    uuid = (uint64_t)[temp longLongValue];
                } else if([temp isKindOfClass:[NSNumber class]]) {
                    uuid = [temp unsignedLongLongValue];
                }
                
                NSString * json = [dict toString];
                NSString * sql = [NSString stringWithFormat:@"INSERT INTO %@ (id, data) VALUES(?, ?);", tableName];
                [database executeUpdate:sql, @(uuid), json];
            }
        }
    }
}

- (void)updateGoodsSimpleInfo:(FMDatabase *)database type:(EControllerType)type dict:(NSDictionary *)dict
{
    if(database && dict) {
        if(type == ECT_GOODS_MGR || type == ECT_FAVOR_GOODS) {
            NSString * tableName = [self getTableName:type];
            NSString * sql = [NSString stringWithFormat:@"REPLACE INTO %@ (id, data) VALUES(?, ?);", tableName];
            [database executeUpdate:sql, [dict objectForKey:@"id"], [dict toString]];
        }
    }
}

- (void)updateOrderStatus:(FMDatabase *)database bSeller:(BOOL)bSeller orderId:(NSString *)orderId status:(uint)status
{
    if(database && orderId) {
        NSString * tableName = nil;
        if(bSeller) {
            tableName = @"qmOrderList";
        } else {
            tableName = @"qmBoughtGoodsList";
        }
        
        NSString * sql = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE id = %@;", tableName, orderId];
        FMResultSet * resultSet = [database executeQuery:sql];
        if([resultSet next]) {
            uint64_t dataId = [resultSet unsignedLongLongIntForColumnIndex:0];
            NSString * data = [resultSet stringForColumnIndex:1];
            NSDictionary * dict = [data toDict];
            OrderSimpleInfo * info = [[OrderSimpleInfo alloc] initWithDictionary:dict];
            info.order_status = status;
            
            NSString * newString = [info toJSONString];
            
            sql = [NSString stringWithFormat:@"UPDATE %@ SET data = ? WHERE id = ?;", tableName];
            [database executeUpdate:sql, newString, @(dataId)];
        }
    }
}

- (void)updateOrderPrice:(FMDatabase *)database bSeller:(BOOL)bSeller orderId:(NSString *)orderId price:(float)price
{
    if(database && orderId) {
        NSString * tableName = nil;
        if(bSeller) {
            tableName = @"qmOrderList";
        } else {
            tableName = @"qmBoughtGoodsList";
        }
        
        NSString * sql = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE id = %@;", tableName, orderId];
        FMResultSet * resultSet = [database executeQuery:sql];
        if([resultSet next]) {
            uint64_t dataId = [resultSet unsignedLongLongIntForColumnIndex:0];
            NSString * data = [resultSet stringForColumnIndex:1];
            NSDictionary * dict = [data toDict];
            OrderSimpleInfo * info = [[OrderSimpleInfo alloc] initWithDictionary:dict];
            info.payAmmount = price;
            
            NSString * newString = [info toJSONString];
            
            sql = [NSString stringWithFormat:@"UPDATE %@ SET data = ? WHERE id = ?;", tableName];
            [database executeUpdate:sql, newString, @(dataId)];
        }
    }
}

- (void)deleteMyList:(FMDatabase *)database type:(EControllerType)type uuId:(uint64_t)uuId;
{
    if(database) {
        NSString * tableName = [self getTableName:type];
        NSString * sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE id = ?;", tableName];
        [database executeUpdate:sql, @(uuId)];
    }
}

- (void)deleteAllMyList:(FMDatabase *)database type:(EControllerType)type
{
    if(database) {
        NSString * tableName = [self getTableName:type];
        NSString * sql = [NSString stringWithFormat:@"DELETE FROM %@;", tableName];
        [database executeUpdate:sql];
    }
}

- (void)asyncLoadMyList:(contextBlock)context type:(EControllerType)type fromId:(NSString *)fromId count:(uint)count
{
    if(_workThread && count > 0) {
        __strong contextBlock bContext = context; //注意：
        NSArray * array = [NSArray arrayWithObjects:@(type), fromId, @(count), bContext, nil];
        [self performSelector:@selector(threadLoadMyListFromId:)
                     onThread:_workThread
                   withObject:array
                waitUntilDone:NO];
    } else {
        if(context) {
            context(nil);
        }
    }
}
- (void)threadLoadMyListFromId:(NSArray *)params
{
    if(params && [params count] >=3) {
        EControllerType type = [[params objectAtIndex:0] intValue];
        NSString * fromId = [params objectAtIndex:1];
        uint    count = [[params objectAtIndex:2] unsignedIntValue];
        contextBlock context = nil;
        if([params count] > 3) {
            context = (contextBlock)[params objectAtIndex:3];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        NSArray * result = [self loadMylist:helper.database type:type fromId:fromId count:count];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(result);
            });
        }
    }
}


- (void)asyncAddMyList:(contextBlock)context type:(EControllerType)type list:(NSArray *)datalist
{
    if(_workThread && datalist) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(type), datalist, bContext, nil];
        [self performSelector:@selector(threadAddMyList:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadAddMyList:(NSArray *)params
{
    if(params && [params count] >=2) {
        EControllerType type = [[params objectAtIndex:0] intValue];
        NSArray * array = [params objectAtIndex:1];
        contextBlock context = nil;
        if([params count] > 2) {
            context = (contextBlock)[params objectAtIndex:2];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self addMyList:helper.database type:type list:array];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}

- (void)asyncupdateGoodsSimpleInfo:(contextBlock)context type:(EControllerType)type dict:(NSDictionary *)dict
{
    if(_workThread && dict) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(type), dict, bContext, nil];
        [self performSelector:@selector(threadUpdateGoodsInfo:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadUpdateGoodsInfo:(NSArray *)params
{
    if(params && [params count] >=2) {
        EControllerType type = [[params objectAtIndex:0] intValue];
        NSDictionary * dict = [params objectAtIndex:1];
        contextBlock context = nil;
        if([params count] > 2) {
            context = (contextBlock)[params objectAtIndex:2];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self updateGoodsSimpleInfo:helper.database type:type dict:dict];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}

- (void)asyncUpdateOrderStatus:(contextBlock)context bSeller:(BOOL)bSeller orderId:(NSString *)orderId status:(uint)status
{
    if(_workThread && orderId) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(bSeller), orderId, @(status), bContext, nil];
        [self performSelector:@selector(threadUpdateOrderStatus:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadUpdateOrderStatus:(NSArray *)params
{
    if(params && [params count] >=3) {
        BOOL bSeller = [[params objectAtIndex:0] boolValue];
        NSString * orderId = [params objectAtIndex:1];
        uint status = [[params objectAtIndex:2] unsignedIntValue];
        contextBlock context = nil;
        if([params count] > 3) {
            context = (contextBlock)[params objectAtIndex:3];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self updateOrderStatus:helper.database bSeller:bSeller orderId:orderId status:status];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}

- (void)asyncUpdateOrderPrice:(contextBlock)context bSeller:(BOOL)bSeller orderId:(NSString *)orderId payAmount:(float)payAmount
{
    if(_workThread && orderId) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(bSeller), orderId, @(payAmount), bContext, nil];
        [self performSelector:@selector(threadUpdateOrderPrice:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}

- (void)threadUpdateOrderPrice:(NSArray *)params
{
    if(params && [params count] >=3) {
        BOOL bSeller = [[params objectAtIndex:0] boolValue];
        NSString * orderId = [params objectAtIndex:1];
        float price = [[params objectAtIndex:2] floatValue];
        contextBlock context = nil;
        if([params count] > 3) {
            context = (contextBlock)[params objectAtIndex:3];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self updateOrderPrice:helper.database bSeller:bSeller orderId:orderId price:price];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}


- (void)asyncDeleteMyList:(contextBlock)context type:(EControllerType)type uuId:(uint64_t)uuId
{
    if(_workThread) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(type), @(uuId), bContext, nil];
        [self performSelector:@selector(threadDeleteMyList:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadDeleteMyList:(NSArray *)params
{
    if(params && [params count] >= 2) {
        EControllerType type = [[params objectAtIndex:0] intValue];
        uint64_t uuid = [[params objectAtIndex:1] unsignedLongLongValue];
        contextBlock context = nil;
        if([params count] > 2) {
            context = (contextBlock)[params objectAtIndex:2];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self deleteMyList:helper.database type:type uuId:uuid];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
        
    }
}

- (void)asyncDeleteAllMyList:(contextBlock)context type:(EControllerType)type
{
    if(_workThread) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(type), bContext, nil];
        [self performSelector:@selector(threadDeleteAllMyList:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadDeleteAllMyList:(NSArray *)params
{
    if(params && [params count] > 0) {
        EControllerType type = [[params objectAtIndex:0] intValue];
        contextBlock context = nil;
        if([params count] > 1) {
            context = (contextBlock)[params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self deleteAllMyList:helper.database type:type];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }

    }
}

@end
