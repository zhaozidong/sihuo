//
//  ZoomImgItem.h
//  ShowImgDome
//
//  Created by chuliangliang on 14-9-28.
//  Copyright (c) 2014年 aikaola. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KL_ImageZoomView.h"

@interface ZoomImgItem : UITableViewCell
{
    KL_ImageZoomView *imageView;
}

//@property (nonatomic, retain)NSString *imgName;
@property (nonatomic, assign)CGSize size;
//@property (nonatomic, strong)NSString *smallImgName;
@property (nonatomic, assign)BOOL isLookLifePhoto; //是否是查看生活照

- (void)setImgName:(NSString *)imgName smallImgName:(NSString *)smallImgName;

@end
