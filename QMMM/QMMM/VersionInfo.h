//
//  VersionInfo.h
//  QMMM
//
//  Created by kingnet  on 14-10-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VersionInfo : NSObject

//最新版本号
@property (nonatomic, strong) NSString *version;

//版本说明
@property (nonatomic, strong) NSString *desc;

//iTunes下载地址
@property (nonatomic, strong) NSString *downloadUrl;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

+ (BOOL)isHaveNewVer:(NSString *)ver;

+ (NSString *)currentVer;

@end
