//
//  UploadProgress.h
//  QMMM
//  上传图片时显示的黑色透明进度条
//  Created by kingnet  on 15-1-20.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploadProgress : UIView

@property (nonatomic, assign) float value; //from 0.0 to 1.0

@property (nonatomic, assign) float cornerRadius; //round corner radius

@property (nonatomic, assign) BOOL isFailed; //是否上传失败

@end
