//
//  ExchangeSuccessViewController.m
//  QMMM
//
//  Created by kingnet  on 15-3-27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "ExchangeSuccessViewController.h"
#import "ExchangeSuccessView.h"
#import "FinanceHandler.h"
#import "FinanceDataInfo.h"
#import "ActivityDetailViewController.h"
#import "BaseHandler.h"
#import "APIConfig.h"

@interface ExchangeSuccessViewController ()<ExchangeSuccessViewDelegate>

@property (nonatomic, strong) ExchangeSuccessView *successView;

@end

@implementation ExchangeSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"兑换成功";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    [self.view addSubview:self.successView];
    
    FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
    self.successView.hongbaoMoney = financeInfo.hongbao;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]postNotificationName:kReloadHtmlNotify object:nil];
}

- (ExchangeSuccessView *)successView
{
    if (!_successView) {
        _successView = [ExchangeSuccessView successView];
        _successView.frame = CGRectMake(0, kMainTopHeight, CGRectGetWidth(self.view.frame), [ExchangeSuccessView viewHeight]);
        _successView.delegate = self;
    }
    return _successView;
}

#pragma mark - ExchangeSuccessViewDelegate
- (void)didTapExchange
{
    //跳转到红包商品界面
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didTapInvite
{
    //添加统计点
    [AppUtils trackCustomEvent:@"click_inviteFriendsForHongBao_event" args:nil];
    
    //跳转到h5的邀请好友界面
    NSString *requestURL = [BaseHandler requestUrlWithPath:API_HONGBAO_INVITE];
    NSMutableArray *mbArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [mbArray removeLastObject];
    ActivityDetailViewController *controller = [[ActivityDetailViewController alloc]initWithUrl:requestURL];
    [mbArray addObject:controller];
    [self.navigationController setViewControllers:mbArray animated:YES];
}

@end
