//
//  GoodsChatHistoryDao.m
//  QMMM
//
//  Created by kingnet  on 15-2-5.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "GoodsChatHistoryDao.h"

@implementation GoodsChatHistoryDao

+ (GoodsChatHistoryDao *)sharedInstance
{
    static GoodsChatHistoryDao *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (BOOL)insertGoodsId:(uint64_t)goodsId
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO %@ (goodsId) VALUES (?)", kGoodsChatHistory];
    return [db executeUpdate:insertSQL, [NSNumber numberWithUnsignedLongLong:goodsId]];
}

- (BOOL)isHaveGoodsId:(uint64_t)goodsId
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE goodsId = ?", kGoodsChatHistory];
    FMResultSet *rs = [db executeQuery:querySQL, [NSNumber numberWithUnsignedLongLong:goodsId]];
    while ([rs next]) {
        return YES;
    }
    return NO;
}

@end
