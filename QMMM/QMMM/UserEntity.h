//
//  UserEntity.h
//  QMMM
//
//  Created by kingnet  on 14-8-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseEntity.h"

@interface UserEntity : BaseEntity

@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *nickname; //昵称
@property (nonatomic, copy) NSString *avatar; //头像
@property (nonatomic, copy) NSString *motto; //个人签名
@property (nonatomic, copy) NSString *sex; //性别
@property (nonatomic, copy) NSString *mobile; //手机号码
@property (nonatomic, copy) NSString *location;//地理坐标，用逗号拼接经纬度
@property (nonatomic, assign) int reg_ts; //注册时间
@property (nonatomic, assign) int login_ts; //登录时间
@property (nonatomic, assign) int update_ts; //资料最近更新时间
@property (nonatomic, copy) NSString *life_photo; //用户的生活照
@property (nonatomic, copy) NSString *email;  //邮箱

- (NSArray *)lifePhotos;

- (void)lifePhotoStr:(NSArray *)array;

- (id)initWithUserEntity:(UserEntity *)entity;

- (id)initWithDictionary:(NSDictionary *)dictionary;

- (NSDictionary *)toDictionary;

- (BOOL)isFirstLogin;

@end
