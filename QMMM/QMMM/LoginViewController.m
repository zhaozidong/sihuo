//
//  LoginViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "LoginViewController.h"
#import "LRInputViewController.h"
#import "UserInfoHandler.h"
#import "AppDelegate.h"
#import "FindPwdViewController.h"

@interface LoginViewController ()<LRInputViewControllerDelegate>

@property (nonatomic, strong) LRInputViewController *inputController;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"登录";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    self.canTapCloseKeyboard = YES;
    
    self.inputController = [[LRInputViewController alloc]init];
    self.inputController.submitBtnTitle = @"登录";
    self.inputController.delegate = self;
    self.inputController.returnKeyType = UIReturnKeyDone;
    self.inputController.isLoginView = YES;
    
    [self addChildViewController:self.inputController];
    
    [[self view] addSubview:[self.inputController view]];
    
    [self.inputController didMoveToParentViewController:self];
    
    UIButton *forgetPwdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    forgetPwdBtn.backgroundColor = [UIColor clearColor];
    [forgetPwdBtn setTitle:@"忘记密码啦？" forState:UIControlStateNormal];
    forgetPwdBtn.titleLabel.font = [UIFont systemFontOfSize:15.f];
    [forgetPwdBtn setTitleColor:kBlueColor forState:UIControlStateNormal];
    forgetPwdBtn.frame = CGRectMake((CGRectGetWidth(self.view.frame)-100)/2, CGRectGetMaxY(self.inputController.view.frame)-10, 100, 30);
    [forgetPwdBtn addTarget:self action:@selector(forgetPwdAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:forgetPwdBtn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickSubmitBtn:(NSString *)phoneNum pwd:(NSString *)pwd
{
    //统计点击登录按钮次数
    [AppUtils trackCustomEvent:@"login_press_finishBtn" args:nil];
    
    [AppUtils showProgressMessage:[AppUtils localizedPersonString:@"QM_HUD_Loading"]];
    
    [[UserInfoHandler sharedInstance] executeLoginWithUserName:phoneNum password:pwd success:^(id obj) {
        [AppUtils dismissHUD];
        //统计由登录进入到app中的次数
        [AppUtils trackCustomEvent:@"login_startApp" args:nil];
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate startApp];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        
    }];
}

- (void)forgetPwdAction:(id)sender
{
    FindPwdViewController *controller = [[FindPwdViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerObserver];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeObserver];
}

- (void)registerObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processKeyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)removeObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [nc removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [nc removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)processKeyboardWillHide:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    frame.size.height = kMainFrameHeight;
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.view.frame = frame;
    }];
}

- (void)processKeyboardWillShow:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
 
    CGRect frame = self.view.frame;
    CGRect bounds = [UIScreen mainScreen].bounds;
    if (CGRectGetHeight(bounds)> 480) {
        frame.origin.y = -107;
    }
    else{
        frame.origin.y = -117;
    }
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.view.frame = frame;
    }];
}

@end
