//
//  QMReportViewController.h
//  QMMM
//
//  Created by Derek on 15/3/10.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportInfo.h"
#import "BaseViewController.h"


@interface QMReportViewController : BaseViewController


-(id)initWithUserId:(NSString *)userId;

-(id)initWithGoodsId:(NSString *)goodsId andUserId:(NSString *)userId;

@end
