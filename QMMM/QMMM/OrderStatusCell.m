//
//  OrderStatusCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderStatusCell.h"
#import "OrderDataInfo.h"

NSString * const kOrderStatusCellIdentifier = @"kOrderStatusCellIdentifier";

@interface OrderStatusCell ()
{
    NSString * orderId_;
}
@property (weak, nonatomic) IBOutlet UILabel *statusLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLbl;
@property (weak, nonatomic) IBOutlet UIButton *actionBtn; //付款/发货/确认收货
@property (weak, nonatomic) IBOutlet UIButton *checkExpressBtn; //查看物流
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statusLblCst;

@end

@implementation OrderStatusCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"OrderStatusCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.statusLbl.hidden = YES;
    self.actionBtn.hidden = YES;
    self.checkExpressBtn.hidden = YES;
    self.totalPriceLbl.textColor = kRedColor;
    self.statusLbl.textColor = kDarkTextColor;
    [self.actionBtn setTitleColor:kRedColor forState:UIControlStateNormal];
    [self.checkExpressBtn setTitleColor:kGrayTextColor forState:UIControlStateNormal];
    UIImage * image = [UIImage imageNamed:@"border_btn_bkg"];
    [self.actionBtn setBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(10, 7, 10, 7)] forState:UIControlStateNormal];
    [self.actionBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIImage * image1 = [UIImage imageNamed:@"gray_border_btn_bkg"];
    [self.checkExpressBtn setBackgroundImage:[image1 resizableImageWithCapInsets:UIEdgeInsetsMake(10, 7, 10, 7)] forState:UIControlStateNormal];
    [self.checkExpressBtn addTarget:self action:@selector(checkExpressAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(OrderSimpleInfo *)orderInfo cellType:(EControllerType)cellType
{
    self.totalPriceLbl.text = [NSString stringWithFormat:@"¥ %.2f", orderInfo.payAmmount];
    self.statusLbl.hidden = YES;
    self.actionBtn.hidden = YES;
    self.checkExpressBtn.hidden = YES;
    self.statusLblCst.constant = 15.f;
    [self.contentView layoutIfNeeded];
    orderId_ = orderInfo.order_id;
    
    if (orderInfo.order_status == QMOrderStatusRefundClosed) {
        self.statusLbl.hidden = NO;
        self.statusLbl.text = @"交易已关闭";
    }
    else if (orderInfo.order_status == QMOrderStatusTimeout){
        self.statusLbl.hidden = NO;
        self.statusLbl.text = @"交易已关闭";
    }
    else if (orderInfo.order_status == QMOrderStatusClosed) {
        self.statusLbl.hidden = NO;
        self.statusLbl.text = @"交易已关闭";
    }
    else if (orderInfo.order_status == QMOrderStatusFinished){
        self.statusLbl.hidden = NO;
        self.statusLbl.text = @"交易完成";
    }
    else if (orderInfo.order_status == QMOrderStatusRefundApply){
        self.statusLbl.hidden = NO;
        self.statusLbl.text = @"退款中";
    }
    else if (orderInfo.order_status == QMOrderStatusRefundReject){
        self.statusLbl.hidden = NO;
        self.statusLbl.text = @"退款中";
    }
    else if (orderInfo.order_status == QMOrderStatusRefundAccept){
        self.statusLbl.hidden = NO;
        self.statusLbl.text = @"交易已关闭";
    }
    else{
        if (cellType == ECT_BUYED_GOODS) { //已买的
            if (orderInfo.order_status == QMOrderStatusWaitPay){
                self.actionBtn.hidden = NO;
                [self.actionBtn setTitle:@"付款" forState:UIControlStateNormal];
                self.statusLbl.hidden = NO;
                self.statusLbl.text = @"待付款";
                self.statusLblCst.constant = 90.f;
                [self.contentView layoutIfNeeded];
            }
            else if (orderInfo.order_status == QMOrderStatusPaid){
                self.statusLbl.hidden = NO;
                self.statusLbl.text = @"已付款";
            }
            else if (orderInfo.order_status == QMOrderStatusDelivery){
                self.actionBtn.hidden = NO;
                [self.actionBtn setTitle:@"确认收货" forState:UIControlStateNormal];
//                self.checkExpressBtn.hidden = NO;
                self.statusLbl.hidden = NO;
                self.statusLbl.text = @"已发货";
                self.statusLblCst.constant = 90.f;
                [self.contentView layoutIfNeeded];
            }
        }
        else{ //已卖的
            if (orderInfo.order_status == QMOrderStatusWaitPay){
                self.statusLbl.hidden = NO;
                self.statusLbl.text = @"待付款";
            }
            else if (orderInfo.order_status == QMOrderStatusPaid){
                self.actionBtn.hidden = NO;
                [self.actionBtn setTitle:@"发货" forState:UIControlStateNormal];
                self.statusLbl.hidden = NO;
                self.statusLbl.text = @"已付款";
                self.statusLblCst.constant = 90.f;
                [self.contentView layoutIfNeeded];
            }
            else if (orderInfo.order_status == QMOrderStatusDelivery){
                self.statusLbl.hidden = NO;
                self.statusLbl.text = @"已发货";
            }
        }
    }
}

+ (CGFloat)cellHeight
{
    return 45.f;
}

- (void)buttonAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapButton:)]) {
        [self.delegate didTapButton:orderId_];
    }
}

- (void)checkExpressAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapCheckExpress:)]) {
        [self.delegate didTapCheckExpress:orderId_];
    }
}

@end
