//
//  BuyGoodsViewController.m
//  QMMM
//
//  Created by kingnet  on 14-9-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BuyGoodsViewController.h"
#import "OrderGoodsInfoCell.h"
#import "OrderInfoControl.h"
#import "GoodEntity.h"
#import "MyAddressViewController.h"
#import "AddressEntity.h"
#import "AddressHandler.h"
#import "GoodsHandler.h"
#import "FinanceHandler.h"
#import "FinanceDataInfo.h"
#import "EditAddressViewController.h"
#import "GoodsHandler.h"
#import "QMOrderSuccessViewController.h"
#import "QMOrderDetailViewController.h"

@interface BuyGoodsViewController ()<UITableViewDataSource, UITableViewDelegate, OrderInfoControlDelegate>
{
    uint64_t gid_;
}
@property (nonatomic)UITableView *tableView;
@property (nonatomic)OrderGoodsInfoCell *goodsInfoCell;
@property (nonatomic, strong) OrderInfoControl *infoControl;
@property (nonatomic, strong) GoodEntity *goodsEntity;
@property (nonatomic, strong) AddressHandler *addressHandler;
@property (nonatomic, strong) AddressEntity *addressEntity;
@property (nonatomic, strong) UIButton *submitBtn; //提交的按钮，上面的文字要动态改变

@end

@implementation BuyGoodsViewController

- (instancetype)initWithGoodsID:(uint64_t)gid
{
    if (self = [super init]) {
        gid_ = gid;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"立即购买";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    self.addressHandler = [[AddressHandler alloc]init];
    self.infoControl = [[OrderInfoControl alloc]init];
    self.infoControl.delegate = self;
    
    [self getGoodsDetails];
    [self getAddress];
    
    [self.view addSubview:self.tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)getGoodsDetails
{
    [AppUtils showProgressMessage:@"加载中"];
    
    __weak __typeof(self)weakSelf = self;
    [[GoodsHandler shareGoodsHandler]getGoodsInfoWithId:gid_ success:^(id obj) {
        [AppUtils dismissHUD];
        if (obj) {
            weakSelf.goodsEntity = [[GoodEntity alloc]init];
            [weakSelf.goodsEntity assignValue:(GoodEntity *)obj];
            weakSelf.infoControl.goodsEntity = (GoodEntity *)obj;
            [weakSelf.goodsInfoCell updateUI:(GoodEntity *)obj];
            [weakSelf getFinance];
        }
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)getAddress
{
    BuyGoodsViewController __weak * weakSelf = self;
    [self.addressHandler getAddressList:^(id obj) {
        NSArray *array = (NSArray *)obj;
        for (AddressEntity *entity in array) {
            if (entity.isDefault) {
                [weakSelf reloadAddress:entity];
                break;
            }
        }
    } failed:^(id obj) {
        
    }];
}

- (void)getFinance
{
    [[FinanceHandler sharedInstance]freeFinanceInfo];
    [[FinanceHandler sharedInstance]getFinanceInfo:^(id obj) {
        
    } failed:^(id obj) {
        
    }];
}

- (void)reloadAddress:(AddressEntity *)entity
{
    self.addressEntity = entity;
    [self.infoControl setAddressEntity:self.addressEntity];
}

- (void)submitAction:(id)sender
{
    if (!self.addressEntity || self.addressEntity.addressId <= 0) {
        [AppUtils showAlertMessage:@"请添加收货地址"];
        //跳到编辑收货地址的页面
        __weak typeof(self) weakSelf = self;
        EditAddressViewController *controller = [[EditAddressViewController alloc]initWithBlock:^(AddressEntity *entity) {
            [weakSelf reloadAddress:entity];
        }];
        [self.navigationController pushViewController:controller animated:YES];
        return;
    }
    
    FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
    float remainPay = 0.f;
    if (financeInfo.amount >= [self.infoControl actualPay]) { //余额够用
        remainPay = [self.infoControl actualPay];
    }
    else{
        remainPay = financeInfo.amount;
    }

    [AppUtils showProgressMessage:@"正在提交"];
    __weak __typeof(self)weakSelf = self;
    [[GoodsHandler shareGoodsHandler]submitOrder:self.addressEntity.addressId goodsId:self.goodsEntity.gid paymentType:0 num:self.infoControl.goodsNum remainPay:remainPay success:^(id obj) {
        [AppUtils dismissHUD];
        //跳到订单界面
        NSDictionary *dataDic = (NSDictionary *)obj;
        NSString *orderId = [dataDic objectForKey:@"id"];
        
        NSMutableArray *mbArray = [NSMutableArray arrayWithArray:weakSelf.navigationController.viewControllers];
        int count = (int)[weakSelf.navigationController.viewControllers count];
        [mbArray removeObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(count-2, 2)]];
        
        QMOrderDetailViewController *controller = [[QMOrderDetailViewController alloc]initWithData:NO orderId:orderId];
        [mbArray addObject:controller];
        
        [weakSelf.navigationController setViewControllers:mbArray animated:YES];
        controller = nil;
        [mbArray removeAllObjects];
        mbArray = nil;
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

#pragma mark - Getter
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
        
        //footer
        UIView *footView = [[[NSBundle mainBundle]loadNibNamed:@"WithdrawFootView" owner:nil options:0] lastObject];
        footView.frame = CGRectMake(0, 0, CGRectGetWidth(_tableView.frame), 80.f);
        _tableView.tableFooterView = footView;
        for (UIButton *button in footView.subviews) {
            if ([button isKindOfClass:[UIButton class]]) {
                [button setTitle:@"提交订单" forState:UIControlStateNormal];
                [button addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
                self.submitBtn = button;
                break;
            }
        }
    }
    return _tableView;
}

- (OrderGoodsInfoCell *)goodsInfoCell
{
    if (!_goodsInfoCell) {
        _goodsInfoCell = [OrderGoodsInfoCell goodsInfoCell];
    }
    return _goodsInfoCell;
}

#pragma mark - UITableViewDatasource  
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [OrderInfoControl rowCount]+1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return [OrderGoodsInfoCell cellHeight];
    }
    return [OrderInfoControl cellHeightWithIndex:indexPath];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return self.goodsInfoCell;
    }
    else{
        return self.infoControl.cellsArray[indexPath.row-1];
    }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 3) {
        cell.backgroundColor = kGrayCellColor;
    }
    else{
        cell.backgroundColor = [UIColor whiteColor];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }

    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 2) {
        BuyGoodsViewController __weak * weakSelf = self;
        if (!self.addressEntity) {
            EditAddressViewController *controller = [[EditAddressViewController alloc]initWithBlock:^(AddressEntity *entity) {
                [weakSelf reloadAddress:entity];
            }];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else{
            MyAddressViewController *controller = [[MyAddressViewController alloc]initWithAddressId:self.addressEntity.addressId addressBlock:^(AddressEntity *entity) {
                [weakSelf reloadAddress:entity];
            }];
            controller.initType=AddressInitTypeBuy;
            controller.needSelected = YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)changeGoodsNum
{
    float totalPrice = _goodsEntity.price * [self.infoControl goodsNum];
    
    [self.goodsInfoCell setTotalPrice:totalPrice];
}

@end

