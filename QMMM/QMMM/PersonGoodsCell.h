//
//  PersonGoodsCell.h
//  QMMM
//
//  Created by kingnet  on 14-11-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kPersonGoodsCellIdentifier;

@interface PersonGoodsCell : UITableViewCell

@property (nonatomic, strong) NSArray *items; //里面存PersonGoodsItem

- (id)initWithItems:(NSArray *)items;

@end
