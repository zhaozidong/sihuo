//
//  ImageListCell.m
//  QMMM
//
//  Created by kingnet  on 14-10-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "ImageListCell.h"

NSString * const kImageListCellIdentifier = @"kImageListCellIdentifier";

@interface ImageListCell ()

@property (weak, nonatomic) IBOutlet UIButton *delButton;

@property (weak, nonatomic) IBOutlet UIImageView *photoImageV;
@end
@implementation ImageListCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"ImageListCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _photoImageV.contentMode = UIViewContentModeScaleAspectFill;
    _photoImageV.transform = CGAffineTransformMakeRotation(M_PI/2);

    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [_photoImageV setUserInteractionEnabled:YES];
    [_photoImageV addGestureRecognizer:singleTap];
    
    
    _delButton.frame = CGRectMake(0, 0, 17, 17);
    _delButton.center = CGPointMake(CGRectGetMaxX(_photoImageV.frame), CGRectGetMinY(_photoImageV.frame));
    [_delButton addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
}


-(void)tapDetected{
    if (self.delegate && [self.delegate respondsToSelector:@selector(editImageWithPath:)]) {
        [self.delegate editImageWithPath:_photoPath];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight
{
    return 77.f;
}

- (void)setPhoto:(UIImage *)photo
{
    _photo = photo;
    _photoImageV.image = photo;
}

- (void)setPhotoPath:(NSString *)photoPath
{
    _photoPath = photoPath;
    UIImage *image = [UIImage imageWithContentsOfFile:photoPath];
    _photoImageV.image = image;
}

- (void)deleteAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteImageWithPath:)]) {
        [self.delegate deleteImageWithPath:_photoPath];
    }
}

@end
