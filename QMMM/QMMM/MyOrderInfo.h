//
//  MyOrderInfo.h
//  QMMM
//
//  Created by Derek.zhao on 14-12-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BaseHandler.h"
#import "Order.h"


@protocol QMPayOrderDelegate <NSObject>

@optional;
-(void)paySuccessForOrderId:(NSString *)orderId;

-(void)payFail:(NSString *)tip;

@end


@interface MyOrderInfo :BaseHandler

@property(strong,nonatomic)NSString *callBackURL;
@property(weak,nonatomic) id<QMPayOrderDelegate> delegate;

- (void)payOrder;
- (void)notifyForSuccessWithParams:(NSDictionary *)dictParams;
- (id)initWithOrderId:(NSString *)orderId andRemain:(float)remainPay;

@end
