//
//  SpecialGoodsTopCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class ActivityEntity;
@interface SpecialGoodsTopCell : UITableViewCell

+ (SpecialGoodsTopCell *)topCell;

- (void)updateUI:(ActivityEntity *)entity;

+ (CGFloat)imageHeight;

@end
