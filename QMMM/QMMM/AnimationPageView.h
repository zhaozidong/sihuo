//
//  AnimationPageView.h
//  QMMM
//
//  Created by kingnet  on 14-11-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern const int kAnimateDuration;

@interface AnimationPageView : UIView

+ (AnimationPageView *)animationView;

- (void)startAnimate;

@end
