//
//  GoodsHandler.h
//  QMMM
//
//  Created by 韩芦 on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseHandler.h"

@class GoodsDataHelper;
@class GoodEntity;
@class MyGoodsList;
@class GoodsSimpleInfo;

//加载数据状态
extern NSInteger const kTTGoodsLoadStateNone;
extern NSInteger const kTTGoodsLoadStateLoadDone;
extern NSInteger const kTTGoodsLoadStateLoading;
extern NSInteger const kTTGoodsLoadStateLoadTimeout;
extern NSInteger const kTTGoodsLoadStateLoadFail;
extern NSInteger const kTTGoodsLoadStateLoadEnd;

//上传文件的key
extern NSString * const kPhotoKey;
extern NSString * const kPhotoFilePath;

//搜索的key
extern NSString * const kSearchGoodsWords; //关键字
extern NSString * const kSearchGoodsCategory; //分类
extern NSString * const kSearchGoodsPriceMin;  //价格下限
extern NSString * const kSearchGoodsPriceMax;  //价格上限
extern NSString * const kSearchGoodsOffset;   //查询偏移量
extern NSString * const kSearchGoodsSort;   //排序方式 可选值：price_desc 价格高->低 price_asc 价格低->高 pub_ts_desc 发布时间近->远 pub_ts_asc 发布时间 远->近
extern NSString * const kSearchGoodsCondition;  //新旧程度
extern NSString * const kSearchGoodsDiscount;   //折扣力度
extern NSString * const kSearchGoodsDistance; //距离
extern NSString * const kSearchGoodsPoint; //地理位置坐标

@interface GoodsHandler : BaseHandler

+ (GoodsHandler *)shareGoodsHandler;

+ (void)freeGoodsHandler;

////测试:线程相关
//+ (BOOL)isRuning;
//- (void)start;
//- (void)stop;

//数据库操作相关
@property (readonly, strong) GoodsDataHelper * goodsHelper;

//精品列表
@property (readonly, strong) MyGoodsList * myGoodsList;

//朋友商品列表
@property (readonly, strong) MyGoodsList * friendGoodsList;

- (void)decreaseCommentCount:(uint64_t)goodsId;

- (void)upLoadAudio:(NSData*)data success:(SuccessBlock)success failed:(FailedBlock)failed;

- (void)downloadAudio:(NSString *)audioPath complete:(CompleteBlock)completeHandle;

- (void)upLoadImage:(NSData *)data success:(SuccessBlock)success failed:(FailedBlock)failed;

- (void)releaseGoods:(GoodEntity *)entity success:(SuccessBlock)success failed:(FailedBlock)failed;
- (void)goodsDetailsWithId:(uint16_t)goodsId prisNum:(NSInteger)prisNum success:(SuccessBlock)success failed:(FailedBlock)failed;

- (void)commentListWithId:(uint64_t)goodsId offset:(uint64_t)offset success:(SuccessBlock)success failed:(FailedBlock)failed;

- (void)addFavor:(uint64_t)goodsId sellerId:(uint64_t)sellerId;

- (void)sendComment:(uint64_t)goodsId pid:(uint64_t)pid comments:(NSString *)comments success:(SuccessBlock)success failed:(FailedBlock)failed;

//搜索商品
- (void)searchGoods:(NSDictionary *)dict success:(SuccessBlock)success failed:(FailedBlock)failed;

//订单列表
- (void)getOrderList:(uint)type orderId:(NSString *)orderId success:(SuccessBlock)success failed:(FailedBlock)failed;

//订单详情
- (void)getOrderDetail:(uint)type orderId:(NSString *)orderId success:(SuccessBlock)success failed:(FailedBlock)failed;

//评分
- (void)doScores:(NSString *)orderId scores:(float)scores comment:(NSString *)comment success:(SuccessBlock)success failed:(FailedBlock)failed;

//发货
- (void)deliverGoods:(NSString *)orderId expressCo:(NSString *)expressCo express_num:(NSString *)express_num success:(SuccessBlock)success failed:(FailedBlock)failed;

//赞列表
- (void)getFavorList:(NSString *)goodsId success:(SuccessBlock)success failed:(FailedBlock)failed;

//删除评论
- (void)deleteCommit:(NSString *)cId goodsId:(NSString *)gId success:(SuccessBlock)success failed:(FailedBlock)failed;

//已购买商品列表
- (void)getBoughtGoodsList:(NSString *)goodsId success:(SuccessBlock)success failed:(FailedBlock)failed;

//获取商品基本信息
- (void)getGoodsInfoWithId:(uint64_t)goodsId success:(SuccessBlock)success failed:(FailedBlock)failed;

//更新商品信息
- (void)updateGoodsInfo:(GoodEntity *)goodsEntity success:(SuccessBlock)success failed:(FailedBlock)failed;

//删除商品
- (void)deleteGoodsWithId:(uint64_t)goodsId success:(SuccessBlock)success failed:(FailedBlock)failed;

//上下架商品
- (void)dropGoodsWithId:(GoodsSimpleInfo*)simpeInfo bdrop:(BOOL)drop success:(SuccessBlock)success failed:(FailedBlock)failed;

//更新订单简要信息
- (void)updateOrderStatus:(BOOL)bSeller orderId:(NSString *)orderId status:(uint)status;

//批量上传图片
//传入的是小图的路径
- (void)batchUploadGoodsPhoto:(NSArray *)filePaths success:(SuccessBlock)success failed:(FailedBlock)failed;

//单张上传图片带进度的
- (void)uploadGoodsPhotoData:(NSData *)imgData uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))uploadProgressBlock success:(SuccessBlock)success failed:(FailedBlock)failed;

//提交订单
- (void)submitOrder:(int)addressId goodsId:(uint64_t)goodsId paymentType:(int)paymentType num:(int)num remainPay:(float)remainPay success:(SuccessBlock)success failed:(FailedBlock)failed;

//删除订单
- (void)deleteOrder:(NSString *)orderId type:(int)type success:(SuccessBlock)success failed:(FailedBlock)failed;

//取消订单
- (void)cancelOrder:(NSString *)orderId success:(SuccessBlock)success failed:(FailedBlock)failed;

//获取活动信息
- (void)getActivitiesInfo:(SuccessBlock)success failed:(FailedBlock)failed;

//获取引导页商品信息
- (void)getIntroGoodsListSuccess:(SuccessBlock)success failed:(FailedBlock)failed;

//获取全部专题
- (void)getAllTopic:(SuccessBlock)success failed:(FailedBlock)failed;
/**
 获取专题商品列表
 @param type 1下拉 0上拉
 @param key 分页Key，首次传0，服务器端给的
 @param tag 活动类型
 **/
- (void)getSpecialGoodsList:(uint)top key:(uint64_t)key tag:(NSString *)tag success:(SuccessBlock)success failed:(FailedBlock)failed;


/**
 获取商品分类
 **/
- (void)getCategory:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 从本地获取商品分类
 **/
- (void)getCategoryFromLocal:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 根据描述获取分类
 **/
- (void)getCategoryByDesc:(NSString *)desc success:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 推荐商品列表，走搜索接口
 **/
- (void)topicsGoodsFromSearch:(NSString *)requestURL success:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 推荐商品列表，走小编推荐
 **/
- (void)topicsGoodsFromSpetial:(NSString *)requestURL success:(SuccessBlock)success failed:(FailedBlock)failed;

/**************************退款相关*****************************/
/**
 卖家关闭订单并退款
 **/
- (void)sellerRefund:(NSString *)orderId success:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 买家申请退款
 **/
- (void)applyRefund:(NSString *)orderId amount:(float)amout reason:(NSString *)reason success:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 卖家同意退款
 **/
- (void)acceptRefund:(NSString *)orderId success:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 卖家拒绝退款
 **/
- (void)rejectRefund:(NSString *)orderId reason:(NSString *)reason success:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 兑换商品
 **/
- (void)exchangeGoods:(int)num goodsId:(NSString *)goodsId addressId:(int)addressId success:(SuccessBlock)success failed:(FailedBlock)failed;

@end
