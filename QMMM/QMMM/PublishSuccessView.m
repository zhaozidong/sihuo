//
//  PublishSuccessView.m
//  QMMM
//
//  Created by kingnet  on 14-10-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PublishSuccessView.h"
#import "ADTickerLabel.h"

@interface PublishSuccessView()
@property (weak, nonatomic) IBOutlet UIButton *inviteFriendsBtn;
@property (weak, nonatomic) IBOutlet UIButton *checkGoodsBtn;
@property (strong, nonatomic) ADTickerLabel *tickerLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipLbl;
@property (strong, nonatomic) IBOutlet UIView *viPush;

@property (strong, nonatomic) IBOutlet UIButton *btnCopy;
@property (strong, nonatomic) IBOutlet UIView *viContainer;
@property (strong, nonatomic) IBOutlet UIButton *btnWeibo;


@end
@implementation PublishSuccessView

+ (PublishSuccessView *)publishView
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"PublishSuccessView" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    UIImage *image = [UIImage imageNamed:@"invite_btn"];
    [self.inviteFriendsBtn setBackgroundImage:[image stretchableImageWithLeftCapWidth:7.f topCapHeight:7.f] forState:UIControlStateNormal];
    UIImage *image1 = [UIImage imageNamed:@"checkGoods_btn"];
    [self.checkGoodsBtn setBackgroundImage:[image1 stretchableImageWithLeftCapWidth:7.f topCapHeight:7.f] forState:UIControlStateNormal];
   
    UIFont *font = [UIFont systemFontOfSize:30.f];
//    float x = (kMainFrameWidth-231)/2.f+20.f;
    self.tickerLabel = [[ADTickerLabel alloc]init];
    self.tickerLabel.font = font;
    self.tickerLabel.textColor = kRedColor;
    self.tickerLabel.characterWidth = 18;
    self.tickerLabel.changeTextAnimationDuration = 1.5;
    [self addSubview:self.tickerLabel];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    UIFont *font = [UIFont systemFontOfSize:30.f];
    NSInteger length=[self.tickerLabel.text length];
    float x=_viPush.frame.origin.x+100-6*length;
    self.tickerLabel.frame=CGRectMake(x, _viPush.frame.origin.y, 0, font.lineHeight);
}

-(void)updateConstraints{
    [super updateConstraints];
    
    CGRect frm=_btnWeibo.frame;
    frm.size.width=200;
    frm.size.height=200;
    _btnWeibo.frame=frm;
}

- (void)setToNum:(NSUInteger)toNum
{
    self.tickerLabel.text = [NSString stringWithFormat:@"%ld", (unsigned long)toNum];
    [self layoutSubviews];
}

- (IBAction)weixinAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareToWeiXin)]) {
        [self.delegate shareToWeiXin];
    }
}

- (IBAction)weixinFriendsAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareToWeiXinFriends)]) {
        [self.delegate shareToWeiXinFriends];
    }
}
- (IBAction)shareToWeiBo:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareToWeiBo)]) {
        [self.delegate shareToWeiBo];
    } 
}

- (IBAction)shareToQZone:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareToQQZone)]) {
        [self.delegate shareToQQZone];
    }
}


//- (IBAction)weiboAction:(id)sender {
//    if (self.delegate && [self.delegate respondsToSelector:@selector(shareToWeiBo)]) {
//        [self.delegate shareToWeiBo];
//    }
//}
//
//- (IBAction)qqzoneAction:(id)sender {
//    if (self.delegate && [self.delegate respondsToSelector:@selector(shareToQQZone)]) {
//        [self.delegate shareToQQZone];
//    }
//}

- (IBAction)copyAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(copyUrl)]) {
        [self.delegate copyUrl];
    }
}

- (IBAction)inviteFriendsAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(inviteMoreFriends)]) {
        [self.delegate inviteMoreFriends];
    }
}

- (IBAction)checkGoodsAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(checkGoods)]) {
        [self.delegate checkGoods];
    }
}

@end
