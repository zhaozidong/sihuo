//
//  GoodsChooseCateCell.h
//  QMMM
//
//  Created by kingnet  on 15-1-14.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GoodsChooseCateCellDelegate <NSObject>

- (void)didTapChooseCate;

@end
@interface GoodsChooseCateCell : UITableViewCell

@property (nonatomic, strong) NSString *cateName;

@property (nonatomic, weak) id<GoodsChooseCateCellDelegate> delegate;

+ (GoodsChooseCateCell *)chooseCateCell;

+ (CGFloat)cellHeight;

@end
