//
//  ContactorInfo.m
//  QMMM
//
//  Created by hanlu on 14-10-21.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "ContactorInfo.h"
#import "FMResultSet.h"

@implementation ContactorInfo

- (id)initWithFMResult:(FMResultSet *)resultSet
{
    self = [super init];
    if(self) {
        _cellPhone = [resultSet stringForColumnIndex:0];
        _name = [resultSet stringForColumnIndex:1];
    }
    return self;
}

@end
