//
//  QMFavorViewController.h
//  QMMM
//
//  Created by hanlu on 14-11-11.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMFavorViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView * tableView;

@end
