//
//  SearchHistoryCell.m
//  QMMM
//
//  Created by kingnet  on 15-1-13.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "SearchHistoryCell.h"
#import "KeyWordEntity.h"

NSString * kSearchHistoryCellIdentifier = @"kSearchHistoryCellIdentifier";

@interface SearchHistoryCell ()
@property (weak, nonatomic) IBOutlet UILabel *textLbl;

@end

@implementation SearchHistoryCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"SearchHistoryCell" bundle:[NSBundle mainBundle]];
}

- (void)awakeFromNib {
    // Initialization code
    self.textLbl.backgroundColor = [UIColor clearColor];
    self.textLbl.text = @"";
    self.textLbl.textColor = kDarkTextColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight
{
    return 45.f;
}

- (void)updateUI:(KeyWordEntity *)entity
{
    self.textLbl.text = entity.keyWord;
}

@end
