//
//  NoCommentTipCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-10.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoCommentTipCell : UITableViewCell

+ (NoCommentTipCell *)tipCell;

@property (nonatomic, strong) NSString *tip;

@end
