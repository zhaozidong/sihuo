//
//  ReliabilityView.h
//  QMMM
//
//  Created by kingnet  on 15-4-14.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReliabilityView : UIControl

+ (ReliabilityView *)reliabilityView;

+ (CGFloat)viewHeight;

@end
