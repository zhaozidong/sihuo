//
//  PersonNumIntroCell.m
//  QMMM
//
//  Created by kingnet  on 15-1-22.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "PersonNumIntroCell.h"
#import "UserDataInfo.h"

@interface PersonNumIntroCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonWidthCst;
@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondBtn;
@property (weak, nonatomic) IBOutlet UIButton *thirdBtn;
@property (weak, nonatomic) IBOutlet UIButton *fouthBtn;

@end
@implementation PersonNumIntroCell

+ (PersonNumIntroCell *)numIntroCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"PersonNumIntroCell" owner:self options:0];
    return [array lastObject];
}

+ (CGFloat)cellHeight
{
    return 65.f;
}

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = [UIColor whiteColor];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.firstBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.firstBtn.titleLabel.numberOfLines = 2;
    self.secondBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.secondBtn.titleLabel.numberOfLines = 2;
    self.thirdBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.thirdBtn.titleLabel.numberOfLines = 2;
    self.fouthBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.fouthBtn.titleLabel.numberOfLines = 2;
 
    [self setButtonTitle:0 influence:0 soldNum:0 positiveRate:0];
    
    self.buttonWidthCst.constant = kMainFrameWidth/4;
    [self.contentView layoutIfNeeded];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(PersonPageUserInfo *)person
{
    int rate = round(((float)person.userInfo.positive/(float)person.userInfo.total)*100);
    [self setButtonTitle:person.reliability influence:person.influence soldNum:person.userInfo.total positiveRate:rate];
}

- (IBAction)btnDidCredibility:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(credibilityClicked)]) {
        [_delegate credibilityClicked];
    }
}

- (IBAction)btnDidInfluence:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(influenceClicked)]) {
        [_delegate influenceClicked];
    }
}

- (IBAction)btnDidTradingVolume:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(tradingVolumeClicked)]) {
        [_delegate tradingVolumeClicked];
    }
}

- (IBAction)btnDidClickGoodRate:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(goodRateClicked)]) {
        [_delegate goodRateClicked];
    }
}

- (void)setButtonTitle:(int)reliability influence:(int)influence soldNum:(int)soldNum positiveRate:(int)positiveRate
{
    NSString *str = [NSString stringWithFormat:@"%d\n可信度", reliability];
    NSString *numStr = [NSString stringWithFormat:@"%d", reliability];
    NSMutableAttributedString *mbAttribtedStr = [[NSMutableAttributedString alloc]initWithString:str];
    [mbAttribtedStr addAttribute:NSForegroundColorAttributeName value:kRedColor range:NSMakeRange(0, numStr.length)];
    [mbAttribtedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20.f] range:NSMakeRange(0, numStr.length)];
    [mbAttribtedStr addAttribute:NSForegroundColorAttributeName value:kDarkTextColor range:NSMakeRange(numStr.length, str.length-numStr.length)];
    [mbAttribtedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.f] range:NSMakeRange(numStr.length, str.length-numStr.length)];
    [self.firstBtn setAttributedTitle:mbAttribtedStr forState:UIControlStateNormal];
    
    str = [NSString stringWithFormat:@"%d\n影响力", influence];
    numStr = [NSString stringWithFormat:@"%d", influence];
    mbAttribtedStr = [[NSMutableAttributedString alloc]initWithString:str];
    [mbAttribtedStr addAttribute:NSForegroundColorAttributeName value:kRedColor range:NSMakeRange(0, numStr.length)];
    [mbAttribtedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20.f] range:NSMakeRange(0, numStr.length)];
    [mbAttribtedStr addAttribute:NSForegroundColorAttributeName value:kDarkTextColor range:NSMakeRange(numStr.length, str.length-numStr.length)];
    [mbAttribtedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.f] range:NSMakeRange(numStr.length, str.length-numStr.length)];
    [self.secondBtn setAttributedTitle:mbAttribtedStr forState:UIControlStateNormal];
    
    str = [NSString stringWithFormat:@"%d\n成交量", soldNum];
    numStr = [NSString stringWithFormat:@"%d", soldNum];
    mbAttribtedStr = [[NSMutableAttributedString alloc]initWithString:str];
    [mbAttribtedStr addAttribute:NSForegroundColorAttributeName value:kRedColor range:NSMakeRange(0, numStr.length)];
    [mbAttribtedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20.f] range:NSMakeRange(0, numStr.length)];
    [mbAttribtedStr addAttribute:NSForegroundColorAttributeName value:kDarkTextColor range:NSMakeRange(numStr.length, str.length-numStr.length)];
    [mbAttribtedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.f] range:NSMakeRange(numStr.length, str.length-numStr.length)];
    
    [self.thirdBtn setAttributedTitle:mbAttribtedStr forState:UIControlStateNormal];
    
    str = [NSString stringWithFormat:@"%d%%\n好评率", positiveRate];
    numStr = [NSString stringWithFormat:@"%d%%", positiveRate];
    mbAttribtedStr = [[NSMutableAttributedString alloc]initWithString:str];
    [mbAttribtedStr addAttribute:NSForegroundColorAttributeName value:kRedColor range:NSMakeRange(0, numStr.length)];
    [mbAttribtedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:20.f] range:NSMakeRange(0, numStr.length)];
    [mbAttribtedStr addAttribute:NSForegroundColorAttributeName value:kDarkTextColor range:NSMakeRange(numStr.length, str.length-numStr.length)];
    [mbAttribtedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.f] range:NSMakeRange(numStr.length, str.length-numStr.length)];
    [self.fouthBtn setAttributedTitle:mbAttribtedStr forState:UIControlStateNormal];
}

@end
