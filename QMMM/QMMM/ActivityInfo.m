//
//  ActivityInfo.m
//  QMMM
//
//  Created by kingnet  on 14-12-31.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "ActivityInfo.h"

@implementation ActivityInfo

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        id temp = [dict objectForKey:@"banner"];
        if (temp && [temp isKindOfClass:[NSArray class]]) {
            NSMutableArray *mbArray = [NSMutableArray array];
            for (NSDictionary *dic in temp) {
                BannerEntity *entity = [[BannerEntity alloc]initWithDictionary:dic];
                [mbArray addObject:entity];
            }
            self.banners = mbArray;
        }
        
        temp = [dict objectForKey:@"activities"];
        if (temp && [temp isKindOfClass:[NSArray class]]) {
            NSMutableArray *mbArray = [NSMutableArray array];
            for (NSDictionary *dic in temp) {
                ActivityEntity *entity = [[ActivityEntity alloc]initWithDictionary:dic];
                [mbArray addObject:entity];
            }
            self.activities = mbArray;
        }
        
        temp = [dict objectForKey:@"hot_category"];
        if (temp && [temp isKindOfClass:[NSArray class]]) {
            NSMutableArray *mbArray = [NSMutableArray array];
            for (NSDictionary *dic in temp) {
                CategoryEntity *entity = [[CategoryEntity alloc]initWithDictionary:dic];
                [mbArray addObject:entity];
            }
            self.categorys = mbArray;
        }
        
        temp = [dict objectForKey:@"special_topic"];
        if (temp && [temp isKindOfClass:[NSArray class]]) {
            NSMutableArray *mbArray = [NSMutableArray array];
            for (NSDictionary *dic in temp) {
                SpecialTopicEntity *entity = [[SpecialTopicEntity alloc]initWithDictionary:dic];
                [mbArray addObject:entity];
            }
            self.specialTopics = mbArray;
        }
    }
    return self;
}

@end

@implementation ActivityEntity

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        id temp = [dict objectForKey:@"bSrc"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.bigImgSrc = temp;
        }
        
        temp = [dict objectForKey:@"sSrc"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.smallImgSrc = temp;
        }
        
        temp = [dict objectForKey:@"title"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.title = temp;
        }
        
        temp = [dict objectForKey:@"tag"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.tag = temp;
        }
        
        temp = [dict objectForKey:@"desc"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.desc = temp;
        }
    }
    return self;
}

- (id)initWithEntity:(ActivityEntity *)entity
{
    self = [super init];
    if (self) {
        self.bigImgSrc = entity.bigImgSrc;
        self.smallImgSrc = entity.smallImgSrc;
        self.title = entity.title;
        self.tag = entity.tag;
        self.desc = entity.desc;
    }
    return self;
}

@end

@implementation BannerEntity

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        id temp = [dict objectForKey:@"src"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.imgSrc = temp;
        }
        
        temp = [dict objectForKey:@"url"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.activityUrl = temp;
        }
        
        temp = [dict objectForKey:@"share"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.isShowShare = [[dict objectForKey:@"share"] integerValue];
        }else if(temp && [temp isKindOfClass:[NSString class]]){
            self.isShowShare=[temp integerValue];
        }else{
            self.isShowShare=0;
        }
    }
    return self;
}

- (id)initWithEntity:(BannerEntity *)entity
{
    self = [super init];
    if (self) {
        self.imgSrc = entity.imgSrc;
        self.activityUrl = entity.activityUrl;
    }
    return self;
}

@end

@implementation CategoryEntity

-(id)initWithDictionary:(NSDictionary *)dict{
    self=[super init];
    if (self) {
        id temp = [dict objectForKey:@"id"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.categoryId = [[dict objectForKey:@"id"] integerValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.categoryId = [temp integerValue];
        }
        
        temp = [dict objectForKey:@"name"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.name = temp;
        }
        else{
            self.name = @"";
        }
        
        temp = [dict objectForKey:@"icon"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.icon = temp;
        }
        else{
            self.icon = @"";
        }
        
    }
    return self;
}

-(id)initWithEntity:(CategoryEntity *)entity{
    self=[super init];
    if (self) {
        self.categoryId=entity.categoryId;
        self.name=entity.name;
        self.icon=entity.icon;
    }
    return self;
}
@end




@implementation SpecialTopicEntity
-(id)initWithDictionary:(NSDictionary *)dict{
    self=[super init];
    if (self) {
        id temp = [dict objectForKey:@"name"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            _name=temp;
        }else{
            _name=@"";
        }
        
        temp=[dict objectForKey:@"type"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            _type=temp;
        }else{
            _type=@"";
        }

        temp=[dict objectForKey:@"icon"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            _icon=temp;
        }else{
            _icon=@"";
        }
        
        temp=[dict objectForKey:@"url"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            _url=temp;
        }else{
            _url=@"";
        }
        
        temp=[dict objectForKey:@"unique_key"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            _key=temp;
        }else{
            _key=@"";
        }
        
        temp = [dict objectForKey:@"parse_type"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            _parseType = temp;
        }
        else{
            _parseType = @"";
        }
        
        temp = [dict objectForKey:@"trust"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            _isTrust = [temp boolValue];
        }
    }
    return self;
}

-(id)initWithEntity:(SpecialTopicEntity *)entity{
    self=[super init];
    if (self) {
        self.name=entity.name;
        self.type=entity.type;
        self.icon=entity.icon;
        self.url=entity.url;
        self.parseType = entity.parseType;
        self.isTrust = entity.isTrust;
    }
    return self;
}
@end


