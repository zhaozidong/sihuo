//
//  TopicGoodsListViewController.m
//  QMMM
//
//  Created by kingnet  on 15-3-16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "TopicGoodsListViewController.h"
#import "SearchResultViewController.h"
#import "GoodsHandler.h"
#import "GoodDataInfo.h"
#import "GoodEntity.h"

#import "QMFailPrompt.h"
#import "ReliabilityView.h"
#import "QMQAViewController.h"

@interface TopicGoodsListViewController ()<SearchResultViewCtlDelegate>
{
    SearchResultViewController *searchResultViewCtl_;
    ReliabilityView *trustView_; //可信度提示view
    NSString *requestURL_;  //请求的路径
    NSString *navTitle_;  //标题
    __block uint64_t lastKey_; //上拉的key
    BOOL isTrust_; //是否是可信度商品界面
}

@end

@implementation TopicGoodsListViewController

- (id)initWithTitle:(NSString *)title requestURL:(NSString *)requestURL isTrust:(BOOL)isTrust
{
    self = [super init];
    if (self) {
        requestURL_ = requestURL;
        navTitle_ = title;
        isTrust_ = isTrust;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (navTitle_) {
        self.navigationItem.title = navTitle_;
    }
    
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    if (requestURL_ == nil) {
        QMFailPrompt *prompt = [[QMFailPrompt alloc]initWithFailType:QMFailTypeLoadFail labelText:@"哎呀，加载失败了" buttonType:QMFailButtonTypeNone buttonText:nil];
        prompt.frame = CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
        [self.view addSubview:prompt];
        [AppUtils showAlertMessage:[AppUtils localizedCommonString:@"QM_Alert_Network"]];
    }
    else{
        searchResultViewCtl_ = [[SearchResultViewController alloc]init];
        [self addChildViewController:searchResultViewCtl_];
        [searchResultViewCtl_ didMoveToParentViewController:searchResultViewCtl_];
        searchResultViewCtl_.view.frame = CGRectMake(0, kMainTopHeight, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-kMainTopHeight);
        searchResultViewCtl_.delegate = self;
        [self.view addSubview:searchResultViewCtl_.view];
        
        lastKey_ = 0;
        [AppUtils showProgressMessage:@"加载中"];
        [self getGoodsList];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//拼接URL
- (void)spliceURL
{
    NSRange range = [requestURL_ rangeOfString:@"&key"];
    if (range.location!=NSNotFound) {
        NSString *subStr = [requestURL_ substringToIndex:range.location];
        requestURL_ = [NSString stringWithFormat:@"%@&key=%qu", subStr, lastKey_];
    }
    else{
        requestURL_ = [NSString stringWithFormat:@"%@&key=%qu", requestURL_, lastKey_];
    }
}

- (void)getGoodsList
{
    //加上offset
    [self spliceURL];
    __weak typeof(self) weakSelf = self;
    [[GoodsHandler shareGoodsHandler]topicsGoodsFromSpetial:requestURL_ success:^(id obj) {
        [AppUtils dismissHUD];
        [searchResultViewCtl_ endFooterRereshing];
        NSDictionary *retDic = (NSDictionary *)obj;
        [weakSelf reloadUI:retDic[@"list"]];
        if (retDic[@"lastKey"]) {
            lastKey_ = [retDic[@"lastKey"] intValue];
        }
        
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        [searchResultViewCtl_ endFooterRereshing];
    }];
}

//TODO:可以放在dispatch_asyn中去做
- (void)reloadUI:(NSArray *)dataArr
{
    if (dataArr.count > 0) {
        NSMutableArray *goodsArr = [NSMutableArray array];
        for (GoodEntity *goods in dataArr) {
            GoodsSimpleInfo *goodsInfo = [[GoodsSimpleInfo alloc]init];
            goodsInfo.gid = goods.gid;
            goodsInfo.photo = goods.picturelist.rawArray[0];
            goodsInfo.desc = goods.desc;
            goodsInfo.price = goods.price;
            goodsInfo.trust = goods.trust;
            [goodsArr addObject:goodsInfo];
        }
        
        [searchResultViewCtl_ addGoods:goodsArr];
    }
    else{
        if (lastKey_ == 0) {
            QMFailPrompt *prompt = [[QMFailPrompt alloc]initWithFailType:QMFailTypeNOData labelText:@"没有找到相关商品~" buttonType:QMFailButtonTypeNone buttonText:nil];
            prompt.frame = CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
            [self.view addSubview:prompt];
        }
    }
}

#pragma mark - SearchResultViewCtlDelegate
- (void)loadMoreData
{
    [self getGoodsList];
}

- (UIView *)topSectionView
{
    if (isTrust_) {
        if (!trustView_) {
            trustView_ = [ReliabilityView reliabilityView];
            trustView_.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), [ReliabilityView viewHeight]);
            [trustView_ addTarget:self action:@selector(clickTrustView:) forControlEvents:UIControlEventTouchUpInside];
        }
        return trustView_;
    }
    return [[UIView alloc]initWithFrame:CGRectZero];
}

#pragma mark - 
- (void)clickTrustView:(id)sender
{
    QMQAViewController *controller = [[QMQAViewController alloc]initWithType:QMQATypeKeXinDu];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
