//
//  QMConfirmReceivingGoodsView.m
//  QMMM
//
//  Created by hanlu on 14-9-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMConfirmReceivingGoodsView.h"
#import "UIButton+Addition.h"

#define DEFAULT_TEXT_LEN 100

@implementation QMConfirmReceivingGoodsView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.commentTextView.placeholder = [AppUtils localizedProductString:@"QM_Text_Comment_Phaceholer"];
    self.commentTextView.placeholderTextColor = kPlaceholderColor;
    self.commentTextView.textColor = kDarkTextColor;
    
    UIColor * defaultColor = UIColorFromRGB(0x9198a3);
    
    [_goodBtn setBackgroundColor:[UIColor whiteColor]];
    [_goodBtn setTitle:[AppUtils localizedProductString:@"QM_Text_Comment_Good"] forState:UIControlStateNormal];
    [_goodBtn setTitleColor:defaultColor forState:UIControlStateNormal];
    [_goodBtn setTitleColor:UIColorFromRGB(0xff3538) forState:UIControlStateSelected];
    [_goodBtn setImage:[UIImage imageNamed:@"comment_unselect"] forState:UIControlStateNormal];
    [_goodBtn setImage:[UIImage imageNamed:@"comment_good"] forState:UIControlStateSelected];
    [_goodBtn addTarget:self action:@selector(goodBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_goodBtn setButtonType:BT_LeftImageRightTitle interGap:5];
    

    [_normalBtn setBackgroundColor:[UIColor whiteColor]];
    [_normalBtn setTitle:[AppUtils localizedProductString:@"QM_Text_Comment_Normal"] forState:UIControlStateNormal];
    [_normalBtn setTitleColor:defaultColor forState:UIControlStateNormal];
    [_normalBtn setTitleColor:UIColorFromRGB(0xffae00) forState:UIControlStateSelected];
    [_normalBtn setImage:[UIImage imageNamed:@"comment_unselect"] forState:UIControlStateNormal];
    [_normalBtn setImage:[UIImage imageNamed:@"comment_normal"] forState:UIControlStateSelected];
    [_normalBtn addTarget:self action:@selector(normalBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_normalBtn setButtonType:BT_LeftImageRightTitle interGap:5];
    
    [_badBtn setBackgroundColor:[UIColor whiteColor]];
    [_badBtn setTitle:[AppUtils localizedProductString:@"QM_Text_Comment_Bad"] forState:UIControlStateNormal];
    [_badBtn setTitleColor:defaultColor forState:UIControlStateNormal];
    [_badBtn setTitleColor:UIColorFromRGB(0x75797f) forState:UIControlStateSelected];
    [_badBtn setImage:[UIImage imageNamed:@"comment_unselect"] forState:UIControlStateNormal];
    [_badBtn setImage:[UIImage imageNamed:@"comment_bad"] forState:UIControlStateSelected];
    [_badBtn addTarget:self action:@selector(badBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_badBtn setButtonType:BT_LeftImageRightTitle interGap:5];
    
    UIImage * image = [UIImage imageNamed:@"ok_btn"];
    image = [image stretchableImageWithLeftCapWidth:5 topCapHeight:6];
    [_confirmBtn setBackgroundImage:image forState:UIControlStateNormal];
    [_confirmBtn setTitle:[AppUtils localizedProductString:@"QM_Text_Accept_Goods"] forState:UIControlStateNormal];
    
    _commentTextView.delegate = self;
    
    self.backgroundColor = [UIColor clearColor];
    
    _normalBtn.selected = NO;
    _badBtn.selected = NO;
    _goodBtn.selected = NO;
    
    _selectIndex = -1;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = self.bounds;
    
    NSUInteger margin = 0;
    CGFloat btnWidth = (frame.size.width - margin * 4) / 3;
    
    frame = _goodBtn.frame;
    frame.origin.x = self.bounds.origin.x + margin;
    frame.size.width = btnWidth;
    _goodBtn.frame = frame;
    
    frame = _normalBtn.frame;
    frame.origin.x = _goodBtn.frame.origin.x + _goodBtn.frame.size.width + margin;
    frame.size.width = btnWidth;
    _normalBtn.frame = frame;
    
    frame = _badBtn.frame;
    frame.origin.x = _normalBtn.frame.origin.x + _normalBtn.frame.size.width + margin;
    frame.size.width = btnWidth;
    _badBtn.frame = frame;
    
//    NSUInteger btnMargin = 43;
//    frame = _confirmBtn.frame;
//    frame.origin.x = self.bounds.origin.x + btnMargin;
//    frame.size.width = self.bounds.size.width - 2 * btnMargin;
//    _confirmBtn.frame = frame;
    
    UIImage * image = [UIImage imageNamed:@"ok_btn"];
    image = [image stretchableImageWithLeftCapWidth:5 topCapHeight:6];
    [_confirmBtn setBackgroundImage:image forState:UIControlStateNormal];
}

#pragma mark - 

- (void)goodBtnClick:(id)sender
{
    _normalBtn.selected = NO;
    _badBtn.selected = NO;
    
    _selectIndex = 3;
    _goodBtn.selected = YES;
}

- (void)normalBtnClick:(id)sender
{
    _goodBtn.selected = NO;
    _badBtn.selected = NO;

     _selectIndex = 2;
     _normalBtn.selected = YES;
}

- (void)badBtnClick:(id)sender
{
    _normalBtn.selected = NO;
    _goodBtn.selected = NO;

    _selectIndex = 1;
    _badBtn.selected = YES;
}


#pragma mark - 

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    BOOL result = YES;
    NSString *textString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([textString length] > DEFAULT_TEXT_LEN) {
        result = NO;
        textView.text = [textString substringToIndex:DEFAULT_TEXT_LEN];
    }
    return result;
}

@end
