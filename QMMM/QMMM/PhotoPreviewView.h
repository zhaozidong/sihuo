//
//  PhotoPreviewView.h
//  QMMM
//
//  Created by Shinancao on 14-9-21.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

enum {
    QMGoodsPreview = 9999,  //发布商品的图片预览
    QMLifePhotoPreview    //个人资料的生活照预览
};
typedef int QMPreviewType;

@protocol PhotoPreviewViewDelegate <NSObject>

- (void)deleteImageWithPath:(NSIndexPath *)indexPath;

@optional
- (void)makeMainPhoto:(NSString *)imgFilePath; //设置为主图

@end
@interface PhotoPreviewView : UIView
@property (nonatomic, weak) id<PhotoPreviewViewDelegate> delegate;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy) NSString *filePath;  //可能为本地的，也可能为服务器上的
@property (nonatomic, assign) BOOL isMainPhoto;
@property (nonatomic, assign) QMImageSizeType imageSizeType; //小图的尺寸
@property (nonatomic, assign) QMPreviewType previewType;
@property (nonatomic, strong) NSIndexPath *indexPath;

+ (PhotoPreviewView *)photoPreviewView;

- (void)show;

@end
