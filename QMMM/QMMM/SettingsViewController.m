//
//  SettingsViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-5.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsJumpCell.h"
#import "SettingsShowInfoCell.h"
#import "SettingsFooterView.h"
#import "VersionInfo.h"
#import "FeedbackViewController.h"
#import "SystemHandler.h"
#import "QMShareViewContrller.h"
#import "AppInfo.h"
#import <StoreKit/StoreKit.h>
#import "UserProtocolViewController.h"
#import <SDWebImage/SDImageCache.h>
#import "AppDelegate.h"
#import "ChatViewController.h"

@interface SettingsViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, SKStoreProductViewControllerDelegate, UIActionSheetDelegate>
{
    QMShareViewContrller *_shareViewController;
    __block VersionInfo *verInfo_;
}

@property (nonatomic, strong) UITableView *tableView;

@end

static const int kClearCacheBtnTag = 100;
static const int kLogoutBtnTag = 101;

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"设置";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    [self setupViews];
    
    //检测是否有新版本
//    __weak typeof(self) weakSelf = self;
//    [[SystemHandler sharedInstance] checkVersion:^(id obj) {
//        verInfo_ = (VersionInfo *)obj;
//        [weakSelf.tableView reloadData];
//    } failed:^(id obj) {
//
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)setupViews
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kMainTopHeight, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-kMainTopHeight) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kBorderColor;
    [_tableView registerNib:[SettingsShowInfoCell nib] forCellReuseIdentifier:kSettingsShowInfoCellIdentifier];
    [_tableView registerNib:[SettingsJumpCell nib] forCellReuseIdentifier:kSettingsJumpCellIdentifier];
    UINib *logoutNib = [UINib nibWithNibName:@"SettingsLogOutCell" bundle:nil];
    [_tableView registerNib:logoutNib forCellReuseIdentifier:@"kSettingsLogOutCellIdentifier"];
    
    SettingsFooterView *footerView = [SettingsFooterView footerView];
    footerView.translatesAutoresizingMaskIntoConstraints = NO;
    footerView.curVersion = [VersionInfo currentVer];
    [self.view addSubview:footerView];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:footerView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:footerView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:-12]];
    [self.view addSubview:_tableView];
}

#pragma mark - UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [SettingsJumpCell cellHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [SettingsJumpCell cellCount]+1;
    }
    else if (section == 1){
        return 1;
    }
    else if (section == 2){
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 3) {
            SettingsShowInfoCell *cell = (SettingsShowInfoCell *)[tableView dequeueReusableCellWithIdentifier:kSettingsShowInfoCellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.leftText = [[SettingsShowInfoCell items] objectAtIndex:0];
            cell.rightText = [NSString stringWithFormat:@"V%@", [VersionInfo currentVer]];
//            if (verInfo_ && [VersionInfo isHaveNewVer:verInfo_.version]) {
//                cell.isShowNewIcon = YES;
//            }
            //不需要显示new标识了
            cell.isShowNewIcon = NO;
            return cell;
        }
        else{
            SettingsJumpCell *cell = (SettingsJumpCell *)[tableView dequeueReusableCellWithIdentifier:kSettingsJumpCellIdentifier];
            [cell updateUI:indexPath.row];
            return cell;
        }
    }
    else if(indexPath.section == 1){
        SettingsShowInfoCell *cell = (SettingsShowInfoCell *)[tableView dequeueReusableCellWithIdentifier:kSettingsShowInfoCellIdentifier];
        cell.leftText = [[SettingsShowInfoCell items] objectAtIndex:1];
        cell.rightText = [self currentCache];
        cell.isShowNewIcon = NO;
        return cell;
    }
    else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"kSettingsLogOutCellIdentifier"];
        return cell;
    }
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            //意见反馈
//            FeedbackViewController *controller = [[FeedbackViewController alloc]init];
            ChatViewController *chatController  = [[ChatViewController alloc] initWithChatter:@"10000"];
            [self.navigationController pushViewController:chatController animated:YES];
        }
//        else if (indexPath.row == 4){
//            //推荐给朋友
//            [self shareAppToFriends];
//        }
        else if (indexPath.row == 1){
            //appstore评分
            [self gotoAppStore];
        }
        else if (indexPath.row == 2){
            //用户协议
            UserProtocolViewController *controller = [[UserProtocolViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (indexPath.row == 3){
            //检测当前的版本
            //[self checkVer];
        }
    }
    else if (indexPath.section == 1){
        //清除缓存
        [self showActionSheet:kClearCacheBtnTag buttonTitle:[AppUtils localizedPersonString:@"QM_Text_ClearCache"]];
    }
    else if (indexPath.section ==2){
        //退出登录
        [self showActionSheet:kLogoutBtnTag buttonTitle:[AppUtils localizedPersonString:@"QM_Text_Logout"]];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 12.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

#pragma mark - Methos
- (void)checkVer
{
    [AppUtils showProgressMessage:[AppUtils localizedCommonString:@"QM_HUD_Checking"]];
    [[SystemHandler sharedInstance] checkVersion:^(id obj) {
        [AppUtils dismissHUD];
        verInfo_ = (VersionInfo *)obj;
        BOOL haveNew = [VersionInfo isHaveNewVer:verInfo_.version];
        if (haveNew) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"检测到新版本V%@", verInfo_.version] message:verInfo_.desc delegate:self cancelButtonTitle:@"再看看" otherButtonTitles:@"马上更新", nil];
            [alertView show];
        }
        else{
            [AppUtils showAlertMessage:@"已是最新的版本"];
        }
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)shareAppToFriends
{
    if(nil == _shareViewController) {
        _shareViewController = [[QMShareViewContrller alloc] init];
    }
    AppInfo *appInfo = [AppInfo shareToWeiXinApp];
    [_shareViewController setAppInfo:appInfo];
    [_shareViewController show];
}

- (void)gotoAppStore
{
    SKStoreProductViewController *storeProductViewContorller = [[SKStoreProductViewController alloc]init];
    storeProductViewContorller.delegate = self;
    //加载一个新的视图展示
    [AppUtils showProgressMessage:@"请稍候"];
    [storeProductViewContorller loadProductWithParameters:
    //指定唯一APPID
    @{SKStoreProductParameterITunesItemIdentifier:kAppleID} completionBlock:^(BOOL result, NSError *error) {
        //block回调
        if(error){
            [AppUtils showErrorMessage:error.localizedDescription];
            NSLog(@"error %@ with userInfo %@",error,[error userInfo]);
        } else {
            [AppUtils dismissHUD];
            //模态弹出评分窗口
            [self presentViewController:storeProductViewContorller animated:YES completion:^{}];
        }
    }];
}

- (void)gotoSafari
{
    //跳转到safari
    NSString *url = verInfo_.downloadUrl;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

//单位M
- (NSString *)currentCache
{
    //图片的缓存
    NSUInteger imageCacheS = [[SDImageCache sharedImageCache]getSize];
    float imageM = imageCacheS/(1024.0f*1024.0f);
    //音频的大小
    float audioM = [self folderSizeAtPath:[BaseHandler audioPath]];
    return [NSString stringWithFormat:@"%.2fM", (imageM+audioM)];
}

- (void)clearCache
{
    //清除图片缓存
    [[SDImageCache sharedImageCache] clearDisk];
    NSFileManager *manager = [NSFileManager defaultManager];
    NSString *audioPath = [BaseHandler audioPath];
    if ([manager fileExistsAtPath:audioPath]){
        NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:audioPath] objectEnumerator];
        NSString *fileName;
        NSError *err = nil;
        while ((fileName=[childFilesEnumerator nextObject])) {
            NSString* fileAbsolutePath = [audioPath stringByAppendingPathComponent:fileName];
            [manager removeItemAtPath:fileAbsolutePath error:&err];
            if (err) {
                DLog(@"err:%@", err.description);
            }
        }
    }
    
    //刷新第二section
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:1];
    [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    [AppUtils showAlertMessage:@"缓存清除成功"];
}

//单个文件的大小
- (long long) fileSizeAtPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}
//遍历文件夹获得文件夹大小，返回多少M
- (float ) folderSizeAtPath:(NSString*) folderPath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:folderPath]) return 0;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString* fileName;
    long long folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
    }
    return folderSize/(1024.0*1024.0);
}

//弹出UIActionSheet
- (void)showActionSheet:(NSInteger)tag buttonTitle:(NSString *)title
{
    UIActionSheet *action = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:title otherButtonTitles:nil];
    action.tag = tag;
    [action showInView:self.view];
}

//退出登录
- (void)logout
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegate logout];
}

#pragma mark - StoreKitDelegate
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController{
    [viewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex) {
        [AppUtils trackCustomEvent:@"cancelVersionUpdate_event" args:nil];
        return ;
    }
    else{
        [AppUtils trackCustomEvent:@"click_versionUpdate_event" args:nil];
        [self gotoSafari];
    }
}

#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.cancelButtonIndex == buttonIndex) {
        return;
    }
    
    if (actionSheet.tag == kClearCacheBtnTag) {
        [self clearCache];
    }
    else if (actionSheet.tag == kLogoutBtnTag) {
        [self logout];
    }
}

@end
