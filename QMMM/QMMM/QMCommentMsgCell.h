//
//  QMCommentMsgCell.h
//  QMMM
//
//  Created by hanlu on 14-11-11.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class MsgData;

extern NSString * const kQMCommentMsgCellIdentifier;

@interface QMCommentMsgCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * nameLabel;

@property (nonatomic, strong) IBOutlet UILabel * timeLabel;

@property (nonatomic, strong) IBOutlet UILabel * descLabel;

@property (nonatomic, strong) IBOutlet UIImageView * goodsImageView;

+ (id)cellForCommentMsg;

//+ (CGFloat)heightForCommentMsg:(MsgData *)data;

- (void)updateUI:(MsgData *)data;

@end
