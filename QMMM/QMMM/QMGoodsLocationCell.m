//
//  QMGoodsLocationCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsLocationCell.h"
#import "GoodEntity.h"

NSString * const kGoodsLocationCellIdentifier = @"kGoodsLocationCellIdentifier";

@interface QMGoodsLocationCell (){
    BOOL _isPrev;
}

@property (weak, nonatomic) IBOutlet UILabel *locationLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

@end

@implementation QMGoodsLocationCell

+ (QMGoodsLocationCell *)locationCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"QMGoodsLocationCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    _isPrev=NO;
    self.locationLbl.text = @"";
    self.timeLbl.text = @"";
    self.locationLbl.textColor = UIColorFromRGB(0x99a0aa);
    self.timeLbl.textColor = UIColorFromRGB(0x99a0aa);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(GoodEntity *)goodsEntity
{
    if (goodsEntity) {
        NSString *str = @"火星";
        if (goodsEntity.city && ![goodsEntity.city isEqualToString:@""]) {
            str = goodsEntity.city;
        }
        self.locationLbl.text = str;
        if (!_isPrev) {
            //时间
            NSDate * date = [NSDate dateWithTimeIntervalSince1970:goodsEntity.pub_time];
            NSString * stringDate = [AppUtils stringFromDate:date];
            self.timeLbl.text = stringDate;
        }
    }
}

- (void)updateUI:(GoodEntity *)goodsEntity isPreView:(BOOL)isPrev
{
    _isPrev=YES;
    [self updateUI:goodsEntity];
}

+ (CGFloat)cellHeight
{
    return 32.f;
}

@end
