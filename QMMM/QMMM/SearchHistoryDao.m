//
//  SearchHistoryDao.m
//  QMMM
//
//  Created by kingnet  on 15-1-13.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "SearchHistoryDao.h"
#import "KeyWordEntity.h"

@implementation SearchHistoryDao

+ (SearchHistoryDao *)sharedInstance
{
    static SearchHistoryDao *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (NSMutableArray *)queryAll
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *selectSQL = [NSString stringWithFormat:@"SELECT * FROM %@ ORDER BY id DESC", kSearchHistory];
    FMResultSet *rs = [db executeQuery:selectSQL];
    NSMutableArray *array = [NSMutableArray array];
    while ([rs next]) {
        KeyWordEntity *entity = [[KeyWordEntity alloc]initWithFMResultSet:rs];
        [array addObject:entity];
    }
    return array;
}

- (BOOL)removeAll
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM %@", kSearchHistory];
    return [db executeUpdate:deleteSQL];
}

- (BOOL)insert:(BaseEntity *)entity
{
    KeyWordEntity *keyWord = (KeyWordEntity *)entity;
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO %@ (keyWord) VALUES (?)", kSearchHistory];
    return [db executeUpdate:insertSQL, keyWord.keyWord];
}

@end
