//
//  UserDefaultsUtils.m
//  QMMM
//
//  Created by kingnet  on 14-8-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "UserDefaultsUtils.h"

@implementation UserDefaultsUtils

+(void)saveValue:(id) value forKey:(NSString *)key{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:value forKey:key];
    [userDefaults synchronize];
}

+(id)valueWithKey:(NSString *)key{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:key];
}

+(BOOL)boolValueWithKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults boolForKey:key];
}

+(void)saveBoolValue:(BOOL)value withKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:value forKey:key];
    [userDefaults synchronize];
}

+(void)print{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dic = [userDefaults dictionaryRepresentation];
    DLog(@"%@",dic);
}

+(void)clearUserDefault{
    [[self class] saveBoolValue:NO withKey:IS_GIVE_COMMENTS];//清空弹窗评分标记
    
    //清空所有的key
    
    [[self class] saveValue:@"0" forKey:@"firstRecommendKey"];
    [[self class] saveValue:@"0" forKey:@"firstFriendKey"];
}

@end
