//
//  QMConfirmReceivingGoodsView.h
//  QMMM
//
//  Created by hanlu on 14-9-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPlaceHolderTextView.h"

@interface QMConfirmReceivingGoodsView : UIView <UITextViewDelegate>

@property (nonatomic, strong) IBOutlet UIButton * goodBtn;

@property (nonatomic, strong) IBOutlet UIButton * normalBtn;

@property (nonatomic, strong) IBOutlet UIButton * badBtn;

@property (nonatomic, strong) IBOutlet UIPlaceHolderTextView * commentTextView;

@property (nonatomic, strong) IBOutlet UIButton * confirmBtn;

@property (nonatomic, assign) int selectIndex;

@end
