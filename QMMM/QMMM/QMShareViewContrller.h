//
//  QMShareViewContrller.h
//  QMMM
//
//  Created by hanlu on 14-9-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@class GoodEntity, AppInfo;

@protocol QMShareViewContrllerDelegate <NSObject>

- (NSString *)shareJsonStr:(NSInteger)index; //获得分享用的参数

@end
@interface QMShareViewContrller : NSObject

@property (nonatomic, weak) id<QMShareViewContrllerDelegate> delegate;

- (void)setGoodsInfo:(GoodEntity *)goodsInfo;

//分享此App
- (void)setAppInfo:(AppInfo *)appInfp;

- (void)show;


-(id)initWithShareMore;

@end
