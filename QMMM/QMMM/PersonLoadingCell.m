//
//  PersonLoadingCell.m
//  QMMM
//
//  Created by kingnet  on 15-1-26.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "PersonLoadingCell.h"

@interface PersonLoadingCell ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@end

@implementation PersonLoadingCell

+ (PersonLoadingCell *)loadingCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"PersonLoadingCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)startAnimate
{
    [self.activityView startAnimating];
}

- (void)stopAnimate
{
    [self.activityView stopAnimating];
}

@end
