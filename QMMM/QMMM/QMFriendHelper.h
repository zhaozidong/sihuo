//
//  QMChatUserHelper.h
//  QMMM
//
//  Created by hanlu on 14-10-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@class FriendInfo;
@class FMDatabase;

@interface QMFriendHelper : NSObject {
    NSThread * _workThread;
}

//数据库工作线程
- (void)setWorkThread:(NSThread *)aThread;

- (NSArray *)loadFriendList:(FMDatabase *)database;
- (void)addFriendList:(FMDatabase *)database list:(NSArray *)userList;
- (void)updateFriend:(FMDatabase *)database userInfo:(FriendInfo *)userInfo;
- (void)deleteFriend:(FMDatabase *)database userId:(uint64_t)userId;
- (void)deleteAllFriend:(FMDatabase *)database;

- (void)asyncLoadFriendList:(contextBlock)context;
- (void)asyncAddFriendList:(contextBlock)context list:(NSArray *)userList;
- (void)asyncUpdateFriend:(contextBlock)context userInfo:(FriendInfo *)userInfo;
- (void)asyncDeleteFriend:(contextBlock)context userId:(uint64_t)userId;
- (void)asyncDeleteAllFriend:(contextBlock)context;

@end
