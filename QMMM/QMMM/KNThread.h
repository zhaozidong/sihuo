//
//  KNThread.h
//  QMMM
//
//  Created by 韩芦 on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KNThreadDelegate;

/**
	线程包装类，增加delegate协作对象等功能
 */
@interface KNThread : NSThread {
    id delegate;
}

/**
	初始化方法
	@param anDelegate delegate协作对象
	@returns TTThread对象
 */
- (id)initWithDelegate:(id<KNThreadDelegate>)anDelegate;

/**
	获取delegate协作对象
	@returns delegate对象
 */
- (id<KNThreadDelegate>)delegate;
/**
	设置delegate协作对象，注意：delegate对象没有retain
	@param anDelegate delegate协作对象
 */
- (void)setDelegate:(id<KNThreadDelegate>)anDelegate;

/**
	杀掉线程，注意：一般应该在deallo之前调用kill，否则可能无法正常结束
	@param waitDone 是否等待线程被杀死才返回
 */
- (void)kill:(BOOL)waitDone;

@end

/**
	线程协作协议
 */
@protocol KNThreadDelegate
@optional
/**
	线程启动了（线程中运行）
	@param thread KNThread对象
 */
- (void)threadStart:(KNThread *)thread;
/**
	线程即将停止（线程中运行）
	@param thread KNThread对象
 */
- (void)threadStop:(KNThread *)thread;
@end