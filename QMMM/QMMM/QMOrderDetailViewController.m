//
//  QMOrderDetailViewController.m
//  QMMM
//
//  Created by hanlu on 14-9-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMOrderDetailViewController.h"

#import "GoodsHandler.h"
#import "OrderDataInfo.h"
#import "QMSelectExpressViewController.h"
#import "QMConfirmReceivingGoodsViewController.h"
#import "QMOrderDetailPayAmountCell.h"
#import "ChatViewController.h"
#import "QMCheckExpressViewController.h"
#import "UserInfoHandler.h"
#import "UserEntity.h"

#import "QMOrderCommentsCells.h"
#import "OrderDetailAddressCell.h"
#import "OrderDetailContactCell.h"
#import "OrderDetailExpressCell.h"
#import "OrderDetailGoodsDescCell.h"
#import "OrderDetailPriceCell.h"
#import "OrderDetailStatusCell.h"
#import "OrderTimeCell.h"
#import "OrderDetailExpressActionCell.h"
#import "OrderDetailTimerCell.h"
#import "OrderDetailBottomView.h"
#import "OrderDetailRefundForBuyerCell.h"
#import "OrderDetailRefundForSellerCell.h"

#import "QMPayOrderViewController.h"
#import "QMModifyPriceViewController.h"

#import "GoodsDetailViewController.h"

#import "QMFailPrompt.h"

#import "MsgHandle.h"
#import "GoodEntity.h"

#import "UserRightsManager.h"
#import "UserDefaultsUtils.h"
#import "OrderDetailFooterView.h"
#import "ChatViewController.h"
#import "QMInputAlert.h"
#import "QMQAViewController.h"

#import "UIScrollView+UzysCircularProgressPullToRefresh.h"


@interface QMOrderDetailViewController () <UITableViewDelegate, UITableViewDataSource, OrderDetailCellDelegate, UIAlertViewDelegate, QMFailPromptDelegate, OrderDetailFooterViewDelegate, QMInputAlertDelegate> {
    BOOL _bSeller;
    NSString * _orderId;
    __block OrderDataInfo * _detailInfo;
    
    NSMutableDictionary* _expressDict;
}

@property (nonatomic, strong) QMOrderCommentsCells *commentsCells;

@property (nonatomic, strong) UITableViewCell *commentDelCell;

@property (nonatomic, strong) OrderDetailBottomView *bottomView;

@property (nonatomic, strong) OrderDetailAddressCell *addressCell;

@property (nonatomic, strong) OrderDetailRefundForBuyerCell *refundForBuyerCell;

@property (nonatomic, strong) OrderDetailRefundForSellerCell *refundForSellerCell;

@property (nonatomic, strong) QMFailPrompt *failPrompt;

@property (nonatomic, strong) OrderDetailFooterView *footerView;

@end

@implementation QMOrderDetailViewController

- (id)initWithData:(BOOL)bseller orderId:(NSString *)orderId
{
    self = [super initWithNibName:@"QMOrderDetailViewController" bundle:nil];
    if(self) {
        _orderId = orderId;
        _bSeller = bseller;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)initNavBar
{
    self.title = [AppUtils localizedProductString:@"QM_Text_Order_Detail"];
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(btnBackClick:)];
}

- (void)initExpressDict
{
    _expressDict = [[NSMutableDictionary alloc] init];
    
    NSString * filePath = [[NSBundle mainBundle] pathForResource:@"express" ofType:@"plist"];
    NSDictionary * dict = [NSDictionary dictionaryWithContentsOfFile:filePath];
    for (int i = 1; i <= [dict count]; i ++) {
        NSString * key = [NSString stringWithFormat:@"Item%d", i];
        NSDictionary * temp = [dict objectForKey:key];
        NSString * code = [temp objectForKey:@"code"];
        [_expressDict setObject:temp forKey:code]; //以快递公司名称的拼音为Key
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initNavBar];
    
    [self initExpressDict];
    
    self.view.backgroundColor = kBackgroundColor;
    
    _tableView.hidden = YES;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kBorderColor;
    [_tableView registerNib:[OrderDetailContactCell nib] forCellReuseIdentifier:kOrderDetailContactCellIdentifier];
    [_tableView registerNib:[OrderDetailExpressActionCell nib] forCellReuseIdentifier:kOrderDetailExpressActionCellIdentifier];
    [_tableView registerNib:[OrderDetailExpressCell nib] forCellReuseIdentifier:kOrderDetailExpressCellIdentifier];
    [_tableView registerNib:[OrderDetailGoodsDescCell nib] forCellReuseIdentifier:kOrderDetailGoodsDescCellIdentifier];
    [_tableView registerNib:[OrderTimeCell nib] forCellReuseIdentifier:kOrderTimeCellIdentifier];
    [_tableView registerNib:[OrderDetailPriceCell nib] forCellReuseIdentifier:kOrderDetailPriceCellIdentifier];
    [_tableView registerNib:[OrderDetailStatusCell nib] forCellReuseIdentifier:kOrderDetailStatusCellIdentifier];
    [_tableView registerNib:[OrderDetailTimerCell nib] forCellReuseIdentifier:kOrderDetailTimerCellIdentifier];
    
    _tableView.tableFooterView = self.footerView;
    
    [self setupRefresh];
    
    //注册通知
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processUpdateOrderStatusNtf:) name:kUpdateOrderStatusNotify object:nil];
    [nc addObserver:self selector:@selector(processUpdateOrderPriceNtf:) name:kUpdateOrderPriceNotify object:nil];
    [nc addObserver:self selector:@selector(processShowComments:) name:kShowCommentsNotify object:nil];
    
    [AppUtils showProgressMessage:@"加载中"];
    [self getOrderDetail];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    _tableView.delegate = nil;
    _tableView.dataSource = nil;
    _tableView = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //如果状态是付款成功
    if (_detailInfo.order_status == 2) {
        [[UserRightsManager sharedInstance] showNotifyTip:self];
    }
//    DLog(@"viewDidAppear Retain count is %ld", CFGetRetainCount((__bridge CFTypeRef)self));
}

#pragma mark - Pull Refresh

/**
 *  集成刷新控件
 */
- (void)setupRefresh
{
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshActionHandler:^{
        [weakSelf getOrderDetail];
    }];
}

- (void)getOrderDetail
{
    uint type = _bSeller ? 1 : 2;
    __weak typeof(self) weakSelf = self;
    [[GoodsHandler shareGoodsHandler] getOrderDetail:type orderId:_orderId success:^(id result) {
        [AppUtils dismissHUD];
        OrderDataInfo *orderInfo = (OrderDataInfo *)result;
        //如果订单状态有改变则通知列表刷新
        if (_detailInfo && _detailInfo.order_status!=orderInfo.order_status) {
            NSDictionary * dict = @{@"orderId":_orderId, @"status":@(orderInfo.order_status)};
            [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateOrderListStatusNotify object:nil userInfo:dict];
        }
        @synchronized(_detailInfo) {
            _detailInfo = result;
        }
        [weakSelf reloadView];
    } failed:^(id result) {
        if(result && [result isKindOfClass:[NSString class]]) {
            [AppUtils showErrorMessage:result];
        } else {
            [AppUtils showErrorMessage:@"获取订单详情失败"];
        }
        if (_tableView.hidden) {
            [AppUtils dismissHUD];
            [weakSelf loadOrderDetailFailed:result];
        }
    }];
}

- (void)loadOrderDetailFailed:(NSString *)tip
{
    //拿到错误代码
    NSRange range = [tip rangeOfString:@"(" options:NSBackwardsSearch];
    if (range.location != NSNotFound) {
        NSString *str = [tip substringFromIndex:range.location+1];
        NSString *str1 = [str substringToIndex:str.length-1];
        if ([str1 intValue] == 2113) {//订单被删除了
            self.failPrompt =[[QMFailPrompt alloc] initWithFailType:QMFailTypeLoadFail labelText:@" 该订单已被删除" buttonType:QMFailButtonTypeNone buttonText:nil ];
            self.failPrompt.frame = CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
            [self.view addSubview:self.failPrompt];
        }
    }
    //超时或异常
    else{
        self.failPrompt =[[QMFailPrompt alloc] initWithFailType:QMFailTypeLoadFail labelText:@"哎呦，加载失败了" buttonType:QMFailButtonTypeFail buttonText:nil];
        self.failPrompt.delegate = self;
        self.failPrompt.frame = CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
        [self.view addSubview:self.failPrompt];
    }
}

- (void)placeHolderButtonClick:(id)sender
{
    [self.failPrompt removeFromSuperview];
    self.failPrompt = nil;
    [AppUtils showProgressMessage:@"加载中"];
    [self getOrderDetail];
}

- (void)reloadView
{
    if ([self.tableView showPullToRefresh]) {
        [self.tableView stopRefreshAnimation];
    }
    
    if (self.failPrompt || self.failPrompt.superview) {
        [self.failPrompt removeFromSuperview];
        self.failPrompt = nil;
    }
    
    if (_detailInfo.order_status != QMOrderStatusClosed && _detailInfo.order_status != QMOrderStatusTimeout && _detailInfo.order_status != QMOrderStatusFinished && _detailInfo.order_status != QMOrderStatusRefundClosed) {//交易关闭与成功时没有
        /************配置底部的button***************/
        if ((!_bSeller && (_detailInfo.order_status == QMOrderStatusDelivery)) || (_bSeller && (_detailInfo.order_status == QMOrderStatusPaid)) || (_detailInfo.order_status == QMOrderStatusWaitPay)) {//卖家已经发货 或 买家已经付款
            if(_bottomView == nil || _bottomView.superview == nil){
                UIEdgeInsets insets = _tableView.contentInset;
                _tableView.contentInset = UIEdgeInsetsMake(insets.top, 0, [OrderDetailBottomView viewHeight], 0);
                _bottomView = [OrderDetailBottomView bottomView];
                _bottomView.frame = CGRectMake(0, CGRectGetMaxY(_tableView.frame)-[OrderDetailBottomView viewHeight], CGRectGetWidth(self.view.frame), [OrderDetailBottomView viewHeight]);
                _bottomView.delegate = self;
                [self.view addSubview:_bottomView];
            }
            [_bottomView updateUI:_detailInfo bSeller:_bSeller];
        }
        else{
            if (nil != _bottomView.superview) {
                [_bottomView removeFromSuperview];
                _bottomView = nil;
                UIEdgeInsets insets = _tableView.contentInset;
                _tableView.contentInset = UIEdgeInsetsMake(insets.top, 0, 0, 0);
            }
        }
        
        /******************配置导航栏上的button************/
        if (_detailInfo.order_status == QMOrderStatusWaitPay) { //正在等待付款
            if (!_bSeller) {
                self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(cancelOrder:) title:@"关闭订单" size:CGSizeMake(80.f, 44.f) imageName:nil];
            }
        }
        else if (_detailInfo.order_status == QMOrderStatusPaid || _detailInfo.order_status == QMOrderStatusDelivery){
            if (_bSeller) {//卖家在已付款和已发货时可以关闭订单
                self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(sellerCancelOrder:) title:@"关闭订单" size:CGSizeMake(80.f, 44.f) imageName:nil];
            }
            else{//买家在已付款和已发货时可以申请退款
                self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(refundAction:) title:@"申请退款" size:CGSizeMake(80.f, 44.f) imageName:nil];
            }
        }
        else{
            self.navigationItem.rightBarButtonItems = nil;
        }
    }
    else{
        self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(deleteOrder:) title:@"删除订单" size:CGSizeMake(80.f, 44.f) imageName:nil];
        if (nil != _bottomView.superview) {
            [_bottomView removeFromSuperview];
            _bottomView = nil;
            UIEdgeInsets insets = _tableView.contentInset;
            _tableView.contentInset = UIEdgeInsetsMake(insets.top, 0, 0, 0);
        }
    }
    
    [_tableView reloadData];
    _tableView.hidden = NO;
}

- (QMOrderCommentsCells *)commentsCells
{
    if (!_commentsCells) {
        _commentsCells = [[QMOrderCommentsCells alloc]init];
        _commentDelCell = (UITableViewCell *)_commentsCells.cellsArr[1];
        _commentDelCell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _commentDelCell.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(_commentDelCell.frame));
    }
    return _commentsCells;
}

- (OrderDetailAddressCell *)addressCell
{
    if (!_addressCell) {
        _addressCell = [OrderDetailAddressCell addressCell];
        _addressCell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _addressCell.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(_addressCell.frame));
    }
    return _addressCell;
}

- (OrderDetailRefundForBuyerCell *)refundForBuyerCell
{
    if (!_refundForBuyerCell) {
        _refundForBuyerCell = [OrderDetailRefundForBuyerCell refundForBuyerCell];
        _refundForBuyerCell.delegate = self;
    }
    return _refundForBuyerCell;
}

- (OrderDetailRefundForSellerCell *)refundForSellerCell
{
    if (!_refundForSellerCell) {
        _refundForSellerCell = [OrderDetailRefundForSellerCell refundForSellderCell];
        _refundForSellerCell.delegate = self;
    }
    return _refundForSellerCell;
}

- (OrderDetailFooterView *)footerView
{
    if (!_footerView) {
        _footerView = [OrderDetailFooterView footerView];
        _footerView.frame = CGRectMake(0, 0, CGRectGetWidth(_tableView.frame), [OrderDetailFooterView viewHeight]);
        _footerView.delegate = self;
    }
    return _footerView;
}

- (void)btnBackClick:(id)sender
{
    [AppUtils dismissHUD];

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)ctrlBtnClick:(id)sender
{
    if(_bSeller) {
        QMSelectExpressViewController * viewController = [[QMSelectExpressViewController alloc] initWithOrderId:_detailInfo.orderId];

        NSArray * array = [self.navigationController viewControllers];
        [viewController setPopViewController:array[[array count] - 2]];
        
        [self.navigationController pushViewController:viewController animated:YES];
    } else {
        QMConfirmReceivingGoodsViewController * viewController = [[QMConfirmReceivingGoodsViewController alloc] initWithOrderInfo:_detailInfo];
        
        NSArray * array = [self.navigationController viewControllers];
        [viewController setPopViewController:array[[array count] - 2]];
        
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void)deleteOrder:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"你确定要删除此笔订单么？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag = 10000;
    [alertView show];
}

- (void)cancelOrder:(id)sender
{
    [AppUtils trackCustomEvent:@"click_cancelOrder_event" args:nil];
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"你确定要取消此笔订单么？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag = 10001;
    [alertView show];
}

//卖家点击关闭订单
- (void)sellerCancelOrder:(id)sender
{
    [self refundAlert:10004];
}

//申请退款
- (void)refundAction:(id)sender
{
    [QMInputAlert showWithPlaceholder:[NSString stringWithFormat:@"最多退款%.2f元", _detailInfo.payAmount] alertType:QMInputAlertTypeFloat delegate:self];
}

- (void)processUpdateOrderStatusNtf:(NSNotification *)nc
{
    self.navigationItem.rightBarButtonItems = nil;
    
    NSString *oid = [nc.userInfo objectForKey:@"orderId"];
    if ([oid isEqualToString:_orderId]) {
        //重新拉取数据
        [self getOrderDetail];
    }
}

- (void)processUpdateOrderPriceNtf:(NSNotification *)nc
{
    //修改了订单价格
    NSDictionary *userInfo = nc.userInfo;
    float price = [userInfo[@"finalPrice"] floatValue];
    _detailInfo.payAmount = price;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:2];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [_bottomView updateUI:_detailInfo bSeller:_bSeller];
}

-(void)processShowComments:(NSNotification *)notification{
    if (![UserDefaultsUtils boolValueWithKey:IS_GIVE_COMMENTS]) {
        [UserDefaultsUtils saveBoolValue:YES withKey:IS_GIVE_COMMENTS];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"您的第一笔交易已经完成啦!求赏个五星好评吧，咱给程序员买点肉。" delegate:self cancelButtonTitle:@"残忍拒绝" otherButtonTitles:@"马上好评",@"我要吐槽", nil];
        alert.tag=10002;
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex == buttonIndex) {
        return;
    }
    if (alertView.tag == 10000) {
        //删除订单
        int type = _bSeller?1:2;
        [AppUtils showProgressMessage:@"处理中"];
        [[GoodsHandler shareGoodsHandler]deleteOrder:_detailInfo.orderId type:type success:^(id obj) {
            [AppUtils showSuccessMessage:@"订单已删除"];
            
            [self.navigationController popViewControllerAnimated:YES];
            
        } failed:^(id obj) {
            [AppUtils showErrorMessage:(NSString *)obj];
        }];
    }
    else if (alertView.tag == 10001){
        //取消订单
        [AppUtils showProgressMessage:@"处理中"];
        [[GoodsHandler shareGoodsHandler]cancelOrder:_detailInfo.orderId success:^(id obj) {
            [AppUtils showSuccessMessage:@"订单已取消"];
        } failed:^(id obj) {
            [AppUtils showErrorMessage:(NSString *)obj];
        }];
    }else if(alertView.tag ==10002){
        if(buttonIndex==1){//马上好评
            NSString *strURL=@"https://itunes.apple.com/cn/app/si-huo-hao-you-jian-xian-zhi/id955693333?l=en&mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strURL]];
        }else if (buttonIndex==2){//立即吐槽
            ChatViewController *chatController  = [[ChatViewController alloc] initWithChatter:@"10000"];
            [self.navigationController pushViewController:chatController animated:YES];
        }
    }
    else if (alertView.tag == 10003){
        //同意退款
        [AppUtils showProgressMessage:@"处理中"];
        __weak typeof(self) weakSelf = self;
        [[GoodsHandler shareGoodsHandler]acceptRefund:_detailInfo.orderId success:^(id obj) {
            [AppUtils showAlertMessage:@"退款成功"];
            [weakSelf getOrderDetail];
        } failed:^(id obj) {
            [AppUtils dismissHUD];
            [AppUtils showAlertMessage:(NSString *)obj];
        }];
    }
    else if (alertView.tag == 10004){
        //卖家点击关闭订单
        [AppUtils showProgressMessage:@"处理中"];
        __weak typeof(self) weakSelf = self;
        [[GoodsHandler shareGoodsHandler]sellerRefund:_detailInfo.orderId success:^(id obj) {
            [AppUtils dismissHUD];
            [weakSelf getOrderDetail];
        } failed:^(id obj) {
            [AppUtils dismissHUD];
            [AppUtils showAlertMessage:(NSString *)obj];
        }];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    if(_detailInfo) {
        return 6;
    } else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(0 == section) {
        return 1;
    } else if(2 == section) {
        return 3;
    } else if(3 == section) {
        //判断是否有查看物流的按钮
        if (_detailInfo) {
            if (_detailInfo.express_num.length > 0) {
                return 3;
            }
            else{
                return 2;
            }
        }
        else{
            return 0;
        }
    } else if(4 == section) { //是否有评价
        if (_detailInfo && _detailInfo.order_status == QMOrderStatusFinished) {
            return 2;
        }
        return 0;
    } else if (5 == section){
        return 1;
    } else if (1 == section){
        //拒绝退款
        if (_detailInfo && _detailInfo.order_status == QMOrderStatusRefundReject) {
            return 1;
        }
        //申请退款中
        else if (_detailInfo && _detailInfo.order_status == QMOrderStatusRefundApply){
            return 1;
        }
        return 0;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (0 == indexPath.section) {
        if (_detailInfo.order_status == QMOrderStatusFinished) {//只有在成功时没有
            OrderDetailStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:kOrderDetailStatusCellIdentifier];
            [cell updateUI:_detailInfo];
            return cell;
        }
        else{
            OrderDetailTimerCell *cell = [tableView dequeueReusableCellWithIdentifier:kOrderDetailTimerCellIdentifier];
            cell.delegate = self;
            [cell updateUI:_detailInfo];
            return cell;
        }
    }
    else if (1 == indexPath.section){
        //拒绝退款
        if (_detailInfo && _detailInfo.order_status == QMOrderStatusRefundReject) {
            [self.refundForBuyerCell updateUI:_detailInfo];
            return self.refundForBuyerCell;
        }
        //申请退款中
        else if (_detailInfo && _detailInfo.order_status == QMOrderStatusRefundApply){
            if (_bSeller) {
                [self.refundForSellerCell updateUI:_detailInfo];
                return self.refundForSellerCell;
            }
            else{
                [self.refundForBuyerCell updateUI:_detailInfo];
                return self.refundForBuyerCell;
            }
        }
    }
    else if (2 == indexPath.section){
        if (0 == indexPath.row) {
            OrderTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:kOrderTimeCellIdentifier];
            OrderSimpleInfo *info = [[OrderSimpleInfo alloc]init];
            info.order_id = _detailInfo.orderId;
            info.order_pub_time = _detailInfo.order_time;
            [cell updateUI:info];
            return cell;
        }
        else if (1 == indexPath.row){
            OrderDetailGoodsDescCell *cell = [tableView dequeueReusableCellWithIdentifier:kOrderDetailGoodsDescCellIdentifier];
            [cell updateUI:_detailInfo];
            return cell;
        }
        else if (2 == indexPath.row){
            OrderDetailPriceCell *cell = [tableView dequeueReusableCellWithIdentifier:kOrderDetailPriceCellIdentifier];
            [cell updateUI:_detailInfo];
            return cell;
        }
    }
    else if (3 == indexPath.section){
        if (0 == indexPath.row) {
            OrderDetailExpressCell *cell = [tableView dequeueReusableCellWithIdentifier:kOrderDetailExpressCellIdentifier];
            [cell updateUI:_detailInfo];
            return cell;
        }
        else if (1 == indexPath.row){
            [self.addressCell updateUI:_detailInfo];
            return self.addressCell;
        }
        else if (2 == indexPath.row){
            OrderDetailExpressActionCell *cell = [tableView dequeueReusableCellWithIdentifier:kOrderDetailExpressActionCellIdentifier];
            cell.delegate = self;
            return cell;
        }
    }
    else if (4 == indexPath.section){
        UITableViewCell *cell = self.commentsCells.cellsArr[indexPath.row];
        [self.commentsCells updateUI:_detailInfo];
        return cell;
    }
    else if (5 == indexPath.section){
        OrderDetailContactCell *cell = [tableView dequeueReusableCellWithIdentifier:kOrderDetailContactCellIdentifier];
        cell.delegate = self;
        [cell updateUI:_detailInfo bSeller:_bSeller];
        return cell;
    }
    return nil;
}

#pragma mark - UITableViewDeletegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(0 == indexPath.section) {
        if (_detailInfo.order_status == QMOrderStatusFinished) {
            return [OrderDetailStatusCell cellHeight];
        }
        else{
            return [OrderDetailTimerCell cellHeight];
        }
    }
    else if (1 == indexPath.section) {
        //拒绝退款
        if (_detailInfo && _detailInfo.order_status == QMOrderStatusRefundReject) {
            [self.refundForBuyerCell updateUI:_detailInfo];
            [self.refundForBuyerCell setNeedsLayout];
            [self.refundForBuyerCell layoutIfNeeded];
            CGFloat height = [self.refundForBuyerCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            height += 1;
            return height;
        }
        //卖家收到买家拒绝退款时
        else if (_detailInfo && _detailInfo.order_status == QMOrderStatusRefundApply){
            if (_bSeller) {
                [self.refundForSellerCell updateUI:_detailInfo];
                [self.refundForSellerCell setNeedsDisplay];
                [self.refundForSellerCell layoutIfNeeded];
                CGFloat height = [self.refundForSellerCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
                height += 1;
                return height;
            }
            else{
                [self.refundForBuyerCell updateUI:_detailInfo];
                [self.refundForBuyerCell setNeedsLayout];
                [self.refundForBuyerCell layoutIfNeeded];
                CGFloat height = [self.refundForBuyerCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
                height += 1;
                return height;
            }
        }
    }
    
    else if(2 == indexPath.section) {
        if (0 == indexPath.row) {
            return [OrderTimeCell cellHeight];
        }
        else if (1 == indexPath.row){
            return [OrderDetailGoodsDescCell cellHeight];
        }
        else if (2 == indexPath.row){
            return [OrderDetailPriceCell cellHeight];
        }
    } else if(3 == indexPath.section) {
        if(0 == indexPath.row) {
            return [OrderDetailExpressCell cellHeight];
        } else if(1 == indexPath.row) {
            [self.addressCell updateUI:_detailInfo];
            [self.addressCell setNeedsDisplay];
            [self.addressCell layoutIfNeeded];
            CGFloat height = [self.addressCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            height += 1;
            return height;
        } else if(2 == indexPath.row) {
            return [OrderDetailExpressActionCell cellHeight];
        }
    } else if(5 == indexPath.section) {
        return [OrderDetailContactCell cellHeight];
    }
    else if (4 == indexPath.section){
        if (0 == indexPath.row) {
            return 30.f;
        }
        else if (1 == indexPath.row){
            [self.commentsCells updateUI:_detailInfo];
            [self.commentDelCell setNeedsLayout];
            [self.commentDelCell layoutIfNeeded];
            CGFloat height = [self.commentDelCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            height += 1;
            return height;
        }
    }
    
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 4) {
        if (_detailInfo && _detailInfo.order_status == 4) {
            return 14.f;
        }
        else{
            return CGFLOAT_MIN;
        }
    }
    else if (section == 5){
        return CGFLOAT_MIN;
    }
    else if (section == 1){
        if (_detailInfo && (_detailInfo.order_status == QMOrderStatusRefundReject || _detailInfo.order_status == QMOrderStatusRefundApply)) {
            return 14.f;
        }
        else{
            return CGFLOAT_MIN;
        }
    }
    return 14;
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2) {
        GoodsDetailViewController *controller = [[GoodsDetailViewController alloc]initWithGoodsId:_detailInfo.goodsId];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - OrderDetailCellDelegate
- (void)didTapCallExpress
{
    if(_detailInfo && [_detailInfo.express_co length] > 0) {
        NSDictionary * dict = [_expressDict objectForKey:_detailInfo.express_co];
        NSString * phoneNum = [dict objectForKey:@"phoneNum"];
        if([phoneNum length] > 0) {
            [self call:phoneNum];
        }
        else{
            [AppUtils showAlertMessage:@"未查到该物流的电话"];
        }
    }
}

- (void)didTapCheckExpress
{
    if([_detailInfo.express_co length] > 0 && [_detailInfo.express_num length] > 0) {
        QMCheckExpressViewController * viewController = [[QMCheckExpressViewController alloc] initWithExpressInfo:_detailInfo.express_co expressNum:_detailInfo.express_num orderId:_detailInfo.orderId];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else{
        [AppUtils showAlertMessage:[NSString stringWithFormat:[AppUtils localizedPersonString:@"QM_Alert_CheckExpressInfo_Error"], _detailInfo.express_co, _detailInfo.express_num]];
    }
}

- (void)didTapContactSeller
{
    if(_detailInfo && _detailInfo.user_Id) {
        
        //联系买家与卖家统计
        if(_bSeller) {
            [AppUtils trackCustomEvent:@"chat_buyer_event" args:nil];
        } else {
            [AppUtils trackCustomEvent:@"chat_seller_event" args:nil];
        }
        
        NSString *title = [NSString stringWithFormat:@"%qu", _detailInfo.user_Id];
        GoodEntity *entity = [[GoodEntity alloc]init];
        entity.gid = _detailInfo.goodsId;
        entity.desc = _detailInfo.goodsDesc;
        GoodsPictureList *picList = [[GoodsPictureList alloc]initWithArray:[NSArray arrayWithObject:_detailInfo.goodsImage]];
        entity.picturelist = picList;
        entity.price = _detailInfo.goodsPrice;
        //模拟一条消息写到消息数据库
        [[MsgHandle shareMsgHandler] sendGoodsPreviewMsg:title goodsInfo:entity];
        
        if([title length] > 0 && ![title isEqualToString:[UserInfoHandler sharedInstance].currentUser.uid]) {
            ChatViewController *chatController = [[ChatViewController alloc] initWithChatter:title];
            [self.navigationController pushViewController:chatController animated:YES];
        }
    }
}

- (void)call:(NSString *)phoneNum
{
    [AppUtils systemCall:phoneNum];
}

- (void)didTapCallSeller
{
    if (_detailInfo.mobile.length > 0) {
        [AppUtils systemCall:_detailInfo.mobile];
    }
    else{
        NSString *side = (_bSeller?@"买家":@"卖家");
        [AppUtils showAlertMessage:[NSString stringWithFormat:@"%@没有留电话", side]];
    }
}

- (void)didTapConfirmReceive
{
    QMConfirmReceivingGoodsViewController * viewController = [[QMConfirmReceivingGoodsViewController alloc] initWithOrderInfo:_detailInfo];
    
    NSArray * array = [self.navigationController viewControllers];
    [viewController setPopViewController:array[[array count] - 2]];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didTapModifyPrice
{
    //修改价格
//    __weak __typeof(self)weakSelf = self;
    QMModifyPriceViewController *modifyPrice=[[QMModifyPriceViewController alloc] initWithNibName:@"QMModifyPriceViewController" bundle:nil];
    
//    [modifyPrice setOrderId:_orderId totalPay:_detailInfo.payAmount completeBlock:^(float finalPrice) {
//        [AppUtils showProgressMessage:@"加载中"];
//        [weakSelf getOrderDetail];
//    }];
    [modifyPrice setOrderId:_orderId totalPay:_detailInfo.payAmount completeBlock:nil];
    [self.navigationController pushViewController:modifyPrice animated:YES];

}

- (void)didTapDeliver
{
    QMSelectExpressViewController * viewController = [[QMSelectExpressViewController alloc] initWithOrderId:_detailInfo.orderId];
    
    NSArray * array = [self.navigationController viewControllers];
    [viewController setPopViewController:array[[array count] - 2]];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didTapPay
{
    [AppUtils trackCustomEvent:@"click_payButton_event" args:nil];
    //付款
    if(_detailInfo){

        QMPayOrderViewController   *payOrder=[[QMPayOrderViewController alloc] initWithNibName:@"QMPayOrderViewController" bundle:nil];
        [payOrder setOrderId:_orderId totalPay:_detailInfo.payAmount];

        [self.navigationController pushViewController:payOrder animated:YES];
        payOrder = nil;
    }

}

- (void)payTimeIsUp
{
    //底部付款按钮不可用
    self.bottomView.canPay = NO;
    
    //删除订单
    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(deleteOrder:) title:@"删除订单" size:CGSizeMake(80.f, 44.f) imageName:nil];
}

- (void)didTapContactService
{
    ChatViewController *chatController  = [[ChatViewController alloc] initWithChatter:@"10000"];
    [self.navigationController pushViewController:chatController animated:YES];
}

- (void)didTapRejectRefund
{
    [QMInputAlert showWithPlaceholder:@"请输入拒绝理由" alertType:QMInputAlertTypeText delegate:self];
}

- (void)didTapAgreeRefund
{
    [self refundAlert:10003];
}

- (void)refundAlert:(NSInteger)tag
{
    float money = (tag==10003)?_detailInfo.refund_amount:_detailInfo.payAmount;
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"点击\"确定\"后会将%.2f元直接汇入买家的账户余额中", money] message:@"如有退货请你收到货后再点击\"确定\"，否则可能钱货两空。" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag = tag;
    [alertView show];
}

#pragma mark - FooterViewDelegate
- (void)didTapGuarantBtn
{
    //跳转到QA界面
    QMQAViewController *controller = [[QMQAViewController alloc]initWithType:QMQATypeHowToBuy];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - QMInputAlertDelegate
- (BOOL)okWithInputStr:(NSString *)str inputAlert:(QMInputAlert *)inputAlert
{
    NSString *temp = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (temp.length > 0) {
        if (QMInputAlertTypeText == inputAlert.alertType) {
            __weak typeof(self) weakSelf = self;
            [AppUtils showProgressMessage:@"处理中"];
            [[GoodsHandler shareGoodsHandler]rejectRefund:_detailInfo.orderId reason:str success:^(id obj) {
                [AppUtils dismissHUD];
                [weakSelf getOrderDetail];
            } failed:^(id obj) {
                [AppUtils dismissHUD];
                [AppUtils showAlertMessage:(NSString *)obj];
            }];
        }
        else{
            float money = [str floatValue];
            if (money > _detailInfo.payAmount) {
                [AppUtils showAlertMessage:[NSString stringWithFormat:@"输入金额不正确，最多退款%.2f", _detailInfo.payAmount]];
                return NO;
            }
            else if(money > 0.0){
                //提交申请退款
                __weak typeof(self) weakSelf = self;
                [AppUtils showProgressMessage:@"处理中"];
                [[GoodsHandler shareGoodsHandler]applyRefund:_detailInfo.orderId amount:money reason:@"" success:^(id obj) {
                    [AppUtils dismissHUD];
                    [weakSelf getOrderDetail];
                    [AppUtils showAlertMessage:[NSString stringWithFormat:@"退款请求已发送给卖家，如10天内不处理，系统会将%.2f元直接汇入您的账户余额中。", money]];
                } failed:^(id obj) {
                    [AppUtils dismissHUD];
                    [AppUtils showAlertMessage:(NSString *)obj];
                }];
            }
            else{
                [AppUtils showAlertMessage:@"输入的金额需大于0"];
                return NO;
            }
        }
    }
    else{
        if (QMInputAlertTypeText == inputAlert.alertType) {
            [AppUtils showAlertMessage:@"请输入您的拒绝理由"];
        }
        else{
            [AppUtils showAlertMessage:@"请输入退款金额"];
        }
        return NO;
    }
    return YES;
}

- (void)cancelInput
{
    
}

@end
