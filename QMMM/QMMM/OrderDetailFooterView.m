//
//  OrderDetailFooterView.m
//  QMMM
//
//  Created by kingnet  on 15-3-10.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "OrderDetailFooterView.h"

@interface OrderDetailFooterView ()
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation OrderDetailFooterView

+ (OrderDetailFooterView *)footerView
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"OrderDetailFooterView" owner:self options:0];
    return [array lastObject];
}

+ (CGFloat)viewHeight
{
    return 40.f;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    [self.button setTitleColor:kLightBlueColor forState:UIControlStateNormal];
}

- (IBAction)clickButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapGuarantBtn)]) {
        [self.delegate didTapGuarantBtn];
    }
}
@end
