//
//  FinanceDao.h
//  QMMM
//  账户提现明细
//  Created by kingnet  on 14-9-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseDao.h"
#import "FinanceDataInfo.h"

@interface FinanceDao : BaseDao

+ (FinanceDao *)sharedInstance;

@end
