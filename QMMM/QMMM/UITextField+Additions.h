//
//  UITextField+Additions.h
//  QMMM
//
//  Created by kingnet  on 15-3-18.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Additions)

- (BOOL)shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

@end
