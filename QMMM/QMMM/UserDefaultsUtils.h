//
//  UserDefaultsUtils.h
//  QMMM
//
//  Created by kingnet  on 14-8-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

#define IS_GIVE_COMMENTS @"comments"




@interface UserDefaultsUtils : NSObject

+(void)saveValue:(id) value forKey:(NSString *)key;

+(id)valueWithKey:(NSString *)key;

+(BOOL)boolValueWithKey:(NSString *)key;

+(void)saveBoolValue:(BOOL)value withKey:(NSString *)key;

+(void)print;


//退出时需要清除的标记都放在这里
+(void)clearUserDefault;

@end
