//
//  QMDatabaseHelper.h
//  QMMM
//  数据库管理
//  Created by kingnet  on 14-9-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "BaseEntity.h"

//________________________________________________________
/**
 *  Database operate delegate
 */
@protocol DatabaseDelegate <NSObject>

@optional
- (BOOL)insert:(BaseEntity *)entity;
- (BOOL)update:(BaseEntity *)entity;
- (BOOL)remove:(BaseEntity *)entity;
- (BOOL)removeClientEntityWithId:(NSString *)entityId;
- (BaseEntity *)queryById:(NSString *) entityId;
- (NSMutableArray *)queryAll;
- (NSMutableArray *)queryAllOrderByName;
- (NSMutableArray *)queryAllOrderByTime;
- (void)closeDB;
- (BOOL)removeAll;

@required
- (void)createTable:(NSString *)sql;

@end
//________________________________________________________

extern NSString * const kUserTable;
extern NSString * const kAddressTable;
extern NSString * const kFinanceTable;
extern NSString * const kSearchHistory;
extern NSString * const kGoodsChatHistory;

@interface QMDatabaseHelper : NSObject

@property (nonatomic, strong) FMDatabase *database;

//管理类单例
+ (QMDatabaseHelper *)sharedInstance;

//获取数据库连接对象
- (FMDatabase *)openDatabase;

//删除所有表的数据
//- (void)removeAllData;

//关闭数据库
- (void)closeDB;

//删除数据库文件
- (BOOL)deleteDatabase:(NSString *)uid;

//判断指定表是否存在
- (BOOL)isTableExists:(NSString *)tableName;

//获取创建table的sql
- (NSDictionary *)sqlForCreateTable;

//创建插入语句
- (NSString *)createInsertSQL:(NSString *)tableName;

//根据当前用户的uid创建数据库
- (void)createDatabase:(NSString *)uid;

@end
