//
//  QMRecommendAdsCell.m
//  QMMM
//
//  Created by Derek on 15/1/4.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMRecommendAdsCell.h"
#import "ActivityInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CycleScrollView/CycleScrollView.h"


@implementation QMRecommendAdsCell{
    NSArray *_arrInfo;
    CycleScrollView *_cycleScroll;
//    NSMutableArray *arrView_;
}


+ (id)cellForGoodsAds{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMRecommendAdsCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}


- (void)awakeFromNib {
    // Initialization code
    _imageViewAds.userInteractionEnabled=YES;
    _imageViewAds.backgroundColor=[UIColor clearColor];
    UITapGestureRecognizer * tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didImageViewClicl)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.numberOfTouchesRequired = 1;
    [_imageViewAds addGestureRecognizer:tapGestureRecognizer];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSMutableArray *)arrView
{
    if (!_arrView) {
        _arrView = [NSMutableArray array];
    }
    return _arrView;
}


-(void)updateUiWithArr:(NSArray *)arrInfo{
    _arrInfo=arrInfo;
    if (arrInfo && [arrInfo count]<=1) {
        
        BannerEntity *banner=[arrInfo objectAtIndex:0];
        NSURL *imageUrl = [NSURL URLWithString:[AppUtils getAbsolutePath:banner.imgSrc]];
        [_imageViewAds sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_middle"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error) {
                NSLog(@"图片加载失败");
                _imageViewAds.image=[UIImage imageNamed:@"goodImgFailed_mid"];
            }
        }];
        
    }else{
        _cycleScroll=[[CycleScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameWidth * 0.43) animationDuration:3];
            [self.contentView addSubview:_cycleScroll];
        
        [self.arrView removeAllObjects];

        for (BannerEntity *banner in arrInfo) {
            NSURL *imageUrl = [NSURL URLWithString:[AppUtils getAbsolutePath:banner.imgSrc]];
            UIImageView *imageView=[[UIImageView alloc] init];
            imageView.frame=CGRectMake(0, 0, kMainFrameWidth, kMainFrameWidth * 0.43);
            [_arrView addObject:imageView];
            [imageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_middle"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error) {
                    NSLog(@"图片加载失败");
                    imageView.image=[UIImage imageNamed:@"goodImgFailed_mid"];
                }
            }];
        }
        
        __weak typeof(self)weakSelf=self;
        _cycleScroll.fetchContentViewAtIndex = ^(NSInteger pageIndex){
            UIImageView *tempV = weakSelf.arrView[pageIndex];
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:tempV.frame];
            imageView.image = tempV.image;
            return imageView;
        };
        _cycleScroll.totalPagesCount = ^NSInteger(void){
            return weakSelf.arrView.count;
        };
        _cycleScroll.TapActionBlock = ^(NSInteger pageIndex){
            [weakSelf didImageWithIndex:pageIndex];
        };
    }
}

-(void)didImageViewClicl{
    BannerEntity *banner=[_arrInfo objectAtIndex:0];
//    NSString *strUrl=banner.activityUrl;
    if (_delegate && [_delegate respondsToSelector:@selector(didAdsClick:)]) {
        [_delegate didAdsClick:banner];
    }
}

-(void)didImageWithIndex:(NSInteger)index{
    BannerEntity *banner=[_arrInfo objectAtIndex:index];
//    NSString *strUrl=banner.activityUrl;
    if (_delegate && [_delegate respondsToSelector:@selector(didAdsClick:)]) {
        [_delegate didAdsClick:banner];
    }
}

@end
