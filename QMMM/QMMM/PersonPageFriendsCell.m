//
//  PersonPageFriendsCell.m
//  QMMM
//
//  Created by kingnet  on 14-11-10.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PersonPageFriendsCell.h"
#import "UserDataInfo.h"

@interface PersonPageFriendsCell (){
    NSArray *_arrFriendList;
}
@property (weak, nonatomic) IBOutlet UIButton *addFriendsBtn;
@property (weak, nonatomic) IBOutlet UILabel *friendsTipLbl;
@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;

@end
@implementation PersonPageFriendsCell

+ (PersonPageFriendsCell *)friendsCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"PersonPageFriendsCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self.addFriendsBtn setTitleColor:kRedColor forState:UIControlStateNormal];
    [self.addFriendsBtn setTitleColor:kDarkTextColor forState:UIControlStateHighlighted];
    [self.addFriendsBtn setTitle:@"加为好友" forState:UIControlStateNormal];
    UIImage *image = [UIImage imageNamed:@"border_btn_bkg"];
    [self.addFriendsBtn setBackgroundImage:[image stretchableImageWithLeftCapWidth:10 topCapHeight:8] forState:UIControlStateNormal];
    [self.chatBtn setTitle:@"私聊" forState:UIControlStateNormal];
    [self.chatBtn setTitleColor:UIColorFromRGB(0x5381db) forState:UIControlStateNormal];
    UIImage *image1 = [UIImage imageNamed:@"blue_border_btn_bkg"];
    [self.chatBtn setBackgroundImage:[image1 stretchableImageWithLeftCapWidth:10 topCapHeight:8] forState:UIControlStateNormal];
    
    self.friendsTipLbl.textColor = UIColorFromRGB(0x9198a3);
    self.friendsTipLbl.text = @"";
    self.friendsTipLbl.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(friendsListActions:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [self.actionView addGestureRecognizer:tapGesture];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight
{
    return 119.f;
}

- (IBAction)addFriendAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickAddFriend)]) {
        [self.delegate clickAddFriend];
    }
}

- (void)friendsListActions:(UITapGestureRecognizer *)tapGesture
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickFriendsList:)]) {
        [self.delegate clickFriendsList:_arrFriendList];
    }
}

- (IBAction)chatAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickChat)]) {
        [self.delegate clickChat];
    }
}

- (void)updateUI:(NSArray *)friends
{
    if (friends.count > 0) {
        _arrFriendList=friends;
        self.friendsTipLbl.text = @"";
        NSMutableArray *mbArr = [NSMutableArray arrayWithCapacity:2];
        for (int i=0; i<2; i++) {
            if (i < friends.count) {
                UserDataInfo *userInfo = friends[i];
                [mbArr addObject:userInfo.nickname];
            }
        }
        NSString *result = @"";
        if (friends.count >1) {
            result = [NSString stringWithFormat:@"等%d人", (int)friends.count];
        }
        UserDataInfo *userInfo = friends[0];
        NSString *tip = [NSString stringWithFormat:@"TA是%@%@的好友", userInfo.nickname, result];
        NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:tip];
        NSRange range = [tip rangeOfString:result];
        [attributeStr addAttribute:NSForegroundColorAttributeName value:kDarkTextColor range:NSMakeRange(range.location, result.length)];
        self.friendsTipLbl.attributedText = attributeStr;
    }
}

@end
