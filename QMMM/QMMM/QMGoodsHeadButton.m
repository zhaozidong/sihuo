//
//  QMGoodsHeadButton.m
//  QMMM
//
//  Created by hanlu on 14-9-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsHeadButton.h"
#import "UIImageView+QMWebCache.h"

@implementation QMGoodsHeadButton

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initView];
    }
    return self;
}

//- (void)awakeFromNib
//{
//    [super awakeFromNib];
//    
//    [self initView];
//}

- (void)initView
{
    self.backgroundColor = [UIColor whiteColor];
    
    _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    [self setMargin:_margin];
    [self addSubview:_imageView];
    
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer * tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headBtnClicked:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGestureRecognizer];
}

- (void)setImagePath:(NSString *)imagePath
{
    if(self) {
        NSURL * url = [NSURL URLWithString:imagePath];
        if(nil != url) {
            if(_imageView.image) {
                _imageView.image = nil;
            }
        }
        
        UIImage *placeholder = [UIImage imageNamed:@"headPlaceholder"];
        [_imageView qm_setImageWithURL:imagePath placeholderImage:placeholder];
    }
}

- (void)setSmallImagePath:(NSString *)imagePath
{
    if (self) {
        NSString *smallPath = [AppUtils smallImagePath:imagePath width:60];
        NSURL * url = [NSURL URLWithString:smallPath];
        if(nil != url) {
            if(_imageView.image) {
                _imageView.image = nil;
            }
        }
        
        UIImage *placeholder = [UIImage imageNamed:@"headPlaceholder_small"];
        [_imageView qm_setImageWithURL:smallPath placeholderImage:placeholder];
    }
}

- (void)setImage:(UIImage *)image
{
    if (image) {
        _imageView.image = image;
    }
}

- (void)setRoundCorner:(CGFloat)roundCorner
{
    if(roundCorner > 0) {
        self.layer.cornerRadius = roundCorner;
        self.layer.masksToBounds = YES;
    } else {
        self.layer.masksToBounds = NO;
    }
    
    _roundCorner = roundCorner;
}

- (void)setMargin:(CGFloat)margin
{
    _margin = margin;
    
    if(_roundCorner) {
        _imageView.layer.cornerRadius = _roundCorner;
        _imageView.layer.masksToBounds = YES;
    }
    
    [self setNeedsLayout];
}

- (void)setMarginColor:(UIColor *)color
{
    self.backgroundColor = color;
}

- (void)setText:(NSString *)text
{
    if([text length] > 0) {
        if(nil == _textLabel) {
            _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 6, self.bounds.size.width, self.bounds.size.height - 6)];
            [_textLabel setFont:[UIFont systemFontOfSize:9.0]];
            [_textLabel setTextAlignment:NSTextAlignmentCenter];
            [self addSubview:_textLabel];
        }
        
        _textLabel.text = text;
    } else {
         if(_textLabel != nil) {
             [_textLabel setText:@""];
         }
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if(_margin > 0) {
        _imageView.frame = CGRectInset(self.bounds, _margin, _margin);
    }
}

- (void)headBtnClicked:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(didHeadButtonClicked:)] && _userId) {
        [_delegate didHeadButtonClicked:self.userId];
    }
}

@end
