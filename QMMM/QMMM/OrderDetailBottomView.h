//
//  OrderDetailBottomView.h
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailCellDelegate.h"

@class OrderDataInfo;

@interface OrderDetailBottomView : UIView

@property (nonatomic, assign) BOOL canPay; //是否还可以付款

@property (nonatomic, weak) id<OrderDetailCellDelegate> delegate;

+ (OrderDetailBottomView *)bottomView;

- (void)updateUI:(OrderDataInfo *)orderInfo bSeller:(BOOL)bSeller;

+ (CGFloat)viewHeight;

@end
