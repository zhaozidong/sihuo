//
//  QMFilterItem.m
//  QMMM
//
//  Created by kingnet  on 15-3-16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMFilterItem.h"
#import <POP/POP.h>

@interface QMFilterItem ()
{
    id target_;   //点击事件的执行者
    SEL action_; //执行的方法
}

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@end

@implementation QMFilterItem

+ (QMFilterItem *)filterItem
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"QMFilterItem" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor whiteColor];
    self.titleLbl.text = @"";
    self.titleLbl.textColor = kDarkTextColor;
}

- (void)setTitle:(NSString *)title
{
    if (title && title.length > 0) {
        self.titleLbl.text = title;
    }
}

- (void)addClickTarget:(id)target action:(SEL)action
{
    target_ = target;
    action_ = action;
    
    //添加点击事件
    self.userInteractionEnabled = YES;
    //移除所有手势
    NSArray *gestures = self.gestureRecognizers;
    for (UIGestureRecognizer *gesture in gestures) {
        [self removeGestureRecognizer:gesture];
    }

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickAction:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGesture];
}

- (void)clickAction:(UITapGestureRecognizer *)gesture
{
    if (target_ && [target_ respondsToSelector:action_]) {
        //得到action的代码块地址
        IMP imp = [target_ methodForSelector:action_];
        NSMethodSignature *sig = [target_ methodSignatureForSelector:action_];
        
        //有两个隐藏的参数，The receiving object、The selector for the method
        if (sig.numberOfArguments == 3) { //selector带一个参数时
            void (*func)(id, SEL, id) = (void (*)(id, SEL, id))imp;
            func(target_, action_, self);
        }
        else if (sig.numberOfArguments == 2){ //selector不带参数时
            void (*func)(id, SEL) = (void (*)(id, SEL))imp;
            func(target_, action_);
        }
    }
}

#pragma mark - Arrow Animate
- (void)rotateArrowUp:(BOOL)isUp
{
    [self.arrowImageView.layer pop_removeAnimationForKey:@"arrowAnim"];
    if (isUp) {
        //从下向上旋转
        POPSpringAnimation *basicAnimation = [POPSpringAnimation animation];
        basicAnimation.property = [POPAnimatableProperty propertyWithName: kPOPLayerRotation];
        basicAnimation.toValue= @(M_PI);
        [self.arrowImageView.layer pop_addAnimation:basicAnimation forKey:@"arrowAnim"];
    }
    else{
        //从上向下旋转
        POPSpringAnimation *basicAnimation = [POPSpringAnimation animation];
        basicAnimation.property = [POPAnimatableProperty propertyWithName: kPOPLayerRotation];
        basicAnimation.toValue= @(2*M_PI);
        [self.arrowImageView.layer pop_addAnimation:basicAnimation forKey:@"arrowAnim"];
    }
}
@end
