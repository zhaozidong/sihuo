//
//  QMGoodsAudioView.m
//  QMMM
//
//  Created by hanlu on 14-9-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsAudioView.h"
#import "GoodEntity.h"
#import "QMAudioPlayer.h"
#import "GoodsHandler.h"

@interface QMGoodsAudioView ()<AVAudioPlayerDelegate>
{
    uint64_t _goodsId;
    GoodEntity *_goodsInfo;
    QMAudioPlayer *_audioPlayer;
    UIActivityIndicatorView *_acIndicator;
    NSString *_audioPath;
    BOOL isLocalPath;
}

@end
@implementation QMGoodsAudioView

@synthesize backgroundView = _backgroundView;
@synthesize audioImageView = _audioImageView;
@synthesize seconds = _seconds;

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initView];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self initView];
}

- (void)dealloc
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initView
{
    self.backgroundColor = [UIColor clearColor];
    
    _backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"audio_bkg"]];
    [self addSubview:_backgroundView];
    
    UIImage * image = [UIImage imageNamed:@"audio_third"];
    NSArray * images = [NSArray arrayWithObjects:[UIImage imageNamed:@"audio_one"], [UIImage imageNamed:@"audio_second"], [UIImage imageNamed:@"audio_third"], nil];
    _audioImageView = [[UIImageView alloc] initWithImage:image];
    [_audioImageView setAnimationImages:images];
    [_audioImageView setAnimationDuration:1.0];
    [self addSubview:_audioImageView];
    
    _seconds = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 12)];
    [_seconds setBackgroundColor:[UIColor clearColor]];
    [_seconds setTextColor:[UIColor whiteColor]];
    [_seconds setFont:[UIFont systemFontOfSize:12.0]];
    [_seconds setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:_seconds];
    
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer * tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didAudioClicked:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGestureRecognizer];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPlayAudioStart:) name:kGoodsAudioPlayNotify object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPlayAudioStop:) name:kGoodsAudioStopNotify object:nil];
}

- (void)updateUI:(GoodEntity *)goodsInfo
{
    _goodsInfo = goodsInfo;
    
    isLocalPath=NO;
    
    _audioPath=_goodsInfo.msgAudio;
    
    if(0 == goodsInfo.msgAudioSeconds) {
        _seconds.text = @"";
    } else {
        _seconds.text = [NSString stringWithFormat:@"%u\"", goodsInfo.msgAudioSeconds];
    }
    
    self.hidden = (0 == goodsInfo.msgAudioSeconds) ? YES : NO;
    
    _goodsId = goodsInfo.gid;
    
    [self hideActivityIndicator];
    
    [self setNeedsLayout];
}

-(void)updateWithLocalPath:(NSString *)localPath andAudioTime:(NSInteger)audioTime{
    
    if (localPath) {
        _audioPath=localPath;
    }
    
    if (audioTime) {
        _seconds.text = [NSString stringWithFormat:@"%ld\"", (long)audioTime];
    }
    
    isLocalPath=YES;
    
    self.hidden = (0 == audioTime) ? YES : NO;
    
    [self hideActivityIndicator];
    
    [self setNeedsLayout];
    
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    UIImage * image = [UIImage imageNamed:@"audio_third"];
    
    CGRect frame = _audioImageView.frame;
    frame.origin.x = self.bounds.origin.x + (self.bounds.size.width - image.size.width) / 2;
    frame.origin.y = self.bounds.origin.y + 11;
    _audioImageView.frame = frame;
    
    frame = _seconds.frame;
    frame.origin.y = _audioImageView.frame.origin.y + _audioImageView.frame.size.height + 3;
    frame.size.height = _audioImageView.frame.size.height;
    _seconds.frame = frame;
    
    _acIndicator.frame=CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);

}

#pragma mark - QMGoodsAudioDelegate

- (void)didAudioClicked:(id)sender
{
    if ([QAudioPlayer sharedInstance].isPlaying) {
        [[QAudioPlayer sharedInstance] stopPlay];
        [_audioImageView stopAnimating];
        return;
    }

    //点击音频
    NSString * audioPath = _audioPath;
    if(audioPath) {
        NSString *strFullName;
        if (!isLocalPath) {
            strFullName=[NSString stringWithFormat:@"%@%@",[AppUtils getCacheDirectory],[AppUtils getAudioFileName:audioPath]];
        }else{
//            strFullName = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath],audioPath];
            strFullName = audioPath;
        }
        
        BOOL bDirectory = NO;
        NSFileManager * fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:strFullName isDirectory:&bDirectory]) {
//            [self audioPlayImp:strFullName tag:_goodsInfo.gid];
            [self didPlayAudioStart];
            [[QAudioPlayer sharedInstance] playAudioWithPath:strFullName withRelatedView:self];
        }
        else{//录音文件不存在
            _audioImageView.hidden=YES;
            _seconds.hidden=YES;
            
            if (!_acIndicator) {
                _acIndicator=[[UIActivityIndicatorView alloc] init];
//                _acIndicator.backgroundColor=[UIColor redColor];
                _acIndicator.hidesWhenStopped=YES;
                [_acIndicator startAnimating];
                [self addSubview:_acIndicator];
            }else{
                _acIndicator.hidden=NO;
                [_acIndicator startAnimating];
            }
//            __weak UIActivityIndicatorView *weakAC=_acIndicator;
            
            __weak QMGoodsAudioView *weakSelf=self;
            [[GoodsHandler shareGoodsHandler] downloadAudio:audioPath complete:^{
                [weakSelf hideActivityIndicator];
            }];
            DLog(@"-------audio file not exist: %@", strFullName);
        }
        
        
        
//        NSString * filename = [[GoodsHandler class] audioPath];
//        //截取音频文件名
//        NSRange range = [audioPath rangeOfString:@"/" options:NSBackwardsSearch];
//        if(range.location != NSNotFound) {
//            NSString * temp = [audioPath substringFromIndex:range.location+1];
//            filename = [filename stringByAppendingPathComponent:temp];
//
//            NSLog(@"didAudioClicked filename=%@",filename);
//            
//            BOOL bDirectory = NO;
//            NSFileManager * fileManager = [NSFileManager defaultManager];
//            if ([fileManager fileExistsAtPath:filename isDirectory:&bDirectory]) {
//                [self audioPlayImp:filename tag:_goodsInfo.gid];
//            }
//            else{
//                DLog(@"-------audio file not exist: %@", filename);
//            }
//            
////            range = [temp rangeOfString:@"." options:NSBackwardsSearch];
////            if(range.location != NSNotFound) {
////                temp = [temp substringToIndex:range.location];
////                NSString * wavSuffix = [filename stringByAppendingFormat:@"/%@.wav", temp];
////                NSString * amrSuffix = [filename stringByAppendingFormat:@"/%@.amr", temp];
////                
////                BOOL bDirectory = NO;
////                NSFileManager * fileManager = [NSFileManager defaultManager];
////                if([fileManager fileExistsAtPath:wavSuffix isDirectory:&bDirectory]) {
////                    [self audioPlayImp:wavSuffix tag:_goodsInfo.gid];
////                } else if ([fileManager fileExistsAtPath:amrSuffix isDirectory:&bDirectory]){
////                    [self audioPlayImp:amrSuffix tag:_goodsInfo.gid];
////                } else {
////                    DLog(@"-------audio file not exist: %@", audioPath);
////                }
////            }
//        }
    }
}

-(void)hideActivityIndicator{
    if (_acIndicator) {
        [_acIndicator stopAnimating];
        _acIndicator.hidden=YES;
    }
    _audioImageView.hidden=NO;
    _seconds.hidden=NO;
}


- (void)audioPlayImp:(NSString *)audio tag:(uint64_t)tag
{
    if([_audioPlayer isPlaying]) {
        _audioPlayer.delegate = nil;
        [_audioPlayer stop];
    }
    
    NSError * error = nil;
    NSData * data = [NSData dataWithContentsOfFile:audio];
    _audioPlayer = [[QMAudioPlayer alloc] initWithData:data error:&error];
    _audioPlayer.delegate = self;
    _audioPlayer.numberOfLoops = 0;
    _audioPlayer.tag = tag;
    [_audioPlayer prepareToPlay];
    BOOL success = [_audioPlayer play];
    if(success) {
        NSDictionary * dict = [[NSDictionary alloc] initWithObjectsAndKeys:@(tag), @"goodsId", nil];
        [self didPlayAudioStart:dict];
    }
}

#pragma mark - AVAudioPlayer Delegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    QMAudioPlayer * audioPlayer = nil;
    NSDictionary * dict = nil;
    if([player isKindOfClass:[QMAudioPlayer class]]) {
        audioPlayer = (QMAudioPlayer *)player;
        dict = [NSDictionary dictionaryWithObjectsAndKeys:@(audioPlayer.tag), @"userInfo", nil];
    }
    [self didPlayAudioStop:dict];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    [self didPlayAudioStop:nil];
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player NS_DEPRECATED_IOS(2_2, 8_0)
{
    [self didPlayAudioStop:nil];
}


- (void)didPlayAudioStart
{
    if(![_audioImageView isAnimating]) {
        [_audioImageView startAnimating];
    }
}

//#pragma mark - notification
//- (void)didPlayAudioStart:(NSNotification *)sender
- (void)didPlayAudioStart:(NSDictionary *)audioParams
{
    id temp = [audioParams objectForKey:@"goodsId"];
    if(temp && [temp isKindOfClass:[NSNumber class]]) {
        if(_goodsId == [temp intValue]) {
            if(![_audioImageView isAnimating]) {
                [_audioImageView startAnimating];
            }
        }
    }
}

- (void)didPlayAudioStop:(NSDictionary *)audioParams
{
    if([_audioImageView isAnimating]) {
        [_audioImageView stopAnimating];
    }
}

@end
