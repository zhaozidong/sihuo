//
//  ImageDisplayView.h
//  QMMM
//
//  Created by Shinancao on 14-9-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageDisplayViewDelegate <NSObject>

- (void)didTapBack;
- (void)finishWithImageFile:(NSString *)filePath;
- (void)finishUpdateImageFile;
- (void)rotateImage:(UIImage *)image;
- (void)restoreOrginalImage;
@end

@interface ImageDisplayView : UIView

@property (nonatomic, assign) BOOL deleteBeforeSave;
@property (nonatomic, strong)NSString *photoPath;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, weak) id<ImageDisplayViewDelegate> delegate;
@end
