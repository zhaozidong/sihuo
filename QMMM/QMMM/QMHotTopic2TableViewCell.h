//
//  QMHotTopic2TableViewCell.h
//  QMMM
//
//  Created by Derek on 15/3/17.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QMHotTopic1TableViewCell.h"


@interface QMHotTopic2TableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *topicImg1;

@property (strong, nonatomic) IBOutlet UIImageView *topicImg2;

@property (weak, nonatomic) id<QMHotTopicCellDelegate> delegate;


+(QMHotTopic2TableViewCell *)hotTopic2;


-(void)updateWithArr:(NSArray *)arr;

@end
