//
//  PublishOKViewController.h
//  QMMM
//
//  Created by kingnet  on 14-10-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@class GoodEntity;
@interface PublishOKViewController : BaseViewController

@property (nonatomic, strong) GoodEntity *goodsEntity;

- (instancetype)initWithGoodsEntity:(GoodEntity *)entity;

@end
