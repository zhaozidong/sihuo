//
//  UIGoodsFilterView.m
//  QMMM
//
//  Created by hanlu on 14-9-19.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsFilterView.h"
#import "QMGoodsFilterTitleCell.h"


@interface QMGoodsFilterView () {
    EDefaultSortType _sortType;
    CGFloat _startPrice;
    CGFloat _endPrice;
}

@end

@implementation QMGoodsFilterView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorColor = [UIColor whiteColor];
    if(kIOS7) {
        _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    _sortType = EST_DEFAULT;
    
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [_tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)releaseFocus
{
    [_startTextField resignFirstResponder];
    [_endTextField resignFirstResponder];
}

- (EDefaultSortType)sortType
{
    return _sortType;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMGoodsFilterTitleCell* titleCell = [_tableView dequeueReusableCellWithIdentifier:kGoodsFilterTitleCellIdentifier];
    if(nil == titleCell) {
        titleCell = [QMGoodsFilterTitleCell cellForGoodsFilterTitleCell];
    }
    
    if(0 == indexPath.row) {
        titleCell.titleLabel.text = [AppUtils localizedProductString:@"QM_DEFAULT_SORT"];
    } else if(1 == indexPath.row) {
        titleCell.titleLabel.text = [AppUtils localizedProductString:@"QM_ASC_SORT"];
    } else if(2 == indexPath.row) {
         titleCell.titleLabel.text = [AppUtils localizedProductString:@"QM_DESC_SORT"];
    }
        
    return titleCell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(0 == indexPath.row) {
        _sortType = EST_DEFAULT;
    } else if(1 == indexPath.row) {
        _sortType = EST_ASC;
    } else if(2 == indexPath.row) {
        _sortType = EST_DESC;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}


@end
