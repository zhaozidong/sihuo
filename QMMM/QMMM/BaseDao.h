//
//  BaseDao.h
//  QMMM
//
//  Created by kingnet  on 14-9-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QMDatabaseHelper.h"

@interface BaseDao : NSObject<DatabaseDelegate>

- (id)initWithTableName:(NSString *)tableName;

@end
