//
//  LRInputViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "LRInputViewController.h"
#import "QMTextField.h"
#import "QMSubmitButton.h"
#import "TimerButton.h"
#import "UserInfoHandler.h"

@interface LRInputViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableViewCell *inputPhoneNumCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *inputPwdCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *checkCodeCell;

@property (weak, nonatomic) IBOutlet QMTextField *phoneNumTf;
@property (weak, nonatomic) IBOutlet QMTextField *pwdTf;
@property (weak, nonatomic) IBOutlet QMTextField *checkCodeTf;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet QMSubmitButton *submitBtn;
@property (weak, nonatomic) IBOutlet TimerButton *timerBtn;
@property (weak, nonatomic) IBOutlet UIButton *changePwdSeeBtn;

@property (nonatomic, strong) UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *headView;

@end

@implementation LRInputViewController

+ (CGFloat)viewHeight:(BOOL)isLoginView
{
    if (isLoginView) {
        return 55*2+28+98+kMainTopHeight+107;
    }
    else{
        return 55*3+28+98+kMainTopHeight;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupViews];
//    [self.phoneNumTf becomeFirstResponder];
    
    if (NO == self.isRegisterView) {
        //拿到历史用户名
        UserEntity *user = [[UserInfoHandler sharedInstance]currentUser];
        self.phoneNumTf.text = user.mobile;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.timerBtn endTiming];
}

- (void)setupViews
{
    self.view.frame = CGRectMake(0, 0, kMainFrameWidth, [[self class]viewHeight:self.isLoginView]);
    //加载需要的xib
    [[NSBundle mainBundle]loadNibNamed:@"LRinputView" owner:self options:0];
    
    //设置tableView
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStylePlain];
    _tableView.scrollEnabled = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.rowHeight = 55.f;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kBorderColor;
    _tableView.tableFooterView = self.footerView;
    [self.view addSubview:_tableView];
    
    self.footerView.backgroundColor = [UIColor clearColor];
    
    if (self.isLoginView) {
        //是登录界面
        _tableView.tableHeaderView = self.headView;
        self.headView.backgroundColor = [UIColor clearColor];
    }
    
    UIImageView *imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone_icon"]];
    self.phoneNumTf.leftView = imgV;
    UIImageView *imgV1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"pwd_icon"]];
    self.pwdTf.leftView = imgV1;
    self.phoneNumTf.attributedPlaceholder = [AppUtils placeholderAttri:@"请输入您的手机号"];
    if (self.pwdPlaceholder) {
        self.pwdTf.attributedPlaceholder = [AppUtils placeholderAttri:self.pwdPlaceholder];
    }
    else{
        self.pwdTf.attributedPlaceholder = [AppUtils placeholderAttri:@"请输入您的密码"];
    }
    self.phoneNumTf.delegate = self;
    self.pwdTf.delegate = self;
    self.pwdTf.returnKeyType = self.returnKeyType;
    
    UIImageView *imgV2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkCode_icon"]];
    self.checkCodeTf.leftView = imgV2;
    self.checkCodeTf.attributedPlaceholder = [AppUtils placeholderAttri:@"请输入验证码"];
    self.checkCodeTf.delegate = self;
    
    [self.submitBtn addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    self.submitBtn.enabled = NO;
    [self.submitBtn setTitle:self.submitBtnTitle forState:UIControlStateNormal];
    
    [self.timerBtn addTarget:self action:@selector(timerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.changePwdSeeBtn addTarget:self action:@selector(changePwdSeeAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)submitAction:(id)sender
{
    if (self.phoneNumTf.text == nil || self.phoneNumTf.text.length == 0) {
        [AppUtils showAlertMessage:[AppUtils localizedPersonString:@"QM_Tip_Phone"]];
        [self.phoneNumTf becomeFirstResponder];
        return;
    }
//    if (![AppUtils checkPhoneNumber:self.phoneNumTf.text]) {
//        [AppUtils showAlertMessage:@"手机号码格式不正确"];
//        [self.phoneNumTf becomeFirstResponder];
//        return;
//    }
    if (self.pwdTf.text == nil || self.pwdTf.text.length == 0) {
        [AppUtils showAlertMessage:[AppUtils localizedPersonString:@"QM_Tip_Pwd"]];
        [self.pwdTf becomeFirstResponder];
        return;
    }
    if (self.pwdTf.text.length <6 || self.pwdTf.text.length >20) {
        [AppUtils showAlertMessage:@"密码长度为6—20"];
        [self.pwdTf becomeFirstResponder];
        return;
    }
    
    if (!self.isLoginView) {
        if (self.checkCodeTf.text == nil || self.checkCodeTf.text.length == 0) {
            [AppUtils showAlertMessage:@"请输入验证码"];
            [self.checkCodeTf becomeFirstResponder];
            return;
        }
    }
    
    [AppUtils closeKeyboard];
    
    if (self.isLoginView) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickSubmitBtn:pwd:)]) {
            [self.delegate clickSubmitBtn:self.phoneNumTf.text pwd:self.pwdTf.text];
        }
    }
    else{
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickSubmitBtn:pwd:checkCode:)]) {
            [self.delegate clickSubmitBtn:self.phoneNumTf.text pwd:self.pwdTf.text checkCode:self.checkCodeTf.text];
        }
    }
    
}

- (void)timerAction:(id)sender
{
    if (self.phoneNumTf.text == nil || self.phoneNumTf.text.length == 0) {
        [AppUtils showAlertMessage:[AppUtils localizedPersonString:@"QM_Tip_Phone"]];
        [self.phoneNumTf becomeFirstResponder];
        return;
    }
    //拉取验证码
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickGetCheckCode:)]) {
        [self startTimer];
        [self.delegate clickGetCheckCode:self.phoneNumTf.text];
    }
}

- (void)changePwdSeeAction:(id)sender
{
    self.changePwdSeeBtn.selected = !self.changePwdSeeBtn.selected;
    self.pwdTf.secureTextEntry = !self.changePwdSeeBtn.selected;
}

- (void)stopTimer
{
    [self.timerBtn endTiming];
}

- (void)startTimer
{
    [self.timerBtn startTiming];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isLoginView) {
        return 2;
    }
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return self.inputPhoneNumCell;
    }
    else if(indexPath.row == 1){
        if (self.isLoginView) {
            return self.inputPwdCell;
        }
        else{
            return self.checkCodeCell;
        }
    }
    else{
        return self.inputPwdCell;
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 28.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (NO == self.isLoginView) {
        if (textField == self.pwdTf) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(viewShouldMoveY:)]) {
                CGRect bounds = [UIScreen mainScreen].bounds;
                if (CGRectGetHeight(bounds)> 480) {
                    [self.delegate viewShouldMoveY:25.f];
                }
                else{
                    [self.delegate viewShouldMoveY:65.f];
                }
            }
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.phoneNumTf) {
        [self.pwdTf becomeFirstResponder];
    }
    else{
        //跳到一个页面
        [self submitAction:nil];
    }
    return YES;
}

- (void)textDidChange:(UITextField *)textField
{
    if (self.phoneNumTf.text!=nil && self.phoneNumTf.text.length >0 && self.pwdTf.text!=nil && self.pwdTf.text.length>0) {
        if (!self.isLoginView) {
            if (self.checkCodeTf.text!=nil && self.checkCodeTf.text.length >0) {
                self.submitBtn.enabled = YES;
            }
            else{
                self.submitBtn.enabled = NO;
            }
        }
        else{
            self.submitBtn.enabled = YES;
        }
    }
    else{
        self.submitBtn.enabled = NO;
    }
}

@end
