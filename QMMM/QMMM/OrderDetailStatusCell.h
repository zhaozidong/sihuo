//
//  OrderDetailStatusCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class OrderDataInfo;

extern NSString * const kOrderDetailStatusCellIdentifier;

@interface OrderDetailStatusCell : UITableViewCell

+ (UINib *)nib;

+ (CGFloat)cellHeight;

- (void)updateUI:(OrderDataInfo *)orderInfo;

@end
