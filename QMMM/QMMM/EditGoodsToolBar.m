//
//  EditGoodsToolBar.m
//  QMMM
//
//  Created by kingnet  on 15-3-2.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "EditGoodsToolBar.h"
#import "QMSubmitButton.h"
#import "UIButton+Addition.h"

@interface EditGoodsToolBar ()
@property (weak, nonatomic) IBOutlet UIButton *previewBtn;
@property (weak, nonatomic) IBOutlet QMSubmitButton *publishGoodsBtn;

@end

@implementation EditGoodsToolBar

+ (EditGoodsToolBar *)toolBar
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"EditGoodsToolBar" owner:self options:0];
    return [array lastObject];
}

+ (CGFloat)viewHeight
{
    return 50.f;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.previewBtn setTitleColor:kLightBlueColor forState:UIControlStateNormal];
    [self.previewBtn setButtonType:BT_LeftImageRightTitle interGap:10.f];
}

- (void)setBtnTitle:(NSString *)btnTitle
{
    [self.publishGoodsBtn setTitle:btnTitle forState:UIControlStateNormal];
}

- (IBAction)publishAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapPublish)]) {
        [self.delegate didTapPublish];
    }
}

- (IBAction)previewAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapPreview)]) {
        [self.delegate didTapPreview];
    }
}


@end
