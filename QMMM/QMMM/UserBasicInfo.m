//
//  UserBasicInfo.m
//  QMMM
//
//  Created by hanlu on 14-10-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "UserBasicInfo.h"
#import "FMResultSet.h"

@implementation UserBasicInfo

- (id)initWithFMResultSet:(FMResultSet *)resultSet
{
    self = [super init];
    if(self) {
        _userId = [resultSet unsignedLongLongIntForColumnIndex:0];
        _nickname = [resultSet stringForColumnIndex:1];
        _icon = [resultSet stringForColumnIndex:2];
        _remark = [resultSet stringForColumnIndex:4];
//        _extData = [resultSet stringForColumnIndex:3];
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self && dict && [dict isKindOfClass:[NSDictionary class]]) {
        id tmp = [dict objectForKey:@"uid"];
        if([tmp isKindOfClass:[NSString class]]) {
            _userId = (uint64_t)[tmp longLongValue];
        } else if([tmp isKindOfClass:[NSNumber class]]) {
            _userId = [tmp unsignedLongLongValue];
        }
        
        tmp = [dict objectForKey:@"nickname"];
        if([tmp isKindOfClass:[NSString class]]) {
            _nickname = tmp;
        } else {
            _nickname = @"";
        }
        
        tmp = [dict objectForKey:@"avatar"];
        if([tmp isKindOfClass:[NSString class]]) {
            _icon = tmp;
        } else {
            _icon = @"";
        }
        
        tmp = [dict objectForKey:@"remark"];
        if ([tmp isKindOfClass:[NSString class]]) {
            _remark = tmp;
        }
        else{
            _remark = @"";
        }
    }
    return self;

}

- (id)initWithUserInfo:(UserBasicInfo *)info
{
    self = [super init];
    if(self) {
        _userId = info.userId;
        _nickname = info.nickname;
        _icon = info.icon;
        _remark = info.remark;
//        _extData = info.extData;
    }
    return self;
}

- (NSString *)showname
{
    if (_remark.length > 0) {
        return _remark;
    }
    else{
        return _nickname;
    }
}

@end
