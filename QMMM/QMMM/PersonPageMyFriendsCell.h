//
//  PersonPageMyFriendsCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-11.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDataInfo.h"

@protocol PersonPageMyFriendsCellDelegate <NSObject>

- (void)clickChat;

- (void)clickFriendsFrom:(PersonPageUserInfo *)userInfo;

@end

@interface PersonPageMyFriendsCell : UITableViewCell

@property (nonatomic, weak) id<PersonPageMyFriendsCellDelegate> delegate;

+ (PersonPageMyFriendsCell *)myFriendsCell;

+ (CGFloat)cellHeight;

-(void)updateUIWithUserInfo:(PersonPageUserInfo *)userInfo;

@end
