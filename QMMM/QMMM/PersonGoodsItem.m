//
//  PersonGoodsItem.m
//  QMMM
//
//  Created by kingnet  on 14-11-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PersonGoodsItem.h"
#import "GoodDataInfo.h"
#import "UIImageView+QMWebCache.h"
#import "StrikeoutLineLabel.h"

@interface PersonGoodsItem ()
@property (weak, nonatomic) IBOutlet UIImageView *goodsImageView;
@property (weak, nonatomic) IBOutlet UILabel *goodsDescLbl;
@property (weak, nonatomic) IBOutlet UILabel *goodsPriceLbl;
@property (weak, nonatomic) IBOutlet StrikeoutLineLabel *oriPriceLbl;
@end

@implementation PersonGoodsItem

+ (PersonGoodsItem *)goodsItem
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"PersonGoodsItem" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.goodsPriceLbl.textColor = kRedColor;
    self.goodsDescLbl.textColor = kDarkTextColor;
    self.goodsPriceLbl.text = @"";
    self.goodsDescLbl.text = @"";
    self.goodsImageView.image = [UIImage imageNamed:@"goodsPlaceholder_middle"];
    self.goodsDescLbl.numberOfLines = 2;
    self.oriPriceLbl.textColor = kLightBlueColor;
    self.oriPriceLbl.strikeThroughColor = kLightBlueColor;
    self.oriPriceLbl.text = @"";
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [kBorderColor CGColor];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(oneTapAction:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGesture];
}

+ (CGSize)itemSize
{
    float margin = [[self class]margin];
    float w = (kMainFrameWidth-margin*3)/2;
    float h = w+68;
    return CGSizeMake(w, h);
}

+ (CGFloat)margin
{
    return 6.f;
}

- (void)setGoodsInfo:(GoodsSimpleInfo *)goodsInfo
{
    if (_goodsInfo != goodsInfo) {
        _goodsInfo = goodsInfo;
        self.goodsDescLbl.text = _goodsInfo.desc;
        self.goodsPriceLbl.text = [NSString stringWithFormat:@"¥%.2f", _goodsInfo.price];
        self.oriPriceLbl.text = [NSString stringWithFormat:@"¥%.2f", _goodsInfo.orig_price];
        UIImage *image = [UIImage imageNamed:@"goodsPlaceholder_middle"];
        NSString *imgUrl = [AppUtils thumbPath:_goodsInfo.photo sizeType:QMImageHalfSize];
        [self.goodsImageView qm_setImageWithURL:imgUrl placeholderImage:image];
    }
}

- (void)oneTapAction:(UITapGestureRecognizer *)gesture
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickGoodsItem:)]) {
        [self.delegate clickGoodsItem:self.goodsInfo];
    }
}

@end
