//
//  GoodsPhotoBoxCell.h
//  QMMM
//
//  Created by kingnet  on 14-11-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GoodsPhotoCellDelegate <NSObject>

- (void)showCamera;

@end
@interface GoodsPhotoBoxCell : UITableViewCell

+ (GoodsPhotoBoxCell *)goodsPhotoBoxCell;

+ (CGFloat)cellHeight;

- (void)addPhotos:(NSArray *)photos isServerPhoto:(BOOL)isServerPhoto;

@property (nonatomic, strong) NSMutableArray *photoArray;

@property (nonatomic, weak) id<GoodsPhotoCellDelegate> delegate;

@property (nonatomic, assign)BOOL isShowMainPic;

- (NSString *)mainPhoto;

//拿到生活照的路径，包括key，如果不需要上传，返回nil
- (NSArray *)keysArray;

//校验是否所有的图片都已经上传完了
- (BOOL)isImgAllUploaded;

//返回所有大图的本地路径
- (NSArray *)bigImgLocalPath;

@end
