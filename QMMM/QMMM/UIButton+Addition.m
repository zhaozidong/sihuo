//
//  UIButton+Addition.m
//  QMMMM
//

#import "UIButton+Addition.h"

@implementation UIButton (Addition)

- (void)setButtonType:(TT_BUTTON_TYPE)buttonType
{
    CGSize titleSize;
#ifdef __IPHONE_7_0
    float sysVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (sysVersion >= 7.0) {
        CGRect rect = [self.titleLabel.text boundingRectWithSize:CGSizeMake(320, 480) options:NSStringDrawingTruncatesLastVisibleLine attributes:nil context:nil];
        titleSize.width = rect.size.width;
        titleSize.height = rect.size.height;
    }
    else {
#endif
        titleSize = [self.titleLabel.text sizeWithFont:self.titleLabel.font constrainedToSize:CGSizeMake(320, 480)];
#ifdef __IPHONE_7_0
    }
#endif
    switch (buttonType) {
        case BT_LeftImageRightTitle:
            [self setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
            [self setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
            break;
        case BT_RightTitleLeftImage:
            [self setImageEdgeInsets:UIEdgeInsetsMake(0, titleSize.width, 0, -titleSize.width)];
            [self setTitleEdgeInsets:UIEdgeInsetsMake(0, -self.imageView.image.size.height, 0, self.imageView.image.size.width)];
            break;
        case BT_UpImageBottomTitle:
            [self setImageEdgeInsets:UIEdgeInsetsMake(-titleSize.height, 0, 0, -titleSize.width)];
            [self setTitleEdgeInsets:UIEdgeInsetsMake(self.imageView.image.size.height, -self.imageView.image.size.width, 0, 0)];
            break;
        case BT_BottomImageUpTitle:
            [self setImageEdgeInsets:UIEdgeInsetsMake(titleSize.height, 0, 0, -titleSize.width)];
            [self setTitleEdgeInsets:UIEdgeInsetsMake(-self.imageView.image.size.height, -self.imageView.image.size.width, 0, 0)];
            break;
        default:
            break;
    }
}

- (void)setButtonType:(TT_BUTTON_TYPE)buttonType interGap:(CGFloat)gap
{
    CGSize titleSize = [self.titleLabel.text sizeWithFont:self.titleLabel.font constrainedToSize:CGSizeMake(320, 480)];
    switch (buttonType) {
        case BT_LeftImageRightTitle:
            [self setImageEdgeInsets:UIEdgeInsetsMake(0, - gap / 2, 0, 0)];
            [self setTitleEdgeInsets:UIEdgeInsetsMake(0, gap / 2, 0, 0)];
            break;
        case BT_RightTitleLeftImage:
            [self setImageEdgeInsets:UIEdgeInsetsMake(0, titleSize.width + gap / 2, 0, -titleSize.width)];
            [self setTitleEdgeInsets:UIEdgeInsetsMake(0, -self.imageView.image.size.height - gap / 2, 0, self.imageView.image.size.width)];
            break;
        case BT_UpImageBottomTitle:
            [self setImageEdgeInsets:UIEdgeInsetsMake(-titleSize.height - gap / 2, 0, 0, -titleSize.width)];
            [self setTitleEdgeInsets:UIEdgeInsetsMake(self.imageView.image.size.height + gap / 2, -self.imageView.image.size.width, 0, 0)];
            break;
        case BT_BottomImageUpTitle:
            [self setImageEdgeInsets:UIEdgeInsetsMake(titleSize.height + gap / 2, 0, 0, -titleSize.width)];
            [self setTitleEdgeInsets:UIEdgeInsetsMake(-self.imageView.image.size.height - gap / 2, -self.imageView.image.size.width, 0, 0)];
            break;
        default:
            break;
    }
}

@end
