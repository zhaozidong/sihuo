//
//  QMGoodCateParser.m
//  QMMM
//
//  Created by hanlu on 14-9-19.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsCateParser.h"

@implementation QMGoodsCateInfo

@end


@interface QMGoodsCateParser ()<NSXMLParserDelegate> {
    NSXMLParser * _xmlParser;
    QMGoodsCateInfo * _currentCateInfo;
    NSMutableString* _currentParsedCharData;
}
@end

@implementation QMGoodsCateParser

- (id)init
{
    self = [super init];
    if(self) {
    }
    return self;
}

- (BOOL)loadGoodsCateXml
{
    NSString * filePath = [[NSBundle mainBundle] pathForResource:@"categorys" ofType:@"xml"];
    NSData * data = [NSData dataWithContentsOfFile:filePath];
    _xmlParser = [[NSXMLParser alloc] initWithData:data];
    _xmlParser.delegate = self;
    
    BOOL ret = [_xmlParser parse];
    if(ret) {
        for (QMGoodsCateInfo * info in _catesList) {
            info.image = [UIImage imageNamed:[NSString stringWithFormat:@"goodsCate_%lu.png", (unsigned long)info.cateId]];
        }
    } else {
         NSLog(@"---------loadGoodsCateXml file failed ...");
    }
    return ret;
}


#pragma mark - NSXMLParse delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"row"]) {
        _currentCateInfo = [[QMGoodsCateInfo alloc]init];
    }
    if ([elementName isEqualToString:@"id"] || [elementName isEqualToString:@"name"] || [elementName isEqualToString:@"desc"]) {
        [_currentParsedCharData setString:@"ok"];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"id"]) {
        _currentCateInfo.cateId = [_currentParsedCharData intValue];
    }
    else if ([elementName isEqualToString:@"name"]){
        _currentCateInfo.name = [NSString stringWithString:_currentParsedCharData];
    }
    else if ([elementName isEqualToString:@"desc"]){
        _currentCateInfo.desc = [NSString stringWithString:_currentParsedCharData];
    }
    else if ([elementName isEqualToString:@"row"]){
        [_catesList addObject:_currentCateInfo];
    }
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([_currentParsedCharData isEqualToString:@"ok"]) {
        [_currentParsedCharData setString:string];
    }
    else{
        [_currentParsedCharData appendString:string];
    }
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    [_catesList removeAllObjects];
    if (!_catesList) {
        _catesList = [[NSMutableArray alloc] init];
    }
    
    _currentParsedCharData = [[NSMutableString alloc] init];
    
}
- (void)parserDidEndDocument:(NSXMLParser *)parser
{
}

@end
