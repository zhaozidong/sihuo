//
//  EditInfoViewController.m
//  QMMM
//
//  Created by kingnet  on 15-1-22.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "EditInfoViewController.h"
#import "UserInfoHandler.h"
#import "UserEntity.h"

@interface EditInfoViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) UITextField *textField;

@end

@implementation EditInfoViewController

- (id)initWithType:(EditInfoType)type
{
    self = [super init];
    if (self) {
        self.editInfoType = type;
    }
    return self;
}

- (NSString *)getTitle
{
    if (self.editInfoType == QMEditNickname) {
        return @"昵称";
    }
    else if (self.editInfoType == QMEditEmail){
        return @"邮箱";
    }
    return @"";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    self.navigationItem.title = [self getTitle];
    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(finishAction:) title:nil size:CGSizeMake(44.f, 44.f) imageName:@"navRight_finish_btn"];
    
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, kMainTopHeight+20, kMainFrameWidth, 45)];
    view.backgroundColor = [UIColor whiteColor];
    [view addSubview:self.textField];
    
    [self.view addSubview:view];

    if (self.text && self.text.length >0) {
        self.textField.text = self.text;
    }
    
    [self.textField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITextField *)textField
{
    if (!_textField) {
        _textField = [[UITextField alloc]initWithFrame:CGRectMake(15, 0, kMainFrameWidth-30, 45)];
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.borderStyle = UITextBorderStyleNone;
        _textField.textColor = kDarkTextColor;
        _textField.returnKeyType = UIReturnKeyDone;
        _textField.delegate = self;
    }
    return _textField;
}

- (void)finishAction:(id)sender
{
    //完成
    NSString *inputStr = self.textField.text;
    if (inputStr==nil || inputStr.length == 0) {
        if (self.editInfoType == QMEditNickname) {
            [AppUtils showAlertMessage:@"请输入您的昵称"];
        }
        else if (self.editInfoType == QMEditEmail){
            [AppUtils showAlertMessage:@"请输入您的邮箱"];
        }
        return;
    }
    
    [self.textField resignFirstResponder];
    
    UserEntity *user = [UserInfoHandler sharedInstance].currentUser;
    UserEntity *entity = [[UserEntity alloc]initWithUserEntity:user];
    if (self.editInfoType == QMEditNickname) {
        entity.nickname = self.textField.text;
    }
    else if (self.editInfoType == QMEditEmail){
        entity.email = self.textField.text;
    }
    
    [AppUtils showProgressMessage:@"正在提交"];
    EditInfoViewController __weak *weakSelf = self;
    [[UserInfoHandler sharedInstance]updateUserInfo:entity success:^(id obj) {
        [AppUtils dismissHUD];
        [weakSelf.navigationController dismissViewControllerAnimated:YES completion:nil];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self finishAction:nil];
    return YES;
}

@end
