//
//  MyGoodsList.m
//  QMMM
//
//  Created by 韩芦 on 14-9-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MyGoodsList.h"
#import "GoodsApi.h"
#import "GoodEntity.h"
#import "GoodsHandler.h"
#import "GoodsDataHelper.h"
#import "UserDefaultsUtils.h"

@interface MyGoodsList () {
    
    // 服务端商品列表
    NSMutableArray      *_serverGoodsList;
    // 本地缓存商品列表
    NSMutableArray      *_localGoodsList;
    
    // 加载状态
    NSInteger _serverLoadState;
    NSInteger _localLoadState;
    
    // 是否已加载过
    BOOL _hasServerListLoaded;
    BOOL _hasLocalListLoaded;
    
    // 是否是从服务端拉取第一页（可能是第一次或重新拉取第一页）
    BOOL _serverFirstPageLoading;
}

@end

@implementation MyGoodsList

- (id)initWithType:(GoodsListType)type
{
    self = [super init];
    if(self) {
        _listType = type;
        
        _serverGoodsList = [[NSMutableArray alloc] initWithCapacity:kDefaultGoodsSize];
        _localGoodsList = [[NSMutableArray alloc] initWithCapacity:kDefaultGoodsSize];
        
        _hasServerListLoaded = NO;
        _hasLocalListLoaded = NO;
    }
    return self;
}

#pragma mark -

- (NSArray *)goodsList
{
    if (_hasServerListLoaded)
        return _serverGoodsList;
    else
        return _localGoodsList;
}

- (BOOL)loading
{
    return _localLoadState == kTTGoodsLoadStateLoading || _serverLoadState == kTTGoodsLoadStateLoading;
}

- (BOOL)endOfAll
{
    if (_hasServerListLoaded)
        return _serverLoadState == kTTGoodsLoadStateLoadEnd;
    else
        return NO;
}

- (BOOL)hasAnyLoaded
{
    @synchronized(self) {
        return _hasServerListLoaded || _hasLocalListLoaded;
    }
}

- (void)deleteGoods:(uint64_t)goodsId
{
    for (int i = 0; i < [_serverGoodsList count]; i ++) {
        GoodEntity * info = [_serverGoodsList objectAtIndex:i];
        if(info.gid == goodsId) {
            [_serverGoodsList removeObjectAtIndex:i];
            break;
        }
    }
    
    for (int i = 0; i < [_localGoodsList count]; i ++) {
        GoodEntity * info = [_localGoodsList objectAtIndex:i];
        if(info.gid == goodsId) {
            [_localGoodsList removeObjectAtIndex:i];
            break;
        }
    }
}

#pragma mark -

- (BOOL)loadNextPage:(BOOL)firstPage
{
    BOOL resultLocal = NO;
    BOOL resultServer = NO;
    
    @synchronized(self) {
        if (firstPage) { //拉取首页
            if (!_hasServerListLoaded && !_hasLocalListLoaded) {
                //本地与服务端都未拉取过
                 resultLocal = [self loadNextPageFromLocal:YES];
            }
            //曾拉取过，则重新从服务端拉取
            resultServer = [self loadNextPageFromServer:YES];
        }
        else {//非首页
            if (!_hasServerListLoaded) { //且未从服务端拉取过，则再次从本地读取
                if (_localLoadState != kTTGoodsLoadStateLoading &&
                    _localLoadState != kTTGoodsLoadStateLoadEnd) {
                    resultLocal = [self loadNextPageFromLocal:NO];
                }
            }
            
            //如果服务端数据未拉取完毕，则继续分页拉取
            if (_serverLoadState != kTTGoodsLoadStateLoading &&
                _serverLoadState != kTTGoodsLoadStateLoadEnd) {
                if (!_hasServerListLoaded)
                    resultServer = [self loadNextPageFromServer:NO];
                else
                    resultServer = [self loadNextPageFromServer:NO];
            }
        }
    }
    
    return resultLocal || resultServer;
}

- (void)changeLoadState:(NSInteger)localState serverState:(NSInteger)serverState
{
    BOOL bStateChanged = NO;
    
    if (_localLoadState != localState) {
        bStateChanged = YES;
        _localLoadState = localState;
    }
    if (_serverLoadState != serverState) {
        bStateChanged = YES;
        _serverLoadState = serverState;
    }
    
    if (bStateChanged) {
        NSDictionary * dict = @{@"type":@(_listType)};
        [[NSNotificationCenter defaultCenter] postNotificationName:kGoodsListLoadStateNotify object:nil userInfo:dict];
    }
}

- (void)changeLocalLoadState:(NSInteger)newState
{
    [self changeLoadState:newState serverState:_serverLoadState];
}

- (void)changeServerLoadState:(NSInteger)newState
{
    [self changeLoadState:_localLoadState serverState:newState];
}

#pragma mark - server goodslist load logic

- (BOOL)loadNextPageFromServer:(BOOL)firstPage
{
    uint64_t lastGoodsId = 0;
    
    @synchronized(_serverGoodsList) {
        if (_serverLoadState == kTTGoodsLoadStateLoading) { //正在加载
            DLog(@"loadNextPageFromServer fail, it's still loading...");
            return NO;
        }
        
        if (!firstPage && _serverLoadState == kTTGoodsLoadStateLoadEnd) {//分页加载完毕
            DLog(@"getNextPageGoodsListFromServer fail, it's end");
            return NO;
        }
        
        _serverFirstPageLoading = firstPage;
        [self changeLoadState:_localLoadState serverState:kTTGoodsLoadStateLoading];
        
        if (!_serverFirstPageLoading && _serverGoodsList.count > 0) {
            GoodEntity * goodsInfo = [_serverGoodsList lastObject];
            lastGoodsId = goodsInfo.gid;
        }
    }
    
    uint type = (_listType == EGLT_RECOMMEND) ? 2 : 1;
    [[GoodsApi shareInstance] getRecommendGoodslist:type top:firstPage lastGoodsId:lastGoodsId context:^(id result) {
        [self loadNextPageFromServerCallback:result];
    }];
    
    return YES;
}

- (void)loadNextPageFromServerCallback:(id)dictParams
{
    BOOL bLocalListChanged = NO;
    BOOL bServerListChanged = NO;

    @synchronized(_serverGoodsList) {
        if(_serverLoadState != kTTGoodsLoadStateLoading) {
            NSLog(@"loadNextPageFromServerCallback ignore, it's not loading...");
            return;
        }
        
        id params=[dictParams objectForKey:@"list"];
        
//count>0则清空所有本地数据，如果count为0则不清空
        
        if(params && [params count] && [params isKindOfClass:[NSArray class]]) {
            bServerListChanged = YES;
            
            NSArray * tempArray = [NSArray arrayWithArray:params];
            
            // 如果是从服务端拉到首页数据，清空本地缓存记录
            BOOL isNew = !_hasServerListLoaded || _serverFirstPageLoading;
            _hasServerListLoaded = YES;
            if (isNew) {
                @synchronized(_localGoodsList) {
                    bLocalListChanged = [_localGoodsList count] != 0;
                    [_localGoodsList removeAllObjects];
                    _hasLocalListLoaded = NO;
                    [self changeLoadState:kTTGoodsLoadStateNone serverState:_serverLoadState];
                    
                    // 从数据库中删除所有数据
                    [[GoodsHandler shareGoodsHandler].goodsHelper asyncDeleteAllRecommendList:nil type:_listType];
                    
                    //保存第一个key
                    if (tempArray && [tempArray count] && [[tempArray objectAtIndex:0] objectForKey:@"key"]) {
                        //保存第一个key
                            if (tempArray && [tempArray count] && [[tempArray objectAtIndex:0] objectForKey:@"key"]) {
                                if (_listType==EGLT_RECOMMEND) {//recommend
                                    [UserDefaultsUtils saveValue:[[tempArray objectAtIndex:0] objectForKey:@"key"] forKey:@"firstRecommendKey"];
                                }else{//friend
                                    [UserDefaultsUtils saveValue:[[tempArray objectAtIndex:0] objectForKey:@"key"] forKey:@"firstFriendKey"];
                                }
                            }
                            if (_serverFirstPageLoading) {
                                NSDictionary * dict = @{@"number":[NSNumber numberWithInteger:[[dictParams objectForKey:@"update_num"] integerValue]]};
                                [[NSNotificationCenter defaultCenter] postNotificationName:kServerGoodsNumUpdateNotify object:nil userInfo:dict];
                            }
                    }
 
                }
                // TODO: 这里以后是否可以考虑做合并处理？
                // 清空Goods列表
                [_serverGoodsList removeAllObjects];
            }
            
            // 加入servergoods缓存列表
            NSMutableArray * newArray = [[NSMutableArray alloc] init];
            for (NSDictionary * dict in tempArray) {
                GoodEntity * goods = [[GoodEntity alloc] initWithDictionary:dict];
                [_serverGoodsList addObject:goods];
                [newArray addObject:goods];
                
                //下载音频数据
                [[GoodsHandler shareGoodsHandler] downloadAudio:goods.msgAudio complete:nil];
            }
            

            
            // 保存数据到数据库
            if([newArray count] > 0) {
                [[GoodsHandler shareGoodsHandler].goodsHelper asyncAddRecommendList:nil type:_listType list:newArray];

                if (_listType==EGLT_RECOMMEND) {//recommend
                    //保存最后一个key,下次刷新的时候发送给服务器
                    [UserDefaultsUtils saveValue:[[tempArray objectAtIndex:[tempArray count]-1] objectForKey:@"key"] forKey:@"lastRecommendKey"];
                }else{
                    [UserDefaultsUtils saveValue:[[tempArray objectAtIndex:[tempArray count]-1] objectForKey:@"key"] forKey:@"lastFriendKey"];
                }
            }
            
            _serverFirstPageLoading = NO;
            
            //为空，则表示服务端已分页加载到最后一页了
            BOOL bEnd = [tempArray count] == 0;
            [self changeLoadState:_localLoadState serverState:bEnd ? kTTGoodsLoadStateLoadEnd : kTTGoodsLoadStateLoadDone];
        } else {
            if (_serverFirstPageLoading) {
                NSDictionary * dict = @{@"number":[NSNumber numberWithInteger:0]};
                [[NSNotificationCenter defaultCenter] postNotificationName:kServerGoodsNumUpdateNotify object:nil userInfo:dict];
            }
            _serverFirstPageLoading = NO;
            [self changeLoadState:_localLoadState serverState:kTTGoodsLoadStateLoadFail];
        }
    }
    
    NSDictionary * dict = @{@"type":@(_listType)};
    [[NSNotificationCenter defaultCenter] postNotificationName:kGoodsListLoadedNotify object:nil userInfo:dict];
    
//    // 通知UI
//    if (bLocalListChanged || bServerListChanged) {
//        NSDictionary * dict = @{@"type":@(_listType)};
//        [[NSNotificationCenter defaultCenter] postNotificationName:kGoodsListLoadedNotify object:nil userInfo:dict];
//    }
}

#pragma mark - local goodslist load logic

- (BOOL)loadNextPageFromLocal:(BOOL)firstPage
{
    if (_hasServerListLoaded) {
        NSLog(@"loadNextPageFromLocal fail, goods already load from server...");
        return NO;
    }
    
    uint64_t lastGoodsId = 0;
    
    @synchronized(_localGoodsList) {
        if (_localLoadState == kTTGoodsLoadStateLoading) {
            NSLog(@"loadNextPageFromLocal fail, it's still loading...");
            return NO;
        }
        
        if (_localLoadState == kTTGoodsLoadStateLoadEnd) {
            NSLog(@"loadNextPageFromLocal fail, it's end");
            return NO;
        }
        
        [self changeLocalLoadState:kTTGoodsLoadStateLoading];
        
        if (_localGoodsList.count > 0) {
            GoodEntity * info = _localGoodsList.lastObject;
            lastGoodsId = info.gid;
        }
    }
    
    // 从数据库加载
    [[GoodsHandler shareGoodsHandler].goodsHelper asyncLoadRecommendList:^(id result) {
        [self loadNextPageFromLocalCallback:result];
    } type:_listType fromId:lastGoodsId count:kDefaultGoodsSize];
    
    return YES;
}

- (void)loadNextPageFromLocalCallback:(id)result
{
    @synchronized(_localGoodsList) {
        if (_localLoadState != kTTGoodsLoadStateLoading || _hasServerListLoaded) {
            NSLog(@"getLocalGoodsListFromDBCallback ignore, it's not loading...");
            return;
        }
        
        if (!_hasLocalListLoaded) {
            _hasLocalListLoaded = result != nil;
        }
        
        if(result && [result isKindOfClass:[NSArray class]]) {
            BOOL bEnd = NO;
            for (int i = 0; i < [result count]; i++) {
                GoodEntity * data =  [result objectAtIndex:i];
                // 最后一条为null说明本地没有更多缓存了
                if([data isMemberOfClass:[NSNull class]]) {
                    bEnd = YES;
                    break;
                }
                
                [_localGoodsList addObject:data];
            }
            
            [self changeLocalLoadState:bEnd ? kTTGoodsLoadStateLoadEnd : kTTGoodsLoadStateLoadDone];
        } else {
            [self changeLocalLoadState:kTTGoodsLoadStateLoadFail];
        }
    }
    
    // 通知UI
    NSDictionary * dict = @{@"type":@(_listType)};
    [[NSNotificationCenter defaultCenter] postNotificationName:kGoodsListLoadedNotify object:nil userInfo:dict];
}

@end