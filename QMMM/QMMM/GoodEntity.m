//
//  GoodEntity.m
//  QMMM
//
//  Created by 韩芦 on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodEntity.h"
#import "NSString+Empty.h"
#import "FMResultSet.h"
#import "NSString+JSONCategories.h"
#import "UserInfoHandler.h"
#import "UserEntity.h"

@implementation GoodEntity

- (id)initWithFMResultSet:(FMResultSet *)resultSet
{
    self = [super init];
    if(self) {
        self.gid = [resultSet unsignedLongLongIntForColumnIndex:0];
        NSString * temp = [resultSet stringForColumnIndex:1];
        self.picturelist = [[GoodsPictureList alloc] initWithArray:[temp toArray]];
        self.desc = [resultSet stringForColumnIndex:2];
        self.price = [resultSet doubleForColumnIndex:3];
        self.orig_price = [resultSet doubleForColumnIndex:4];
        self.cate_type = [resultSet intForColumnIndex:5];
        self.city = [resultSet stringForColumnIndex:6];
        self.msgAudio = [resultSet stringForColumnIndex:7];
        self.msgAudioSeconds = (uint)[resultSet intForColumnIndex:8];
        self.pub_time = [resultSet intForColumnIndex:9];
        self.sellerUserId = [resultSet unsignedLongLongIntForColumnIndex:10];
        self.sellerHeadIcon = [resultSet stringForColumnIndex:11];
        self.sellerNick = [resultSet stringForColumnIndex:12];
        temp = [resultSet stringForColumnIndex:13];
        self.favorList = [[FavorList alloc] initWithArray:[temp toArray]];
        self.favorCounts = [resultSet intForColumnIndex:14];
        self.commentCounts = [resultSet intForColumnIndex:15];
        self.isFavor = [resultSet boolForColumnIndex:16];
//        self.is_friend = [resultSet boolForColumnIndex:17];
        self.is_friend = [resultSet intForColumnIndex:17];
        self.whose_friend = [resultSet stringForColumnIndex:18];
        self.friendId=[resultSet stringForColumnIndex:19];
        self.number = [resultSet intForColumnIndex:20];
        self.fNum = [resultSet intForColumnIndex:21];
        self.isNew = [resultSet intForColumnIndex:22];
        self.status = [resultSet intForColumnIndex:23];
        self.update_time = [resultSet intForColumnIndex:24];
        self.isSpecialGoods = [resultSet boolForColumnIndex:25];
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self) {
        
        id temp = [dict objectForKey:@"id"];
        if(temp && [temp isKindOfClass:[NSNumber class]]) {
            self.gid = [temp unsignedLongLongValue];
        } else if(temp && [temp isKindOfClass:[NSString class]]) {
            self.gid = (uint64_t)[temp longLongValue];
        }
        
        temp = [dict objectForKey:@"photo"];
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            self.picturelist = [[GoodsPictureList alloc] initWithArray:temp];
        }
        
        self.desc = [dict objectForKey:@"desc"];
        if([self.desc isKindOfClass:[NSNull class]]) {
            self.desc = @"";
        }
        
        temp = [dict objectForKey:@"price"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            self.price = [temp floatValue];
        } else if( temp && [temp isKindOfClass:[NSNumber class]]) {
            self.price = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"ori_price"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            self.orig_price = [temp floatValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            self.orig_price = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"category"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            self.cate_type = [temp floatValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            self.cate_type = [temp floatValue];
        }
        
        self.city = [dict objectForKey:@"loc"];
        if([self.city isKindOfClass:[NSNull class]]) {
            self.city = @"";
        }

        self.msgAudio = [dict objectForKey:@"audio"];
        if([self.msgAudio isKindOfClass:[NSNull class]]) {
            self.msgAudio = @"";
        }
        
        temp = [dict objectForKey:@"audio_t"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            self.msgAudioSeconds = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            self.msgAudioSeconds = [temp intValue];
        }
        
        temp = [dict objectForKey:@"pub_ts"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            self.pub_time = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            self.pub_time = [temp intValue];
        }
        
        temp = [dict objectForKey:@"uid"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            self.sellerUserId = (uint64_t)[temp longLongValue];
         } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            self.sellerUserId = [temp unsignedLongLongValue];
        }
            
        NSString * tempString = [dict objectForKey:@"avatar"];
        if(tempString && ![tempString isKindOfClass:[NSNull class]]) {
            self.sellerHeadIcon = tempString;
        }
        
        self.sellerNick = [dict objectForKey:@"uName"];
        if([self.sellerNick isKindOfClass:[NSNull class]]) {
            self.sellerNick = @"";
        }
        
        temp = [dict objectForKey:@"praiseInfo"];
        if(temp && [temp isKindOfClass:[NSArray class]]) {
             self.favorList = [[FavorList alloc] initWithArray:(NSArray *)temp];
        }
        
        temp = [dict objectForKey:@"pNum"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            self.favorCounts = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            self.favorCounts = [temp intValue];
        }
        
        temp = [dict objectForKey:@"cNum"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            self.commentCounts = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            self.commentCounts = [temp intValue];
        }

        temp = [dict objectForKey:@"isPraise"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            self.isFavor = [temp intValue];
        } else if(temp && [temp isKindOfClass:[NSNumber class]]) {
            self.isFavor = [temp intValue];
        }
        
        temp = [dict objectForKey:@"relation"];
        if(temp && [temp isKindOfClass:[NSDictionary class]]) {
            id obj = [temp objectForKey:@"type"];
            if(obj && [obj isKindOfClass:[NSNumber class]]) {
                self.is_friend = [obj intValue];
            }
            
            obj = [temp objectForKey:@"friend_name"];
            if(obj && [obj isKindOfClass:[NSString class]]) {
                self.whose_friend = obj;
            } else {
                self.whose_friend = @"";
            }
            
            obj = [temp objectForKey:@"friend_uid"];
            if(obj && [obj isKindOfClass:[NSNumber class]]) {
                self.friendId = [NSString stringWithFormat:@"%@",obj];
            }else if (obj && [obj isKindOfClass:[NSString class]]){
                self.friendId = obj;
            }else {
                self.friendId = @"";
            }
        }
        
        //新增字段
        temp = [dict objectForKey:@"num"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.number = [temp intValue];
        }
        else if(temp && [temp isKindOfClass:[NSNumber class]]){
            self.number = [temp intValue];
        }
        
        temp = [dict objectForKey:@"fNum"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.fNum = [temp intValue];
        }
        else if(temp && [temp isKindOfClass:[NSNumber class]]){
            self.fNum = [temp intValue];
        }
        
        temp = [dict objectForKey:@"usage"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.isNew = [temp intValue];
        }
        else if(temp && [temp isKindOfClass:[NSString class]]){
            self.isNew = [temp intValue];
        }
        
        temp = [dict objectForKey:@"status"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.status = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.status = [temp intValue];
        }
        
        temp = [dict objectForKey:@"update_ts"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.update_time = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.update_time = [temp intValue];
        }
        
        temp = [dict objectForKey:@"freight"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.freight = [temp floatValue];
        }
        else if ([temp isKindOfClass:[NSString class]]){
            self.freight = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"isBoutique"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.isSpecialGoods = [temp boolValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.isSpecialGoods = [temp boolValue];
        }
        
        temp = [dict objectForKey:@"lock_num"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.lockNum = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.lockNum = [temp intValue];
        }
        
        temp = [dict objectForKey:@"sys_ts"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.curSysTime = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.curSysTime = [temp intValue];
        }
        
        temp = [dict objectForKey:@"trust"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.trust = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.trust = [temp intValue];
        }
    }
    return self;
}

- (id)initWithGoodsEntity:(GoodEntity *)entity
{
    self = [super init];
    if (self) {
        [self assignValue:entity];
    }
    return self;
}

- (void)assignValue:(GoodEntity *)goodInfo
{
    self.gid = goodInfo.gid;
    self.picturelist = goodInfo.picturelist;
    self.desc = goodInfo.desc;
    self.price = goodInfo.price;
    self.orig_price = goodInfo.orig_price;
    self.cate_type = goodInfo.cate_type;
    self.city = goodInfo.city;
    self.msgAudio = goodInfo.msgAudio;
    self.msgAudioSeconds = goodInfo.msgAudioSeconds;
    self.pub_time = goodInfo.pub_time;
    self.sellerUserId = goodInfo.sellerUserId;
    self.sellerHeadIcon = goodInfo.sellerHeadIcon;
    self.sellerNick = goodInfo.sellerNick;
    self.favorList = goodInfo.favorList;
    self.favorCounts = goodInfo.favorCounts;
    self.commentCounts = goodInfo.commentCounts;
    self.isFavor = goodInfo.isFavor;
    self.number = goodInfo.number;
    self.fNum = goodInfo.fNum;
    self.isNew = goodInfo.isNew;
    self.location = goodInfo.location;
    self.status = goodInfo.status;
    self.update_time = goodInfo.update_time;
    self.freight = goodInfo.freight;
    self.isSpecialGoods = goodInfo.isSpecialGoods;
    self.lockNum = goodInfo.lockNum;
    self.curSysTime = goodInfo.curSysTime;
    self.trust = goodInfo.trust;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"gid : %qu, picturelist : %@, desc : %@, price : %fd, orig_price : %fd, cate_type : %d, \
            city : %@, msgAudio : %@, audiosecond: %u, pub_time : %d, sellerUserId : %qu, sellerHeadIcon : %@, sellerNick : %@, favorList : %@, favorCounts : %d, commentCounts : %d, isFavor : %u, isNew:%d fNum:%d, number:%d, location:%@, status:%d, update_time:%d, freight:%.2f isSpecialGoods:%d, lockNum:%d, curSysTime:%d trust:%d",
            self.gid,
            self.picturelist,
            self.desc,
            self.price,
            self.orig_price,
            self.cate_type,
            self.city,
            self.msgAudio,
            self.msgAudioSeconds,
            self.pub_time,
            self.sellerUserId,
            self.sellerHeadIcon,
            self.sellerNick,
            self.favorList,
            self.favorCounts,
            self.commentCounts,
            self.isFavor,
            self.isNew,
            self.fNum,
            self.number,
            self.location,
            self.status,
            self.update_time,
            self.freight,
            self.isSpecialGoods,
            self.lockNum,
            self.curSysTime,
            self.trust
            ];
}

- (void)addFavor:(uint64_t)userId
{
    _isFavor = !_isFavor;
    
    if(_isFavor) {
        _favorCounts ++;
        FavorInfo * info = [[FavorInfo alloc] init];
        info.uid = userId;
        info.uicon = [UserInfoHandler sharedInstance].currentUser.avatar;
        info.nickName = [UserInfoHandler sharedInstance].currentUser.nickname;
        [_favorList addFavor:info];
    } else {
        _favorCounts --;
        
        [_favorList deleteFavor:userId];
    }
}

+ (NSArray *)usageDesc
{
    return @[@"全新", @"几乎全新", @"较新", @"半新", @"较旧"];
}

//- (NSString *)getNewPrecent
//{
//    NSString * retString = nil;
//    
//    switch (_isNew) {
//        case 0:
//            retString = @"全新";
//            break;
//        case 1:
//            retString = @"九成新";
//            break;
//        case 2:
//            retString = @"八成新";
//            break;
//        case 3:
//            retString = @"七成新";
//            break;
//        case 4:
//            retString = @"六成新";
//            break;
//        case 5:
//            retString = @"五成新";
//            break;
//        case 6:
//            retString = @"无成新以下";
//            break;
//        default:
//            retString = @"未知";
//            break;
//    }
//    return retString;
//}

@end
