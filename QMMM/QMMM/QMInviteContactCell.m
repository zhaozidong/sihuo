//
//  QMInviteContactCell.m
//  QMMM
//
//  Created by hanlu on 14-10-31.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMInviteContactCell.h"
#import "ContactInfo.h"

NSString * const kQMInviteContactCellIdentifier = @"kQMInviteContactCellIdentifier";

@implementation QMInviteContactCell

+ (id)cellForInviteContact
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMInviteContactCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [_inviteBtn setTitle:@"邀请" forState:UIControlStateNormal];
    [_inviteBtn addTarget:self action:@selector(inviteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(ContactInfo *)info
{
    _contactInfo = info;

    _nameLabel.text = _contactInfo.personName;
    
    if ([info.personName isEqualToString:@"来自通讯录的好友"]) {
        [_inviteBtn setTitle:@"全部邀请" forState:UIControlStateNormal];
    }else{
        [_inviteBtn setTitle:@"邀请" forState:UIControlStateNormal];
    }
}

- (void)updateUIWithContact:(ContactInfo *)info andSearchInfo:(NSString *)strSearch{
    
    _contactInfo = info;
    
//    _nameLabel.text = _contactInfo.personName;
    
    NSRange range=[_contactInfo.personName rangeOfString:strSearch];
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:_contactInfo.personName];
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:range];
    _nameLabel.attributedText=attString;
    
    if ([info.personName isEqualToString:@"来自通讯录的好友"]) {
        [_inviteBtn setTitle:@"全部邀请" forState:UIControlStateNormal];
    }else{
        [_inviteBtn setTitle:@"邀请" forState:UIControlStateNormal];
    }
}

- (void)inviteBtnClick:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(didInviteContactClick:)]) {
        [_delegate didInviteContactClick:_contactInfo];
    }
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.contentView.frame = self.bounds;
}

@end
