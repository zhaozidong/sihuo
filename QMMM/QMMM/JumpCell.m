//
//  JumpCell.m
//  QMMM
//
//  Created by kingnet  on 14-9-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "JumpCell.h"
#import "QMNumView.h"

NSString * const kJumpCellIdentifier = @"kJumpCellIdentifier";

@interface JumpCell ()
@property (weak, nonatomic) IBOutlet UIImageView *rightImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *numLbl;
@property (weak, nonatomic) IBOutlet QMNumView *redNumView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *numViewWidthCst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *numViewHeightCst;

@end
@implementation JumpCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"JumpCell" bundle:[NSBundle mainBundle]];
}

- (void)awakeFromNib {
    // Initialization code
    self.titleLbl.text = @"";
    self.rightImg.image = nil;
    self.numLbl.hidden = YES;
    self.numLbl.text = @"";
    self.numLbl.textColor = UIColorFromRGB(0x99a0aa);
    self.redNumView.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(NSIndexPath *)indexPath
{
    self.indexPath = indexPath;
    if (indexPath.section == 1) {
        self.titleLbl.text = [[[self class]items] objectAtIndex:indexPath.row];
        NSString *imgName = [[[self class]imageNames]objectAtIndex:indexPath.row];
        self.rightImg.image = [UIImage imageNamed:imgName];
    }
    else if(indexPath.section == 2){
        self.titleLbl.text = [[[self class]items] objectAtIndex:indexPath.row+3];
        NSString *imgName = [[[self class]imageNames]objectAtIndex:indexPath.row+3];
        self.rightImg.image = [UIImage imageNamed:imgName];
    }
    else if(indexPath.section == 3){
        self.titleLbl.text = [[[self class]items] objectAtIndex:indexPath.row+6];
        NSString *imgName = [[[self class]imageNames]objectAtIndex:indexPath.row+6];
        self.rightImg.image = [UIImage imageNamed:imgName];
    }
}

+ (NSArray *)items
{
    return @[@"我的主页", @"我的朋友", @"我的钱包", @"在卖的商品", @"已卖掉的商品", @"已买到的商品", @"设置"];
}

+ (NSArray *)imageNames
{
    return @[@"person_dark_icon", @"friends_icon", @"wallet_icon", @"selling_icon", @"sold_icon", @"buyed_icon", @"setting_icon"];
}

+ (CGFloat)rowHeight
{
    return 45.f;
}

- (void)setNum:(int)num
{
    if (num > 0) {
        self.redNumView.hidden = NO;
        self.redNumView.isRedDot = NO;
        self.numViewHeightCst.constant = 16.f;
        [self.redNumView setNum:num];
        self.numViewWidthCst.constant = self.redNumView.width;
        
        if ((self.indexPath.section == 1 && self.indexPath.row == 2) || (self.indexPath.section==3 && self.indexPath.row==0)){
            //我的钱包或有新版本
            self.redNumView.isRedDot = YES;
            [self.redNumView setNum:0];
            self.numViewWidthCst.constant = 8.f;
            self.numViewHeightCst.constant = 8.f;
        }
    }
    else{
        self.numLbl.hidden = YES;
        self.redNumView.hidden = YES;
    }
}

@end
