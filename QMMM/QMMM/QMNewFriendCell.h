//
//  QMNewFriendCell.h
//  QMMM
//
//  Created by hanlu on 14-10-31.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMNewFriendCellIdentifier;

@protocol  QMNewFriendDelegate;

@class MsgData;

@interface QMNewFriendCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * nameLabel;

@property (nonatomic, strong) IBOutlet UILabel * desclabel;

@property (nonatomic, strong) IBOutlet UIButton * ctnBtn;

@property (nonatomic, strong) IBOutlet UILabel * statusLabel;

@property (nonatomic, weak) id<QMNewFriendDelegate> delegate;

+ (id)cellForNewFriend;

- (void)updateUI:(MsgData *)msgData;

@end

@protocol  QMNewFriendDelegate <NSObject>

- (void)didAgreeBtnClick:(MsgData *)info;

@end