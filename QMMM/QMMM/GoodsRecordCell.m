//
//  GoodsRecordCell.m
//  QMMM
//
//  Created by kingnet  on 14-11-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsRecordCell.h"
#import "LCVoice.h"
#import "ToggleButton.h"
#import "GoodsHandler.h"
#import "CircularProgressView.h"
#import "UIButton+Addition.h"
#import "UserRightsManager.h"

@interface GoodsRecordCell ()<CircularProgressViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *tipLbl;
@property (weak, nonatomic) IBOutlet UIButton *recordBtn;

//@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) BOOL isRecording; //是否在录音
@property (nonatomic, strong)LCVoice *voice;
@property (nonatomic, strong) NSData *curAudio;
@property (weak, nonatomic) IBOutlet ToggleButton *playOrStopButton;
@property (weak, nonatomic) IBOutlet CircularProgressView *circularProgressView;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deleteXCst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressVXCst;
@property (nonatomic, strong) CADisplayLink *displayLink;

@end

static int count = 0;

@implementation GoodsRecordCell

+ (GoodsRecordCell *)goodsRecordCell
{
    return [[[NSBundle mainBundle]loadNibNamed:@"GoodsRecordCell" owner:self options:0] lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.tipLbl.text = @"别害羞，说说私货背后的故事，可以大幅度提高成功率哦";
    self.tipLbl.textColor = UIColorFromRGB(0x4f4f4f);
    [self.recordBtn setTitleColor:UIColorFromRGB(0xb7bfc9) forState:UIControlStateNormal];
    [self.recordBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.recordBtn setTitle:@"按住说两句" forState:UIControlStateNormal];
    
    //设置button的title下移
    CGPoint buttonBoundsCenter = CGPointMake(CGRectGetMidX(self.recordBtn.bounds), CGRectGetMidY(self.recordBtn.bounds));
    // 找出titleLabel最的center
    CGPoint endTitleLabelCenter = CGPointMake(buttonBoundsCenter.x, CGRectGetHeight(self.bounds)-CGRectGetMidY(self.recordBtn.titleLabel.bounds));
    // 取得titleLabel最初的center
    CGPoint startTitleLabelCenter = self.recordBtn.titleLabel.center;
    // 设置titleEdgeInsets
    
    CGFloat titleEdgeInsetsTop = endTitleLabelCenter.y-startTitleLabelCenter.y;
    
    CGFloat titleEdgeInsetsLeft = endTitleLabelCenter.x - startTitleLabelCenter.x;
    
    CGFloat titleEdgeInsetsBottom = -titleEdgeInsetsTop;
    
    CGFloat titleEdgeInsetsRight = -titleEdgeInsetsLeft;
    
    float iosV = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (iosV >= 8.0) {
        self.recordBtn.titleEdgeInsets = UIEdgeInsetsMake(-90, titleEdgeInsetsLeft, titleEdgeInsetsBottom, titleEdgeInsetsRight);
    }
    else{
        self.recordBtn.titleEdgeInsets = UIEdgeInsetsMake(-80, titleEdgeInsetsLeft+1, titleEdgeInsetsBottom, titleEdgeInsetsRight);
    }
    
    //circle progress
    self.circularProgressView.backColor = UIColorFromRGB(0xd8dee6);
    self.circularProgressView.progressColor = UIColorFromRGB(0xff4544);
    self.circularProgressView.lineWidth = 1.f;
    self.circularProgressView.delegate = self;
    
    [self setPlayOrStopButtonStyle];
    [self setDeleteBtnStyle];
    self.voice = [[LCVoice alloc]init];
    self.audioPath = @"";
    self.audioTime = 0;
}

- (void)setDeleteBtnStyle
{
    //delete button
    [self.deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    [self.deleteBtn setTitleColor:UIColorFromRGB(0xb7bfc9) forState:UIControlStateNormal];
    [self.deleteBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    //设置button的title下移
    CGPoint buttonBoundsCenter = CGPointMake(CGRectGetMidX(self.deleteBtn.bounds), CGRectGetMidY(self.deleteBtn.bounds));
    // 找出titleLabel最的center
    CGPoint endTitleLabelCenter = CGPointMake(buttonBoundsCenter.x, CGRectGetHeight(self.bounds)-CGRectGetMidY(self.deleteBtn.titleLabel.bounds));
    // 取得titleLabel最初的center
    CGPoint startTitleLabelCenter = self.deleteBtn.titleLabel.center;
    // 设置titleEdgeInsets
    
    CGFloat titleEdgeInsetsTop = endTitleLabelCenter.y-startTitleLabelCenter.y;
    
    CGFloat titleEdgeInsetsLeft = endTitleLabelCenter.x - startTitleLabelCenter.x;
    
    CGFloat titleEdgeInsetsBottom = -titleEdgeInsetsTop;
    
    CGFloat titleEdgeInsetsRight = -titleEdgeInsetsLeft;
    
    float iosV = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (iosV >= 8.0) {
        self.deleteBtn.titleEdgeInsets = UIEdgeInsetsMake(-120, titleEdgeInsetsLeft, titleEdgeInsetsBottom, titleEdgeInsetsRight);
    }
    else{
        self.deleteBtn.titleEdgeInsets = UIEdgeInsetsMake(-110, titleEdgeInsetsLeft+1, titleEdgeInsetsBottom, titleEdgeInsetsRight);
    }
}

- (void)setPlayOrStopButtonStyle
{
    //play button
    self.playOrStopButton.layer.cornerRadius = CGRectGetWidth(self.playOrStopButton.frame)/2;
    self.playOrStopButton.layer.masksToBounds = YES;
    [self.playOrStopButton setTitleColor:UIColorFromRGB(0xb7bfc9) forState:UIControlStateNormal];
    [self.playOrStopButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.playOrStopButton setTitleColor:UIColorFromRGB(0xb7bfc9) forState:UIControlStateSelected];
    
    //设置button的title下移
    CGPoint buttonBoundsCenter = CGPointMake(CGRectGetMidX(self.playOrStopButton.bounds), CGRectGetMidY(self.playOrStopButton.bounds));
    // 找出titleLabel最的center
    CGPoint endTitleLabelCenter = CGPointMake(buttonBoundsCenter.x, CGRectGetHeight(self.bounds)-CGRectGetMidY(self.playOrStopButton.titleLabel.bounds));
    // 取得titleLabel最初的center
    CGPoint startTitleLabelCenter = self.playOrStopButton.titleLabel.center;
    // 设置titleEdgeInsets
    
    CGFloat titleEdgeInsetsTop = endTitleLabelCenter.y-startTitleLabelCenter.y;
    
    CGFloat titleEdgeInsetsLeft = endTitleLabelCenter.x - startTitleLabelCenter.x;
    
    CGFloat titleEdgeInsetsBottom = -titleEdgeInsetsTop;
    
    CGFloat titleEdgeInsetsRight = -titleEdgeInsetsLeft;
    
    float iosV = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (iosV >= 8.0) {
        self.playOrStopButton.titleEdgeInsets = UIEdgeInsetsMake(-145, titleEdgeInsetsLeft-40, titleEdgeInsetsBottom, titleEdgeInsetsRight);
    }
    else{
        self.playOrStopButton.titleEdgeInsets = UIEdgeInsetsMake(-145, titleEdgeInsetsLeft-40, titleEdgeInsetsBottom, titleEdgeInsetsRight);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight
{
    return 160.f;
}

#pragma mark - Button Actions
- (IBAction)deleteRecordAction:(id)sender {
    self.curAudio = nil;
    //停止播放
    if (self.circularProgressView.playOrPauseButtonIsPlaying) {
        [self.circularProgressView stop];
        self.playOrStopButton.on = NO;
        self.circularProgressView.playOrPauseButtonIsPlaying = NO;
    }
    self.audioPath = @"";
    self.audioTime = 0;
    [self.recordBtn setTitle:@"按住说两句" forState:UIControlStateNormal];
    [self showOrHideSmallButton:YES];
}



//touch drag inside 手指移动到按钮内部
- (IBAction)recordBtnTouchDragIn:(id)sender {
    
}

//touch drag outside 手指移动到按钮外部
- (IBAction)recordBtnTouchDragOut:(id)sender {
    
}

//touch up outside 手指在按钮外部离开，取消录音
- (IBAction)recordBtnTouchUpOutside:(id)sender {
    if (!self.isRecording) {
        return;
    }
    
    self.isRecording = NO;
    
    [self.voice stopRecord];
    
    [self resetTimer];
}

//touch up inside 手指在按钮内部离开，完成录音
- (IBAction)endRecordAction:(id)sender {
    if (!self.isRecording) {
        return;
    }
  
    self.isRecording = NO;
    NSURL *url = [self.voice stopRecord];
    NSData *data = [NSData dataWithContentsOfURL:
                    url];
    NSTimeInterval interval = [LCVoice getAudioTime:data];
    int minutes = floor(interval/60);
    int seconds = round(interval - minutes * 60);
    
    if (data.length>0) {
        if (seconds <= 1) {
            [AppUtils showErrorMessage:@"说话时间太短"];
        }
        else{
            [self.recordBtn setTitle:@"按住重录" forState:UIControlStateNormal];
            if (self.audioTime == 0) {
                [self showOrHideSmallButton:YES];
            }
            self.curAudio = data;
            self.deleteBtn.enabled = YES;
            self.playOrStopButton.enabled = YES;
            
            self.audioTime = seconds;
            if (self.soundBlock) {
//                self.soundBlock(self.curAudio, seconds);
                self.soundBlock(self.voice.audioLocPath, seconds);
            }
            [self.playOrStopButton setTitle:[NSString stringWithFormat:@"%d’", seconds] forState:UIControlStateNormal];
        }
    }
    else{
        [AppUtils showErrorMessage:[AppUtils localizedProductString:@"QM_Tip_Sound"]];
    }
    [self resetTimer];
}

//touch down 录音按钮按下
- (IBAction)startRecordAciton:(id)sender {
    if (self.isRecording) {
        return;
    }
//    
//    if (self.delegate && [self.delegate respondsToSelector:@selector(canRecord)]) {
//        if (![self.delegate canRecord]) {
//            return;
//        }
//    }
    //判断是否开启麦克风
    __weak typeof(self) weakSelf = self;
    [[UserRightsManager sharedInstance] getMicroPhoneRights:^(QMRightType rights) {
        if (rights==QMRightTypeAuthorized) {
            weakSelf.isRecording = YES;
            if (weakSelf.circularProgressView.playOrPauseButtonIsPlaying) {
                [weakSelf.circularProgressView stop];
                weakSelf.playOrStopButton.on = NO;
                weakSelf.circularProgressView.playOrPauseButtonIsPlaying = NO;
            }
            weakSelf.deleteBtn.enabled = NO;
            weakSelf.playOrStopButton.enabled = NO;
            [weakSelf setTimer];
            [weakSelf.voice setCurRecordTime:count];
            [weakSelf.voice startRecord];
        }else if (rights==QMRightTypeDenied){
            [AppUtils showAlertMessage:@"请到设置里打开私货对您麦克风的访问权限"];
        }
    } failure:^{
        
    }];
}

- (IBAction)clickPlayOrStop:(ToggleButton *)sender {
    if (sender.on) {
        if (![self.audioPath isEqualToString:@""] && self.curAudio == nil) {
            NSString * savePath = [[GoodsHandler class] audioPath];
            NSRange range = [self.audioPath rangeOfString:@"/" options:NSBackwardsSearch];
            if(range.location != NSNotFound) {
                NSString * picName = [self.audioPath substringFromIndex:range.location];
                savePath = [savePath stringByAppendingPathComponent:picName];
            }
            BOOL bDirectory = NO;
            NSFileManager * fileManager = [NSFileManager defaultManager];
            if([fileManager fileExistsAtPath:savePath isDirectory:&bDirectory]) {
                NSURL *audioURL = [NSURL fileURLWithPath:savePath];
                //                        NSData *data = [NSData dataWithContentsOfFile:wavSuffix];
                //                        self.circularProgressView.audioData = data;
                self.circularProgressView.audioURL = audioURL;
            }
            else {
                DLog(@"-------audio file not exist: %@", savePath);
            }
        }
        else{
            //            self.circularProgressView.audioData = [self decodeAmr:self.curAudio];
            self.circularProgressView.audioData = self.curAudio;
        }
        
        [self.circularProgressView play];
        self.circularProgressView.playOrPauseButtonIsPlaying = YES;
    }
    else{
        [self.circularProgressView stop];
        self.playOrStopButton.on = NO;
        self.circularProgressView.playOrPauseButtonIsPlaying = NO;
    }
}

- (void)stopPlaying
{
    if (self.playOrStopButton.on) {
        [self.circularProgressView stop];
        self.playOrStopButton.on = NO;
        self.circularProgressView.playOrPauseButtonIsPlaying = NO;
    }
}

- (void)handleTimer
{
    if (count >0) {
        [self.voice setCurRecordTime:count];
    }
    if (count++>=60) {
        //录音1分钟时停止录音
        count = 0;
        [self endRecordAction:nil];
        [self resetTimer];
    }
}

-(void)resetTimer
{
    if (self.displayLink && !self.displayLink.paused) {
        self.displayLink.paused = YES;
        [self.displayLink invalidate];
        self.displayLink = nil;
        count = 0;
    }
}

//create and start timer
- (void)setTimer
{
    self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(handleTimer)];
    self.displayLink.paused = NO;
    self.displayLink.frameInterval = 60;
    [self.displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

#pragma mark - Circular Progress View Delegate method
- (void)updateProgressViewWithPlayer:(AVAudioPlayer *)player {
    //update timeLabel
    
}
- (void)updatePlayOrPauseButton{
    self.playOrStopButton.on = YES;
}

- (void)playerDidFinishPlaying{
    self.playOrStopButton.on = NO;
}

#pragma mark - Animation
- (void)showOrHideSmallButton:(BOOL)animate
{
    if (animate) {
        //动画
        [UIView beginAnimations:nil context:nil];
        //动画结束时变缓慢
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.25];
        [UIView setAnimationRepeatCount:1];
        
        [self setConstant];
        [UIView commitAnimations];
    }
    else{
        [self setConstant];
    }
}

- (void)setConstant
{
    //在中心位置
    if (self.deleteXCst.constant == 0) {
        self.deleteXCst.constant = 95;
        self.progressVXCst.constant = -95;
    }
    else{
        self.deleteXCst.constant = 0;
        self.progressVXCst.constant = 0;
    }
    [self.contentView setNeedsUpdateConstraints];
    [self.contentView layoutIfNeeded];
}

- (void)updateUI:(int)audioTime audioPath:(NSString *)audioPath
{
    if (audioTime > 0) {
        [self.recordBtn setTitle:@"按住重录" forState:UIControlStateNormal];
        if (self.audioTime == 0) {
            [self showOrHideSmallButton:NO];
        }
        
        self.audioTime = audioTime;
        self.audioPath = audioPath;
        self.deleteBtn.enabled = YES;
        self.playOrStopButton.enabled = YES;
        
        [self.playOrStopButton setTitle:[NSString stringWithFormat:@"%d’", self.audioTime] forState:UIControlStateNormal];
    }
}

@end
