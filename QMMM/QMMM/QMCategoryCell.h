//
//  QMCategoryCell.h
//  QMMM
//
//  Created by Derek on 15/3/16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityInfo.h"

@protocol QMCategoryCellDelegate <NSObject>

-(void)didTapCategoryCell:(CategoryEntity *)entity;

@end




@interface QMCategoryCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;


@property (weak, nonatomic) id<QMCategoryCellDelegate> delegate;

+ (QMCategoryCell *)categoryCell;

+ (CGFloat)cellHeight;

- (void)updateWithArr:(NSArray *)arr;

@end
