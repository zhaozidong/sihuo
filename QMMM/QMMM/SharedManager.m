//
//  SharedManager.m
//  QMMM
//
//  Created by Derek on 15/2/9.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "SharedManager.h"

#define BUNDLE_NAME @"Resource"

#define CONTENT NSLocalizedString(@"TEXT_SHARE_CONTENT", @"ShareSDK不仅集成简单、支持如QQ好友、微信、新浪微博、腾讯微博等所有社交平台，而且还有强大的统计分析管理后台，实时了解用户、信息流、回流率、传播效应等数据，详情见官网http://sharesdk.cn @ShareSDK")

@interface SharedManager(){
    ShareType _shareType;
    NSString *_strTitle;
    NSString *_strContent;
    NSString *_strURL;
    NSString *_imageURL;
    NSString *_strDesc;
}
@end


@implementation SharedManager

static SharedManager *sharedInstance=nil;

+(id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if(sharedInstance == nil)
        {
            sharedInstance = [super allocWithZone:zone];
//            [self config];
            return sharedInstance;
        }
    }
    return nil;
}

+(SharedManager *)sharedInstance{
    @synchronized(self)
    {
        if(nil == sharedInstance)
        {
            [self new];
        }
    }
    return sharedInstance;
}

+(void)config{
    [ShareSDK registerApp:@"5a943722644c"];
    
    [ShareSDK ssoEnabled:NO];

    [ShareSDK connectSinaWeiboWithAppKey:@"3345005833"
                               appSecret:@"47485aed20b4c0a196c4b94784c1f685"
                             redirectUri:@"http://www.sihuo.com"
                             weiboSDKCls:[WeiboSDK class]];
    
    [ShareSDK connectWeChatWithAppId:@"wx521f9b96d484555d"
                           appSecret:@"30b2d483ad56790554b843899e0e06e0"
                           wechatCls:[WXApi class]];
    
    [ShareSDK connectQZoneWithAppKey:@"1104215630" appSecret:@"qUqtVhcceoWArS3k"];
}

-(void)sharedWithType:(QMSharedType)type title:(NSString *)strTitle content:(NSString *)strContent  description:(NSString *)desc image:(NSString *)image url:(NSString *)strUrl{
    _strTitle=strTitle;
    _strContent=strContent;
    _strURL=strUrl;
    _imageURL=image;
    _strDesc=desc;
    
    switch (type) {
        case QMSharedTypeSinaWeiBo:
        {
            _shareType=ShareTypeSinaWeibo;
            [self shareToClient];
        }
            break;
        case QMSharedTypeWeiXin:
        {
            _shareType=ShareTypeWeixiSession;
            [self shareToClient];
        }
            break;
        case QMSharedTypeWeiXinFriends:
        {
            _shareType=ShareTypeWeixiTimeline;
            [self shareToClient];
        }
            break;
        case QMSharedTypeQZone:
        {
            _shareType=ShareTypeQQSpace;
            [self shareToQQSpace];
        }
            break;
        case QMSharedTypeTextMessage:{//跳回app别的页面
            
            
        }
            break;
        case QMSharedTypeCopyText:{//复制
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            [pasteboard setString:_strContent];
        }
            break;
        default:
            break;
    }
}

//QQ空间分享
-(void)shareToQQSpace{
    
    //创建分享内容
    id<ISSContent> publishContent = [ShareSDK content:_strContent
                                       defaultContent:@""
                                                image:[ShareSDK imageWithUrl:_imageURL]
                                                title:_strTitle
                                                  url:_strURL
                                          description:_strDesc
                                            mediaType:SSPublishContentMediaTypeNews];
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];

    //显示分享菜单
    [ShareSDK showShareViewWithType:ShareTypeQQSpace
                          container:container
                            content:publishContent
                      statusBarTips:YES
                        authOptions:nil
                       shareOptions:nil
                             result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                 if (state == SSPublishContentStateBegan) {
                                     [AppUtils showProgressMessage:@"开始分享"];
                                 }
                                 else{
                                     [AppUtils dismissHUD];
                                     if (state == SSPublishContentStateFail) {
                                         [AppUtils showAlertMessage:@"分享到QQ空间失败"];
                                     }
                                 }
                             }];
}

//微博,QQ,微信,客户端分享
-(void)shareToClient{
    //创建分享内容
    id<ISSContent> publishContent = [ShareSDK content:_strContent
                                       defaultContent:@""
                                                image:[ShareSDK imageWithUrl:_imageURL]
                                                title:_strTitle
                                                  url:_strURL
                                          description:_strDesc
                                            mediaType:SSPublishContentMediaTypeNews];

    
    [ShareSDK clientShareContent:publishContent
                            type:_shareType
                   statusBarTips:YES
                          result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                              if (state == SSPublishContentStateBegan) {
                                  [AppUtils showProgressMessage:@"开始分享"];
                              }
                              else{
                                  [AppUtils dismissHUD];
//注：若提示分享失败，先查看分享的图片的大小是否大于了30K，各项参数是否齐全
//                                  DLog(@"error:%@  errorCode:%ld", [error errorDescription], [error errorCode]);
                                  
                                  if (state == SSPublishContentStateFail) {
                                      if (_shareType ==ShareTypeSinaWeibo) {
                                          [AppUtils showAlertMessage:@"分享到微博失败"];
                                      }else{
                                          [AppUtils showAlertMessage:@"分享到微信失败"];
                                      }
                                  }
                              }
                          }];
}

@end
