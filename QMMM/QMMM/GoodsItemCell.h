//
//  GoodsItemCell.h
//  QMMM
//
//  Created by kingnet  on 15-3-16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kGoodsItemCellIdentifier;

@interface GoodsItemCell : UITableViewCell

@property (nonatomic, strong) NSArray * items;

- (id)initWithItems:(NSArray *)items;

@end
