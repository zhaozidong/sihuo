//
//  ImageSizeEnum.h
//  QMMM
//
//  Created by kingnet  on 14-11-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

//Image Size Enum
typedef NS_ENUM(NSInteger, QMImageSizeType){
    QMImageHalfSize = 2000, //一半屏幕的大小
    QMImageQuarterSize, //四分之一屏幕大小
    QMImageOneSize  //屏幕的一倍大小
};
