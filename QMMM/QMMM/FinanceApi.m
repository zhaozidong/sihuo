//
//  FinanceApi.m
//  QMMM
//
//  Created by kingnet  on 14-9-27.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "FinanceApi.h"
#import "QMHttpClient.h"
#import "APIConfig.h"
#import "TempFlag.h"

@implementation FinanceApi

- (void)getFinanceInfo:(contextBlock)context
{
    __block contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_USER_FINANCE];

    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:url method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
    
            [super processResponse:responseObject error:nil operation:operation context:bContext];

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"get user finance error:%@", error);
            [super processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

- (void)drawMoney:(NSString *)pwd money:(float)money payee:(NSString *)payee accountNo:(NSString *)accountNo context:(contextBlock)context
{
    __block contextBlock bContext = context;
    __block NSString * url = @"";
    if ([kWithdrawFrom isEqualToString:kHongBao]) {
        url = [[self class] httpsRequestUrlWithPath:API_HONGBAO_DRAW_MONEY];
    }
    else{
        url = [[self class] httpsRequestUrlWithPath:API_DRAW_MONEY];
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"password":pwd, @"money":@(money)}];
    if (![payee isEqualToString:@""]) {
        [params setObject:payee forKey:@"payee"];
    }
    if (![accountNo isEqualToString:@""]) {
        [params setObject:accountNo forKey:@"account_no"];
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [super processResponse:responseObject error:nil operation:operation context:bContext];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"draw money error:%@", error);
            [super processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

- (void)getDrawList:(int)startId top:(int)top context:(contextBlock)context
{
    __block contextBlock bContext = context;
    __block NSString * url = @"";
    if ([kWithdrawFrom isEqualToString:kHongBao]) {
        url = [[self class] httpsRequestUrlWithPath:API_HONGBAO_DRAW_LIST];
    }
    else{
        url = [[self class] httpsRequestUrlWithPath:API_DRAW_LIST];
    }
    
    NSDictionary *params = @{@"start":@(startId), @"top":@(top)};
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [super processResponse:responseObject error:nil operation:operation context:bContext];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"draw list error:%@", error);
            
            [super processResponse:nil error:error operation:operation context:bContext];
            
        }];
        
    });
}

- (void)setAccount:(NSString *)accountNo accountName:(NSString *)accountName oldAccountNo:(NSString *)oldAccountNo oldAccountName:(NSString *)oldAccountName context:(contextBlock)context
{
    __block contextBlock bContext = context;
    __block NSString * url = [[self class] httpsRequestUrlWithPath:API_DRAW_ACCOUNT];
    
    NSDictionary *params;
    if (oldAccountName == nil) {
        params = @{@"new_account_no":accountNo, @"new_payee":accountName};
    }
    else{
        params = @{@"new_account_no":accountNo, @"new_payee":accountName, @"old_payee":oldAccountName, @"old_account_no":oldAccountNo};
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [super processResponse:responseObject error:nil operation:operation context:bContext];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"set draw accout error:%@", error);
            
            [super processResponse:nil error:error operation:operation context:bContext];
            
        }];
        
    });

}

@end
