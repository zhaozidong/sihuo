//
//  QMSpecialTopicsCell.h
//  QMMM
//
//  Created by Derek on 15/1/4.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityInfo.h"

@protocol QMSpecialTopicsDelegate <NSObject>

-(void)didSpecialTopiceClick:(ActivityEntity *)actInfo;

@end




@interface QMSpecialTopicsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *topicImg1;

@property (weak, nonatomic) IBOutlet UIImageView *topicImg2;

@property (weak, nonatomic) IBOutlet UIImageView *topicImg3;

@property (weak, nonatomic) IBOutlet UIImageView *topicImg4;


@property (weak, nonatomic) id<QMSpecialTopicsDelegate> delegate;

+ (id)cellForGoodsTopics;


-(void)updateWithArr:(NSArray *)arrInfo;

@end
