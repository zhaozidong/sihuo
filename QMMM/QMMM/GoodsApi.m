//
//  GoodsApi.m
//  QMMM
//
//  Created by 韩芦 on 14-9-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsApi.h"
#import "APIConfig.h"
#import "QMHttpClient.h"
#import "UserDefaultsUtils.h"
#import "TempFlag.h"

@implementation GoodsApi

static GoodsApi * _shareInstance = NULL;

+(GoodsApi *)shareInstance
{
    if(nil == _shareInstance) {
        _shareInstance = [[GoodsApi alloc] init];
    }
    
    return _shareInstance;
}

- (void)getRecommendGoodslist:(uint)type top:(BOOL)bTop lastGoodsId:(uint64_t)lastGoodsId context:(contextBlock)context;
{
    __block __strong contextBlock bContext = context;
    __block NSString * requestUrl = [[self class] requestUrlWithPath:URL_RECOMMEND_GOODS_LIST];
    
    int dire = bTop ? 1 : 0;
    if(0 == lastGoodsId) {
        lastGoodsId = 1;
    }
    
    NSString *returnKey=@"0";
    if(0==dire){//上拉刷新,获取服务器上次返回的key的值
        if (1==type && [UserDefaultsUtils valueWithKey:@"lastFriendKey"]) {//好友
            returnKey=[UserDefaultsUtils valueWithKey:@"lastFriendKey"];
        }else if(2==type && [UserDefaultsUtils valueWithKey:@"lastRecommendKey"]){//发现
            returnKey=[UserDefaultsUtils valueWithKey:@"lastRecommendKey"];
        }
    }else{//下拉刷新，返回上次第一个key
        if (1==type && [UserDefaultsUtils valueWithKey:@"firstFriendKey"]) {
            returnKey=[UserDefaultsUtils valueWithKey:@"firstFriendKey"];
        }else if(2==type && [UserDefaultsUtils valueWithKey:@"firstRecommendKey"]){
            returnKey=[UserDefaultsUtils valueWithKey:@"firstRecommendKey"];
        }
    }
    
//    NSDictionary *params = @{@"type":@(type), @"id" : @(lastGoodsId), @"top" : @(dire), @"praiseNum" : @(7)};
    
    //key:首次为空 否则为服务器返回的数据
    NSDictionary *params = @{@"type":@(type), @"key" : returnKey, @"top" : @(dire)};
     //异步线程请求
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:requestUrl method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            //切回主线程
            dispatch_async(dispatch_get_main_queue(), ^{
                if(bContext) {
                    bContext(result);
                }
            });
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------getRecommendGoodsList fail : %@", requestUrl);
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            //切回主线程
            dispatch_async(dispatch_get_main_queue(), ^{
                if(bContext) {
                    bContext(dic);
                }
            });
        }];
    });
}

- (void)upLoadAudio:(NSData*)data context:(contextBlock)context
{
    __block NSString *path = [[self class]uploadFileWithPath:API_UPLOAD_AUDIO];

    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
    
        [[QMHttpClient defaultClient]uploadFileWithPath:path data:data contentType:@"*/*" success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"upload audio error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"upload audio error:%@", error.description);
        } uploadProgressBlock:nil];
    });
}

- (void)uploadGoodsImage:(NSData *)data context:(contextBlock)context
{
    __block NSString *path = [[self class]uploadFileWithPath:[NSString stringWithFormat:API_UPLOAD_FILE, @"goods"]];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]uploadFileWithPath:path data:data contentType:@"*/*" success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"upload goods image error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"upload goods image error:%@", error.description);
        } uploadProgressBlock:nil];
    });

}

- (void)releaseGoodsWithDes:(NSString *)description price:(float)price number:(int)number category:(int)category photos:(NSArray *)photos ori_price:(float)oriPrice isNew:(int)isNew audioTime:(int)audioTime audioKey:(NSString *)audioKey location:(NSString *)location address:(NSString *)address freight:(float)freight context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_RELEASE_GOODS];
    
    __block contextBlock bContext = context;
    
    NSArray *copyPhotos = [NSArray arrayWithArray:photos];
    
    NSString *priceStr = [NSString stringWithFormat:@"%.2f", price];
    NSString *oriPriceStr = [NSString stringWithFormat:@"%.2f", oriPrice];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        //
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"desc":description, @"price":priceStr, @"num":@(number), @"cate":@(category), @"ori_price":oriPriceStr, @"audio_t":@(audioTime), @"audio":audioKey, @"loc":address, @"point":location, @"freight":@(freight)}];
       
        NSString * retString = [copyPhotos componentsJoinedByString:@","];
        
        [params setObject:retString forKey:@"photo"];
        
        [params setObject:@(isNew) forKey:@"usage"];
        
        [params setObject:kPublishGoodsFrom forKey:@"from"];
    
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"release goods error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == 1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"release goods error:%@", error.description);

        }];
    });
}

- (void)goodsDetailsWithId:(uint64_t)goodsId prisNum:(NSInteger)prisNum context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_GOODS_DETAIL];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        //
        NSDictionary *params = @{@"id":@(goodsId), @"praiseNum":@(prisNum)};
        
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"goods details error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
   
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"goods details error:%@", error.description);
            
        }];
    });
}

- (void)commentListWithId:(uint64_t)goodsId offset:(uint64_t)offset context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_COMMENT_LIST];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"gid":@(goodsId), @"offset":@(offset)};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"comments list error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"comments list error:%@", error.description);
            
        }];
    });
}

- (void)downloadAudio:(NSString *)audioPath context:(contextBlock)context
{
    if([audioPath length] > 0) {
        __block contextBlock bContext = context;
        __block NSString * savePath = [[self class] audioPath];
        
        NSRange range = [audioPath rangeOfString:@"/" options:NSBackwardsSearch];
        if(range.location != NSNotFound) {
            NSString * picName = [audioPath substringFromIndex:range.location];
            savePath = [savePath stringByAppendingPathComponent:picName];
        }
        NSString *urlPath = [AppUtils getAbsolutePath:audioPath];
        //异步线程请求
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [[QMHttpClient defaultClient] downloadFileWithPath:urlPath savename:savePath success:^(AFHTTPRequestOperation *operation, id responseObject) {
                dispatch_async(dispatch_get_main_queue(), ^{
                     NSDictionary *dic = @{@"s":@(0), @"d":savePath};
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDictionary *dic;
                    if (error.code == -1001) {
                        dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
                    }
                    else if(error.code == QMNotNetworkError){
                        dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
                    }
                    else{
                        dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
                    }
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"download audio error:%@", error.description);
            }];
        });
    }
}

- (void)addfavor:(uint64_t)goodsId sellerId:(uint64_t)sellerId context:(contextBlock)context
{
    if(goodsId && sellerId) {
        
#ifdef DEBUG
        NSString * favorUrl = [NSString stringWithFormat:@"%@&debug=1", API_ADD_FAVOR];
#else
        NSString * favorUrl = API_ADD_FAVOR;
#endif
        __block  contextBlock bContext = context;

        __block NSString * requestUrl = [[self class] requestUrlWithPath:favorUrl];

        NSDictionary *params = @{@"id":@(goodsId), @"sellerId" : @(sellerId)};

        
        //异步线程请求
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [[QMHttpClient defaultClient] requestWithPath:requestUrl method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSError *error = nil;
                id result = [self parseData:responseObject error:&error];
                if (result) {
                    NSDictionary *dic = @{@"s":@0, @"d":result};
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (bContext) {
                            bContext(dic);
                        }
                    });
                }
                else{
                    NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (bContext) {
                            bContext(dic);
                        }
                    });
                    DLog(@"addfavor error:%@",error.userInfo[NSLocalizedDescriptionKey]);
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                DLog(@"--------addfavor fail : %@", requestUrl);
                
                NSDictionary *dic;
                if (error.code == -1001) {
                    dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
                }
                else if(error.code == QMNotNetworkError){
                    dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
                }
                else{
                    dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }];
        });
    }
}

- (void)sendComment:(uint64_t)goodsId pid:(uint64_t)pid comments:(NSString *)comments context:(contextBlock)context
{
#ifdef DEBUG
    NSString * commentUrl = [NSString stringWithFormat:@"%@&debug=1", API_SEND_COMMENT];
#else
    NSString * commentUrl = API_SEND_COMMENT;
#endif
    
    __block NSString *path = [[self class]requestUrlWithPath:commentUrl];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"gid":@(goodsId), @"pid":@(pid), @"comments":comments};
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"send comment error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"send comment error:%@", error.description);
            
        }];
    });
}

- (void)searchGoods:(NSDictionary *)dict context:(contextBlock)context
{
    if(dict && [dict isKindOfClass:[NSDictionary class]]) {
        
        NSString * requestUrl = API_SEARCH_GOODS;
        id temp = [dict objectForKey:@"words"];
        if(temp && [temp isKindOfClass:[NSString class]]) {
            requestUrl = [requestUrl stringByAppendingFormat:@"&words=%@", temp];
        }
        
        temp = [dict objectForKey:@"category"];
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            requestUrl = [requestUrl stringByAppendingFormat:@"&category=%@", temp];
        }
        
        temp = [dict objectForKey:@"price_min"];
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            requestUrl = [requestUrl stringByAppendingFormat:@"&price_min=%@", temp];
        }
        
        temp = [dict objectForKey:@"price_max"];
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            requestUrl = [requestUrl stringByAppendingFormat:@"&price_max=%@", temp];
        }
        
        temp = [dict objectForKey:@"offset"];
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            requestUrl = [requestUrl stringByAppendingFormat:@"&offset=%@", temp];
        }
        
        temp = [dict objectForKey:@"limit"];
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            requestUrl = [requestUrl stringByAppendingFormat:@"&limit=%@", temp];
        }
        
        temp = [dict objectForKey:@"sort"];
        if(temp && ![temp isKindOfClass:[NSNull class]]) {
            requestUrl = [requestUrl stringByAppendingFormat:@"&sort=%@", temp];
        }
        
        temp = [dict objectForKey:@"condition"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            requestUrl = [requestUrl stringByAppendingFormat:@"&condition=%@", temp];
        }
        
        temp = [dict objectForKey:@"discount"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            requestUrl = [requestUrl stringByAppendingFormat:@"&discount=%@", temp];
        }
        
        temp = [dict objectForKey:@"distance"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            requestUrl = [requestUrl stringByAppendingFormat:@"&distance=%@", temp];
        }
        
        temp = [dict objectForKey:@"point"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            requestUrl = [requestUrl stringByAppendingFormat:@"&point=%@", temp];
        }
        
        __block  contextBlock bContext = context;
        __block NSString * url = [[self class] requestUrlWithPath:requestUrl];
        
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSError *error = nil;
                id result = [self parseData:responseObject error:&error];
                if (result) {
                    NSDictionary *dic = @{@"s":@0, @"d":result};
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (bContext) {
                            bContext(dic);
                        }
                    });
                } else {
                    NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (bContext) {
                            bContext(dic);
                        }
                    });
                    DLog(@"searchGoods error:%@",error.userInfo[NSLocalizedDescriptionKey]);
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                DLog(@"--------searchGoods fail : %@", requestUrl);
                
                NSDictionary *dic;
                if (error.code == -1001) {
                    dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
                }
                else if(error.code == QMNotNetworkError){
                    dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
                }
                else{
                    dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }];
        });
    }
}

- (void)getOrderList:(uint)type orderId:(NSString *)orderId context:(contextBlock)context
{
    NSString * temp = [NSString stringWithFormat:@"%@&type=%u&id=%@", API_ORDER_LIST, type, orderId];
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:temp];
      dispatch_async(dispatch_get_global_queue(0, 0), ^{
          [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSError *error = nil;
              id result = [self parseData:responseObject error:&error];
              if (result) {
                  NSDictionary *dic = @{@"s":@(0), @"d":result};
                  dispatch_async(dispatch_get_main_queue(), ^{
                      if (bContext) {
                          bContext(dic);
                      }
                  });
              } else {
                  NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                  dispatch_async(dispatch_get_main_queue(), ^{
                      if (bContext) {
                          bContext(dic);
                      }
                  });
                  DLog(@"getOrderList error:%@",error.userInfo[NSLocalizedDescriptionKey]);
              }

          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              DLog(@"--------getOrderList fail : %@", url);
              
              NSDictionary *dic;
              if (error.code == -1001) {
                  dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
              }
              else if(error.code == QMNotNetworkError){
                  dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
              }
              else{
                  dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
              }
              dispatch_async(dispatch_get_main_queue(), ^{
                  if (bContext) {
                      bContext(dic);
                  }
              });

              
          }];
      });
}

- (void)getOrderDetail:(uint)type orderId:(NSString *)orderId context:(contextBlock)context
{
    NSString * temp = [NSString stringWithFormat:@"%@&type=%u&id=%@", API_ORDER_DETAIL, type, orderId];
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] httpsRequestUrlWithPath:temp];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"getOrderDetail error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------getOrderDetail fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
            
        }];
    });
}

- (void)deliverGoods:(NSString *)orderId expressCo:(NSString *)express_co expressNum:(NSString *)express_num context:(contextBlock)context
{
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_DELIVER_GOODS];
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:orderId, @"id", express_co, @"express_co", express_num, @"express_no", nil];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"deliverGoods error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------deliverGoods fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });

}

- (void)doScores:(NSString *)orderId scores:(CGFloat)scores comment:(NSString *)comment context:(contextBlock)context
{
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_DO_SCORES];
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:orderId, @"id", @(scores), @"rate", comment, @"comments", nil];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"doScores error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------doScores fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });

}

- (void)getFavorList:(NSString *)goodsId context:(contextBlock)context
{
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_FAVOR_LIST];
    NSDictionary * params = nil;
    if(goodsId > 0) {
        params = [NSDictionary dictionaryWithObjectsAndKeys:goodsId, @"id", nil];
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"getFavorList error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------getFavorList fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });
}

//删除用户评论
- (void)deleteCommitWithGoodsId:(NSString *)gId commitId:(NSString *)cId context:(contextBlock)context{
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_DELETE_COMMIT];
    NSDictionary * params = nil;
    if(gId && cId) {
        params = [NSDictionary dictionaryWithObjectsAndKeys:gId, @"gid",cId, @"id",nil];
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"deleteCommit error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------deleteCommit fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });
    
    
    
    
    
}


- (void)getBoughtList:(NSString *)goodsId context:(contextBlock)context
{
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_BOUGHT_GOODS_LIST];
    NSDictionary * params = nil;
    if(goodsId > 0) {
        params = [NSDictionary dictionaryWithObjectsAndKeys:goodsId, @"id", nil];
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"getBoughtList error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------getBoughtList fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });
}

- (void)getGoodsInfoWithId:(uint64_t)goodsId context:(contextBlock)context
{
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_GOODS_INFO];
    NSDictionary * params = nil;
    if(goodsId > 0) {
        params = [NSDictionary dictionaryWithObjectsAndKeys:@(goodsId), @"id", nil];
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"getGoodsInfo error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------getGoodsInfo fail : %@", error.description);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });

}

- (void)updateGoodsWithId:(uint64_t)goodsId desc:(NSString *)desc price:(float)price number:(int)number category:(int)category photos:(NSArray *)photos ori_price:(float)oriPrice isNew:(int)isNew audioTime:(int)audioTime audioKey:(NSString *)audioKey location:(NSString *)location address:(NSString *)address freight:(float)freight context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_UPDATE_GOODS];
    
    __block contextBlock bContext = context;
    
    NSArray *copyPhotos = [NSArray arrayWithArray:photos];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        //
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"id":@(goodsId), @"desc":desc, @"price":@(price), @"num":@(number), @"cate":@(category), @"ori_price":@(oriPrice), @"audio_t":@(audioTime), @"audio":audioKey, @"loc":address, @"point":location, @"freight":@(freight)}];
        
        NSString * retString = [copyPhotos componentsJoinedByString:@","];
        
        [params setObject:retString forKey:@"photo"];
        
        [params setObject:@(isNew) forKey:@"usage"];
        
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"update goods error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"update goods error:%@", error.description);
            
        }];
    });
}

- (void)updateGoodsStaWithId:(uint64_t)goodsId type:(int)type context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_GOODS_STATUS];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = nil;
    
    if (goodsId > 0) {
        params = [NSDictionary dictionaryWithObjectsAndKeys:@(goodsId), @"id", @(type), @"type", nil];
    }
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@(0), @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"updateGoodsSta error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"updateGoodsSta error:%@", error.description);
        }];
    });
}

- (void)submitOrder:(int)addressId goodsId:(uint64_t)goodsId paymentType:(int)paymentType num:(int)num remainPay:(float)remainPay context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_SUBMIT_ORDER];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"address_id":@(addressId), @"goods_id":@(goodsId), @"payment_type":@(paymentType), @"num":@(num), @"remain_pay":[NSString stringWithFormat:@"%.2f", remainPay]};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@(0), @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"submitOrder error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"submitOrder error:%@", error.description);
        }];
    });
}

- (void)deleteOrder:(NSString *)orderId type:(int)type context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_DELETE_ORDER];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"id":orderId, @"type":@(type)};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@(0), @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"deleteOrder error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"deleteOrder error:%@", error.description);
        }];
    });
}

- (void)cancelOrder:(NSString *)orderId context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_CANCEL_ORDER];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"id":orderId};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@(0), @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"cancelOrder error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else if(error.code == QMNotNetworkError){
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"cancelOrder error:%@", error.description);
        }];
    });
}

- (void)getActivitiesInfo:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_GET_ACTIVITYINFO];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

- (void)getAllTopic:(contextBlock)context{
    __block NSString *path = [[self class]requestUrlWithPath:API_GET_ALLTOPIC];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

- (void)getSpecialGoodsList:(uint)top key:(uint64_t)key tag:(NSString *)tag context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_GET_SPECIALGOODSLIST];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSDictionary *params = @{@"key":@(key), @"top":@(top), @"tag":tag};
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

- (void)topicsGoodsList:(NSString *)requestURL context:(contextBlock)context
{
    __block  contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:requestURL method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}


- (void)getCategory:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_GOODS_CATEGORY];
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

- (void)getIntroGoodsList:(contextBlock)context{
    __block NSString *path = [[self class]requestUrlWithPath:API_GET_INTROGOODS];
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

- (void)getCategoryByDesc:(NSString *)desc context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_GET_CATEGORY];
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"desc":desc};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

- (void)uploadGoodsPhotoData:(NSData *)imgData uploadProgressBlock:(void (^)(NSUInteger, long long, long long))uploadProgressBlock context:(contextBlock)context
{
    __block NSString *path = [[self class]uploadFileWithPath:[NSString stringWithFormat:API_UPLOAD_FILE, @"goods"]];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]uploadFileWithPath:path data:imgData contentType:@"*/*" success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        } uploadProgressBlock:uploadProgressBlock];
    });
}

- (void)sellerRefund:(NSString *)orderId context:(contextBlock)context
{
    __block NSString *path = [[self class]httpsRequestUrlWithPath:API_ORDER_SELLERREFUND];
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:@{@"id":orderId} prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

- (void)applyRefund:(NSString *)orderId amount:(float)amout reason:(NSString *)reason context:(contextBlock)context
{
    __block NSString *path = [[self class]httpsRequestUrlWithPath:API_ORDER_APPLYREFUND];
    __block contextBlock bContext = context;
    
    NSString *amountStr = [NSString stringWithFormat:@"%.2f", amout];
    NSDictionary *params = @{@"id":orderId, @"amount":amountStr, @"reason":reason};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

- (void)acceptRefund:(NSString *)orderId context:(contextBlock)context
{
    __block NSString *path = [[self class]httpsRequestUrlWithPath:API_ORDER_ACCEPTREFUND];
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:@{@"id":orderId} prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

- (void)rejectRefund:(NSString *)orderId reason:(NSString *)reason context:(contextBlock)context
{
    __block NSString *path = [[self class]httpsRequestUrlWithPath:API_ORDER_REJECTREFUND];
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"id":orderId, @"reason":reason};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

/*************************红包兑换商品****************/
- (void)exchangeGoods:(int)num goodsId:(NSString *)goodsId addressId:(int)addressId context:(contextBlock)context
{
    __block NSString *path = [[self class]httpsRequestUrlWithPath:API_HONGBAO_BUY_GOODS];
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"id":goodsId, @"num":@(num), @"address_id":@(addressId)};
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
    });
}

@end
