//
//  GoodsListDescCell.m
//  QMMM
//
//  Created by Shinancao on 14/12/14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsListDescCell.h"
#import "GoodEntity.h"

NSString * const kGoodsListDescCellIdentifier = @"kGoodsListDescCellIdentifier";

@interface GoodsListDescCell (){
    BOOL _isPreview;
}
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidthCst;
@property (weak, nonatomic) IBOutlet UIImageView *imageV;
@property (nonatomic,assign,setter=maxLine:)NSUInteger maxLine;

@end
@implementation GoodsListDescCell

-(void)maxLine:(NSUInteger)number{
    self.descLbl.numberOfLines=number;
}

+ (GoodsListDescCell *)goodsListDescCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"GoodsListDescCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    _isPreview=NO;
    self.descLbl.text = @"";
    self.descLbl.textColor = kDarkTextColor;
    self.descLbl.font = [UIFont systemFontOfSize:14.f];
    self.descLbl.numberOfLines = 2;
    self.descLbl.backgroundColor = [UIColor clearColor];
    self.descLbl.textAlignment = NSTextAlignmentLeft;
    self.descLbl.lineBreakMode = NSLineBreakByTruncatingTail;
    self.descLbl.preferredMaxLayoutWidth = kMainFrameWidth - 32 - 16;
    self.imageV.image = [[UIImage imageNamed:@"descUsage_bg"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(GoodEntity *)entity
{
    if (entity) {
        _isPreview=NO;
        NSString *strUsage=[[GoodEntity usageDesc] objectAtIndex:entity.isNew];
        NSString * displayname = [NSString stringWithFormat:@"%@ %@", strUsage, entity.desc];
        
        //属性字符串设置属性
        NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:displayname];
        [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.0] range:NSMakeRange(0, [strUsage length]+1)];
        [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange([strUsage length]+1, [attributeString length]-([strUsage length]+1))];
        [attributeString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x99a0aa) range:NSMakeRange(0, [strUsage length]+1)];
        
        self.descLbl.attributedText = attributeString;
        
        NSMutableAttributedString * attributeString1 = [[NSMutableAttributedString alloc] initWithString:strUsage];
        [attributeString1 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.0] range:NSMakeRange(0, [strUsage length])];
        
        [attributeString1 addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x99a0aa) range:NSMakeRange(0, strUsage.length)];
        //width - index - margin
        CGRect rect = [attributeString1 boundingRectWithSize:CGSizeMake(100, 17) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine context:nil];
        CGFloat width = rect.size.width;
        self.imageViewWidthCst.constant = width+4;
        [self.contentView layoutIfNeeded];
    }
}

- (void)updateUI:(GoodEntity *)entity isPreview:(BOOL)isPrev{
    [self updateUI:entity];
    _isPreview=YES;
}

+ (CGFloat)heightForGoodsListCell:(GoodEntity *)entity{
    if (entity) {
        NSString *strUsage=[[GoodEntity usageDesc] objectAtIndex:entity.isNew];
        NSString * displayname = [NSString stringWithFormat:@"%@ %@", strUsage, entity.desc];
        
        //属性字符串设置属性
        NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:displayname];
        [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.0] range:NSMakeRange(0, [strUsage length]+1)];
        [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange([strUsage length]+1, [attributeString length]-([strUsage length]+1))];
        [attributeString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x99a0aa) range:NSMakeRange(0, [strUsage length]+1)];
        
        CGRect rect = [attributeString boundingRectWithSize:CGSizeMake(kMainFrameWidth - 32 - 16, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil];
        CGFloat height=rect.size.height;
        
        return height+10;
    }
    return 50;
}


-(void)layoutSubviews{
    [super layoutSubviews];
    if (_isPreview) {
        CGRect frame=self.contentView.frame;
        frame.origin.x=0;
        frame.size.width=kMainFrameWidth;
        self.contentView.frame=frame;
    }
}

@end
