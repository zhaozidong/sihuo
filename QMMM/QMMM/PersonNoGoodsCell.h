//
//  PersonNoGoodsCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonNoGoodsCell : UITableViewCell

@property (nonatomic, strong) NSString *tip; //提示

+ (PersonNoGoodsCell *)personNoGoods;

@end
