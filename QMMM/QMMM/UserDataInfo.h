//
//  UserDataInfo.h
//  QMMM
//
//  Created by kingnet  on 14-10-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseEntity.h"

@interface UserDataInfo : BaseEntity
@property (nonatomic, strong) NSString *uid;  //用户id
@property (nonatomic, strong) NSString *nickname; //用户昵称
@property (nonatomic, strong) NSArray *lifePhotos; //生活照
@property (nonatomic, strong) NSString *avatar; //头像
@property (nonatomic, strong) NSString *mobile; //手机号码
@property (nonatomic, strong) NSString *remark; //备注

@property (nonatomic, assign) int positive; //好评的数量
@property (nonatomic, assign) int total; //已售商品的总数

- (instancetype)initWithDictionary:(NSDictionary *)dict;

- (void)assignValue:(UserDataInfo *)userInfo;

@end

@class PersonRelation;
@interface PersonPageUserInfo : BaseEntity

@property (nonatomic, strong) UserDataInfo *userInfo;

@property (nonatomic, strong) PersonRelation *relation; //朋友关系

@property (nonatomic, assign) int reliability; //可靠度

@property (nonatomic, assign) int influence; //影响力

- (instancetype)initWithDictionary:(NSDictionary *)dict;

- (void)assignValue:(PersonPageUserInfo *)userInfo;

@end

/**
 我的界面靠谱度和影响力
 **/
@interface PersonInfluence : BaseEntity

@property (nonatomic, copy) NSString *uid; //用户id

@property (nonatomic, assign) int influence; //影响力

@property (nonatomic, assign) int reliability; //可信度

@property (nonatomic, copy) NSString *influenceBeat; //影响力打败了多少人

@property (nonatomic, copy) NSString *reliabilityBeat; //靠谱度打败了多少人

- (instancetype)initWithInfluence:(int)influence reliability:(int)reliability influenceBeat:(NSString *)influenceBeat reliabilityBeat:(NSString *)reliabilityBeat uid:(NSString *)uid;

- (void)assignValue:(PersonInfluence *)influence;

@end

/**
 我的界面发货数和待确认发货数
 **/
@interface PersonOrderStatusNum : BaseEntity

@property (nonatomic, assign) int salerNum; //待发货数

@property (nonatomic, assign) int buyerNum; //待确认收货数

- (instancetype)initWithSalerNum:(int)salerNum buyerNum:(int)buyerNum;

- (void)assignValue:(PersonOrderStatusNum *)orderStatus;

@end

/**
 朋友关系
 **/
@interface PersonRelation : BaseEntity

@property (nonatomic, assign) BOOL isFriends; //是否为1度好友

@property (nonatomic, assign) BOOL is2ndFriends; //是否为2度好友

@property (nonatomic, assign) BOOL isFromApp;//是否来自通讯录

@property (nonatomic, copy) NSArray *friendsArr; //谁的朋友

- (instancetype)initWithDictionary:(NSDictionary *)dict;

- (void)assignValue:(PersonRelation *)relation;

@end