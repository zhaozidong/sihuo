//
//  QMGetLocation.h
//  QMMM
//  获取地理位置
//  Created by kingnet  on 14-12-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^GetLocationBlock)(CLLocation *currentLoc, NSString *placeMark, NSError *error);

typedef void(^GetCurrentPointBlock)(CLLocation *currentLoc, NSError *error);

@interface QMGetLocation : NSObject

/**
 获取当前的坐标(国际坐标)和具体位置(市区)
 **/
+ (void)getCurrentLoc:(GetLocationBlock)locationBlock curPointBlock:(GetCurrentPointBlock)curPointBlock;

/**
 只获取当前的坐标(国际坐标)
 **/
+ (void)getCurrentPoint:(GetCurrentPointBlock)pointBlock;

@end
