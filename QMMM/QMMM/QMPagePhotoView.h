//
//  QMPagePhotoView.h
//  QMMM
//
//  Created by hanlu on 14-10-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QMPagePhotoDataSource;
@protocol QMPagePhotoDelegate;

@interface QMPagePhotoView : UIView<UIScrollViewDelegate> {
    UIScrollView * _scrollView;
    UIPageControl * _pageControl;
}

@property (nonatomic, weak) id<QMPagePhotoDataSource> datasource;

- (void)refreshDataSource:(NSInteger)selectIndex;

- (NSInteger)getCurrentPage;

@end

@protocol QMPagePhotoDataSource <NSObject>

- (NSInteger)numberOfPage;

- (NSString *)imageUrlAtIndex:(NSInteger)index;

@end

@protocol QMPagePhotoDelegate <NSObject>

- (void)didImageClick:(NSInteger)index;

@end