//
//  QMGoodsTitleCell.h
//  QMMM
//
//  Created by hanlu on 14-9-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QMGoodsBaseTableViewCell.h"
#import "QMRoundHeadView.h"

@class GoodEntity;

@class QMRoundHeadView;
@class QMGoodsRelationView;

extern NSString * const kQMGoodsTitleCellIndentifier;

@protocol RelationViewTitleDelegate <NSObject>

-(void)clickRelationTitleWithId:(NSString *)friendId;

@end






@interface QMGoodsTitleCell : QMGoodsBaseTableViewCell<QMRoundHeadViewDelegate,RelationViewTitleDelegate>

@property (nonatomic, strong) IBOutlet UILabel * title;

//@property (nonatomic, strong) IBOutlet UILabel * location;

//@property (nonatomic, strong) IBOutlet UIImageView * locationView;

//@property (nonatomic, strong) IBOutlet UIImageView * timeImageView;

//@property (nonatomic, strong) IBOutlet UILabel * timeLabel;

@property (strong, nonatomic) IBOutlet UIImageView *friendImageView;

@property (nonatomic, strong) IBOutlet QMRoundHeadView * headImageView;

@property (nonatomic, strong) IBOutlet UIImageView * roundBkgView;

@property (nonatomic, strong) IBOutlet QMGoodsRelationView * relationView;

@property (nonatomic, weak) id<QMRoundHeadViewDelegate> delegate;

@property (nonatomic,weak) id<RelationViewTitleDelegate> delegateRelation;

+ (id)cellAwakeFromNib;

+ (NSUInteger)heightForGoodsTitleCell:(GoodEntity *)goodsInfo;

- (void)updateUI:(GoodEntity *)goodsInfo showFull:(BOOL)showFull;

@end
