//
//  FindPwdViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "FindPwdViewController.h"
#import "LRInputViewController.h"
#import "UserInfoHandler.h"
#import "FindPwdCheckCodeViewController.h"
#import "AppDelegate.h"

@interface FindPwdViewController ()<LRInputViewControllerDelegate>

@property (nonatomic, strong) LRInputViewController *inputController;

@end

@implementation FindPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = [AppUtils localizedPersonString:@"QM_Text_ForgetPwd"];
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    self.canTapCloseKeyboard = YES;
    
    self.inputController = [[LRInputViewController alloc]init];
    self.inputController.submitBtnTitle = @"重置密码并登录";
    self.inputController.delegate = self;
    self.inputController.returnKeyType = UIReturnKeyNext;
    self.inputController.pwdPlaceholder = @"重置新密码";
    
    [self addChildViewController:self.inputController];
    
    [[self view] addSubview:[self.inputController view]];
    
    [self.inputController didMoveToParentViewController:self];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.inputController = nil;
}

- (void)clickSubmitBtn:(NSString *)phoneNum pwd:(NSString *)pwd checkCode:(NSString *)checkCode
{
    [AppUtils showProgressMessage:[AppUtils localizedPersonString:@"QM_HUD_Treating"]];
    [[UserInfoHandler sharedInstance] executeFindPwdWithNewPwd:pwd checkCode:checkCode phoneNum:phoneNum success:^(id obj) {
        [AppUtils showSuccessMessage:[AppUtils localizedPersonString:@"QM_Tip_ChangeOK"]];
        
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate startApp];
        
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)clickGetCheckCode:(NSString *)phoneNum
{
    [[UserInfoHandler sharedInstance] sendSMSWithMobile:phoneNum type:@"recovery_password" success:^(id obj) {
#ifdef DEBUG
        [AppUtils showAlertMessage:[NSString stringWithFormat:@"验证码：%@(仅供测试用)", (NSString *)obj]];
#else
        if (kIsTestRelease) {
            [AppUtils showAlertMessage:[NSString stringWithFormat:@"验证码：%@(仅供测试用)", (NSString *)obj]];
        }
#endif
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        //重置button
        [self.inputController stopTimer];
    }];
}

- (void)viewShouldMoveY:(CGFloat)Y
{
    CGRect frame = self.view.frame;
    frame.origin.y = -Y;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = frame;
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerObserver];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeObserver];
}

- (void)registerObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processKeyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
}

- (void)removeObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)processKeyboardWillHide:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0 ;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    frame.size.height = kMainFrameHeight;
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.view.frame = frame;
    }];
}

@end
