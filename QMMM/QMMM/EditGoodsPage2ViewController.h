//
//  EditGoodsPage2ViewController.h
//  QMMM
//
//  Created by kingnet  on 15-3-2.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"
#import "GoodEntity.h"
#import "QMPublishPreviewViewController.h"

@interface EditGoodsPage2ViewController : BaseViewController

@property (nonatomic, assign) QMEditGoodsType controllerType;

@property (nonatomic, copy) UpdateGoodsBlock updateGoodsBlock; //更新商品后的回调

@property (nonatomic, strong) NSString *audioPath; //音频的本地路径

@property (nonatomic, strong) NSArray *goodsPhotoArr; //商品图片的路径

- (id)initWithType:(QMEditGoodsType)type goodsEntity:(GoodEntity *)goodsEntity;

- (void)setGoodsEntity:(GoodEntity *)goodsEntity;

@end
