//
//  BuyHongBaoGoodsViewController.m
//  QMMM
//
//  Created by kingnet  on 15-3-27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BuyHongBaoGoodsViewController.h"
#import "AddressHandler.h"
#import "AddressEntity.h"
#import "OrderInfoControl+HongBao.h"
#import "MyAddressViewController.h"
#import "EditAddressViewController.h"
#import "GoodEntity.h"
#import "ExchangeSuccessViewController.h"
#import "FinanceHandler.h"
#import "FinanceDataInfo.h"
#import "GoodsHandler.h"

@interface BuyHongBaoGoodsViewController ()<UITableViewDataSource, UITableViewDelegate, OrderInfoControlDelegate>

@property (nonatomic)UITableView *tableView;
@property (nonatomic, strong) AddressHandler *addressHandler;
@property (nonatomic, strong) AddressEntity *addressEntity;
@property (nonatomic, strong) OrderInfoControl *infoControl;
@property (nonatomic, strong) GoodEntity *goodsEntity;

@end

@implementation BuyHongBaoGoodsViewController

- (id)initWithNum:(int)num price:(float)price goodsId:(int)goodsId
{
    self = [super init];
    if (self) {
        self.goodsEntity = [[GoodEntity alloc]init];
        self.goodsEntity.number = num;
        self.goodsEntity.price = price;
        self.goodsEntity.freight = 0;
        self.goodsEntity.gid = goodsId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"立即兑换";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    self.addressHandler = [[AddressHandler alloc]init];
    self.infoControl = [[OrderInfoControl alloc]init];
    self.infoControl.delegate = self;
    
    [self getAddress];
    
    [self.infoControl setLabelText];
    [self.view addSubview:self.tableView];
    self.infoControl.goodsEntity = self.goodsEntity;
    
    //拉取红包数量
    [self getFinanceInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Getter
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
        
        //footer
        UIView *footView = [[[NSBundle mainBundle]loadNibNamed:@"WithdrawFootView" owner:nil options:0] lastObject];
        footView.frame = CGRectMake(0, 0, CGRectGetWidth(_tableView.frame), 80.f);
        _tableView.tableFooterView = footView;
        for (UIButton *button in footView.subviews) {
            if ([button isKindOfClass:[UIButton class]]) {
                [button setTitle:@"立即兑换" forState:UIControlStateNormal];
                [button addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
                break;
            }
        }
    }
    return _tableView;
}

- (void)getAddress
{
    __weak typeof(self) weakSelf = self;
    [self.addressHandler getAddressList:^(id obj) {
        NSArray *array = (NSArray *)obj;
        for (AddressEntity *entity in array) {
            if (entity.isDefault) {
                [weakSelf reloadAddress:entity];
                break;
            }
        }
    } failed:^(id obj) {
        
    }];
}

- (void)reloadAddress:(AddressEntity *)entity
{
    self.addressEntity = entity;
    [self.infoControl setAddressEntity:self.addressEntity];
}

- (void)submitAction:(id)sender
{
    //检测是否填写了地址
    if (!self.addressEntity || self.addressEntity.addressId <= 0) {
        [AppUtils showAlertMessage:@"请添加收货地址"];
        //跳到编辑收货地址的页面
        __weak typeof(self) weakSelf = self;
        EditAddressViewController *controller = [[EditAddressViewController alloc]initWithBlock:^(AddressEntity *entity) {
            [weakSelf reloadAddress:entity];
        }];
        [self.navigationController pushViewController:controller animated:YES];
        return;
    }
    
    [AppUtils showProgressMessage:@"正在处理"];
    [[GoodsHandler shareGoodsHandler]exchangeGoods:[self.infoControl goodsNum] goodsId:[@(self.goodsEntity.gid)stringValue] addressId:self.addressEntity.addressId success:^(id obj) {
        [AppUtils dismissHUD];
        float hongbao = 0.0;
        if (obj && [obj isKindOfClass:[NSNumber class]]) {
            hongbao = [obj floatValue];
        }
        else if (obj && [obj isKindOfClass:[NSString class]]){
            hongbao = [obj floatValue];
        }
        FinanceUserInfo *finance = [[FinanceHandler sharedInstance]financeUserInfo];
        finance.hongbao = hongbao;
        NSMutableArray *mbArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [mbArray removeLastObject];
        ExchangeSuccessViewController *controller = [[ExchangeSuccessViewController alloc]init];
        [mbArray addObject:controller];
        [self.navigationController setViewControllers:mbArray animated:YES];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)getFinanceInfo
{
    [[FinanceHandler sharedInstance]freeFinanceInfo];
    [[FinanceHandler sharedInstance]getFinanceInfo:nil failed:nil];
}

#pragma mark - UITableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [OrderInfoControl rowCount];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        return 66.f;
    }
    else{
        return 45.f;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.infoControl.cellsArray[indexPath.row];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2) {
        cell.backgroundColor = kGrayCellColor;
    }
    else{
        cell.backgroundColor = [UIColor whiteColor];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 1) {
        __weak typeof(self) weakSelf = self;
        if (!self.addressEntity) {
            EditAddressViewController *controller = [[EditAddressViewController alloc]initWithBlock:^(AddressEntity *entity) {
                [weakSelf reloadAddress:entity];
            }];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else{
            MyAddressViewController *controller = [[MyAddressViewController alloc]initWithAddressId:self.addressEntity.addressId addressBlock:^(AddressEntity *entity) {
                [weakSelf reloadAddress:entity];
            }];
            controller.initType=AddressInitTypeBuy;
            controller.needSelected = YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

#pragma mark - OrderInfoControl Delegate
- (void)changeGoodsNum
{
    //判断金额是否超过了红包金额
    FinanceUserInfo *finance = [[FinanceHandler sharedInstance]financeUserInfo];
    if (finance.hongbao < [self.infoControl actualPay]) {
        [AppUtils showAlertMessage:@"红包余额不够啦"];
        //将数量减一，重新计算数量
        [self.infoControl resetGoodsNum];
    }
}

@end
