//
//  QMSelectExpressViewController.h
//  QMMM
//  发货界面
//  Created by hanlu on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@class QMSelectExpressFootView;

@interface QMSelectExpressViewController : BaseViewController

//@property (nonatomic, strong) IBOutlet QMSelectExpressFootView * footView;

- (id)initWithOrderId:(NSString *)orderId;

- (void)setPopViewController:(UIViewController *)viewController;

@end
