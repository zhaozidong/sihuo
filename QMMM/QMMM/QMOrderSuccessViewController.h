//
//  QMOrderSuccessViewController.h
//  QMMM
//
//  Created by Derek.zhao on 14-12-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMOrderSuccessViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblTotal;
@property (strong, nonatomic) IBOutlet UILabel *lblSiHuoPay;
@property (strong, nonatomic) IBOutlet UILabel *lblAliPay;

- (void)setOrderId:(NSString *)orderId sihuoPay:(float)sihuoPay andAliPay:(float)aliPay;
- (IBAction)payForOrder:(id)sender;
- (IBAction)viewOrder:(id)sender;
@end
