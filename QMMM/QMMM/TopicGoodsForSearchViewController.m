//
//  TopicGoodsForSearchViewController.m
//  QMMM
//
//  Created by kingnet  on 15-3-19.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "TopicGoodsForSearchViewController.h"
#import "SearchResultViewController.h"
#import "GoodsHandler.h"
#import "GoodDataInfo.h"
#import "GoodEntity.h"

#import "QMFailPrompt.h"

@interface TopicGoodsForSearchViewController ()<SearchResultViewCtlDelegate>
{
    SearchResultViewController *searchResultViewCtl_;
    __block int offset_; //分页
    NSString *requestURL_;  //请求的路径
    NSString *navTitle_;  //标题
}
@end

@implementation TopicGoodsForSearchViewController

- (id)initWithTitle:(NSString *)title requestURL:(NSString *)requestURL
{
    self = [super init];
    if (self) {
        requestURL_ = requestURL;
        navTitle_ = title;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = navTitle_;
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    if (requestURL_ == nil) {
        QMFailPrompt *prompt = [[QMFailPrompt alloc]initWithFailType:QMFailTypeLoadFail labelText:@"哎呀，加载失败了" buttonType:QMFailButtonTypeNone buttonText:nil];
        prompt.frame = CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
        [self.view addSubview:prompt];
        [AppUtils showAlertMessage:[AppUtils localizedCommonString:@"QM_Alert_Network"]];
    }
    else{
        searchResultViewCtl_ = [[SearchResultViewController alloc]init];
        [self addChildViewController:searchResultViewCtl_];
        [searchResultViewCtl_ didMoveToParentViewController:searchResultViewCtl_];
        searchResultViewCtl_.view.frame = CGRectMake(0, kMainTopHeight, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-kMainTopHeight);
        searchResultViewCtl_.delegate = self;
        [self.view addSubview:searchResultViewCtl_.view];
        
        offset_ = 0;
        [AppUtils showProgressMessage:@"加载中"];
        [self getGoodsList];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//拼接URL
- (void)spliceURL
{
    NSRange range = [requestURL_ rangeOfString:[NSString stringWithFormat:@"&%@", kSearchGoodsOffset]];
    if (range.location!=NSNotFound) {
        NSString *subStr = [requestURL_ substringToIndex:range.location-1];
        requestURL_ = [NSString stringWithFormat:@"%@&%@=%d", subStr, kSearchGoodsOffset, offset_];
    }
    else{
        requestURL_ = [NSString stringWithFormat:@"%@&%@=%d", requestURL_, kSearchGoodsOffset, offset_];
    }
}

//从服务器请求商品
- (void)getGoodsList
{
    //加上offset
    [self spliceURL];
    __weak typeof(self) weakSelf = self;
    [[GoodsHandler shareGoodsHandler]topicsGoodsFromSearch:requestURL_ success:^(id obj) {
        [AppUtils dismissHUD];
        [searchResultViewCtl_ endFooterRereshing];
        NSDictionary *retDic = (NSDictionary *)obj;
        [weakSelf reloadUI:retDic[@"list"]];
        offset_ += [retDic[@"offset"] intValue];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        [searchResultViewCtl_ endFooterRereshing];
    }];
}

- (void)reloadUI:(NSArray *)dataArr
{
    if (dataArr.count>0) {
        NSMutableArray *goodsArr = [NSMutableArray array];
        for (GoodEntity *goods in dataArr) {
            GoodsSimpleInfo *goodsInfo = [[GoodsSimpleInfo alloc]init];
            goodsInfo.gid = goods.gid;
            goodsInfo.photo = goods.picturelist.rawArray[0];
            goodsInfo.desc = goods.desc;
            goodsInfo.price = goods.price;
            [goodsArr addObject:goodsInfo];
        }
        
        [searchResultViewCtl_ addGoods:goodsArr];
    }
    else{
        if (offset_ == 0) {
            QMFailPrompt *prompt = [[QMFailPrompt alloc]initWithFailType:QMFailTypeNOData labelText:@"没有找到相关商品~" buttonType:QMFailButtonTypeNone buttonText:nil];
            prompt.frame = CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
            [self.view addSubview:prompt];
            [AppUtils showAlertMessage:[AppUtils localizedCommonString:@"QM_Alert_Network"]];
        }
    }
}

#pragma mark - SearchResultViewCtlDelegate
- (void)loadMoreData
{
    [self getGoodsList];
}

@end
