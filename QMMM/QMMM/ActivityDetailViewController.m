//
//  ActivityDetailViewController.m
//  QMMM
//
//  Created by kingnet  on 15-1-4.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "ActivityDetailViewController.h"
#import "UIWebView+Additions.h"
#import "QMHttpClient.h"
#import "TGCameraNavigationController.h"
#import "AppDelegate.h"
#import "QMFailPrompt.h"
#import "TempFlag.h"
#import "QMWebViewDelegate.h"
#import "Friendhandle.h"
#import "QMFriendViewController.h"
#import "GoodsDetailViewController.h"
#import "PersonPageViewController.h"
#import "MLKMenuPopover.h"
#import "QMAllTopicViewController.h"
#import "QMShareViewContrller.h"
#import <ShareSDK/ShareSDK.h>
#import "BuyHongBaoGoodsViewController.h"
#import "YFInputBar.h"


@interface ActivityDetailViewController ()<JSMethodDelegate, UIAlertViewDelegate,MLKMenuPopoverDelegate,QMShareViewContrllerDelegate, YFInputBarDelegate>
{
    NSString *webURL_; //页面的url
    BannerEntity *_entity;
}

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) QMWebViewDelegate *webViewDelegate;
@property (nonatomic, strong) MLKMenuPopover *menuPopover;
@property (nonatomic, strong) YFInputBar *inputBar;

@end

@implementation ActivityDetailViewController

- (id)initWithUrl:(NSString *)url
{
    self = [super init];
    if (self) {
        webURL_ = url;
    }
    return self;
}

- (id)initWithEntity:(BannerEntity *)entity
{
    self = [super init];
    if (self) {
        webURL_ = entity.activityUrl;
        _entity=entity;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    if (_entity.isShowShare) {
        self.navigationItem.rightBarButtonItems=[AppUtils createRightButtonWithTarget:self selector:@selector(moreAction:) title:@"更多" size:CGSizeMake(50, 50) imageName:nil];
    }
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(processPublishGoodsOKNotify:) name:kPublishGoodsOKNotify object:nil];
    
    self.webViewDelegate = [[QMWebViewDelegate alloc]initWithDelegate:self];
    
    if (webURL_) {
        [self.view addSubview:self.webView];
        [self.webView loadRemoteUrl:webURL_];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadView) name:kReloadHtmlNotify object:nil];
        //添加键盘上部的输入框
        [self.view addSubview:[self inputBar]];
    }
    else{
        QMFailPrompt *prompt = [[QMFailPrompt alloc]initWithFailType:QMFailTypeLoadFail labelText:@"哎呀，加载失败了" buttonType:QMFailButtonTypeNone buttonText:nil];
        prompt.frame = CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
        [self.view addSubview:prompt];
        [AppUtils showAlertMessage:[AppUtils localizedCommonString:@"QM_Alert_Network"]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)backAction:(id)sender
{
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
    else {
        [super backAction:sender];
    }
}

-(void)moreAction:(id)sender{
    
    
    if (nil == self.menuPopover) {
        NSArray *menuItems=[NSArray arrayWithObjects:@"更多专题", @"分享",nil];
        NSArray *arrImages=[NSArray arrayWithObjects:@"menu_more",@"menu_share", nil];
        self.menuPopover = [[MLKMenuPopover alloc] initWithFrame:CGRectMake(kMainFrameWidth-140-5, kMainTopHeight+5, 140, 88) menuItems:menuItems imgItems:arrImages];
        self.menuPopover.menuPopoverDelegate = self;
    }
    
    [self.menuPopover showInView:self.view];
}

- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
        _webView.delegate = self.webViewDelegate;
        _webView.multipleTouchEnabled = NO;
        _webView.scalesPageToFit = YES;
        [_webView removeWebViewDoubleTapGestureRecognizer];
    }
    return _webView;
}

- (YFInputBar *)inputBar
{
    if (!_inputBar) {
        _inputBar = [[YFInputBar alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY([UIScreen mainScreen].bounds), CGRectGetWidth([UIScreen mainScreen].bounds), 50.f)];
        _inputBar.delegate = self;
        _inputBar.clearInputWhenSend = YES;
        _inputBar.resignFirstResponderWhenSend = YES;
    }
    return _inputBar;
}

- (void)reloadView
{
    [_webView reload];
}

#pragma mark - JSMethodeDelegate
- (void)inviteFriends
{    
    QMFriendViewController * viewController = [[QMFriendViewController alloc] initWithNibName:@"QMFriendViewController" bundle:nil];
    viewController.slideToInvite = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)publishGoods:(NSString *)from
{
    kChoice = from;
    kPublishGoodsFrom = from;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    TGCameraNavigationController *navigationController = [TGCameraNavigationController newWithCameraDelegate:appDelegate];
    [appDelegate.viewController presentViewController:navigationController animated:YES completion:nil];
}

- (void)setTitle:(NSString *)title
{
    self.navigationItem.title = title;
}

- (void)copyContent:(NSString *)content
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:content];
    [AppUtils showSuccessMessage:@"已复制"];
}

- (void)inviteAllFriends
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"确定全部邀请？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag = 1000;
    [alertView show];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)goodsDetail:(uint64_t)goodsId
{
    GoodsDetailViewController *controller = [[GoodsDetailViewController alloc]initWithGoodsId:goodsId];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)personPage:(uint64_t)userId
{
    PersonPageViewController *controller = [[PersonPageViewController alloc]initWithUserId:userId];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)buyHongBaoGoods:(int)num price:(float)price goodsId:(int)goodsId
{
    [AppUtils trackCustomEvent:@"click_exchangeGoods_event" args:nil];
    //跳转到下单界面
    BuyHongBaoGoodsViewController *controller = [[BuyHongBaoGoodsViewController alloc]initWithNum:num price:price goodsId:goodsId];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)keyboard
{
    [self.inputBar becomeFirstResponder];
}

#pragma mark - Notify Handler
- (void)processPublishGoodsOKNotify:(NSNotification *)notification
{
    NSArray *array = [NSArray arrayWithObject:[self.navigationController.viewControllers firstObject]];
    [self.navigationController setViewControllers:array];
}

#pragma mark - UIAlerViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex == buttonIndex) {
        return;
    }
    if (alertView.tag == 1000) {
        //全部邀请
        [AppUtils showProgressMessage:@"正在发送"];
        [[Friendhandle shareFriendHandle]inviteAllFriends:^(id obj) {
            [AppUtils showSuccessMessage:@"邀请完成"];
        } failed:^(id obj) {
            if ([obj isEqualToString:@"denied"]) {
                [AppUtils dismissHUD];
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请到设置里打开私货对通讯录的访问" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles: nil];
                [alertView show];
            }
            else if ([obj isEqualToString:@"unknown"]){
                [AppUtils dismissHUD];
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"该设备无法获得通讯录" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles: nil];
                [alertView show];
            }
            else{
                [AppUtils showErrorMessage:(NSString *)obj];
            }
        }];
    }
}

#pragma mark MLKMenuPopoverDelegate
- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex{
    if (0==selectedIndex) {
        QMAllTopicViewController *all=[[QMAllTopicViewController alloc] init];
        [self.navigationController pushViewController:all animated:YES];
    }else{
        QMShareViewContrller *share=[[QMShareViewContrller alloc] initWithShareMore];
        share.delegate = self;
        [share show];
        
        
        
//        NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"sharesdk_img" ofType:@"jpg"];
//        
//        //构造分享内容
//        id<ISSContent> publishContent = [ShareSDK content:@"test"
//                                           defaultContent:@""
//                                                    image:[ShareSDK imageWithPath:imagePath]
//                                                    title:@"ShareSDK"
//                                                      url:@"http://www.mob.com"
//                                              description:NSLocalizedString(@"TEXT_TEST_MSG", @"这是一条测试信息")
//                                                mediaType:SSPublishContentMediaTypeNews];
//        
//        
//        
////        //创建弹出菜单容器
////        id<ISSContainer> container = [ShareSDK container];
////        [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionUp];
//        
//        
//        
//        
//        
//        //自定义QQ空间分享菜单项
//        id<ISSShareActionSheetItem> qzoneItem = [ShareSDK shareActionSheetItemWithTitle:[ShareSDK getClientNameWithType:ShareTypeQQSpace]
//                                                                                   icon:[ShareSDK getClientIconWithType:ShareTypeQQSpace]
//                                                                           clickHandler:^{
//                                                                               [ShareSDK shareContent:publishContent
//                                                                                                 type:ShareTypeQQSpace
//                                                                                          authOptions:nil
//                                                                                        statusBarTips:YES
//                                                                                               result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
//                                                                                                   
//                                                                                                   if (state == SSPublishContentStateSuccess)
//                                                                                                   {
//                                                                                                       NSLog(NSLocalizedString(@"TEXT_SHARE_SUC", @"分享成功"));
//                                                                                                   }
//                                                                                                   else if (state == SSPublishContentStateFail)
//                                                                                                   {
//                                                                                                       NSLog(NSLocalizedString(@"TEXT_SHARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
//                                                                                                   }
//                                                                                               }];
//                                                                           }];
//
//        
//        //创建自定义分享列表
//        NSArray *shareList = [ShareSDK customShareListWithType:qzoneItem,
//                              nil];
//        
//        [ShareSDK showShareActionSheet:nil
//                             shareList:shareList
//                               content:publishContent
//                         statusBarTips:YES
//                           authOptions:nil
//                          shareOptions:[ShareSDK defaultShareOptionsWithTitle:nil
//                                                              oneKeyShareList:[NSArray defaultOneKeyShareList]
//                                                               qqButtonHidden:NO
//                                                        wxSessionButtonHidden:NO
//                                                       wxTimelineButtonHidden:NO
//                                                         showKeyboardOnAppear:NO
//                                                            shareViewDelegate:nil
//                                                          friendsViewDelegate:nil
//                                                        picViewerViewDelegate:nil]
//                                result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
//                                    
//                                    if (state == SSPublishContentStateSuccess)
//                                    {
//                                        NSLog(NSLocalizedString(@"TEXT_SHARE_SUC", @"发表成功"));
//                                    }
//                                    else if (state == SSPublishContentStateFail)
//                                    {
//                                        NSLog(NSLocalizedString(@"TEXT_SHARE_FAI", @"发布失败!error code == %d, error code == %@"), [error errorCode], [error errorDescription]);
//                                    }
//                                }];
        
    }
    
}

- (NSString *)shareJsonStr:(NSInteger)index
{
    NSString *plaform = @"";
    switch (index) {
        case 0:
        {
            [AppUtils trackCustomEvent:@"topics_shareToWeChat_event" args:nil];
            plaform = @"Wechat";
        }
            break;
        case 1:
        {
            [AppUtils trackCustomEvent:@"topics_shareToMoment_event" args:nil];
            plaform = @"WechatMoments";
        }
            break;
        case 2:
        {
            [AppUtils trackCustomEvent:@"topics_shareToWeibo_event" args:nil];
            plaform = @"SinaWeibo";
        }
            break;
        case 3:
        {
            [AppUtils trackCustomEvent:@"topics_shareToQZone_event" args:nil];
            plaform = @"QZone";
        }
            break;
        default:
            break;
    }

    NSString *jsonStr = [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"getShareData(\"%@\");", plaform]];
    return jsonStr;
}

#pragma mark - YFInputBarDelegate
- (void)inputBar:(YFInputBar *)inputBar sendBtnPress:(UIButton *)sendBtn withInputString:(NSString *)str
{
    NSString *temp = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (str && ![str isKindOfClass:[NSNull class]] && str.length>0 && ![temp isEqualToString:@""]) {
        [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"SihuoApp.Comments.Send(\"%@\");", str]];
    }
    else{
        [AppUtils showAlertMessage:@"评论的内容不能为空"];
        return;
    }
}

@end
