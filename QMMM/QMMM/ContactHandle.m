//
//  ContactHandle.m
//  QMMM
//
//  Created by hanlu on 14-10-21.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "ContactHandle.h"
#import "QMDataCenter.h"
#import "QMContactHelper.h"
#import "ContactorInfo.h"
#import "APAddressBook.h"
#import "APContact.h"
#import "UserInfoHandler.h"

@interface ContactHandle () {
    QMContactHelper * _contactHelper;
}

//@property (nonatomic, strong, readwrite) NSMutableDictionary * contactDict;

@end

@implementation ContactHandle

static ContactHandle* _shareInstance = NULL;

+ (ContactHandle *)shareContactInstance
{
    @synchronized([ContactHandle class]) {
        if (nil == _shareInstance) {
            _shareInstance = [[ContactHandle alloc] init];
        }
        return _shareInstance;
    }
}

- (id)init
{
    self = [super init];
    if(self) {
        _contactHelper = [QMDataCenter sharedDataCenter].contactHelper;
        //_contactDict = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)uploadContactList
{
//    [_contactHelper asyncDeleteAllContact:nil];
    [_contactHelper asyncLoadContactList:^(id result) {
        if(result && [result isKindOfClass:[NSArray class]]) {
            NSArray * array = [NSArray arrayWithArray:result];
            [self loadABAddressBook:array];
        }
    }];
}

- (void)loadABAddressBook:(NSArray *)array
{
    __block NSArray * localArray = array;
    
    APAddressBook *addressBook = [[APAddressBook alloc]init];
    addressBook.fieldsMask = APContactFieldDefault;
    addressBook.filterBlock = ^BOOL(APContact *contact){
        return contact.phones.count > 0;
    };
    
    
    [addressBook loadContactsOnQueue:dispatch_get_global_queue(0, 0) completion:^(NSArray *contacts, NSError *error) {
        if (!error) {
            //build dict
            NSMutableDictionary * localDict = [[NSMutableDictionary alloc] init];
            for (ContactorInfo * info in localArray) {
                [localDict setObject:info forKey:info.cellPhone];
            }
            
            //check diff
            NSMutableSet * abSet = [[NSMutableSet alloc] init];
            NSMutableArray * diffArray = [[NSMutableArray alloc] init];
            NSArray * array = [NSArray arrayWithArray:contacts];
            for (APContact * info in array) {
                for (NSString * phone in info.phones) {
                    
                    [abSet addObject:phone];
                    
                    //联系人名
                    NSString * firstName = [info.firstName length] > 0 ? info.firstName : @"";
                    NSString * lastName = [info.lastName length] > 0 ? info.lastName : @"";
                    NSString *name = [NSString stringWithFormat:@"%@%@", lastName, firstName];
                    
                    //新增联系人
                    ContactorInfo * contactorInfo = [localDict objectForKey:phone];
                    if(nil == contactorInfo) {
                        contactorInfo = [[ContactorInfo alloc] init];
                        contactorInfo.cellPhone = phone;
                        contactorInfo.name = name;
                        [diffArray addObject:contactorInfo];
                        [localDict setObject:contactorInfo forKey:phone];
                        continue;
                    }
                    
                    //联系人姓名变更
                    if(![contactorInfo.name isEqualToString:name]) {
                        contactorInfo.name = name;
                        [diffArray addObject:contactorInfo];
                    }
                }
            }
            
            //delete
            NSMutableArray * deleteArray = [[NSMutableArray alloc] init];
            for (ContactorInfo * info in localArray) {
                if(![abSet containsObject:info.cellPhone]) {
                    [deleteArray addObject:info];
                    [localDict removeObjectForKey:info.cellPhone];
                }
            }
            
            
            //新增或变化的
            if([diffArray count] > 0) {
                //更新变更的联系信息
                [_contactHelper asyncAddContactList:nil list:diffArray];
                
                //同步
                [[UserInfoHandler sharedInstance] uploadContacts:diffArray success:nil failed:nil];
            }
            
            //删除的
            for (ContactorInfo * info in deleteArray) {
                [_contactHelper asyncDeleteContact:nil cellPhone:info.cellPhone];
            }
        }
    }];
}

@end
