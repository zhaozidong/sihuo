//
//  ErrorEnum.h
//  QMMM
//
//  Created by kingnet  on 14-9-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, QMError){
    QMNotDictionaryError = 1000,
    QMCNotZeroError,
    QMNotNetworkError //没有网络时
};

