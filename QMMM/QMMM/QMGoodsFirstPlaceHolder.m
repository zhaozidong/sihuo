//
//  QMGoodsFirstPlaceHolder.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsFirstPlaceHolder.h"

@interface QMGoodsFirstPlaceHolder (){
    NSString *_btnTitle;
    QMGoodsListPlaceHolderType _type;
    UIImageView *_imgPlaceHolder;
    UIButton *_btnContact;
}
@end


@implementation QMGoodsFirstPlaceHolder

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithType:(QMGoodsListPlaceHolderType)type andTitle:(NSString *)title{
    self=[super init];
    if (self) {
        _type=type;
        _btnTitle=title;
        _imgPlaceHolder=[[UIImageView alloc] init];
        UIImage *_image;
        if (_type==QMGoodsListPlaceHolderTypeNoGoods) {
            _image=[UIImage imageNamed:@"placeHolderNOGoods"];
        }else if (_type==QMGoodsListPlaceHolderTypeCloseContact){
            _image=[UIImage imageNamed:@"placeholderCloseContact"];
        }else if (_type==QMGoodsListPlaceHolderTypeClostLongTime){
            _image=[UIImage imageNamed:@"placeHolderCloseLongTime"];
        }else if (_type==QMGoodsListPlaceHolderTypeInvite){
            _image=[UIImage imageNamed:@"placeHolderInvite"];
        }
        
        _imgPlaceHolder.frame=CGRectMake(0, 0, _image.size.width, _image.size.height);
        _imgPlaceHolder.image=_image;
        [self addSubview:_imgPlaceHolder];
        
        if (_type==QMGoodsListPlaceHolderTypeInvite) {
            _imgPlaceHolder.userInteractionEnabled=YES;
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
            tapGesture.numberOfTapsRequired = 1;
            tapGesture.numberOfTouchesRequired = 1;
            [_imgPlaceHolder addGestureRecognizer:tapGesture];
        }
        
        
        if (title) {
            _btnContact=[[UIButton alloc] init];
            //        _btnContact.frame=CGRectMake(0, 0, 100, 100);
            _btnContact.titleLabel.font=[UIFont systemFontOfSize:15.0f];
            [_btnContact setTitleColor:UIColorFromRGB(0x5381db) forState:UIControlStateNormal];
            [_btnContact setTitle:_btnTitle forState:UIControlStateNormal];
            [_btnContact addTarget:self action:@selector(btnDidClick:) forControlEvents:UIControlEventTouchUpInside];
            [_btnContact sizeToFit];
            [self addSubview:_btnContact];
        }
        
        self.backgroundColor=[UIColor whiteColor];
        
    }
    return self;
}


-(void)layoutSubviews{
//    _imgPlaceHolder.frame=CGRectMake(0, 0, 80, 80);
//CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2)
    _imgPlaceHolder.center=self.center;
    _btnContact.center=self.center;
    
}

- (void)handleTap:(UITapGestureRecognizer *)gesture{
    if (_delegate && [_delegate respondsToSelector:@selector(btnDidInvite)]) {
        [_delegate btnDidInvite];
    }
}

-(void)btnDidClick:(UIButton *)sender{
    if (_delegate && [_delegate respondsToSelector:@selector(btnDidOpenContact:)]) {
        [_delegate btnDidOpenContact:sender];
    }
}



@end
