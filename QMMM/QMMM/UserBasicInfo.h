//
//  UserBasicInfo.h
//  QMMM
//
//  Created by hanlu on 14-10-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@class  FMResultSet;

@interface UserBasicInfo : NSObject

@property (nonatomic, assign) uint64_t userId;

@property (nonatomic, strong) NSString * nickname;

@property (nonatomic, strong) NSString * icon;

@property (nonatomic, strong) NSString * remark;

//@property (nonatomic, strong) id extData;

- (id)initWithFMResultSet:(FMResultSet *)resultSet;

- (id)initWithDictionary:(NSDictionary *)dict;

- (id)initWithUserInfo:(UserBasicInfo *)info;

- (NSString *)showname;

@end
