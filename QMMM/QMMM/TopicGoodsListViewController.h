//
//  TopicGoodsListViewController.h
//  QMMM
//  小编精选的商品列表
//  Created by kingnet  on 15-3-16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@interface TopicGoodsListViewController : BaseViewController

- (id)initWithTitle:(NSString *)title requestURL:(NSString *)requestURL isTrust:(BOOL)isTrust;

@end
