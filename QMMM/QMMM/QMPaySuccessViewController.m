//
//  QMPaySuccessViewController.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMPaySuccessViewController.h"
#import "GoodsHandler.h"
#import "OrderDataInfo.h"
#import <CoreText/CoreText.h>
#import "ChatViewController.h"
#import "GoodsDetailViewController.h"
#import "QMOrderDetailViewController.h"

@interface QMPaySuccessViewController (){
    NSString *_orderId;
    OrderDataInfo * _detailInfo;
    
    UILabel *lblBuyer;
    UILabel *lblPhone;
    UILabel *lblAddress;
    UILabel *lblBuyCount;
    UILabel *lblPrice;
    UILabel *lblTotal;
    UILabel *lblTitleCount;
    UILabel *lblActualPay;
    
    UIView *viewLine;
    UIView *line2;
    UIView *line3;
    
    UIButton *btnContact;
    UIButton *btnView;
}

@end

@implementation QMPaySuccessViewController
-(id)initWithOrderId:(NSString *)orderId{
    self=[super init];
    if (self) {
        _orderId=orderId;
    }
    return self;
}

- (void)loadView
{
    UIScrollView *scroll= [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight)];
    scroll.backgroundColor = kBackgroundColor;
    scroll.contentSize=CGSizeMake(kMainFrameWidth, kMainFrameWidth+180);
    scroll.scrollEnabled=YES;
    self.view=scroll;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"购买成功";
    self.view.backgroundColor = kBackgroundColor;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout =UIRectEdgeNone;
    }
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    UIImageView *imageOK=[[UIImageView alloc] init];
    imageOK.frame=CGRectMake(kMainFrameWidth/2-25,45, 50, 50);
    imageOK.image=[UIImage imageNamed:@"payOK"];
    [self.view addSubview:imageOK];
    
    UILabel *lblSuccess=[[UILabel alloc]init];
    lblSuccess.frame=CGRectMake(20,imageOK.frame.origin.y+imageOK.frame.size.height+30 , kMainFrameWidth-15*2 , 30);
//    lblSuccess.backgroundColor=[UIColor redColor];
    lblSuccess.textAlignment=NSTextAlignmentCenter;
    lblSuccess.textColor=UIColorFromRGB(0x4f4f4f);
    lblSuccess.font=[UIFont systemFontOfSize:14.0f];
    lblSuccess.text=@"您的订单已支付成功，卖家会尽快为您发货~";
    [lblSuccess sizeToFit];
    CGPoint ptCenter=lblSuccess.center;
    ptCenter.x=kMainFrameWidth/2;
    lblSuccess.center=ptCenter;
    [self.view addSubview:lblSuccess];
    
    viewLine=[[UIView alloc] init];
    viewLine.frame=CGRectMake(0, lblSuccess.frame.origin.y+lblSuccess.frame.size.height+20, kMainFrameWidth, 0.5);
    viewLine.backgroundColor=UIColorFromRGB(0x4f4f4f);
    [self.view addSubview:viewLine];
    
    lblBuyer=[[UILabel alloc] init];
    lblBuyer.frame=CGRectMake(14, viewLine.frame.origin.y+14, 180, 20);
//    lblBuyer.backgroundColor=[UIColor purpleColor];
    lblBuyer.textColor=UIColorFromRGB(0x4f4f4f);
//    lblBuyer.text=@"收货人：吴可可";
    [self.view addSubview:lblBuyer];
    
    
    lblPhone=[[UILabel alloc] init];
    lblPhone.frame=CGRectMake(kMainFrameWidth-15-150, lblBuyer.frame.origin.y, 150, 20);
    lblPhone.textAlignment=NSTextAlignmentRight;
//    lblPhone.backgroundColor=[UIColor greenColor];
    lblPhone.textColor=UIColorFromRGB(0x4f4f4f);
//    lblPhone.text=@"15801332565";
    [self.view addSubview:lblPhone];
    
    lblAddress=[[UILabel alloc] init];
    lblAddress.frame=CGRectMake(14, lblPhone.frame.origin.y+lblPhone.frame.size.height+14, kMainFrameWidth-15*2, 60);
//    lblAddress.backgroundColor=[UIColor grayColor];
    lblAddress.numberOfLines=2;
    lblAddress.textColor=UIColorFromRGB(0x4f4f4f);
    lblAddress.lineBreakMode=NSLineBreakByCharWrapping;
//    lblAddress.text=@"收货地址：上海市闵行区浦江科技广场3号楼4楼1111111";
    [lblAddress sizeToFit];
    [self.view addSubview:lblAddress];
    
    
    line2=[[UIView alloc] init];
    line2.frame=CGRectMake(0, lblAddress.frame.origin.y+lblAddress.frame.size.height+14, kMainFrameWidth, 0.5);
    line2.backgroundColor=UIColorFromRGB(0x4f4f4f);
//    line2.backgroundColor = kBorderColor;
    [self.view addSubview:line2];
    
    lblTitleCount=[[UILabel alloc] init];
    lblTitleCount.frame=CGRectMake(15, line2.frame.origin.y+14, kMainFrameWidth, 20);
    lblTitleCount.text=@"购买数量";
    lblTitleCount.textColor=UIColorFromRGB(0x4f4f4f);
    [self.view addSubview:lblTitleCount];
    
    lblBuyCount=[[UILabel alloc] init];
    lblBuyCount.frame=CGRectMake(kMainFrameWidth-15-60, lblTitleCount.frame.origin.y, 60, 20);
    lblBuyCount.textAlignment=NSTextAlignmentRight;
//    lblBuyCount.text=@"× 1";
    [self.view addSubview:lblBuyCount];
    
    lblActualPay=[[UILabel alloc] init];
    lblActualPay.frame=CGRectMake(15, lblBuyCount.frame.origin.y+lblBuyCount.frame.size.height+14, 100, 20);
    lblActualPay.text=@"实际付款";
    lblActualPay.textColor=UIColorFromRGB(0x4f4f4f);
    [self.view addSubview:lblActualPay];
    
    
    lblPrice=[[UILabel alloc] init];
    lblPrice.frame=CGRectMake(kMainFrameWidth-15-100, lblActualPay.frame.origin.y, 100, 20);
//    lblPrice.text=@"￥500.00";
    lblPrice.textAlignment=NSTextAlignmentRight;
    lblPrice.textColor=[UIColor redColor];
    [self.view addSubview:lblPrice];
    
    lblTotal=[[UILabel alloc] init];
    lblTotal.frame=CGRectMake(15, lblActualPay.frame.origin.y+50, kMainFrameWidth-15*2, 20);
    lblTotal.textAlignment=NSTextAlignmentRight;
    [self.view addSubview:lblTotal];
    
    line3=[[UIView alloc] init];
    line3.frame=CGRectMake(0, lblTotal.frame.origin.y+30, kMainFrameWidth, 0.5);
    line3.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:line3];
    
    
    btnContact=[[UIButton alloc] init];
    btnContact.frame=CGRectMake(15, line3.frame.origin.y+20, kMainFrameWidth/2-15*2+5, 40);
    [btnContact setTitle:@"联系卖家发货" forState:UIControlStateNormal];
//    [btnContact setBackgroundColor:[UIColor  blueColor]];
//    btnContact.layer.cornerRadius=5.0f;
    UIImage *blueImg = [UIImage imageNamed:@"invite_btn"];
    blueImg = [blueImg stretchableImageWithLeftCapWidth:7 topCapHeight:7];
    [btnContact setBackgroundImage:blueImg forState:UIControlStateNormal];
    [btnContact setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnContact addTarget:self action:@selector(btnDidContact:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnContact];
    
    btnView=[[UIButton alloc] init];
    btnView.frame=CGRectMake(btnContact.frame.origin.x+btnContact.frame.size.width+15*2-10, btnContact.frame.origin.y, btnContact.frame.size.width, btnContact.frame.size.height);
    [btnView setTitle:@"查看购买商品" forState:UIControlStateNormal];
//    [btnView setBackgroundColor:[UIColor redColor]];
    [btnView setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    btnView.layer.cornerRadius=5.0f;
    UIImage *redImage = [UIImage imageNamed:@"ok_btn"];
    redImage = [redImage stretchableImageWithLeftCapWidth:5 topCapHeight:6];
    [btnView setBackgroundImage:redImage forState:UIControlStateNormal];
    [btnView addTarget:self action:@selector(btnDidView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnView];
    
    [self getOrderDetail];
}

- (void)backAction:(id)sender{
   
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setOrderInfo{
    if (_detailInfo) {
        lblBuyer.text=[NSString stringWithFormat:@"收货人:%@",_detailInfo.recipients] ;
        lblPhone.text=_detailInfo.recipients_phone;
        lblAddress.text=[NSString stringWithFormat:@"收货地址：%@",_detailInfo.recipients_address];
        lblBuyCount.text=[NSString stringWithFormat:@"× %u",_detailInfo.buy_num];
        lblPrice.text=[NSString stringWithFormat:@"￥%.2f",_detailInfo.payAmount];
        
        NSString *strBalance=[NSString stringWithFormat:@"￥%.2f",_detailInfo.balancePay];
        NSString *strAlipay=[NSString stringWithFormat:@"￥%.2f",_detailInfo.aliPay];
        
        NSString *string=[NSString stringWithFormat:@"账户余额%@ + 在线支付%@",strBalance,strAlipay];
        
        NSMutableAttributedString * attStr=[[NSMutableAttributedString alloc] initWithString:string];
        [attStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(4, [strBalance length])];
        [attStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange([string length]-[strAlipay length], [strAlipay length])];
        lblTotal.attributedText=attStr;
        
        [self layoutSubView];
    }
}


-(void)layoutSubView{
    lblBuyer.frame=CGRectMake(14, viewLine.frame.origin.y+14, 180, 20);
    lblPhone.frame=CGRectMake(kMainFrameWidth-15-150, lblBuyer.frame.origin.y, 150, 20);
    lblAddress.frame=CGRectMake(14, lblPhone.frame.origin.y+lblPhone.frame.size.height+14, kMainFrameWidth-15*2, 60);
    line2.frame=CGRectMake(0, lblAddress.frame.origin.y+lblAddress.frame.size.height+14, kMainFrameWidth, 0.5);
    lblTitleCount.frame=CGRectMake(15, line2.frame.origin.y+14, kMainFrameWidth, 20);
    lblBuyCount.frame=CGRectMake(kMainFrameWidth-15-60, lblTitleCount.frame.origin.y, 60, 20);
    
    lblActualPay.frame=CGRectMake(15, lblBuyCount.frame.origin.y+lblBuyCount.frame.size.height+14, 100, 20);
    lblPrice.frame=CGRectMake(kMainFrameWidth-15-100, lblActualPay.frame.origin.y, 100, 20);
    
    lblTotal.frame=CGRectMake(15, lblActualPay.frame.origin.y+50, kMainFrameWidth-15*2, 20);
    
    line3.frame=CGRectMake(0, lblTotal.frame.origin.y+30, kMainFrameWidth, 0.5);
    
    btnContact.frame=CGRectMake(15, line3.frame.origin.y+20, kMainFrameWidth/2-15*2+5, 40);
    btnView.frame=CGRectMake(btnContact.frame.origin.x+btnContact.frame.size.width+15*2-10, btnContact.frame.origin.y, btnContact.frame.size.width, btnContact.frame.size.height);
}





-(void)btnDidContact:(UIButton *)sender{//联系卖家
    NSString * uid = [NSString stringWithFormat:@"%qu", _detailInfo.user_Id];
    ChatViewController *chatController = [[ChatViewController alloc] initWithChatter:uid];
    NSMutableArray *mbArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
  
    int count = (int)[self.navigationController.viewControllers count];
    
    [mbArray removeObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(count-2, 2)]];
    [mbArray addObject:chatController];
    
    [self.navigationController setViewControllers:mbArray animated:YES];
}

-(void)btnDidView:(UIButton *)sender{//查看订单
    NSMutableArray *mbArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    int i=-1;
    int count = (int)[self.navigationController.viewControllers count];
    for (int k=(int)(mbArray.count-1); k>=0; k--) {
        UIViewController *viewController = [mbArray objectAtIndex:k];
        if ([viewController isKindOfClass:[QMOrderDetailViewController class]]) {
            i = k;
            break;
        }
    }
    
    if (i == -1) { //没有订单详情界面，push详情界面，并移除掉支付界面与该界面
        QMOrderDetailViewController * viewController = [[QMOrderDetailViewController alloc] initWithData:NO orderId:_detailInfo.orderId];
        [mbArray removeObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(count-2, 2)]];
        [mbArray addObject:viewController];
        [self.navigationController setViewControllers:mbArray animated:YES];
    }
    else{ //有订单详情界面，直接pop到订单详情界面
        UIViewController *viewController = [mbArray objectAtIndex:i];
        [self.navigationController popToViewController:viewController animated:YES];
    }
}

- (void)getOrderDetail
{
//    NSLog(@"orderId=%@",_orderId);
    
    
    [[GoodsHandler shareGoodsHandler] getOrderDetail:2 orderId:_orderId success:^(id result) {
        [AppUtils dismissHUD];
        @synchronized(_detailInfo) {
            _detailInfo = result;
        }
        [self setOrderInfo];
    } failed:^(id result) {
        if(result && [result isKindOfClass:[NSString class]]) {
            [AppUtils showErrorMessage:result];
        } else {
            [AppUtils showErrorMessage:@"获取订单详情失败"];
        }
    }];
}

@end
