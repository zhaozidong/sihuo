//
//  ActivityInfo.h
//  QMMM
//
//  Created by kingnet  on 14-12-31.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseEntity.h"

@interface ActivityInfo : BaseEntity

@property (nonatomic, strong) NSArray *activities;

@property (nonatomic, strong) NSArray *banners;

@property (nonatomic, strong) NSArray *categorys;

@property (nonatomic, strong) NSArray *specialTopics;

- (id)initWithDictionary:(NSDictionary *)dict;;

@end

/**
 发现页的专题
 **/
@interface ActivityEntity : BaseEntity

@property (nonatomic, copy) NSString *bigImgSrc; //大图路径

@property (nonatomic, copy) NSString *smallImgSrc; //小图路径

@property (nonatomic, copy) NSString *title; //专题标题

@property (nonatomic, copy) NSString *tag; //专题类型

@property (nonatomic, copy) NSString *desc; //专题描述

- (id)initWithDictionary:(NSDictionary *)dict;

- (id)initWithEntity:(ActivityEntity *)entity;

@end

/**
 banner
 **/
@interface BannerEntity : BaseEntity

@property (nonatomic, copy) NSString *imgSrc; //图片路径

@property (nonatomic, copy) NSString *activityUrl; //活动详情的url

@property (nonatomic, assign) NSInteger isShowShare;

- (id)initWithDictionary:(NSDictionary *)dict;

- (id)initWithEntity:(BannerEntity *)entity;

@end

/**
 分类
 **/
@interface CategoryEntity : BaseEntity

@property (nonatomic, assign) NSInteger categoryId;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *icon;

- (id)initWithDictionary:(NSDictionary *)dict;

- (id)initWithEntity:(CategoryEntity *)entity;

@end

/**
 专题
 **/
@interface SpecialTopicEntity : BaseEntity

@property (nonatomic, copy) NSString *type; //web、native

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *icon;

@property (nonatomic, copy) NSString *url;

@property (nonatomic, copy) NSString *parseType; //search   是否走搜索的接口

@property (nonatomic, copy) NSString *key;

@property (nonatomic, assign) BOOL isTrust; //是否为可信度板块

- (id)initWithDictionary:(NSDictionary *)dict;

- (id)initWithEntity:(SpecialTopicEntity *)entity;


@end







