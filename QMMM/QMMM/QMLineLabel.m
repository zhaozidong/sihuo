//
//  QMLineLabel.m
//  QMMM
//
//  Created by kingnet  on 14-9-18.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMLineLabel.h"

@implementation QMLineLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.backgroundColor = kBorderColor;
    self.text = @"";
}

@end
