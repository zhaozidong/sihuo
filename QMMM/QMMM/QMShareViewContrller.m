//
//  QMShareViewContrller.m
//  QMMM
//
//  Created by hanlu on 14-9-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMShareViewContrller.h"
#import "TTIconMenuBar.h"
#import "UIButton+Addition.h"
#import "GoodEntity.h"
#import "AppInfo.h"
#import "SystemHandler.h"
#import "SharedManager+Goods.h"
#import "NSString+JSONCategories.h"

@interface QMShareViewContrller () <TTIconMenuBarDelegate> {
    TTIconMenuBar * _menuBar;
    NSMutableArray * _menuItemArray;
    GoodEntity * _goodsInfo;
    AppInfo *_appInfo;
    BOOL _isShwoMore;
}

@end

@implementation QMShareViewContrller

- (id)init
{
    self = [super init];
    if(self) {
        [self initMenuBar];
        _isShwoMore=NO;
        _menuBar = [[TTIconMenuBar alloc] initWithIconMenuBarDelegate:self itemCount:[self numberOfItemsInMenuBar]];
        _menuBar.iconMenuBarDelegate = self;
    }
    return self;
}

- (void)initMenuBar
{
    _menuItemArray = [[NSMutableArray alloc] init];
    
    IconMenuItemData * menuItemData = [[IconMenuItemData alloc] initWithName:@"weixin" title:[AppUtils localizedProductString:@"QM_Text_Weixin"]];
    [_menuItemArray addObject:menuItemData];
    
    menuItemData = [[IconMenuItemData alloc] initWithName:@"weixin_friend" title:[AppUtils localizedProductString:@"QM_Text_Weixin_friend"]];
    [_menuItemArray addObject:menuItemData];
    
    menuItemData = [[IconMenuItemData alloc] initWithName:@"weibo_icon" title:@"新浪微博"];
    [_menuItemArray addObject:menuItemData];
    
    
    menuItemData = [[IconMenuItemData alloc] initWithName:@"qqzone_icon" title:@"QQ空间"];
    [_menuItemArray addObject:menuItemData];
    
    if (!_appInfo) {
        menuItemData = [[IconMenuItemData alloc] initWithName:@"copy" title:[AppUtils localizedProductString:@"QM_Text_Copy"]];
        [_menuItemArray addObject:menuItemData];
    }
}

-(id)initWithShareMore{
    self = [super init];
    if(self) {
        _menuItemArray = [[NSMutableArray alloc] init];
        _isShwoMore=YES;
        
        IconMenuItemData * menuItemData = [[IconMenuItemData alloc] initWithName:@"weixin" title:[AppUtils localizedProductString:@"QM_Text_Weixin"]];
        [_menuItemArray addObject:menuItemData];
        
        menuItemData = [[IconMenuItemData alloc] initWithName:@"weixin_friend" title:[AppUtils localizedProductString:@"QM_Text_Weixin_friend"]];
        [_menuItemArray addObject:menuItemData];
        
        
        menuItemData = [[IconMenuItemData alloc] initWithName:@"weibo_icon" title:@"新浪微博"];
        [_menuItemArray addObject:menuItemData];
        
        
        menuItemData = [[IconMenuItemData alloc] initWithName:@"qqzone_icon" title:@"QQ空间"];
        [_menuItemArray addObject:menuItemData];
        
        
        
        _menuBar = [[TTIconMenuBar alloc] initWithIconMenuBarDelegate:self itemCount:[self numberOfItemsInMenuBar]];
        _menuBar.iconMenuBarDelegate = self;
    }
    
    return self;
}

- (void)setGoodsInfo:(GoodEntity *)goodsInfo
{
    _goodsInfo = goodsInfo;
}

- (void)setAppInfo:(AppInfo *)appInfp
{
    _appInfo = appInfp;
}

- (void)show
{
    [_menuBar show];
}

#pragma mark - TTIconMenuBarDelegate

- (int)numberOfItemsInMenuBar
{
    if (_appInfo) {
        return 2;
    }
    return (int)[_menuItemArray count];
}

- (TTIconMenuItem*)menuItemForMenuAtIndex:(NSInteger)index
{
    TTIconMenuItem* cell = nil;
    
    if(index < [_menuItemArray count]) {
        cell = [[TTIconMenuItem alloc] init];
        IconMenuItemData * data = [_menuItemArray objectAtIndex:index];
        [cell setImage:[UIImage imageNamed:data.imageName] forState:UIControlStateNormal];
        [cell setImage:[UIImage imageNamed:data.imageName] forState:UIControlStateHighlighted];
        [cell setTitle:data.titleName forState:UIControlStateNormal];
        [cell setButtonType:BT_UpImageBottomTitle interGap:15];
    } else {
        cell = [[TTIconMenuItem alloc] init];
        [cell setBackgroundImage:cell.currentBackgroundImage forState:UIControlStateHighlighted];
    }
    cell.index = (int)index;
    return cell;
    
}

- (void)didTapOnItemAtIndex:(NSInteger)index
{
    NSString *platform = @"", *title = @"", *content = @"",*imageurl = @"", *shareUrl = @"", *desc = @"";
    if (_isShwoMore) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(shareJsonStr:)]) {
            NSString *jsonStr = [self.delegate shareJsonStr:index];
            jsonStr = [AppUtils DisEncodeUTF8ToChina:jsonStr];
            NSDictionary *params = [jsonStr toDict];
            id temp = [params objectForKey:@"platform"];
            if (temp && [temp isKindOfClass:[NSString class]]) {
                platform = temp;
            }
            temp = [params objectForKey:@"title"];
            if (temp && [temp isKindOfClass:[NSString class]]) {
                title = temp;
            }
            temp = [params objectForKey:@"content"];
            if (temp && [temp isKindOfClass:[NSString class]]) {
                content = temp;
            }
            temp = [params objectForKey:@"imageurl"];
            if (temp && [temp isKindOfClass:[NSString class]]) {
                imageurl = temp;
            }
            temp = [params objectForKey:@"url"];
            if (temp && [temp isKindOfClass:[NSString class]]) {
                shareUrl = temp;
            }
            temp = [params objectForKey:@"description"];
            if (temp && [temp isKindOfClass:[NSString class]]) {
                desc = temp;
            }
        }
    }
    switch (index) {
        //分享到对话
        case 0:
        {
            //统计分享到微信次数
            [AppUtils trackCustomEvent:@"share_weixin_event" args:nil];
            if (!_isShwoMore) {

                [[SharedManager sharedInstance]sharedWithType:QMSharedTypeWeiXin goodsEntity:_goodsInfo];
            }else{
                [[SharedManager sharedInstance]sharedWithType:QMSharedTypeWeiXin title:title content:content description:desc image:imageurl url:shareUrl];
            }
        }
            break;
        case 1:
        {
            //统计分享到微信朋友圈次数
            [AppUtils trackCustomEvent:@"share_weixin_friend_event" args:nil];
            if (!_isShwoMore) {
                [[SharedManager sharedInstance]sharedWithType:QMSharedTypeWeiXinFriends goodsEntity:_goodsInfo];
            }else{
                [[SharedManager sharedInstance]sharedWithType:QMSharedTypeWeiXinFriends title:title content:content description:desc image:imageurl url:shareUrl];
            }
        }
            break;
        case 2://微博
        {
            if (!_isShwoMore) {
                [[SharedManager sharedInstance]sharedWithType:QMSharedTypeSinaWeiBo goodsEntity:_goodsInfo];
            }else{
                [[SharedManager sharedInstance]sharedWithType:QMSharedTypeSinaWeiBo title:title content:content description:desc image:imageurl url:shareUrl];
            }
            break;
        }case 3:{//QZone
            if (!_isShwoMore) {
                [[SharedManager sharedInstance]sharedWithType:QMSharedTypeQZone goodsEntity:_goodsInfo];
            }
            else{
                [[SharedManager sharedInstance]sharedWithType:QMSharedTypeQZone title:title content:content description:desc image:imageurl url:shareUrl];
            }
            break;
        }
        case 4:{ //复制
            if (!_isShwoMore) {
                [[SharedManager sharedInstance]sharedWithType:QMSharedTypeCopyText goodsEntity:_goodsInfo];
            }
            else{
                [[SharedManager sharedInstance]sharedWithType:QMSharedTypeCopyText title:title content:content description:desc image:imageurl url:shareUrl];
            }
        }
        default:
            break;
    }
}

@end
