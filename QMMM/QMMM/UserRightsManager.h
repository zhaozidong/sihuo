//
//  UserRightsManager.h
//  QMMM
//
//  Created by Derek on 15/1/21.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, QMRightType) {
    QMRightTypeNotDetermined,
    QMRightTypeAuthorized,
    QMRightTypeDenied
};

typedef void(^successBlock)(QMRightType);
typedef void(^failureBlock)();

@interface UserRightsManager : NSObject


+(UserRightsManager *)sharedInstance;

- (void)getContactRights:(successBlock)successCallBack failure:(failureBlock)failureCallBack ;

- (BOOL)getPushNotificationRights;

- (void)showNotifyTip:(UIViewController *)viewCtl;

- (void)getMicroPhoneRights:(successBlock)successCallBack failure:(failureBlock)failureCallBack;


@end
