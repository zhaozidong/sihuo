//
//  WithDrawHeadView.h
//  QMMM
//
//  Created by kingnet  on 14-11-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WithDrawHeadView : UIView

+ (WithDrawHeadView *)headView;

+ (CGFloat)headViewHeight;

@property (nonatomic, assign) float money;

@end
