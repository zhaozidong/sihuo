//
//  EditGoodsToolBar.h
//  QMMM
//
//  Created by kingnet  on 15-3-2.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EditGoodsToolBarDelegate <NSObject>

- (void)didTapPublish;

- (void)didTapPreview;

@end
@interface EditGoodsToolBar : UIView

+ (EditGoodsToolBar *)toolBar;

+ (CGFloat)viewHeight;

@property (nonatomic, strong) id<EditGoodsToolBarDelegate> delegate;

@property (nonatomic, strong) NSString *btnTitle;

@end
