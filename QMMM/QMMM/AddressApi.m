//
//  AddressApi.m
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "AddressApi.h"
#import "APIConfig.h"
#import "QMHttpClient.h"

@implementation AddressApi

- (void)getAddressList:(contextBlock)context
{
    __block contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_ADDRESS_LIST];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"getAddressList error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------getAddressList fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });

        }];
    });

}

- (void)addNewAddress:(int)province city:(int)city district:(int)district address:(NSString *)address name:(NSString *)name mobile:(NSString *)mobile isDefault:(BOOL)isDefault context:(contextBlock)context
{
    __block contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_NEW_ADDRESS];
    
    NSDictionary *params = @{@"province":@(province), @"city":@(city), @"district":@(district), @"address":address, @"name":name, @"mobile":mobile, @"is_default":@(isDefault)};
   
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"add new address error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------add new address fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });

}

- (void)updateAddress:(int)id_ province:(int)province city:(int)city district:(int)district address:(NSString *)address name:(NSString *)name mobile:(NSString *)mobile isDefault:(BOOL)isDefault context:(contextBlock)context
{
    __block contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_UPDATE_ADDRESS];
    
    NSDictionary *params = @{@"id":@(id_), @"province":@(province), @"city":@(city), @"district":@(district), @"address":address, @"name":name, @"mobile":mobile, @"is_default":@(isDefault)};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"update address error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------update address fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });
}

- (void)deleteAddress:(int)id_ context:(contextBlock)context
{
    __block contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:[NSString stringWithFormat:API_DELETE_ADDRESS, id_]];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"delete address error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------delete address fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });
}

@end
