//
//  QAudioPlayer.m
//  QMMM
//
//  Created by Derek.zhao on 14-11-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QAudioPlayer.h"

@implementation QAudioPlayer{
    QMGoodsAudioView *audioView;
}

static QAudioPlayer * sharedInstance = nil;
+(QAudioPlayer *) sharedInstance
{
    @synchronized(self)
    {
        if(nil == sharedInstance)
        {
            [self new];
        }
    }
    return sharedInstance;
}

+(id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if(sharedInstance == nil)
        {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;
        }
    }
    return nil;
}


-(void)playAudioWithPath:(NSString *)strPath withRelatedView:(id)relatedView{
    if (nil ==strPath) {
        return;
    }
    strAudioPath=strPath;
//    finishBlk=successBlock;
    [self stopPlay];
    audioView =(QMGoodsAudioView *)relatedView;
    audioPlayer = nil;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    NSURL *urlMP3 = [NSURL fileURLWithPath:strAudioPath];
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:urlMP3 error:&error];
    audioPlayer.delegate=self;
    [audioPlayer setCurrentTime:0];
    
    if (error) {
        NSLog(@"播放文件失败 path=%@",strPath);
        NSLog(@"err.code=%ld,err.description=%@",(long)error.code,error.description);
        return;
    }
//    audioPlayer.enableRate=YES;
//    [audioPlayer prepareToPlay];
    [audioPlayer play];

}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag//播放完成
{
    if (flag) {
        [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
//        if (finishBlk) {
//            finishBlk(flag);
//        }
        audioPlayer=nil;
        if (audioView) {
            [audioView didPlayAudioStop:nil];
        }
        NSLog(@"播放完成停止");
    }
}

-(BOOL)isPlaying{
    if (audioPlayer) {
        return audioPlayer.isPlaying;
    }
    return NO;
}

-(void)stopPlay{
    if (audioPlayer && audioPlayer.isPlaying) {
        [audioPlayer stop];
        NSLog(@"手动停止播放");
        audioPlayer=nil;
        if (audioView) {
            [audioView didPlayAudioStop:nil];
        }
    }
}

-(void)pausePlay{
    if (audioPlayer && audioPlayer.isPlaying) {
        [audioPlayer pause];
//        NSLog(@"播放已暂停");
    }
}

-(BOOL)deleteAudio{
    if (!strAudioPath) {
        return  YES;
    }
    if (strAudioPath && [self deleteFileAtPath:strAudioPath]) {
        strAudioPath=nil;
        return YES;
    }
    return NO;
}

-(BOOL)deleteFileAtPath:(NSString *)strFilePath{
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    BOOL bRet = [fileMgr fileExistsAtPath:strFilePath];
    if (bRet) {
        NSError *err;
        [fileMgr removeItemAtPath:strFilePath error:&err];
        if (err) {
            NSLog(@"删除文件失败");
            return NO;
        }else{
            NSLog(@"删除文件成功");
            return YES;
        }
    }else{
        NSLog(@"要删除的文件不存在");
    }
    return YES;
}


-(NSString *)getTimeForAudio{
    NSURL *urlMP3 = [NSURL fileURLWithPath:strAudioPath];
    NSError *error;
    audioPlayer = nil;
    audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:urlMP3 error:&error];
    audioPlayer.delegate=self;
    NSTimeInterval aTimer = [audioPlayer duration];
    int hour = (int)(aTimer/3600);
    int minute = (int)(aTimer - hour*3600)/60;
    int second = aTimer - hour*3600 - minute*60;
    NSString *time;
    if (0==minute) {
        time=[NSString stringWithFormat:@"%d〞",second];
    }else{
        time=[NSString stringWithFormat:@"%d´%d〞",minute,second];
    }
    audioPlayer=nil;
    return time;
}


@end
