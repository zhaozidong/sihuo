//
//  MinePhotoCell.m
//  QMMM
//
//  Created by kingnet  on 14-9-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MinePhotoCell.h"
#import "PhotoPreviewView.h"
#import "MinePhotoCollectionCell.h"
#import "UserEntity.h"
#import "KL_ImagesZoomController.h"
#import "UserDataInfo.h"
#import "UserInfoHandler.h"

@interface MinePhotoCell ()<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PhotoPreviewViewDelegate, MinePhotoCollectionCellDelegate>
{
    NSMutableDictionary *keyAndFilePath_; //存放已经上传的图片key path
}
@property (weak, nonatomic) IBOutlet UICollectionView *collection;
@property (nonatomic, strong) KL_ImagesZoomController *imageZoomView;
@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic, strong) NSMutableArray *smallImageArray;
@end

const NSInteger kPhotoNum = 8;

@implementation MinePhotoCell

+ (MinePhotoCell *)minePhotoCell
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"MinePhotoCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.collection.delegate = self;
    self.collection.dataSource = self;
    self.collection.backgroundColor = [UIColor whiteColor];
    self.collection.scrollEnabled = NO;
    
    [self.collection registerClass:[MinePhotoCollectionCell class] forCellWithReuseIdentifier:kMinePhotoCellIdentifier];

    keyAndFilePath_ = [NSMutableDictionary dictionary];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(oneTapHandler:) name:kShowImageOneTapNotity object:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

+ (CGFloat)rowHeight
{
    CGSize size = [MinePhotoCollectionCell cellSize];
    return [self itemBorder]*3+size.height*2+32;
}

+ (CGFloat)itemBorder
{
//    CGSize size = [MinePhotoCollectionCell cellSize];
//    CGFloat w = (kMainFrameWidth-(size.width*4))/5;
//    return w;
    return 8;
}

- (NSMutableArray *)photoArray
{
    if (!_photoArray) {
        _photoArray = [[NSMutableArray alloc] initWithCapacity:kPhotoNum];
    }
    return _photoArray;
}

- (void)updateUI:(UserEntity *)entity
{
    if (entity) {
        NSArray *photos = [entity lifePhotos];
        [self.photoArray removeAllObjects];
        [self.photoArray addObjectsFromArray:photos];
        [self.collection reloadData];
        
        self.mainImageIndex = [photos indexOfObject:entity.avatar];
    }                         
}

- (void)updateUIWithUserDataInfo:(UserDataInfo *)userInfo
{
    [self.photoArray removeAllObjects];
    [self.photoArray addObjectsFromArray:userInfo.lifePhotos];
    [self.collection reloadData];
}

- (void)addPhotos:(NSArray *)photos
{
    MinePhotoCell __weak * weakSelf = self;
    [self.collection performBatchUpdates:^{
        NSMutableArray *arr = [NSMutableArray array];
        for (int i=0; i<photos.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(i+self.photoArray.count) inSection:0];
            [arr addObject:indexPath];
        }
        [weakSelf.photoArray addObjectsFromArray:photos];
        [weakSelf.collection insertItemsAtIndexPaths:arr];
    } completion:^(BOOL finished) {
        
    }];
}

- (NSMutableArray *)imageArray
{
    if (!_imageArray) {
        _imageArray = [NSMutableArray array];
        for (NSString *s in self.photoArray) {
            NSString *urlS = [AppUtils getAbsolutePath:s];
            [_imageArray addObject:urlS];
        }
    }
    return _imageArray;
}

- (NSMutableArray *)smallImageArray
{
    if (!_smallImageArray) {
        _smallImageArray = [NSMutableArray array];
        for (NSString *s in self.photoArray) {
            NSString *urlS = [AppUtils getAbsolutePath:[AppUtils smallImagePath:s width:140]];
            [_smallImageArray addObject:urlS];
        }
    }
    return _smallImageArray;
}

- (NSArray *)keysArray
{
    BOOL flag = NO;  //标识是否需要更新生活照
    UserEntity *user = [UserInfoHandler sharedInstance].currentUser;
    NSArray *photos = [user lifePhotos];
    
    if (photos.count != self.photoArray.count) {
        flag = YES;
    }
    else{
        //检查是否有新上传的路径
        for (NSString *path in self.photoArray) {
            NSFileManager *fileManager = [NSFileManager defaultManager];
            if ([fileManager fileExistsAtPath:path]) {
                flag = YES;
                break;
            }
        }
    }
    
    if (flag) {
        NSMutableArray *mbArray = [NSMutableArray array];
        for (NSString *path in self.photoArray) {
            NSFileManager *fileManager = [NSFileManager defaultManager];
            if ([fileManager fileExistsAtPath:path]) {
                NSString *key = [keyAndFilePath_ objectForKey:path];
                if (key && ![key isEqualToString:@""]) {
                    [mbArray addObject:key];
                }
            }
            else{
                [mbArray addObject:path];
            }
        }
        
        return mbArray;
    }
    return nil;
}

#pragma mark - Notification Handler
- (void)oneTapHandler:(NSNotification *)notification
{
    __weak __typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        __typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.imageZoomView.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            __typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.imageZoomView removeFromSuperview];
            strongSelf.imageZoomView = nil;
        }
    }];
}

#pragma mark - Preview Delegate
- (void)deleteImageWithPath:(NSIndexPath *)indexPath
{
    if (indexPath) {
        [self.collection performBatchUpdates:^{
            [self.photoArray removeObjectAtIndex:indexPath.row];
            NSArray *array = [NSArray arrayWithObject:indexPath];
            [self.collection deleteItemsAtIndexPaths:array];
        } completion:^(BOOL finished) {
            if (finished) {
            }
        }];
    }
}

- (void)makeMainPhoto:(NSString *)imgFilePath
{
    NSInteger i = [self.photoArray indexOfObject:imgFilePath];
    self.mainImageIndex = i;
}

#pragma mark - MinePhotoCollectionCell Delegate
- (void)addPhotoKey:(NSString *)key filePath:(NSString *)filePath
{
    [keyAndFilePath_ setObject:key forKey:filePath];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.photoType == QMEditMinePhoto) {
        return [self.photoArray count]+1; //1个是AddButton
    }
    else{
        return [self.photoArray count];
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MinePhotoCollectionCell *cell = (MinePhotoCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kMinePhotoCellIdentifier forIndexPath:indexPath];
    if (indexPath.row == self.photoArray.count) {
        cell.isLastOne = YES;
    }
    else{
        cell.isLastOne = NO;
        cell.filePath = [self.photoArray objectAtIndex:indexPath.row];
        cell.delegate = self;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.photoArray.count) {
        if (self.photoType == QMEditMinePhoto) {
            MinePhotoCollectionCell *cell = (MinePhotoCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
            
            //这句话会导致再创建一个cell ==!
//            MinePhotoCollectionCell *cell = (MinePhotoCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kMinePhotoCellIdentifier forIndexPath:indexPath];
          
            if (cell.isUploading) {
                [AppUtils showErrorMessage:@"正在上传"];
            }
            else if (NO == cell.isUploadOK){ //上传失败了
                [cell uploadPhoto];
            }
            else{
                PhotoPreviewView *preview = [PhotoPreviewView photoPreviewView];
                preview.delegate = self;
                NSString *filePath = (NSString *)[self.photoArray objectAtIndex:indexPath.row];
                NSFileManager *fileManager = [NSFileManager defaultManager];
                if ([fileManager fileExistsAtPath:filePath]) {
                    NSString *bigS = [filePath stringByReplacingOccurrencesOfString:@"_small" withString:@""];
                    NSString *editPath = [filePath stringByReplacingOccurrencesOfString:@"_small" withString:@"_edit"];
                    if ([fileManager fileExistsAtPath:bigS]) {//原图大图存在
                        filePath = bigS;
                    }
                    else if ([fileManager fileExistsAtPath:editPath]){//经过滤镜处理过的大图
                        filePath = editPath;
                    }
                }

                preview.imageSizeType = QMImageQuarterSize;
                preview.filePath = filePath;
                preview.previewType = QMLifePhotoPreview;
                preview.isMainPhoto = NO;
                preview.indexPath = indexPath;
                [preview show];
            }
        }
        else{
            UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
            if (!self.imageZoomView) {
                self.imageZoomView = [[KL_ImagesZoomController alloc] initWithFrame:CGRectMake(0, 0, keyWindow.frame.size.width, keyWindow.frame.size.height )imgViewSize:CGSizeZero];
            }
            self.imageZoomView.alpha = 0.0;
            [keyWindow addSubview:self.imageZoomView];
            
            __weak __typeof(self) weakSelf = self;
            [UIView animateWithDuration:0.2 animations:^{
                __typeof(weakSelf) strongSelf = weakSelf;
                strongSelf.imageZoomView.alpha = 1.0;
            } completion:^(BOOL finished) {
                if (finished) {
                    __typeof(weakSelf) strongSelf = weakSelf;
                    [strongSelf.imageZoomView updateImageDate:[strongSelf imageArray] smallImgs:[strongSelf smallImageArray] selectIndex:indexPath.row];
                }
            }];
        }
    }
    else{
        //打开相机
        if (self.delegate && [self.delegate respondsToSelector:@selector(didTapAddPhoto)]) {
            [self.delegate didTapAddPhoto];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [MinePhotoCollectionCell cellSize];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    //一排中每块之间的距离
//    float iosV = [[[UIDevice currentDevice] systemVersion] floatValue];
//    if (iosV >= 8.0) {
//        return [MinePhotoCell itemBorder];
//    }
//    return 0;
    return [MinePhotoCell itemBorder];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return [MinePhotoCell itemBorder]; //每行之间的距离
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    float w = (float)[MinePhotoCell itemBorder];
    return UIEdgeInsetsMake(w, w, w, w);
}

@end
