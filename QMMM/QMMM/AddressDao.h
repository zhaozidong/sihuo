//
//  AddressDao.h
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseDao.h"
#import "AddressEntity.h"

@interface AddressDao : BaseDao

+ (AddressDao *)sharedInstance;

- (void)updateFirstRow;

- (int)getAddressCount;

@end
