//
//  QMPayOrderViewController.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMPayOrderViewController.h"
#import "MyOrderInfo.h"
#import "QMPayFailViewController.h"
#import "QMPaySuccessViewController.h"
#import "FinanceHandler.h"
#import "FinanceDataInfo.h"
#import "QMOrderDetailViewController.h"

@interface QMPayOrderViewController ()<QMPayOrderDelegate>{
    float _sihuoPay;
    float _aliPay;
    NSString *_orderId;
    float _totalPay;
}

@end

@implementation QMPayOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title=@"支付";
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout =UIRectEdgeNone;
    }
    self.view.backgroundColor = kBackgroundColor;
    
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    [self getFinanceInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getFinanceInfo
{
    //拉取余额
    [AppUtils showProgressMessage:@"加载中"];
    [[FinanceHandler sharedInstance]freeFinanceInfo];
    __weak typeof(self) weakSelf = self;
    [[FinanceHandler sharedInstance]getFinanceInfo:^(id obj) {
        [AppUtils dismissHUD];
        //刷新界面
        [weakSelf reloadView];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:[NSString stringWithFormat:@"余额获取失败：%@",(NSString *)obj]];
        [weakSelf reloadView];
    }];
}

- (void)reloadView
{
    FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
    if (financeInfo) {
        if (financeInfo.amount >= _totalPay) { //余额够用
            _sihuoPay = _totalPay;
            _aliPay = 0.0;
        }
        else{
            _sihuoPay = financeInfo.amount;
            _aliPay = _totalPay - financeInfo.amount;
        }
    }
    else{
        _sihuoPay = 0.0;
        _aliPay = _totalPay;
    }
    
    _lblTotal.text=[NSString stringWithFormat:@"￥%.2f",_totalPay];
    _lblRemainPay.text=[NSString stringWithFormat:@"￥%.2f",_sihuoPay];
    _lblAliPay.text=[NSString stringWithFormat:@"￥%.2f",_aliPay];
}

- (void)setOrderId:(NSString *)orderId totalPay:(float)totalPay
{
    _orderId = orderId;
    _totalPay = totalPay;
}

- (void)setOrderId:(NSString *)orderId sihuoPay:(float)sihuoPay andAliPay:(float)aliPay {
    _sihuoPay=sihuoPay;
    _aliPay=aliPay;
    _orderId=orderId;
}

- (IBAction)payForOrder:(id)sender {//去支付
    [AppUtils trackCustomEvent:@"click_payMoneyBtn_event" args:nil];
    
    if (_orderId) {
        MyOrderInfo *orderInfo=[[MyOrderInfo alloc] initWithOrderId:_orderId andRemain:_sihuoPay];
        orderInfo.delegate=self;
        [orderInfo payOrder];
    }else{
        DLog(@"支付参数为空");
    }
}

-(void)payFail:(NSString *)tip{
    [AppUtils showErrorMessage:tip];
    
    QMPayFailViewController *payFail=[[QMPayFailViewController alloc] initWithNibName:@"QMPayFailViewController" bundle:nil];
    [payFail setOrderId:_orderId sihuoPay:_sihuoPay andAliPay:_aliPay];
    [self.navigationController pushViewController:payFail animated:YES];
}

-(void)paySuccessForOrderId:(NSString *)orderId{
    
    if (orderId) {
        NSMutableArray *mbArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        int count = (int)mbArray.count;
        [mbArray removeObjectsInRange:NSMakeRange(count-2, 2)];
        
        QMOrderDetailViewController *orderDetail = [[QMOrderDetailViewController alloc]initWithData:NO orderId:orderId];
        [mbArray addObject:orderDetail];
        
        QMPaySuccessViewController *paySuccess=[[QMPaySuccessViewController alloc] initWithOrderId:orderId];
        [mbArray addObject:paySuccess];
        
        [self.navigationController setViewControllers:mbArray animated:YES];
        orderDetail = nil;
        paySuccess = nil;
        
    }else{
        DLog(@"订单id为空");
    }
}

@end
