//
//  AddressHandler.m
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "AddressHandler.h"
#import "AddressApi.h"
#import "AddressDao.h"
#import "UserInfoHandler.h"
#import "UserEntity.h"

@interface AddressHandler ()
{
    AddressApi *addressApi_;
}
@end
@implementation AddressHandler

- (instancetype)init
{
    self = [super init];
    if (self) {
        addressApi_ = [[AddressApi alloc]init];
    }
    return self;
}

- (void)getAddressList:(SuccessBlock)success failed:(FailedBlock)failed
{
    NSMutableArray *array = [[AddressDao sharedInstance]queryAll];
    if (array.count > 0) {
        if (success) {
            success(array);
        }
    }
    else{
        [addressApi_ getAddressList:^(id resultDic) {
            if ([resultDic[@"s"] intValue] == 0) {
                NSArray *dataArr = (NSArray *)resultDic[@"d"];
                NSMutableArray *models = [NSMutableArray array];
                for (NSDictionary *dic in dataArr) {
                    AddressEntity *entity = [[AddressEntity alloc]initWithDictionary:dic];
                    [models addObject:entity];
                    [[AddressDao sharedInstance] insert:entity];
                }
                if (success) {
                    success(models);
                }
            }
            else{
                if (failed) {
                    failed(resultDic[@"d"]);
                }
            }
        }];
    }
}

- (void)addNewAddress:(AddressEntity *)entity success:(SuccessBlock)success failed:(FailedBlock)failed
{
    __block AddressEntity *blockEntity = entity;
    int row = [[AddressDao sharedInstance]getAddressCount];
    if (row == 0) {
        blockEntity.isDefault = YES;
        entity.isDefault = YES;
    }
    [addressApi_ addNewAddress:entity.provinceId city:entity.cityId district:entity.distritId address:entity.detailAddress name:entity.consignee mobile:entity.phone isDefault:entity.isDefault context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            UserEntity *entity = [UserInfoHandler sharedInstance].currentUser;
            blockEntity.uid = [entity.uid longLongValue];
            NSDictionary *dataDic = resultDic[@"d"];
            blockEntity.addressId = [[dataDic objectForKey:@"id"] intValue];
            NSString *errMsg = [AppUtils localizedCommonString:@"QM_Alert_Error"];
            BOOL rs = [[AddressDao sharedInstance]insert:blockEntity];
            if (rs) {
                if (success) {
                    success(blockEntity);
                }
            }
            else{
                if (failed) {
                    failed(errMsg);
                }
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)updateAddress:(AddressEntity *)entity success:(SuccessBlock)success failed:(FailedBlock)failed
{
    __block AddressEntity *blockEntity = entity;
    int row = [[AddressDao sharedInstance]getAddressCount];
    if (row == 0) {
        blockEntity.isDefault = YES;
        entity.isDefault = YES;
    }

    [addressApi_ updateAddress:entity.addressId province:entity.provinceId city:entity.cityId district:entity.distritId address:entity.detailAddress name:entity.consignee mobile:entity.phone isDefault:entity.isDefault context:^(id resultDic) {
        
        if ([resultDic[@"s"] intValue] == 0) {
            UserEntity *entity = [UserInfoHandler sharedInstance].currentUser;
            blockEntity.uid = [entity.uid longLongValue];
            NSString *errMsg = [AppUtils localizedCommonString:@"QM_Alert_Error"];
            BOOL rs = [[AddressDao sharedInstance]update:blockEntity];
            if (rs) {
                if (success) {
                    success(blockEntity);
                }
            }
            else{
                if (failed) {
                    failed(errMsg);
                }
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)deleteAddress:(AddressEntity *)entity success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [addressApi_ deleteAddress:entity.addressId context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            //删除本地的数据
            NSString *errMsg = [AppUtils localizedCommonString:@"QM_Alert_Error"];
            BOOL rs = [[AddressDao sharedInstance]remove:entity];
            if (rs) {
                if (entity.isDefault) {
                    //把第一条设置成是默认的
                    [[AddressDao sharedInstance]updateFirstRow];
                }
                if (success) {
                    success(kHTTPTreatSuccess);
                }
            }
            else{
                if (failed) {
                    failed(errMsg);
                }
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

@end
