//
//  QMSelectExpressCell.h
//  QMMM
//
//  Created by hanlu on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMSelectExpressCellIdentifier;

@protocol QMSelectExpressDelegate;

@interface QMSelectExpressCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) IBOutlet UICollectionView * collectionView;

@property (nonatomic, weak) id<QMSelectExpressDelegate> delegate;

+ (id)cellforSelectExpress;

- (void)updateUI:(NSArray *)expressArray;

@end

@protocol QMSelectExpressDelegate <NSObject>

- (void)didSelectedItem:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)didCallBtnClick:(NSString *)phoneNum;

@end