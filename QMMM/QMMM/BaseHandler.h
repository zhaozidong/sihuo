//
//  BaseHandler.h
//  QMMM
//  BaseHandler : Every subclass handler should extends
//  Created by kingnet  on 14-8-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Handler处理完成后调用的Block
 */
typedef void (^CompleteBlock)();

/**
 *  Handler处理成功时调用的Block
 *  @param obj 为客户端需要的数据
 */
typedef void (^SuccessBlock)(id obj);

/**
 *  Handler处理失败时调用的Block
 *  @param obj 为错误信息
 */
typedef void (^FailedBlock)(id obj);

extern NSString * const kServerErrorTip;
extern NSString * const kHTTPTreatSuccess;
extern NSString * const kHTTPTreatFailed;
extern NSString * const kNotNetWorkError;

@class AFHTTPRequestOperation;

@interface BaseHandler : NSObject

/**
 *  获取请求URL
 *  @param path
 *  @return 拼装好的URL
 */
+ (NSString *)requestUrlWithPath:(NSString *)path;

/**
 * 获取以https请求的URL
 * @param path 相对路径
 * @return 拼接好的URL
 **/
+ (NSString *)httpsRequestUrlWithPath:(NSString *)path;
/**
 * 获取以上传文件请求的URL
 * @param path 相对路径
 * @return 拼接好的URL
 **/
+ (NSString *)uploadFileWithPath:(NSString *)path;

/**
 * 发短信
 * @param type 可为register login change_password
 * @param mobile 手机号码
 **/
- (void)sendSMSWithMobile:(NSString *)mobile type:(NSString *)type success:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 * msgpack解析NSData
 * @param data 待解析的数据
 * @param error 解析出错时的错误信息
 * @return 拿到的d字段数据，如果出错则为nil
 **/
- (id)parseData:(NSData *)data error:(NSError * __autoreleasing *)error;

//音频存放目录
+ (NSString *)audioPath;

//获取百度经纬度
- (void)getBaiDuLatitude:(double)latitude longitude:(double)longitude success:(SuccessBlock)success failed:(FailedBlock)failed;

//获取详细位置
- (void)getDetailLoc:(double)latitude longitude:(double)longitude  success:(SuccessBlock)success failed:(FailedBlock)failed;

/**
 * 上传图片
 * @param datas 图片的data数据
 * @param type 图片的类型 可选值为goods life_photo
 */
- (void)batchUploadImageData:(NSArray *)datas type:(NSString *)type context:(contextBlock)context;

/**
 处理http返回的值
 **/
- (void)processResponse:(id)responseObject error:(NSError *)error operation:(AFHTTPRequestOperation *)operation context:(contextBlock)context;

@end
