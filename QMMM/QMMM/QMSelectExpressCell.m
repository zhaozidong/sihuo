//
//  QMSelectExpressCell.m
//  QMMM
//
//  Created by hanlu on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMSelectExpressCell.h"
#import "QMExpressCollectionViewCell.h"

#define MARGIN 6

NSString * const kQMSelectExpressCellIdentifier = @"kQMSelectExpressCellIdentifier";

@interface QMSelectExpressCell ()<QMExpressCallBtnDelegate> {
    
    NSArray * _expressArray;
}

@end

@implementation QMSelectExpressCell

+ (id)cellforSelectExpress
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMSelectExpressCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerClass:[QMExpressCollectionViewCell class] forCellWithReuseIdentifier:kQMExpressCollectionViewCellIdentifier];
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    self.contentView.backgroundColor = [UIColor clearColor];
    
    self.backgroundColor = [UIColor clearColor];
//    self.contentView.backgroundColor = [UIColor redColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(NSArray *)expressArray
{
    if(_expressArray != expressArray) {
        _expressArray = expressArray;
         [_collectionView reloadData];
    }
}

#pragma mark - UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_expressArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    QMExpressCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:kQMExpressCollectionViewCellIdentifier forIndexPath:indexPath];
    NSDictionary * dict = [_expressArray objectAtIndex:indexPath.row];
    [cell updateUI:dict];
    cell.delegate = self;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(_delegate && [_delegate respondsToSelector:@selector(didSelectedItem:didSelectItemAtIndexPath:)]) {
        [_delegate didSelectedItem:collectionView didSelectItemAtIndexPath:indexPath];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - UICollectionViewDelegateFlowLayout

//每个collectionView的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float width = (collectionView.frame.size.width - MARGIN * 3) / 2;
    return CGSizeMake(width, 45);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, MARGIN, 0, MARGIN);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return MARGIN;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _collectionView.frame = self.bounds;
}

#pragma mark -

- (void)didExpressCallBtnClick:(NSString *)phoneNum
{
    if(_delegate && [_delegate respondsToSelector:@selector(didCallBtnClick:)]) {
        [_delegate didCallBtnClick:phoneNum];
    }
}

@end
