//
//  GoodsCategoryViewController.h
//  QMMM
//
//  Created by kingnet  on 15-1-14.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

typedef void(^CategoryBlock)(int categoryId, NSString *categoryName);

@interface GoodsCategoryViewController : BaseViewController

@property (nonatomic, copy) CategoryBlock categoryBlock;

- (id)initWithBlock:(CategoryBlock)block;

@end
