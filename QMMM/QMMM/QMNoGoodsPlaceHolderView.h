//
//  QMNoGoodsPlaceHolderView.h
//  QMMM
//
//  Created by Derek on 15/3/30.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QMNoGoodsPlaceHolderDelegate <NSObject>

-(void)btnDidClickNoGoods:(id)sender;


@end




@interface QMNoGoodsPlaceHolderView : UIView

@property(nonatomic, weak) id<QMNoGoodsPlaceHolderDelegate> delegate;

@end
