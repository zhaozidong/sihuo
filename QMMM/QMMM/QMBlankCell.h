//
//  QMBlankCell.h
//  QMMM
//
//  Created by hanlu on 14-10-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    QMFailTypeNOSellingGoods, //没有在卖的商品
    QMFailTypeNOSoldGoods, //没有已卖掉的商品
    QMFailTypeNOBoughtGoods //没有已买到的商品
} QMFailGoodsType;

extern NSString * const kQMBlankCellIdentifier;

@interface QMBlankCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView * defaultImageView;

@property (nonatomic, strong) IBOutlet UILabel * tipsLabel;

@property (nonatomic, assign) QMFailGoodsType failGoodsType;

+ (id)cellForBlank;

- (void)setTips:(NSString *)tips;

@end
