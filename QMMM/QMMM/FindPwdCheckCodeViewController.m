//
//  FindPwdCheckCodeViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "FindPwdCheckCodeViewController.h"
#import "LRCheckCodeViewController.h"
#import "UserInfoHandler.h"
#import "AppDelegate.h"

@interface FindPwdCheckCodeViewController ()<LRCheckCodeViewDelegate>
{
    NSString *phoneNum_;
    NSString *pwd_;
}
@property (nonatomic, strong) LRCheckCodeViewController *checkCodeController;

@end

@implementation FindPwdCheckCodeViewController

- (id)initWithPhoneNum:(NSString *)phoneNum pwd:(NSString *)pwd
{
    self = [super init];
    if (self) {
        phoneNum_ = phoneNum;
        pwd_ = pwd;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = [AppUtils localizedPersonString:@"QM_Text_ForgetPwd"];
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    self.canTapCloseKeyboard = YES;
    
    self.checkCodeController = [[LRCheckCodeViewController alloc]init];
    self.checkCodeController.submitBtnTitle = @"完成";
    self.checkCodeController.delegate = self;
    
    [self addChildViewController:self.checkCodeController];
    
    [[self view] addSubview:[self.checkCodeController view]];
    
    [self.checkCodeController didMoveToParentViewController:self];
    
    [self sendSMS];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sendSMS
{
    __weak typeof(self) weakSelf = self;
    [[UserInfoHandler sharedInstance] sendSMSWithMobile:phoneNum_ type:@"recovery_password" success:^(id obj) {
//        [AppUtils showAlertMessage:[NSString stringWithFormat:@"验证码：%@(仅供测试用)", (NSString *)obj]];
        NSLog(@"check code:%@", obj);
        
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        //重置button
        [weakSelf.checkCodeController stopTimer];
        //拿到错误代码
        NSRange range = [(NSString *)obj rangeOfString:@"(" options:NSBackwardsSearch];
        if (range.location != NSNotFound) {
            NSString *str = [(NSString *)obj substringFromIndex:range.location+1];
            NSString *str1 = [str substringToIndex:str.length-1];
            if ([str1 intValue] == 1202) {
                //手机号码格式错误
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.0*NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^{
                    [weakSelf backToRoot:NO];
                });
            }
        }

    }];
}

#pragma mark - 
- (void)clickGetCheckCode
{
    [self.checkCodeController startTimer];
    [self sendSMS];
}

- (void)clickSubmit:(NSString *)checkCode
{
    __weak typeof(self) weakSelf = self;
    [AppUtils showProgressMessage:[AppUtils localizedPersonString:@"QM_HUD_Treating"]];
    [[UserInfoHandler sharedInstance] executeFindPwdWithNewPwd:pwd_ checkCode:checkCode phoneNum:phoneNum_ success:^(id obj) {
        [AppUtils showSuccessMessage:[AppUtils localizedPersonString:@"QM_Tip_ChangeOK"]];
        
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate startApp];
        
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        //拿到错误代码
        NSRange range = [(NSString *)obj rangeOfString:@"(" options:NSBackwardsSearch];
        if (range.location != NSNotFound) {
            NSString *str = [(NSString *)obj substringFromIndex:range.location+1];
            NSString *str1 = [str substringToIndex:str.length-1];
            if ([str1 intValue] == 1018 || [str1 intValue] == 1021) {
                //手机号码格式错误或手机号码未注册
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.0*NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^{
                    [weakSelf backToRoot:NO];
                });
            }
        }
    }];
}

- (void)backToRoot:(BOOL)isToRoot
{
    if (isToRoot) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
