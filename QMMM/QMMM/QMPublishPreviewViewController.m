//
//  QMPublishPreviewViewController.m
//  QMMM
//
//  Created by Derek on 15/2/28.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMPublishPreviewViewController.h"
#import "QMGoodsTitleCell.h"
#import "QMGoodsImageCellV2.h"
//#import "GoodsListDescCell.h"
#import "QMSubmitButton.h"
#import "GoodsHandler.h"
#import "PublishOKViewController.h"
#import "GoodsDetailDescCell.h"
#import "QMGoodsLocationCell.h"

@interface QMPublishPreviewViewController ()<UITableViewDataSource,UITableViewDelegate>{
    UITableView *_tableView;
    GoodEntity *_goodEntity;
    NSDictionary *_dictPath;
    NSArray *_arrImgPath;
    NSString *_localAudioPath;
    QMEditGoodsType _editType;
    GoodEntity *_previewGoods; //预览的商品信息，图片和本地路径跟goodsEntity不同
}
@property (nonatomic, strong) GoodsDetailDescCell *detailDescCell;
@end

@implementation QMPublishPreviewViewController

- (id)initWithGoodsEntity:(GoodEntity *)goodEntity localImgPath:(NSArray *)arrImgPath localAudioPath:(NSString *)localAudioPath editType:(QMEditGoodsType)editType{
    self=[super init];
    if (self) {
        _goodEntity=goodEntity;
        _arrImgPath=arrImgPath;
        _localAudioPath=localAudioPath;
        _editType=editType;
        _previewGoods = [[GoodEntity alloc]initWithGoodsEntity:_goodEntity];
        GoodsPictureList *picList = [[GoodsPictureList alloc]initWithArray:arrImgPath];
        _previewGoods.picturelist = picList;
        _previewGoods.msgAudio = localAudioPath;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"商品预览";
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
    self.navigationItem.leftBarButtonItems=[AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight-kMainTopHeight-60) style:UITableViewStylePlain];
    _tableView.separatorColor=[UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.allowsSelection = NO;
    _tableView.contentInset = UIEdgeInsetsMake(10, 0, 20, 0);
    [self.view addSubview:_tableView];
    
    [self addPrivew];
}

-(void)addPrivew{
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(-1, kMainFrameHeight-kMainTopHeight-60+1, kMainFrameWidth+2, 60)];
    view.backgroundColor=[UIColor whiteColor];
    view.layer.borderColor=[UIColor grayColor].CGColor;
    view.layer.borderWidth=0.5f;

    QMSubmitButton *preView=[[QMSubmitButton alloc] initWithFrame:CGRectMake(16, 10, kMainFrameWidth-2*16, 40)];
    if (_editType==QMEditNewGoods) {
        [preView setTitle:@"确认发布" forState:UIControlStateNormal];
    }else{
        [preView setTitle:@"确定更新" forState:UIControlStateNormal];
    }
    
    [preView addTarget:self action:@selector(btnDidTapPublish:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:preView];
    
    [self.view addSubview:view];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (GoodsDetailDescCell *)detailDescCell
{
    if (!_detailDescCell) {
        _detailDescCell = [GoodsDetailDescCell goodsDetailDescCell];
        _detailDescCell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _detailDescCell.frame = CGRectMake(0, 0, kMainFrameWidth, CGRectGetHeight(_detailDescCell.frame));
    }
    return _detailDescCell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark
#pragma mark -UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {//标题
        QMGoodsTitleCell *titleCell=[QMGoodsTitleCell cellAwakeFromNib];
        [titleCell updateUI:_goodEntity showFull:YES];
        return titleCell;
    }else if(indexPath.row==1){//图片
        QMGoodsImageCellV2 *imageCell=[QMGoodsImageCellV2 cellAwakeFromNib];
        [imageCell setShowFull:YES];
        
        [imageCell updateUI:_previewGoods localImgPath:_arrImgPath localAudioPath:_localAudioPath];
        return imageCell;
    }else if(indexPath.row==2){//描述
//        GoodsListDescCell *desc=[GoodsListDescCell goodsListDescCell];
//        [desc maxLine:0];
//        [desc updateUI:_goodEntity isPreview:YES];
        
        [self.detailDescCell updateUI:_goodEntity];
        return self.detailDescCell;
    }else if (indexPath.row==3){
        QMGoodsLocationCell *location=[QMGoodsLocationCell locationCell];
        [location updateUI:_goodEntity isPreView:YES];
        return location;
    }
    return nil;
}


#pragma mark
#pragma mark -UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return [QMGoodsTitleCell heightForGoodsTitleCell:_goodEntity];
    }else if (indexPath.row==1){
        if (0 != _goodEntity.msgAudioSeconds) {//有声音
            return (kMainFrameWidth-2*15)+46*2;
        }else{
            return (kMainFrameWidth-3*15)+46*2;
        }
//        return kMainFrameWidth+50;
    }else if (indexPath.row==2){
//        return [GoodsListDescCell heightForGoodsListCell:_goodEntity];
        [self.detailDescCell updateUI:_goodEntity];
        [self.detailDescCell setNeedsLayout];
        [self.detailDescCell layoutIfNeeded];
        CGFloat height = [self.detailDescCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
        height += 5;
        return height;
    }else if(indexPath.row==3){
        return 30.0f;
    }
    return 100;
}

//发布
-(void)btnDidTapPublish:(id)sender{
    if (_editType == QMEditNewGoods) {//发布新商品
        [self publishGoods];
    }
    else{//更新
        [self updateGoods];
//        NSArray *controllers=self.navigationController.viewControllers;
//        UIViewController *vi=[controllers  objectAtIndex:[controllers count]-2];
//        [self.navigationController popToViewController:vi animated:YES];
    }
}

- (void)publishGoods
{
    [AppUtils showProgressMessage:[AppUtils localizedProductString:@"QM_HUD_Releasing"]];
    __weak typeof(self) weakSelf = self;
    [[GoodsHandler shareGoodsHandler]releaseGoods:_goodEntity success:^(id obj) {
        [AppUtils showSuccessMessage:@"发布成功"];
        GoodEntity *goods = [[GoodEntity alloc]initWithGoodsEntity:_goodEntity];
        if ([obj isKindOfClass:[NSDictionary class]]) {
            id tmp = [obj objectForKey:@"id"];
            if (tmp && ([tmp isKindOfClass:[NSNumber class]] || [tmp isKindOfClass:[NSString class]])) {
                goods.gid = [tmp longLongValue];
            }
            
            tmp = [obj objectForKey:@"photo"];
            if (tmp && [tmp isKindOfClass:[NSArray class]]) {
                GoodsPictureList *picList = [[GoodsPictureList alloc]initWithArray:tmp];
                goods.picturelist = picList;
            }
        }
        
        PublishOKViewController *controller = [[PublishOKViewController alloc]initWithGoodsEntity:goods];
        NSArray *viewControllers = [NSArray arrayWithObject:controller];
        [weakSelf.navigationController setViewControllers:viewControllers animated:YES];
        
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)updateGoods
{
    [AppUtils showProgressMessage:@"正在提交"];
    __weak __typeof(self) weakSelf = self;
    DLog(@"updateGoods:%@", _goodEntity);
    [[GoodsHandler shareGoodsHandler]updateGoodsInfo:_goodEntity success:^(id obj) {
        [AppUtils showSuccessMessage:@"更新成功"];
        //通知前面的界面
        __typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf.updateGoodsBlock) {
            strongSelf.updateGoodsBlock(_goodEntity);
        }
        NSMutableArray *controllers = [NSMutableArray arrayWithArray:strongSelf.navigationController.viewControllers];
        [controllers removeObjectsInRange:NSMakeRange(controllers.count-3, 3)];
        [strongSelf.navigationController setViewControllers:controllers animated:YES];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    return 60.0f;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    
//    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, 60)];
//    view.backgroundColor=[UIColor clearColor];
////    view.layer.borderColor=[UIColor grayColor].CGColor;
////    view.layer.borderWidth=1;
////    
////    view.layer.shadowOffset=CGSizeMake(0, 2);
////    view.layer.shadowOpacity=0.6f;
////    view.layer.shadowColor=[UIColor grayColor].CGColor;
//    
//    
////    UIButton *btnPublish=[UIButton buttonWithType:UIButtonTypeRoundedRect];
////    btnPublish.frame=CGRectMake(16, 10, kMainFrameWidth-2*16, 40);
////    btnPublish.backgroundColor=[UIColor redColor];
////    [btnPublish setTitle:@"确认发布" forState:UIControlStateNormal];
////    [btnPublish setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
////    btnPublish.titleLabel.font=[UIFont systemFontOfSize:20.0f];
////    btnPublish.layer.cornerRadius=5.0f;
////    [view addSubview:btnPublish];
//    
//    QMSubmitButton *preView=[[QMSubmitButton alloc] initWithFrame:CGRectMake(16, 10, kMainFrameWidth-2*16, 40)];
//    [preView setTitle:@"确认发布" forState:UIControlStateNormal];
//    [view addSubview:preView];
//    
//    
//    
//    return view;
//}

@end
