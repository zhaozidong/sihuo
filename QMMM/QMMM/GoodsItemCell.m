//
//  GoodsItemCell.m
//  QMMM
//
//  Created by kingnet  on 15-3-16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "GoodsItemCell.h"
#import "GoodsItem.h"

NSString * const kGoodsItemCellIdentifier = @"kGoodsItemCellIdentifier";

@implementation GoodsItemCell
@synthesize items = _items;

- (id)initWithItems:(NSArray *)items
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kGoodsItemCellIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.items = items;
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setItems:(NSArray *)items
{
    @synchronized(self)
    {
        if (_items != items) {
            _items = items;
            for (UIView *view in [self.contentView subviews]) {
                [view removeFromSuperview];
            }
            
            for (GoodsItem * goodsItem in _items) {
                [self.contentView addSubview:goodsItem];
            }
            [self setItemPosition];
        }
    }
}

- (NSArray *)items
{
    NSArray *array = nil;
    @synchronized(self)
    {
        array = _items;
    }
    return array;
}

- (void)setItemPosition
{
    self.frame = CGRectMake(0, 0, self.frame.size.width, [GoodsItem itemSize].height+[GoodsItem margin]);
    self.contentView.frame = self.bounds;
    float x = [GoodsItem margin];
    for (GoodsItem *goodsItem in _items) {
        goodsItem.translatesAutoresizingMaskIntoConstraints = NO;
        NSDictionary *views = NSDictionaryOfVariableBindings(goodsItem);
        NSDictionary *metrics = @{@"margin":@([GoodsItem margin])};
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[goodsItem]-margin-|" options:0 metrics:metrics views:views]];
        metrics = @{@"x":@(x), @"w":@([GoodsItem itemSize].width)};
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-x-[goodsItem(w)]" options:0 metrics:metrics views:views]];
        x += [GoodsItem itemSize].width + [GoodsItem margin];
    }
}

@end
