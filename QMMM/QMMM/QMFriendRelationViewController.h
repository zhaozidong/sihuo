//
//  QMFriendRelationViewController.h
//  QMMM
//
//  Created by Derek on 15/1/26.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"
#import "UserDataInfo.h"


@interface QMFriendRelationViewController : BaseViewController


@property (nonatomic, strong)UITableView *tableView;


-(instancetype)initWithUserInfo:(NSArray *)arrUserInfo;


@end
