//
//  GoodsDetailBaseCell.m
//  QMMM
//
//  Created by kingnet  on 14-9-18.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsDetailBaseCell.h"

@implementation GoodsDetailBaseCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state

}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView setNeedsDisplay];
    [self.contentView layoutIfNeeded];
}



@end
