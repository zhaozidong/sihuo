//
//  QMFriendViewController.m
//  QMMM
//
//  Created by hanlu on 14-10-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//
#import "QMFriendViewController.h"
#import "Friendhandle.h"
#import "FriendInfo.h"
#import "QMContactCell.h"
#import "QMSwipeContactCell.h"
#import "QMNewFriendViewController.h"
#import "PersonPageViewController.h"
#import "MsgHandle.h"
#import "QMRemarkViewController.h"
#import "ContactInfo.h"
#import "UserInfoHandler.h"
#import "QMInviteContactCell.h"
#import "QMProbablyKnowViewController.h"

@interface QMFriendViewController () <UITableViewDataSource, UITableViewDelegate, RMSwipeTableViewCellDelegate, QMSwipeContactDelegate,QMInviteContactDelegate, UIAlertViewDelegate,UISearchDisplayDelegate,UISearchControllerDelegate> {
    NSMutableArray * _friendList;
    NSMutableArray * _friendSectionList;
    
    NSMutableArray * _sectionIndexForTitle;
    
    uint    _unreadCount;
    
    NSIndexPath * _swipeIndexPath;
    NSIndexPath * _selectIndexPath;
    
    
//add by zzd
    NSMutableArray * _friendListPhone;
    NSMutableArray * _inviteList;
    NSMutableArray * _invitedPhoneList;
    
    NSString * _tmpInviteString;
    
    BOOL _needGetFriendList;
    
    NSMutableArray *_arrSearchFriend;
    NSMutableArray *_arrSearchContact;
    
    NSString *strSearch;
}

@end

@implementation QMFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout =UIRectEdgeNone;
    }
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    if(kIOS7) {
        self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        self.tableView.separatorInset = UIEdgeInsetsZero;
    }
    self.tableView.sectionIndexColor = UIColorFromRGB(0x687483);
    self.tableView.separatorColor = UIColorFromRGB(0xd8dee6);
    self.tableView.backgroundColor = UIColorFromRGB(0xededed);
    
//    self.searchBar.tintColor=[UIColor redColor];
//    self.searchDisplayController.searchBar.barTintColor = [UIColor greenColor];
    
    _sectionIndexForTitle = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processFriendListLoadedNotify:) name:kFriendListLoadedNotify object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processFriendListChangedNotify:) name:kFriendListChangedNotify object:nil];
    //有人申请加为好友
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processRefreshMsgListNotify:) name:kRefreshMsgListNotify object:nil];
    //更改好友备注
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processUpdateFriendRemarkNotify:) name:kUpdateFriendRemarkNotify object:nil];
    //加载朋友列表失败
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processFriendListLoadedFailedNotify:) name:kFriendListLoadedFailedNotify object:nil];
    
    [AppUtils showProgressMessage:@"正在加载"];
    
    _friendList = [[NSMutableArray alloc] init];
    _friendSectionList = [[NSMutableArray alloc] init];
    _friendListPhone=[[NSMutableArray alloc] init];
    
    _arrSearchFriend=[[NSMutableArray alloc] init];
    _arrSearchContact=[[NSMutableArray alloc] init];
    
//    [[Friendhandle shareFriendHandle] getLocalFriendList];
    
//add by zhaozd
    
    _inviteList = [[NSMutableArray alloc] init];
    
    _needGetFriendList=YES;
    if(_needGetFriendList) {
        [[Friendhandle shareFriendHandle] getFriendList];//从服务器获取朋友列表
    } else {
        [self getContactList];
    }
    
    //已发送过邀请的列表
    [self LoadInvitedPhoneList];
}

- (void)refreshFriendUnreadCount
{
     //加载未读消息数目
    __weak UITableView * weakTableView = self.tableView;
    [[Friendhandle shareFriendHandle] getFriendUnreadMsgCount:^(id result) {
        _unreadCount = [result unsignedIntValue];
        
//        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        [weakTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [weakTableView reloadData];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = @"我的朋友";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backBtnClick:)];
    
    //刷新
    [[MsgHandle shareMsgHandler] getMsgList];
    
    //加载未读消息数目
    [self refreshFriendUnreadCount];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)backBtnClick:(id)sender
{
    [AppUtils dismissHUD];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - NSNotification

- (void)processFriendListChangedNotify:(NSNotification *)notification
{
    [[Friendhandle shareFriendHandle] getFriendList];
}

- (void)processFriendListLoadedNotify:(NSNotification *)notification
{
    [AppUtils dismissHUD];
    
    NSMutableArray * myFriendList = [Friendhandle shareFriendHandle].friendList;
    NSMutableArray * myFriendSectionList = [Friendhandle shareFriendHandle].friendSectionList;
    
    @synchronized(myFriendList) {
        [_friendList removeAllObjects];
        [_friendList addObjectsFromArray:myFriendList];
        
        for (FriendInfo * info in myFriendList) {
            [_friendListPhone addObject:info.cellphone];
        }
    }
    @synchronized(myFriendSectionList) {
        [_friendSectionList removeAllObjects];
        [_friendSectionList addObjectsFromArray:myFriendSectionList];
    }
    
    [self getContactList];
    
//    [_tableView reloadData];
}

- (void)processRefreshMsgListNotify:(NSNotification *)notification
{
    [self refreshFriendUnreadCount];
}

- (void)processUpdateFriendRemarkNotify:(NSNotification *)notification
{
    [[Friendhandle shareFriendHandle] getFriendList];
//    if(_selectIndexPath && _selectIndexPath.section < [_friendSectionList count] + 1) {
//        [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:_selectIndexPath] withRowAnimation:UITableViewRowAnimationFade];
//    }
}

- (void)processFriendListLoadedFailedNotify:(NSNotification *)notification
{
    NSString *errorStr = notification.object;
    [AppUtils showErrorMessage:errorStr];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==self.tableView) {
        return [_friendSectionList count] + 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.tableView) {
        if(0 == section) {//新的朋友
            return 2;
        }else if(section == [_friendSectionList count]+1){//邀请朋友
            if ([_inviteList count]>0) {
                return [_inviteList count]+1;
            }else{
                return [_inviteList count];
            }
        }else {//已存在的朋友
            if([_friendSectionList count] > section - 1) {
                FriendSectionInfo * sectionInfo = [_friendSectionList objectAtIndex:section - 1];
                return sectionInfo.length;
            } else {
                return 0;
            }
        }
    }else{
        return [_arrSearchFriend count]+[_arrSearchContact count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * retCell = nil;
    if (tableView==self.tableView) {
        if( 0 == indexPath.section ) {//新的朋友
            QMContactCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMContactCellIdentifier];
            if(nil == cell) {
                cell = [QMContactCell cellForContact];
            }
            
            if(0 == indexPath.row) {
                cell.contactLabel.text = @"新的朋友";
                [cell setUnreadCount:_unreadCount];
                cell.headImageView.image = [UIImage imageNamed:@"newFriend_bkg"];
            }else if (1==indexPath.row){
                cell.contactLabel.text=@"可能认识的人";
                cell.headImageView.image =[UIImage imageNamed:@"newFriend_ProbablyKnow"];
            }
            retCell = cell;
            
        }
        else if(indexPath.section==[_friendSectionList count]+1){//邀请
            QMInviteContactCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMInviteContactCellIdentifier];
            if(nil == cell) {
                cell = [QMInviteContactCell cellForInviteContact];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
            if (indexPath.row==0 && [_inviteList count]>0) {//全部邀请
                ContactInfo *contact=[[ContactInfo alloc] init];
                contact.personName=@"来自通讯录的好友";
                [cell updateUI:contact];
            }else{
                ContactInfo * contactInfo = [_inviteList objectAtIndex:indexPath.row-1];
                [cell updateUI:contactInfo];
            }
            retCell=cell;
            
        }else {//已经存在的朋友
            QMSwipeContactCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMSwipeContactCellIdentifier];
            if(nil == cell) {
                cell = [QMSwipeContactCell cellForSwipeContact];
            }
            cell.delegate = self;
            cell.contactDelegate = self;
            
            FriendSectionInfo * sectionInfo = [_friendSectionList objectAtIndex:indexPath.section - 1];
            FriendInfo * contactInfo = [_friendList objectAtIndex:sectionInfo.pos + indexPath.row];
            [cell updateUI:contactInfo];
            
            retCell = cell;
        }

        retCell.contentView.backgroundColor = UIColorFromRGB(0xfafafa);
    }
    else{
        if (indexPath.row<[_arrSearchFriend count]) {
            
            QMSwipeContactCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMSwipeContactCellIdentifier];
            if(nil == cell) {
                cell = [QMSwipeContactCell cellForSwipeContact];
            }
            cell.delegate = self;
            cell.contactDelegate = self;
            
            FriendInfo * contactInfo = [_arrSearchFriend objectAtIndex:indexPath.row];
            [cell updateUIWithFriend:contactInfo andSearchString:strSearch];
            
            retCell = cell;

        }
        else if([_arrSearchContact count]>0){
            QMInviteContactCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMInviteContactCellIdentifier];
            if(nil == cell) {
                cell = [QMInviteContactCell cellForInviteContact];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
            
            ContactInfo * contactInfo = [_arrSearchContact objectAtIndex:indexPath.row-[_arrSearchFriend count]];
            [cell updateUIWithContact:contactInfo andSearchInfo:strSearch];
            retCell=cell;
        }
        
    }
    return retCell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (tableView==self.tableView) {
        [_sectionIndexForTitle removeAllObjects];
        //[_sectionIndexForTitle addObject:UITableViewIndexSearch];
        for (FriendSectionInfo* uiSection in _friendSectionList) {
            [_sectionIndexForTitle addObject:uiSection.letter];
        }
        [_sectionIndexForTitle addObject:@"邀"];//add by zzd
        return _sectionIndexForTitle;
    }
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView==self.tableView) {
        if([_sectionIndexForTitle count] >= section && section != 0) {
            return [_sectionIndexForTitle objectAtIndex:section -1];
        }
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (tableView==self.tableView) {
        return index + 1;
    }
    return 0;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==self.tableView) {
        if(0 == section) {
            return CGFLOAT_MIN;
        } else {
            return 22;
        }
    }else{
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==self.tableView) {
        return 55;
    }
    return 55;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==self.tableView) {
        if([_sectionIndexForTitle count] > section && section != 0) {
            UIImageView* imgView = [[UIImageView alloc] initWithFrame:tableView.bounds];
            imgView.backgroundColor = UIColorFromRGB(0xededed);
            
            UILabel* textLable = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 30, 21)];
            textLable.backgroundColor = [UIColor clearColor];
            textLable.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
            textLable.textColor = UIColorFromRGB(0xa9afb8);
            textLable.text = [_sectionIndexForTitle objectAtIndex:section - 1];
            [imgView addSubview:textLable];
            
            return imgView;
        }else if([_sectionIndexForTitle count] == section){
            UIImageView* imgView = [[UIImageView alloc] initWithFrame:tableView.bounds];
            imgView.backgroundColor = UIColorFromRGB(0xededed);
            UILabel* textLable = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, kMainFrameWidth-2*8, 21)];
            textLable.textAlignment=NSTextAlignmentCenter;
            textLable.backgroundColor = [UIColor clearColor];
            textLable.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
            textLable.textColor = UIColorFromRGB(0xa9afb8);
            //        textLable.text = [_sectionIndexForTitle objectAtIndex:section-1];
            textLable.text=@"邀请通讯录好友";
            [imgView addSubview:textLable];
            
            return imgView;
        }
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView==self.tableView) {
        if(0 == indexPath.section && 0 == indexPath.row) {
            QMNewFriendViewController * viewController = [[QMNewFriendViewController alloc] initWithNibName:@"QMNewFriendViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if(0 == indexPath.section && 1 == indexPath.row) {
    
            QMProbablyKnowViewController *probably=[[QMProbablyKnowViewController alloc] init];
            [self.navigationController pushViewController:probably animated:YES];
 
        }
        else {
            if (indexPath.section-1 < [_friendSectionList count]) {
                _selectIndexPath = indexPath;
                FriendSectionInfo * sectionInfo = [_friendSectionList objectAtIndex:indexPath.section - 1];
                FriendInfo * contactInfo = [_friendList objectAtIndex:sectionInfo.pos + indexPath.row];
                PersonPageViewController *personController = [[PersonPageViewController alloc]initWithUserId:contactInfo.uid];
                [self.navigationController pushViewController:personController animated:YES];
            }
        }
        
        if(_swipeIndexPath) {
            [self resetSelectedCell];
        }
    }else{
        if (indexPath.row<[_arrSearchFriend count]) {
            [self.searchDisplayController setActive:NO];
            FriendInfo * contactInfo = [_arrSearchFriend objectAtIndex:indexPath.row];
            PersonPageViewController *personController = [[PersonPageViewController alloc]initWithUserId:contactInfo.uid];
            [self.navigationController pushViewController:personController animated:YES];
        }
    }
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
//    NSLog(@"searchResultFriend=%@",_arrSearchFriend);
//    NSLog(@"searchResultContact=%@",_arrSearchContact);
}


#pragma mark -UISearchDisplayDelegate delegate methods

-(void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller{
    [_arrSearchFriend removeAllObjects];
    [_arrSearchContact removeAllObjects];
    
    self.searchDisplayController.searchBar.showsCancelButton = YES;
    UIButton *cancelButton;
    UIView *topView = self.searchDisplayController.searchBar.subviews[0];
    for (UIView *subView in topView.subviews) {
        if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
            cancelButton = (UIButton*)subView;
        }
    }
    if (cancelButton) {
        //Set the new title of the cancel button
        [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    }
}



- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString NS_DEPRECATED_IOS(3_0,8_0){
    [_arrSearchFriend removeAllObjects];
    [_arrSearchContact removeAllObjects];
    strSearch=searchString;
    
    for(FriendInfo * contactInfo in _friendList){
        if([contactInfo.nickname rangeOfString:searchString].location !=NSNotFound || [contactInfo.remark rangeOfString:searchString].location !=NSNotFound){
            [_arrSearchFriend addObject:contactInfo];
        }
    }
    
    for(ContactInfo * contactInfo in _inviteList){
        if(contactInfo.personName && [contactInfo.personName rangeOfString:searchString].location!=NSNotFound){
            [_arrSearchContact addObject:contactInfo];
        }
    }
    
    return YES;
}


-(void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView
{
    [tableView setContentInset:UIEdgeInsetsZero];
    [tableView setScrollIndicatorInsets:UIEdgeInsetsZero];
}

#pragma mark - RMSwipeTableViewCell delegate methods

-(void)swipeTableViewCellDidStartSwiping:(RMSwipeTableViewCell *)swipeTableViewCell {
    NSIndexPath *indexPathForCell = [self.tableView indexPathForCell:swipeTableViewCell];
    if(indexPathForCell.section != _swipeIndexPath.section) {
        [self resetSelectedCell];
    } else {
        if (_swipeIndexPath.row != indexPathForCell.row) {
            [self resetSelectedCell];
        }
    }
}

-(void)resetSelectedCell {
    QMSwipeContactCell *cell = (QMSwipeContactCell*)[self.tableView cellForRowAtIndexPath:_swipeIndexPath];
    if(cell) {
        [cell resetContentView];
        _swipeIndexPath = nil;
    }
    
//    cell.selectionStyle = UITableViewCellSelectionStyleGray;
}

-(void)swipeTableViewCellWillResetState:(RMSwipeTableViewCell *)swipeTableViewCell fromPoint:(CGPoint)point animation:(RMSwipeTableViewCellAnimationType)animation velocity:(CGPoint)velocity {
    if (velocity.x <= -500) {
        _swipeIndexPath = [self.tableView indexPathForCell:swipeTableViewCell];
        swipeTableViewCell.shouldAnimateCellReset = NO;
        swipeTableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSTimeInterval duration = MAX(-point.x / ABS(velocity.x), 0.10f);
        [UIView animateWithDuration:duration
                              delay:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             swipeTableViewCell.contentView.frame = CGRectOffset(swipeTableViewCell.contentView.bounds, point.x - (ABS(velocity.x) / 150), 0);
                         }
                         completion:^(BOOL finished) {
                             [UIView animateWithDuration:duration
                                                   delay:0
                                                 options:UIViewAnimationOptionCurveEaseOut
                                              animations:^{
                                                  swipeTableViewCell.contentView.frame = CGRectOffset(swipeTableViewCell.contentView.bounds, -80, 0); //80*2
                                              }
                                              completion:^(BOOL finished) {
                                              }];
                         }];
    }
    else if (velocity.x > -500 && point.x < -80) {
        //self.selectedIndexPath = [self.tableView indexPathForCell:swipeTableViewCell];
        swipeTableViewCell.shouldAnimateCellReset = NO;
        swipeTableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSTimeInterval duration = MIN(-point.x / ABS(velocity.x), 0.15f);
        [UIView animateWithDuration:duration
                         animations:^{
                             swipeTableViewCell.contentView.frame = CGRectOffset(swipeTableViewCell.contentView.bounds, -80, 0); //80*2
                         }];
    } else {
        swipeTableViewCell.shouldAnimateCellReset = YES;
    }
}

#pragma mark - 

- (void)didRemarkBtnClick:(QMSwipeContactCell *)cell
{
    [self resetSelectedCell];

    if(cell) {
        _selectIndexPath = [_tableView indexPathForCell:cell];
        //传的是引用，会导致直接修改其值
        QMRemarkViewController * viewController = [[QMRemarkViewController alloc] initWithFriendInfo:[cell getFriendInfo]];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}




// inventFriend
//add by zhaozd

#pragma mark inventFrined

- (void)dealloc
{
    
    [Friendhandle freeFriendHandle];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self saveInvitePhoneList];
}


- (void)getContactList
{
    __weak UITableView * weakTableView = self.tableView;
    
    [[Friendhandle shareFriendHandle] loadContact:^(id result) {
        
        [AppUtils dismissHUD];
        
        NSArray * array = result;
        for (ContactInfo * info in array) {//ContactInfo为当前通讯录中得好友
            BOOL bFound = NO;
            for(NSString * cellString in info.phoneList) {//比较本地通讯录和朋友列表，如果不在朋友列表中则添加到邀请列表
                if([_friendListPhone containsObject:cellString]) {
                    bFound = YES;
                    break;
                }
            }
            
            if(!bFound) {
                [_inviteList addObject:info];
            }
        }
        
        [weakTableView reloadData];
        
        if (_inviteList.count > 0) {
            self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(inviteAllClick:) title:@"全部邀请" size:CGSizeMake(80, 44) imageName:nil];
        }
        
        if (self.slideToInvite && _inviteList.count > 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:[_friendSectionList count]+1];
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
    }];
    
}

//全部邀请
- (void)inviteAllClick:(id)sender
{
    [self inviteAllFriend ];
}

- (void)LoadInvitedPhoneList//加载已邀请列表
{
    [_invitedPhoneList removeAllObjects];
    
    _invitedPhoneList = [[NSMutableArray alloc] init];
    
    NSString * strDoc = [AppUtils stringForUserDocumentPath];
    NSString * fileName = [NSString stringWithFormat:@"%@_invited", [UserInfoHandler sharedInstance].currentUser.uid];
    NSString * filePath = [strDoc stringByAppendingPathComponent:fileName];
    NSString * fileContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSArray * array = [fileContent componentsSeparatedByString:@","];
    if([array count] > 0) {
        [_invitedPhoneList addObjectsFromArray:array];
    }
}

- (void)saveInvitePhoneList//保存已邀请列表
{
    if([_invitedPhoneList count] > 0) {
        NSString * strDoc = [AppUtils stringForUserDocumentPath];
        NSString * fileName = [NSString stringWithFormat:@"%@_invited", [UserInfoHandler sharedInstance].currentUser.uid];
        NSString * filePath = [strDoc stringByAppendingPathComponent:fileName];
        NSString * content = [_invitedPhoneList componentsJoinedByString:@","];
        [content writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
}


#pragma mark - QMInviteContactDelegate

- (void)didInviteContactClick:(ContactInfo *)info
{
    if ([info.personName isEqualToString:@"来自通讯录的好友"]) {//邀请全部好友
        [self inviteAllFriend];
    }else{
        if(info && [info.phoneList count] > 0) {
            BOOL bFound = NO;
            if([_invitedPhoneList count] > 0) {
                for (NSString * cellphone in info.phoneList) {
                    if([_invitedPhoneList containsObject:cellphone]) {
                        bFound = YES;
                        break;
                    }
                }
            }
            _tmpInviteString = @"";
            _tmpInviteString = [info.phoneList componentsJoinedByString:@","];
            if(bFound) {
                UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:nil message:@"已邀请过TA，确定需要再次短信邀请么？" delegate:self cancelButtonTitle:[AppUtils localizedCommonString:@"QM_Button_Cancel"] otherButtonTitles:[AppUtils localizedCommonString:@"QM_Button_OK"], nil];
                alertView.tag = 1000;
                [alertView show];
            } else {
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:[AppUtils localizedPersonString:@"QM_Alert_AskForInvite"] message:nil delegate:self cancelButtonTitle:[AppUtils localizedCommonString:@"QM_Button_Cancel"] otherButtonTitles:[AppUtils localizedCommonString:@"QM_Button_OK"], nil];
                alertView.tag = 999;
                [alertView show];
            }
        }
    }
}

- (void)inviteFriendsImp:(NSString *)list resend:(BOOL)resend inviteAll:(BOOL)bAll
{
    __weak NSMutableArray * weakInvitedPhoneList = _invitedPhoneList;
    
    [[Friendhandle shareFriendHandle] inviteFriend:list isInviteAll:bAll success:^(id obj) {
        [AppUtils showSuccessMessage:@"邀请已发送"];
        if(!resend && !bAll) {
            [weakInvitedPhoneList addObject:list];
        } else {
            if(bAll) {
                [weakInvitedPhoneList removeAllObjects];
                [weakInvitedPhoneList addObject:list];
            }
        }
        _tmpInviteString = @"";
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        if(!resend && !bAll) {
            [weakInvitedPhoneList addObject:list];
        } else {
            if(bAll) {
                [weakInvitedPhoneList removeAllObjects];
                [weakInvitedPhoneList addObject:list];
            }
        }
        _tmpInviteString = @"";
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex == buttonIndex) {
        return;
    }
    if (alertView.tag == 999) {//邀请
        [self inviteFriendsImp:_tmpInviteString resend:NO inviteAll:NO];
    }
    else if (alertView.tag == 1000){ //再次邀请
        [self inviteFriendsImp:_tmpInviteString resend:YES inviteAll:NO];
    }else if(alertView.tag == 1) {//邀请全部好友
        if(buttonIndex == 1) {
            NSMutableArray * array = [[NSMutableArray alloc] init];
            for(ContactInfo * info in _inviteList) {
                NSString * phone = [info.phoneList componentsJoinedByString:@","];
                [array addObject:phone];
            }
            NSString * phone = [array componentsJoinedByString:@","];
            [self inviteFriendsImp:phone resend:NO inviteAll:YES];
        }
    }
}

- (void)inviteAllFriend
{
    if([_inviteList count] > 0) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:nil message:@"您确定要邀请所有好友吗？" delegate:self cancelButtonTitle:nil otherButtonTitles:@"取消", @"确定", nil];
        alertView.tag = 1;
        [alertView show];
    }
}


@end
