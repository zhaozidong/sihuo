//
//  QMChatUserHelper.h
//  QMMM
//
//  Created by hanlu on 14-10-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserBasicInfo;
@class FMDatabase;

@interface QMChatUserHelper : NSObject {
    NSThread * _workThread;
}

//数据库工作线程
- (void)setWorkThread:(NSThread *)aThread;

- (NSArray *)loadChatUserList:(FMDatabase *)database;
- (void)addChatUserList:(FMDatabase *)database list:(NSArray *)userList;
- (void)updateChatUser:(FMDatabase *)database userInfo:(UserBasicInfo *)userInfo;
- (void)deleteChatUser:(FMDatabase *)database userId:(uint64_t)userId;

- (void)asyncLoadChatUserList:(contextBlock)context;
- (void)asyncAddChatUserList:(contextBlock)context list:(NSArray *)userList;
- (void)asyncUpdateChatUser:(contextBlock)context userInfo:(UserBasicInfo *)userInfo;
- (void)asyncDeleteChatUser:(contextBlock)context userId:(uint64_t)userId;

@end
