//
//  QMExpressCollectionViewCell.m
//  QMMM
//
//  Created by hanlu on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMExpressCollectionViewCell.h"
#import "UIButton+Addition.h"

NSString * const kQMExpressCollectionViewCellIdentifier = @"kQMExpressCollectionViewCellIdentifier";

@implementation QMExpressCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frameRect
{
    self = [super initWithFrame:frameRect];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        _bkgImageView = [[UIImageView alloc] initWithFrame:frameRect];
         _bkgImageView.layer.borderColor = UIColorFromRGB(0xd8dee6).CGColor;
         _bkgImageView.layer.borderWidth = 1;
        _bkgImageView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_bkgImageView];
        
        _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"express_1.png"]];
        [self addSubview:_imageView];
        
        _title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 14)];
        [_title setTextAlignment:NSTextAlignmentLeft];
        [_title setTextColor:UIColorFromRGB(0x4f4f4f)];
        [_title setFont:[UIFont systemFontOfSize:12.0]];
        [self addSubview:_title];

        _flagImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"express_quick.png"]];
        _flagImageView.backgroundColor = [UIColor clearColor];
        _flagImageView.hidden = YES;
        [self addSubview:_flagImageView];

        _selectImageView = [[UIImageView alloc] initWithFrame:_flagImageView.bounds];
        _selectImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:_selectImageView];

        _expressPhoneNum = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70, 12)];
        [_expressPhoneNum setTextAlignment:NSTextAlignmentLeft];
        [_expressPhoneNum setTextColor:UIColorFromRGB(0x4f4f4f)];
        [_expressPhoneNum setFont:[UIFont systemFontOfSize:10.0]];
        [self addSubview:_expressPhoneNum];
        
        _sepratorLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 4, 1, frameRect.size.height - 4 * 2)];
        _sepratorLine.backgroundColor = UIColorFromRGB(0xd8dee6);
        [_sepratorLine setText:@""];
        [self addSubview:_sepratorLine];
        
        _callBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, frameRect.size.height)];
        [_callBtn setImage:[UIImage imageNamed:@"call"] forState:UIControlStateNormal];
        [_callBtn addTarget:self action:@selector(callBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_callBtn];
    }
    return self;
}

- (void)updateUI:(NSDictionary *)dict
{
    //_infoDict = dict;
    
    id temp = [dict objectForKey:@"icon"];
    if(temp && [temp isKindOfClass:[NSString class]]) {
        [_imageView setImage:[UIImage imageNamed:temp]];
    }
    
    temp = [dict objectForKey:@"desc"];
    if(temp && [temp isKindOfClass:[NSString class]]) {
        [_title setText:temp];
    }
    
    temp = [dict objectForKey:@"flag"];
    if(temp && [temp isKindOfClass:[NSString class]] && [temp length] > 0) {
        [_flagImageView setImage:[UIImage imageNamed:temp]];
        [_flagImageView setHidden:NO];
    } else {
        [_flagImageView setHidden:YES];
    }
    
    temp = [dict objectForKey:@"phoneNum"];
    if(temp && [temp isKindOfClass:[NSString class]]) {
        [_expressPhoneNum setText:temp];
    }
    
    _phoneNum = _expressPhoneNum.text;
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    if(selected) {
        _bkgImageView.layer.borderColor = (kRedColor).CGColor;
        [_selectImageView setImage:[UIImage imageNamed:@"express_select"]];
    } else {
         _bkgImageView.layer.borderColor = UIColorFromRGB(0xd8dee6).CGColor;
         [_selectImageView setImage:nil];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat leftMargin = 37;
    CGFloat topMargin = 9;
    
    CGRect frame = self.bounds;
    _bkgImageView.frame = frame;
    
    frame = _imageView.frame;
    frame.origin.x = self.bounds.origin.x + (leftMargin - _imageView.frame.size.width) / 2;
    frame.origin.y = self.bounds.origin.y + (self.bounds.size.height - _imageView.bounds.size.height) / 2;
    _imageView.frame = frame;
    
    frame = _title.frame;
    frame.origin.x = leftMargin;
    frame.origin.y = topMargin;
    _title.frame = frame;
    
    frame = _expressPhoneNum.frame;
    frame.origin.x = _title.frame.origin.x;
    frame.origin.y = _title.frame.origin.y + _title.frame.size.height + 5;
    _expressPhoneNum.frame = frame;
    
    frame = _callBtn.frame;
    frame.origin.x = self.bounds.size.width - _callBtn.frame.size.width;
    _callBtn.frame = frame;
    
    frame = _sepratorLine.frame;
    frame.origin.x = _callBtn.frame.origin.x - 1;
    _sepratorLine.frame = frame;
    
    frame = _flagImageView.frame;
    frame.origin.x = self.bounds.origin.x + self.bounds.size.width - _flagImageView.bounds.size.width;
    frame.origin.y = self.bounds.origin.y;
    _flagImageView.frame = frame;
    _selectImageView.frame = frame;
    
}

#pragma mark -

- (void)callBtnClick:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(didExpressCallBtnClick:)] && [_phoneNum length] > 0) {
        [_delegate didExpressCallBtnClick:_phoneNum];
    }
}

@end
