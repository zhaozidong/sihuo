//
//  QMGoodsViewController.m
//  QMMM
//
//  Created by 韩芦 on 14-9-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "QMGoodsViewController.h"
#import "GoodsHandler.h"
#import "MyGoodsList.h"
#import "GoodEntity.h"
#import "QMGoodsFootView.h"
#import "QMSegmentedControl.h"
#import "QMGoodsController.h"
#import "GoodsDetailViewController.h"
#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import "UserDefaultsUtils.h"
#import "QMShareViewContrller.h"
#import "PersonPageViewController.h"
#import "MJRefresh.h"
#import "MJRefreshConst.h"
#import "QMFriendViewController.h"
#import "QAudioPlayer.h"
#import "QMGoodsFirstPlaceHolder.h"
#import "QMOpenContactViewController.h"
#import "QMAuthorizationGuide.h"
#import <AddressBook/AddressBook.h>
#import "MLKMenuPopover.h"
#import "QMFriendViewController.h"
#import "QMProbablyKnowViewController.h"
#import "UserRightsManager.h"
#import "ContactHandle.h"
#import "QMNoGoodsPlaceHolderView.h"
#import "ActivityDetailViewController.h"
#import "APIConfig.h"
#import "UIScrollView+UzysCircularProgressPullToRefresh.h"

@interface QMGoodsViewController ()<QMGoodsControllerDelegate,QMGoodsFirstPlaceHoderDelegate,MLKMenuPopoverDelegate,UIAlertViewDelegate,QMNoGoodsPlaceHolderDelegate> {
    NSMutableArray * _goodsList;
    NSMutableDictionary * _goodslistDict;
    BOOL _needCheckGetNextPage;
    QMShareViewContrller * _shareViewController;
    GoodsListType _listType;
    UILabel *lblUpdateMessage;
    
    CGFloat startContentOffset; //用于判断上下滑
    CGFloat lastContentOffset;
    BOOL hidden;
    BOOL hidePrompt;
//    NSTimeInterval lastInterval;
    QMGoodsFirstPlaceHolder *_placeHolder;
    NSUInteger scrollFlag;
    
    QMNoGoodsPlaceHolderView *_noGoodsPlaceHolder;

}

@property (nonatomic, strong) UIView *statusbarBgView; //在滑动时占位用，填充状态栏的颜色

@property (nonatomic, strong) UIColor *navBarBackgroundColor;  //导航栏的颜色

@property(nonatomic,strong) MLKMenuPopover *menuPopover;

@end

@implementation QMGoodsViewController

- (id)initWithListType:(GoodsListType)listType
{
    self = [super initWithNibName:@"QMGoodsViewController" bundle:nil];
    if(self) {
        _listType = listType;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UIView *)statusbarBgView
{
    if (!_statusbarBgView) {
        _statusbarBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, 20)];
        _statusbarBgView.backgroundColor = [UIColorFromRGB(0xff3232) colorWithAlphaComponent:0.9];
    }
    return _statusbarBgView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"朋友";
    self.view.frame = CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight);
    _tableView.frame = self.view.frame;
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    self.navigationItem.rightBarButtonItems=[AppUtils createRightButtonWithTarget:self selector:@selector(btnDidRightItemClick:) title:nil size:CGSizeMake(40, 40) imageName:@"navAddFriend"];
    
    [self registerObserver];
    
    
    //初始化tableView
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.allowsSelection = YES;
    _tableView.separatorColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.scrollsToTop=YES;
    

    UIImageView *bkgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_tableView.frame), CGRectGetHeight(_tableView.frame))];
    bkgImageView.contentMode = UIViewContentModeScaleAspectFill;
    UIImage *bkgImage = [UIImage imageNamed:@"goods_bkg"];
    bkgImageView.image = bkgImage;
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setBackgroundView:bkgImageView];

    
//    [self.view addSubview:self.statusbarBgView];
    
    ((UIScrollView*)_tableView).delegate = self;
    
    //记录下开始时的导航栏颜色
    self.navBarBackgroundColor = self.navigationController.view.backgroundColor;
    
    if (self.rdv_tabBarController.tabBar.translucent) {
        UIEdgeInsets insets = UIEdgeInsetsMake(64,
                                               0,
                                               CGRectGetHeight(self.rdv_tabBarController.tabBar.frame),
                                               0);
        
        self.tableView.contentInset = insets;
        self.tableView.scrollIndicatorInsets = insets;
    }
    
    [self setupRefresh];

    [[GoodsHandler shareGoodsHandler].friendGoodsList loadNextPage:YES];

}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    hidden=NO;
    if (self.navigationController.viewControllers.count < 2) {
        //是当前界面
        [[self rdv_tabBarController] setTabBarHidden:NO animated:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    scrollFlag=0;
    [[QAudioPlayer sharedInstance] stopPlay];
    if (self.navigationController.viewControllers.count > 1) {
        //是当前界面
        [[self rdv_tabBarController] setTabBarHidden:YES animated:YES];
    }
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //防止在个人主页返回时，导航栏的颜色消失
    scrollFlag=2;
    [self.navigationController.navigationBar setBackgroundImage:nil
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = nil;
    self.navigationController.view.backgroundColor = self.navBarBackgroundColor;
    
    if (![UserDefaultsUtils boolValueWithKey:@"showContact"]) {
        [UserDefaultsUtils saveBoolValue:YES withKey:@"showContact"];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"私货想访问您的通讯录" message:@"我们需要访问通讯录，帮你找到正在私货上卖闲置的好友。通讯录仅用作好友匹配，不作他用" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        [[UserRightsManager sharedInstance] getContactRights:^(QMRightType type) {
            if (type==QMRightTypeAuthorized) {
                [[ContactHandle shareContactInstance] uploadContactList];
            }
        } failure:^{
            DLog(@"获取通讯录访问权限失败！");
        }];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [[UserRightsManager sharedInstance] getContactRights:^(QMRightType type) {
        [[ContactHandle shareContactInstance] uploadContactList];
    } failure:^{
        DLog(@"获取通讯录访问权限失败！");
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)btnDidRightItemClick:(id)sender{
    [_tableView setContentOffset:_tableView.contentOffset animated:NO];

    if (nil == self.menuPopover) {
        NSArray *menuItems=[NSArray arrayWithObjects:@"邀请朋友", @"可能认识的人", nil];
        NSArray *arrImages=[NSArray arrayWithObjects:@"menu_friend",@"menu_people",nil];
        self.menuPopover = [[MLKMenuPopover alloc] initWithFrame:CGRectMake(kMainFrameWidth-140-5, kMainTopHeight+5, 140, 88) menuItems:menuItems imgItems:arrImages];
        self.menuPopover.menuPopoverDelegate = self;
    }

    [self.menuPopover showInView:self.view];
}


- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex{
    if (0==selectedIndex) {//邀请朋友
//        QMFriendViewController *friend=[[QMFriendViewController alloc] initWithNibName:@"QMFriendViewController" bundle:nil];
//        [self.navigationController pushViewController:friend animated:YES];
        
        QMFriendViewController * viewController = [[QMFriendViewController alloc] initWithNibName:@"QMFriendViewController" bundle:nil];
        viewController.slideToInvite = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }else if(1==selectedIndex){//可能认识的人
        QMProbablyKnowViewController *probably=[[QMProbablyKnowViewController alloc] init];
        [self.navigationController pushViewController:probably animated:YES];
    }
}


#pragma mark - RegisterObserver

- (void)registerObserver
{
    NSNotificationCenter * nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(processGoodsListLoadedNotify:) name:kGoodsListLoadedNotify object:nil];
    [nc addObserver:self selector:@selector(processGoodsListLoadStateNotify:) name:kGoodsListLoadStateNotify object:nil];
    [nc addObserver:self selector:@selector(processGoodsUpdateFavorNotify:) name:kGoodsUpdateFavorNotify object:nil];
    [nc addObserver:self selector:@selector(processNetworkErrorNotify:) name:kNotNetworkErrorNotity object:nil];
    [nc addObserver:self selector:@selector(processGoodsUpdateCommentNotify:) name:kAddGoodsCommentNotify object:nil];
    [nc addObserver:self selector:@selector(processDeleteGoodsNotify:) name:kDeleteGoodsNotify object:nil];
    [nc addObserver:self selector:@selector(processGoodsNumUpdateNotify:) name:kServerGoodsNumUpdateNotify object:nil];// add by zhaozd
    [nc addObserver:self selector:@selector(processScrollToTopNotify:) name:kScrollToTopNotify object:nil];

    [nc addObserver:self selector:@selector(processPublishGoodsNotify:) name:kPublishGoodsOKNotify object:nil];
}

#pragma mark - 

- (void)reloadGoodsList
{
    NSMutableArray * tmpArray = [[NSMutableArray alloc] init];
    NSMutableDictionary * tmpDict = [[NSMutableDictionary alloc] init];
    
    // 先清空所有delegate
    for (int i = 0; i < _goodsList.count; i++) {
        QMGoodsController *goodsController = [_goodsList objectAtIndex:i];
        goodsController.delegate = nil;
    }
    
    //更新数据源
    MyGoodsList *myGoodsList = [GoodsHandler shareGoodsHandler].friendGoodsList;

    @synchronized(myGoodsList.goodsList) {
        for (int i = 0; i < [myGoodsList.goodsList count]; i++) {
            GoodEntity * goodsInfo = [myGoodsList.goodsList objectAtIndex:i];
            
            QMGoodsController * controller = [_goodslistDict objectForKey:@(goodsInfo.gid)];
            if(nil == controller) {
                controller = [[QMGoodsController alloc] initWithGoodsInfo:goodsInfo];
            } else {
                [controller setGoodsInfo:goodsInfo];
            }
            controller.delegate = self;
            
            [tmpArray addObject:controller];
            [tmpDict setObject:controller forKey:@(goodsInfo.gid)];
        }
    }
    _goodsList = tmpArray;
    _goodslistDict = tmpDict;
    
    
//    if (_goodsList.count<=0) {
//        if (_listType==EGLT_FRIEND) { //好友商品列表
//            if ([[UserDefaultsUtils valueWithKey:@"contactAuthorization"] isEqualToString:@"allow"]) {//允许访问
//                _placeHolder=[[QMGoodsFirstPlaceHolder alloc] initWithType:QMGoodsListPlaceHolderTypeNoGoods andTitle:nil];
//            }else{
//                _placeHolder=[[QMGoodsFirstPlaceHolder alloc] initWithType:QMGoodsListPlaceHolderTypeCloseContact andTitle:@"开启通讯录访问权限"];
//            }
//            _placeHolder.frame=CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight-kMainTopHeight-44);
//            _placeHolder.delegate=self;
//            [_tableView addSubview:_placeHolder];
//        }
//    }else{
//        if(_listType==EGLT_FRIEND){
//            if (_placeHolder) {
//                [_placeHolder removeFromSuperview];
//            }
//            if (_tableView.backgroundView == nil) {
//                [_tableView setBackgroundColor:[UIColor clearColor]];
//                UIImageView *bkgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_tableView.frame), CGRectGetHeight(_tableView.frame))];
//                bkgImageView.contentMode = UIViewContentModeScaleAspectFill;
//                UIImage *bkgImage = [UIImage imageNamed:@"goods_bkg"];
//                bkgImageView.image = bkgImage;
//                [_tableView setBackgroundView:bkgImageView];
//            }
//        }
//    }
    
    
    if (_goodsList.count<=0) {
        
//        [self showPlaceHolderWithType:QMGoodsListPlaceHolderTypeInvite];
        
        [self showNoGoodsPlachHolder];
        
//        __weak typeof(self) weakSelf=self;
//        [[UserRightsManager sharedInstance] getContactRights:^(QMRightType type) {
//            if (type==QMRightTypeAuthorized) {//允许访问
//                [weakSelf showPlaceHolderWithType:QMGoodsListPlaceHolderTypeNoGoods];
//            }else if(type==QMRightTypeDenied){//不允许访问
//                [weakSelf showPlaceHolderWithType:QMGoodsListPlaceHolderTypeCloseContact];
//            }
//        } failure:^{
//            DLog(@"获取通讯录访问权限失败！");
//        }];
    }else{
        if (_placeHolder) {
            [_placeHolder removeFromSuperview];
            _placeHolder=nil;
        }
        if (_noGoodsPlaceHolder) {
            [_noGoodsPlaceHolder removeFromSuperview];
            _noGoodsPlaceHolder=nil;
        }
        if (_tableView.backgroundView == nil) {
            [_tableView setBackgroundColor:[UIColor clearColor]];
            UIImageView *bkgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_tableView.frame), CGRectGetHeight(_tableView.frame))];
            bkgImageView.contentMode = UIViewContentModeScaleAspectFill;
            UIImage *bkgImage = [UIImage imageNamed:@"goods_bkg"];
            bkgImageView.image = bkgImage;
            [_tableView setBackgroundView:bkgImageView];
        }
        //刷新
        [_tableView reloadData];
    }
}



-(void)showPlaceHolderWithType:(QMGoodsListPlaceHolderType)type{
    if (_placeHolder && !_placeHolder.hidden) {
        return;
    }
    if (type==QMGoodsListPlaceHolderTypeNoGoods) {
        _placeHolder=[[QMGoodsFirstPlaceHolder alloc] initWithType:QMGoodsListPlaceHolderTypeNoGoods andTitle:nil];
    }else if (type==QMGoodsListPlaceHolderTypeCloseContact){
        _placeHolder=[[QMGoodsFirstPlaceHolder alloc] initWithType:QMGoodsListPlaceHolderTypeCloseContact andTitle:@"开启通讯录访问权限"];
    }else if (type==QMGoodsListPlaceHolderTypeInvite){
        _placeHolder=[[QMGoodsFirstPlaceHolder alloc] initWithType:QMGoodsListPlaceHolderTypeInvite andTitle:nil];
    }
    _placeHolder.frame=CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight-kMainTopHeight-44);
    _placeHolder.delegate=self;
    [_tableView addSubview:_placeHolder];
}

-(void)showNoGoodsPlachHolder{
    if (_noGoodsPlaceHolder && !_noGoodsPlaceHolder.hidden) {
        return;
    }
    _noGoodsPlaceHolder=[[QMNoGoodsPlaceHolderView alloc] init];
    _noGoodsPlaceHolder.frame=CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight-kMainTopHeight-44);
    _noGoodsPlaceHolder.delegate=self;
    [_tableView addSubview:_noGoodsPlaceHolder];
}


-(void)btnDidClickNoGoods:(id)sender{
    UIButton *button=(UIButton *)sender;
    NSString *requestURL = @"";
    if (button.tag==1) {//去接好友
        requestURL = [BaseHandler requestUrlWithPath:API_HONGBAO_INVITE];
        [AppUtils trackCustomEvent:@"click_toInviteFriends_event" args:nil];
    }else{//兑换礼品
        requestURL = [BaseHandler requestUrlWithPath:API_HONGBAO_EXCHANGE_LIST];
    }
    ActivityDetailViewController *controller = [[ActivityDetailViewController alloc]initWithUrl:requestURL];
    [self.navigationController pushViewController:controller animated:YES];
}


-(void)btnDidInvite{
    [AppUtils   trackCustomEvent:@"inviteNow" args:nil];
    QMFriendViewController *friend=[[QMFriendViewController alloc] initWithNibName:@"QMFriendViewController" bundle:nil];
    friend.slideToInvite=YES;
    [self.navigationController pushViewController:friend animated:YES];
}



-(void)btnDidOpenContact:(UIButton *)sender{//点击开启通讯录
    if (IS_IOS8) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }else{
        QMOpenContactViewController *openContact=[[QMOpenContactViewController alloc] init];
        [self.navigationController pushViewController:openContact animated:YES];
    }
}


- (void)refreshStateViewUI
{
    MyGoodsList * myGoodsList = nil;
    if(_listType == EGLT_RECOMMEND) {
        myGoodsList = [GoodsHandler shareGoodsHandler].myGoodsList;
    } else {
        myGoodsList = [GoodsHandler shareGoodsHandler].friendGoodsList;
    }
    
    if (myGoodsList.serverFirstPageLoading) {
        if (NO == [_tableView showPullToRefresh]) {
            [_tableView triggerPullToRefresh];
        }
    } else {
        [self stopHeadRefresh];
    }
    
    if (myGoodsList.loading && !myGoodsList.serverFirstPageLoading) {
//        if (![self.tableView.footer isRefreshing]) {
//            [self.tableView.footer beginRefreshing];
//        }
    }
    else{
        if ([self.tableView.footer isRefreshing]) {
            [self.tableView.footer endRefreshing];
        }
    }

////////////////////// modify by zn /////////////
    // 计算底部加载状态栏可见状态
//    NSArray * array = [_tableView indexPathsForVisibleRows];
//    if(array && [array count] < [_goodsList count]) {
//        if (myGoodsList.loading && !myGoodsList.serverFirstPageLoading) {
//            _footView.hidden = NO;
//            _tableView.tableFooterView = _footView;
//            [_footView.indicatorView startAnimating];
//        }
//        else {
//            _needCheckGetNextPage = !myGoodsList.endOfAll;
//            _tableView.tableFooterView = nil;
//            _footView.hidden = YES;
//            [_footView.indicatorView stopAnimating];
//        }
//    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_goodsList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([_goodsList count] > 0) {
        QMGoodsController * controller = [_goodsList objectAtIndex:section];
        return [controller tableView:tableView numberOfRowsInSection:section];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = nil;
    
    QMGoodsController * controller = [_goodsList objectAtIndex:indexPath.section];
    cell = [controller tableView:tableView cellForRowAtIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

#pragma mark - UITableViewDeletegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMGoodsController * controller = [_goodsList objectAtIndex:indexPath.section];
    return [controller tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 8.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == ([_goodsList count]-1)) {
        return 8.f;
    }
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMGoodsController * controller = [_goodsList objectAtIndex:indexPath.section];
    [controller tableView:tableView didSelectRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2) {
        cell.backgroundColor = [UIColor clearColor];
    }
}

////////////////////// modify by zn /////////////
//#pragma mark - UIScrollViewDelegate
//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    if (_needCheckGetNextPage) {
//        CGFloat contentOffsetPointY = _tableView.contentOffset.y - _tableView.contentInset.bottom;
//        CGSize contentSize = _tableView.contentSize;
//        CGRect frame = _tableView.frame;
//        
//        if (contentOffsetPointY > contentSize.height - frame.size.height || contentSize.height < frame.size.height) {
//            
//            _needCheckGetNextPage = NO;
//            
//            MyGoodsList * myGoodsList = nil;
//            if(_listType == EGLT_RECOMMEND) {
//                myGoodsList = [GoodsHandler shareGoodsHandler].myGoodsList;
//            } else {
//                myGoodsList = [GoodsHandler shareGoodsHandler].friendGoodsList;
//            }
//            
//            if (_goodsList &&!myGoodsList.loading && !myGoodsList.endOfAll) {
//                [myGoodsList loadNextPage:NO];
//            }
//        }
//    }
//}
//
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//}

#pragma mark - Pull Refresh

/**
 *  集成刷新控件
 */
- (void)setupRefresh
{
    //设置下拉刷新
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshActionHandler:^{
        [weakSelf headerRereshing];
    }];
    [self.tableView.tipLabel setText:[UzysTipLabel refreshTips]];
    [self.tableView triggerPullToRefresh];
    
    [self.tableView addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRereshing];
    }];
    self.tableView.footer.automaticallyRefresh = YES;
}

- (void)headerRereshing
{
    MyGoodsList * myGoodsList = nil;
    if(_listType == EGLT_RECOMMEND) {
        myGoodsList = [GoodsHandler shareGoodsHandler].myGoodsList;
    } else {
        myGoodsList = [GoodsHandler shareGoodsHandler].friendGoodsList;
    }
    if (!myGoodsList.loading) {
        [myGoodsList loadNextPage:YES];
    }
}

////////////////////// modify by zn /////////////
- (void)footerRereshing
{
    MyGoodsList * myGoodsList = nil;
    if(_listType == EGLT_RECOMMEND) {
        myGoodsList = [GoodsHandler shareGoodsHandler].myGoodsList;
    } else {
        myGoodsList = [GoodsHandler shareGoodsHandler].friendGoodsList;
    }
    if (!myGoodsList.loading) {
        if(![myGoodsList loadNextPage:NO]) {
            [self.tableView.footer endRefreshing];
        }
    }
}

- (void)stopHeadRefresh
{
    if ([_tableView showPullToRefresh]) {
        [_tableView stopRefreshAnimation];
        [_tableView.tipLabel setText:[UzysTipLabel refreshTips]];
    }
}

#pragma mark - notification

- (void)processPublishGoodsNotify:(NSNotification *)notification
{
    MyGoodsList *myGoodsList = [GoodsHandler shareGoodsHandler].friendGoodsList;
    [myGoodsList loadNextPage:YES];
}

- (void)processGoodsListLoadedNotify:(NSNotification *)notification
{
    NSNumber * type = [notification.userInfo objectForKey:@"type"];
    if(type && [type intValue] == _listType) {
        [self reloadGoodsList];
    }
}

- (void)processGoodsListLoadStateNotify:(NSNotification *)notification
{
    NSNumber * type = [notification.userInfo objectForKey:@"type"];
     if(type && [type intValue] == _listType) {
         [self refreshStateViewUI];
     }
}

- (void)processGoodsUpdateFavorNotify:(NSNotification *)notification
{
    NSDictionary * dict = notification.userInfo;
    NSNumber * temp = [dict objectForKey:@"goodsId"];
    for (int i=0; i<_goodsList.count; i++) {
        QMGoodsController *goodsController = [_goodsList objectAtIndex:i];
        if ([temp intValue] == goodsController.goodsInfo.gid) {
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:3 inSection:i];
            [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        }
    }
    
//    NSNumber * type = [dict objectForKey:@"type"];
//    if(type && [type intValue] == _listType) {
//        QMGoodsController * viewController = [_goodslistDict objectForKey:temp];
//        if(viewController) {
//            NSUInteger index = [_goodsList indexOfObject:viewController];
//            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:3 inSection:index];
//            [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
////            [_tableView reloadData];
//        }
//    }
}

- (void)processDeleteGoodsNotify:(NSNotification *)notification
{
    uint64_t goodsId = [[notification.userInfo objectForKey:@"goodsId"] unsignedLongLongValue];
    if(goodsId != 0) {
        QMGoodsController * viewController = [_goodslistDict objectForKey:@(goodsId)];
        [_goodslistDict removeObjectForKey:@(goodsId)];
        
         NSUInteger index = [_goodsList indexOfObject:viewController];
        if(index != NSNotFound) {
            [_goodsList removeObjectAtIndex:index];
            [_tableView deleteSections:[NSIndexSet indexSetWithIndex:index] withRowAnimation:UITableViewRowAnimationNone];
        } else {
            [_tableView reloadData];
        }
    }
}


- (void)processNetworkErrorNotify:(NSNotification *)notification
{
    [self stopHeadRefresh];
    
////////////////////// modify by zn /////////////
    if ([_tableView.footer isRefreshing]) {
        [self.tableView.footer endRefreshing];
    }
}

- (void)processGoodsUpdateCommentNotify:(NSNotification *)notification
{
    NSDictionary * dict = notification.userInfo;
    NSNumber * temp = [dict objectForKey:@"goodsId"];
    QMGoodsController * viewController = [_goodslistDict objectForKey:temp];
    if(viewController) {
        NSUInteger index = [_goodsList indexOfObject:viewController];
        if(NSNotFound != index) {
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:3 inSection:index];
            [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

-(void)processScrollToTopNotify:(NSNotification *)notification{
    if (scrollFlag>1) {
        NSDictionary *dictInfo=notification.userInfo;
        UIViewController *vi=[dictInfo objectForKey:@"viewController"];
        if(self==vi){
            [_tableView setContentOffset:CGPointMake(0, -60) animated:YES];
        }
    }else{
        scrollFlag++;
    }
}

- (void)processGoodsNumUpdateNotify:(NSNotification *)notification
{
    NSString *strText;
    hidePrompt=NO;
    NSDictionary *dict=notification.userInfo;
    NSNumber *temp=[dict objectForKey:@"number"];
//    [self updateMessageLabelWithNum:temp.integerValue];

    if(temp.integerValue >0){
        strText=[NSString stringWithFormat:@"有%ld个新商品",(long)temp.integerValue];
        [self updateMessageLabelWithText:strText];
    }else if(temp.integerValue ==0){
        strText=[NSString stringWithFormat:@"没有新的商品了"];
        [self updateMessageLabelWithText:strText];
    }
}

-(void)updateMessageLabelWithText:(NSString *)text{//add by zhaozd
    
    if(nil==lblUpdateMessage){
        lblUpdateMessage=[[UILabel alloc] init];
    }
    lblUpdateMessage.frame=CGRectMake(0, kMainTopHeight, kMainFrameWidth, 36);
    lblUpdateMessage.hidden=NO;
    lblUpdateMessage.backgroundColor=UIColorFromRGB(0x5381db);
    lblUpdateMessage.layer.opacity=0.9;
    lblUpdateMessage.layer.shadowColor=UIColorFromRGB(0xd8d8d8).CGColor;
    lblUpdateMessage.layer.shadowOffset=CGSizeMake(0, 1);
    lblUpdateMessage.layer.shadowOpacity=0.8;
    lblUpdateMessage.textAlignment=NSTextAlignmentCenter;
    lblUpdateMessage.font=[UIFont systemFontOfSize:15.0];
    lblUpdateMessage.textColor=UIColorFromRGB(0xffffff);
    lblUpdateMessage.text=text;
    [self.view addSubview:lblUpdateMessage];
    
//    [self contract];
//    _tableView.scrollEnabled=NO;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation setDuration:1];
    [animation setRepeatCount:1];
    [animation setFromValue:[NSNumber numberWithFloat:0]];
    [animation setToValue:[NSNumber numberWithFloat:1]];
    [animation setDelegate:self];
    [lblUpdateMessage.layer addAnimation:animation forKey:@"firstView-Opacity"];
    
    CABasicAnimation *positionAnimation =[CABasicAnimation animationWithKeyPath:@"position"];
    [positionAnimation setFromValue:[NSValue valueWithCGPoint:CGPointMake(lblUpdateMessage.center.x, lblUpdateMessage.center.y-36)]];
    [positionAnimation setToValue:[NSValue valueWithCGPoint:CGPointMake(lblUpdateMessage.center.x, lblUpdateMessage.center.y)]];
    [positionAnimation setDuration:0.5f];
    [lblUpdateMessage.layer addAnimation:positionAnimation forKey:nil];
    
}


-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if (!hidePrompt) {
        hidePrompt=YES;
        [self performSelector:@selector(hideLabel) withObject:nil afterDelay:1.0];
    }else{
        _tableView.scrollEnabled=YES;
        lblUpdateMessage.hidden=YES;
    }
}

-(void)hideLabel{
    //透明度变化
    CABasicAnimation *opacityAnim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnim.fromValue = [NSNumber numberWithFloat:1.0];
    opacityAnim.toValue = [NSNumber numberWithFloat:0.1];
    opacityAnim.duration=1.0f;
    opacityAnim.removedOnCompletion = NO;
    opacityAnim.delegate=self;
    [lblUpdateMessage.layer addAnimation:opacityAnim forKey:nil];
    
    
    CABasicAnimation *positionAnimation =[CABasicAnimation animationWithKeyPath:@"position"];
    [positionAnimation setFromValue:[NSValue valueWithCGPoint:CGPointMake(lblUpdateMessage.center.x, lblUpdateMessage.center.y)]];
    [positionAnimation setToValue:[NSValue valueWithCGPoint:CGPointMake(lblUpdateMessage.center.x, lblUpdateMessage.center.y-36)]];
    [positionAnimation setDuration:0.5f];
    [positionAnimation setDelegate:self];
    [lblUpdateMessage.layer addAnimation:positionAnimation forKey:nil];
    
    lblUpdateMessage.layer.opacity=0.1;
    lblUpdateMessage.frame=CGRectMake(0, 0, kMainFrameWidth, 36);
}


#pragma mark - QMGoodsControllerDelegate

//点击商品图像
- (void)didGoodsImageClick:(QMGoodsController *)controller curIndex:(NSInteger)index
{
    [[QAudioPlayer sharedInstance] stopPlay];
    GoodsDetailViewController *viewController = [[GoodsDetailViewController alloc] initWithGoodsId:controller.goodsInfo.gid];

    [self.navigationController pushViewController:viewController animated:YES];
    
//    QMFriendViewController * viewController = [[QMFriendViewController alloc] initWithNibName:@"QMFriendViewController" bundle:nil];
//    [self.navigationController pushViewController:viewController animated:YES];
}

//点击用户头像
- (void)didGoodsHeadImageClick:(QMGoodsController *)controller userId:(uint64_t)userId
{
    [[QAudioPlayer sharedInstance] stopPlay];
    PersonPageViewController *personController = [[PersonPageViewController alloc]initWithUserId:userId];
    [self.navigationController pushViewController:personController animated:YES];
}

//点击赞
- (void)didGoodsFavorClick:(QMGoodsController *)controller
{
    [[GoodsHandler shareGoodsHandler] addFavor:controller.goodsInfo.gid sellerId:controller.goodsInfo.sellerUserId];
}

//点击评论
- (void)didGoodsCommentClick:(QMGoodsController *)controller
{
    [[QAudioPlayer sharedInstance] stopPlay];
    GoodsDetailViewController *viewController = [[GoodsDetailViewController alloc] initWithGoodsId:controller.goodsInfo.gid];
    viewController.needScrollToComments = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

//点击分享
- (void)didGoodsShareClick:(QMGoodsController *)controller
{
    if(nil == _shareViewController) {
        _shareViewController = [[QMShareViewContrller alloc] init];
    }
    [_shareViewController setGoodsInfo:controller.goodsInfo];
    [_shareViewController show];
}


#pragma mark - The Magic!

-(void)expand
{
    if(hidden)
        return;
    
    hidden = YES;
    
    [[self rdv_tabBarController] setTabBarHidden:YES animated:YES];
    UIEdgeInsets insets = UIEdgeInsetsMake(20,
                                           0,
                                           0,
                                           0);
    self.tableView.contentInset = insets;
    self.tableView.scrollIndicatorInsets = insets;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}

-(void)contract
{
    if(!hidden)
        return;
    
    hidden = NO;
    
    [[self rdv_tabBarController] setTabBarHidden:NO animated:YES];
    if (self.rdv_tabBarController.tabBar.translucent) {
        UIEdgeInsets insets = UIEdgeInsetsMake(64,
                                               0,
                                               CGRectGetHeight(self.rdv_tabBarController.tabBar.frame),
                                               0);
        
        self.tableView.contentInset = insets;
        self.tableView.scrollIndicatorInsets = insets;
    }
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//    startContentOffset = lastContentOffset = scrollView.contentOffset.y;
//}
//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    NSTimeInterval thisInterval=[[NSDate date] timeIntervalSince1970];
//    if ((thisInterval-lastInterval)*1000<250) {
//        return;
//    }
//    lastInterval=thisInterval;
//    
//    CGFloat currentOffset = scrollView.contentOffset.y;
//    CGFloat differenceFromStart = startContentOffset - currentOffset;
//    CGFloat differenceFromLast = lastContentOffset - currentOffset;
//    lastContentOffset = currentOffset;
//    
//    if((differenceFromStart) < 0)
//    {
//        // scroll up
//        if(scrollView.isTracking && (abs(differenceFromLast)>1))
//            [self expand];
//    }
//    else {
//        if(scrollView.isTracking && (abs(differenceFromLast)>1))
//            [self contract];
//    }
//}
//
//- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
//{
//    [self contract];
//    return YES;
//}
@end
