//
//  EditGoodsPage2ViewController.m
//  QMMM
//
//  Created by kingnet  on 15-3-2.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "EditGoodsPage2ViewController.h"
#import "GoodsChooseCell.h"
#import "GoodsOriPriceCell.h"
#import "EditGoodsToolBar.h"
#import "GoodsHandler.h"
#import "QMGoodsCateInfo.h"
#import "GoodsCategoryViewController.h"
#import "QMPickerView.h"
#import "ChooseCityView.h"
#import "QMGetLocation.h"
#import "ButtonAccessoryView.h"
#import "PublishOKViewController.h"
#import "QMPublishPreviewViewController.h"
#import "UserInfoHandler.h"
#import "UserEntity.h"
#import "GoodsHandler.h"
#import "QMQAViewController.h"

@interface EditGoodsPage2ViewController ()<UITableViewDelegate, UITableViewDataSource, EditGoodsToolBarDelegate>
{
    __block BOOL choosedCate_; //是否手动选择过分类
    __block NSArray *cateArray_; //分类
    
    __block int provinceID_;
    __block int cityID_;
    __block int districtID_;
    __block BOOL chooseLoc_; //是否手动选择过地址
    
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) GoodsChooseCell *chooseCateCell; //分类
@property (nonatomic, strong) GoodsChooseCell *chooseUsageCell; //新旧程度
@property (nonatomic, strong) GoodsChooseCell *chooseNumCell; //数量
@property (nonatomic, strong) GoodsChooseCell *chooseLocCell; //位置
@property (nonatomic, strong) GoodsOriPriceCell *oriPriceCell; //原价
@property (nonatomic, strong) EditGoodsToolBar *toolBar;
@property (nonatomic, strong) ButtonAccessoryView *accessoryView;

@property (nonatomic, strong) GoodEntity *editGoods; //正在编辑的商品

@end

@implementation EditGoodsPage2ViewController

- (id)initWithType:(QMEditGoodsType)type goodsEntity:(GoodEntity *)goodsEntity
{
    self= [super init];
    if (self) {
        self.controllerType = type;
        _editGoods = [[GoodEntity alloc]init];
        if (self.controllerType == QMEditNewGoods) {
            _editGoods.cate_type = -1;
            _editGoods.isNew = 1;
            _editGoods.number = 1;
            _editGoods.city = @"";
            _editGoods.orig_price = 0.00;
            UserEntity *user = [[UserInfoHandler sharedInstance]currentUser];
            _editGoods.sellerUserId = [user.uid longLongValue];
            _editGoods.sellerNick = user.nickname;
            _editGoods.sellerHeadIcon = user.avatar;
        }
        else{
            [_editGoods assignValue:goodsEntity];
        }
        
        _editGoods.location = @""; //两种情况一开始都没有
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.controllerType == QMEditNewGoods) {
        self.navigationItem.title = @"发布商品";
    }
    else{
        self.navigationItem.title = @"编辑商品";
    }
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    self.navigationItem.rightBarButtonItems=[AppUtils createRightButtonWithTarget:self selector:@selector(tipsAction:) title:nil size:CGSizeMake(50, 50) imageName:@"btnTips"];
    
    
    provinceID_ = 0;
    cityID_ = 0;
    districtID_ = 0;
    
    [self setupViews];
    
    [self getGoodsCategory];
    
    //没有地址时再请求
    if ([_editGoods.city isEqualToString:@""]) {
        [self requestCurrentLoc];
    }
    else{
        self.chooseLocCell.content = _editGoods.city;
    }
}

-(void)tipsAction:(id)sender{
    QMQAViewController *qa=[[QMQAViewController alloc] initWithType:QMQATypeCondition];
    [self.navigationController pushViewController:qa animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Other Methods
- (void)getGoodsCategory
{
    __weak typeof(self) weakSelf = self;
    [[GoodsHandler shareGoodsHandler]getCategoryFromLocal:^(id obj) {
        NSArray *arr = (NSArray *)obj;
        cateArray_ = [NSArray arrayWithArray:arr];
        [weakSelf updateCateName:weakSelf.editGoods.cate_type];
        
    } failed:^(id obj) {
        //本地没有缓存分类
        //从服务器拉取
        [[GoodsHandler shareGoodsHandler]getCategory:^(id obj) {
            NSArray *arr = (NSArray *)obj;
            
            cateArray_ = [NSArray arrayWithArray:arr];
            
            [weakSelf updateCateName:weakSelf.editGoods.cate_type];
        } failed:^(id obj) {
            
        }];
    }];
}

- (void)updateCateName:(NSInteger)cate
{
    BOOL find = NO;
    if (cate != 0 && cateArray_ && cateArray_.count>0) {
        for (QMGoodsCateInfo *cateInfo in cateArray_) {
            if (cateInfo.cateId == cate) {
                self.chooseCateCell.contentColor = kDarkTextColor;
                self.chooseCateCell.content = cateInfo.name;
                find = YES;
                break;
            }
        }
    }
    if (!find) {
        //没有找到则显示请选择分类
        self.chooseCateCell.contentColor = kLightBlueColor;
        self.chooseCateCell.content = @"请选择分类";
    }
}

- (void)requestCurrentLoc
{
    __weak typeof(self) weakSelf = self;
    self.chooseLocCell.content = @"正在定位...";
    [QMGetLocation getCurrentLoc:^(CLLocation *currentLoc, NSString *placeMark, NSError *error) {
        if (nil == error) {
            CLLocationCoordinate2D coordinate = currentLoc.coordinate;
            if (placeMark && ![placeMark isEqualToString:@""]) {
                //拿到了具体位置
                if (NO == chooseLoc_) {
                    weakSelf.editGoods.city = [placeMark stringByReplacingOccurrencesOfString:@"•" withString:@""];
                    weakSelf.chooseLocCell.content = [placeMark stringByReplacingOccurrencesOfString:@"•" withString:@""];
                }
                weakSelf.editGoods.location = [NSString stringWithFormat:@"%f,%f", coordinate.latitude, coordinate.longitude];
            }
            else{
                //只有坐标，没有具体的位置
                weakSelf.editGoods.location = [NSString stringWithFormat:@"%f,%f", coordinate.latitude, coordinate.longitude];
                if (NO == chooseLoc_) {
                    weakSelf.chooseLocCell.content = @"定位失败";
                }
            }
        }
        else{
            if (NO == chooseLoc_) {
                weakSelf.chooseLocCell.content = @"定位失败";
            }
        }
    } curPointBlock:^(CLLocation *currentLoc, NSError *error) {
        CLLocationCoordinate2D coordinate = currentLoc.coordinate;
        weakSelf.editGoods.location = [NSString stringWithFormat:@"%f,%f", coordinate.latitude, coordinate.longitude];
    }];
}

- (void)publishGoods
{
    [AppUtils showProgressMessage:[AppUtils localizedProductString:@"QM_HUD_Releasing"]];
    EditGoodsPage2ViewController __weak *weakSelf = self;
    [[GoodsHandler shareGoodsHandler]releaseGoods:self.editGoods success:^(id obj) {
        [AppUtils showSuccessMessage:@"发布成功"];
        GoodEntity *goods = [[GoodEntity alloc]initWithGoodsEntity:weakSelf.editGoods];
        if ([obj isKindOfClass:[NSDictionary class]]) {
            id tmp = [obj objectForKey:@"id"];
            if (tmp && ([tmp isKindOfClass:[NSNumber class]] || [tmp isKindOfClass:[NSString class]])) {
                goods.gid = [tmp longLongValue];
            }
            
            tmp = [obj objectForKey:@"photo"];
            if (tmp && [tmp isKindOfClass:[NSArray class]]) {
                GoodsPictureList *picList = [[GoodsPictureList alloc]initWithArray:tmp];
                goods.picturelist = picList;
            }
            
            tmp = [obj objectForKey:@"audio"];
            if (tmp && [tmp isKindOfClass:[NSString class]]) {
                goods.msgAudio = tmp;
            }
        }
        
        PublishOKViewController *controller = [[PublishOKViewController alloc]initWithGoodsEntity:goods];
        NSArray *viewControllers = [NSArray arrayWithObject:controller];
        [weakSelf.navigationController setViewControllers:viewControllers animated:YES];
        
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)updateGoods
{
    [AppUtils showProgressMessage:@"正在提交"];
    __weak __typeof(self) weakSelf = self;
    [[GoodsHandler shareGoodsHandler]updateGoodsInfo:self.editGoods success:^(id obj) {
        [AppUtils showSuccessMessage:@"更新成功"];
        //通知前面的界面
        __typeof(weakSelf) strongSelf = weakSelf;
        GoodEntity *goods = [[GoodEntity alloc]initWithGoodsEntity:weakSelf.editGoods];
        if ([obj isKindOfClass:[NSDictionary class]]) {
            id tmp = [obj objectForKey:@"id"];
            if (tmp && ([tmp isKindOfClass:[NSNumber class]] || [tmp isKindOfClass:[NSString class]])) {
                goods.gid = [tmp longLongValue];
            }
            
            tmp = [obj objectForKey:@"photo"];
            if (tmp && [tmp isKindOfClass:[NSArray class]]) {
                GoodsPictureList *picList = [[GoodsPictureList alloc]initWithArray:tmp];
                goods.picturelist = picList;
            }
            tmp = [obj objectForKey:@"audio"];
            if (tmp && [tmp isKindOfClass:[NSString class]]) {
                goods.msgAudio = tmp;
            }
        }
        
        if (strongSelf.updateGoodsBlock) {
            strongSelf.updateGoodsBlock(goods);
        }
        NSMutableArray *controllers = [NSMutableArray arrayWithArray:strongSelf.navigationController.viewControllers];
        [controllers removeObjectsInRange:NSMakeRange(controllers.count-2, 2)];
        [strongSelf.navigationController setViewControllers:controllers animated:YES];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

#pragma mark - UIView Method

- (void)setupViews
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStyleGrouped];
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kBorderColor;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    [self.view addSubview:_tableView];
    
    [self.view addSubview:self.toolBar];
    
    //键盘收回按钮
    _accessoryView = [[ButtonAccessoryView alloc]init];
    _accessoryView.originalFrame = CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds)-CGRectGetWidth(_accessoryView.frame)-5.f,CGRectGetHeight(self.view.frame), CGRectGetWidth(_accessoryView.frame), CGRectGetHeight(_accessoryView.frame));
    [self.view addSubview:_accessoryView];
}

- (GoodsChooseCell *)chooseCateCell
{
    if (!_chooseCateCell) {
        _chooseCateCell = [GoodsChooseCell chooseCell];
        _chooseCateCell.leftText = [[GoodsChooseCell leftTextArr] objectAtIndex:0];
    }
    return _chooseCateCell;
}

- (GoodsChooseCell *)chooseLocCell
{
    if (!_chooseLocCell) {
        _chooseLocCell = [GoodsChooseCell chooseCell];
        _chooseLocCell.leftText = [[GoodsChooseCell leftTextArr]objectAtIndex:3];
    }
    return _chooseLocCell;
}

- (GoodsChooseCell *)chooseNumCell
{
    if (!_chooseNumCell) {
        _chooseNumCell = [GoodsChooseCell chooseCell];
        _chooseNumCell.leftText = [[GoodsChooseCell leftTextArr]objectAtIndex:2];
        _chooseNumCell.content = [NSString stringWithFormat:@"%d", self.editGoods.number];
    }
    return _chooseNumCell;
}

- (GoodsChooseCell *)chooseUsageCell
{
    if (!_chooseUsageCell) {
        _chooseUsageCell = [GoodsChooseCell chooseCell];
        _chooseUsageCell.leftText = [[GoodsChooseCell leftTextArr]objectAtIndex:1];
        _chooseUsageCell.content = [[GoodEntity usageDesc]objectAtIndex:self.editGoods.isNew];
    }
    return _chooseUsageCell;
}

- (GoodsOriPriceCell *)oriPriceCell
{
    if (!_oriPriceCell) {
        _oriPriceCell = [GoodsOriPriceCell oriPriceCell];
        _oriPriceCell.price = self.editGoods.orig_price;
    }
    return _oriPriceCell;
}

- (EditGoodsToolBar *)toolBar
{
    if (!_toolBar) {
        _toolBar = [EditGoodsToolBar toolBar];
        _toolBar.frame = CGRectMake(0, kMainFrameHeight-[EditGoodsToolBar viewHeight], CGRectGetWidth(self.view.frame), [EditGoodsToolBar viewHeight]);
        _toolBar.delegate = self;
        if (self.controllerType == QMEditNewGoods) {
            _toolBar.btnTitle = @"确认发布";
        }
        else{
            _toolBar.btnTitle = @"确认更新";
        }
    }
    return _toolBar;
}

#pragma mark - UITableView Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (0 == section) {
        return 2;
    }
    else if (1 == section){
        return 3;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (0 == indexPath.section) {
        if (0 == indexPath.row) {
            return self.chooseCateCell;
        }
        else if (1 == indexPath.row){
            return self.chooseUsageCell;
        }
    }
    else if(1 == indexPath.section){
        if (0 == indexPath.row) {
            return self.oriPriceCell;
        }
        else if (1 == indexPath.row){
            return self.chooseNumCell;
        }
        else if (2 == indexPath.row){
            return self.chooseLocCell;
        }
    }
    return nil;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 14.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [AppUtils closeKeyboard];
    
    __weak typeof(self) weakSelf = self;
    if (0 == indexPath.section && 0 == indexPath.row) {
        GoodsCategoryViewController *controller = [[GoodsCategoryViewController alloc]initWithBlock:^(int categoryId, NSString *categoryName) {
            weakSelf.chooseCateCell.content = categoryName;
            weakSelf.editGoods.cate_type = categoryId;
            weakSelf.chooseCateCell.contentColor = kDarkTextColor;
            choosedCate_ = YES;
        }];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (0 == indexPath.section && 1 == indexPath.row){
        [QMPickerView showWithArray:[GoodEntity usageDesc] inView:self.view atIndex:self.editGoods.isNew completion:^(NSString *string) {
            NSInteger atIndex = [[GoodEntity usageDesc] indexOfObject:string];
            if (atIndex != NSNotFound) {
                weakSelf.chooseUsageCell.content = string;
                weakSelf.editGoods.isNew = (int)atIndex;
            }
        }];
    }
    else if (1 == indexPath.section && 1 == indexPath.row){
        NSArray *numArr = @[@"1", @"2", @"3", @"4", @"5"];
        
        [QMPickerView showWithArray:numArr inView:self.view atIndex:self.editGoods.number-1 completion:^(NSString *string) {
            NSInteger atIndex = [numArr indexOfObject:string];
            if (atIndex != NSNotFound) {
                weakSelf.chooseNumCell.content = string;
                weakSelf.editGoods.number = [string intValue];
            }
        }];
    }
    else if (1 == indexPath.section && 2 == indexPath.row){
        [ChooseCityView showInView:self.view provinceId:provinceID_ cityId:cityID_ distritId:districtID_ completion:^(int provinceID, int cityID, int districtID, NSString *result) {
            if (result.length > 0) {
                weakSelf.chooseLocCell.content = result;
                weakSelf.editGoods.city = result;
                provinceID_ = provinceID;
                cityID_ = cityID;
                districtID_ = districtID;
                chooseLoc_ = YES;
            }
        }];
    }
}

#pragma mark - EditGoodsTabBarDelegate
//发布商品
- (void)didTapPublish
{
    if (_editGoods.cate_type == -1) {
        [AppUtils showAlertMessage:@"请选择分类"];
        return;
    }
    //获取原价
    _editGoods.orig_price = self.oriPriceCell.price;
    
    if (self.controllerType == QMEditNewGoods) {
        [self publishGoods];
    }
    else{
        [self updateGoods];
    }
}

//预览商品
- (void)didTapPreview
{
    //获取原价
    _editGoods.orig_price = self.oriPriceCell.price;
    
    QMPublishPreviewViewController *previewController = [[QMPublishPreviewViewController alloc]initWithGoodsEntity:_editGoods localImgPath:self.goodsPhotoArr localAudioPath:self.audioPath editType:self.controllerType];
    previewController.updateGoodsBlock = self.updateGoodsBlock;
    [self.navigationController pushViewController:previewController animated:YES];
}

#pragma mark - 设置商品信息
- (void)setGoodsEntity:(GoodEntity *)goodsEntity
{
    _editGoods.picturelist = goodsEntity.picturelist;
    _editGoods.desc = goodsEntity.desc;
    _editGoods.price = goodsEntity.price;
    _editGoods.msgAudio = goodsEntity.msgAudio;
    _editGoods.msgAudioSeconds = goodsEntity.msgAudioSeconds;
    
    if (NO == choosedCate_ && _editGoods.desc.length > 0) { //未手动选择过
        //查询分类
        __weak typeof(self) weakSelf = self;
        [[GoodsHandler shareGoodsHandler]getCategoryByDesc:_editGoods.desc success:^(id obj) {
            if (obj && [obj isKindOfClass:[NSString class]]) {
                NSInteger cid = [obj integerValue];
                cid = (cid == kDefaultCategoryId)?-1:cid;
                [weakSelf updateCateName:cid];
                weakSelf.editGoods.cate_type = (int)cid;
            }
            else if (obj && [obj isKindOfClass:[NSNumber class]]){
                NSInteger cid = [obj integerValue];
                cid = (cid == kDefaultCategoryId)?-1:cid;
                [weakSelf updateCateName:cid];
                weakSelf.editGoods.cate_type = (int)cid;
            }
        } failed:^(id obj) {
            
        }];
    }
}

@end
