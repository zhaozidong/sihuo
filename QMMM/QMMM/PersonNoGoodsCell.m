//
//  PersonNoGoodsCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PersonNoGoodsCell.h"

@interface PersonNoGoodsCell ()
@property (weak, nonatomic) IBOutlet UILabel *tipLbl;

@end
@implementation PersonNoGoodsCell

+ (PersonNoGoodsCell *)personNoGoods
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"PersonNoGoodsCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.tipLbl.text = @"";
    self.tipLbl.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTip:(NSString *)tip
{
    self.tipLbl.text = tip;
}

@end
