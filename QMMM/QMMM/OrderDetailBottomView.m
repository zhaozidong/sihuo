//
//  OrderDetailBottomView.m
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderDetailBottomView.h"
#import "OrderDataInfo.h"

@interface OrderDetailBottomView ()
{
    BOOL bSeller_;
    OrderDataInfo *orderInfo_;
}
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end
@implementation OrderDetailBottomView

+ (OrderDetailBottomView *)bottomView
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"OrderDetailBottomView" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.priceLbl.textColor = kRedColor;
    self.priceLbl.text = @"";
    self.priceLbl.hidden = YES;
    UIImage * image = [UIImage imageNamed:@"ok_btn"];
    image = [image stretchableImageWithLeftCapWidth:5 topCapHeight:6];
    [self.button setBackgroundImage:image forState:UIControlStateNormal];
    UIImage *image1 = [UIImage imageNamed:@"btn_cancel"];
    [self.button setBackgroundImage:[image1 stretchableImageWithLeftCapWidth:10 topCapHeight:8] forState:UIControlStateDisabled];
}

- (void)updateUI:(OrderDataInfo *)orderInfo bSeller:(BOOL)bSeller
{
    orderInfo_ = orderInfo;
    bSeller_ = bSeller;
    
    if (orderInfo.order_status == 0) {
        self.priceLbl.hidden = NO;
        self.priceLbl.text = [NSString stringWithFormat:@"¥ %.2f", orderInfo.payAmount];
        if (bSeller) {
            [self.button setTitle:@"修改价格" forState:UIControlStateNormal];
        }
        else{
            [self.button setTitle:@"付 款" forState:UIControlStateNormal];
        }
    }
    else if (orderInfo.order_status == 2){//已付款待发货
        if (bSeller) {
            [self.button setTitle:@"发 货" forState:UIControlStateNormal];
        }
    }
    else if (orderInfo.order_status == 3){
        if (!bSeller) {
            [self.button setTitle:@"确认收货" forState:UIControlStateNormal];
        }
    }
}

- (IBAction)buttonAction:(id)sender {
    if (orderInfo_.order_status == 0) {
        if (bSeller_) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(didTapModifyPrice)]) {
                [self.delegate didTapModifyPrice];
            }
        }
        else{
            if (self.delegate && [self.delegate respondsToSelector:@selector(didTapPay)]) {
                [self.delegate didTapPay];
            }
        }
    }
    else if (orderInfo_.order_status == 2){
        if (bSeller_) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(didTapDeliver)]) {
                [self.delegate didTapDeliver];
            }
        }
    }
    else if (orderInfo_.order_status == 3){
        if (!bSeller_) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(didTapConfirmReceive)]) {
                [self.delegate didTapConfirmReceive];
            }
        }
    }
}

- (void)setCanPay:(BOOL)canPay
{
    _canPay = canPay;
    self.button.enabled = _canPay;
}

+ (CGFloat)viewHeight
{
    return 50.f;
}


@end
