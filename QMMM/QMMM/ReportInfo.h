//
//  ReportInfo.h
//  QMMM
//
//  Created by Derek on 15/3/10.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger,QMReportType) {
    QMReportTypePerson=1,
    QMReportTypeGoods
};


@interface ReportInfo : NSObject

@property (nonatomic, strong)NSString *userId;

@property (nonatomic, assign)QMReportType type;

@property (nonatomic, strong)NSString *goodsID;

@property (nonatomic, strong)NSString *reason;

@property (nonatomic, strong)NSArray *photoKey;

-(id)initWithUserId:(NSString *)userID reason:(NSString *)reason photoKey:(NSArray *)key;

-(id)initWithGoodsId:(NSString *)goodsId userId:(NSString *)userId reaseon:(NSString *)reason photoKey:(NSArray *)key;

@end
