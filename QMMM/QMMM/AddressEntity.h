//
//  AddressEntity.h
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseEntity.h"

@class FMResultSet;

@interface AddressEntity : BaseEntity
//地址id
@property (nonatomic, assign) int addressId;

//用户id
@property (nonatomic, assign) int64_t uid;

//省id
@property (nonatomic, assign) int provinceId;

//市id
@property (nonatomic, assign) int cityId;

//区id
@property (nonatomic, assign) int distritId;

//详细地址
@property (nonatomic, copy) NSString *detailAddress;

//省市区
@property (nonatomic, copy) NSString *locationName;

//默认地址
@property (nonatomic, assign) BOOL isDefault;

//收货人姓名
@property (nonatomic, copy) NSString *consignee;

//收货人电话
@property (nonatomic, copy) NSString *phone;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

- (id)initWithFMResultSet:(FMResultSet *)rs;

- (void)assignWithEntity:(AddressEntity *)entity;

@end
