//
//  QMGoodsSearchViewController.h
//  QMMM
//
//  Created by kingnet  on 15-1-12.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@interface QMGoodsSearchViewController : BaseViewController

//填写关键字进来
- (id)initWithKeyWords:(NSString *)keyWords;

//选择分类进来
- (id)initWithCategoryId:(int)categoryId;

@end
