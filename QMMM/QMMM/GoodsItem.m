//
//  GoodsItem.m
//  QMMM
//
//  Created by kingnet  on 15-3-16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "GoodsItem.h"
#import "GoodDataInfo.h"
#import "UIImageView+QMWebCache.h"

@interface GoodsItem ()
@property (weak, nonatomic) IBOutlet UIImageView *goodsImageView;
@property (weak, nonatomic) IBOutlet UILabel *goodsDescLbl;
@property (weak, nonatomic) IBOutlet UILabel *goodsPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *trustLbl;

@end

@implementation GoodsItem

+ (GoodsItem *)goodsItem
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"GoodsItem" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.goodsPriceLbl.textColor = kRedColor;
    self.goodsDescLbl.textColor = kDarkTextColor;
    self.goodsDescLbl.text = @"";
    self.goodsPriceLbl.text = @"";
    self.goodsImageView.image = [UIImage imageNamed:@"goodsPlaceholder_middle"];
    self.trustLbl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    self.trustLbl.textColor = UIColorFromRGB(0xeeeeee);
    self.trustLbl.text = @"";
    self.trustLbl.layer.cornerRadius = CGRectGetHeight(self.trustLbl.frame)/2;
    self.trustLbl.layer.masksToBounds = YES;
    self.trustLbl.hidden = YES;
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [kBorderColor CGColor];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(oneTapAction:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGesture];
}

+ (CGSize)itemSize
{
    float margin = [[self class]margin];
    float w = (kMainFrameWidth-margin*3)/2;
    float h = w+58;
    return CGSizeMake(w, h);
}

+ (CGFloat)margin
{
    return 10.f;
}

- (void)setGoodsInfo:(GoodsSimpleInfo *)goodsInfo
{
    if (_goodsInfo != goodsInfo) {
        _goodsInfo = goodsInfo;
        self.goodsDescLbl.text = _goodsInfo.desc;
        self.goodsPriceLbl.text = [NSString stringWithFormat:@"¥%.2f", _goodsInfo.price];
        UIImage *image = [UIImage imageNamed:@"goodsPlaceholder_middle"];
        NSString *imgUrl = [AppUtils thumbPath:_goodsInfo.photo sizeType:QMImageHalfSize];
        [self.goodsImageView qm_setImageWithURL:imgUrl placeholderImage:image];
        if (_goodsInfo.trust > 0) {
            self.trustLbl.text = [NSString stringWithFormat:@"可信度：%d      ", _goodsInfo.trust];
            self.trustLbl.hidden = NO;
        }
        else{
            self.trustLbl.hidden = YES;
        }
    }
}

- (void)oneTapAction:(UITapGestureRecognizer *)gesture
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickGoodsItem:)]) {
        [self.delegate clickGoodsItem:self.goodsInfo];
    }
}

@end
