//
//  QMCheckExpressViewController.m
//  QMMM
//
//  Created by hanlu on 14-11-10.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMCheckExpressViewController.h"
#import "APIConfig.h"
#import "BaseHandler.h"

@interface QMCheckExpressViewController ()<UIWebViewDelegate> {
    NSString * _requestUrl;
    NSString * _expressNum;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewTopCst;

@end

@implementation QMCheckExpressViewController

/**
 @param expressCo 快递公司拼音
 @param expressNum 快递单号
 **/
- (id)initWithExpressInfo:(NSString *)expressCo expressNum:(NSString *)expressNum orderId:(NSString *)orderId
{
    self = [super initWithNibName:@"QMCheckExpressViewController" bundle:nil];
    if(self) {
        NSString * request = [NSString stringWithFormat:@"%@&com=%@&nu=%@&order_id=%@", API_CHECK_EXPRESSINFO, expressCo, expressNum, orderId];
         _requestUrl = [BaseHandler requestUrlWithPath:request];
        _expressNum = expressNum;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"物流信息";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(btnBackClick:)];
    
    _webView.delegate = self;
    _webView.scalesPageToFit = YES;
    
    self.webViewTopCst.constant = kMainTopHeight;
    [self.view layoutIfNeeded];
    
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:_requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [_webView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)btnBackClick:(id)sender
{
    [AppUtils dismissHUD];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [AppUtils showProgressMessage:@"正在加载..."];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [AppUtils dismissHUD];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [AppUtils showErrorMessage:@"加载失败!"];
}

- (IBAction)didTapCopyBtn:(id)sender {
    //复制快递单号
    if (_expressNum && _expressNum.length >0) {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        [pasteboard setString:_expressNum];
        [AppUtils showSuccessMessage:@"已复制"];
    }
    else{
        [AppUtils showErrorMessage:[NSString stringWithFormat:@"快递单号不对：%@", _expressNum]];
    }
}


@end
