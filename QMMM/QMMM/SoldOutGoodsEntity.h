//
//  SoldOutGoodsEntity.h
//  QMMM
//
//  Created by kingnet  on 14-10-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseEntity.h"

@interface EvaluateEntity : BaseEntity
//用户id
@property (nonatomic, strong) NSString *uid;

//评论内容
@property (nonatomic, strong) NSString *comment;

//用户头像
@property (nonatomic, strong) NSString *avatar;

//评论时间
@property (nonatomic, assign) int comment_ts;

//评分
@property (nonatomic, assign) int rate; //1差 2中 3好

//售出价格
@property (nonatomic, assign) float amout;

//品论人昵称
@property (nonatomic, strong) NSString *nickname;

@end

@interface SoldOutGoodsEntity : BaseEntity

@property (nonatomic, assign) uint64_t goodsId;

@property (nonatomic, strong) NSString *photo; //图片

@property (nonatomic, strong) NSString *desc; //描述

@property (nonatomic, assign) float price; //商品价格

@property (nonatomic, strong) NSArray *evaluateArr; //评价列表

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
