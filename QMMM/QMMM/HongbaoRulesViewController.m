//
//  HongbaoRulesViewController.m
//  QMMM
//
//  Created by kingnet  on 15-2-6.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "HongbaoRulesViewController.h"
#import "QMWebViewDelegate.h"
#import "BaseHandler.h"
#import "UIWebView+Additions.h"
#import "APIConfig.h"

@interface HongbaoRulesViewController ()

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) QMWebViewDelegate *webViewDelegate;

@end

@implementation HongbaoRulesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"红包使用说明";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    self.webViewDelegate = [[QMWebViewDelegate alloc]initWithDelegate:nil];
    [self.view addSubview:self.webView];
    NSString *urlStr = [BaseHandler requestUrlWithPath:API_HONGBAO_RULES];
    [self.webView loadRemoteUrl:urlStr];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
        _webView.delegate = self.webViewDelegate;
        _webView.multipleTouchEnabled = NO;
        _webView.scalesPageToFit = YES;
    }
    return _webView;
}

@end
