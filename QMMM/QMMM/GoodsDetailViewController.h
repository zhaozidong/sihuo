//
//  GoodsDetailViewController.h
//  QMMM
//
//  Created by kingnet  on 14-10-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@interface GoodsDetailViewController : BaseViewController<UIActionSheetDelegate>

@property (nonatomic, assign) uint64_t goodsId;

//是否自动滚动到评论列表部分
@property (nonatomic, assign) BOOL needScrollToComments;

- (id)initWithGoodsId:(uint16_t)goodsId;

@end
