//
//  OrderDetailTimerCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderDetailTimerCell.h"
#import "OrderDataInfo.h"

NSString * const kOrderDetailTimerCellIdentifier = @"kOrderDetailTimerCellIdentifier";

@interface OrderDetailTimerCell ()
{
    NSTimer *timer_;
    NSTimeInterval timeInterval_;
}
@property (weak, nonatomic) IBOutlet UILabel *timerLbl; //显示倒计时
@property (weak, nonatomic) IBOutlet UILabel *tipLbl;
@property (weak, nonatomic) IBOutlet UILabel *orderClosedLbl;
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;

@end
@implementation OrderDetailTimerCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"OrderDetailTimerCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.timerLbl.hidden = YES;
    self.tipLbl.hidden = YES;
    self.orderClosedLbl.hidden = YES;
    self.timerLbl.text = @"";
    self.orderClosedLbl.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(OrderDataInfo *)orderInfo
{
    if (timer_) {
        [timer_ invalidate];
        timer_ = nil;
    }
    if (orderInfo.order_status == QMOrderStatusTimeout) {
        self.orderClosedLbl.text = @"订单超时，已被关闭";
        self.orderClosedLbl.hidden = NO;
        self.timerLbl.hidden = YES;
        self.tipLbl.hidden = YES;
    }
    else if (orderInfo.order_status == QMOrderStatusClosed) {
        self.orderClosedLbl.text = @"订单已关闭";
        self.orderClosedLbl.hidden = NO;
        self.timerLbl.hidden = YES;
        self.tipLbl.hidden = YES;
    }
    else if (orderInfo.order_status == QMOrderStatusPaid) {
        self.orderClosedLbl.text = @"买家已付款，等待卖家发货";
        self.orderClosedLbl.hidden = NO;
        self.timerLbl.hidden = YES;
        self.tipLbl.hidden = YES;
        self.statusImage.image = [UIImage imageNamed:@"order_payed_bkg"];
    }
    else if (orderInfo.order_status == QMOrderStatusDelivery){
        self.orderClosedLbl.hidden = YES;
        NSString *tips = @"买家需在15天内确认收货";
        NSRange range = [tips rangeOfString:@"15"];
        NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:tips];
        [attributedStr addAttributes:@{NSForegroundColorAttributeName:kRedColor} range:range];
        self.timerLbl.attributedText = attributedStr;
        self.timerLbl.attributedText = attributedStr;
        self.tipLbl.text = @"逾期系统将自动汇款给卖家";
        self.timerLbl.hidden = NO;
        self.tipLbl.hidden = NO;
        self.statusImage.image = [UIImage imageNamed:@"order_delivered_bkg"];
    }
    else if (orderInfo.order_status == QMOrderStatusRefundApply || orderInfo.order_status == QMOrderStatusRefundReject){
        self.orderClosedLbl.text = @"买家申请退款";
        self.orderClosedLbl.hidden = NO;
        self.timerLbl.hidden = YES;
        self.tipLbl.hidden = YES;
        if (orderInfo.last_status == QMOrderStatusDelivery) {
            self.statusImage.image = [UIImage imageNamed:@"order_delivered_bkg"];
        }
        else{
            self.statusImage.image = [UIImage imageNamed:@"order_payed_bkg"];
        }
    }
    else if (orderInfo.order_status == QMOrderStatusRefundClosed){
        self.orderClosedLbl.hidden = YES;
        self.timerLbl.text = @"订单已关闭";
        self.tipLbl.text = @"退款已汇入买家账户余额中";
        self.timerLbl.hidden = NO;
        self.tipLbl.hidden = NO;
        if (orderInfo.last_status == QMOrderStatusDelivery) {
            self.statusImage.image = [UIImage imageNamed:@"order_delivered_bkg"];
        }
        else{
            self.statusImage.image = [UIImage imageNamed:@"order_payed_bkg"];
        }
    }
    else if (orderInfo.order_status == QMOrderStatusWaitPay){//正在等待付款
        //计算还剩多少时间
        NSTimeInterval diffSeconds = orderInfo.sys_ts - orderInfo.order_time;
#ifdef DEBUG
        diffSeconds = 3*60 - diffSeconds; //有效时间是3*60秒
#else
        if (kIsTestRelease) {
            diffSeconds = 3*60 - diffSeconds; //有效时间是3*60秒
        }
        else{
            diffSeconds = 30*60 - diffSeconds; //有效时间是30*60秒
        }
#endif
        if (diffSeconds > 0) {
            self.timerLbl.hidden = NO;
            self.tipLbl.hidden = NO;
            timeInterval_ = diffSeconds;
            NSArray *array = [AppUtils getTimeWithInterval:timeInterval_];
            NSString *timeStr = [NSString stringWithFormat:@"%@:%@", array[1], array[2]];
            NSString *tips = [NSString stringWithFormat:@"买家需在%@内完成付款", timeStr];
            NSRange range = [tips rangeOfString:timeStr];
            NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:tips];
            [attributedStr addAttributes:@{NSForegroundColorAttributeName:kRedColor} range:range];
            self.timerLbl.attributedText = attributedStr;
            [timer_ invalidate];
            timer_ = nil;
            timer_ = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop]addTimer:timer_ forMode:NSDefaultRunLoopMode];
        }
        else{
            [self payTimeUp];
        }
    }
}

+ (CGFloat)cellHeight
{
    return 150.f;
}

- (void)timerAction
{
    if (timeInterval_ <= 0) {
        [self payTimeUp];
    }
    else{
        NSArray *array = [AppUtils getTimeWithInterval:timeInterval_];
        NSString *timeStr = [NSString stringWithFormat:@"%@:%@", array[1], array[2]];
        NSString *tips = [NSString stringWithFormat:@"买家需在%@内完成付款", timeStr];
        NSRange range = [tips rangeOfString:timeStr];
        NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]initWithString:tips];
        [attributedStr addAttributes:@{NSForegroundColorAttributeName:kRedColor} range:range];
        self.timerLbl.attributedText = attributedStr;
        timeInterval_ --;
    }
}

- (void)dealloc
{
    if (timer_) {
        [timer_ invalidate];
        timer_ = nil;
    }
}

- (void)payTimeUp
{
    [timer_ invalidate]; //时间已到
    timer_ = nil;
    self.timerLbl.hidden = YES;
    self.tipLbl.hidden = YES;
    self.orderClosedLbl.text = [AppUtils localizedProductString:@"QM_Text_PayTimeUp"];
    self.orderClosedLbl.hidden = NO;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(payTimeIsUp)]) {
        [self.delegate payTimeIsUp];
    }
}

@end
