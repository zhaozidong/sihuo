//
//  QMGoodsTitleCell.m
//  QMMM
//
//  Created by hanlu on 14-9-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsTitleCell.h"
#import "GoodEntity.h"
#import "AppUtils.h"
#import "UIImageView+QMWebCache.h"

NSString * const kQMGoodsTitleCellIndentifier = @"kQMGoodsTitleCellIndentifier";

@interface QMGoodsRelationView : UIView{
    NSString *titleText;
    NSString *friendId;
}

@property (nonatomic, strong) UIImageView * backgroundView;

@property (nonatomic, strong) UIImageView * relationImageView;;

@property (nonatomic, strong) UILabel * textLabel;

@property (nonatomic, assign) BOOL isFriend;

@property (nonatomic, assign) id<RelationViewTitleDelegate> delegate;

- (void)setText:(int)isFriend text:(NSString *)text userId:(NSString *)userId;

@end

@implementation QMGoodsRelationView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.clipsToBounds=YES;
    
    _backgroundView = [[UIImageView alloc] initWithFrame:self.bounds];
//    _backgroundView.backgroundColor = [UIColor redColor];
    UIImage * image = [UIImage imageNamed:@"relationFriendBg"];
    image = [image stretchableImageWithLeftCapWidth:15 topCapHeight:2];
//    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(2, 15, 15, 5)];
    _backgroundView.image=image;
    [self addSubview:_backgroundView];
    
    _textLabel = [[UILabel alloc] initWithFrame:CGRectInset(_backgroundView.frame, 5, 0)];
    _textLabel.userInteractionEnabled=YES;
    [_textLabel setFont:[UIFont systemFontOfSize:12.0]];
    [_textLabel setTextColor:[UIColor lightGrayColor]];
    [_textLabel setTextAlignment:NSTextAlignmentRight];
    [_textLabel setBackgroundColor:[UIColor clearColor]];
    [_textLabel setLineBreakMode:NSLineBreakByTruncatingMiddle];
    [self addSubview:_textLabel];

    
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer * tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textLabelClicked)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.numberOfTouchesRequired = 1;
    [_textLabel addGestureRecognizer:tapGestureRecognizer];
    
    
    _relationImageView=[[UIImageView alloc] init];
    [self addSubview:_relationImageView];
}

- (void)setText:(int)isFriend text:(NSString *)text userId:(NSString *)userId;
{
//    if(!isFriend) {
//        _textLabel.text = @"朋友";
//        self.hidden = NO;
//    } else {
//        if([text length] > 0) {
//            _textLabel.text = [NSString stringWithFormat:@"%@的朋友", text];
//             self.hidden = NO;
//        } else {
//            _textLabel.text = @"";
//            self.hidden = YES;
//        }
//    }
    
    if (0==isFriend) {//自己
        titleText=nil;
        _textLabel.text = @"";
        _relationImageView.image=nil;
        self.hidden = YES;
        _relationImageView.hidden=YES;
    }else if (1==isFriend){//朋友
        titleText=@"你们是朋友";
        _textLabel.text =titleText;
        _relationImageView.image=[UIImage imageNamed:@"relationMyFriend"];
        _relationImageView.hidden=NO;
        friendId=userId;
        self.hidden = NO;
    }else if(2==isFriend){//...的朋友
        titleText=[NSString stringWithFormat:@"%@的朋友", text];
        NSMutableAttributedString *strTitle = [[NSMutableAttributedString alloc] initWithString:titleText];
        [strTitle addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,[text length])];
        [strTitle addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x99a0aa) range:NSMakeRange([text length],[titleText length]-[text length])];
        _textLabel.attributedText=strTitle;
        _relationImageView.image=[UIImage imageNamed:@"relationOtherFriend"];
        _relationImageView.hidden=NO;
        self.hidden = NO;
        friendId=userId;
    }else if (3==isFriend){//...觉得很赞
        titleText=[NSString stringWithFormat:@"%@赞过它", text];
        
        NSMutableAttributedString *strTitle = [[NSMutableAttributedString alloc] initWithString:titleText];
        [strTitle addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,[text length])];
        [strTitle addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x99a0aa) range:NSMakeRange([text length],[titleText length]-[text length])];
        _textLabel.attributedText=strTitle;
        _relationImageView.image=[UIImage imageNamed:@"relationFeelGood"];
        _relationImageView.hidden=NO;
        self.hidden = NO;
        friendId=userId;
    }else{
        titleText=nil;
        _textLabel.text = @"";
        _relationImageView.image=nil;
        _relationImageView.hidden=YES;
        self.hidden = YES;
    }
    
    
    self.isFriend = isFriend;
    
    [self setNeedsLayout];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
//    UIImage * image = nil;
//    if(_isFriend) {
//        image = [UIImage imageNamed:@"friend_bkg"];
//    } else {
//        image = [UIImage imageNamed:@"relative_friend_bkg"];
//    }
//    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(8, 9, 7, 5)];
//    _backgroundView.frame = self.bounds;
//    _backgroundView.image = image;
    
    CGRect r;
    if (titleText) {
         r= [titleText boundingRectWithSize:CGSizeMake(kMainFrameWidth, 12)
                                             options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                          attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}
                                             context:nil];
    }
    
    CGRect frame = _textLabel.frame;
//    frame.origin.x = _backgroundView.frame.origin.x + 8;
//    frame.size.width = _backgroundView.frame.size.width - 12;
    
//    frame.size.width=r.size.width;
    
    
    frame.origin.x=self.bounds.size.width - r.size.width-10 < 20 ? 20 :self.bounds.size.width - r.size.width-10 ;
//    frame.size.width=r.size.width>kMainFrameWidth/2-30 ? kMainFrameWidth/2-30 : r.size.width;
//    frame.size.width=r.size.width > self.bounds.size.width-20 ? self.bounds.size.width-20 : r.size.width;
    if (r.size.width > self.bounds.size.width-20) {
        frame.size.width=self.bounds.size.width-30;
    }else{
        frame.size.width=r.size.width;
    }
    frame.size.height=50;
    _textLabel.frame = frame;
    
    frame.origin.x=_textLabel.frame.origin.x-20;
    frame.origin.y=_textLabel.frame.origin.y;
    frame.size.width=20;
    frame.size.height=20;
    _relationImageView.frame=frame;
    CGPoint center=_relationImageView.center;
    center.y=_textLabel.center.y;
    _relationImageView.center=center;
    
    
    
    frame=CGRectMake(_relationImageView.frame.origin.x-5, _relationImageView.frame.origin.y-5, _textLabel.frame.size.height+_relationImageView.frame.size.width+150, _relationImageView.frame.size.height+10);
    _backgroundView.frame=frame;
    
    
    
//    CGRect frame;
////    frame.origin.x =(kMainFrameWidth/2-30)-((kMainFrameWidth/2-30)-r.size.width);
//    frame.origin.x=kMainFrameWidth/2-30;
//    frame.size.width=r.size.width;
//    
//    _textLabel.frame=frame;
    
}
-(void)textLabelClicked{
    if (friendId && ![friendId isEqualToString:@""] && _delegate && [_delegate respondsToSelector:@selector(clickRelationTitleWithId:)]) {
        [_delegate clickRelationTitleWithId:friendId];
    }
}

@end

@implementation QMGoodsTitleCell{
    uint64_t sellerId;
}

@synthesize title = _title;
//@synthesize locationView = _locationView;
//@synthesize location = _location;

+ (id)cellAwakeFromNib
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMGoodsTitleCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

+ (NSUInteger)heightForGoodsTitleCell:(GoodEntity *)goodsInfo
{
    return 50;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Initialization code
    self.contentView.backgroundColor = [UIColor clearColor];
    
    [_headImageView setHeadViewStyle:QMRoundHeadViewSmall];
    [_headImageView setDelegate:self];
    
    [_title setText:@""];
//    [_title setTextColor:kTitleColor];
//    [_title setTextColor:kTitleColor];
    [_title setTextColor:UIColorFromRGB(0x4f4f4f)];
    [_title setFont:[UIFont systemFontOfSize:15.0]];
    [_title setBackgroundColor:[UIColor clearColor]];
    
    
//    [_location setText:@""];
//    [_location setTextColor:kGrayTextColor];
//    
//    [_locationView setImage:[UIImage imageNamed:@"location"]];
//    
//    [_timeLabel setTextColor:kGrayTextColor];
//    [_timeImageView setImage:[UIImage imageNamed:@"clock"]];
    
//    _relationView.backgroundColor=[UIColor orangeColor];
    
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer * tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickHeadViewWithUid)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGestureRecognizer];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(GoodEntity *)goodsInfo showFull:(BOOL)showFull
{
    sellerId=goodsInfo.sellerUserId;
    [self.headImageView setSmallImageUrl:goodsInfo.sellerHeadIcon];
    self.headImageView.userId = goodsInfo.sellerUserId;
    self.title.text = goodsInfo.sellerNick;

    [self.relationView setText:goodsInfo.is_friend text:goodsInfo.whose_friend userId:goodsInfo.friendId];
    self.relationView.delegate=self;
    
//    NSDate * date = [NSDate dateWithTimeIntervalSince1970:goodsInfo.pub_time];
//    NSString * stringDate = [AppUtils stringFromDate:date];
//    NSString * locationText = [NSString stringWithFormat:@"%@ | %@", goodsInfo.city, stringDate];
    
    
//    self.location.text = [goodsInfo.city length] > 0 ? goodsInfo.city : @"火星";
//    self.timeLabel.text = stringDate;
    
    if(showFull) {
        _roundBkgView.hidden = YES;
        self.contentView.backgroundColor = [UIColor whiteColor];
    } else {
        _roundBkgView.hidden = NO;
        self.contentView.backgroundColor = [UIColor clearColor];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    UIImage * image = [UIImage imageNamed:@"top_half_round_bkg"];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(5, 9, 2, 9)];
    _roundBkgView.frame = self.contentView.bounds;
    _roundBkgView.image = image;
    
    CGRect frame = _title.frame;
    CGSize textSize = [_title.text sizeWithFont:_title.font constrainedToSize:CGSizeMake(131, 15)];
    frame.size.width = textSize.width;
    _title.frame = frame;
    
    frame = _relationView.frame;
    textSize = [_relationView.textLabel.text sizeWithFont:_relationView.textLabel.font constrainedToSize:CGSizeMake(75, 17)];
//    frame.origin.x = _title.frame.origin.x + _title.frame.size.width + 7;
//    frame.origin.x = _title.frame.origin.x + _title.frame.size.width + 80;
    
//    frame.origin.x =self.contentView.frame.size.width-100;
//    frame.origin.y =_title.frame.origin.y+3;
//    frame.origin.x =self.contentView.frame.size.width-(textSize.width+ 6 * 2)-30;
    frame.origin.x =self.contentView.frame.size.width/2;
    frame.origin.y=0;
//    frame.size.width = textSize.width + 6 * 2;
    frame.size.width=self.contentView.frame.size.width/2;
    frame.size.height=50;
    _relationView.frame = frame;
    
    
    CGPoint center=_relationView.center;
    center.y=_title.center.y;
    _relationView.center=center;

    
    
//    CGSize textSize = [_relationView.textLabel.text sizeWithFont:_relationView.textLabel.font constrainedToSize:CGSizeMake(75, 17)];
//    frame.origin.x = frame.origin.x + frame.size.width - (textSize.width + 6 * 2);
//    frame.size.width = textSize.width + 6 * 2;
//    _relationView.frame = frame;
    
    
    
    
//    frame = _location.frame;
//    textSize = [_location.text sizeWithFont:_location.font constrainedToSize:CGSizeMake(138, 13)];
//    frame.size.width = textSize.width;
//    _location.frame = frame;
//    
//    frame = _timeImageView.frame;
//    frame.origin.x = _location.frame.origin.x + _location.frame.size.width + 5;
//    _timeImageView.frame = frame;
//    
//    frame = _timeLabel.frame;
//    frame.origin.x = _timeImageView.frame.origin.x + _timeImageView.frame.size.width + 5;
//    frame.size.width = self.contentView.bounds.size.width - 12 - frame.origin.x;
//    _timeLabel.frame = frame;
}

- (void)clickHeadViewWithUid
{
    if (sellerId !=0 && self.delegate && [self.delegate respondsToSelector:@selector(clickHeadViewWithUid:)]) {
        [self.delegate clickHeadViewWithUid:sellerId];
    }
}

- (void)clickHeadViewWithUid:(uint64_t)userId
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickHeadViewWithUid:)]) {
        [self.delegate clickHeadViewWithUid:userId];
    }
}

-(void)clickRelationTitleWithId:(NSString *)friendId{
    NSLog(@"textLabelClicked friendid=%@",friendId);
    if (_delegateRelation && [_delegateRelation respondsToSelector:@selector(clickRelationTitleWithId:)]) {
        [self.delegateRelation clickRelationTitleWithId:friendId];
    }
    
}

@end
