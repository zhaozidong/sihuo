//
//  SystemHandler.m
//  QMMM
//
//  Created by kingnet  on 14-10-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "SystemHandler.h"
#import "SystemApi.h"
#import "VersionInfo.h"
#import "WXApi.h"
#import "UserDefaultsUtils.h"

@interface SystemHandler ()
{
    SystemApi *systemApi_;
}
@end
@implementation SystemHandler

- (instancetype)init
{
    if (self = [super init]) {
        systemApi_ = [[SystemApi alloc] init];
    }
    return self;
}

+ (SystemHandler *)sharedInstance
{
    static SystemHandler *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)checkVersion:(SuccessBlock)success failed:(FailedBlock)failed
{
    [systemApi_ getAppVersion:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = [resultDic objectForKey:@"d"];
            if (dataDic && [dataDic isKindOfClass:[NSDictionary class]]) {
                VersionInfo *info = [[VersionInfo alloc] initWithDictionary:dataDic];
                if (success) {
                    success(info);
                }
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)submitFeedback:(NSString *)content type:(int)type success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [systemApi_ submitFeedback:content type:type context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)shareToWeiXin:(uint)type appInfo:(AppInfo *)appInfo
{
    //微信是否安装
    if(![WXApi isWXAppInstalled]) {
        [AppUtils showAlertMessage:@"未安装微信，分享失败"];
        return;
    }
    
    WXMediaMessage *message = [WXMediaMessage message];
//    message.title = title;
//    message.description = desc;
//    [message setThumbImage:thumbImage];
    
    WXWebpageObject *ext = [WXWebpageObject object];
//    ext.webpageUrl = [AppUtils getAbsolutePath:webpageUrl];
    message.mediaObject = ext;
    
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = (0 == type) ? WXSceneSession : WXSceneTimeline;
    
    [WXApi sendReq:req];
    
    if(type == 0) {
        //统计分享到微信次数
        [AppUtils trackCustomEvent:@"share_weixin_event" args:nil];
    } else {
        //统计分享到微信朋友圈次数
        [AppUtils trackCustomEvent:@"share_weixin_friend_event" args:nil];
    }
}

- (void)reportDeviceInfo{
    [systemApi_ reportDevInfoWithContext:^(id result) {
         if ([result[@"s"] intValue] == 0) {
             DLog(@"上报设备信息成功");
             [UserDefaultsUtils saveBoolValue:YES withKey:@"reportDeviceInfo"];
         }else{
             DLog(@"上报设备信息失败");
         }
    }];
}

- (void)reportUseTime:(NSString *)time{
    [systemApi_ reportUseTime:time Context:^(id result) {
        if ([result[@"s"] intValue] == 0) {
//            DLog(@"report use time success");
//TODO:设置当前使用时间为0
            DLog(@"上报使用时间成功");
        }else{
            DLog(@"上报使用时间失败");
        }
    }];
}

- (void)reportRegisterStep:(NSString *)step{
    [systemApi_ reportCurrentPage:step contex:^(id result) {
        if ([result[@"s"] intValue] == 0) {
            DLog(@"上报当前页面成功");
        }else{
            DLog(@"上报当前页面失败");
        }
    }];
}

@end
