//
//  QMRemarkContactCell.h
//  QMMM
//
//  Created by hanlu on 14-11-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMRemarkContactCellIdentifier;

@interface QMRemarkContactCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UITextField * textField;

+ (id)cellForRemarkContact;

@end
