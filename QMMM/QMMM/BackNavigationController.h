//
//  BackNavigationController.h
//  QMMM
//
//  Created by kingnet  on 14-10-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackNavigationController : UINavigationController<UIGestureRecognizerDelegate>

@end
