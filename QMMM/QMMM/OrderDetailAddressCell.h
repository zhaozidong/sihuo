//
//  OrderDetailAddressCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class OrderDataInfo;

extern NSString * const kOrderDetailAddressCellIdentifier;

@interface OrderDetailAddressCell : UITableViewCell

+ (UINib *)nib;

+ (OrderDetailAddressCell *)addressCell;

- (void)updateUI:(OrderDataInfo *)orderInfo;

+ (CGFloat)cellHeight;

@end
