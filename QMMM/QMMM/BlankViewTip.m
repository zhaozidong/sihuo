//
//  BlankViewTip.m
//  QMMM
//
//  Created by kingnet  on 14-11-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BlankViewTip.h"

@interface BlankViewTip()
@property (weak, nonatomic) IBOutlet UILabel *msgLbl;

@end
@implementation BlankViewTip

- (id)initWithMsg:(NSString *)msg imageName:(NSString *)imageName
{
    self = [[[NSBundle mainBundle]loadNibNamed:@"BlankViewTip" owner:self options:0] lastObject];
    if (self) {
        self.msgLbl.textColor = kGrayTextColor;
        self.msgLbl.text = msg;
    }
    return self;
}

- (void)showInView:(UIView *)containerView
{
    CGSize containerS = containerView.frame.size;
    self.frame = CGRectMake((containerS.width-[[self class]viewSize].width)/2, (containerS.height-[[self class]viewSize].height)/2, [[self class]viewSize].width, [[self class]viewSize].height);
    [containerView addSubview:self];
}

+ (CGSize)viewSize
{
    return CGSizeMake(100, 100);
}

@end
