//
//  KL_ImageZoomView.m
//  ShowImgDome
//
//  Created by chuliangliang on 14-9-28.
//  Copyright (c) 2014年 aikaola. All rights reserved.
//

#import "KL_ImageZoomView.h"
#import "UIImageView+QMWebCache.h"
#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/SDImageCache.h>

#define HandDoubleTap 2
#define HandOneTap 1
#define MaxZoomScaleNum 5.0
#define MinZoomScaleNum 1.0
#define ActionSheetTag 1000
@implementation KL_ImageZoomView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self _initView];
    }
    return self;
}

//获取图片和显示视图宽度的比例系数
- (float)getImgWidthFactor {
   return   self.bounds.size.width / self.image.size.width;
}
//获取图片和显示视图高度的比例系数
- (float)getImgHeightFactor {
    return  self.bounds.size.height / self.image.size.height;
}

//获获取尺寸
- (CGSize)newSizeByoriginalSize:(CGSize)oldSize maxSize:(CGSize)mSize
{
    if (oldSize.width <= 0 || oldSize.height <= 0) {
        return CGSizeZero;
    }
    
    CGSize newSize = CGSizeZero;
    if (oldSize.width > mSize.width || oldSize.height > mSize.height) {
        //按比例计算尺寸
        float bs = mSize.width / oldSize.width;
        float newHeight = oldSize.height * bs;
        newSize = CGSizeMake(mSize.width, newHeight);
        
        if (newHeight > mSize.height) {
            bs = mSize.height / oldSize.height;
            float newWidth = oldSize.width * bs;
            newSize = CGSizeMake(newWidth, mSize.height);
        }
    }
    //比最大的宽高都小
    else {
        
        newSize = oldSize;
    }
    return newSize;
}

- (CGSize)sizeFatBorderWidth:(CGSize)mSize oldSize:(CGSize)oldSize
{
    if (oldSize.width <= 0 || oldSize.height <= 0) {
        return CGSizeZero;
    }
    
    CGSize newSize = CGSizeZero;
    //按比例计算尺寸
    float bs = mSize.width / oldSize.width;
    float newHeight = oldSize.height * bs;
    newSize = CGSizeMake(mSize.width, newHeight);
    
    if (newHeight > mSize.height) {
        bs = mSize.height / oldSize.height;
        float newWidth = oldSize.width * bs;
        newSize = CGSizeMake(newWidth, mSize.height);
    }
    return newSize;
}

- (void)_initView {
    
    self.backgroundColor = [UIColor clearColor];
    self.clipsToBounds = YES;
    self.contentMode = UIViewContentModeScaleAspectFill;
    
    
    
    _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    _scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.delegate = self;
    _scrollView.bounces = NO;
    _scrollView.backgroundColor = [UIColor clearColor];
    _containerView = [[UIView alloc] initWithFrame:self.bounds];
    _containerView.backgroundColor = [UIColor clearColor];
    [_scrollView addSubview:_containerView];
    
    [self addSubview:_scrollView];

    _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    [_containerView addSubview:_imageView];
    
    //双击
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapsAction:)];
    [doubleTapGesture setNumberOfTapsRequired:HandDoubleTap];
    [self addGestureRecognizer:doubleTapGesture];
    
    //长按
    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(LongPressAction:)];
    longGesture.minimumPressDuration = 0.3;
    [self addGestureRecognizer:longGesture];
    
    //单击
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapsAction:)];
    [tapGesture setNumberOfTapsRequired:HandOneTap];
    [self addGestureRecognizer:tapGesture];
    
    //双击失败之后执行长按
    [doubleTapGesture requireGestureRecognizerToFail:longGesture];
    
    //长按之后执行单击
    [tapGesture requireGestureRecognizerToFail:doubleTapGesture];

    self.progress = [[MACircleProgressIndicator alloc]initWithFrame:CGRectMake(0, 0, kDefaultProgressSize, kDefaultProgressSize)];
    self.progress.strokeWidth = 4;
    self.progress.value = 0.0;
    self.progress.center = self.center;
    self.progress.color = [UIColor whiteColor];
    [self addSubview:self.progress];
    
    self.scrollView.maximumZoomScale = MaxZoomScaleNum;
    self.scrollView.minimumZoomScale = MinZoomScaleNum;
    self.scrollView.zoomScale = MinZoomScaleNum;

}

- (void)resetViewFrame:(CGRect)newFrame
{
    self.frame = newFrame;
    _scrollView.frame = self.bounds;
    _containerView.frame = self.bounds;
    
    CGSize vsize = self.frame.size;
    self.progress.hidden = NO;
    self.progress.frame = CGRectMake((vsize.width - kDefaultProgressSize) * 0.5, (vsize.height - kDefaultProgressSize) * 0.5, kDefaultProgressSize, kDefaultProgressSize);
}


#pragma mark- 手势事件
//单击 / 双击 手势
- (void)TapsAction:(UITapGestureRecognizer *)tap
{
    NSInteger tapCount = tap.numberOfTapsRequired;
    if (HandDoubleTap == tapCount) {
        //双击
        if (self.scrollView.minimumZoomScale <= self.scrollView.zoomScale && self.scrollView.maximumZoomScale > self.scrollView.zoomScale) {
            [self.scrollView setZoomScale:self.scrollView.maximumZoomScale animated:YES];
        }else {
            [self.scrollView setZoomScale:self.scrollView.minimumZoomScale animated:YES];
        }
        
    }else if (HandOneTap == tapCount) {
        //单击
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowImageOneTapNotity object:nil];
//        NSLog(@"imgUrl: %@, imgSize:(%f, %f) zoomScale:%f",downImgUrl,self.imageView.frame.size.width,self.imageView.frame.size.height,self.scrollView.zoomScale);
        
    }
}

- (void)LongPressAction:(UILongPressGestureRecognizer *)longPress
{
    if (longPress.state == UIGestureRecognizerStateBegan) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"保存图片", nil];
        actionSheet.tag = ActionSheetTag;
        [actionSheet showInView:self.superview.superview];
    }
}

#pragma mark- Properties

- (UIImage *)image {
    return _imageView.image;
}

- (void)setImage:(UIImage *)image {
    if(self.imageView == nil){
        self.imageView = [UIImageView new];
        self.imageView.clipsToBounds = YES;
    }
    self.imageView.image = image;
}

//本地图片
- (void)updateImage:(NSString *)imgName {
    self.scrollView.scrollEnabled = YES;
    self.image = [UIImage imageNamed:imgName];
    [self setImageViewWithImg:self.image];
}
//网络图片
- (void)uddateImageWithUrl:(NSString *)imgUrl smallImgUrl:(NSString *)smallImgUrl
{
    self.scrollView.scrollEnabled = NO;
    self.imageView.image = nil;
    
    //如果已经有图片了，则直接显示
    UIImage *tempImg = [[SDImageCache sharedImageCache]imageFromDiskCacheForKey:imgUrl];
    if (tempImg) {
        [self setImageViewWithImg:tempImg];
        return;
    }
    
    //如果路径为空显示失败的图片
    if (nil==imgUrl || [imgUrl isEqualToString:@""]) {
        if (self.isLookLifePhoto) {
            [self setImageViewWithImg:[UIImage imageNamed:@"headPlaceholder_big"]];
        }
        else{
            [self setImageViewWithImg:[UIImage imageNamed:@"goodImgFailed_big"]];
        }
        
        return;
    }
    
    //判断是否为本地路径
    if ([[NSFileManager defaultManager]fileExistsAtPath:imgUrl]) {
        UIImage *localImg = [[UIImage alloc]initWithContentsOfFile:imgUrl];
        [self setImageViewWithImg:localImg];
        return;
    }
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    KL_ImageZoomView __weak * weakSelf = self;
    
    if (smallImgUrl && ![smallImgUrl isEqualToString:@""]) {
        UIImage *cachedImg = [[SDImageCache sharedImageCache]imageFromDiskCacheForKey:smallImgUrl];
        if (cachedImg) {
            self.image = cachedImg;
            float scale = [UIScreen mainScreen].scale;
            CGSize imgSize;
            if (scale <= cachedImg.scale) {
                imgSize = cachedImg.size;
            }
            else{
                imgSize = CGSizeMake(cachedImg.size.width*cachedImg.scale/scale, cachedImg.size.height*cachedImg.scale/scale);
            }
            CGSize showSize = [self newSizeByoriginalSize:imgSize maxSize:self.bounds.size];
            self.imageView.frame = CGRectMake(0, 0, showSize.width, showSize.height);
            self.containerView.bounds = _imageView.bounds;
        }
    }
    
    downImgUrl = imgUrl;

    if (!imgUrl) {
        return;
    }
    DLog(@"down url:%@", downImgUrl);
    self.progress.hidden = NO;
    
    [manager downloadImageWithURL:[NSURL URLWithString:downImgUrl] options:SDWebImageProgressiveDownload | SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        if(expectedSize > 0){
            weakSelf.progress.value = (float) receivedSize/expectedSize;
        }
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        if (finished) {
            //保证下载完在设置图像时，是当前正在显示的图像
            
            if ([imageURL.absoluteString isEqualToString:downImgUrl]) {
                if (image) {
                    weakSelf.progress.value = 1.0;
                    weakSelf.progress.hidden = YES;
                    
                    [weakSelf setImageViewWithImg:image];
                }
                else{
                    if (error) {
                        DLog(@"error:%@", error);
                        if (self.isLookLifePhoto) {
                            [weakSelf setImageViewWithImg:[UIImage imageNamed:@"headPlaceholder_big"]];
                        }
                        else{
                            [weakSelf setImageViewWithImg:[UIImage imageNamed:@"goodImgFailed_big"]];
                        }
                    }
                }
            }
        }
    }];
}

- (void)setImageViewWithImg:(UIImage *)img {
    self.scrollView.scrollEnabled = YES;
    self.progress.hidden = YES;
    self.progress.value = 0.0;
    
    if (self.image == nil) {
        self.imageView.image = img;
        float scale = [UIScreen mainScreen].scale;
        CGSize imgSize;
        if (scale <= img.scale) {
            imgSize = img.size;
        }
        else{
            imgSize = CGSizeMake(img.size.width*img.scale/scale, img.size.height*img.scale/scale);
        }

        CGSize showSize = [self sizeFatBorderWidth:self.bounds.size oldSize:imgSize];
        self.imageView.frame = CGRectMake(0, 0, showSize.width, showSize.height);
        
        _scrollView.zoomScale = 1;
        _scrollView.contentOffset = CGPointZero;
        _containerView.bounds = _imageView.bounds;
        _scrollView.zoomScale  = _scrollView.minimumZoomScale;
        [self scrollViewDidZoom:_scrollView];
        return;
    }
    
    self.imageView.image = img;
    
    //动画
    [UIView beginAnimations:nil context:nil];
    //动画结束时变缓慢
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationRepeatCount:1];
    
    CGSize showSize = [self sizeFatBorderWidth:self.bounds.size oldSize:img.size];
    self.imageView.frame = CGRectMake(0, 0, showSize.width, showSize.height);
    
    _scrollView.zoomScale = 1;
    _scrollView.contentOffset = CGPointZero;
    _containerView.bounds = _imageView.bounds;
    _scrollView.zoomScale  = _scrollView.minimumZoomScale;
    [self scrollViewDidZoom:_scrollView];
    
    [UIView commitAnimations];
}

- (BOOL)isViewing {
    return (_scrollView.zoomScale != _scrollView.minimumZoomScale);
}

#pragma mark- Scrollview delegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _containerView;
}


- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    CGFloat Ws = _scrollView.frame.size.width - _scrollView.contentInset.left - _scrollView.contentInset.right;
    CGFloat Hs = _scrollView.frame.size.height - _scrollView.contentInset.top - _scrollView.contentInset.bottom;
    CGFloat W = _containerView.frame.size.width;
    CGFloat H = _containerView.frame.size.height;
    
    CGRect rct = _containerView.frame;
    rct.origin.x = MAX((Ws-W)*0.5, 0);
    rct.origin.y = MAX((Hs-H)*0.5, 0);
    _containerView.frame = rct;
    
}

#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.cancelButtonIndex == buttonIndex) {
        return;
    }
    if (actionSheet.tag == ActionSheetTag) {
        //保存图片到相册
        if (self.image) {
            UIImageWriteToSavedPhotosAlbum(self.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        }
    }
}

#pragma mark - Saved Image Callback
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    [AppUtils showSuccessMessage:@"已保存到相册"];
}

@end
