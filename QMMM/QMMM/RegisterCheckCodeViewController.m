//
//  RegisterCheckCodeViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "RegisterCheckCodeViewController.h"
#import "LRCheckCodeViewController.h"
#import "UserInfoHandler.h"
#import "AppDelegate.h"

@interface RegisterCheckCodeViewController ()<LRCheckCodeViewDelegate>
{
    NSString *phoneNum_;
    NSString *pwd_;
}
@property (nonatomic, strong) LRCheckCodeViewController *checkCodeController;

@end

@implementation RegisterCheckCodeViewController

- (id)initWithPhoneNum:(NSString *)phoneNum pwd:(NSString *)pwd
{
    self = [super init];
    if (self) {
        phoneNum_ = phoneNum;
        pwd_ = pwd;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"注册";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    self.canTapCloseKeyboard = YES;
    
    self.checkCodeController = [[LRCheckCodeViewController alloc]init];
    self.checkCodeController.submitBtnTitle = @"完成";
    self.checkCodeController.delegate = self;
    
    [self addChildViewController:self.checkCodeController];
    
    [[self view] addSubview:[self.checkCodeController view]];
    
    [self.checkCodeController didMoveToParentViewController:self];
    
    [self sendSMS];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickSubmit:(NSString *)checkCode
{
    //统计点击注册完成按钮的次数
    [AppUtils trackCustomEvent:@"register_edit_userInfo" args:nil];
    [AppUtils closeKeyboard];
    
    [AppUtils showProgressMessage:[AppUtils localizedPersonString:@"QM_HUD_SigningUp"]];
    
    __weak typeof(self) weakSelf = self;
    [[UserInfoHandler sharedInstance] executeRegisterWithMobile:phoneNum_ password:pwd_ checkCode:checkCode success:^(id obj) {
        [AppUtils dismissHUD];
        
        //统计由注册进入到编辑资料页面
        [AppUtils trackCustomEvent:@"register_startApp" args:nil];
        
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate startApp];
        
    } failed:^(id obj) {
        //拿到错误代码
        NSRange range = [(NSString *)obj rangeOfString:@"(" options:NSBackwardsSearch];
        if (range.location != NSNotFound) {
            NSString *str = [(NSString *)obj substringFromIndex:range.location+1];
            NSString *str1 = [str substringToIndex:str.length-1];
            if ([str1 intValue] == 1004) {
                //手机号码已被注册
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.0*NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^{
                    [weakSelf backToRoot:YES];
                });
            }
            else if ([str1 intValue] == 1000){
                //手机号码不正确
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.0*NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^{
                    [weakSelf backToRoot:NO];
                });
            }
        }
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)clickGetCheckCode
{
    [self sendSMS];
    [self.checkCodeController startTimer];
}

- (void)sendSMS
{
    __weak typeof(self) weakSelf = self;
    [[UserInfoHandler sharedInstance] sendSMSWithMobile:phoneNum_ type:@"register" success:^(id obj) {
//        [AppUtils showAlertMessage:[NSString stringWithFormat:@"验证码：%@(仅供测试用)", (NSString *)obj]];
        NSLog(@"check code:%@", obj);
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        //重置button
        [weakSelf.checkCodeController stopTimer];
        //拿到错误代码
        NSRange range = [(NSString *)obj rangeOfString:@"(" options:NSBackwardsSearch];
        if (range.location != NSNotFound) {
            NSString *str = [(NSString *)obj substringFromIndex:range.location+1];
            NSString *str1 = [str substringToIndex:str.length-1];
            if ([str1 intValue] == 1203) {
                //手机号码已被注册
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.0*NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^{
                    [weakSelf backToRoot:YES];
                });
            }
            else if ([str1 intValue] == 1202){
                //手机号码不正确
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.0*NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^{
                    [weakSelf backToRoot:NO];
                });
            }
        }
    }];
}

- (void)backToRoot:(BOOL)isToRoot
{
    if (isToRoot) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
