//
//  FillUserInfoViewController.h
//  QMMM
//
//  Created by kingnet  on 14-9-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@interface FillUserInfoViewController : BaseViewController

- (id)initWithPhoneNum:(NSString *)phoneNum checkCode:(NSString *)checkCode pwd:(NSString *)pwd;

@end
