//
//  CloNetworkUtil.h
//  QMMM
//
//  Created by kingnet  on 14-9-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface CloNetworkUtil : NSObject

- (Reachability *)reachability;
- (BOOL)getNetWorkStatus;
- (NSString *)getNetWorkType;

@end

