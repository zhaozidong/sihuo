//
//  SpecialGoodsViewController.h
//  QMMM
//  专题精选
//  Created by kingnet  on 14-12-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@class ActivityEntity;

@interface SpecialGoodsViewController : BaseViewController

- (id)initWithActivity:(ActivityEntity *)activity;

@end
