//
//  LRCheckCodeViewController.h
//  QMMM
//
//  Created by kingnet  on 14-11-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@protocol LRCheckCodeViewDelegate <NSObject>

- (void)clickSubmit:(NSString *)checkCode;

- (void)clickGetCheckCode;

@end
@interface LRCheckCodeViewController : BaseViewController

@property (nonatomic, strong) NSString *submitBtnTitle;

@property (nonatomic, weak) id<LRCheckCodeViewDelegate> delegate;

- (void)stopTimer;

- (void)startTimer;

@end
