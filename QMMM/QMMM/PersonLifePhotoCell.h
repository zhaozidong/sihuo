//
//  PersonLifePhotoCell.h
//  QMMM
//
//  Created by kingnet  on 14-11-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kPersonLifePhotoCellIdentifier;

@interface PersonLifePhotoCell : UICollectionViewCell

@property (nonatomic, strong) NSString *filePath;

@property (nonatomic, assign) BOOL showAddBtn;

@end
