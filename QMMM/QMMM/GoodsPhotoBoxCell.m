//
//  GoodsPhotoBoxCell.m
//  QMMM
//
//  Created by kingnet  on 14-11-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsPhotoBoxCell.h"
#import "PhotoCell.h"
#import "PhotoPreviewView.h"
#import "UploadStatusInfo.h"

@interface GoodsPhotoBoxCell ()<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PhotoPreviewViewDelegate>
{
    NSInteger mainImageIndex_;
    __block NSMutableDictionary *keyAndFilePath_; //存放已经上传的图片key path
    NSMutableArray *uploadInfoArr_; //存放上传状态的数组，用于控制cell重用出现的问题
}
@property (weak, nonatomic) IBOutlet UICollectionView *colloctionView;
@property (weak, nonatomic) IBOutlet UIButton *tipButton;



@end

const NSInteger kGoodsPhotoNum = 10;

@implementation GoodsPhotoBoxCell

+ (GoodsPhotoBoxCell *)goodsPhotoBoxCell
{
    return [[[NSBundle mainBundle]loadNibNamed:@"GoodsPhotoBoxCell" owner:self options:0] lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.isShowMainPic=NO;
    self.backgroundColor = [UIColor clearColor];
    self.colloctionView.delegate = self;
    self.colloctionView.dataSource = self;
    self.colloctionView.scrollEnabled = YES;
    self.colloctionView.backgroundColor = [UIColor clearColor];
    [self.colloctionView registerClass:[PhotoCell class] forCellWithReuseIdentifier:kPhotoCellIdentifier];
    self.tipButton.layer.borderColor = [kBorderColor CGColor];
    self.tipButton.layer.borderWidth = 0.5;
    [self.tipButton addTarget:self action:@selector(didTapTidBtn:) forControlEvents:UIControlEventTouchUpInside];
    self.tipButton.hidden = NO;
    self.colloctionView.hidden = YES;
    uploadInfoArr_ = [NSMutableArray array];
    keyAndFilePath_ = [NSMutableDictionary dictionary];
    for (int i=0; i<kGoodsPhotoNum; i++) {
        [uploadInfoArr_ addObject:[NSNull null]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight
{
    return 83.f;
}

- (NSMutableArray *)photoArray
{
    if (!_photoArray) {
        _photoArray = [[NSMutableArray alloc] initWithCapacity:kGoodsPhotoNum];
    }
    return _photoArray;
}

- (void)addPhotos:(NSArray *)photos isServerPhoto:(BOOL)isServerPhoto
{
    if (self.colloctionView.hidden) {
        self.tipButton.hidden = YES;
        self.colloctionView.hidden = NO;
    }
    
    //是服务器上的路径就不需要再下载了
    if (isServerPhoto) {
        for (int i=(int)self.photoArray.count; i<photos.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            UploadStatusInfo *statusInfo = [[UploadStatusInfo alloc]initWithFilePath:@""];
            statusInfo.indexPath = indexPath;
            statusInfo.status = UploadStatusSuccess;
            [uploadInfoArr_ replaceObjectAtIndex:i withObject:statusInfo];
        }
    }
    [self.photoArray addObjectsFromArray:photos];
    [self.colloctionView reloadData];
    //延迟几秒执行
    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, 1* NSEC_PER_SEC);
    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
        NSInteger item = [self.colloctionView numberOfItemsInSection:0] - 1 ;
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:0];
        [self.colloctionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
    });
}


#pragma mark - Button Actions
- (void)didTapTidBtn:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(showCamera)]) {
        [self.delegate showCamera];
    }
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([self.photoArray count] == kGoodsPhotoNum) {
        return [self.photoArray count];
    }
    return [self.photoArray count]+1; //1个是AddButton
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoCell *cell = (PhotoCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kPhotoCellIdentifier forIndexPath:indexPath];
    if (indexPath.row == self.photoArray.count) {
        cell.isHaveAddBtn = YES;
        GoodsPhotoBoxCell __weak * weakSelf = self;
        cell.addPhotoBlock = ^{
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(showCamera)]) {
                [weakSelf.delegate showCamera];
            }
        };
    }
    else{
        cell.isHaveAddBtn = NO;
        NSString *path = [self.photoArray objectAtIndex:indexPath.row];
        cell.filePath = path;
        if (_isShowMainPic && indexPath.row == mainImageIndex_) {
            cell.isMainPhoto = YES;
        }
        else{
            cell.isMainPhoto = NO;
        }
        
        dispatch_queue_t myQueue = dispatch_queue_create(kMyGlobalQueueId, NULL);
        dispatch_async(myQueue, ^{
            id temp = [uploadInfoArr_ objectAtIndex:indexPath.row];
            UploadStatusInfo *statusInfo;
            if ([temp isKindOfClass:[NSNull class]]) {
                statusInfo = [[UploadStatusInfo alloc]initWithFilePath:path];
                statusInfo.indexPath = indexPath;
                [uploadInfoArr_ replaceObjectAtIndex:indexPath.row withObject:statusInfo];
            }
            else{
                statusInfo = (UploadStatusInfo *)temp;
            }
            
            if (statusInfo.status!=UploadStatusUploading && statusInfo.status!=UploadStatusSuccess && statusInfo.status!= UploadStatusFailed) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [cell prepareUpload];
                });
                [statusInfo uploadImageProgress:^(float progress){
                    PhotoCell *photoCell = (PhotoCell *)[collectionView cellForItemAtIndexPath:statusInfo.indexPath];
                    if (photoCell) {
                        //更新进度
                        photoCell.progress = progress;
                    }
                } success:^(id obj) {
                    [keyAndFilePath_ setObject:(NSString *)obj forKey:statusInfo.filePath];
                    //上传成功
                    PhotoCell *photoCell = (PhotoCell *)[collectionView cellForItemAtIndexPath:statusInfo.indexPath];
                    if (photoCell) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [photoCell uploadSuccess];
                        });
                    }
                } failed:^(id obj) {
                    //上传是失败
                    PhotoCell *photoCell = (PhotoCell *)[collectionView cellForItemAtIndexPath:statusInfo.indexPath];
                    if (photoCell) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [photoCell uploadFailed];
                        });
                    }
                }];
            }
        });
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [AppUtils closeKeyboard];
    if (indexPath.row < self.photoArray.count) {
        PhotoCell *cell = (PhotoCell *)[collectionView cellForItemAtIndexPath:indexPath];
        UploadStatusInfo *statusInfo = [uploadInfoArr_ objectAtIndex:indexPath.row];
        if ([statusInfo isKindOfClass:[UploadStatusInfo class]]) {
            if (statusInfo.status == UploadStatusUploading) {
                [AppUtils showErrorMessage:@"正在上传"];
            }
            else if (statusInfo.status == UploadStatusFailed){
                //重新上传
                [cell prepareUpload];
                [statusInfo uploadImageProgress:^(float progress){
                    PhotoCell *photoCell = (PhotoCell *)[collectionView cellForItemAtIndexPath:statusInfo.indexPath];
                    if (photoCell) {
                        //更新进度
                        photoCell.progress = progress;
                    }
                } success:^(id obj) {
                    [keyAndFilePath_ setObject:(NSString *)obj forKey:statusInfo.filePath];
                    //上传成功
                    PhotoCell *photoCell = (PhotoCell *)[collectionView cellForItemAtIndexPath:statusInfo.indexPath];
                    [photoCell uploadSuccess];
                } failed:^(id obj) {
                    //上传是失败
                    PhotoCell *photoCell = (PhotoCell *)[collectionView cellForItemAtIndexPath:statusInfo.indexPath];
                    [photoCell uploadFailed];
                }];
            }
            else if (statusInfo.status == UploadStatusSuccess){
                PhotoPreviewView *preview = [PhotoPreviewView photoPreviewView];
                preview.delegate = self;
                NSString *filePath = (NSString *)[self.photoArray objectAtIndex:indexPath.row];
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                if ([fileManager fileExistsAtPath:filePath]) {
                    if ([fileManager fileExistsAtPath:[filePath stringByReplacingOccurrencesOfString:@"_small" withString:@""]]) {
                        filePath = [filePath stringByReplacingOccurrencesOfString:@"_small" withString:@""];
                    }else if([fileManager fileExistsAtPath:[filePath stringByReplacingOccurrencesOfString:@"_small" withString:@"_edit"]]){
                        filePath = [filePath stringByReplacingOccurrencesOfString:@"_small" withString:@"_edit"];
                    }
                }
                preview.imageSizeType = QMImageQuarterSize;
                preview.filePath = filePath;
                if (!_isShowMainPic) {
                    preview.isMainPhoto=NO;
                }else{
                    preview.isMainPhoto = (indexPath.row == mainImageIndex_);
                }

                preview.previewType = QMGoodsPreview;
                preview.indexPath = indexPath;
                [preview show];
            }
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [PhotoCell cellSize];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    //一排中每块之间的距离
//    CGSize size = [PhotoCell cellSize];
//    CGFloat w = (CGRectGetWidth(collectionView.frame)-(size.width*5+8*2))/4;
//    float iosV = [[[UIDevice currentDevice] systemVersion] floatValue];
//    if (iosV >= 8.0) {
//        return w;
//    }
    return 9;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 9.f; //每行之间的距离
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(9.f, 9.f, 9.f, 9.f);
}

#pragma mark - PhotoPreview Delegate
- (void)makeMainPhoto:(NSString *)imgFilePath
{
    if (imgFilePath) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:imgFilePath]) {
            if ([imgFilePath hasSuffix:@"_edit.jpg"]) {
                imgFilePath = [imgFilePath stringByReplacingOccurrencesOfString:@"_edit.jpg" withString:@"_small.jpg"];
            }else{
                //是本地的图片，拼接上_small
                imgFilePath = [imgFilePath stringByReplacingOccurrencesOfString:@".jpg" withString:@"_small.jpg"];
            }
        }
        NSInteger i = [self.photoArray indexOfObject:imgFilePath];
        [self.colloctionView performBatchUpdates:^{
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:mainImageIndex_ inSection:0];
            
            NSArray *array = [NSArray arrayWithObjects:indexPath, indexPath2, nil];
            mainImageIndex_ = i;
            [self.colloctionView reloadItemsAtIndexPaths:array];
        } completion:^(BOOL finished) {
            if (finished) {
                
            }
        }];
    }
}

- (void)deleteImageWithPath:(NSIndexPath *)indexPath
{
    if (indexPath) {
        [self.photoArray removeObjectAtIndex:indexPath.row];
        [uploadInfoArr_ removeObjectAtIndex:indexPath.row];
        [uploadInfoArr_ addObject:[NSNull null]];
        if (self.photoArray.count == 0) {
            self.colloctionView.hidden = YES;
            self.tipButton.hidden = NO;
        }
        else if (indexPath.row == mainImageIndex_){
            mainImageIndex_ = 0;
        }
        [self.colloctionView reloadData];
    }
}

- (NSString *)mainPhoto
{
    return [self.photoArray objectAtIndex:mainImageIndex_];
}

#pragma mark - 供外部调用
- (NSArray *)keysArray
{
    int i=0;
    NSMutableArray *mbArray = [NSMutableArray array];
    for (NSString *path in self.photoArray) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:path]) {
            NSString *key = [keyAndFilePath_ objectForKey:path];
            if (key && ![key isEqualToString:@""]) {
                if (i == mainImageIndex_) {//如果是主图则放在最前面
                    [mbArray insertObject:key atIndex:0];
                }
                else{
                    [mbArray addObject:key];
                }
            }
        }
        else{
            if (i == mainImageIndex_) {//如果是主图则放在最前面
                [mbArray insertObject:path atIndex:0];
            }
            else{
                [mbArray addObject:path];
            }
        }
        i++;
    }
    return mbArray;
}

- (BOOL)isImgAllUploaded
{
    BOOL flag = YES;
    for (id temp in uploadInfoArr_) {
        if ([temp isKindOfClass:[UploadStatusInfo class]]) {
            UploadStatusInfo *statusInfo = (UploadStatusInfo *)temp;
            if (statusInfo.status == UploadStatusUploading) {
                flag = NO;
                break;
            }
        }
    }
    return flag;
}

- (NSArray *)bigImgLocalPath{
    NSMutableArray *array = [NSMutableArray array];
    int i=0;
    for (NSString *filePath in _photoArray) {
        NSString *bigS= [filePath stringByReplacingOccurrencesOfString:@"_small" withString:@""];
        NSString *editPic=[filePath stringByReplacingOccurrencesOfString:@"_small" withString:@"_edit"];
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        if ([fileMgr fileExistsAtPath:bigS]) {
            if (i == mainImageIndex_) {
                [array insertObject:bigS atIndex:0];
            }
            else{
                [array addObject:bigS];
            }
        }
        else if ([fileMgr fileExistsAtPath:editPic]){
            if (i == mainImageIndex_) {
                [array insertObject:editPic atIndex:0];
            }
            else{
                [array addObject:editPic];
            }
        }
        else{
            //是服务器上的路径
            if (i == mainImageIndex_) {
                [array insertObject:filePath atIndex:0];
            }
            else{
                [array addObject:filePath];
            }
        }
        i++;
    }
    return array;
}

@end
