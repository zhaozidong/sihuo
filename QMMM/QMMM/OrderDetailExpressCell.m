//
//  OrderDetailExpressCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderDetailExpressCell.h"
#import "OrderDataInfo.h"
#import "HTCopyableLabel.h"

NSString * const kOrderDetailExpressCellIdentifier = @"kOrderDetailExpressCellIdentifier";

@interface OrderDetailExpressCell ()<HTCopyableLabelDelegate>
{
    NSString *expressNum_;
}
@property (weak, nonatomic) IBOutlet UILabel *expressNameLbl; //快递公司
@property (weak, nonatomic) IBOutlet HTCopyableLabel *expressNumLbl; //快递单号

@end

@implementation OrderDetailExpressCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"OrderDetailExpressCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.expressNumLbl.text = @"";
    self.expressNameLbl.text = @"";
    self.expressNameLbl.textColor = kLightBlueColor;
    self.expressNumLbl.textColor = kLightBlueColor;
    self.expressNumLbl.copyableLabelDelegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(OrderDataInfo *)orderInfo
{
    if (orderInfo.express_name.length > 0) {
        self.expressNameLbl.text = [NSString stringWithFormat:@"快递公司：%@", orderInfo.express_name];
    }
    else{
        self.expressNameLbl.text = @"快递公司：暂无";
    }
    
    if (orderInfo.express_num.length > 0) {
        self.expressNumLbl.text = [NSString stringWithFormat:@"快递单号：%@", orderInfo.express_num];
    }
    else{
        self.expressNumLbl.text = @"快递单号：暂无";
        self.expressNumLbl.copyingEnabled = NO;
    }
}

+ (CGFloat)cellHeight
{
    return 30.f;
}

#pragma mark - HTCopyableLabelDelegate
- (NSString *)stringToCopyForCopyableLabel:(HTCopyableLabel *)copyableLabel
{
    if (expressNum_ && expressNum_.length>0) {
        return expressNum_;
    }
    return @"";
}

- (CGRect)copyMenuTargetRectInCopyableLabelCoordinates:(HTCopyableLabel *)copyableLabel
{
    return copyableLabel.bounds;
}

@end
