//
//  QMFriendsRelationCell.m
//  QMMM
//
//  Created by Derek on 15/1/27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMFriendsRelationCell.h"


@implementation QMFriendsRelationCell

+ (QMFriendsRelationCell *)cellAwakeFromNIB{
    NSArray *array=[[NSBundle mainBundle] loadNibNamed:@"QMFriendsRelationCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib {
    // Initialization code
    self.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    _lblName.textColor = kDarkTextColor;
    
    self.headView.headViewStyle=QMRoundHeadViewSmall;
    _headView.delegate=self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateUIWithUserInfo:(UserDataInfo *)userInfo{
    [_headView setSmallImageUrl:userInfo.avatar];
    _lblName.text=userInfo.nickname;
}

- (void)clickHeadViewWithUid:(uint64_t)userId{
    if (_delgate && [_delgate respondsToSelector:@selector(clickCellWithUserId:)]) {
        [_delgate clickCellWithUserId:userId];
    }
}

@end
