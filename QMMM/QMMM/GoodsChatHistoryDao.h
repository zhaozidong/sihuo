//
//  GoodsChatHistoryDao.h
//  QMMM
//  记录是否发送过给卖家该商品信息
//  Created by kingnet  on 15-2-5.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseDao.h"

@interface GoodsChatHistoryDao : BaseDao

+ (GoodsChatHistoryDao *)sharedInstance;

- (BOOL)insertGoodsId:(uint64_t)goodsId;

- (BOOL)isHaveGoodsId:(uint64_t)goodsId;

@end
