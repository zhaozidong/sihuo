//
//  QMPinYin.h
//  QMMM
//
//  Created by hanlu on 14-11-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QMPinYin : NSObject

+ (NSString *)getPinYin:(NSString *)value hasSymbol:(BOOL)hasSymbol;

@end
