//
//  QMCategoryCellDetail.h
//  QMMM
//
//  Created by Derek on 15/3/17.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityInfo.h"
@interface QMCategoryCellDetail : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *left;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *right;

-(void)updateWithCategory:(CategoryEntity *)category;

@end
