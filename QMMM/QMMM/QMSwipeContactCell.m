//
//  QMSwipeContactCell.m
//  QMMM
//
//  Created by hanlu on 14-11-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMSwipeContactCell.h"
#import "FriendInfo.h"

#define BUTTON_WIDTH 80

NSString * const kQMSwipeContactCellIdentifier = @"kQMSwipeContactCellIdentifier";

@interface QMSwipeContactCell () {
    FriendInfo * _friendInfo;
}

@end

@implementation QMSwipeContactCell

+ (id)cellForSwipeContact
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMSwipeContactCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.headView setHeadViewStyle:QMRoundHeadViewSmall];
    self.headView.clickEnable = NO;
    
    [_contactLabel setTextColor:UIColorFromRGB(0x4f4f4f)];
    
    self.revealDirection = RMSwipeTableViewCellRevealDirectionRight;
    self.animationType = RMSwipeTableViewCellAnimationTypeEaseOut;
    self.panElasticityStartingPoint = BUTTON_WIDTH;
    self.shouldAnimateCellReset = YES;
    self.backViewbackgroundColor = [UIColor colorWithWhite:0.92 alpha:1];
    self.panElasticity = NO;
    self.panElasticityStartingPoint = 0.0f;
    
    _remarkButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, BUTTON_WIDTH, self.bounds.size.height)];
    [_remarkButton.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [_remarkButton setTitleColor:UIColorFromRGB(0x6a7077) forState:UIControlStateNormal];
    [_remarkButton setTitle:@"备注" forState:UIControlStateNormal];
    [_remarkButton setBackgroundColor:UIColorFromRGB(0xd8dee6)];
    [_remarkButton addTarget:self action:@selector(remarkBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_remarkButton setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(FriendInfo *)friendInfo
{
    _friendInfo = friendInfo;
    
    if (_friendInfo.remark.length > 0 && _friendInfo.nickname.length > 0) {
        NSString *s = [NSString stringWithFormat:@"%@ (%@)", _friendInfo.remark, _friendInfo.nickname];
        NSMutableAttributedString *attriStr = [[NSMutableAttributedString alloc]initWithString:s];
        [attriStr addAttribute:NSForegroundColorAttributeName value:kLightBlueColor range:NSMakeRange(s.length-2-_friendInfo.nickname.length, _friendInfo.nickname.length+2)];
        [attriStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.f] range:NSMakeRange(s.length-2-_friendInfo.nickname.length, _friendInfo.nickname.length+2)];
        _contactLabel.attributedText = attriStr;
    }
    else{
        _contactLabel.text = friendInfo.showname;
    }
    [_headView setImageUrl:friendInfo.icon ];
}


- (void)updateUIWithFriend:(FriendInfo *)friendInfo andSearchString:(NSString *)searchString
{
    _friendInfo = friendInfo;
    
    if (_friendInfo.remark.length > 0 && _friendInfo.nickname.length > 0) {
        
        NSString *s = [NSString stringWithFormat:@"%@ (%@)", _friendInfo.remark, _friendInfo.nickname];
        NSMutableAttributedString *attriStr = [[NSMutableAttributedString alloc]initWithString:s];
        [attriStr addAttribute:NSForegroundColorAttributeName value:kLightBlueColor range:NSMakeRange(s.length-2-_friendInfo.nickname.length, _friendInfo.nickname.length+2)];
        [attriStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.f] range:NSMakeRange(s.length-2-_friendInfo.nickname.length, _friendInfo.nickname.length+2)];
        
        NSRange range=[s rangeOfString:searchString];
        [attriStr addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:range];
        
        _contactLabel.attributedText = attriStr;
    }
    else{
        _contactLabel.text = friendInfo.showname;
    }
    [_headView setImageUrl:friendInfo.icon ];
}



- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.contentView.frame = self.bounds;
    
    CGRect frame = _remarkButton.frame;
    frame.origin.x = self.contentView.frame.origin.x + self.contentView.frame.size.width - _remarkButton.bounds.size.width;
    frame.origin.y = self.contentView.frame.origin.y;
    frame.size.height = self.contentView.frame.size.height;
    _remarkButton.frame = frame;
}

- (FriendInfo *)getFriendInfo
{
    return _friendInfo;
}

-(void)didStartSwiping {
    [super didStartSwiping];
    
    [self.backView addSubview:self.remarkButton];
}

-(void)resetContentView {
    [UIView animateWithDuration:0.15f
                     animations:^{
                         self.contentView.frame = CGRectOffset(self.contentView.bounds, 0, 0);
                     }
                     completion:^(BOOL finished) {
                         self.shouldAnimateCellReset = YES;
                         [self cleanupBackView];
                     }];
}

-(void)cleanupBackView {
    [super cleanupBackView];
    [_remarkButton removeFromSuperview];
//    _remarkButton = nil;
}

#pragma mark - Action

- (void)remarkBtnClick:(id)sender
{
    if(_contactDelegate && [_contactDelegate respondsToSelector:@selector(didRemarkBtnClick:)]) {
        [_contactDelegate didRemarkBtnClick:self];
    }
}

@end
