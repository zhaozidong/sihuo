//
//  SystemHandler.h
//  QMMM
//  该类放一些与系统相关的接口
//  Created by kingnet  on 14-10-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseHandler.h"

@class AppInfo;
@interface SystemHandler : BaseHandler

+ (SystemHandler *)sharedInstance;

- (void)checkVersion:(SuccessBlock)success failed:(FailedBlock)failed;

- (void)submitFeedback:(NSString *)content
                  type:(int)type
               success:(SuccessBlock)success failed:(FailedBlock)failed;

//分享到微信
- (void)shareToWeiXin:(uint)type appInfo:(AppInfo *)appInfo;

- (void)reportDeviceInfo;//上报设备信息

- (void)reportUseTime:(NSString *)time;//上报使用时长

- (void)reportRegisterStep:(NSString *)step;//上报当前所在页面


@end
