//
//  GoodsDataHelper.h
//  QMMM
//
//  Created by 韩芦 on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseDao.h"
#import "Constants.h"
#import "GoodDataInfo.h"

@class GoodEntity;
@class FMDatabase;

@interface GoodsDataHelper : BaseDao

//数据库工作线程
- (void)setWorkThread:(NSThread *)aThread;

//获取精品列表
- (NSArray *)loadRecommendList:(FMDatabase *)database type:(uint)type count:(uint)count;
- (NSArray *)loadRecommendList:(FMDatabase *)database type:(uint)type fromId:(uint64_t)fromId count:(uint)count;
- (void)addRecommendList:(FMDatabase *)database type:(uint)type list:(NSArray *)dataList;
- (void)deleteGoodsById:(FMDatabase *)database type:(uint)type goodsId:(uint64_t)goodsId;
- (void)deleteAllRecommendList:(FMDatabase *)database type:(uint)type;
- (void)updateRecommendList:(FMDatabase *)database type:(uint)type newList:(NSArray *)newRecommendList;
- (void)updateRecommendGoodsForFavor:(FMDatabase *)database goodsInfo:(GoodEntity *)info;
- (void)updateGoodsListForComment:(FMDatabase *)database goodsInfo:(GoodEntity *)info;

//异步操作
- (void)asyncLoadRecommendList:(contextBlock)context type:(uint)type count:(uint)count;
- (void)asyncLoadRecommendList:(contextBlock)context type:(uint)type fromId:(uint64_t)fromId count:(uint)count;
- (void)asyncAddRecommendList:(contextBlock)context type:(uint)type list:(NSArray *)datalist;
- (void)asyncDeleteGoodsById:(contextBlock)context type:(uint)type goodsId:(uint64_t)goodsId;
- (void)asyncDeleteAllRecommendList:(contextBlock)context type:(uint)type;
- (void)asyncUpdateRecomendList:(contextBlock)context type:(uint)type newList:(NSArray*)newRecommendList;
- (void)asyncUpdateRecommendGoodsForFavor:(contextBlock)context goodsInfo:(GoodEntity *)info;
- (void)asyncUpdateGoodsForComment:(contextBlock)context goodsInfo:(GoodEntity *)info;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//获取订单列表，商品列表，已购列表，赞列表
- (NSArray *)loadMylist:(FMDatabase *)database type:(EControllerType)type fromId:(NSString *)fromId count:(uint)count;
//- (NSArray *)loadGoodslist:(FMDatabase *)database timeStamp:(int)timestamp count:(uint)count;
- (void)addMyList:(FMDatabase *)database type:(EControllerType)type list:(NSArray *)dataList;
- (void)deleteMyList:(FMDatabase *)database type:(EControllerType)type uuId:(uint64_t)uuId;
- (void)deleteAllMyList:(FMDatabase *)database type:(EControllerType)type;
- (void)updateGoodsSimpleInfo:(FMDatabase *)database type:(EControllerType)type dict:(NSDictionary *)dict;
- (void)updateOrderStatus:(FMDatabase *)database bSeller:(BOOL)bSeller orderId:(NSString *)orderId status:(uint)status;
- (void)updateOrderPrice:(FMDatabase *)database bSeller:(BOOL)bSeller orderId:(NSString *)orderId price:(float)price;

- (void)asyncLoadMyList:(contextBlock)context type:(EControllerType)type fromId:(NSString *)fromId count:(uint)count;
- (void)asyncAddMyList:(contextBlock)context type:(EControllerType)type list:(NSArray *)datalist;
- (void)asyncDeleteMyList:(contextBlock)context type:(EControllerType)type uuId:(uint64_t)uuId;
- (void)asyncDeleteAllMyList:(contextBlock)context type:(EControllerType)type;
- (void)asyncupdateGoodsSimpleInfo:(contextBlock)context type:(EControllerType)type dict:(NSDictionary *)dict;
- (void)asyncUpdateOrderStatus:(contextBlock)context bSeller:(BOOL)bSeller orderId:(NSString *)orderId status:(uint)status;
- (void)asyncUpdateOrderPrice:(contextBlock)context bSeller:(BOOL)bSeller orderId:(NSString *)orderId payAmount:(float)payAmount;

@end
