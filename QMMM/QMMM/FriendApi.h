//
//  FriendApi.h
//  QMMM
//
//  Created by hanlu on 14-11-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseHandler.h"

@interface FriendApi : BaseHandler

- (void)getFriendList:(contextBlock)context;

- (void)inviteFriend:(NSString *)friendList isInviteAll:(BOOL)isInviteAll context:(contextBlock)context;

- (void)acceptFriend:(uint64_t)fid token:(NSString *)token context:(contextBlock)context;

- (void)modifyFriendRemark:(uint64_t)fid remark:(NSString *)remark context:(contextBlock)context;

- (void)getProbablyKnowList:(contextBlock)context;

@end
