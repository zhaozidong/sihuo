//
//  QMSpecialTopicsCell.m
//  QMMM
//
//  Created by Derek on 15/1/4.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMSpecialTopicsCell.h"

#import <SDWebImage/UIImageView+WebCache.h>

@implementation QMSpecialTopicsCell{
    NSArray *_arrInfo;
}

+ (id)cellForGoodsTopics{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMSpecialTopicsCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor=[UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    for(UIImageView *imageView in self.contentView.subviews){
        imageView.userInteractionEnabled=YES;
        UITapGestureRecognizer * tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didImageViewClicked:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        tapGestureRecognizer.numberOfTouchesRequired = 1;
        [imageView addGestureRecognizer:tapGestureRecognizer];
    }
}

-(void)updateWithArr:(NSArray *)arrInfo{
    if (!arrInfo) {
        NSLog(@"活动数据为空");
        return;
    }
    _arrInfo=arrInfo;
    NSMutableArray *arrPath=[[NSMutableArray alloc] initWithCapacity:4];
    for (int i=0; i<[arrInfo count]; i++) {
        ActivityEntity *tempEntity=[arrInfo objectAtIndex:i];
        [arrPath addObject:tempEntity.smallImgSrc];
        NSString *strPath=tempEntity.smallImgSrc;
        NSURL *imageUrl = [NSURL URLWithString:[AppUtils getAbsolutePath:strPath]];
        for (UIImageView *imageView in self.contentView.subviews) {
            if (imageView.tag==i+1) {
                [imageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_small"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error) {
                        NSLog(@"图片加载失败");
                        imageView.image=[UIImage imageNamed:@"goodImgFailed_small"];
                    }
                }];
            }
        }
    }
}

-(void)setActImage:(UIImage *)image forIndex:(NSInteger)index{
    for (UIImageView *imgView in self.contentView.subviews) {
        if (imgView.tag==index) {
            imgView.image=image;
        }
    }
}
-(void)setFailImageForIndex:(NSInteger)index{
    for (UIImageView *imgView in self.contentView.subviews) {
        if (imgView.tag==index) {
            imgView.image=[UIImage imageNamed:@"goodImgFailed_big"];
        }
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)didImageViewClicked:(UIGestureRecognizer *)gestureRecognizer{
    UIImageView *imgView=(UIImageView *)gestureRecognizer.view;
    if (imgView.tag && _delegate && [_delegate respondsToSelector:@selector(didSpecialTopiceClick:)]) {
        [_delegate didSpecialTopiceClick:[_arrInfo objectAtIndex:imgView.tag-1]];
    }
}
@end
