//
//  GoodsHandler.m
//  QMMM
//
//  Created by 韩芦 on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <SDWebImage/SDWebImageManager.h>
#import "GoodsHandler.h"
#import "GoodsDataHelper.h"
#import "QMDatabaseHelper.h"
#import "GoodsApi.h"
#import "MyGoodsList.h"
#import "GoodEntity.h"
#import "CommentEntity.h"
#import "GoodDataInfo.h"
#import "OrderDataInfo.h"
//#import "WXApi.h"
#import "UserDao.h"
#import "UIImage+Additions.h"
#import "QMDataCenter.h"
#import "UserInfoHandler.h"
#import "FinanceHandler.h"
#import "FinanceDataInfo.h"
#import "UIImage+FixOrientation.h"
#import "ActivityInfo.h"
#import "QMGoodsCateInfo.h"
#import "UserDefaultsUtils.h"

//加载数据状态
NSInteger const kTTGoodsLoadStateNone = 0;
NSInteger const kTTGoodsLoadStateLoadDone = 1;
NSInteger const kTTGoodsLoadStateLoading = 2;
NSInteger const kTTGoodsLoadStateLoadTimeout = 3;
NSInteger const kTTGoodsLoadStateLoadFail = 4;
NSInteger const kTTGoodsLoadStateLoadEnd = 5;

//上传图片的key
NSString * const kPhotoKey = @"kPhotoKey";
NSString * const kPhotoFilePath = @"kPhotoFilePath";

//搜索处的关键字
NSString * const kSearchGoodsWords = @"words"; //关键字
NSString * const kSearchGoodsCategory = @"category"; //分类
NSString * const kSearchGoodsPriceMin = @"price_min";  //价格下限
NSString * const kSearchGoodsPriceMax = @"price_max";  //价格上限
NSString * const kSearchGoodsOffset = @"offset";   //查询偏移量
NSString * const kSearchGoodsSort = @"sort";   //排序方式
NSString * const kSearchGoodsCondition = @"condition";  //新旧程度
NSString * const kSearchGoodsDiscount = @"discount";   //折扣力度
NSString * const kSearchGoodsDistance = @"distance"; //距离
NSString * const kSearchGoodsPoint = @"point"; //地理位置坐标

@interface GoodsHandler () {
    GoodsApi * _goodsApi;
}

//@property (nonatomic, strong) KNThread * workThread;

@end

@implementation GoodsHandler

//@synthesize workThread = _workThread;

static GoodsHandler* _shareInstance = NULL;

+ (GoodsHandler *)shareGoodsHandler
{
    @synchronized([GoodsHandler class]) {
        if (nil == _shareInstance) {
            _shareInstance = [[GoodsHandler alloc] init];
        }
        return _shareInstance;
    }
}

//+ (BOOL)isRuning
//{
//    @synchronized([GoodsHandler class]) {
//        return nil != _shareInstance;
//    }
//}

- (id)init
{
    self = [super init];
    if(self) {
//        _goodsHelper = [[GoodsDataHelper alloc] init];
        
        _goodsHelper = [QMDataCenter sharedDataCenter].goodsDataHelper;
        
        _goodsApi = [GoodsApi shareInstance];
        
        _myGoodsList = [[MyGoodsList alloc] initWithType:EGLT_RECOMMEND];
        
        _friendGoodsList = [[MyGoodsList alloc] initWithType:EGLT_FRIEND];
    }
    return self;
}

//- (void)start
//{
//    @synchronized(self) {
//        if (_workThread)
//            return;
//        
//        _workThread = [[KNThread alloc] initWithDelegate:self];
//        
//        [_goodsHelper setWorkThread:_workThread];
//        
//        [_workThread start];
//    }
//}
//
//- (void)stop
//{
//    @synchronized(self) {
//        if (!_workThread)
//            return;
//        
//        [_goodsHelper setWorkThread:nil];
//        
//        [_workThread kill:YES];
//        
//        self.workThread = nil;
//    }
//}
//
//#pragma mark - KNThreadDelegate
//
//- (void)threadStart:(KNThread *)thread
//{
//    if (thread) {
//        [thread setName:[NSString stringWithFormat:@"%@ Work Thread", NSStringFromClass([self class])]];
//    }
//    
//    QMDatabaseHelper *helper = [QMDatabaseHelper sharedInstance];
//    [helper.openDatabase open];
//    
//    [_goodsHelper initAllTables:helper.openDatabase];
//    
////    DLog(@"goods work thread begin");
////    
//////    // 创建数据库帮助对象，以备线程中其他对象使用
//////    QMDatabaseHelper *helper = [QMDatabaseHelper sharedInstance];
//////    [helper putIntoCurrentThread];
//////    if (![helper open]) {
//////        DLog(@"open error!");
//////    }
////    // TODO: 处理 open 不成功的情况
////    DLog(@"goods work thread end");
//}
//
//- (void)threadStop:(KNThread *)thread
//{
//    // 移除并释放数据库帮助对象
//    //[TTDatabaseHelper removeFromCurrentThread];
//}

+ (void)freeGoodsHandler
{
    if(_shareInstance) {
        _shareInstance = nil;
    }
}

#pragma mark - 业务逻辑

- (void)uploadGoodsPhotoData:(NSData *)imgData uploadProgressBlock:(void (^)(NSUInteger, long long, long long))uploadProgressBlock success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi uploadGoodsPhotoData:imgData uploadProgressBlock:uploadProgressBlock context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            NSDictionary *dataDic = (NSDictionary *)resultDic[@"d"];
            NSString *key = dataDic[@"asset_key"];
            if (success) {
                success(key);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)upLoadAudio:(NSData *)data success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi upLoadAudio:data context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = (NSDictionary *)resultDic[@"d"];
            NSString *key = dataDic[@"asset_key"];
            if (success) {
                success(key);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)upLoadImage:(NSData *)data success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi uploadGoodsImage:data context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = (NSDictionary *)resultDic[@"d"];
            NSString *key = dataDic[@"asset_key"];
            if (success) {
                success(key);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)releaseGoods:(GoodEntity *)entity success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi releaseGoodsWithDes:entity.desc price:entity.price number:entity.number category:entity.cate_type photos:entity.picturelist.rawArray ori_price:entity.orig_price isNew:entity.isNew audioTime:entity.msgAudioSeconds audioKey:entity.msgAudio location:entity.location address:entity.city freight:entity.freight context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = (NSDictionary *)resultDic[@"d"];
            if (success) {
                success(dataDic);
            }
            
            //统计发布成功页
            [AppUtils trackCustomEvent:@"confirm_publish_event" args:nil];
            
            //统计发布金额与数量
            NSDictionary *params = @{@"amount":@(entity.price), @"goodsNum":@(entity.number)};
            [AppUtils trackCustomKeyValueEvent:@"goods_publish_ok" props:params];
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)goodsDetailsWithId:(uint16_t)goodsId prisNum:(NSInteger)prisNum success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi goodsDetailsWithId:goodsId prisNum:prisNum context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = (NSDictionary *)resultDic[@"d"];
            GoodEntity *entity = [[GoodEntity alloc]initWithDictionary:dataDic];
            if (success) {
                success(entity);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)commentListWithId:(uint64_t)goodsId offset:(uint64_t)offset success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi commentListWithId:goodsId offset:offset context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = [resultDic objectForKey:@"d"];
            if ([dataDic isKindOfClass:[NSDictionary class]]) {
                NSArray *dataArr = [dataDic objectForKey:@"list"];
                NSMutableArray *commentList = [NSMutableArray arrayWithCapacity:dataArr.count];
                for (NSDictionary *dic in dataArr) {
                    CommentEntity *entity = [[CommentEntity alloc]initWithDictionary:dic];
                    [commentList addObject:entity];
                }
                NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:commentList, @"list", [dataDic objectForKey:@"max_id"], @"maxId", nil];
                if (success) {
                    success(dict);
                }
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)downloadAudio:(NSString *)audioPath complete:(CompleteBlock)completeHandle
{
    if([audioPath isKindOfClass:[NSNull class]]) {
        return;
    }
    
    //检测本地是否存在，存在则不下载
    NSString * filename = [[GoodsHandler class] audioPath];
    
    //截取音频文件名
    NSRange range = [audioPath rangeOfString:@"/" options:NSBackwardsSearch];
    if(range.location != NSNotFound) {
        NSString * temp = [audioPath substringFromIndex:range.location+1];
        range = [temp rangeOfString:@"." options:NSBackwardsSearch];
        if(range.location != NSNotFound) {
            temp = [temp substringToIndex:range.location];
            NSString * wavSuffix = [filename stringByAppendingFormat:@"/%@.m4a", temp];
            
            BOOL bDirectory = NO;
            NSFileManager * fileManager = [NSFileManager defaultManager];
            if([fileManager fileExistsAtPath:wavSuffix isDirectory:&bDirectory]) {
                //本地存在，则不用下载了
                return;
            } else {
                //
                //去下载
                [_goodsApi downloadAudio:audioPath context:^(id result) {
                    if(result && [result isKindOfClass:[NSDictionary class]]) {
                        BOOL success = [[result objectForKey:@"s"] intValue];
                        if(0 == success && completeHandle) {
                            completeHandle();
                        }
                    }
                }];
            }
        }
    }
}

- (void)addFavor:(uint64_t)goodsId sellerId:(uint64_t)sellerId
{
    //统计赞被点击的次数
    [AppUtils trackCustomEvent:@"favor_click_event" args:nil];
    
    GoodEntity * retEntify = nil;
    NSArray * array = _myGoodsList.goodsList;
    for (int i = 0; i < [array count]; i ++) {
        GoodEntity * info = [array objectAtIndex:i];
        if(info.gid == goodsId) {
            uint64_t userId = (uint64_t)[[UserInfoHandler sharedInstance].currentUser.uid longLongValue];
            [info addFavor:userId];
            retEntify = info;
            break;
        }
    }
    
    GoodEntity * retEntify2 = nil;
    NSArray * friendGoodsArray = _friendGoodsList.goodsList;
    for (int i = 0; i < [friendGoodsArray count]; i ++) {
        GoodEntity * info2 = [friendGoodsArray objectAtIndex:i];
        if(info2.gid == goodsId) {
            uint64_t userId = (uint64_t)[[UserInfoHandler sharedInstance].currentUser.uid longLongValue];
            [info2 addFavor:userId];
            retEntify2 = info2;
            break;
        }
    }
    
    BOOL saveDB = NO;
    if(retEntify) {
        saveDB = YES;
        //DB
        [_goodsHelper asyncUpdateRecommendGoodsForFavor:nil goodsInfo:retEntify];
        
    }
    
    if(retEntify2) {
        if(!saveDB) {
            [_goodsHelper asyncUpdateRecommendGoodsForFavor:nil goodsInfo:retEntify2];
        }
    }
    
    //刷新UI
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:@(goodsId), @"goodsId", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kGoodsUpdateFavorNotify object:nil userInfo:dict];
    
    //发请求
    [_goodsApi addfavor:goodsId sellerId:sellerId context:nil];
}

//列表中增加评论数量
- (void)addComment:(uint64_t)goodsId
{
    //发现
    GoodEntity * retEntity = nil;
    NSArray * array = _myGoodsList.goodsList;
    for (int i = 0; i < [array count]; i ++) {
        GoodEntity * info = [array objectAtIndex:i];
        if(info.gid == goodsId) {
            info.commentCounts++;
            retEntity = info;
            break;
        }
    }
    
    //首页
    GoodEntity *retEntity2 = nil;
    NSArray * friendGoodsArray = _friendGoodsList.goodsList;
    for (int i = 0; i < [friendGoodsArray count]; i ++) {
        GoodEntity * info2 = [friendGoodsArray objectAtIndex:i];
        if(info2.gid == goodsId) {
            info2.commentCounts++;
            retEntity2 = info2;
            break;
        }
    }

    BOOL saveDB = NO;
    if (retEntity) {
        saveDB = YES;
        //DB
        [_goodsHelper asyncUpdateGoodsForComment:nil goodsInfo:retEntity];
    }
    
    if (retEntity2) {
        if (!saveDB) {
            //DB
            [_goodsHelper asyncUpdateGoodsForComment:nil goodsInfo:retEntity];
        }
    }
    
    //刷新UI
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:@(goodsId), @"goodsId", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kAddGoodsCommentNotify object:nil userInfo:dict];
}


//列表中减少评论数量
- (void)decreaseCommentCount:(uint64_t)goodsId
{
    //发现
    GoodEntity * retEntity = nil;
    NSArray * array = _myGoodsList.goodsList;
    for (int i = 0; i < [array count]; i ++) {
        GoodEntity * info = [array objectAtIndex:i];
        if(info.gid == goodsId) {
            info.commentCounts--;
            retEntity = info;
            break;
        }
    }
    
    //首页
    GoodEntity *retEntity2 = nil;
    NSArray * friendGoodsArray = _friendGoodsList.goodsList;
    for (int i = 0; i < [friendGoodsArray count]; i ++) {
        GoodEntity * info2 = [friendGoodsArray objectAtIndex:i];
        if(info2.gid == goodsId) {
            info2.commentCounts--;
            retEntity2 = info2;
            break;
        }
    }
    
    BOOL saveDB = NO;
    if (retEntity) {
        saveDB = YES;
        //DB
        [_goodsHelper asyncUpdateGoodsForComment:nil goodsInfo:retEntity];
    }
    
    if (retEntity2) {
        if (!saveDB) {
            //DB
            [_goodsHelper asyncUpdateGoodsForComment:nil goodsInfo:retEntity];
        }
    }
    
    //刷新UI
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:@(goodsId), @"goodsId", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kAddGoodsCommentNotify object:nil userInfo:dict];
}


- (void)sendComment:(uint64_t)goodsId pid:(uint64_t)pid comments:(NSString *)comments success:(SuccessBlock)success failed:(FailedBlock)failed
{
    //统计评论次数
    [AppUtils trackCustomEvent:@"comment_event" args:nil];
    
    __weak typeof(self) weakSelf = self;
    [_goodsApi sendComment:goodsId pid:pid comments:comments context:^(id result) {
        if (result && [result isKindOfClass:[NSDictionary class]]) {
            if ([result[@"s"] intValue]==0) {
                [weakSelf addComment:goodsId];
                if (success) {
                    NSDictionary *dataDic = result[@"d"];
                    CommentEntity *entity = [[CommentEntity alloc]initWithDictionary:dataDic];
                    success(entity);
                }
            }
            else{
                if (failed) {
                    failed(result[@"d"]);
                }
            }
        }
    }];
}

- (void)searchGoods:(NSDictionary *)dict success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi searchGoods:dict context:^(id result) {
        if(result && [result isKindOfClass:[NSDictionary class]]) {
            if(0 == [[result objectForKey:@"s"] intValue]) {
                id temp = [result objectForKey:@"d"];
                if (temp && [temp isKindOfClass:[NSDictionary class]]) {
                    NSMutableArray * ret = [[NSMutableArray alloc] init];
                    id temp2 = [temp objectForKey:@"list"];
                    if (temp2 && [temp2 isKindOfClass:[NSArray class]]) {
                        for (NSDictionary * dict in temp2) {
                            GoodEntity * info = [[GoodEntity alloc] initWithDictionary:dict];
                            [ret addObject:info];
                        }

                    }
                    
                    temp2 = [temp objectForKey:@"offset"];
                    NSDictionary *retDic = @{@"list":ret, @"offset":temp2};
                    
                    if (success) {
                        success(retDic);
                    }
                }
            } else {
                if(failed) {
                    failed(result[@"d"]);
                }
            }
        }
    }];
}

- (void)getOrderList:(uint)type orderId:(NSString *)orderId success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi getOrderList:type orderId:orderId context:^(id result) {
        if(result && [result isKindOfClass:[NSDictionary class]]) {
            if(0 == [[result objectForKey:@"s"] intValue]) {
                id temp = [result objectForKey:@"d"];
                if(temp && [temp isKindOfClass:[NSArray class]]) {
                    NSMutableArray * ret = [[NSMutableArray alloc] init];
                    for (NSDictionary * dict in temp) {
                        OrderSimpleInfo * info = [[OrderSimpleInfo alloc] initWithDictionary:dict];
                        [ret addObject:info];
                    }
                    
                    if(success) {
                        success(ret);
                    }
                }
            } else {
                if(failed) {
                    failed(result[@"d"]);
                }
            }
        }
    }];
}

- (void)getOrderDetail:(uint)type orderId:(NSString *)orderId success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi getOrderDetail:type orderId:orderId context:^(id result) {
         if(result && [result isKindOfClass:[NSDictionary class]]) {
             if(0 == [[result objectForKey:@"s"] intValue]) {
                 id temp = [result objectForKey:@"d"];
                 if(temp && [temp isKindOfClass:[NSDictionary class]]) {
                    OrderDataInfo * info = [[OrderDataInfo alloc] initWithDictionary:temp];
                     if(success) {
                         success(info);
                     }
                     
                 } else {
                     if(failed) {
                         failed(@"获取订单详情失败");
                     }
                 }
             } else {
                 if(failed) {
                     failed(result[@"d"]);
                 }
             }
         }
    }];
}

- (void)doScores:(NSString *)orderId scores:(float)scores comment:(NSString *)comment success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi doScores:orderId scores:scores comment:comment context:^(id result) {
        if(result && [result isKindOfClass:[NSDictionary class]]) {
            if(0 == [[result objectForKey:@"s"] intValue]) {
                [AppUtils trackCustomEvent:@"buy_recive_ok" args:nil];
                
                id temp = [result objectForKey:@"d"];
                if(success) {
                    
                    //数据库更新
                    [_goodsHelper asyncUpdateOrderStatus:nil bSeller:NO orderId:orderId status:QMOrderStatusFinished];
                    
                    //UI
                    NSDictionary * dict = @{@"orderId":orderId, @"status":@(QMOrderStatusFinished)};
                    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateOrderStatusNotify object:nil userInfo:dict];
                    
                    success(temp);
                }
            } else {
                if(failed) {
                    failed([result objectForKey:@"d"]);
                }
            }
        }
    }];
}

- (void)deliverGoods:(NSString *)orderId expressCo:(NSString *)expressCo express_num:(NSString *)express_num success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi deliverGoods:orderId expressCo:expressCo expressNum:express_num context:^(id result) {
        if(result && [result isKindOfClass:[NSDictionary class]]) {
            if(0 == [[result objectForKey:@"s"] intValue]) {
                [AppUtils trackCustomEvent:@"buy_deliver_ok" args:nil];
                
                //数据库更新
                [_goodsHelper asyncUpdateOrderStatus:nil bSeller:YES orderId:orderId status:QMOrderStatusDelivery];
                
                //UI
                NSDictionary * dict = @{@"orderId":orderId, @"status":@(QMOrderStatusDelivery)};
                [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateOrderStatusNotify object:nil userInfo:dict];
                
                if(success) {
                    success(kHTTPTreatSuccess);
                }
            } else {
                if(failed) {
                    failed([result objectForKey:@"d"]);
                }
            }
        }
    }];
}

//赞列表
- (void)getFavorList:(NSString *)goodsId success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi getFavorList:goodsId context:^(id result) {
        if(result && [result isKindOfClass:[NSDictionary class]]) {
            if(0 == [[result objectForKey:@"s"] intValue]) {
                id temp = [result objectForKey:@"d"];
                if(temp && [temp isKindOfClass:[NSArray class]]) {
                    NSMutableArray * ret = [[NSMutableArray alloc] init];
                    for (NSDictionary * dict in temp) {
                        GoodsSimpleInfo * info = [[GoodsSimpleInfo alloc] initWithDictionary:dict];
                        [ret addObject:info];
                    }
                    if(success) {
                        success(ret);
                    }
                }
            } else {
                if(failed) {
                    failed(result[@"d"]);
                }
            }
        }
    }];
}

//删除评论
- (void)deleteCommit:(NSString *)cId goodsId:(NSString *)gId success:(SuccessBlock)success failed:(FailedBlock)failed{
    [_goodsApi deleteCommitWithGoodsId:gId commitId:cId context:^(id result) {
        if(result && [result isKindOfClass:[NSDictionary class]]) {
            if(0 == [[result objectForKey:@"s"] intValue]) {
//                id temp = [result objectForKey:@"d"];
                if([[result objectForKey:@"c"] integerValue]==0) {
                    if(success) {
                        success(result);
                    }
                }
            } else {
                if(failed) {
                    failed(result[@"d"]);
                }
            }
        }
    }];

}

//已购买商品列表
- (void)getBoughtGoodsList:(NSString *)goodsId success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi getBoughtList:goodsId context:^(id result) {
        if(result && [result isKindOfClass:[NSDictionary class]]) {
            if(0 == [[result objectForKey:@"s"] intValue]) {
                id temp = [result objectForKey:@"d"];
                if(temp && [temp isKindOfClass:[NSArray class]]) {
                    NSMutableArray * ret = [[NSMutableArray alloc] init];
                    for (NSDictionary * dict in temp) {
                        GoodsSimpleInfo * info = [[GoodsSimpleInfo alloc] initWithDictionary:dict];
                        [ret addObject:info];
                    }
                    if(success) {
                        success(ret);
                    }
                }
            } else {
                if(failed) {
                    failed(result[@"d"]);
                }
            }
        }
    }];
}

- (void)getGoodsInfoWithId:(uint64_t)goodsId success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi getGoodsInfoWithId:goodsId context:^(id resultDic) {
        if (resultDic && [resultDic isKindOfClass:[NSDictionary class]]) {
            if ([resultDic[@"s"]intValue] == 0) {
                id temp = [resultDic objectForKey:@"d"];
                if (temp && [temp isKindOfClass:[NSDictionary class]]) {
                    GoodEntity *goodsEntity = [[GoodEntity alloc]initWithDictionary:temp];
                    if (success) {
                        success(goodsEntity);
                    }
                }
            }
            else{
                if (failed) {
                    failed(resultDic[@"d"]);
                }
            }
        }
    }];
}

- (void)updateGoodsInfo:(GoodEntity *)goodsEntity success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi updateGoodsWithId:goodsEntity.gid desc:goodsEntity.desc price:goodsEntity.price number:goodsEntity.number category:goodsEntity.cate_type photos:goodsEntity.picturelist.rawArray ori_price:goodsEntity.orig_price isNew:goodsEntity.isNew audioTime:goodsEntity.msgAudioSeconds audioKey:goodsEntity.msgAudio location:goodsEntity.location address:goodsEntity.city freight:goodsEntity.freight context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] ==0) {
            if (success) {
                NSString * photo = @"";
                if([goodsEntity.picturelist.rawArray count] > 0) {
                    photo = goodsEntity.picturelist.rawArray[0];
                }
                
                NSDictionary * dict = @{@"id":@(goodsEntity.gid), @"desc":goodsEntity.desc, @"photo":photo, @"price":@(goodsEntity.price), @"ori_price":@(goodsEntity.orig_price), @"publish_time":@(goodsEntity.pub_time)};
                
                //清理数据库
                [_goodsHelper asyncupdateGoodsSimpleInfo:nil type:ECT_GOODS_MGR dict:dict];
                
                //
                //UI
//                NSDictionary * userInfo = @{@"type":@(ECT_GOODS_MGR), @"info":dict};
//                [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateGoodsSimpleInfoNotify object:nil userInfo:userInfo];
                NSDictionary *dataDic = [resultDic objectForKey:@"d"];
                success(dataDic);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)deleteGoodsWithId:(uint64_t)goodsId success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi updateGoodsStaWithId:goodsId type:-1 context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            if (success) {
                
                //清理商品缓存
                [_myGoodsList deleteGoods:goodsId];
                [_friendGoodsList deleteGoods:goodsId];
                
                //清理商品数据库
                [_goodsHelper asyncDeleteGoodsById:nil type:EGLT_RECOMMEND goodsId:goodsId];
                [_goodsHelper asyncDeleteGoodsById:nil type:EGLT_FRIEND goodsId:goodsId];
                
                //清理商品简要信息数据库
                [_goodsHelper asyncDeleteMyList:nil type:ECT_GOODS_MGR uuId:goodsId];
                
                //UI
                NSDictionary * dict = @{@"goodsId":@(goodsId)};
                [[NSNotificationCenter defaultCenter] postNotificationName:kDeleteGoodsNotify object:nil userInfo:dict];
                
                success(kHTTPTreatSuccess);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)dropGoodsWithId:(GoodsSimpleInfo*)simpeInfo bdrop:(BOOL)drop success:(SuccessBlock)success failed:(FailedBlock)failed
{
    int type = drop ? 0 : 1;
    [_goodsApi updateGoodsStaWithId:simpeInfo.gid type:type context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            
            simpeInfo.status = type;
            
            //更新数据库
            NSDictionary * dict = [simpeInfo toDictionary];
            [_goodsHelper asyncupdateGoodsSimpleInfo:nil type:ECT_GOODS_MGR dict:dict];
            
            if (success) {
                success(kHTTPTreatSuccess);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];

}

- (void)updateOrderStatus:(BOOL)bSeller orderId:(NSString *)orderId status:(uint)status
{
    
}


- (float) fileSizeAtPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize]/(1024.0*1024);
    }
    return 0;
}

- (void)batchUploadGoodsPhoto:(NSArray *)filePaths success:(SuccessBlock)success failed:(FailedBlock)failed
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSArray *tmpArr = [filePaths copy];
        NSMutableArray *fileData = [NSMutableArray arrayWithCapacity:tmpArr.count];
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        for (NSString *s in tmpArr) {
            //把小图片路径换成大图片路径
            NSData *data;
            NSString *bigS= [s stringByReplacingOccurrencesOfString:@"_small" withString:@""];
            NSString *editPic=[s stringByReplacingOccurrencesOfString:@"_small" withString:@"_edit"];

            if ([fileMgr fileExistsAtPath:bigS]){
                UIImage *image=[[UIImage imageWithContentsOfFile:bigS] fixOrientation];
                if ([self fileSizeAtPath:bigS]>1) {
                    data=UIImageJPEGRepresentation(image, 0.1);
                }else{
                    data=UIImageJPEGRepresentation(image, 0.6);
                }
//                data = [NSData dataWithContentsOfFile:bigS];
            }else if ([fileMgr fileExistsAtPath:editPic]){
                UIImage *image=[[UIImage imageWithContentsOfFile:editPic] fixOrientation];
                if ([self fileSizeAtPath:editPic]>1) {
                    data=UIImageJPEGRepresentation(image, 0.1);
                }else{
                    data=UIImageJPEGRepresentation(image, 0.6);
                }
 //               data=UIImageJPEGRepresentation(image, 0.6);
//                data = [NSData dataWithContentsOfFile:editPic];
            }
            
//            NSString *imagePath=[AppUtils getBigImgPathFromSmallImgPath:s];
//            if (imagePath) {
//                UIImage *image=[[UIImage imageWithContentsOfFile:imagePath] fixOrientation];
//                data=UIImageJPEGRepresentation(image, 0.9);
//            }else{
//                DLog(@"获取图片路径失败");
//                return ;
//            }
            if (data) {
                [fileData addObject:data];
            }
        }
        [super batchUploadImageData:fileData type:@"goods" context:^(id resultDic) {
            NSArray *keys = (NSArray *)resultDic;
            if (keys.count != tmpArr.count) {
                DLog(@"keys num error!");
                if (failed) {
                    failed(@"keys num error!");
                }
            }
            else{
                NSMutableDictionary *keysAndFilePaths = [NSMutableDictionary dictionary];
                for (int i=0; i<keys.count; i++) {
                    [keysAndFilePaths setObject:keys[i] forKey:tmpArr[i]];
                }
                if (success) {
                    success(keysAndFilePaths);
                }
            }
        }];
    });
}

- (void)submitOrder:(int)addressId goodsId:(uint64_t)goodsId paymentType:(int)paymentType num:(int)num remainPay:(float)remainPay success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi submitOrder:addressId goodsId:goodsId paymentType:paymentType num:num remainPay:remainPay context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            //统计提交订单成功
            [AppUtils trackCustomEvent:@"submitOrder_ok" args:nil];
            
            if (success) {
                NSDictionary *dataDic = [resultDic objectForKey:@"d"];
                if (dataDic && [dataDic isKindOfClass:[NSDictionary class]]) {
                    success(dataDic); //{"id":"2014121159","callback_url":"http://test.sihuo.com/?c=order&a=callback"}
                }
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)deleteOrder:(NSString *)orderId type:(int)type success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi deleteOrder:orderId type:type context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] ==0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:kDeleteOrderOKNotify object:nil];
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)cancelOrder:(NSString *)orderId success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi cancelOrder:orderId context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
            
            //数据库更新
            [_goodsHelper asyncUpdateOrderStatus:nil bSeller:NO orderId:orderId status:QMOrderStatusClosed];
            
            NSDictionary *userInfo = @{@"orderId":orderId, @"status":@(QMOrderStatusClosed)};
            [[NSNotificationCenter defaultCenter]postNotificationName:kUpdateOrderStatusNotify object:nil userInfo:userInfo];
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)getActivitiesInfo:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi getActivitiesInfo:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = [resultDic objectForKey:@"d"];
//            NSLog(@"dict=%@",dataDic);
            ActivityInfo *activityInfo = [[ActivityInfo alloc]initWithDictionary:dataDic];
            if (success) {
                success(activityInfo);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)getAllTopic:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi getAllTopic:^(id resultDic) {
        if ([resultDic[@"s"] intValue] ==0) {
//            NSDictionary *dataDic= [resultDic objectForKey:@"d"];
            NSMutableArray *arrRes=[NSMutableArray arrayWithCapacity:1];
            NSArray *arrData=[resultDic objectForKey:@"d"];
            for (NSDictionary *dictData in arrData) {
                if (dictData && [dictData isKindOfClass:[NSDictionary class]]) {
                    BannerEntity *banner=[[BannerEntity alloc] initWithDictionary:dictData];
                    [arrRes addObject:banner];
                }
            }
            if (success) {
                success(arrRes);
            }
        }else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}


- (void)getSpecialGoodsList:(uint)top key:(uint64_t)key tag:(NSString *)tag success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi getSpecialGoodsList:top key:key tag:tag context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = [resultDic objectForKey:@"d"];
            if (dataDic && [dataDic isKindOfClass:[NSDictionary class]]) {
                NSArray *dataArr = [dataDic objectForKey:@"list"];
                NSMutableArray *mbArr = [NSMutableArray array];
                for (NSDictionary *dic in dataArr) {
                    GoodEntity *entity = [[GoodEntity alloc]initWithDictionary:dic];
                    [mbArr addObject:entity];
                }
                NSMutableDictionary *returnDic = [NSMutableDictionary dictionary];
                [returnDic setObject:mbArr forKey:@"list"];
                [returnDic setObject:[dataDic objectForKey:@"update_num"] forKey:@"update_num"];
                [returnDic setObject:[dataDic objectForKey:@"page_num"] forKey:@"page_num"];
                if (top == 1) { //下拉
                    if (dataArr.count > 0) {
                        NSDictionary *firstDic = [dataArr firstObject];
                        [returnDic setObject:[firstDic objectForKey:@"key"] forKey:@"topKey"];
                        if (key == 0) {
                            //第一次拉去要拿到最后一个key
                            NSDictionary *lastDic = [dataArr lastObject];
                            [returnDic setObject:[lastDic objectForKey:@"key"] forKey:@"lastKey"];
                        }
                    }
                }
                else{ //上拉的key
                    if (dataArr.count > 0) {
                        NSDictionary *lastDic = [dataArr lastObject];
                        [returnDic setObject:[lastDic objectForKey:@"key"] forKey:@"lastKey"];
                        if (key == 0) {
                            //第一次拉去要拿到第一个key
                            NSDictionary *firstDic = [dataArr firstObject];
                            [returnDic setObject:[firstDic objectForKey:@"key"] forKey:@"topKey"];
                        }
                    }
                }
                if (success) {
                    success(returnDic);
                }
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)topicsGoodsFromSpetial:(NSString *)requestURL success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi topicsGoodsList:requestURL context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = [resultDic objectForKey:@"d"];
            if (dataDic && [dataDic isKindOfClass:[NSDictionary class]]) {
                NSArray *dataArr = [dataDic objectForKey:@"list"];
                NSMutableArray *mbArr = [NSMutableArray array];
                for (NSDictionary *dic in dataArr) {
                    GoodEntity *entity = [[GoodEntity alloc]initWithDictionary:dic];
                    [mbArr addObject:entity];
                }
                NSMutableDictionary *returnDic = [NSMutableDictionary dictionary];
                [returnDic setObject:mbArr forKey:@"list"];
        
                if (dataArr.count > 0) {
                    NSDictionary *lastDic = [dataArr lastObject];
                    [returnDic setObject:[lastDic objectForKey:@"key"] forKey:@"lastKey"];
                }
                if (success) {
                    success(returnDic);
                }
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)getCategory:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi getCategory:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSArray *dataArr = [resultDic objectForKey:@"d"];
            //缓存在本地一份
            [UserDefaultsUtils saveValue:dataArr forKey:kGoodsCategory];
            if (dataArr && dataArr.count > 0) {
                NSMutableArray *mbArr = [NSMutableArray arrayWithCapacity:dataArr.count];
                for (NSDictionary *dic in dataArr) {
                    QMGoodsCateInfo *cate = [[QMGoodsCateInfo alloc]initWithDictionary:dic];
                    [mbArr addObject:cate];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (success) {
                        success(mbArr);
                    }
                });
            }
            else{
                //如果从服务器拉取失败了，再从本地拉取一次
                [self getCategoryFromLocal:success failed:failed];
            }
        }
        else{
            [self getCategoryFromLocal:success failed:failed];
        }
    }];
}

- (void)getCategoryFromLocal:(SuccessBlock)success failed:(FailedBlock)failed
{
    //先读取本地的
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSArray *dataArr = [UserDefaultsUtils valueWithKey:kGoodsCategory];
        if (dataArr && dataArr.count > 0) {
            NSMutableArray *mbArr = [NSMutableArray arrayWithCapacity:dataArr.count];
            for (NSDictionary *dic in dataArr) {
                QMGoodsCateInfo *cate = [[QMGoodsCateInfo alloc]initWithDictionary:dic];
                [mbArr addObject:cate];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (success) {
                    success(mbArr);
                }
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                if (failed) {
                    failed(@"获取分类列表失败");
                }
            });
        }
    });
}

//获取引导页商品列表
-(void)getIntroGoodsListSuccess:(SuccessBlock)success failed:(FailedBlock)failed{
     [_goodsApi getIntroGoodsList:^(id result) {
     //        NSLog(@"result=%@",result);
     if ([result[@"s"]intValue]==0) {
            NSMutableArray *arrResult=[NSMutableArray arrayWithCapacity:4];
            NSArray *arrTemp=[result objectForKey:@"d"];
            for (NSDictionary *dict in arrTemp) {
                  GoodEntity *entity=[[GoodEntity alloc] initWithDictionary:dict];
                  [arrResult addObject:entity];
            }
            if (success) {
                  success(arrResult);
            }
      }else{
            if (failed) {
                  failed(result[@"d"]);
            }
       }
     }];
}

- (void)getCategoryByDesc:(NSString *)desc success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi getCategoryByDesc:desc context:^(id result) {
        if ([result[@"s"] intValue] == 0) {
            id cateId = [[result objectForKey:@"d"] objectForKey:@"category"];
            if (success) {
                success(cateId);
            }
        }
        else{
            if (failed) {
                failed(result[@"d"]);
            }
        }
    }];
}

- (void)topicsGoodsFromSearch:(NSString *)requestURL success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi topicsGoodsList:requestURL context:^(id result) {
        if(result && [result isKindOfClass:[NSDictionary class]]) {
            if(0 == [[result objectForKey:@"s"] intValue]) {
                id temp = [result objectForKey:@"d"];
                if (temp && [temp isKindOfClass:[NSDictionary class]]) {
                    NSMutableArray * ret = [[NSMutableArray alloc] init];
                    id temp2 = [temp objectForKey:@"list"];
                    if (temp2 && [temp2 isKindOfClass:[NSArray class]]) {
                        for (NSDictionary * dict in temp2) {
                            GoodEntity * info = [[GoodEntity alloc] initWithDictionary:dict];
                            [ret addObject:info];
                        }
                        
                    }
                    
                    temp2 = [temp objectForKey:@"offset"];
                    NSDictionary *retDic = @{@"list":ret, @"offset":temp2};
                    
                    if (success) {
                        success(retDic);
                    }
                }
            } else {
                if(failed) {
                    failed(result[@"d"]);
                }
            }
        }
    }];
}


/********************退款**********************/
- (void)sellerRefund:(NSString *)orderId success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi sellerRefund:orderId context:^(id result) {
        if ([result[@"s"] intValue] == 0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
            //数据库更新
            [_goodsHelper asyncUpdateOrderStatus:nil bSeller:NO orderId:orderId status:QMOrderStatusRefundClosed];
            
            //UI
            NSDictionary * dict = @{@"orderId":orderId, @"status":@(QMOrderStatusRefundClosed)};
            [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateOrderStatusNotify object:nil userInfo:dict];
        }
        else{
            if (failed) {
                failed(result[@"d"]);
            }
        }
    }];
}

- (void)applyRefund:(NSString *)orderId amount:(float)amout reason:(NSString *)reason success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi applyRefund:orderId amount:amout reason:reason context:^(id result) {
        if ([result[@"s"] intValue] == 0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
            //数据库更新
            [_goodsHelper asyncUpdateOrderStatus:nil bSeller:NO orderId:orderId status:QMOrderStatusRefundApply];
            
            //UI
            NSDictionary * dict = @{@"orderId":orderId, @"status":@(QMOrderStatusRefundApply)};
            [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateOrderStatusNotify object:nil userInfo:dict];
        }
        else{
            if (failed) {
                failed(result[@"d"]);
            }
        }
    }];
}

- (void)acceptRefund:(NSString *)orderId success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi acceptRefund:orderId context:^(id result) {
        if ([result[@"s"] intValue] == 0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
            //数据库更新
            [_goodsHelper asyncUpdateOrderStatus:nil bSeller:NO orderId:orderId status:QMOrderStatusRefundClosed];
            
            //UI
            NSDictionary * dict = @{@"orderId":orderId, @"status":@(QMOrderStatusRefundClosed)};
            [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateOrderStatusNotify object:nil userInfo:dict];
        }
        else{
            if (failed) {
                failed(result[@"d"]);
            }
        }
    }];
}

- (void)rejectRefund:(NSString *)orderId reason:(NSString *)reason success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi rejectRefund:orderId reason:reason context:^(id result) {
        if ([result[@"s"] intValue] == 0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
        }
        else{
            if (failed) {
                failed(result[@"d"]);
            }
        }
    }];
}

- (void)exchangeGoods:(int)num goodsId:(NSString *)goodsId addressId:(int)addressId success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_goodsApi exchangeGoods:num goodsId:goodsId addressId:addressId context:^(id result) {
        if ([result[@"s"]intValue] == 0) {
            if (success) {
                success([result[@"d"] objectForKey:@"hongbao"]);
            }
        }
        else{
            if (failed) {
                failed(result[@"d"]);
            }
        }
    }];
}

@end
