//
//  UnderLineLabel.m
//  QMMM
//
//  Created by kingnet  on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "UnderLineLabel.h"

@interface UnderLineLabel ()
{
    CALayer *lineLayer_;
}
@end
@implementation UnderLineLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    if (!lineLayer_) {
        lineLayer_ = [CALayer layer];
        lineLayer_.frame = CGRectMake(0, CGRectGetHeight(self.frame)-2.f, CGRectGetWidth(self.frame), 2.f);
        [self.layer addSublayer:lineLayer_];
        self.textColor = UIColorFromRGB(0x4991fc);
    }
    self.textAlignment = NSTextAlignmentCenter;
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    //计算文本宽度
    CGSize size = [text sizeWithFont:self.font];
    lineLayer_.frame = CGRectMake(0, size.height, size.width, 2.f);
}

- (void)setTextColor:(UIColor *)textColor
{
    [super setTextColor:textColor];
    lineLayer_.backgroundColor = textColor.CGColor;
}

- (void)addTouchWithTarget:(id)target selector:(SEL)selector
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:target action:selector];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGesture];
    self.userInteractionEnabled = YES;
}


@end
