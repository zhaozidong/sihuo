//
//  SettingsFooterView.m
//  QMMM
//
//  Created by kingnet  on 14-11-5.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "SettingsFooterView.h"

@interface SettingsFooterView ()
@property (weak, nonatomic) IBOutlet UILabel *versionLbl;

@end

@implementation SettingsFooterView

+ (SettingsFooterView *)footerView
{
    NSArray *arrray = [[NSBundle mainBundle]loadNibNamed:@"SettingsFooterView" owner:self options:0];
    return [arrray lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.versionLbl.text = @"";
    self.versionLbl.textColor = UIColorFromRGB(0xe0e0e0);
}

- (void)setCurVersion:(NSString *)curVersion
{
    if (curVersion) {
        self.versionLbl.text= curVersion;
    }
}

@end
