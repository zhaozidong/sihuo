//
//  AlbumViewController.h
//  QMMM
//
//  Created by kingnet  on 14-11-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"
@protocol AlbumViewControllerDelegate <NSObject>

- (void)choosePhotosFromAlbum:(NSArray *)photos;

@end
@interface AlbumViewController : BaseViewController

@property (nonatomic, strong) NSMutableArray *selectedPhotos;
@property (assign, nonatomic) int totalCount; //总共可以添加的数量
@property (weak, nonatomic) id<AlbumViewControllerDelegate> delegate;

@end
