//
//  UserInfoApi.m
//  QMMM
//
//  Created by kingnet  on 14-9-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "UserInfoApi.h"
#import "APIConfig.h"
#import "QMHttpClient.h"
#import "UserDefaultsUtils.h"
#import <AdSupport/AdSupport.h>

@implementation UserInfoApi

- (void)saveMsgServerPwd:(NSDictionary *)allHeaders
{
    NSString *pwd = [allHeaders objectForKey:@"Chat-Token"];
    if (pwd && [pwd isKindOfClass:[NSString class]] && ![pwd isEqualToString:@""]) {
        [UserDefaultsUtils saveValue:pwd forKey:kMsgServerPwd];
    }
    else{
        [UserDefaultsUtils saveValue:pwd forKey:@""];
    }
}

- (void)uploadLifePhotoData:(NSData *)imgData uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))uploadProgressBlock context:(contextBlock)context
{
    __block NSString *path = [[self class]uploadFileWithPath:[NSString stringWithFormat:API_UPLOAD_FILE, @"life_photo"]];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]uploadFileWithPath:path data:imgData contentType:@"*/*" success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        } uploadProgressBlock:uploadProgressBlock];
    });

}

- (void)uploadAvatarData:(NSData *)imgData context:(contextBlock)context
{
    __block NSString *path = [[self class]uploadFileWithPath:[NSString stringWithFormat:API_UPLOAD_FILE, @"avatar"]];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]uploadFileWithPath:path data:imgData contentType:@"*/*" success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        } uploadProgressBlock:nil];
    });
}

- (void)uploadLifePhotos:(NSArray *)photos context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_LIFE_PHOTOS];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = [NSDictionary dictionaryWithObject:photos forKey:@"photo"];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"upload life photo image error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"upload life photo image error:%@", error.description);
        }];
        
    });

}

- (void)updateUserAvatar:(NSString *)avatar nickName:(NSString *)nickName email:(NSString *)email motto:(NSString *)motto location:(NSString *)location context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_UPDATE_USERINFO];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"avatar":avatar, @"nickname":nickName, @"email":email, @"motto":motto, @"location":location};
  
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"update user info error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"update user info error:%@", error.description);
        }];
        
    });
    
}

- (void)uploadContacts:(NSDictionary *)contacts context:(contextBlock)context
{
    __block NSString *path = [[self class]httpsRequestUrlWithPath:API_UPLOAD_CONTACTS];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = [NSDictionary dictionaryWithObject:contacts forKey:@"contacts"];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"upload contacts error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"upload contacts error:%@", error.description);
        }];
        
    });
}

- (void)executeLoginWithUserName:(NSString *)userName password:(NSString *)password context:(contextBlock)context
{
    __block NSString *path = [[self class]httpsRequestUrlWithPath:API_LOGIN];
    
    __block contextBlock bContext = context;
    
    NSDictionary *baseInfo = @{@"username":userName, @"password":password, @"type":@"default", @"captcha":@"", @"welcome":@(YES)};
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:baseInfo];
    [params addEntriesFromDictionary:[AppUtils systemInfo]];
    
    [params setObject:@"" forKey:@"mobile"];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                //保存cookie
                [AppUtils saveCookies:[operation.response allHeaderFields]];
                //存储还信密码
                [self saveMsgServerPwd:operation.response.allHeaderFields];
                
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"login error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"login error:%@", error.description);
        }];
        
    });
}

- (void)executeRegisterWithMobile:(NSString *)mobile password:(NSString *)password checkCode:(NSString *)checkCode avatar:(NSString *)avatar nickname:(NSString *)nickname context:(contextBlock)context
{
    __block NSString *path = [[self class]httpsRequestUrlWithPath:API_REGISTER];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"mobile":mobile, @"password":password, @"captcha":checkCode, @"avatar":avatar, @"nickname":nickname};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                //保存cookie
                [AppUtils saveCookies:[operation.response allHeaderFields]];
                //存储还信密码
                [self saveMsgServerPwd:operation.response.allHeaderFields];
                
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"register error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"register error:%@", error.description);
        }];
        
    });

}

- (void)executeChangePwdWithPwd:(NSString *)pwd oldPwd:(NSString *)oldPwd mobile:(NSString *)mobile context:(contextBlock)context
{
    __block NSString *path = [[self class]httpsRequestUrlWithPath:API_CHANGE_PWD];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"new_password":pwd, @"old_password":oldPwd, @"mobile":mobile};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                //存储还信密码
                [self saveMsgServerPwd:operation.response.allHeaderFields];
                
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"change pwd error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"change pwd error:%@", error.description);
        }];
        
    });

}

- (void)executeFindPwdWithNewPwd:(NSString *)pwd checkCode:(NSString *)checkCode phoneNum:(NSString *)phoneNum context:(contextBlock)context
{
    __block NSString *path = [[self class]httpsRequestUrlWithPath:API_FIND_PWD];
    
    __block contextBlock bContext = context;
    
    NSString *udid = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    NSDictionary *params = @{@"mobile":phoneNum, @"new_password":pwd, @"captcha":checkCode, @"did":udid};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                //保存cookie
                [AppUtils saveCookies:[operation.response allHeaderFields]];
                //存储还信密码
                [self saveMsgServerPwd:operation.response.allHeaderFields];
                
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"register error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"register error:%@", error.description);
        }];
        
    });

}

- (void)registerDeviceToken:(NSString *)deviceToken context:(contextBlock)context
{
    __block NSString *path = [[self class]httpsRequestUrlWithPath:API_REGISTER_DEVICE_TOKEN];
    
    __block contextBlock bContext = context;
    
    int env = 2; //开发环境上报devicetoken

#ifdef DEBUG
    env = 3;
#else
    if (kIsTestRelease) {
        env = 2;
    }
    else{
        env = 1; //正式发布环境时
    }
#endif
    NSDictionary *params = @{@"device_type":@(2), @"device_token":deviceToken, @"env":@(env)};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"registerDeviceToken error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"registerDeviceToken error:%@", error.description);
        }];
        
    });
}

- (void)getUserBasicInfo:(NSArray *)userList context:(contextBlock)context
{
    if(userList && [userList count] > 0) {
        NSString * userListString = [userList componentsJoinedByString:@","];
        NSString * params = [NSString stringWithFormat:@"%@&uid=%@&fields=uid,nickname,avatar,remark", API_GET_USER_BASIC_INFO, userListString];
         __block NSString *path = [[self class]requestUrlWithPath:params];
        __block contextBlock bContext = context;
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSError *error = nil;
                id result = [self parseData:responseObject error:&error];
                
                if (result) {
                    NSDictionary *dic = @{@"s":@0, @"d":result};
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (bContext) {
                            bContext(dic);
                        }
                    });
                }
                else{
                    NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (bContext) {
                            bContext(dic);
                        }
                    });
                    DLog(@"getUserBasicInfo error:%@",error.userInfo[NSLocalizedDescriptionKey]);
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSDictionary *dic;
                if (error.code == -1001) {
                    dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
                }
                else{
                    dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"getUserBasicInfo error:%@", error.description);
            }];
            
        });
        
    }
}

- (void)getOrderStateNum:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_ORDERSTATE_NUM];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"registerDeviceToken error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"registerDeviceToken error:%@", error.description);
        }];
        
    });
}

- (void)userLogout:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_LOGOUT];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"user logout error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"user logout error:%@", error.description);
        }];
        
    });
}

- (void)smsVerify:(NSString *)type mobile:(NSString *)mobile captcha:(NSString *)captcha context:(contextBlock)context
{
    __block NSString *path = [[self class]requestUrlWithPath:API_VERIFY_CHECKCODE];
    
    __block contextBlock bContext = context;
    
    NSDictionary *params = @{@"type":type, @"mobile":mobile, @"captcha":captcha};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            }
            else{
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"verify sms error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            DLog(@"verify sms error:%@", error.description);
        }];
        
    });
}

- (void)newerHongBao:(contextBlock)context
{
    __block NSString *path = [[self class]httpsRequestUrlWithPath:API_HONGBAO_MONEY];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self processResponse:responseObject error:nil operation:operation context:bContext];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self processResponse:nil error:error operation:operation context:bContext];
        }];
        
    });
}

@end
