//
//  MinePhotoCell.h
//  QMMM
//
//  Created by kingnet  on 14-9-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MinePhotoCellType){
    QMShowMinePhoto,
    QMEditMinePhoto
};

@protocol MinePhotoCellDelegate <NSObject>

@optional
- (void)didTapAddPhoto;

@end

@class UserEntity, UserDataInfo;

@interface MinePhotoCell : UITableViewCell

@property (nonatomic, weak) id<MinePhotoCellDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *photoArray;

@property (nonatomic, assign) NSInteger mainImageIndex;

@property (nonatomic, assign) MinePhotoCellType photoType;

+ (MinePhotoCell *)minePhotoCell;

+ (CGFloat)rowHeight;

+ (CGFloat)itemBorder;

//进入编辑资料时更新生活照
- (void)updateUI:(UserEntity *)entity;

//个人主页处的更新，不是本人时
- (void)updateUIWithUserDataInfo:(UserDataInfo *)userInfo;

//从相机回来后添加到界面上
- (void)addPhotos:(NSArray *)photos;

//拿到生活照的路径，包括key，如果不需要上传，返回nil
- (NSArray *)keysArray;

@end
