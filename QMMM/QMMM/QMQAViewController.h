//
//  QMQAViewController.h
//  QMMM
//
//  Created by Derek on 15/3/11.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSUInteger, QMQAType){
    QMQATypeSiHuo=1,//私货是什么
    QMQATypeAddFriend,//如何邀请和添加好友
    QMQATypeKeXinDu,//可信度是什么
    QMQATypeYingXiangLi,//影响力是什么
    QMQATypeTotalVolume,//成交量是什么
    QMQATypeHaoPingLv,//好评率是什么
    QMQATypeAddPhotos,//如何添加照片
    QMQATypeDescribing,//如何描述
    QMQATypeMakePrice,//如何给商品定合理的价格
    QMQATypeAudio,//语音有什么用
    QMQATypeCondition,//如何判定商品的新旧程度
    QMQATypeHowToBuy,//购买支付流程
    QMQATypeCash,//现金是什么
    QMQATypeHongBao,//红包是什么
    QMQATypePrivateTrade,//私下交易无保障
    QMQATypeJustWantCheap,//切勿贪便宜，因小失大
    QMQATypeReportInTime,//及时举报
    QMQATypeBelieveTrade//和可信的人交易
};

@interface QMQAViewController : BaseViewController


-(id)initWithURL:(NSString *)URL;

-(id)initWithType:(QMQAType)type;


@end
