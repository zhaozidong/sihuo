//
//  MineUserInfoCell.h
//  QMMM
//
//  Created by kingnet  on 15-1-19.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserEntity, PersonInfluence;

@protocol MineUserInfoCellDelegate <NSObject>

- (void)didTapAvatar; //头像

@end

@interface MineUserInfoCell : UITableViewCell

@property (nonatomic, weak) id<MineUserInfoCellDelegate> delegate;

+ (MineUserInfoCell *)userInfoCell;

+ (CGFloat)cellHeight;

- (void)updateUI:(UserEntity *)entity;

@end
