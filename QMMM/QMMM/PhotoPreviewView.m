//
//  PhotoPreviewView.m
//  QMMM
//
//  Created by Shinancao on 14-9-21.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PhotoPreviewView.h"
#import "QMSubmitButton.h"
#import "MACircleProgressIndicator.h"
#import "UIImageView+QMWebCache.h"
#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/SDImageCache.h>

@interface PhotoPreviewView ()<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidth;
@property (weak, nonatomic) IBOutlet QMSubmitButton *mainPhotoBtn;
@property (nonatomic, strong) MACircleProgressIndicator *progress; //下载进度条

@property (nonatomic, strong) UIView *backgroundView; //防止在动画执行

@end

@implementation PhotoPreviewView
@synthesize image = _image;

//获取图片和显示视图宽度的比例系数
- (float)getImgWidthFactor {
    return self.bounds.size.width / self.image.size.width;
}
//获取图片和显示视图高度的比例系数
- (float)getImgHeightFactor {
    return (self.bounds.size.height-60) / self.image.size.height;
}

//获取尺寸
- (CGSize)newSizeByoriginalSize:(CGSize)oldSize maxSize:(CGSize)mSize
{
    if (oldSize.width <= 0 || oldSize.height <= 0) {
        return CGSizeZero;
    }
    
    CGSize newSize = CGSizeZero;
    if (oldSize.width > mSize.width || oldSize.height > mSize.height) {
        //按比例计算尺寸
        float bs = mSize.width / oldSize.width;
        float newHeight = oldSize.height * bs;
        newSize = CGSizeMake(mSize.width, newHeight);
        if (newHeight > mSize.height) {
            bs = mSize.height / oldSize.height;
            float newWidth = oldSize.width * bs;
            newSize = CGSizeMake(newWidth, mSize.height);
        }
    }else {
        
        newSize = oldSize;
    }
    return newSize;
}

- (CGSize)sizeFatBorderWidth:(CGSize)mSize oldSize:(CGSize)oldSize
{
    if (oldSize.width <= 0 || oldSize.height <= 0) {
        return CGSizeZero;
    }
    
    CGSize newSize = CGSizeZero;
    //按比例计算尺寸
    float bs = mSize.width / oldSize.width;
    float newHeight = oldSize.height * bs;
    newSize = CGSizeMake(mSize.width, newHeight);
    
    if (newHeight > mSize.height) {
        bs = mSize.height / oldSize.height;
        float newWidth = oldSize.width * bs;
        newSize = CGSizeMake(newWidth, mSize.height);
    }
    return newSize;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (PhotoPreviewView *)photoPreviewView
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"PhotoPreviewView" owner:self options:nil];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.frame = CGRectMake(0, kMainFrameHeight, kMainFrameWidth, kMainFrameHeight);
    self.imageViewHeight.constant = CGRectGetWidth(self.frame);
    self.imageViewWidth.constant = CGRectGetHeight(self.frame);
    [self layoutIfNeeded];
    
    self.progress = [[MACircleProgressIndicator alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    self.progress.value = 0.0;
    CGSize vsize = self.frame.size;
    self.progress.frame = CGRectMake((vsize.width - kDefaultProgressSize) * 0.5, (vsize.height - 60 - kDefaultProgressSize) * 0.5, kDefaultProgressSize, kDefaultProgressSize);
    self.progress.strokeWidth = 4;
    self.progress.color = [UIColor whiteColor];
    [self addSubview:self.progress];
    self.progress.hidden = YES;
}

- (UIView *)backgroundView
{
    if (!_backgroundView) {
        _backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight)];
        _backgroundView.backgroundColor = [UIColor clearColor];
    }
    return _backgroundView;
}

- (void)setPreviewType:(QMPreviewType)previewType
{
    if (previewType == QMGoodsPreview) {
        [self.mainPhotoBtn setTitle:@"设为主图" forState:UIControlStateNormal];
    }
    else if (previewType == QMLifePhotoPreview){
        [self.mainPhotoBtn setTitle:@"设为头像" forState:UIControlStateNormal];
        self.mainPhotoBtn.hidden = YES;
    }
}

- (UIImage *)image
{
    return self.imageView.image;
}

- (void)setImage:(UIImage *)image
{
    self.imageView.image = image;
    if (image) {
        float scale = [UIScreen mainScreen].scale;
        CGSize imgSize;
        if (scale == image.scale) {
            imgSize = image.size;
        }
        else{
            imgSize = CGSizeMake(image.size.width/scale, image.size.height/scale);
        }
        CGSize size = [self sizeFatBorderWidth:CGSizeMake(self.bounds.size.width, self.bounds.size.height-60) oldSize:imgSize];
        self.imageViewHeight.constant = size.height;
        self.imageViewWidth.constant = size.width;
        [self layoutIfNeeded];
    }
}

- (void)setSmallImage:(UIImage *)image
{
    self.imageView.image = image;
    if (image) {
        float scale = [UIScreen mainScreen].scale;
        CGSize imgSize;
        if (scale == image.scale) {
            imgSize = image.size;
        }
        else{
            imgSize = CGSizeMake(image.size.width/scale, image.size.height/scale);
        }
        CGSize size = [self newSizeByoriginalSize:imgSize maxSize:CGSizeMake(self.bounds.size.width, self.bounds.size.height-60)];
        self.imageViewHeight.constant = size.height;
        self.imageViewWidth.constant = size.width;
        [self layoutIfNeeded];
    }
}

- (void)setFilePath:(NSString *)filePath
{
    _filePath = filePath;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:_filePath]) {
        UIImage *tmpImage = [UIImage imageWithContentsOfFile:_filePath];
        [self setImage:tmpImage];
    }
    else{
        //这地方要换成下图的路径
        NSString *absoluteurl = [AppUtils getAbsolutePath:_filePath];
        NSURL *url = [NSURL URLWithString:absoluteurl];
 
        //如果已经下载过了
        UIImage *cachedImage = [[SDImageCache sharedImageCache]imageFromDiskCacheForKey:absoluteurl];
        if (cachedImage) {
            [self setImage:cachedImage];
        }
        else{
            self.progress.hidden = NO;
            
            //加载小图
            NSString *smallImagePath = [AppUtils getAbsolutePath:[AppUtils thumbPath:_filePath sizeType:self.imageSizeType]];
            UIImage *smallCachedImage = [[SDImageCache sharedImageCache]imageFromDiskCacheForKey:smallImagePath];
            if (smallCachedImage) {
                [self setSmallImage:smallCachedImage];
            }
            
            PhotoPreviewView __weak *weakSelf = self;
            [[SDWebImageManager sharedManager]downloadImageWithURL:url options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                if (expectedSize > 0) {
                    weakSelf.progress.value = (float) receivedSize/expectedSize;
                }
                
            } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                if (finished) {
                    if (!error) {
                        weakSelf.progress.value = 1.0;
                        weakSelf.progress.hidden = YES;
                        if (image) {
                            
                            [weakSelf setDownloadImage:image];
                        }
                    }
                    else{
                        DLog(@"download image error:%@", error.description);
                    }
                }
                
            }];
        }
        
    }
}

- (void)setDownloadImage:(UIImage *)image
{
    if (self.image == nil) {
        [self setImage:image];
        return;
    }
    
    //动画
    [UIView beginAnimations:nil context:nil];
    //动画结束时变缓慢
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationRepeatCount:1];
    
    [self setImage:image];
    [UIView commitAnimations];
}

- (void)setIsMainPhoto:(BOOL)isMainPhoto
{
    _isMainPhoto = isMainPhoto;
    self.mainPhotoBtn.enabled = !isMainPhoto;
}

- (void)show
{
    [[UIApplication sharedApplication].keyWindow addSubview:self.backgroundView];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight);
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(0, kMainFrameHeight, kMainFrameWidth, kMainFrameHeight);
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
            [self.backgroundView removeFromSuperview];
        }
    }];
}

- (IBAction)cancelAction:(id)sender {
    [self dismiss];
}
- (IBAction)deleteAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteImageWithPath:)]) {
        [self dismiss];
        [self.delegate deleteImageWithPath:_indexPath];
    }
}
- (IBAction)mainPhotoAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(makeMainPhoto:)]) {
        [self dismiss];
        [self.delegate makeMainPhoto:_filePath];
    }
}

@end
