//
//  MyAddressViewController.m
//  QMMM
//
//  Created by kingnet  on 14-9-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MyAddressViewController.h"
#import "AddAddressCell.h"
#import "AddressListCell.h"
#import "EditAddressViewController.h"
#import "AddressHandler.h"
#import "AddressEntity.h"
//#import "EditAddressViewController.m"

@interface MyAddressViewController ()<UITableViewDataSource, UITableViewDelegate, RMSwipeTableViewCellDelegate, AddressListCellDelegate, UIAlertViewDelegate>{
    
    BOOL _isEdit;
}

@property (nonatomic) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *addressList;
@property (nonatomic) AddAddressCell *addAddressCell;
@property (nonatomic, strong) AddressHandler *handler;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation MyAddressViewController

- (id)initWithAddressId:(int)addressId addressBlock:(AddressBlock)addressBlock
{
    self = [super init];
    if (self) {
        self.addressId = addressId;
        self.addressBlock = [addressBlock copy];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"收货地址";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    self.navigationItem.rightBarButtonItems=[AppUtils createRightButtonWithTarget:self selector:@selector(btnDidEdit:) title:@"编辑" size:CGSizeMake(50, 40) imageName:nil];
    self.canSwipCloseKeyboard = NO;
    
    _isEdit=NO;
    self.addressList = [NSMutableArray array];
    self.handler = [[AddressHandler alloc]init];
    
    [self.view addSubview:self.tableView];
    
    [self.tableView registerClass:[AddressListCell class] forCellReuseIdentifier:kAddressCellIdentifier];
    
    //拉取数据
    [self getAddressList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)btnDidEdit:(id)sender{
    UIButton *btnRight=(UIButton *)sender;
    if (_isEdit) {
        _isEdit=!_isEdit;
        [btnRight setTitle:@"编辑" forState:UIControlStateNormal];
        [_tableView reloadData];
        [_tableView setEditing:NO animated:YES];
    }else{
        _isEdit=!_isEdit;
        [btnRight setTitle:@"完成" forState:UIControlStateNormal];
        [_tableView reloadData];
        [_tableView setEditing:YES animated:YES];
    }
}

-(void)endEdit{
    _isEdit=NO;
    self.navigationItem.rightBarButtonItems=nil;
    self.navigationItem.rightBarButtonItems=[AppUtils createRightButtonWithTarget:self selector:@selector(btnDidEdit:) title:@"编辑" size:CGSizeMake(50, 40) imageName:nil];
    [_tableView reloadData];
    [_tableView setEditing:NO animated:YES];
}


- (void)backAction:(id)sender
{
    if (self.needSelected) {
        //如果没有选择任何一个
        for (AddressEntity *entity in self.addressList) {
            if (entity.addressId == self.addressId) {
                
                self.addressBlock(entity);
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"辛辛苦苦的编辑就这么放弃了么？" message:nil delegate:self cancelButtonTitle:@"放弃" otherButtonTitles:@"继续", nil];
        [alert show];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex == buttonIndex) {
        return;
    }
    self.addressBlock(nil);
    [self.navigationController popViewControllerAnimated:YES];
}

//如果只有一条，则默认选上
- (void)setWhenOne
{
    if (self.addressList.count == 1) {
        AddressEntity *entity = [self.addressList lastObject];
        self.addressId = entity.addressId;
        [self.tableView reloadData];
    }
}

#pragma mark - Get Data
- (void)getAddressList
{
    [AppUtils showProgressMessage:@"加载中"];
    
    MyAddressViewController __weak * weakSelf = self;
    
    [self.handler getAddressList:^(id obj) {
        [AppUtils dismissHUD];
        
        NSMutableArray *array = (NSMutableArray *)obj;
        weakSelf.addressList = array;
        
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)setAddressList:(NSMutableArray *)addressList
{
    [_addressList removeAllObjects];
    if (!_addressList) {
        _addressList = [[NSMutableArray alloc]initWithCapacity:addressList.count];
    }
    [_addressList addObjectsFromArray:addressList];
    //如果前一个页面没有给选择，则选择默认的
    if (self.addressId == 0) {
        for (AddressEntity *entity in _addressList) {
            if (entity.isDefault) {
                self.addressId = entity.addressId;
                break;
            }
        }
    }
    [self.tableView reloadData];
}

#pragma mark - Getter
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
    }
    return _tableView;
}

- (AddAddressCell *)addAddressCell
{
    if (!_addAddressCell) {
        _addAddressCell = [AddAddressCell addAddressCell];
    }
    return _addAddressCell;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_isEdit) {
        return self.addressList.count;
    }
    return self.addressList.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section == 0) {
//        return [AddAddressCell cellHeight];
//    }
//    else{
//        AddressEntity *entity = (AddressEntity *)[self.addressList objectAtIndex:indexPath.section-1];
//        return [AddressListCell cellHeight:entity.detailAddress];
//    }
    
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && !_isEdit) {
        return self.addAddressCell;
    }
    else{
        AddressListCell *cell = (AddressListCell *)[tableView dequeueReusableCellWithIdentifier:kAddressCellIdentifier];
        NSInteger index=_isEdit? indexPath.section:indexPath.section-1;
        AddressEntity *entity = (AddressEntity *)[self.addressList objectAtIndex:index];
        cell.delegate = self;
        if (self.needSelected) {
            cell.isSelected = (self.addressId == entity.addressId);
        }
        else{
            cell.isSelected = NO;
        }
        if (_isEdit) {
            [cell updateUI:entity withMode:addressEditModeEdit];
        }else{
            [cell updateUI:entity withMode:addressEditModeView];
        }
        
//        cell.editingAccessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.editingAccessoryView=[self rightArrowButtonWithTag:index];
        cell.cellDelegate = self;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (UITableViewCellEditingStyleDelete==editingStyle) {//删除数据
        AddressEntity *entity = [self.addressList objectAtIndex:indexPath.section];
        MyAddressViewController __weak *weakSelf = self;
        [AppUtils showProgressMessage:@"正在提交"];
        [self.handler deleteAddress:entity success:^(id obj) {
            [AppUtils dismissHUD];
            //删除的是选中的但不是默认的
            if (entity.addressId == weakSelf.addressId && !entity.isDefault) {
                //找默认的选上
                [weakSelf.addressList removeObjectAtIndex:indexPath.section];
                for (AddressEntity *e in weakSelf.addressList) {
                    if (e.isDefault) {
                        weakSelf.addressId = e.addressId;
                        break;
                    }
                }
            }
            //删除的是默认的但不是选中的
            else if (entity.addressId != weakSelf.addressId && entity.isDefault) {
                if (weakSelf.addressList.count>0) {
                    [weakSelf.addressList removeObjectAtIndex:indexPath.section];
                    AddressEntity *e = [weakSelf.addressList firstObject];
                    e.isDefault = YES;
                }
            }
            //删除的即是选中的也是默认的
            else if (entity.addressId == weakSelf.addressId && entity.isDefault) {
                [weakSelf.addressList removeObjectAtIndex:indexPath.section];
                AddressEntity *e = [weakSelf.addressList firstObject];
                e.isDefault = YES;
                weakSelf.addressId = e.addressId;
            }
            //删除的即不是选中的也不是默认的
            else if (entity.addressId != weakSelf.addressId && !entity.isDefault) {
                [weakSelf.addressList removeObjectAtIndex:indexPath.section];
            }
            [weakSelf.tableView reloadData];
        } failed:^(id obj) {
            [AppUtils showErrorMessage:(NSString *)obj];
        }];
        
    }
}

//-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
//    
//    __weak typeof(self)weakSelf=self;
//    AddressEntity *entity = (AddressEntity *)[self.addressList objectAtIndex:indexPath.row];
//    
//    EditAddressViewController   *edit=[[EditAddressViewController alloc] initWithAddress:entity editBlock:^(AddressEntity *entity) {
//        NSLog(@"EditAddressViewController Block");
//        [weakSelf getAddressList];
//        [weakSelf.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
//        [weakSelf.navigationController popViewControllerAnimated:YES];
//    }];
//    //    edit.isUpdate=YES;
//    [self.navigationController pushViewController:edit animated:YES];
//}


-(UIButton *)rightArrowButtonWithTag:(NSUInteger)tag{
    UIButton *btnArrow=[UIButton buttonWithType:UIButtonTypeCustom];
    btnArrow.frame=CGRectMake(0, 0, 50, 50);
    [btnArrow setImage:[UIImage imageNamed:@"rightArrow"] forState:UIControlStateNormal];
    [btnArrow addTarget:self action:@selector(btnDidEditAddress:) forControlEvents:UIControlEventTouchUpInside];
    btnArrow.tag=tag;
    return btnArrow;
}


-(void)btnDidEditAddress:(id)sender{
    UIButton *btnSender=(UIButton *)sender;
    __weak typeof(self)weakSelf=self;
//    __block NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:btnSender.tag+1];
    AddressEntity *entity = (AddressEntity *)[self.addressList objectAtIndex:btnSender.tag];
    EditAddressViewController   *edit=[[EditAddressViewController alloc] initWithAddress:entity editBlock:^(AddressEntity *entity) {
        NSLog(@"EditAddressViewController Block");
        [weakSelf getAddressList];
        if (_initType==AddressInitTypeBuy) {
//            [weakSelf.navigationController popViewControllerAnimated:YES];
//            [weakSelf tableView:weakSelf.tableView didSelectRowAtIndexPath:indexPath];
        }else{
            [weakSelf endEdit];
        }
    }];
    [self.navigationController pushViewController:edit animated:YES];
}


#pragma mark - UITableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    NSLog(@"indexPath.section=%ld,indexPath.row=%ld",(long)indexPath.section,(long)indexPath.row);
    if (indexPath.section == 0) {
        MyAddressViewController __weak * weakSelf = self;
        EditAddressViewController *controller = [[EditAddressViewController alloc]initWithBlock:^(AddressEntity *entity) {
            if (entity.isDefault) {
                for (AddressEntity *e in weakSelf.addressList) {
                    e.isDefault = NO;
                }
            }
            [weakSelf.addressList addObject:entity];
            [weakSelf.tableView reloadData];
        }];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else{
        if (self.selectedIndexPath && self.selectedIndexPath.section == indexPath.section) {
            [self resetSelectedCell];
        }
        else{
            [self resetSelectedCell];
            if (self.needSelected) {
                AddressEntity *entity = (AddressEntity *)[self.addressList objectAtIndex:indexPath.section-1];
                self.addressId = entity.addressId;
                [self.tableView reloadData];
                self.addressBlock(entity);
                [self.navigationController popViewControllerAnimated:YES];
            }
            else{
                AddressListCell *cell = (AddressListCell *)[tableView cellForRowAtIndexPath:indexPath];
                [self cellDidUpdate:cell];
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 14.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

#pragma mark - AddressCellDelegate methods
- (void)cellDidDelegate:(AddressListCell *)cell
{
    [cell resetContentView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AddressEntity *entity = [self.addressList objectAtIndex:indexPath.section-1];
    
    MyAddressViewController __weak *weakSelf = self;
    [AppUtils showProgressMessage:@"正在提交"];
    [self.handler deleteAddress:entity success:^(id obj) {
        [AppUtils dismissHUD];
        //删除的是选中的但不是默认的
        if (entity.addressId == weakSelf.addressId && !entity.isDefault) {
            //找默认的选上
            [weakSelf.addressList removeObjectAtIndex:indexPath.section-1];
            for (AddressEntity *e in weakSelf.addressList) {
                if (e.isDefault) {
                    weakSelf.addressId = e.addressId;
                    break;
                }
            }
        }
        //删除的是默认的但不是选中的
        if (entity.addressId != weakSelf.addressId && entity.isDefault) {
            if (weakSelf.addressList.count>0) {
                [weakSelf.addressList removeObjectAtIndex:indexPath.section-1];
                AddressEntity *e = [weakSelf.addressList firstObject];
                e.isDefault = YES;
            }
        }
        //删除的即是选中的也是默认的
        if (entity.addressId == weakSelf.addressId && entity.isDefault) {
            [weakSelf.addressList removeObjectAtIndex:indexPath.section-1];
            AddressEntity *e = [weakSelf.addressList firstObject];
            e.isDefault = YES;
            weakSelf.addressId = e.addressId;
        }
        //删除的即不是选中的也不是默认的
        if (entity.addressId != weakSelf.addressId && !entity.isDefault) {
            [weakSelf.addressList removeObjectAtIndex:indexPath.section-1];
        }
        [weakSelf.tableView reloadData];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)cellDidUpdate:(AddressListCell *)cell
{
    [cell resetContentView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AddressEntity *entity = [self.addressList objectAtIndex:indexPath.section-1];
    MyAddressViewController __weak *weakSelf = self;
    EditAddressViewController *controller = [[EditAddressViewController alloc]initWithAddress:entity editBlock:^(AddressEntity *entity) {
        //替换原来的entity 刷新界面
        for (AddressEntity *e in weakSelf.addressList) {
            if (e.addressId == entity.addressId) {
                [e assignWithEntity:entity];
            }
            else{
                if (entity.isDefault) {
                    e.isDefault = NO;
                }
            }
        }
        
        [weakSelf.tableView reloadData];
    }];
    
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - RMSwipeTableViewCell delegate methods
//-(void)swipeTableViewCellDidStartSwiping:(RMSwipeTableViewCell *)swipeTableViewCell {
//    NSIndexPath *indexPathForCell = [self.tableView indexPathForCell:swipeTableViewCell];
//    if (self.selectedIndexPath.section != indexPathForCell.section) {
//        [self resetSelectedCell];
//    }
//}

-(void)resetSelectedCell {
    AddressListCell *cell = (AddressListCell*)[self.tableView cellForRowAtIndexPath:self.selectedIndexPath];
    [cell resetContentView];
    self.selectedIndexPath = nil;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
}

//-(void)swipeTableViewCellWillResetState:(RMSwipeTableViewCell *)swipeTableViewCell fromPoint:(CGPoint)point animation:(RMSwipeTableViewCellAnimationType)animation velocity:(CGPoint)velocity {
//    if (velocity.x <= -500) {
//        self.selectedIndexPath = [self.tableView indexPathForCell:swipeTableViewCell];
//        swipeTableViewCell.shouldAnimateCellReset = NO;
//        swipeTableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
//        NSTimeInterval duration = MAX(-point.x / ABS(velocity.x), 0.10f);
//        [UIView animateWithDuration:duration
//                              delay:0
//                            options:UIViewAnimationOptionCurveLinear
//                         animations:^{
//                             swipeTableViewCell.contentView.frame = CGRectOffset(swipeTableViewCell.contentView.bounds, point.x - (ABS(velocity.x) / 150), 0);
//                         }
//                         completion:^(BOOL finished) {
//                             [UIView animateWithDuration:duration
//                                                   delay:0
//                                                 options:UIViewAnimationOptionCurveEaseOut
//                                              animations:^{
//                                                  swipeTableViewCell.contentView.frame = CGRectOffset(swipeTableViewCell.contentView.bounds, -80, 0); //80*2
//                                              }
//                                              completion:^(BOOL finished) {
//                                              }];
//                         }];
//    }
//    else if (velocity.x > -500 && point.x < -80) {
//        self.selectedIndexPath = [self.tableView indexPathForCell:swipeTableViewCell];
//        swipeTableViewCell.shouldAnimateCellReset = NO;
//        swipeTableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
//        NSTimeInterval duration = MIN(-point.x / ABS(velocity.x), 0.15f);
//        [UIView animateWithDuration:duration
//                         animations:^{
//                             swipeTableViewCell.contentView.frame = CGRectOffset(swipeTableViewCell.contentView.bounds, -80, 0); //80*2
//                         }];
//    }
//}

@end
