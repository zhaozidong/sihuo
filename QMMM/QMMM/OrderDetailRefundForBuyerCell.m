//
//  OrderDetailRefundForBuyerCell.m
//  QMMM
//
//  Created by kingnet  on 15-3-11.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "OrderDetailRefundForBuyerCell.h"
#import "OrderDataInfo.h"
#import "NSDate+Category.h"

@interface OrderDetailRefundForBuyerCell ()
@property (weak, nonatomic) IBOutlet UIButton *contactServiceBtn;
@property (weak, nonatomic) IBOutlet UILabel *contentLbl;

@end
@implementation OrderDetailRefundForBuyerCell

+ (OrderDetailRefundForBuyerCell *)refundForBuyerCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"OrderDetailRefundForBuyerCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    self.contentLbl.text = @"";
    self.contentLbl.numberOfLines = 0;
    self.contentLbl.textColor = kDarkTextColor;
    self.contentLbl.preferredMaxLayoutWidth = kMainFrameWidth-140;
    [self.contactServiceBtn setTitleColor:kRedColor forState:UIControlStateNormal];
    [self.contactServiceBtn addTarget:self action:@selector(contactServiceAction:) forControlEvents:UIControlEventTouchUpInside];
    UIImage * image1 = [UIImage imageNamed:@"border_btn_bkg"];
    [self.contactServiceBtn setBackgroundImage:[image1 resizableImageWithCapInsets:UIEdgeInsetsMake(10, 7, 10, 7)] forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)contactServiceAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapContactService)]) {
        [self.delegate didTapContactService];
    }
}

- (void)updateUI:(OrderDataInfo *)orderInfo
{
    if (orderInfo.order_status == QMOrderStatusRefundApply) {
        self.contentLbl.preferredMaxLayoutWidth = kMainFrameWidth-50;
        
        NSInteger dayNum = [[NSDate dateWithTimeIntervalSince1970:orderInfo.apply_ts] distanceInDaysToDate:[NSDate dateWithTimeIntervalSince1970:orderInfo.sys_ts]];
        NSString *days = [NSString stringWithFormat:@"%ld", (kRefundWaitDays-dayNum)];
        NSString *tips = [NSString stringWithFormat:@"已申请退款，卖家还有%@天来处理你的请求，逾期未处理，系统会将%.2f元直接汇入你的账户余额中。", days, orderInfo.refund_amount];
        NSRange range = [tips rangeOfString:days];
        NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:tips];
        [attributedStr addAttributes:@{NSForegroundColorAttributeName:kRedColor} range:range];
        self.contentLbl.attributedText = attributedStr;
        self.contactServiceBtn.hidden = YES;
    }
    else{
        self.contentLbl.preferredMaxLayoutWidth = kMainFrameWidth-140;
        self.contentLbl.text = [NSString stringWithFormat:@"退款申请被拒绝，拒绝理由：%@", orderInfo.rejectReason];
        self.contactServiceBtn.hidden = NO;
    }
}

@end
