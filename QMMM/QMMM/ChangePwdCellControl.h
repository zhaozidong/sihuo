//
//  ChangePwdCellControl.h
//  QMMM
//
//  Created by kingnet  on 14-10-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChangePwdCellControl : NSObject

@property (nonatomic, strong) NSArray *cellsArray;

@property (nonatomic, strong) NSString *oldPwd; //验证码

@property (nonatomic, strong) NSString *pwd;  //新密码

+ (CGFloat)rowHeight;

@end
