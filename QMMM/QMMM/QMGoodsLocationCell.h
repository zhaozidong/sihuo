//
//  QMGoodsLocationCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kGoodsLocationCellIdentifier;

@class GoodEntity;
@interface QMGoodsLocationCell : UITableViewCell

+ (CGFloat)cellHeight;

+ (QMGoodsLocationCell *)locationCell;

- (void)updateUI:(GoodEntity *)goodsEntity;

- (void)updateUI:(GoodEntity *)goodsEntity isPreView:(BOOL)isPrev;

@end
