//
//  QMFilterTableViewCell.m
//  QMMM
//
//  Created by kingnet  on 15-3-17.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMFilterTableViewCell.h"

NSString * const kQMFilterTableViewCellIdentifier = @"kQMFilterTableViewCellIdentifier";

@interface QMFilterTableViewCell ()
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UILabel *textLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineHeightCst;
@end
@implementation QMFilterTableViewCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"QMFilterTableViewCell" bundle:[NSBundle mainBundle]];
}

- (void)awakeFromNib {
    // Initialization code
    self.lineHeightCst.constant = 0.5;
    self.lineView.backgroundColor = kBorderColor;
    self.textLbl.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}

- (void)setText:(NSString *)text
{
    _text = text;
    self.textLbl.text = _text;
}

- (void)setChoosed:(BOOL)choosed
{
    _choosed = choosed;
    if (_choosed) {
        self.textLbl.textColor = kRedColor;
        self.lineView.backgroundColor = kRedColor;
    }
    else{
        self.textLbl.textColor = kDarkTextColor;
        self.lineView.backgroundColor = kBorderColor;
    }
}

@end
