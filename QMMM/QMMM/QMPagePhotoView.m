//
//  QMPagePhotoView.m
//  QMMM
//
//  Created by hanlu on 14-10-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMPagePhotoView.h"
#import "UIImageView+QMWebCache.h"

@interface QMPagePhotoView () {
//    NSMutableDictionary * _imageViewDict;
    NSMutableDictionary * _contentViewDict;
}

@end

@implementation QMPagePhotoView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        [self initView];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self initView];
}

- (void)initView
{
    CGRect bounds = self.bounds;
    
    int pageControlHeight = 20;
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, bounds.size.width, bounds.size.height)];
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, bounds.size.width, pageControlHeight)];
    
    [self addSubview:_scrollView];
    [self addSubview:_pageControl];
    
    // a page is the width of the scroll view
    _scrollView.pagingEnabled = YES;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.scrollsToTop = NO;
    _scrollView.delegate = self;
    
    _pageControl.numberOfPages = 0;
    _pageControl.currentPage = 0;
    _pageControl.backgroundColor = [UIColor clearColor];
    
//    _imageViewDict = [[NSMutableDictionary alloc] init];
    _contentViewDict = [[NSMutableDictionary alloc] init];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    NSInteger numbersofPage = [_datasource numberOfPage];
    _scrollView.frame = self.bounds;
    _scrollView.contentSize = CGSizeMake(numbersofPage * self.bounds.size.width, self.bounds.size.height);
    _pageControl.frame = CGRectMake(_pageControl.frame.origin.x, _pageControl.frame.origin.y, self.bounds.size.width, _pageControl.frame.size.height);
    
    for (NSInteger i = 0; i < [_contentViewDict count]; i ++) {
        UIImageView * imageView = [_contentViewDict objectForKey:@(i)];
        imageView.frame = CGRectMake(_scrollView.bounds.size.width * i, 0, _scrollView.bounds.size.width, _scrollView.bounds.size.height);
    }
}

- (void)dealloc
{
    NSInteger contentViewCount = [_contentViewDict count];
    for (NSInteger i = 0; i < contentViewCount; i ++) {
        UIImageView * view = [_contentViewDict objectForKey:@(i)];
        [view removeFromSuperview];
        [_contentViewDict removeObjectForKey:@(i)];
    }
    [_contentViewDict removeAllObjects];
}

- (void)refreshDataSource:(NSInteger)selectIndex
{
     NSInteger numbersofPage = [_datasource numberOfPage];
    
    _pageControl.numberOfPages = numbersofPage;
    _pageControl.currentPage = selectIndex;
    _pageControl.hidden = (numbersofPage <= 1) ? YES : NO;
    
    //clear contentView
    NSInteger contentViewCount = [_contentViewDict count];
    for (NSInteger i = numbersofPage; i < contentViewCount; i ++) {
        UIImageView * view = [_contentViewDict objectForKey:@(i)];
        [view removeFromSuperview];
        [_contentViewDict removeObjectForKey:@(i)];
    }
    
    //暂时这样处理
    for (NSInteger i = 0; i < numbersofPage; i ++) {
        if(![_contentViewDict objectForKey:@(i)]) {
            UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i * _scrollView.bounds.size.width, 0, _scrollView.bounds.size.width, _scrollView.bounds.size.height)];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            [_contentViewDict setObject:imageView forKey:@(i)];
            [_scrollView addSubview:imageView];
        }
    }
    
    [self loadPhotoPage:selectIndex];
}

- (void)loadPhotoPage:(NSInteger)page
{
    NSInteger numbersofPage = [_datasource numberOfPage];
    
    if(page < 0 || page >= numbersofPage) return;
    
    //page
    [self loadPageImp:page];
    
    //page - 1
    NSInteger prePage = page - 1;
    if(prePage >= 0 && prePage < numbersofPage) {
        [self loadPageImp:prePage];
    }
    
    //page + 1
     NSInteger nextPage = page + 1;
    if(nextPage >= 0 && nextPage < numbersofPage) {
        [self loadPageImp:nextPage];
    }
}

- (void)loadPageImp:(NSInteger)page
{
    NSString * url = [_datasource imageUrlAtIndex:page];
    url = [AppUtils thumbPath:url sizeType:QMImageOneSize];
    UIImageView * imageView = [_contentViewDict objectForKey:@(page)];
    [imageView qm_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_big"]];
}

- (NSInteger)getCurrentPage
{
    return _pageControl.currentPage;
}

#pragma mark - ScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    //	NSLog(@"page = %d, lastPage = %d", page, scrollView.contentSize.width/pageWidth);
    
    if (_pageControl.currentPage != page)
    {
        [self loadPhotoPage:page];
        
        _pageControl.currentPage = page;
    }
}

//- (UIImageView *)createImageView
//{
//    static NSString * identifier = @"photoImageViewIdentifier";
//    UIImageView * view = [self dequeueReusableViewWithIdentifier:identifier];
//    if(nil == view) {
//        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _scrollView.bounds.size.width, _scrollView.bounds.size.height)];
//        [_imageViewDict setObject:view forKey:identifier];
//    }
//    return view;
//}
//
////FIFO
//- (UIImageView *)dequeueReusableViewWithIdentifier:(NSString *)identifier
//{
//    NSMutableArray * array = [_imageViewDict objectForKey:identifier];
//    if ([array count] > 0) {
//        id object = [array lastObject];
//        [array removeObject:object];
//        return object;
//    } else {
//        return nil ;
//    }
//}

@end
