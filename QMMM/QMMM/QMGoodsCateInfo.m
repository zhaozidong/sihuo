//
//  QMGoodsCateInfo.m
//  QMMM
//
//  Created by kingnet  on 15-1-12.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMGoodsCateInfo.h"

const int kDefaultCategoryId = 20;

@implementation QMGoodsCateInfo

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        id temp = [dict objectForKey:@"id"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.cateId = [temp integerValue];
        }
        
        temp = [dict objectForKey:@"name"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.name = temp;
        }
        
        temp = [dict objectForKey:@"icon"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.iconURL = temp;
        }
        
        temp = [dict objectForKey:@"child"];
        if (temp && [temp isKindOfClass:[NSArray class]]) {
            NSArray *arr = (NSArray *)temp;
            if ([arr count] > 0) {
                NSMutableArray *mbArr = [NSMutableArray arrayWithCapacity:arr.count];
                for (NSDictionary *dic in arr) {
                    QMGoodsCateInfo *cate = [[QMGoodsCateInfo alloc]initWithDictionary:dic];
                    [mbArr addObject:cate];
                }
                self.childCate = mbArr;
            }
        }
    }
    return self;
}

- (NSString *)childDesc
{
    if (self.cateId == kDefaultCategoryId) {
        return @"其他未分类商品";
    }
    NSMutableString *mbStr = [[NSMutableString alloc]initWithString:@""];
    for (QMGoodsCateInfo * cate in self.childCate) {
        if ([mbStr isEqualToString:@""]) {
            [mbStr appendString:cate.name];
        }
        else{
            [mbStr appendFormat:@"  %@", cate.name];
        }
    }
    return mbStr;
}

@end
