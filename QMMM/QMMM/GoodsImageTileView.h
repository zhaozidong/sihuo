//
//  GoodsImageTileView.h
//  QMMM
//
//  Created by kingnet  on 14-9-17.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "DARecycledTileView.h"

@interface GoodsImageTileView : DARecycledTileView
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSURL *imageURL;

@end
