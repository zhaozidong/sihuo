//
//  ImageBrowerCell.m
//  QMMM
//
//  Created by kingnet  on 14-10-21.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "ImageBrowerCell.h"
#import "UIImageView+QMWebCache.h"
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/SDWebImagePrefetcher.h>
#import "YLProgressBar.h"

NSString * const kImageBrowerCellIdty = @"kImageBrowerCellIdty";

@implementation ImageBrowerCell
@synthesize imageName = _imageName;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self _initView];
    }
    return self;
}

- (void)_initView
{
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    imageView_ = [[UIImageView alloc]initWithFrame:CGRectZero];
    imageView_.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:imageView_];
    _progressBar = [[YLProgressBar alloc]initWithFrame:CGRectMake(0, 0, CGRectGetHeight(self.bounds), 2)];
    _progressBar.type = YLProgressBarTypeFlat;
    _progressBar.progressTintColor = UIColorFromRGB(0xaeaeae);
    _progressBar.hideStripes = YES;
    _progressBar.hideTrack = YES;
    _progressBar.behavior = YLProgressBarBehaviorDefault;
    [_progressBar setProgress:1.0];
    _progressBar.hidden = YES;
    [self.contentView addSubview:_progressBar];
}

- (void)setImageName:(NSString *)imageName
{
    if (_imageName!=imageName) {
        _imageName = imageName;
    }
    //旋转过了则不需要再旋转了
    if (_progressBar.center.y != 0.5*self.size.height) {
        imageView_.frame = CGRectMake(0, 0, self.size.width, self.size.height);
        imageView_.transform = CGAffineTransformMakeRotation(M_PI/2);
        _progressBar.frame = CGRectMake(0, 0, self.size.height, 2);
        _progressBar.transform = CGAffineTransformRotate(CGAffineTransformIdentity, M_PI/2);
        _progressBar.center = CGPointMake(self.size.width-1, 0.5*self.size.height);
    }
    
    [self updateImage];
}

- (void)updateImage
{
    UIImage *cachedImg = [[SDImageCache sharedImageCache]imageFromDiskCacheForKey:_imageName];
    if (cachedImg) {
        [self setImageWithImg:cachedImg];
    }
    else{
        //本地路径
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:_imageName]) {
            UIImage *image = [UIImage imageWithContentsOfFile:_imageName];
            [self setImageWithImg:image];
        }
        else{
            [_progressBar setProgress:0.0];
            _progressBar.hidden = NO;
            UIImage *placeholder = [UIImage imageNamed:@"goodsPlaceholder_big"];
            imageView_.image = placeholder;
            __weak __typeof(self) weakSelf = self;
            [[SDWebImageManager sharedManager]downloadImageWithURL:[NSURL URLWithString:_imageName] options:SDWebImageProgressiveDownload | SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                //显示进度
                if (expectedSize > 0) {
                    [weakSelf.progressBar setProgress:(float)receivedSize/expectedSize animated:YES];
                }
            } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                if (finished) {
                    weakSelf.progressBar.hidden = YES;
                    if ([imageURL.absoluteString isEqualToString:_imageName]) {
                        if(image){
                            imageView_.alpha = 0.0;
                            [UIView transitionWithView:imageView_ duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                                [weakSelf setImageWithImg:image];
                                imageView_.alpha = 1.0;
                            } completion:nil];
                        }
                        else{
                            imageView_.image = [UIImage imageNamed:@"goodImgFailed_big"];
                        }
                    }
                }
            }];
        }
    }
}

- (void)setImageWithImg:(UIImage *)image
{
    imageView_.image = image;
    //设置图片尺寸
//    CGSize showSize = [self newSizeByoriginalSize:image.size maxSize:self.size];
//    imageView_.frame = CGRectMake((self.size.width - showSize.width)*0.5, (self.size.height - showSize.height)*0.5, showSize.width, showSize.height);
}

- (void)downloadNextImage:(NSString *)imageName
{
    //本地路径
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (NO == [fileManager fileExistsAtPath:_imageName]) {
        dispatch_queue_t myQueue = dispatch_queue_create(kMyGlobalQueueId, NULL);
        dispatch_async(myQueue, ^{
            UIImage *cachedImg = [[SDImageCache sharedImageCache]imageFromDiskCacheForKey:imageName];
            if (nil == cachedImg) {
                [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:[NSArray arrayWithObject:imageName]];
            }
        });
    }
}

//获取尺寸
- (CGSize)newSizeByoriginalSize:(CGSize)oldSize maxSize:(CGSize)mSize
{
    if (oldSize.width <= 0 || oldSize.height <= 0) {
        return CGSizeZero;
    }
    
    CGSize newSize = CGSizeZero;
    if (oldSize.width > mSize.width || oldSize.height > mSize.height) {
        //按比例计算尺寸
        float bs = (mSize.width / oldSize.width);
        float newHeight = oldSize.height * bs;
        newSize = CGSizeMake(mSize.width, newHeight);
        
        if (newHeight > mSize.height) {
            bs = (mSize.height / oldSize.height);
            float newWidth = oldSize.width * bs;
            newSize = CGSizeMake(newWidth, mSize.height);
        }
    }else {
        newSize = oldSize;
    }
    return newSize;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
