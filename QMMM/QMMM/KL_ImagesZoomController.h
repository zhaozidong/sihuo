//
//  KL_ImagesZoomController.h
//  ShowImgDome
//
//  Created by chuliangliang on 14-9-28.
//  Copyright (c) 2014年 aikaola. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KL_ImagesZoomController : UIView<UITableViewDelegate,UITableViewDataSource>
- (id)initWithFrame:(CGRect)frame imgViewSize:(CGSize)size;
@property (nonatomic, strong)NSArray *imgs;
@property (nonatomic, strong)NSArray *smallImgs;

@property (nonatomic, assign)BOOL isShowTip;

@property (nonatomic, assign)BOOL isLookLifePhoto; //是否是查看生活照

- (void)setBlurBackground:(UIImage *)image;

- (void)updateImageDate:(NSArray *)imageArr smallImgs:(NSArray *)smallImgs selectIndex:(NSInteger)index;

@end
