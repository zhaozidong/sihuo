//
//  QMOrderDetailPayAmountCell.m
//  QMMM
//
//  Created by hanlu on 14-9-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMOrderDetailPayAmountCell.h"

NSString * const kQMOrderDetailPayAmountCellIdentifier = @"kQMOrderDetailPayAmountCellIdentifier";

@interface QMOrderDetailPayAmountCell () {
    CGFloat _accountPay;
    CGFloat _onlinePay;
}

@end

@implementation QMOrderDetailPayAmountCell

+(id)cellForOrderDetailPayAmount
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMOrderDetailPayAmountCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib
{
    // Initialization code
    
    _namelabel.text = [AppUtils localizedProductString:@"QM_Text_Pay_Amount"];
    _valuelabel.textColor = kRedColor;
    
    _onelabel.text = @"(余额支付";
    _threelabel.text = @" + 在线支付";
    _fivelablel.text = @")";
    
    _twolabel.textColor = kRedColor;
    _fourlabel.textColor = kRedColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(CGFloat)ammount accountPay:(CGFloat)accountPay onlinePay:(CGFloat)onlinePay
{
    _valuelabel.text = [NSString stringWithFormat:@"¥ %.2f", ammount];
    
    _twolabel.text = [NSString stringWithFormat:@"¥ %.2f", accountPay];
    
    _fourlabel.text = [NSString stringWithFormat:@"¥ %.2f", onlinePay];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGSize oneSize = [_onelabel.text sizeWithFont:_onelabel.font];
    CGSize twoSize = [_twolabel.text sizeWithFont:_twolabel.font];
    CGSize threeSize = [_threelabel.text sizeWithFont:_threelabel.font];
    CGSize fourSize = [_fourlabel.text sizeWithFont:_fourlabel.font];
    CGSize fiveSize = [_fivelablel.text sizeWithFont:_fivelablel.font];
    CGFloat startX = _valuelabel.frame.origin.x + _valuelabel.frame.size.width - oneSize.width - twoSize.width - threeSize.width - fourSize.width - fiveSize.width;
    
    CGRect frame = _onelabel.frame;
    frame.origin.x = startX;
    frame.size.width = oneSize.width;
    _onelabel.frame = frame;
    
    frame = _twolabel.frame;
    frame.origin.x = _onelabel.frame.origin.x + _onelabel.frame.size.width;
    frame.size.width = twoSize.width;
    _twolabel.frame = frame;
    
    frame = _threelabel.frame;
    frame.origin.x = _twolabel.frame.origin.x + _twolabel.frame.size.width;
    frame.size.width = threeSize.width;
    _threelabel.frame = frame;
    
    frame = _fourlabel.frame;
    frame.origin.x = _threelabel.frame.origin.x + _threelabel.frame.size.width;
    frame.size.width = fourSize.width;
    _fourlabel.frame = frame;
    
    frame = _fivelablel.frame;
    frame.origin.x = _fourlabel.frame.origin.x + _fourlabel.frame.size.width;
    frame.size.width = fiveSize.width;
    _fivelablel.frame = frame;
}

@end
