//
//  QMWebViewDelegate.m
//  QMMM
//
//  Created by kingnet  on 15-2-6.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMWebViewDelegate.h"
#import "QMHttpClient.h"
#import "NSString+JSONCategories.h"
#import "SharedManager.h"

@implementation QMWebViewDelegate

- (id)initWithDelegate:(id<JSMethodDelegate>) delegate
{
    self = [super init];
    if (self) {
        self.methodDelegate = delegate;
    }
    return self;
}

- (void)setMethodDelegate:(id<JSMethodDelegate>)methodDelegate
{
    _methodDelegate = methodDelegate;
    
    _methodFlags.delegateInviteFriends = _methodDelegate && [_methodDelegate respondsToSelector:@selector(inviteFriends)];
    _methodFlags.delegatePublishGoods = _methodDelegate && [_methodDelegate respondsToSelector:@selector(publishGoods:)];
    _methodFlags.delegatePageTitle = _methodDelegate && [_methodDelegate respondsToSelector:@selector(setTitle:)];
    _methodFlags.delegateCopy = _methodDelegate && [_methodDelegate respondsToSelector:@selector(copyContent:)];
    _methodFlags.delegateInviteAllFriends = _methodDelegate && [_methodDelegate respondsToSelector:@selector(inviteAllFriends)];
    _methodFlags.delegateBack = _methodDelegate && [_methodDelegate respondsToSelector:@selector(back)];
    _methodFlags.delegatePersonPage = _methodDelegate && [_methodDelegate respondsToSelector:@selector(personPage:)];
    _methodFlags.delegateGoodsDetail = _methodDelegate && [_methodDelegate respondsToSelector:@selector(goodsDetail:)];
    _methodFlags.delegateBuyHongBaoGoods = _methodDelegate && [_methodDelegate respondsToSelector:@selector(buyHongBaoGoods:price:goodsId:)];
    _methodFlags.delegateKeyboard = _methodDelegate && [_methodDelegate respondsToSelector:@selector(keyboard)];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestStr = [[request URL]absoluteString];
    NSArray *urlComps = [requestStr componentsSeparatedByString:@"://"];
    if ([urlComps count] && [[urlComps objectAtIndex:0]isEqualToString:@"protocol"]) {
        NSArray *arrFucnameAndParameter = [(NSString *)[urlComps objectAtIndex:1]componentsSeparatedByString:@"/"];
        //只有一个方法名
        if ((arrFucnameAndParameter.count==1)||(arrFucnameAndParameter.count==2 && [arrFucnameAndParameter[1] isEqualToString:@""])) {
            if ([arrFucnameAndParameter[0] isEqualToString:@"inviteFriends"]) {
                if (_methodFlags.delegateInviteFriends) {
                    [_methodDelegate inviteFriends];
                }
            }
            else if ([arrFucnameAndParameter[0] isEqualToString:@"inviteAllFriends"]){
                [AppUtils trackCustomEvent:@"activity_clickInviteAll_event" args:nil];
                if (_methodFlags.delegateInviteAllFriends) {
                    [_methodDelegate inviteAllFriends];
                }
            }
            else if ([arrFucnameAndParameter[0] isEqualToString:@"back"]){
                if (_methodFlags.delegateBack) {
                    [_methodDelegate back];
                }
            }
            else if ([arrFucnameAndParameter[0] isEqualToString:@"keyboard"]){
                if (_methodFlags.delegateKeyboard) {
                    [_methodDelegate keyboard];
                }
            }
        }
        //方法名后面有参数
        else{
            if (![arrFucnameAndParameter[1] isEqualToString:@""]) {
                NSString *jsonStr = [arrFucnameAndParameter objectAtIndex:1];
                jsonStr = [AppUtils DisEncodeUTF8ToChina:jsonStr];
                NSDictionary *params = [jsonStr toDict];
                if (params && [params isKindOfClass:[NSDictionary class]]) {
                    if ([arrFucnameAndParameter[0] isEqualToString:@"setTitle"]){
                        if (_methodFlags.delegatePageTitle) {
                            id temp = [params objectForKey:@"title"];
                            if (temp && [temp isKindOfClass:[NSString class]]) {
                                [_methodDelegate setTitle:temp];
                            }
                        }
                    }
                    else if ([arrFucnameAndParameter[0] isEqualToString:@"copyContent"]){
                        if (_methodFlags.delegateCopy) {
                            id temp = [params objectForKey:@"content"];
                            if (temp && [temp isKindOfClass:[NSString class]]) {
                                [_methodDelegate copyContent:temp];
                            }
                        }
                    }
                    else if ([arrFucnameAndParameter[0] isEqualToString:@"share"]){
                        NSString *platform = @"", *title = @"", *content = @"",*imageurl = @"", *shareUrl = @"", *desc = @"";
                        id temp = [params objectForKey:@"platform"];
                        if (temp && [temp isKindOfClass:[NSString class]]) {
                            platform = temp;
                        }
                        temp = [params objectForKey:@"title"];
                        if (temp && [temp isKindOfClass:[NSString class]]) {
                            title = temp;
                        }
                        temp = [params objectForKey:@"content"];
                        if (temp && [temp isKindOfClass:[NSString class]]) {
                            content = temp;
                        }
                        temp = [params objectForKey:@"imageurl"];
                        if (temp && [temp isKindOfClass:[NSString class]]) {
                            imageurl = temp;
                        }
                        temp = [params objectForKey:@"url"];
                        if (temp && [temp isKindOfClass:[NSString class]]) {
                            shareUrl = temp;
                        }
                        temp = [params objectForKey:@"description"];
                        if (temp && [temp isKindOfClass:[NSString class]]) {
                            desc = temp;
                        }
                        
                        if ([platform isEqualToString:@"Wechat"]) {
                            [AppUtils trackCustomEvent:@"activity_shareToWeChat_event" args:nil];
                            [[SharedManager sharedInstance] sharedWithType:QMSharedTypeWeiXin title:title content:content description:desc image:imageurl url:shareUrl];
                        }
                        else if ([platform isEqualToString:@"WechatMoments"]){
                            [AppUtils trackCustomEvent:@"activity_shareToWeChatFriends_event" args:nil];
                            [[SharedManager sharedInstance] sharedWithType:QMSharedTypeWeiXinFriends title:title content:content description:desc image:imageurl url:shareUrl];
                        }
                        else if ([platform isEqualToString:@"QZone"]){
                            [AppUtils trackCustomEvent:@"activity_shareToQzone_event" args:nil];
                            [[SharedManager sharedInstance] sharedWithType:QMSharedTypeQZone title:title content:content description:desc image:imageurl url:shareUrl];
                        }
                        else if ([platform isEqualToString:@"SinaWeibo"]){
                            [AppUtils trackCustomEvent:@"activity_shareToWeibo_event" args:nil];
                            [[SharedManager sharedInstance] sharedWithType:QMSharedTypeSinaWeiBo title:title content:content description:desc image:imageurl url:shareUrl];
                        }
                        else if ([platform isEqualToString:@"SMS"]){
                            [AppUtils trackCustomEvent:@"activity_shareToSMS_event" args:nil];
                            //与邀请好友一样
                            if (_methodFlags.delegateInviteFriends) {
                                [_methodDelegate inviteFriends];
                            }
                        }
                    }
                    else if ([arrFucnameAndParameter[0] isEqualToString:@"goodsDetail"]){
                        if (_methodFlags.delegateGoodsDetail) {
                            id temp = [params objectForKey:@"goodsId"];
                            if (temp) {
                                [_methodDelegate goodsDetail:[temp longLongValue]];
                            }
                        }
                    }
                    else if ([arrFucnameAndParameter[0] isEqualToString:@"personPage"]){
                        if (_methodFlags.delegatePersonPage) {
                            id temp = [params objectForKey:@"userId"];
                            if (temp) {
                                [_methodDelegate personPage:[temp longLongValue]];
                            }
                        }
                    }
                    else if ([arrFucnameAndParameter[0] isEqualToString:@"publishGoods"]){
                        if (_methodFlags.delegatePublishGoods) {
                            id temp = [params objectForKey:@"from"];
                            if (temp) {
                                [_methodDelegate publishGoods:temp];
                            }
                        }
                    }
                    else if ([arrFucnameAndParameter[0] isEqualToString:@"buyHongBaoGoods"]){
                        if (_methodFlags.delegateBuyHongBaoGoods) {
                            int num = 0;
                            float price = 0.f;
                            int goodsId = 0;
                            id temp = [params objectForKey:@"num"];
                            if (temp && [temp isKindOfClass:[NSNumber class]]) {
                                num = [temp intValue];
                            }
                            
                            temp = [params objectForKey:@"price"];
                            if (temp && [temp isKindOfClass:[NSNumber class]]) {
                                price = [temp floatValue];
                            }
                            else if (temp && [temp isKindOfClass:[NSString class]]){
                                price = [temp floatValue];
                            }
                            
                            temp = [params objectForKey:@"id"];
                            if (temp && [temp isKindOfClass:[NSNumber class]]) {
                                goodsId = [temp intValue];
                            }
                            else if (temp && [temp isKindOfClass:[NSString class]]){
                                goodsId = [temp intValue];
                            }
                            [_methodDelegate buyHongBaoGoods:num price:price goodsId:goodsId];
                        }
                    }
                }
            }
        }
        return NO;
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    if ([[QMHttpClient defaultClient] isConnectionAvailable]) {
        [AppUtils showProgressMessage:@"加载中"];
    }
    else{
        [AppUtils showAlertMessage:@"当前网络不可用"];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [AppUtils dismissHUD];
}

@end
