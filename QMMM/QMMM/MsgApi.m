//
//  MsgApi.m
//  QMMM
//
//  Created by hanlu on 14-10-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MsgApi.h"
#import "APIConfig.h"
#import "QMHttpClient.h"

@implementation MsgApi

//获取系统消息列表
- (void)getMsgList:(uint)preGetCount context:(contextBlock)context
{
    NSString * reqUrl = [NSString stringWithFormat:@"%@&start=%u", API_SYSTEM_CHAT_LIST, preGetCount];
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:reqUrl];

    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"getMsgList error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------getMsgList fail : %@", url);
            
            NSDictionary *dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });
    
}

- (void)deleteMsg:(uint)preMsgCount context:(contextBlock)context
{
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_DELETE_SYSTEM_MSG];
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:@(preMsgCount), @"start", nil];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"deleteMsg error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------deleteMsg fail : %@", url);
            
            NSDictionary *dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
            
        }];
    });


}

@end
