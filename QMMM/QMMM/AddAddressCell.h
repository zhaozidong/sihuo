//
//  AddAddressCell.h
//  QMMM
//
//  Created by kingnet  on 14-9-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAddressCell : UITableViewCell
+ (AddAddressCell *)addAddressCell;
+ (CGFloat)cellHeight;
@end
