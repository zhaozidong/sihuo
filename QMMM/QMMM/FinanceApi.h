//
//  FinanceApi.h
//  QMMM
//
//  Created by kingnet  on 14-9-27.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseHandler.h"

@interface FinanceApi : BaseHandler

- (void)getFinanceInfo:(contextBlock)context;

- (void)drawMoney:(NSString *)pwd
            money:(float)money  //取现金额
            payee:(NSString *)payee
        accountNo:(NSString *)accountNo
          context:(contextBlock)context;

- (void)getDrawList:(int)startId top:(int)top context:(contextBlock)context;

/**
 设置账户
 **/
- (void)setAccount:(NSString *)accountNo
       accountName:(NSString *)accountName
      oldAccountNo:(NSString *)oldAccountNo
    oldAccountName:(NSString *)oldAccountName
           context:(contextBlock)context;

@end
