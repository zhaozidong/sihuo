//
//  PersonPageViewController.m
//  QMMM
//
//  Created by kingnet  on 15-1-22.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "PersonPageViewController.h"
#import "PersonPageHeadView.h"
#import "PersonNumIntroCell.h"
#import "QMTopNavView.h"
#import "PersonPageHandler.h"
#import "UserDataInfo.h"
#import "PersonGoodsItem.h"
#import "GoodDataInfo.h"
#import "MJRefresh.h"
#import "MJRefreshConst.h"
#import "PersonGoodsCell.h"
#import "PersonNoGoodsCell.h"
#import "PersonEvaluateCell.h"
#import "PersonEvaluateEntity.h"
#import "PersonLoadingCell.h"
#import "PersonBottomView.h"
#import "UserDefaultsUtils.h"
#import "GoodsDetailViewController.h"
#import "ChatViewController.h"
#import "QMRemarkViewController.h"
#import "FriendInfo.h"
#import "QMFriendFromViewController.h"
#import "QMFriendRelationViewController.h"
#import "KL_ImagesZoomController.h"
#import <POP/POP.h>
#import <SDWebImage/SDImageCache.h>
#import "MineInfoViewController.h"
#import "QMReportViewController.h"
#import "QMQAViewController.h"

@interface PersonPageViewController ()<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, PersonPageHeadViewDelegate, QMTopNavViewDelegate, PersonGoodsItemDelegate, PersonBottomViewDelegate,PersonEvaluateCellDelegate,PersonNumIntroDelegate>
{
    NSString *uid_;  //用户id
    BOOL goodsListFooterView_; //标识是否可以加载更多
    BOOL evaluateListFooterView_; //同上
    __block BOOL goodsListLoadFailed_;
    __block BOOL evaluateListLoadFailed_;
    
    CGFloat startContentOffset;
    CGFloat lastContentOffset;
    BOOL hidden;
    
    float alpha_; //记录离开时的状态
    BOOL willDisappear_; //是否要消失了
}

@property (nonatomic, strong) PersonPageHeadView *headView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) PersonNumIntroCell *numIntroCell;
@property (nonatomic, strong) QMTopNavView *topNavView;
@property (nonatomic, strong) PersonNoGoodsCell *personNoGoodsCell;
@property (nonatomic, strong) PersonEvaluateCell *sizeCell;
@property (nonatomic, strong) PersonLoadingCell *loadingCell;
@property (nonatomic, strong) PersonBottomView *bottomView;
@property (nonatomic, strong) UIButton *backButton;//在导航栏消失时显示
@property (nonatomic, strong) UIButton *moreButton;//在导航栏消失时显示

@property (nonatomic, strong) UIColor *navBarBackgroundColor;  //导航栏的颜色

@property (nonatomic, strong) PersonPageUserInfo *personInfo; //个人信息
@property (nonatomic, strong) NSMutableArray *goodsItems;
@property (nonatomic, strong) NSMutableArray *evaluateList;
@property (nonatomic, assign) int numberOfItemsPerRow;
@property (nonatomic, strong) KL_ImagesZoomController *imageZoomView;

@end

@implementation PersonPageViewController

- (id)initWithUserId:(uint64_t)uid
{
    self = [super init];
    if (self) {
        uid_ = [NSString stringWithFormat:@"%qu", uid];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    if(kIOS7) {
        self.navBarBackgroundColor = self.navigationController.navigationBar.barTintColor;
    } else {
        // iOS 6.1 or earlier
        self.navBarBackgroundColor = self.navigationController.navigationBar.tintColor;
    }
    
    [self.view addSubview:self.tableView];
    
    [self.view addSubview:self.backButton];
    
    self.numberOfItemsPerRow = 2;
    
    alpha_ = 0.0;
    
    [self requestUserInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self navBarTransparent:YES];
    self.navigationController.navigationBar.alpha = alpha_;
    willDisappear_ = NO;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(oneTapHandler:) name:kShowImageOneTapNotity object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(becomeActiveHandler:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enteredBackgroundHandler:)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self navBarTransparent:NO];
    willDisappear_ = YES;
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kShowImageOneTapNotity object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)navBarTransparent:(BOOL)isTransparent
{
    if (isTransparent) {
        self.navigationController.navigationBar.alpha = 0.0;
    }
    else{
        self.navigationController.navigationBar.alpha = 1.0;
    }
}

#pragma mark - Get Data
- (void)requestUserInfo
{
    __weak __typeof(self) weakSelf = self;
    [AppUtils showProgressMessage:@"加载中"];
    [[PersonPageHandler sharedInstance]getUserInfo:uid_ success:^(id obj) {
        [AppUtils dismissHUD];
        __typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf reloadUserInfo:(PersonPageUserInfo *)obj];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)reloadUserInfo:(PersonPageUserInfo *)personInfo
{
    if (personInfo.userInfo.remark.length > 0) {
        self.navigationItem.title = personInfo.userInfo.remark;
    }
    else{
        self.navigationItem.title = personInfo.userInfo.nickname;
    }
    self.personInfo = [[PersonPageUserInfo alloc]init];
    [self.personInfo assignValue:personInfo];
    [self.headView updateUI:self.personInfo.userInfo];
    [self.numIntroCell updateUI:self.personInfo];
    NSString *uid = [UserDefaultsUtils valueWithKey:kUserId];
    if (![uid isEqualToString:uid_]) {//不是自己
        if (self.personInfo.relation.isFriends || self.personInfo.relation.is2ndFriends) {
            [self.bottomView updateUI:self.personInfo.relation];
            [self.bottomView showWithCompletion:nil];
            [self.headView updateFriendButton:self.personInfo.relation];
        }
        if (personInfo.relation.isFriends) {
//            [self.view addSubview:self.moreButton];
//            self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(moreAction:) title:nil size:CGSizeMake(44.f, 44.f) imageName:@"more_btn"];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processUpdateFriendRemarkNotify:) name:kUpdateFriendRemarkNotify object:nil];
        }
        [self.view addSubview:self.moreButton];
        self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(moreAction:) title:nil size:CGSizeMake(44.f, 44.f) imageName:@"more_btn"];
    }
    //是自己
    else{
        [self.view addSubview:self.moreButton];
        self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(moreAction:) title:nil size:CGSizeMake(44.f, 44.f) imageName:@"more_btn"];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userInfoUpdate:) name:kUserInfoUpdateNotify object:nil];
    }
}

- (void)requestGoodsInfo
{
    __weak __typeof(self) weakSelf = self;
    uint64_t goodsId = 0;
    if (self.goodsItems.count >0) {
        PersonGoodsItem *lastItem = [self.goodsItems lastObject];
        GoodsSimpleInfo *goodsInfo = lastItem.goodsInfo;
        goodsId = goodsInfo.gid;
    }
    
    [[PersonPageHandler sharedInstance]getSellingGoods:goodsId uid:uid_ success:^(id obj) {
        __typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf loadGoodsInfo:(NSDictionary *)obj];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        __typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf.tableView.footer.isRefreshing) {
            [strongSelf.tableView.footer endRefreshing];
        }
        goodsListLoadFailed_ = YES;
        [strongSelf.tableView reloadData];
    }];
}

//dict 包含了对象和每页的数量
- (void)loadGoodsInfo:(NSDictionary *)dict
{
    int itemCount = (int)self.goodsItems.count; //记录下之前的count
    
    NSArray *goodsArr = [dict objectForKey:@"models"];
    int pageNum = [[dict objectForKey:@"pageNum"] intValue];
    
    if (nil == _goodsItems) {
        _goodsItems = [NSMutableArray array];
        [self.loadingCell stopAnimate];
    }
    
    if (goodsArr.count >0) {
        for (GoodsSimpleInfo *goodsInfo in goodsArr) {
            PersonGoodsItem *goodsItem = [PersonGoodsItem goodsItem];
            goodsItem.goodsInfo = goodsInfo;
            goodsItem.delegate = self;
            [self.goodsItems addObject:goodsItem];
        }
        
        if (itemCount==0) { //第一次拉取
            if (goodsArr.count >= pageNum) {
                goodsListFooterView_ = YES;
                [self addRefreshFooterView];
            }
        }
    }
    
    [self.tableView reloadData];
    
    if (itemCount > 0) {//不是第一拉取
        [self.tableView.footer endRefreshing];
        if (goodsArr.count <pageNum ) {
            evaluateListFooterView_ = NO;
            [self removeFooterView];
        }
    }
}

//获取已售评价列表
- (void)requestEvaluateList
{
    uint64_t orderId = 0;
    if (self.evaluateList.count >0) {
        PersonEvaluateEntity *entity = [self.evaluateList lastObject];
        orderId = entity.orderId;
    }

    __weak typeof(self) weakSelf = self;
    [[PersonPageHandler sharedInstance] getEvaluateList:uid_ orderId:orderId success:^(id obj) {
        NSArray *array = (NSArray *)obj;
        [weakSelf loadEvaluate:array];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        __typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf.tableView.footer.isRefreshing) {
            [strongSelf.tableView.footer endRefreshing];
        }
        evaluateListLoadFailed_ = YES;
        [strongSelf.tableView reloadData];
    }];
}

- (void)loadEvaluate:(NSArray *)evaluates
{
    int itemCount = (int)self.evaluateList.count;
    int pageNum = 10;
    
    if (nil == _evaluateList) {
        _evaluateList = [NSMutableArray array];
        [self.loadingCell stopAnimate];
    }
    
    if (evaluates.count > 0) {
        [self.evaluateList addObjectsFromArray:evaluates];
        if (itemCount == 0) { //第一次拉取评价列表
            if (evaluates.count >= pageNum) {
                //如果等于10条则添加刷新footerView
                evaluateListFooterView_ = YES;
                [self addRefreshFooterView];
            }
        }
    }
    
    [self.tableView reloadData];
    
    if (itemCount > 0) {
        [self.tableView.footer endRefreshing];
        if (evaluates.count < pageNum) {
            evaluateListFooterView_ = NO;
            [self removeFooterView];
        }
    }
}

#pragma mark - Set The View
- (void)addRefreshFooterView
{
    __weak typeof(self) weakSelf = self;
    [self.tableView addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRereshing];
    }];
}

- (void)removeFooterView
{
    [self.tableView removeFooter];
}

- (void)footerRereshing
{
    if (self.topNavView.selectedIndex == 0) {
        [self requestGoodsInfo];
    }
    else{
        [self requestEvaluateList];
    }
}

- (UIButton *)backButton
{
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (kMainFrameWidth>375) {
            _backButton.frame = CGRectMake(4, 20.f, 44.f, 44.f);
        }
        else{
            _backButton.frame = CGRectMake(0, 20.f, 44.f, 44.f);
        }
        UIImage *image = [UIImage imageNamed:@"back_btn"];
        [_backButton setImage:image forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

- (UIButton *)moreButton
{
    if (!_moreButton) {
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (kMainFrameWidth>375) {
            _moreButton.frame = CGRectMake(kMainFrameWidth-44.f-10, 20.f, 44.f, 44.f);
        }
        else{
            _moreButton.frame = CGRectMake(kMainFrameWidth-44.f-6, 20.f, 44.f, 44.f);
        }
        UIImage *image = [UIImage imageNamed:@"more_btn"];
        [_moreButton setImage:image forState:UIControlStateNormal];
        [_moreButton addTarget:self action:@selector(moreAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreButton;
}

- (PersonPageHeadView *)headView
{
    if (!_headView) {
        _headView = [PersonPageHeadView headView];
        _headView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), [PersonPageHeadView headViewHeight]);
        _headView.delegate = self;
    }
    return _headView;
}

- (PersonBottomView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [PersonBottomView bottomView];
        _bottomView.frame = CGRectMake(0, kMainFrameHeight+25, CGRectGetWidth(self.view.frame), [PersonBottomView viewHeight]);
        _bottomView.delegate = self;
        [self.view addSubview:_bottomView];
    }
    return _bottomView;
}

- (PersonNumIntroCell *)numIntroCell
{
    if (!_numIntroCell) {
        _numIntroCell = [PersonNumIntroCell numIntroCell];
        _numIntroCell.delegate=self;
    }
    return _numIntroCell;
}

- (PersonNoGoodsCell *)personNoGoodsCell
{
    if (!_personNoGoodsCell) {
        _personNoGoodsCell = [PersonNoGoodsCell personNoGoods];
    }
    return _personNoGoodsCell;
}

- (PersonEvaluateCell *)sizeCell
{
    if (!_sizeCell) {
        _sizeCell = [PersonEvaluateCell evaluateCell];
        _sizeCell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _sizeCell.frame = CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), CGRectGetHeight(_sizeCell.frame));
    }
    return _sizeCell;
}

- (PersonLoadingCell *)loadingCell
{
    if (!_loadingCell) {
        _loadingCell = [PersonLoadingCell loadingCell];
    }
    return _loadingCell;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.allowsSelection = NO;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.headView;
        [_tableView registerClass:[PersonGoodsCell class] forCellReuseIdentifier:kPersonGoodsCellIdentifier];
        [_tableView registerNib:[PersonEvaluateCell nib] forCellReuseIdentifier:kPersonEvaluateCellIdentifier];
    }
    return _tableView;
}

- (QMTopNavView *)topNavView
{
    if (!_topNavView) {
        _topNavView = [[QMTopNavView alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, 45) titles:@[@"在售商品", @"已售评价"] images:nil];
        _topNavView.delegate = self;
    }
    return _topNavView;
}

#pragma mark - UITableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    else{
        NSInteger nr = 1;
        if (self.topNavView.selectedIndex == 0) { // 当前是在售商品
            if (self.goodsItems.count>0) {
                if (self.goodsItems.count % self.numberOfItemsPerRow == 0) {
                    nr = self.goodsItems.count / self.numberOfItemsPerRow;
                }
                else{
                    nr = self.goodsItems.count / self.numberOfItemsPerRow +1;
                }
            }
        }
        else{
            if (self.evaluateList.count>0) {
                nr = self.evaluateList.count;
            }
        }
        return nr;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return [PersonNumIntroCell cellHeight];
    }
    else{
        if (self.topNavView.selectedIndex == 0) {
            if (self.goodsItems.count >0) {
                return [PersonGoodsItem itemSize].height+[PersonGoodsItem margin];
            }
            else{
                if (goodsListLoadFailed_ || self.goodsItems) {//加载过数据但是没有商品或加载失败了
                    return 200;
                }
                else{ //正在加载中
                    return 45.f;
                }
            }
        }
        else{
            if (self.evaluateList.count >0) {
                PersonEvaluateEntity *entity = [self.evaluateList objectAtIndex:indexPath.row];
                [self.sizeCell updateUI:entity];
                [self.sizeCell setNeedsDisplay];
                [self.sizeCell layoutIfNeeded];
                CGFloat height = [self.sizeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
                height += 1;
                return height;
            }
            else{
                if (evaluateListLoadFailed_ || self.evaluateList) {
                    return 200;
                }
                else{
                    return 45.f;
                }
            }
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return self.numIntroCell;
    }
    else{
        if (self.topNavView.selectedIndex == 0) {
            if (self.goodsItems.count == 0) {
                if (goodsListLoadFailed_) {
                    self.personNoGoodsCell.tip = @"加载失败了~";
                    return self.personNoGoodsCell;
                }
                else if (self.goodsItems) {
                    self.personNoGoodsCell.tip = @"暂无在售商品";
                    return self.personNoGoodsCell;
                }
                else{
                    [self.loadingCell startAnimate];
                    return self.loadingCell;
                }
            }
            else{
                PersonGoodsCell *cell = (PersonGoodsCell *)[tableView dequeueReusableCellWithIdentifier:kPersonGoodsCellIdentifier];
                if (!cell) {
                    cell = [[PersonGoodsCell alloc] initWithItems:[self itemsForRowAtIndexPath:indexPath]];
                }
                else{
                    cell.items = [self itemsForRowAtIndexPath:indexPath];
                }
                return cell;
            }
        }
        else{
            if (self.evaluateList.count == 0) {
                if (evaluateListLoadFailed_) {
                    self.personNoGoodsCell.tip = @"加载失败了~";
                    return self.personNoGoodsCell;
                }
                else if (self.evaluateList) {
                    self.personNoGoodsCell.tip = @"暂无已售评价";
                    return self.personNoGoodsCell;
                }
                else{
                    [self.loadingCell startAnimate];
                    return self.loadingCell;
                }
            }
            else{
                PersonEvaluateCell *cell = (PersonEvaluateCell *)[tableView dequeueReusableCellWithIdentifier:kPersonEvaluateCellIdentifier];
                PersonEvaluateEntity *entity = (PersonEvaluateEntity *)[self.evaluateList objectAtIndex:indexPath.row];
                [cell updateUI:entity];
                cell.delegate = self;
                return cell;
            }
        }
    }
    return nil;
}

- (NSArray *)itemsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:self.numberOfItemsPerRow];
    
    NSUInteger startIndex = indexPath.row * self.numberOfItemsPerRow,
    endIndex = startIndex + self.numberOfItemsPerRow - 1;
    if (startIndex < self.goodsItems.count)
    {
        if (endIndex > self.goodsItems.count - 1)
            endIndex = self.goodsItems.count - 1;
        
        for (NSUInteger i = startIndex; i <= endIndex; i++)
        {
            [items addObject:(self.goodsItems)[i]];
        }
    }
    
    return items;
}


#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    else if (section == 1){
        if (self.topNavView.selectedIndex == 0) {
            if (NO == goodsListLoadFailed_ && self.goodsItems.count > 0) {
                return 45.f+6.f;
            }
        }
        return 45.f;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return 8.f;
    }
    else if (section == 1){
        return CGFLOAT_MIN;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 8.f)];
        view.backgroundColor = kBackgroundColor;
        return view;
    }
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), CGFLOAT_MIN)];
    [view setBackgroundColor:[UIColor whiteColor]];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        int height = (self.topNavView.selectedIndex == 0)?(45.f+6.f):45.f;
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), height)];
        [view addSubview:self.topNavView];
        view.backgroundColor = [UIColor whiteColor];
        return view;
    }
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), CGFLOAT_MIN)];
    [view setBackgroundColor:[UIColor whiteColor]];
    
    return view;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        cell.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - UIScrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    self.headView.offsetY = scrollView.contentOffset.y;
    if (scrollView.contentOffset.y >= 0) {
        float alpha = 1/[PersonPageHeadView headViewHeight]*scrollView.contentOffset.y*2;
        if (alpha >= 1.0) {
            alpha = 1.0;
        }
        else if (alpha <= 0.1){
            alpha = 0.0;
        }
        if (willDisappear_) {
            alpha_ = alpha;
        }
        else{
            self.navigationController.navigationBar.alpha = alpha;
        }
    }
    
    if ((self.topNavView.selectedIndex==0 && self.goodsItems.count>0) || (self.topNavView.selectedIndex==1 && self.evaluateList.count>0)) {
        CGFloat currentOffset = scrollView.contentOffset.y;
        CGFloat differenceFromStart = startContentOffset - currentOffset;
        CGFloat differenceFromLast = lastContentOffset - currentOffset;
        lastContentOffset = currentOffset;
        
        if((differenceFromStart) < 0)
        {
            // scroll up
            if(scrollView.isTracking && (abs(differenceFromLast)>1))
                [self expand];
        }
        else {
            if(scrollView.isTracking && (abs(differenceFromLast)>1))
                [self contract];
        }
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.headView.touching = NO;
//    if (scrollView.contentOffset.y > 0 && scrollView.contentOffset.y <= [PersonPageHeadView headViewHeight]/4) {
//        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
//    }
//    else if (scrollView.contentOffset.y > 0 && scrollView.contentOffset.y > [PersonPageHeadView headViewHeight]/4 && scrollView.contentOffset.y <= [PersonPageHeadView headViewHeight]/2){
//        [scrollView setContentOffset:CGPointMake(0, [PersonPageHeadView headViewHeight]/2+1) animated:YES];
//    }
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if(decelerate==NO)
    {
        self.headView.touching = NO;
//        if (scrollView.contentOffset.y > 0 && scrollView.contentOffset.y <= [PersonPageHeadView headViewHeight]/4) {
//            [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
//        }
//        else if (scrollView.contentOffset.y > 0 && scrollView.contentOffset.y > [PersonPageHeadView headViewHeight]/4 && scrollView.contentOffset.y <= [PersonPageHeadView headViewHeight]/2){
//            [scrollView setContentOffset:CGPointMake(0, [PersonPageHeadView headViewHeight]/2+1) animated:YES];
//        }
    }
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.headView.touching = YES;
    if ((self.topNavView.selectedIndex==0 && self.goodsItems.count>0) || (self.topNavView.selectedIndex==1 && self.evaluateList.count>0)) {
        startContentOffset = lastContentOffset = scrollView.contentOffset.y;
    }
}
- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    if ((self.topNavView.selectedIndex==0 && self.goodsItems.count>0) || (self.topNavView.selectedIndex==1 && self.evaluateList.count>0)) {
        [self contract];
    }
    
    return YES;
}

- (void)expand
{
    if (hidden) {
        return;
    }
    hidden = YES;
    if (_bottomView) {

        [self.bottomView dismissWithCompletion:^{
        }];
    }
}

- (void)contract
{
    if (!hidden) {
        return;
    }
    hidden = NO;
    if (_bottomView) {

        [self.bottomView showWithCompletion:^{

        }];
    }
}

#pragma mark - TopNavView Delegate
- (void)filterButtonIndex:(NSInteger)index
{
    [self.tableView reloadData];
    if (index == 0) {
        if (goodsListFooterView_) {
            [self addRefreshFooterView];
        }
        else{
            //清除掉footerview
            [self removeFooterView];
        }
        if (nil == _goodsItems && NO == goodsListLoadFailed_) { //以此来判断是否拉取过
            [self requestGoodsInfo];
        }
    }
    else{
        if (evaluateListFooterView_) {
            [self addRefreshFooterView];
        }
        else{
            //清除掉footerview
            [self removeFooterView];
        }
        if (nil == _evaluateList && NO == evaluateListLoadFailed_) {
            [self requestEvaluateList];
        }
    }
}

#pragma mark - HeadViewDelegate
- (void)didTapHeadView
{
    //查看头像加上生活照
    if (!self.imageZoomView) {
        self.imageZoomView = [[KL_ImagesZoomController alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height )imgViewSize:CGSizeZero];
    }
    self.imageZoomView.alpha = 0.0;
    self.imageZoomView.isLookLifePhoto = YES;
    [self.view addSubview:self.imageZoomView];
    NSString *imgUrl = [AppUtils getAbsolutePath:self.personInfo.userInfo.avatar];
    UIImage *avatarImg = [[SDImageCache sharedImageCache]imageFromDiskCacheForKey:imgUrl];
    if (avatarImg) {
        [self.imageZoomView setBlurBackground:avatarImg];
    }
    else{
        [self.imageZoomView setBlurBackground:[UIImage imageNamed:@"goodImgFailed_big"]];
    }
    
    [self.imageZoomView updateImageDate:[self imageArr] smallImgs:nil selectIndex:0];
    
    POPBasicAnimation *alphaAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    alphaAnimation.toValue = @(1);
    __weak KL_ImagesZoomController *weakImage = self.imageZoomView;
    __weak typeof(self) weakSelf = self;
    alphaAnimation.completionBlock = ^(POPAnimation *animation, BOOL finish){
        if (weakSelf.personInfo.userInfo.lifePhotos.count > 1) {
            [weakImage setIsShowTip:YES];
        }
    };
    [self.imageZoomView pop_addAnimation:alphaAnimation forKey:@"alphaAnim"];
}

- (NSArray *)imageArr{
    NSMutableArray *arr = [NSMutableArray array];
    if ([self.personInfo.userInfo.avatar isEqualToString:@""]) {
        [arr addObject:@""];
    }
    else{
        NSString *avatarStr = [AppUtils getAbsolutePath:self.personInfo.userInfo.avatar];
        [arr addObject:avatarStr];
    }
    
    for (NSString *urlStr in self.personInfo.userInfo.lifePhotos) {
        [arr addObject:[AppUtils getAbsolutePath:urlStr]];
    }
    return arr;
}

- (void)didTapFriendBtn
{
    if (self.personInfo.relation.isFriends) {
        //如果是一度好友
        QMFriendFromViewController *from=[[QMFriendFromViewController alloc] initWithUserInfo:self.personInfo];
        alpha_ = self.navigationController.navigationBar.alpha;
        [self.navigationController  pushViewController:from animated:YES];
    }
    else if (self.personInfo.relation.is2ndFriends){
        //如果是二度好友
        QMFriendRelationViewController *relation=[[QMFriendRelationViewController alloc] initWithUserInfo:self.personInfo.relation.friendsArr];
        alpha_ = self.navigationController.navigationBar.alpha;
        [self.navigationController pushViewController:relation animated:YES];
    }
}

#pragma mark - GoodsItemDelegate
- (void)clickGoodsItem:(GoodsSimpleInfo *)goodsInfo
{
    GoodsDetailViewController *controller = [[GoodsDetailViewController alloc]initWithGoodsId:goodsInfo.gid];
     alpha_ = self.navigationController.navigationBar.alpha;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - PersonBottomViewDelegate
- (void)didTapChat
{
    ChatViewController *controller = [[ChatViewController alloc]initWithChatter:uid_];
    alpha_ = self.navigationController.navigationBar.alpha;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didTapAddFriend
{
    [AppUtils showProgressMessage:@"正在发送"];
    [[PersonPageHandler sharedInstance]addFriendId:uid_ success:^(id obj) {
        [AppUtils showSuccessMessage:@"发送成功"];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

#pragma mark - PersonEvaluateCellDelegate
- (void)didTapEvaluateCell:(PersonEvaluateEntity *)evaluate
{
    GoodsDetailViewController *controller = [[GoodsDetailViewController alloc]initWithGoodsId:evaluate.goodsId];
    alpha_ = self.navigationController.navigationBar.alpha;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.cancelButtonIndex == buttonIndex) {
        return;
    }

    if (actionSheet.tag == 1001) {
        if (buttonIndex==0) {
            //编辑个人资料
            MineInfoViewController *mineInfoController = [[MineInfoViewController alloc]init];
            alpha_ = self.navigationController.navigationBar.alpha;
            [self.navigationController pushViewController:mineInfoController animated:YES];
        }
    }
    else if (actionSheet.tag == 1000){
        if (buttonIndex==0) {
            //修改备注名
            UserDataInfo *uDataInfo = self.personInfo.userInfo;
            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:uDataInfo.uid, @"uid",uDataInfo.remark, @"remark", uDataInfo.avatar, @"avatar", uDataInfo.mobile, @"mobile", nil];
            FriendInfo *friendInfo = [[FriendInfo alloc]initWithDictionary:dict];
            QMRemarkViewController *remarkController = [[QMRemarkViewController alloc]initWithFriendInfo:friendInfo];
            alpha_ = self.navigationController.navigationBar.alpha;
            [self.navigationController pushViewController:remarkController animated:YES];
        }else if (buttonIndex==1){//举报
            QMReportViewController *report=[[QMReportViewController alloc] initWithUserId:uid_];
            [self.navigationController pushViewController:report animated:YES];
        }
    }else if (actionSheet.tag==1002){
        if (buttonIndex==0){//举报
            QMReportViewController *report=[[QMReportViewController alloc] initWithUserId:uid_];
            [self.navigationController pushViewController:report animated:YES];
        }
    }
}

#pragma mark - NSNotification Handler
//更改朋友的备注
- (void)processUpdateFriendRemarkNotify:(NSNotification *)notification
{
    FriendInfo *friendInfo = (FriendInfo *)notification.object;
    self.personInfo.userInfo.remark = friendInfo.remark;
    [self.headView updateUI:self.personInfo.userInfo];
    if (self.personInfo.userInfo.remark.length > 0) {
        self.navigationItem.title = self.personInfo.userInfo.remark;
    }
    else{
        self.navigationItem.title = self.personInfo.userInfo.nickname;
    }
}

- (void)oneTapHandler:(NSNotification *)notification
{
    self.navigationController.navigationBarHidden = NO;
    
    POPBasicAnimation *alphaAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    alphaAnimation.toValue = @(0);
    __weak typeof(self) weakSelf = self;
    alphaAnimation.completionBlock = ^(POPAnimation *anim, BOOL finish){
        if (finish) {
            [weakSelf.imageZoomView removeFromSuperview];
            weakSelf.imageZoomView = nil;
        }
    };
    [self.imageZoomView pop_addAnimation:alphaAnimation forKey:@"alphaAnim"];
}

- (void)becomeActiveHandler:(NSNotification *)notification
{
    self.navigationController.navigationBar.alpha = alpha_;
}

- (void)enteredBackgroundHandler:(NSNotification *)notification
{
    alpha_ = self.navigationController.navigationBar.alpha;
}

- (void)userInfoUpdate:(NSNotification *)notification
{
    [self requestUserInfo];
}

#pragma mark - Button Action
- (void)moreAction:(id)sender
{
    NSString *uid = [UserDefaultsUtils valueWithKey:kUserId];
    if ([uid isEqualToString:uid_]) {
        //编辑资料
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"编辑资料", nil];
        actionSheet.tag = 1001;
        [actionSheet showInView:self.view];
    }
    else{
        //修改备注名
        if (self.personInfo.relation.isFriends){
            UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"修改备注名",@"举报", nil];
            actionSheet.tag = 1000;
            [actionSheet showInView:self.view];
        }else{
            UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"举报", nil];
            actionSheet.tag = 1002;
            [actionSheet showInView:self.view];
        }
    }
}

#pragma mark PersonNumIntroDelegate
-(void)credibilityClicked{
    QMQAViewController *qa=[[QMQAViewController alloc] initWithType:QMQATypeKeXinDu];
    [self.navigationController pushViewController:qa animated:YES];
}

-(void)influenceClicked{
    QMQAViewController *qa=[[QMQAViewController alloc] initWithType:QMQATypeYingXiangLi];
    [self.navigationController pushViewController:qa animated:YES];
}

-(void)tradingVolumeClicked{
    QMQAViewController *qa=[[QMQAViewController alloc] initWithType:QMQATypeTotalVolume];
    [self.navigationController pushViewController:qa animated:YES];
}

-(void)goodRateClicked{
    QMQAViewController *qa=[[QMQAViewController alloc] initWithType:QMQATypeHaoPingLv];
    [self.navigationController pushViewController:qa animated:YES];
}

@end
