//
//  UserDao.m
//  QMMM
//  对user表的操作
//  Created by kingnet  on 14-9-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "UserDao.h"
#import "FMDatabase.h"
#import "UserDefaultsUtils.h"

@implementation UserDao
//@synthesize currentUser = _currentUser;

+ (UserDao *)sharedInstance
{
    static UserDao *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (BOOL)insert:(BaseEntity *)entity
{
    UserEntity *user = (UserEntity *)entity;
    NSDictionary *userDic = [user toDictionary];
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *insertSQL = [[QMDatabaseHelper sharedInstance]createInsertSQL:kUserTable];
    return [db executeUpdate:insertSQL withParameterDictionary:userDic];
}

- (BOOL)isUserExists:(NSString *)uid
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *selectSQL = [NSString stringWithFormat:@"select * from %@ where uid = ?", kUserTable];
    return [[db executeQuery:selectSQL, uid] next];
}

- (BaseEntity *)queryById:(NSString *)entityId
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *selectSQL = [NSString stringWithFormat:@"select * from %@ where uid = ?", kUserTable];
    FMResultSet *rs = [db executeQuery:selectSQL, entityId];
    UserEntity *user = nil;
    while ([rs next]) {
        user = [[UserEntity alloc]initWithDictionary:[rs resultDictionary]];
        break;
    }
    
    return user;
}

- (BOOL)removeClientEntityWithId:(NSString *)entityId
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *deleteSQL = [NSString stringWithFormat:@"delete from %@ where uid = ?", kUserTable];
    BOOL rs = [db executeUpdate:deleteSQL, entityId];
    return rs;
}

- (BOOL)update:(BaseEntity *)entity propertyName:(NSString *)propertyName
{
    UserEntity *user = (UserEntity *)entity;
    NSDictionary *dic = [user toDictionary];
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    
    NSString *updateSQL = [NSString stringWithFormat:@"update %@ set %@ = ? where uid = ?", kUserTable, propertyName];

    return [db executeUpdate:updateSQL, [dic objectForKey:propertyName], user.uid];
}

///****************** current user *********************/
//- (UserEntity *)currentUser
//{
//    if (_currentUser == nil) {
//        
//        NSString *uid = [UserDefaultsUtils valueWithKey:kUserId];
//        _currentUser = (UserEntity *)[self queryById:uid];
//        
//    }
//    return _currentUser;
//}
//
//- (void)setCurrentUser:(UserEntity *)currentUser
//{
//    //保存uid
//    [UserDefaultsUtils saveValue:currentUser.uid forKey:kUserId];
//    //保存当前的状态
//    [UserDefaultsUtils saveBoolValue:YES withKey:kIsLogin];
//    
//    //设置当前的用户
//    NSDictionary *userDic = [currentUser toDictionary];
//    _currentUser = [[UserEntity alloc]initWithDictionary:userDic error:nil];
//}

@end
