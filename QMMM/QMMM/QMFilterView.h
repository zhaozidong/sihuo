//
//  QMFilterView.h
//  QMMM
//
//  Created by kingnet  on 15-3-16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class QMFilterView, QMFilterItem;
@protocol QMFilterViewDataSource <NSObject>
@required
//获取需要显示多少个item， 最多支持5个，超过5个后面的将被忽略
- (NSInteger)numberOfItemForFilterView:(QMFilterView *)filterView;
//获取每个item上显示的title
- (NSString *)filterView:(QMFilterView *)filterView titleForItem:(QMFilterItem *)filterItem;
//获取下拉时要展示的view
- (UIView *)filterView:(QMFilterView *)filterView viewAtIndex:(NSUInteger)index;
@end

@protocol QMFilterViewDelegate <NSObject>

@end

@interface QMFilterView : UIView

@property (nonatomic, weak) id <QMFilterViewDataSource> dataSource;

@property (nonatomic, assign, readonly) NSInteger selectedIndex;

- (void)reloadItemAtIndex:(NSUInteger)index;

- (void)dismissWithCompletion:(void(^)(void))completion;

+ (CGFloat)viewHeight;

@end
