//
//  OrderGoodsInfoCell.m
//  QMMM
//
//  Created by kingnet  on 14-9-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderGoodsInfoCell.h"
#import "QMGoodsHeadButton.h"
#import "UIImageView+QMWebCache.h"
#import "GoodEntity.h"

@interface OrderGoodsInfoCell ()
@property (weak, nonatomic) IBOutlet UIImageView *goodsImgV;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLbl;
@property (weak, nonatomic) IBOutlet UIImageView *textBgImgV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightCst;

@end

@implementation OrderGoodsInfoCell

+ (OrderGoodsInfoCell *)goodsInfoCell
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"OrderGoodsInfoCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.priceLbl.textColor = kDarkTextColor;
    self.priceLbl.text = @"";
    self.priceLbl.backgroundColor = [UIColor clearColor];
    self.descLbl.text = @"";
    self.descLbl.textColor = kDarkTextColor;
    self.descLbl.font = [UIFont systemFontOfSize:14.f];
    self.descLbl.numberOfLines = 2;
    self.descLbl.backgroundColor = [UIColor clearColor];
    self.descLbl.textAlignment = NSTextAlignmentLeft;
    self.descLbl.lineBreakMode = NSLineBreakByTruncatingTail;
    self.descLbl.preferredMaxLayoutWidth = CGRectGetWidth(self.frame);
    self.totalPriceLbl.text = @"";
    self.totalPriceLbl.textColor = kRedColor;
    self.textBgImgV.image = [[UIImage imageNamed:@"descUsage_bg"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
}

- (void)updateUI:(GoodEntity *)entity
{
    if (entity) {
        UIImage *placeholder = [UIImage imageNamed:@"goodsPlaceholder_small"];
        NSString *imageUrl = [AppUtils thumbPath:entity.picturelist.rawArray[0] sizeType:QMImageHalfSize];
        [self.goodsImgV qm_setImageWithURL:imageUrl placeholderImage:placeholder];
        self.priceLbl.text = [NSString stringWithFormat:@"单价：%.2f", entity.price];
        self.totalPriceLbl.text = [NSString stringWithFormat:@"¥ %.2f", entity.price];
        
        NSString *strUsage=[[GoodEntity usageDesc] objectAtIndex:entity.isNew];
        NSString * displayname = [NSString stringWithFormat:@"%@ %@", strUsage, entity.desc];
        
        //属性字符串设置属性
        NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:displayname];
        [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.0] range:NSMakeRange(0, [strUsage length]+1)];
        [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange([strUsage length]+1, [attributeString length]-([strUsage length]+1))];
        [attributeString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x99a0aa) range:NSMakeRange(0, [strUsage length]+1)];
    
        self.descLbl.attributedText = attributeString;
        
        NSMutableAttributedString * attributeString1 = [[NSMutableAttributedString alloc] initWithString:strUsage];
        [attributeString1 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.0] range:NSMakeRange(0, [strUsage length])];
        
        [attributeString1 addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x99a0aa) range:NSMakeRange(0, strUsage.length)];
        //width - index - margin
        CGRect rect = [attributeString1 boundingRectWithSize:CGSizeMake(100, 17) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine context:nil];
        CGFloat width = rect.size.width;
        self.imageHeightCst.constant = width+4;
        [self.contentView layoutIfNeeded];
    }
}

- (void)setTotalPrice:(float)totalPrice
{
    self.totalPriceLbl.text = [NSString stringWithFormat:@"¥ %.2f", totalPrice];
}

+ (CGFloat)cellHeight
{
    return 116.f;
}

@end
