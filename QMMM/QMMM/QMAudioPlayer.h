//
//  QMAudioPlayer.h
//  QMMM
//
//  Created by hanlu on 14-9-18.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface QMAudioPlayer : AVAudioPlayer

@property (nonatomic, assign) uint64_t tag;

@end
