//
//  ImageDisplayViewController.m
//  QMMM
//
//  Created by Shinancao on 14-9-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "ImageDisplayViewController.h"
#import "ImageDisplayView.h"
#import "UIImage+IF.h"


@interface ImageDisplayViewController ()<ImageDisplayViewDelegate>{
    BOOL _hasPath;
    BOOL _isEdit;
    NSString *imageFilePath;
}
@property (nonatomic, strong) ImageDisplayView *displayView;
@end

@implementation ImageDisplayViewController

- (id)initWithImage:(UIImage *)image
{
    self = [super init];
    if (self) {
        _hasPath=NO;
        self.image = image;
    }
    return self;
}

- (id)initWithImage:(UIImage *)image andPath:(NSString *)path isEdit:(BOOL)isEdit{
    self = [super init];
    if (self) {
        _hasPath=YES;
        _isEdit=isEdit;
        self.image = image;
        self.photoPath=path;
    }
    return self;
}

-(UIImage *)processImage:(UIImage *)imageFrom{
    
    UIImage *image = imageFrom;
    //进行一次处理
    
    if (!_isEdit) {
        CGSize size = [[self class]preferSize];
        float w = size.width, h = size.height;

        if ((image.size.width/w)<(image.size.height/h)) {

            h = (w/image.size.width)*image.size.height;
        }
        else{
            w = (h/image.size.height)*image.size.width;
        }

        UIGraphicsBeginImageContextWithOptions(CGSizeMake(w, h), YES, [UIScreen mainScreen].scale);
        CGRect scaledImageRect = CGRectMake( 0.0, 0.0, w, h);
        [image drawInRect:scaledImageRect];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();

        float x = (image.size.width-size.width)/2;
        float y = ((image.size.height-size.height)/2)+kCameraTopHeight*image.scale;
        image = [image cropImageNoRotateWithBounds:CGRectMake(x, y, size.width, size.width)];
    }
    
    return image;
//    self.displayView.image = self.image;
}

+ (CGSize)preferSize
{
    float scale = [UIScreen mainScreen].scale;
    return CGSizeMake(kMainFrameWidth*scale, (kMainFrameHeight-100)*scale);
    //    return CGSizeMake((kMainFrameHeight-100)*scale, kMainFrameWidth*scale);
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (_hasPath) {
        [self.view addSubview:[self displayViewWithPath:self.photoPath]];
    }else{
        [self.view addSubview:[self displayView]];
    }
    
//    self.displayView.image = self.image;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.image=[self processImage:self.image];
    self.displayView.image = self.image;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [AppUtils hideStatusBar:YES viewController:self];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (ImageDisplayView *)displayView
{
    if (!_displayView) {
        _displayView = [[ImageDisplayView alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight)];
        _displayView.deleteBeforeSave=NO;
        _displayView.delegate = self;
    }
    return _displayView;
}

- (ImageDisplayView *)displayViewWithPath:(NSString *)path
{
    if (!_displayView) {
        _displayView = [[ImageDisplayView alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight)];
        _displayView.deleteBeforeSave=YES;
        _displayView.photoPath=path;
        _displayView.delegate = self;
    }
    return _displayView;
}

-(UIImage *)rotateImage:(UIImage *)image ByDegree:(float)degrees{
    return [image imageRotatedByDegrees:degrees];
}


#pragma mark - view delegate

- (void)didTapBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)finishWithImageFile:(NSString *)filePath
{
    [[NSNotificationCenter defaultCenter]postNotificationName:kAddImageNotification object:filePath];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)finishUpdateImageFile{
    [[NSNotificationCenter defaultCenter]postNotificationName:kUpdateImageNotification object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rotateImage:(UIImage *)image{//旋转图片
    
    UIImage *tempImage=[self rotateImage:image ByDegree:90.0];
    self.displayView.image = tempImage;
    
//    NSData *imgData = UIImageJPEGRepresentation(image, 0.9);
//    UInt64 t = (UInt64)([[NSDate date] timeIntervalSince1970]*1000);
//    NSString *timeSp = [NSString stringWithFormat:@"%llu", t];
//    [self saveData:imgData imgName:[NSString stringWithFormat:@"%@_rotate.jpg", timeSp]];
//    
//    if (nil !=imageFilePath) {
//        image=[UIImage imageWithContentsOfFile:imageFilePath];
//    }
}

- (void)restoreOrginalImage{
    self.displayView.image = self.image;
}


-(NSString *)getTempPath:(NSString*)filename{
    NSString *tempPath = NSTemporaryDirectory();
    return [tempPath stringByAppendingPathComponent:filename];
}

//返回path
- (NSString *)saveData:(NSData *)imgData imgName:(NSString *)imgName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dirPath = [self getTempPath:@"cacheImg"];
    BOOL isDir = NO;
    BOOL isDirExist = [fileManager fileExistsAtPath:dirPath isDirectory:&isDir];
    if (!(isDirExist && isDir)) {
        isDirExist = [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    if (isDirExist) {
        NSString *filePath = [self getTempPath:[NSString stringWithFormat:@"cacheImg/%@", imgName]];
        imageFilePath=filePath;
        
        if([fileManager createFileAtPath:filePath contents:imgData attributes:nil]){
            return filePath;
        }
        else{
            return @"";
        }
    }
    else{
        return @"";
    }
}



@end
