//
//  QMGoodsImageCellV2.m
//  QMMM
//
//  Created by hanlu on 14-10-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsImageCellV2.h"
#import "GoodEntity.h"
#import "QMGoodsAudioView.h"
#import "StrikeoutLineLabel.h"
#import "QMPagePhotoView.h"
#import "ImageBrowerView.h"

NSString * const kQMGoodsImageCellV2Indentifier = @"kQMGoodsImageCellV2Indentifier";

@implementation QMGoodsNewPrecentView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _backgroundView = [[UIImageView alloc] initWithFrame:self.bounds];
    _backgroundView.backgroundColor = [UIColor clearColor];
    [self addSubview:_backgroundView];
    
    _textLabel = [[UILabel alloc] initWithFrame:CGRectInset(_backgroundView.frame, 5, 0)];
    [_textLabel setFont:[UIFont systemFontOfSize:12.0]];
    [_textLabel setTextColor:[UIColor whiteColor]];
    [_textLabel setTextAlignment:NSTextAlignmentLeft];
    [_textLabel setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_textLabel];
}

- (void)setText:(NSString *)text
{
    _textLabel.text = text;
    
    [self setNeedsLayout];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    UIImage * image = [UIImage imageNamed:@"newPrecent_bkg"];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(5, 13, 5, 5)];
    _backgroundView.frame = self.bounds;
    _backgroundView.image = image;
    
    CGRect frame = _textLabel.frame;
    frame.origin.x = _backgroundView.frame.origin.x + 12;
    frame.size.width = _backgroundView.frame.size.width - 12;
    _textLabel.frame = frame;
}

@end


@interface QMGoodsImageCellV2 () <QMPagePhotoDataSource> {
    GoodEntity * _goodsInfo ;
}
@property (weak, nonatomic) IBOutlet UIImageView *specialGoodsIcon;

@end

@implementation QMGoodsImageCellV2

+ (id)cellAwakeFromNib
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMGoodsImageCellV2" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _isPreView=NO;
    self.backgroundColor=[UIColor whiteColor];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.contentView.clipsToBounds=YES;
    
    _imageBrowerView.delegate = self;
    
    
    [_priceLabel setTextColor:UIColorFromRGB(0xf5292c)];
    [_priceLabel setFont:[UIFont systemFontOfSize:18.0]];
    [_priceLabel setBackgroundColor:[UIColor clearColor]];
    [_priceLabel sizeToFit];
    
    [_origPriceLabel setTextColor:UIColorFromRGB(0x99a0aa)];
    [_origPriceLabel setStrikeThroughColor:UIColorFromRGB(0x99a0aa)];
    [_origPriceLabel setFont:[UIFont systemFontOfSize:12.0]];
    [_numLabel setTextColor:UIColorFromRGB(0x99a0aa)];
    [_numLabel setTextAlignment:NSTextAlignmentRight];
    [_numLabel setFont:[UIFont systemFontOfSize:12.0f]];
    
    imageSellout=[[UIImageView alloc] initWithFrame:CGRectMake(80, 0, 90, 80)];
    imageSellout.hidden=YES;
    self.specialGoodsIcon.hidden = YES;
    [self.contentView addSubview:imageSellout];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setShowFull:(BOOL)showFull
{
    _showFull = showFull;
}

- (void)updateUI:(GoodEntity *)goodsInfo
{
    _goodsInfo = goodsInfo;
    
    [self layoutSubviews];
//    if (_isPreView) {
//        [_imageBrowerView updateImages:_arrLocalPath selectIndex:0];
//    }else{
//        
//    }
    [_imageBrowerView updateImages:[self goodsImgURLs:goodsInfo.picturelist.rawArray] selectIndex:0];

    //audio
    if (_isPreView) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:goodsInfo.msgAudio]) {
            [_audioView updateWithLocalPath:goodsInfo.msgAudio andAudioTime:goodsInfo.msgAudioSeconds];
        }else{
            [_audioView updateUI:goodsInfo];
        }
    }else{
        [_audioView updateUI:goodsInfo];
    }
    
    //price
    [_priceLabel setText:[NSString stringWithFormat:@"¥ %.2f", goodsInfo.price]];
    
    //orig_price
    if(goodsInfo.orig_price > 0) {
        [_origPriceLabel setText:[NSString stringWithFormat:@"¥ %.2f", goodsInfo.orig_price]];
    } else {
        [_origPriceLabel setText:@""];
    }
    
    //newPrecent
//    [_newsPrecentView setText:[[GoodEntity usageDesc] objectAtIndex:goodsInfo.isNew]];
    
    //goods remain
    if(_showFull) {
        [_numLabel setText:[NSString stringWithFormat:@"库存: %u", goodsInfo.number-goodsInfo.lockNum]];
    }
    if (goodsInfo && goodsInfo.number==0) {
//        imageSellout=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sellOut"]];
        imageSellout.image=[UIImage imageNamed:@"sellOut"];
        imageSellout.hidden=NO;
        [self.contentView addSubview:imageSellout];
    }else{
        imageSellout.image=nil;
        imageSellout.hidden=YES;
    }
    
    if (goodsInfo && goodsInfo.isSpecialGoods) {
        self.specialGoodsIcon.hidden = NO;
    }
    else{
        self.specialGoodsIcon.hidden = YES;
    }
}

- (void)updateUI:(GoodEntity *)goodsInfo localImgPath:(NSArray *)arrPath localAudioPath:(NSString *)audioPath{
    _isPreView=YES;
    [self updateUI:goodsInfo];
}


- (NSArray *)goodsImgURLs:(NSArray *)goodsOriURLs
{
    NSMutableArray *array = [NSMutableArray array];
    for (NSString *str in goodsOriURLs) {
        if (_isPreView) {
            //本地路径
            NSFileManager *fileManager = [NSFileManager defaultManager];
            if ([fileManager fileExistsAtPath:str]) {
                [array addObject:str];
            }
            else{
                NSString *thumbS = [AppUtils thumbPath:str sizeType:QMImageOneSize];
                NSString *url = [AppUtils getAbsolutePath:thumbS];
                [array addObject:url];
            }
        }
        else{
            NSString *thumbS = [AppUtils thumbPath:str sizeType:QMImageOneSize];
            NSString *url = [AppUtils getAbsolutePath:thumbS];
            [array addObject:url];
        }
    }
    return array;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame;
    if (_isPreView) {
        frame =self.contentView.frame;
        frame.origin.x=0;
        frame.size.width=kMainFrameWidth;
        self.contentView.frame=frame;
    }
    
    CGRect contentViewRect = self.contentView.bounds;
    
    frame = _imageBrowerView.frame;
    
    frame.origin.x = contentViewRect.origin.x;
    frame.origin.y = contentViewRect.origin.y+10;
    frame.size.width = contentViewRect.size.width;
    frame.size.height=contentViewRect.size.width;
    
    _imageBrowerView.frame = frame;
    
    frame = _audioView.frame;
    frame.origin.x = contentViewRect.origin.x + (contentViewRect.size.width - _audioView.frame.size.width) / 2;
    frame.origin.y = _imageBrowerView.frame.origin.y + _imageBrowerView.frame.size.height - _audioView.frame.size.height / 2-10;
    _audioView.frame = frame;
    
    NSString *strPrice=[NSString stringWithFormat:@"¥ %.2f", _goodsInfo.price];
    CGRect r = [strPrice boundingRectWithSize:CGSizeMake(kMainFrameWidth/2-30, 17)
                                  options:NSStringDrawingUsesLineFragmentOrigin
                               attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18]}
                                  context:nil];
    if (_isPreView) {
        frame.origin.x =25;
    }else{
        frame.origin.x =15;
    }

    if(0 != _goodsInfo.msgAudioSeconds)
        frame.origin.y =_imageBrowerView.frame.origin.y+_imageBrowerView.frame.size.height+23;
    else{
        frame.origin.y =_imageBrowerView.frame.origin.y+_imageBrowerView.frame.size.height+10;
    }
    
//    frame.size.width=kMainFrameWidth/2-30;
//    frame.size.height=18;
    frame.size.width=r.size.width;
    frame.size.height=r.size.height;
    _priceLabel.frame=frame;
    
    NSString *strOriPrice=[NSString stringWithFormat:@"¥ %.2f", _goodsInfo.orig_price];
    r = [strOriPrice boundingRectWithSize:CGSizeMake(kMainFrameWidth/2-30, 17)
                                  options:NSStringDrawingUsesLineFragmentOrigin
                               attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18]}
                                  context:nil];
    frame.origin.x=_priceLabel.frame.origin.x+ _priceLabel.frame.size.width+5;
    frame.origin.y=_priceLabel.frame.origin.y;
    frame.size.width=r.size.width;
    frame.size.height=r.size.height;
    _origPriceLabel.frame=frame;
    
    
//    CGFloat rightMargin = 6;
//    CGSize textSize = [_newsPrecentView.textLabel.text sizeWithFont:_newsPrecentView.textLabel.font];
//    textSize.width += 15;
//    frame = _newsPrecentView.frame;
//    frame.origin.x = contentViewRect.origin.x + contentViewRect.size.width - textSize.width - rightMargin;
//    frame.size.width = textSize.width;
//    _newsPrecentView.frame = frame;
    
//    frame = _numLabel.frame;
//    frame.origin.x = _newsPrecentView.frame.origin.x + _newsPrecentView.frame.size.width - _numLabel.frame.size.width;
    
    frame.origin.x=self.contentView.frame.size.width-80;
    frame.origin.y=_origPriceLabel.frame.origin.y;
    frame.size.width=60;
    frame.size.height=25;
    
    _numLabel.frame = frame;
    
    imageSellout.frame=CGRectMake(80, 0, 90, 80);
    
    self.specialGoodsIcon.frame = CGRectMake(CGRectGetWidth(self.contentView.frame)-10-32, 10, 32, 32);
    
}

#pragma mark - QMPagePhotoDataSource

- (NSInteger)numberOfPage
{
    return [_goodsInfo.picturelist.rawArray count];
}

- (NSString *)imageUrlAtIndex:(NSInteger)index
{
    return [_goodsInfo.picturelist.rawArray objectAtIndex:index];
}

#pragma mark - UIGestureRecognizer

- (void)tap:(UIGestureRecognizer *)recognizer
{

}

- (void)clickeImageAtIndex:(NSInteger)index
{
    BOOL isDelegate = (_delegate && [_delegate respondsToSelector:@selector(clickeImageAtIndex:)]);
    if (isDelegate) {
        [_delegate clickeImageAtIndex:index];
    }
    
//    if(_showFull) {
//        CGPoint point = [recognizer locationInView:self];
//        if(CGRectContainsPoint(_imageBrowerView.frame, point)) {
//            if(_delegate && [_delegate respondsToSelector:@selector(didImageClick:)]) {
//                [_delegate clickeImageAtIndex:index];
//            }
//        }
//    } else {
//        if(_delegate && [_delegate respondsToSelector:@selector(didImageClick:)]) {
//            //            [_delegate didImageClick:[_pageControlView getCurrentPage]];
//        }
//    }
}

@end
