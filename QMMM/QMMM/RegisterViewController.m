//
//  RegisterViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "RegisterViewController.h"
#import "LRInputViewController.h"
#import "UserProtocolView.h"
#import "UserProtocolViewController.h"
#import "RegisterCheckCodeViewController.h"
#import "UserInfoHandler.h"
#import "FillUserInfoViewController.h"

@interface RegisterViewController ()<LRInputViewControllerDelegate, UserProtocolViewDelegate>

@property (nonatomic, strong) LRInputViewController *inputController;

@property (nonatomic, strong) UserProtocolView *proView;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"注册(1/2)";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    self.canTapCloseKeyboard = YES;
    
    self.inputController = [[LRInputViewController alloc]init];
    self.inputController.submitBtnTitle = @"下一步";
    self.inputController.delegate = self;
    self.inputController.returnKeyType = UIReturnKeyNext;
    self.inputController.isRegisterView = YES;
    
    [self addChildViewController:self.inputController];
    
    [[self view] addSubview:[self.inputController view]];
    
    [self.inputController didMoveToParentViewController:self];
    
    //添加用户协议view
    self.proView = [UserProtocolView protocolView];
    self.proView.delegate = self;
    self.proView.frame = CGRectMake((CGRectGetWidth(self.view.frame)-[UserProtocolView viewSize].width)/2, CGRectGetMaxY(self.inputController.view.frame)-10, [UserProtocolView viewSize].width, [UserProtocolView viewSize].height);
    [self.view addSubview:self.proView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.inputController = nil;
}

#pragma mark - 
- (void)clickSubmitBtn:(NSString *)phoneNum pwd:(NSString *)pwd checkCode:(NSString *)checkCode
{
    if (!self.proView.isCheckUserPro) {
        [AppUtils showAlertMessage:@"需要同意用户协议哦~"];
        return;
    }
    
    //统计点击注册的下一步的次数
    [AppUtils trackCustomEvent:@"register_click_next" args:nil];
    
    //校验验证码是否正确
    [AppUtils showProgressMessage:@"正在校验验证码"];
    __weak typeof(self) weakSelf = self;
    [[UserInfoHandler sharedInstance]smsVerify:@"register" mobile:phoneNum captcha:checkCode success:^(id obj) {
        [AppUtils dismissHUD];
        
        FillUserInfoViewController *controller = [[FillUserInfoViewController alloc]initWithPhoneNum:phoneNum checkCode:checkCode pwd:pwd];
        [weakSelf.navigationController pushViewController:controller animated:YES];
        
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)clickUserProtocol
{
    UserProtocolViewController *controller = [[UserProtocolViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)clickGetCheckCode:(NSString *)phoneNum
{
    //统计点击获取验证码的次数
    [AppUtils trackCustomEvent:@"register_click_checkcode" args:nil];
    
    [[UserInfoHandler sharedInstance] sendSMSWithMobile:phoneNum type:@"register" success:^(id obj) {
#ifdef DEBUG
        [AppUtils showAlertMessage:[NSString stringWithFormat:@"验证码：%@(仅供测试用)", (NSString *)obj]];
#else
        if (kIsTestRelease) {
            [AppUtils showAlertMessage:[NSString stringWithFormat:@"验证码：%@(仅供测试用)", (NSString *)obj]];
        }
#endif
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        //重置button
        [self.inputController stopTimer];
    }];
}

- (void)viewShouldMoveY:(CGFloat)Y
{
    CGRect frame = self.view.frame;
    frame.origin.y = -Y;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = frame;
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerObserver];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeObserver];
}

- (void)registerObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processKeyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
}

- (void)removeObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)processKeyboardWillHide:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0 ;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    frame.size.height = kMainFrameHeight;
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.view.frame = frame;
    }];
}

@end
