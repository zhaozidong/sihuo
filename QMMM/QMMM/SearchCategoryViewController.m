//
//  SearchCategoryViewController.m
//  QMMM
//
//  Created by kingnet  on 15-3-19.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "SearchCategoryViewController.h"
#import "QMGoodsCateInfo.h"
#import "QMGoodsCatetCell.h"
#import "SearchHistoryViewController.h"
#import "GoodsHandler.h"
#import "QMGoodsSearchViewController.h"
#import "SearchHistoryDao.h"
#import "KeyWordEntity.h"

@interface SearchCategoryViewController ()<UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate, SearchHistoryViewControllerDelegate>
{
    SearchHistoryViewController *historyViewController_;
    UITextField *textField_;
    UIButton *cancelButton_;
}

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic, strong) NSMutableArray *cateList;
@end

@implementation SearchCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.titleView = self.searchBar;
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    [self.view addSubview:self.tableView];
    
    self.cateList = [NSMutableArray array];
    
    [self getGoodsCategory];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)getGoodsCategory
{
    [AppUtils showProgressMessage:@"加载中"];
    __weak typeof(self) weakSelf = self;
    [[GoodsHandler shareGoodsHandler]getCategory:^(id obj) {
        [AppUtils dismissHUD];
        
        NSArray *arr = (NSArray *)obj;
        [weakSelf.cateList removeAllObjects];
        [weakSelf.cateList addObjectsFromArray:arr];
        [weakSelf.tableView reloadData];
        
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

#pragma mark - init View

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
        [_tableView registerNib:[QMGoodsCatetCell nib] forCellReuseIdentifier:kQMGoodsCatetCellIdentifier];
        _tableView.rowHeight = [QMGoodsCatetCell cellHeight];
    }
    return _tableView;
}

- (UISearchBar *)searchBar
{
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
        _searchBar.delegate = self;
        
        UIImage* clearImg = [UIImage imageNamed:@"search_bg"];
        clearImg = [clearImg stretchableImageWithLeftCapWidth:7 topCapHeight:5];
        [_searchBar setBackgroundImage:clearImg];
        [_searchBar setSearchFieldBackgroundImage:clearImg forState:UIControlStateNormal];
        [_searchBar setBackgroundColor:[UIColor clearColor]];
        [_searchBar setImage:[UIImage imageNamed:@"search_icon"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
        [_searchBar setImage:[UIImage imageNamed:@"searchClearDel_icon"] forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
        
        for (UIView *subView in _searchBar.subviews) {
            for (UIView *view in subView.subviews) {
                if ([view isKindOfClass:[UITextField class]]) {
                    textField_ = (UITextField *)view;
                }
                if ([view isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                    [view removeFromSuperview];
                }
            }
        }
        
        if (textField_) {
            UIFont *font = [UIFont systemFontOfSize:15];
            NSDictionary *attrDic = @{NSForegroundColorAttributeName:UIColorFromRGB(0xffd4d4), NSFontAttributeName:font};
            NSAttributedString *attrStr = [[NSAttributedString alloc]initWithString:@"输入关键字搜索商品" attributes:attrDic];
            textField_.attributedPlaceholder = attrStr;
            textField_.textColor = [UIColor whiteColor];
        }
    }
    return _searchBar;
}

#pragma mark - UITableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cateList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMGoodsCatetCell *cell = [tableView dequeueReusableCellWithIdentifier:kQMGoodsCatetCellIdentifier];
    QMGoodsCateInfo *cateInfo = [self.cateList objectAtIndex:indexPath.row];
    [cell updateUI:cateInfo];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    QMGoodsCateInfo *cate = [self.cateList objectAtIndex:indexPath.row];
    
    QMGoodsSearchViewController *controller = [[QMGoodsSearchViewController alloc] initWithCategoryId:(int)cate.cateId];
    [self.navigationController pushViewController:controller animated:YES];
    
    NSString *mtaEvent = [NSString stringWithFormat:@"click_category_%ld", cate.cateId];
    [AppUtils trackCustomEvent:mtaEvent args:nil];
}

#pragma mark - UISearchBar Delegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
    self.navigationItem.leftBarButtonItems = nil;
    self.navigationItem.hidesBackButton = YES;
    
    if(kIOS7) {
        UIView *topView = _searchBar.subviews[0];
        for (UIView *subView in topView.subviews) {
            if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
                cancelButton_ = (UIButton*)subView;
                [cancelButton_ setTitle:[AppUtils localizedProductString:@"QM_Text_Cancel"] forState:UIControlStateNormal];
            }
        }
    } else {
        for(UIView *subView in _searchBar.subviews){
            if([subView isKindOfClass:[UIButton class]]){
                cancelButton_ = (UIButton*)subView;
                [cancelButton_ setTitle:[AppUtils localizedProductString:@"QM_Text_Cancel"] forState:UIControlStateNormal];
            }
        }
    }
    
    //显示搜索历史记录
    if (nil == historyViewController_) {
        historyViewController_ = [[SearchHistoryViewController alloc]init];
        historyViewController_.delegate = self;
        [self addChildViewController:historyViewController_];
        [historyViewController_ didMoveToParentViewController:self];
        [historyViewController_ showInView:self.view];
    }
    
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    //取消搜索
    [searchBar setShowsCancelButton:NO animated:YES];
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    [textField_ resignFirstResponder];
    searchBar.text = @"";
    [historyViewController_ dismissWithCompletion:^{
        historyViewController_ = nil;
    }];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    //点击搜索按钮
    NSString *searchText = _searchBar.text;
    QMGoodsSearchViewController *controller = [[QMGoodsSearchViewController alloc]initWithKeyWords:searchText];
    [self.navigationController pushViewController:controller animated:YES];
    
    [self searchBarCancelButtonClicked:_searchBar];
    
    //存储搜索记录
    KeyWordEntity *entity = [[KeyWordEntity alloc]init];
    entity.keyWord = searchText;
    [[SearchHistoryDao sharedInstance]insert:entity];
}

#pragma mark -
- (void)closeKeyBorad
{
    [textField_ resignFirstResponder];
    cancelButton_.enabled = YES;
}

//选中搜索历史的某一行
- (void)selectRow:(KeyWordEntity *)entity
{
    QMGoodsSearchViewController *controller = [[QMGoodsSearchViewController alloc]initWithKeyWords:entity.keyWord];
    [self.navigationController pushViewController:controller animated:YES];
    
    [self searchBarCancelButtonClicked:_searchBar];
}

@end
