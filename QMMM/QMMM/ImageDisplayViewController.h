//
//  ImageDisplayViewController.h
//  QMMM
//
//  Created by Shinancao on 14-9-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@interface ImageDisplayViewController : BaseViewController

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *photoPath;

- (id)initWithImage:(UIImage *)image;
- (id)initWithImage:(UIImage *)image andPath:(NSString *)path isEdit:(BOOL)isEdit;

@end
