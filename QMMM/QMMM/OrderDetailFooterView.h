//
//  OrderDetailFooterView.h
//  QMMM
//
//  Created by kingnet  on 15-3-10.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OrderDetailFooterViewDelegate <NSObject>

- (void)didTapGuarantBtn;

@end
@interface OrderDetailFooterView : UIView

@property (nonatomic, strong) id <OrderDetailFooterViewDelegate> delegate;

+ (OrderDetailFooterView *)footerView;

+ (CGFloat)viewHeight;

@end
