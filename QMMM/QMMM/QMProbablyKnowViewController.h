//
//  QMProbablyKnowViewController.h
//  QMMM
//
//  Created by Derek on 15/1/21.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMProbablyKnowViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
