//
//  QMGoodsFilterTitleCell.h
//  QMMM
//
//  Created by hanlu on 14-9-19.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kGoodsFilterTitleCellIdentifier ;

@interface QMGoodsFilterTitleCell : UITableViewCell

+(id)cellForGoodsFilterTitleCell;

@property (nonatomic, strong) IBOutlet UILabel * titleLabel;

@property (nonatomic, strong) IBOutlet UIImageView * flagImageView;

@end
