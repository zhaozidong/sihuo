//
//  WithDrawInputCell.h
//  QMMM
//
//  Created by kingnet  on 14-9-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kWithDrawInputCellIdentifer;

@protocol WithDrawInputCellDelegate <NSObject>

- (void)inputText:(NSString *)text;

@end
@interface WithDrawInputCell : UITableViewCell

@property (nonatomic, strong)NSString *placeholder;

@property (nonatomic, strong)NSString *text;

@property (nonatomic, assign) BOOL textFieldEnabled; //文本框是否可用

@property (nonatomic, assign) BOOL isShowRightView; //是否显示右边的图标

@property (nonatomic, weak) id<WithDrawInputCellDelegate> delegate;

@property (nonatomic, assign) UIKeyboardType keboardType;

+ (CGFloat)rowHeight;

+ (WithDrawInputCell *)inputCell;

@end
