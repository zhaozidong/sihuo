//
//  AppUtils.h
//  QMMM
//
//  Created by kingnet  on 14-8-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageSizeEnum.h"

@interface AppUtils : NSObject

/******************* 获取本地化文本内容 *************/
+ (NSString *)localizedCommonString:(NSString *)key;
+ (NSString *)localizedProductString:(NSString *)key;
+ (NSString *)localizedPersonString:(NSString *)key;

/****************** System Utils ******************/
//弹出UIAlertView
+ (void)showAlertMessage:(NSString *)msg;
//获取MD5加密后字符串
+ (NSString *)md5FromString:(NSString *)str;
//计算签名
+ (NSString *)signWithDictionary:(NSDictionary *)dictionary method:(NSString *)method;
//关闭键盘
+ (void)closeKeyboard;
+ (void)hideStatusBar:(BOOL)hidden viewController:(UIViewController *)viewController;
//返回按钮
+(NSArray *)createBackButtonWithTarget:(id)target selector:(SEL)selector;
//右侧按钮
+(NSArray *)createRightButtonWithTarget:(id)target selector:(SEL)selector title:(NSString *)title size:(CGSize)size imageName:(NSString *)imageName;
//textfield的placeholder的样式
+ (NSAttributedString *)placeholderAttri:(NSString *)placeholder;
//获得系统信息
+(NSDictionary *)systemInfo;
//导航栏的标题
+(UILabel *)titleLable:(NSString *)title;
//url前添加cdnhost
+ (NSString *)getAbsolutePath:(NSString *)path;
//拼接缩略图的url
+ (NSString *)smallImagePath:(NSString *)path width:(int)width;
//返回缩略图的url
+ (NSString *)thumbPath:(NSString *)path sizeType:(QMImageSizeType)sizeType;

/********************* Verification Utils **********************/
//验证手机号码合法性（正则）
+ (BOOL)checkPhoneNumber:(NSString *)phoneNumber;
//验证邮箱的合法性
+ (BOOL)emailValid:(NSString *)text;
//验证快递单号格式
+ (BOOL)checkExpressNumber:(NSString *)expressNumber;

/********************* Cookie Utils *************************/
+ (void)saveCookies:(NSDictionary *)headerFields;
+ (void)loadCookies;
+ (void)clearCookies;
+ (NSString *)getTokenValue;

/********************* SVProgressHUD **********************/
//弹出操作错误信息提示框
+ (void)showErrorMessage:(NSString *)message;
//弹出操作成功信息提示框
+ (void)showSuccessMessage:(NSString *)message;
//弹出加载提示框
+ (void)showProgressMessage:(NSString *) message;
//取消弹出框
+ (void)dismissHUD;


/*********************** Format date **********************/
+(NSString *)stringFromDate:(NSDate *)date;
+(NSString *)stringForOrderDate:(NSDate *)date;
+(NSString *)stringForPublishDate:(NSDate *)date;

+(NSString *)stringForUserLibraryPath;
+(NSString *)stringForUserDocumentPath;

/*********************** Save Temp Image ***********************/
//返回path
+ (NSString *)saveData:(NSData *)imgData imgName:(NSString *)imgName;


/*********************** Get Cache Directory ***********************/
//返回Cache目录
+ (NSString *)getCacheDirectory;
+ (NSString *)getAudioFileName:(NSString *)strPath;

/*********************** Get Server Host **************************/
+ (NSString *)serverHost;
+ (NSString *)uploadServerHost;

/*********************** MTA Track ********************************/
+ (void)trackCustomEvent:(NSString *)event args:(NSArray *)args;
+ (void)trackCustomKeyValueEvent:(NSString *)event props:(NSDictionary *)props;

/*********************** Get Freight *********************************/
+ (float)freightForAddress:(NSString *)address;

+ (NSArray *)getTimeWithInterval:(NSTimeInterval )timeInterval;

/*********************** encode decode *************************************/
+( NSString *)DisEncodeUTF8ToChina:(NSString *)encodeStr;
+( NSString *)EncodeChinaToUTF8:(NSString *)encodeStr;

/********************** System Call ****************************************/
+ (void)systemCall:(NSString *)phoneNum;

@end
