//
//  NSObject+UIWebView.h
//  QMMM
//
//  Created by kingnet  on 14-10-10.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIWebView (Additions)

/**
 加载服务器上的链接
 **/
- (void)loadRemoteUrl:(NSString *)url;

/**
 移除webview上的双击事件
 **/
- (void)removeWebViewDoubleTapGestureRecognizer;

@end
