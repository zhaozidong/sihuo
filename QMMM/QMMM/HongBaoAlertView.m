
//
//  HongBaoAlertView.m
//  QMMM
//
//  Created by kingnet  on 15-3-26.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "HongBaoAlertView.h"
#import <POP/POP.h>

@interface HongBaoAlertView ()
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UIButton *exchangeBtn;

@property (strong, nonatomic) UIControl *overlayView;  //半透明黑色背景

@end

@implementation HongBaoAlertView

- (id)initWithMoney:(int)money
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"HongBaoAlertView" owner:self options:0];
    self = [array lastObject];
    if (self) {
        CGRect screenBounds = [UIScreen mainScreen].bounds;
        self.frame = CGRectMake(0, 0, 210, 290);
        self.center = CGPointMake(CGRectGetWidth(screenBounds)/2, -290/2);
        self.money = money;
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    _label1.textColor = UIColorFromRGB(0xffd83c);
    _label2.textColor = UIColorFromRGB(0xffd83c);
    _moneyLabel.textColor = UIColorFromRGB(0xf55914);
    _moneyLabel.text = @"";
    [_exchangeBtn setTitleColor:UIColorFromRGB(0xf55914) forState:UIControlStateNormal];
    [_exchangeBtn setBackgroundImage:[[UIImage imageNamed:@"yellowBtn"] stretchableImageWithLeftCapWidth:6 topCapHeight:7] forState:UIControlStateNormal];
}

- (void)setMoney:(int)money
{
    _moneyLabel.text = [NSString stringWithFormat:@"%d元", money];
}

//关闭当前界面
- (IBAction)closeAction:(id)sender {
    [self dismissWithCompletion:nil];
}

//兑换
- (IBAction)exchangeAction:(id)sender {
    __weak typeof(self) weakSelf = self;
    [self dismissWithCompletion:^{
        if (weakSelf.alertBlock) {
            weakSelf.alertBlock();
        }
    }];
}

- (UIControl *)overlayView
{
    if (!_overlayView) {
        CGRect screenBounds = [[UIScreen mainScreen]bounds];
        _overlayView = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(screenBounds), CGRectGetHeight(screenBounds))];
        _overlayView.backgroundColor = [UIColor blackColor];
        _overlayView.layer.opacity = 0.0;
    }
    return _overlayView;
}

- (void)show
{
    if(!self.overlayView.superview){
        NSEnumerator *frontToBackWindows = [[[UIApplication sharedApplication]windows]reverseObjectEnumerator];
        
        for (UIWindow *window in frontToBackWindows)
            if (window.windowLevel == UIWindowLevelNormal) {
                [window addSubview:self.overlayView];
                break;
            }
    }
    if(!self.superview)
        [self.overlayView.superview addSubview:self];
    
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    positionAnimation.toValue = @(CGRectGetHeight(screenBounds)/2);
    positionAnimation.springBounciness = 10;
    positionAnimation.springSpeed = 3;
//    [positionAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
//        
//    }];
    
    POPBasicAnimation *opacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation.toValue = @(0.8);
    
    [self.layer pop_addAnimation:positionAnimation forKey:@"positionAnimation"];
    [self.overlayView.layer pop_addAnimation:opacityAnimation forKey:@"opacityAnimation"];
}

- (void)dismissWithCompletion:(void(^)(void))completion
{
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    
    POPBasicAnimation *opacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation.toValue = @(0.0);
    [opacityAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        if (finished) {
            [self.overlayView removeFromSuperview];
        }
    }];
    
    POPBasicAnimation *offscreenAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    offscreenAnimation.toValue = @(CGRectGetHeight(self.frame)/2+CGRectGetHeight(screenBounds));
    [offscreenAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
            if (completion) {
                completion();
            }
        }
    }];
    [self.layer pop_addAnimation:offscreenAnimation forKey:@"offscreenAnimation"];
    [self.overlayView.layer pop_addAnimation:opacityAnimation forKey:@"opacityAnimation"];
}

@end
