//
//  CommentListCell.m
//  QMMM
//
//  Created by kingnet  on 14-9-18.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "CommentListCell.h"
#import "QMGoodsHeadButton.h"
#import "CommentEntity.h"
#import "QMRoundHeadView.h"

NSString * const kCommentListCell = @"kCommentListCell";

@interface CommentListCell ()<QMClickNameViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *commentLbl;

@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet QMClickNameView *nameView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameViewWidthCst;
@property (weak, nonatomic) IBOutlet UIImageView *commentIcon;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@end

@implementation CommentListCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"CommentListCell" bundle:nil];
}

+ (CommentListCell *)cellFromNib
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"CommentListCell" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.nameView.height = 40;
    self.nameView.delegate = self;
    self.commentLbl.text = @"";
    self.timeLbl.text = @"";
    self.commentLbl.textColor = kGrayTextColor;
    self.commentLbl.font = [UIFont fontWithName:kFontName size:14.f];
    self.timeLbl.textColor = UIColorFromRGB(0x99a0aa);
    self.timeLbl.font = [UIFont fontWithName:kFontName size:12.f];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.commentLbl.preferredMaxLayoutWidth = kMainFrameWidth-75;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected) {
        self.containerView.backgroundColor = kCellSelectedColor;
        [UIView animateWithDuration:0.5 animations:^{
            self.containerView.backgroundColor = [UIColor whiteColor];
        }];
    }
    else{
        self.containerView.backgroundColor = [UIColor whiteColor];
    }
}

- (void)updateUI:(CommentEntity *)entity
{
    self.commentLbl.text = entity.comments;
    self.nameView.width = 0;  //需要将其置为0，因为cell会重用
    self.nameView.name = entity.cNickname;
    self.nameView.userId = entity.cUserId;
    self.nameViewWidthCst.constant = self.nameView.width;
    
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:entity.comments_ts];
    NSString * stringDate = [AppUtils stringFromDate:date];
    self.timeLbl.text = stringDate;
    if (entity.pid > 0) {
        self.commentLbl.text = [NSString stringWithFormat:@"回复%@:%@", entity.pnickname, entity.comments];
    }
    else{
        self.commentLbl.text = entity.comments;
    }

}

- (void)setIsFirst:(BOOL)isFirst
{
    _isFirst = isFirst;
    self.commentIcon.hidden = !_isFirst;
}

- (void)clickNameViewWithUid:(uint64_t)userId
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickNameViewWithUid:)]) {
        [self.delegate clickNameViewWithUid:userId];
    }
}

@end
