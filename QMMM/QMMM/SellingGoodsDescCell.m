//
//  SellingGoodsDescCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "SellingGoodsDescCell.h"
#import "StrikeoutLineLabel.h"
#import "UIImageView+QMWebCache.h"
#import "GoodDataInfo.h"

NSString * const kSellingGoodsDescCellIdentifier = @"kSellingGoodsDescCellIdentifier";

@interface SellingGoodsDescCell ()
@property (weak, nonatomic) IBOutlet UIImageView *goodsImgV;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet StrikeoutLineLabel *oriPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *stockLbl;

@end
@implementation SellingGoodsDescCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"SellingGoodsDescCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.descLbl.text = @"";
    self.descLbl.textColor = kDarkTextColor;
    self.descLbl.numberOfLines = 3;
    self.descLbl.preferredMaxLayoutWidth = kMainFrameWidth-120;
    self.stockLbl.text = @"";
    self.stockLbl.textColor = kDarkTextColor;
    self.priceLbl.textColor = kRedColor;
    self.oriPriceLbl.strikeThroughEnabled = YES;
    self.oriPriceLbl.strikeThroughColor = kGrayTextColor;
    self.oriPriceLbl.textColor = kGrayTextColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(GoodsSimpleInfo *)goodsInfo
{
    NSString * imagePath = [AppUtils thumbPath:goodsInfo.photo sizeType:QMImageQuarterSize];
    [self.goodsImgV qm_setImageWithURL:imagePath placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_middle"]];
    self.descLbl.text = goodsInfo.desc;
    self.priceLbl.text = [NSString stringWithFormat:@"¥%.2f", goodsInfo.price];
    self.oriPriceLbl.text = [NSString stringWithFormat:@"¥%.2f", goodsInfo.orig_price];
    self.stockLbl.text = [NSString stringWithFormat:@"库存：%d", goodsInfo.num];
}

+ (CGFloat)cellHeight
{
    return 110.f;
}

@end
