//
//  SellingGoodsTimeCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "SellingGoodsTimeCell.h"
#import "GoodDataInfo.h"

NSString * const kSellingGoodsTimeCellIdentifier = @"kSellingGoodsTimeCellIdentifier";

@interface SellingGoodsTimeCell ()
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

@end
@implementation SellingGoodsTimeCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"SellingGoodsTimeCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.timeLbl.textColor = kLightBlueColor;
    self.timeLbl.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(GoodsSimpleInfo *)goodsInfo
{
    NSString * time = [AppUtils stringForPublishDate:[NSDate dateWithTimeIntervalSince1970:goodsInfo.publish_time]];
    self.timeLbl.text = [NSString stringWithFormat:@"发布时间：%@", time];
}

+ (CGFloat)cellHeight
{
    return 30.f;
}

@end
