//
//  SharedManager.h
//  QMMM
//
//  Created by Derek on 15/2/9.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ShareSDK/ShareSDK.h>
#import <QZoneConnection/QZoneConnection.h>
#import "WeiboSDK.h"
#import "WXApi.h"

typedef NS_ENUM(NSInteger,QMSharedType){
    QMSharedTypeSinaWeiBo,
    QMSharedTypeWeiXin,
    QMSharedTypeWeiXinFriends,
    QMSharedTypeQZone,
    QMSharedTypeTextMessage,
    QMSharedTypeCopyText
};

@interface SharedManager : NSObject

+(SharedManager *)sharedInstance;

+(void)config;

-(void)sharedWithType:(QMSharedType)type title:(NSString *)strTitle content:(NSString *)strContent  description:(NSString *)desc image:(NSString *)image url:(NSString *)strUrl;

@end
