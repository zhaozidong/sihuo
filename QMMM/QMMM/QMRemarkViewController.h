//
//  QMRemarkViewController.h
//  QMMM
//
//  Created by hanlu on 14-11-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class FriendInfo;

@interface QMRemarkViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView * tableView;

- (id)initWithFriendInfo:(FriendInfo *)info;

@end
