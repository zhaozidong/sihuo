//
//  QMCheckExpressViewController.h
//  QMMM
//
//  Created by hanlu on 14-11-10.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMCheckExpressViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIWebView * webView;

/**
 @param expressCo 快递公司拼音
 @param expressNum 快递单号
 @param orderId 订单号
 **/
- (id)initWithExpressInfo:(NSString *)expressCo expressNum:(NSString *)expressNum orderId:(NSString *)orderId;

@end
