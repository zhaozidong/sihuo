//
//  QMGoodsFavorViewController.m
//  QMMM
//
//  Created by hanlu on 14-9-22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsFavorViewController.h"
#import "GoodsHandler.h"
#import "GoodDataInfo.h"
#import "OrderDataInfo.h"
#import "UserDefaultsUtils.h"
#import "QMSelectExpressViewController.h"
#import "QMOrderDetailViewController.h"
#import "QMConfirmReceivingGoodsViewController.h"
#import "GoodsDetailViewController.h"
#import "MyOrderList.h"
#import "QMGoodsFootView.h"
#import "MJRefresh.h"
#import "MJRefreshConst.h"
#import "QMBlankCell.h"
#import "QMCheckExpressViewController.h"
#import "EditGoodsPage1ViewController.h"
#import "GoodEntity.h"
#import "QMFailPrompt.h"
#import "OrderStatusCell.h"
#import "OrderGoodsPriceCell.h"
#import "OrderTimeCell.h"
#import "OrderGoodsDescCell.h"
#import "SellingGoodsControlCell.h"
#import "SellingGoodsDescCell.h"
#import "SellingGoodsTimeCell.h"
#import "QMPayOrderViewController.h"
#import "FinanceHandler.h"
#import "BaseEntity.h"
#import "FinanceDataInfo.h"
#import "ChatViewController.h"
#import "UIScrollView+UzysCircularProgressPullToRefresh.h"


@interface QMGoodsFavorViewController ()<UIScrollViewDelegate, UIAlertViewDelegate, OrderStatusCellDelegate, SellingGoodsControlCellDelegate> {
    EControllerType _controllerType;
    NSMutableArray * _dataArray;
    NSMutableDictionary *_dataDict;
    BOOL _needCheckGetNextPage;
    
    uint64_t _deleteGoodsId;
    GoodsSimpleInfo * _simpleGoodsInfo;
    
     NSMutableDictionary* _expressDict;
}
@end

@implementation QMGoodsFavorViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithControllerType:(EControllerType)type
{
    self = [super initWithNibName:@"QMGoodsFavorViewController" bundle:nil];
    if (self) {
        _controllerType = type;
        _dataArray = [[NSMutableArray alloc] init];
        _dataDict = [[NSMutableDictionary alloc] init];
         _myOrderList = [[MyOrderList alloc] initWithType:_controllerType];
    }
    return self;
}

- (void)initNavBar
{
    self.navigationItem.title = [self title];
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(btnBackClick:)];
    
//    UILabel * titlelabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
//    titlelabel.backgroundColor = [UIColor clearColor];
//    titlelabel.textAlignment = NSTextAlignmentCenter;
//    titlelabel.text = [self title];
//    [self.navigationItem setTitleView:titlelabel];
}

- (NSString *)title
{
    if(ECT_ORDER_MGR == _controllerType) {
        return [AppUtils localizedProductString:@"QM_Text_Order_Mgr"];
    } else if(ECT_GOODS_MGR == _controllerType) {
        return [AppUtils localizedProductString:@"QM_Text_Goods_Mgr"];
    } else if(ECT_BUYED_GOODS == _controllerType) {
        return [AppUtils localizedProductString:@"QM_Text_Bought_Goods"];
    } else if(ECT_FAVOR_GOODS == _controllerType) {
        return [AppUtils localizedProductString:@"QM_Text_Like"];
    }
    
    return @"";
}

- (void)registerObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processMyCacheListLoadedNotify:) name: kMyCacheListLoadedNotify object:nil];
    [nc addObserver:self selector:@selector(processMyCacheListLoadStateNotify:) name: kMyCacheListLoadStateNotify object:nil];
    [nc addObserver:self selector:@selector(processUpdateCacheNotify:) name:kUpdateGoodsSimpleInfoNotify object:nil];
    [nc addObserver:self selector:@selector(processUpdateOrderStatusNtf:) name:kUpdateOrderStatusNotify object:nil];
    [nc addObserver:self selector:@selector(processUpdateOrderStatusNtf:) name:kUpdateOrderListStatusNotify object:nil];
    [nc addObserver:self selector:@selector(processNetworkErrorNotify:) name:kNotNetworkErrorNotity object:nil];
    [nc addObserver:self selector:@selector(processDeleteOrderNotify:) name:kDeleteOrderOKNotify object:nil];
    [nc addObserver:self selector:@selector(processUpdateOrderPriceNtf:) name:kUpdateOrderPriceNotify object:nil];
    [nc addObserver:self selector:@selector(processShowComments:) name:kShowCommentsNotify object:nil];
}


- (void)initExpressDict
{
    if(_controllerType == ECT_BUYED_GOODS) {
        _expressDict = [[NSMutableDictionary alloc] init];
        
        NSString * filePath = [[NSBundle mainBundle] pathForResource:@"express" ofType:@"plist"];
        NSDictionary * dict = [NSDictionary dictionaryWithContentsOfFile:filePath];
        for (int i = 1; i <= [dict count]; i ++) {
            NSString * key = [NSString stringWithFormat:@"Item%d", i];
            NSDictionary * temp = [dict objectForKey:key];
            NSString * name = [temp objectForKey:@"desc"];
            [_expressDict setObject:temp forKey:name];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.frame = CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight);
    self.tableView.frame = self.view.frame;
    
    [self initNavBar];
    
    //[self initExpressDict];
    
    [self registerObserver];
    
    //初始化tableView
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = kBackgroundColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kBorderColor;
    ((UIScrollView*)_tableView).delegate = self;
    //注册xib
    [_tableView registerNib:[OrderStatusCell nib] forCellReuseIdentifier:kOrderStatusCellIdentifier];
    [_tableView registerNib:[OrderGoodsDescCell nib] forCellReuseIdentifier:kOrderGoodsDescCellIdentifier];
    [_tableView registerNib:[OrderGoodsPriceCell nib] forCellReuseIdentifier:kOrderGoodsPriceCellIdentifier];
    [_tableView registerNib:[OrderTimeCell nib] forCellReuseIdentifier:kOrderTimeCellIdentifier];
    [_tableView registerNib:[SellingGoodsControlCell nib] forCellReuseIdentifier:kSellingGoodsControlCellIdentifier];
    [_tableView registerNib:[SellingGoodsDescCell nib] forCellReuseIdentifier:kSellingGoodsDescCellIdentifier];
    [_tableView registerNib:[SellingGoodsTimeCell nib] forCellReuseIdentifier:kSellingGoodsTimeCellIdentifier];
    
    if(kIOS7) {
         _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    
    [self setupRefresh];
    
   // [_myOrderList loadNextPage:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _tableView.delegate = nil;
}

- (void)btnBackClick:(id)sender
{
    [AppUtils dismissHUD];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _tableView.delegate = nil;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)processDeleteOrderNotify:(NSNotification *)notification
{
    [_myOrderList loadNextPage:YES];
}

#pragma mark - 

- (void)reloadMyCacheList
{
    NSMutableArray * tmpArray = [[NSMutableArray alloc] init];
    NSMutableDictionary * tmpDict = [[NSMutableDictionary alloc] init];
    
    //更新数据源
    @synchronized(_myOrderList.cacheList) {
        for (int i = 0; i < [_myOrderList.cacheList count]; i++) {
            
            id temp = [_myOrderList.cacheList objectAtIndex:i];
            NSString * key = nil;
            if(_controllerType == ECT_ORDER_MGR || _controllerType == ECT_BUYED_GOODS) {
                OrderSimpleInfo * info = (OrderSimpleInfo *)temp;
                key = info.order_id;
            } else {
                GoodsSimpleInfo * info = (GoodsSimpleInfo *)temp;
                key = [NSString stringWithFormat:@"%qu", info.gid];
            }
            
            [tmpArray addObject:temp];
            [tmpDict setObject:temp forKey:key];
        }
    }
    
    _dataArray = tmpArray;
    _dataDict = tmpDict;
    
    //刷新
    [_tableView reloadData];
}

- (void)refreshStateViewUI
{
    if (!_myOrderList.loading) {
        if ([self.tableView showPullToRefresh]) {
            [self.tableView stopRefreshAnimation];
        }
        
        if ([self.tableView.footer isRefreshing]) {
            [self.tableView.footer endRefreshing];
        }
        
        if (_myOrderList.endOfAll) {
            //全部加载完了，取消footer
            [self.tableView removeFooter];
        }
    }
}

#pragma mark - UITableViewDataSource

- (BOOL)isFirstPage
{
    @synchronized(_myOrderList) {
        if(_myOrderList && !_myOrderList.loading && _myOrderList.endOfAll && [_dataArray count] == 0) {
            return TRUE;
        }
    }
    return FALSE;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    if([self isFirstPage]) {
        return 1;
    } else {
        return [_dataArray count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self isFirstPage]) {
        return 1;
    }
    if (ECT_GOODS_MGR == _controllerType) {//正在卖的
        return 3;
    }
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self isFirstPage]) {
        QMBlankCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMBlankCellIdentifier];
        if(nil == cell) {
            cell = [QMBlankCell cellForBlank];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (ECT_GOODS_MGR == _controllerType) {
            cell.failGoodsType = QMFailTypeNOSellingGoods;
        }
        else if (ECT_ORDER_MGR == _controllerType){
            cell.failGoodsType = QMFailTypeNOSoldGoods;
        }
        else if (ECT_BUYED_GOODS == _controllerType){
            cell.failGoodsType = QMFailTypeNOBoughtGoods;
        }
        return cell;
        
    } else {
        //在卖的商品
        if(ECT_GOODS_MGR == _controllerType) {
//            QMGoodsSimpleInfoV2Cell * cell = [tableView dequeueReusableCellWithIdentifier:kQMGoodsSimpleInfoV2CellIdentifier];
//            if(nil == cell) {
//                cell = [QMGoodsSimpleInfoV2Cell cellForGoodsSimpleInfo];
//            }
//            cell.delegate = self;
//            
//            GoodsSimpleInfo * info = [_dataArray objectAtIndex:indexPath.section];
//            [cell updateUI:_controllerType goodsInfo:info];
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            return cell;
            GoodsSimpleInfo * info = [_dataArray objectAtIndex:indexPath.section];
            if (indexPath.row == 0) {
                SellingGoodsTimeCell *timeCell = [tableView dequeueReusableCellWithIdentifier:kSellingGoodsTimeCellIdentifier];
                [timeCell updateUI:info];
                return timeCell;
            }
            else if (indexPath.row == 1){
                SellingGoodsDescCell *descCell = [tableView dequeueReusableCellWithIdentifier:kSellingGoodsDescCellIdentifier];
                [descCell updateUI:info];
                return descCell;
            }
            else{
                SellingGoodsControlCell *controlCell = [tableView dequeueReusableCellWithIdentifier:kSellingGoodsControlCellIdentifier];
                controlCell.delegate = self;
                [controlCell updateUI:info];
                return controlCell;
            }
        }
        //已买的/已卖的
        else {
            OrderSimpleInfo * info = [_dataArray objectAtIndex:indexPath.section];
            if (indexPath.row == 0) {
                OrderTimeCell *timeCell = [tableView dequeueReusableCellWithIdentifier:kOrderTimeCellIdentifier];
                [timeCell updateUI:info];
                return timeCell;
            }
            else if (indexPath.row == 1){
                OrderGoodsDescCell *descCell = [tableView dequeueReusableCellWithIdentifier:kOrderGoodsDescCellIdentifier];
                [descCell updateUI:info cellType:_controllerType];
                return descCell;
            }
            else if (indexPath.row == 2){
                OrderGoodsPriceCell *priceCell = [tableView dequeueReusableCellWithIdentifier:kOrderGoodsPriceCellIdentifier];
                [priceCell updateUI:info];
                return priceCell;
            }
            else{
                OrderStatusCell *statusCell = [tableView dequeueReusableCellWithIdentifier:kOrderStatusCellIdentifier];
                statusCell.delegate = self;
                [statusCell updateUI:info cellType:_controllerType];
                return statusCell;
            }
        }
    }
}

#pragma mark - UITableViewDeletegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(![self isFirstPage]) {
        if(_controllerType == ECT_ORDER_MGR || _controllerType == ECT_BUYED_GOODS) {
            BOOL bSeller = _controllerType == ECT_ORDER_MGR ? YES : NO;
            OrderSimpleInfo * info = [_dataArray objectAtIndex:indexPath.section];
            QMOrderDetailViewController * viewController = [[QMOrderDetailViewController alloc] initWithData:bSeller orderId:info.order_id];
            [self.navigationController pushViewController:viewController animated:YES];
        } else if (_controllerType == ECT_GOODS_MGR) {
            GoodsSimpleInfo * info = [_dataArray objectAtIndex:indexPath.section];
            GoodsDetailViewController * viewController = [[GoodsDetailViewController alloc] initWithGoodsId:info.gid];
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self isFirstPage]) {
        return _tableView.bounds.size.height - kMainTopHeight;
    } else {
        if (ECT_GOODS_MGR == _controllerType) {
            if (indexPath.row == 0) {
                return [SellingGoodsTimeCell cellHeight];
            }
            else if (indexPath.row == 1){
                return [SellingGoodsDescCell cellHeight];
            }
            else{
                return [SellingGoodsControlCell cellHeight];
            }
        }
        else{
            if (indexPath.row == 0) {
                return [OrderTimeCell cellHeight];
            }
            else if (indexPath.row == 1){
                return [OrderGoodsDescCell cellHeight];
            }
            else if (indexPath.row == 2){
                return [OrderGoodsPriceCell cellHeight];
            }
            else{
                return [OrderStatusCell cellHeight];
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if([self isFirstPage]) {
        return CGFLOAT_MIN;
    }
    return 14;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex == buttonIndex) {
        return;
    }
    if (alertView.tag == 1000 && _deleteGoodsId) {
        //删除该商品
        [AppUtils showProgressMessage:@"正在提交"];
        [[GoodsHandler shareGoodsHandler]deleteGoodsWithId:_deleteGoodsId success:^(id obj) {
            [AppUtils dismissHUD];
            
            //删除数据源
            id data = [_dataDict objectForKey:[NSString stringWithFormat:@"%qu", _deleteGoodsId]];
            NSUInteger section = [_dataArray indexOfObject:data];
            NSIndexSet * deleteSet = [NSIndexSet indexSetWithIndex:section];
            [_dataArray removeObject:data];
            [_dataDict removeObjectForKey:[NSString stringWithFormat:@"%qu", _deleteGoodsId]];
            
            //删除该行
            if([self isFirstPage]) {
                [_tableView reloadSections:deleteSet withRowAnimation:UITableViewRowAnimationFade];
            } else {
                [_tableView deleteSections:deleteSet withRowAnimation:UITableViewRowAnimationNone];
            }
            [_myOrderList deleteGoods:_deleteGoodsId];
            
        } failed:^(id obj) {
            [AppUtils showErrorMessage:(NSString *)obj];
        }];
    } else if(alertView.tag == 1001 && _simpleGoodsInfo) {
        
        BOOL bdrop = NO;
        if(_simpleGoodsInfo.status == 1) {
            bdrop = YES;
        }
        
        __weak UITableView * weakTableView = self.tableView;
        __weak NSArray * weakDataArray = _dataArray;
        __weak GoodsSimpleInfo * weakSimpleGoodsInfo = _simpleGoodsInfo;
        
        [[GoodsHandler shareGoodsHandler] dropGoodsWithId:_simpleGoodsInfo bdrop:bdrop success:^(id obj) {
            GoodsSimpleInfo * strongSimpleGoodsInfo = (GoodsSimpleInfo *)weakSimpleGoodsInfo;
            
            NSUInteger section = [weakDataArray indexOfObject:strongSimpleGoodsInfo];
            if(NSNotFound != section) {
                NSIndexPath * indexPath = [NSIndexPath indexPathForRow:2 inSection:section];
                [weakTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
            strongSimpleGoodsInfo = nil;
            if(bdrop) {
                [AppUtils showSuccessMessage:@"下架完成"];
            } else {
                [AppUtils showSuccessMessage:@"上架完成"];
            }
        } failed:^(id obj) {
            [AppUtils showErrorMessage:(NSString *)obj];
        }];
    }else if (alertView.tag==1002){
        if(buttonIndex==1){//马上好评
            NSString *strURL=@"https://itunes.apple.com/cn/app/si-huo-hao-you-jian-xian-zhi/id955693333?l=en&mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strURL]];
        }else if (buttonIndex==2){//立即吐槽
            ChatViewController *chatController  = [[ChatViewController alloc] initWithChatter:@"10000"];
            [self.navigationController pushViewController:chatController animated:YES];
        }
    }
}

#pragma mark - Pull Refresh

/**
 *  集成刷新控件
 */
- (void)setupRefresh
{
    __weak typeof(self) weakSelf = self;
    //加载更多
    [self.tableView addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRereshing];
    }];
    self.tableView.footer.automaticallyRefresh = YES;
    //设置下拉刷新
    [self.tableView addPullToRefreshActionHandler:^{
        [weakSelf headerRereshing];
    }];

    [self.tableView triggerPullToRefresh];
}

- (void)headerRereshing
{
    if (!_myOrderList.loading) {
        [_myOrderList loadNextPage:YES];
    }
}

- (void)footerRereshing
{
    if (_dataArray &&!_myOrderList.loading && !_myOrderList.endOfAll) {
        [_myOrderList loadNextPage:NO];
    }
}

#pragma mark - OrderStatusCellDelegate
- (void)didTapButton:(NSString *)orderId
{
    __block OrderSimpleInfo * orderInfo =  [_dataDict objectForKey:orderId];
    if (_controllerType == ECT_BUYED_GOODS) { //已买到的
        if (orderInfo.order_status == 0) {
            [AppUtils trackCustomEvent:@"click_payButton_event" args:nil];
            //待付款
            QMPayOrderViewController   *payOrder=[[QMPayOrderViewController alloc] initWithNibName:@"QMPayOrderViewController" bundle:nil];
            [payOrder setOrderId:orderId totalPay:orderInfo.payAmmount];
            [self.navigationController pushViewController:payOrder animated:YES];
        }
        else if(orderInfo.order_status == 3){
            //确认收货
            QMConfirmReceivingGoodsViewController * viewController = [[QMConfirmReceivingGoodsViewController alloc] initWithOrderId:orderInfo.order_id];
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
    else if (_controllerType == ECT_ORDER_MGR){ //已卖出的
        //发货
        QMSelectExpressViewController * viewController = [[QMSelectExpressViewController alloc] initWithOrderId:orderInfo.order_id];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

//查看物流
- (void)didTapCheckExpress:(NSString *)orderId
{
    OrderSimpleInfo * info =  [_dataDict objectForKey:orderId];

//    DLog(@"info:%@", info);
//    NSDictionary * dict = [_expressDict objectForKey:info.express_name];
//    DLog(@"dict:%@", dict);
//    NSString * code = [dict objectForKey:@"code"];
    if([info.express_co length] > 0 && [info.express_num length] > 0) {
        QMCheckExpressViewController * viewController = [[QMCheckExpressViewController alloc] initWithExpressInfo:info.express_co expressNum:info.express_num orderId:info.order_id];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else{
        [AppUtils showAlertMessage:[NSString stringWithFormat:[AppUtils localizedPersonString:@"QM_Alert_CheckExpressInfo_Error"], info.express_co, info.express_num]];
    }
}

#pragma mark - SellingGoodsControlCellDelegate
- (void)didTapDeleteBtn:(GoodsSimpleInfo *)goodsInfo
{
    _deleteGoodsId = goodsInfo.gid;
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"确定要删除么？" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag = 1000;
    [alertView show];
}

- (void)didTapDropBtn:(GoodsSimpleInfo *)goodsInfo
{
    if(goodsInfo.status == -1) {
        return;
    }
    
    BOOL bdrop = NO;
    if(goodsInfo.status == 1) {
        bdrop = YES;
    }
    
    _simpleGoodsInfo = goodsInfo;
    
    NSString * tips = [NSString stringWithFormat:@"您确定要%@此商品吗?", bdrop ? @"下架" : @"上架"];
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:tips message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag = 1001;
    [alertView show];
}

- (void)didTapEditBtn:(GoodsSimpleInfo *)goodsInfo
{
    GoodsSimpleInfo * info = [_dataDict objectForKey:[NSString stringWithFormat:@"%qu", goodsInfo.gid]];
    if(info) {
        
        __weak UITableView * weakTableView = self.tableView;
        
        __block NSIndexSet * indexSet = nil;
        NSUInteger index = [_dataArray indexOfObject:goodsInfo];
        if(NSNotFound != index) {
            indexSet = [NSIndexSet indexSetWithIndex:index];
        }
        
        EditGoodsPage1ViewController *controller = [[EditGoodsPage1ViewController alloc]initWithType:QMUpdateGoods];
        controller.goodsId = info.gid;
        controller.updateGoodsBlock = ^(GoodEntity *entity){
            //刷新该条商品信息
            
            //数据源
            info.desc = entity.desc;
            info.price = entity.price;
            info.orig_price = entity.orig_price;
            info.num = entity.number;
            info.photo = [entity.picturelist.rawArray objectAtIndex:0];
            
            //            //DB
            //            [weakOrderList updateSimpleGoodsInfo:info];
            
            //UI
            if(indexSet) {
                [weakTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
            }
        };
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - notification
- (void)processUpdateOrderPriceNtf:(NSNotification *)notification
{
    if(_controllerType == ECT_ORDER_MGR || _controllerType == ECT_BUYED_GOODS) {
        NSString * orderId = [notification.userInfo objectForKey:@"orderId"];
        NSNumber * price = [notification.userInfo objectForKey:@"finalPrice"];
        OrderSimpleInfo * info =  [_dataDict objectForKey:orderId];
        
        if(orderId && price && info) {
            info.payAmmount = [price floatValue];
            
            //刷新UI
            NSUInteger section = [_dataArray indexOfObject:info];
            [_tableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
        }
    }

}

- (void)processConfirmDeliverGoodsNty:(NSNotification *)notification
{
    
}

- (void)processMyCacheListLoadedNotify:(NSNotification *)notification
{
    [self reloadMyCacheList];
}

- (void)processMyCacheListLoadStateNotify:(NSNotification *)notification
{
     [self refreshStateViewUI];
}

- (void)processUpdateCacheNotify:(NSNotification *)notification
{
    id temp = [notification.userInfo objectForKey:@"type"];
    if(temp && [temp unsignedIntValue] == _controllerType) {
        temp = [notification.userInfo objectForKey:@"info"];
        if(temp && [temp isKindOfClass:[NSDictionary class]]) {
            id data = [_dataDict objectForKey:[temp objectForKey:@"id"]];
            if(data) {
                 NSUInteger section = [_dataArray indexOfObject:data];
                
                //更新数据源
                GoodsSimpleInfo * info = [[GoodsSimpleInfo alloc] initWithDictionary:temp];
                [data assignValue:info];
                
                //刷新UI
                [_tableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
            }
        }
    }
}

- (void)processUpdateOrderStatusNtf:(NSNotification *)notification
{
    if(_controllerType == ECT_ORDER_MGR || _controllerType == ECT_BUYED_GOODS) {
        NSString * orderId = [notification.userInfo objectForKey:@"orderId"];
        NSNumber * status = [notification.userInfo objectForKey:@"status"];
        OrderSimpleInfo * info =  [_dataDict objectForKey:orderId];
        
        if(orderId && status && info) {
            info.order_status = [status intValue];
            
            //刷新UI
            NSUInteger section = [_dataArray indexOfObject:info];
            [_tableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

- (void)processNetworkErrorNotify:(NSNotification *)notification
{
    if ([_tableView showPullToRefresh]) {
        [_tableView stopRefreshAnimation];
    }
}

-(void)processShowComments:(NSNotification *)notification{
    if (![UserDefaultsUtils boolValueWithKey:IS_GIVE_COMMENTS]) {
        [UserDefaultsUtils saveBoolValue:YES withKey:IS_GIVE_COMMENTS];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"您的第一笔交易已经完成啦!求赏个五星好评吧，咱给程序员买点肉。" delegate:self cancelButtonTitle:@"残忍拒绝" otherButtonTitles:@"马上好评",@"我要吐槽", nil];
        alert.tag=1002;
        [alert show];
    }
}

@end
