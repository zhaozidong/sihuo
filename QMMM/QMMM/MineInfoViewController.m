//
//  MineInfoViewController.m
//  QMMM
//
//  Created by kingnet  on 14-9-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MineInfoViewController.h"
#import "MineInfoCellControl.h"
#import "MinePhotoCell.h"
#import "UserDao.h"
#import "UserInfoHandler.h"
#import "FinanceDataInfo.h"
#import "FinanceHandler.h"
#import "ChangePwdViewController.h"
#import "MyAddressViewController.h"
#import "TGCameraNavigationController.h"
#import "UploadImg.h"
#import "UIImage+IF.h"
#import "EditInfoViewController.h"

@interface MineInfoViewController ()<UITableViewDataSource, UITableViewDelegate, MinePhotoCellDelegate,TGCameraDelegate, QMRoundHeadViewDelegate>
{
    UploadImg *uploadImg_;
}
@property (nonatomic) UITableView *tableView;
@property (nonatomic, strong) MineInfoCellControl *infoControl;
@property (nonatomic) MinePhotoCell *photoCell;

@end

NSString * const kFilePath = @"kFilePath";
NSString * const kImageKey = @"kImageKey";

@implementation MineInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"我的资料";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];

    self.infoControl = [[MineInfoCellControl alloc] init];
    self.infoControl.delegate = self;
    
    [self.view addSubview:self.tableView];
    
    //注册通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userInfoUpdate:) name:kUserInfoUpdateNotify object:nil];
    
    //刷新顶部生活照
    UserEntity *user = [UserInfoHandler sharedInstance].currentUser;
    [self.photoCell updateUI:user];
    //刷新中间的信息
    [self.infoControl updateUI:user];
    
    //获取支付宝帐号信息
    [self getFinanceInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    //判断是返回还是push进了其他的页面
    UIViewController *controller = [self.navigationController.viewControllers lastObject];
    if (controller == nil) {
        NSArray *array = [self.photoCell keysArray];
        if (array) {
            //上传生活照
            [[UserInfoHandler sharedInstance]uploadLifePhotos:array success:nil failed:nil];
        }
    }
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)getFinanceInfo
{
    [[FinanceHandler sharedInstance]getFinanceInfo:nil failed:nil];
}

#pragma mark - Notification Handler
- (void)userInfoUpdate:(NSNotification *)notification
{
    UserEntity *user = [UserInfoHandler sharedInstance].currentUser;
    //刷新中间的信息
    [self.infoControl updateUI:user];
}

#pragma mark - UIView
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
    }
    return _tableView;
}

- (MinePhotoCell *)photoCell
{
    if (!_photoCell) {
        _photoCell = [MinePhotoCell minePhotoCell];
        _photoCell.delegate = self;
        _photoCell.photoType = QMEditMinePhoto;
    }
    return _photoCell;
}

- (void)addPersonPhoto:(NSArray *)photos
{
    NSMutableArray *array = [NSMutableArray arrayWithArray:photos];
    [self.photoCell addPhotos:array];
}

- (void)setUserAvarta:(UIImage *)img
{
    [self.infoControl updateUserAvarta:img];
}

- (void)uploadAvarta
{
    if (!uploadImg_) {
        uploadImg_ = [[UploadImg alloc]init];
    }
    
    __block MineInfoViewController *blockSelf = self;
    MineInfoViewController __weak *weakSelf = blockSelf;
    
    [uploadImg_ uploadImg:self uploadBlock:^(UIImage *img) {
        [weakSelf setUserAvarta:img];
        //上传
        NSData *data = UIImageJPEGRepresentation(img, 0.9);
        
        [[UserInfoHandler sharedInstance] uploadAvatarData:data success:^(id obj) {
            NSString *key = (NSString *)obj;
            UserEntity *curUser = [UserInfoHandler sharedInstance].currentUser;
            UserEntity *entity = [[UserEntity alloc]initWithUserEntity:curUser];
            entity.avatar = key;
            [[UserInfoHandler sharedInstance]updateUserInfo:entity success:^(id obj) {
                
            } failed:^(id obj) {
                [AppUtils showErrorMessage:(NSString *)obj];
            }];
        } failed:^(id obj) {
            [AppUtils showErrorMessage:(NSString *)obj];
        }];
    }];
}

#pragma mark - QMHeadViewDelegate
- (void)clickHeadViewWithUid:(uint64_t)userId
{
    [self uploadAvarta];
}

#pragma mark - MinePhotoCellDelegate
- (void)didTapAddPhoto
{
    TGCameraNavigationController *navigationController = [TGCameraNavigationController newWithCameraDelegate:self];
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark -
#pragma mark - TGCameraDelegate

- (void)cameraDidTakePhoto:(UIImage *)image
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraWillTakePhoto
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (int)maxNumberOfPhotos
{
    return kPersonPhotoLimitNum - (int)self.photoCell.photoArray.count;
}

- (void)cameraDidOKWithPhotos:(NSArray *)photoArr
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self addPersonPhoto:photoArr];
}

#pragma mark - UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return 5;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [MinePhotoCell rowHeight];
    }
    return [MineInfoCellControl rowHeightWithIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return self.photoCell;
    }
    if (indexPath.section == 1) {
        return self.infoControl.cellsArray[indexPath.row];
    }
    else if (indexPath.section == 2){
        return self.infoControl.cellsArray[5];
    }
    else if (indexPath.section == 3){
        return self.infoControl.cellsArray[6];
    }
    return nil;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            //上传头像
            [self uploadAvarta];
        }
        else if (indexPath.row == 3 || indexPath.row == 4){
            //修改昵称
            EditInfoType type;
            if (indexPath.row == 3) {
                type = QMEditNickname;
            }
            else{
                type = QMEditEmail;
            }
            EditInfoViewController *controller = [[EditInfoViewController alloc]initWithType:type];
            UserEntity *user = [UserInfoHandler sharedInstance].currentUser;
            if (indexPath.row == 3) {
                controller.text = user.nickname;
            }
            else{
                controller.text = user.email;
            }
            
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:controller];
            [self presentViewController:navController animated:YES completion:nil];
        }
    }
    //修改密码
    else if (indexPath.section == 2) {
        ChangePwdViewController *controller = [[ChangePwdViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    //地址管理
    else if (indexPath.section == 3) {
        MyAddressViewController *controller = [[MyAddressViewController alloc]init];
        controller.needSelected = NO;
        controller.initType=AddressInitTypeMine;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 14.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
