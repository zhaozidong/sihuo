//
//  OrderDataInfo.h
//  QMMM
//
//  Created by hanlu on 14-9-22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseEntity.h"

extern const NSInteger kRefundWaitDays;

typedef enum {
    QMOrderStatusRefundClosed = -3, //因退款而关闭订单
    QMOrderStatusTimeout = -2, //订单超时被关闭
    QMOrderStatusClosed = -1,  //交易关闭
    QMOrderStatusWaitPay = 0,  //待付款
    QMOrderStatusPaid = 2,     //已付款
    QMOrderStatusDelivery = 3, //已发货
    QMOrderStatusFinished = 4, //交易完成
    QMOrderStatusRefundApply = 10,    //申请退款中
    QMOrderStatusRefundAccept = 11,   //接受退款申请
    QMOrderStatusRefundReject = 12    //拒绝退款申请
} QMOrderStatus;

@class FMResultSet;

//订单信息
@interface OrderDataInfo : BaseEntity

//订单ID
@property (nonatomic, copy) NSString * orderId;

//商品ID
@property (nonatomic, assign) uint64_t goodsId;

//商品描述
@property (nonatomic, copy) NSString * goodsDesc;

//商品图片
@property (nonatomic, copy) NSString * goodsImage;

//余额支付金额
@property (nonatomic, assign) float balancePay;

//支付宝支付金额
@property (nonatomic, assign) float aliPay;

//支付总金额
@property (nonatomic, assign) float payAmount;

//单价
@property (nonatomic, assign) float goodsPrice;

//购买数量
@property (nonatomic, assign) uint buy_num;

//支付状态
@property (nonatomic, assign) int paymentStatus;

//订单状态
@property (nonatomic, assign) QMOrderStatus order_status;

//关闭订单时停留的位置
@property (nonatomic, assign) QMOrderStatus last_status;

//买家下单时间
@property (nonatomic, assign) int order_time;

//卖家发货时间
@property (nonatomic, assign) int express_time;

//用户ID:买家/卖家
@property (nonatomic, assign) uint64_t user_Id;

//用户昵称:买家/卖家
@property (nonatomic, copy) NSString * user_nick;

//用户姓名:卖家/卖家
@property (nonatomic, copy) NSString * user_name;

//收件人
@property (nonatomic, copy) NSString * recipients;

//收件人联系电话
@property (nonatomic, copy) NSString * recipients_phone;

//收件人联系地址
//@property (nonatomic, strong) NSString * recipients_city;
@property (nonatomic, copy) NSString * recipients_address;

//快递公司名称
@property (nonatomic, copy) NSString * express_name;

//快递编号
@property (nonatomic, copy) NSString * express_num;

//快递公司编号
@property (nonatomic, copy) NSString * express_co;

//评论内容
@property (nonatomic, copy) NSString *comments;

//评论类型 1差评 2中评 3好评
@property (nonatomic, assign) int rate;

//评分时间
@property (nonatomic, assign) int rate_ts;

//运费
@property (nonatomic, assign) float freight;

//系统时间
@property (nonatomic, assign) int sys_ts;

//拒绝理由
@property (nonatomic, copy) NSString * rejectReason;

//卖家/买家电话
@property (nonatomic, copy) NSString * mobile;

//退款金额
@property (nonatomic, assign) float refund_amount;

//申请退款的时间
@property (nonatomic, assign) int apply_ts;


- (id)initWithDictionary:(NSDictionary *)dict;

@end


//简要订单信息
@interface OrderSimpleInfo : BaseEntity

//订单Id
@property (nonatomic, copy) NSString * order_id;

//订单时间
@property (nonatomic, assign) int order_pub_time;

//商品Id
@property (nonatomic, assign) uint64_t goods_id;

//商品描述
@property (nonatomic, strong) NSString * goods_desc;

//商品图片
@property (nonatomic, strong) NSString * goods_image;

//商品价格
@property (nonatomic, assign) CGFloat goods_price;

//购买数量
@property (nonatomic, assign) int goods_num;

//支付总价
@property (nonatomic, assign) CGFloat payAmmount;

//收货人信息
@property (nonatomic, strong) NSString * recipients;
@property (nonatomic, strong) NSString * cellPhone;
@property (nonatomic, strong) NSString * recipients_address;

//快递信息
@property (nonatomic, strong) NSString * express_name;
@property (nonatomic, strong) NSString * express_num;
@property (nonatomic, strong) NSString * express_co; //快递公司编号

//订单状态
//-1交易关闭，0待付款，2已付款，3确认收货, 4已买到 //买家
//-1交易关闭，0待付款，2已付款待发货，3已发货， 4已售出   //卖家
@property (nonatomic, assign) QMOrderStatus order_status;

//支付状态
@property (nonatomic, assign) int payment_status;

@property (nonatomic, assign) float freight;

- (id)initWithDictionary:(NSDictionary *)dict;

- (id)initWithOrderSimpleInfo:(OrderSimpleInfo *)info;

- (NSString *)toJSONString;

@end

