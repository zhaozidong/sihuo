//
//  ReportHandler.h
//  QMMM
//
//  Created by Derek on 15/3/10.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseHandler.h"
#import "ReportInfo.h"

@interface ReportHandler : BaseHandler

+(ReportHandler *)sharedInstance;

-(void)reportByInfo:(ReportInfo *)info success:(SuccessBlock)successBlock failure:(FailedBlock)failureBlock;


@end
