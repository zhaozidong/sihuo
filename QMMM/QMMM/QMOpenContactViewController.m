//
//  QMOpenContactViewController.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMOpenContactViewController.h"

@interface QMOpenContactViewController ()

@end

@implementation QMOpenContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"开启权限";
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout =UIRectEdgeNone;
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    UIImageView *imageView=[[UIImageView alloc] init];
    imageView.frame=CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
    imageView.image=[UIImage imageNamed:@"openContact"];
    [self.view addSubview:imageView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

//-(void)viewWillDisappear:(BOOL)animated{
//    self.navigationController.navigationBarHidden = YES;
//    [super viewWillDisappear:animated];
//}

- (void)backAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
