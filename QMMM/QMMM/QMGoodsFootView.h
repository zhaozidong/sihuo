//
//  QMGoodsFootView.h
//  QMMM
//
//  Created by hanlu on 14-9-17.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMGoodsFootView : UIView

@property (nonatomic, strong) IBOutlet UILabel * textLabel;

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView * indicatorView;

@end
