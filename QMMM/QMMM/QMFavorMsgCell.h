//
//  QMFavorMsgCell.h
//  QMMM
//
//  Created by hanlu on 14-10-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMFavorMsgCellIdentifier;

@class MsgData;

@interface QMFavorMsgCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * nameLabel;

@property (nonatomic, strong) IBOutlet UILabel * timeLabel;

@property (nonatomic, strong) IBOutlet UILabel * favorLabel;

@property (nonatomic, strong) IBOutlet UILabel * descLabel;

@property (nonatomic, strong) IBOutlet UIImageView * goodsImageView;

@property (nonatomic, strong) IBOutlet UIImageView * flagImageView;

+ (id)cellForFavorMsg;

- (void)updateUI:(MsgData *)data;

@end
