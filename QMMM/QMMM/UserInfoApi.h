//
//  UserInfoApi.h
//  QMMM
//
//  Created by kingnet  on 14-9-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseHandler.h"

@interface UserInfoApi : BaseHandler

/**
 上传生活照的二进制数据
 **/
- (void)uploadLifePhotoData:(NSData *)imgData uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))uploadProgressBlock context:(contextBlock)context;

/**
 上传头像的二进制数据
 **/
- (void)uploadAvatarData:(NSData *)imgData context:(contextBlock)context;

/**
 上传生活照的key
 **/
- (void)uploadLifePhotos:(NSArray *)photo
                 context:(contextBlock)context;


/**
 更新用户信息
 **/
- (void)updateUserAvatar:(NSString *)avatar
                nickName:(NSString *)nickName
                   email:(NSString *)email
                   motto:(NSString *)motto
                location:(NSString *)location
                 context:(contextBlock)context;

/**
 上传好友通讯录
 **/
- (void)uploadContacts:(NSDictionary *)contacts
               context:(contextBlock)context;

/**
 登录
 **/
- (void)executeLoginWithUserName:(NSString *)userName
                        password:(NSString *)password
                         context:(contextBlock)context;

/**
 注册
 **/
- (void)executeRegisterWithMobile:(NSString *)mobile
                         password:(NSString *)password
                        checkCode:(NSString *)checkCode
                           avatar:(NSString *)avatar
                         nickname:(NSString *)nickname
                          context:(contextBlock)context;

/**
 修改密码
 **/
- (void)executeChangePwdWithPwd:(NSString *)pwd
                      oldPwd:(NSString *)oldPwd
                         mobile:(NSString *)mobile
                        context:(contextBlock)context;

/**
 找回密码
 **/
- (void)executeFindPwdWithNewPwd:(NSString *)pwd
                       checkCode:(NSString *)checkCode
                        phoneNum:(NSString *)phoneNum
                         context:(contextBlock)context;

/**
 注册设备
 **/
- (void)registerDeviceToken:(NSString *)deviceToken context:(contextBlock)context;

/**
 获取用户基本信息
 **/
- (void)getUserBasicInfo:(NSArray *)userList context:(contextBlock)context;

/**
 获取待发货数和待确认收货数
 **/
- (void)getOrderStateNum:(contextBlock)context;

/**
 退出登录
 **/
- (void)userLogout:(contextBlock)context;

/**
 校验验证码
 **/
- (void)smsVerify:(NSString *)type mobile:(NSString *)mobile captcha:(NSString *)captcha context:(contextBlock)context;

/**
 获取新手红包
 **/
- (void)newerHongBao:(contextBlock)context;


@end
