//
//  GoodsCategoryViewController.m
//  QMMM
//
//  Created by kingnet  on 15-1-14.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "GoodsCategoryViewController.h"
#import "GoodsCategoryCell.h"
#import "GoodsHandler.h"
#import "QMGoodsCateInfo.h"

@interface GoodsCategoryViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *cateList;

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation GoodsCategoryViewController

- (id)initWithBlock:(CategoryBlock)block
{
    self = [super init];
    if (self) {
        self.categoryBlock = [block copy];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"分类";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    [self.view addSubview:self.tableView];
    
    self.cateList = [NSMutableArray array];
    
    [self getGoodsCategory];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)getGoodsCategory
{
    [AppUtils showProgressMessage:@"加载中"];
    __weak typeof(self) weakSelf = self;
    [[GoodsHandler shareGoodsHandler]getCategory:^(id obj) {
        [AppUtils dismissHUD];
        
        NSArray *arr = (NSArray *)obj;
        [weakSelf.cateList removeAllObjects];
        [weakSelf.cateList addObjectsFromArray:arr];
        [weakSelf.tableView reloadData];
        
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
        [_tableView registerNib:[GoodsCategoryCell nib] forCellReuseIdentifier:kGoodsCategoryCellIdentifier];
        _tableView.rowHeight = [GoodsCategoryCell cellHeight];
    }
    return _tableView;
}

#pragma mark - UITableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cateList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GoodsCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:kGoodsCategoryCellIdentifier];
    QMGoodsCateInfo *cateInfo = [self.cateList objectAtIndex:indexPath.row];
    [cell updateUI:cateInfo];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    QMGoodsCateInfo *cateInfo = [self.cateList objectAtIndex:indexPath.row];
    if (self.categoryBlock) {
        self.categoryBlock((int)cateInfo.cateId, cateInfo.name);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
