//
//  IntroduceViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "IntroduceViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "AnimationPageView.h"
#import "UserDefaultsUtils.h"
#import "QMIntroDetailViewController.h"

@interface IntroduceViewController ()<UIScrollViewDelegate>
{
    BOOL isLoaded_;
    BOOL isFirstLoad;
}
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) AnimationPageView *firstPageView; //第一页带动画的

@property (nonatomic, strong) UIButton *loginBtn; //登录按钮

@property (nonatomic, strong) UIButton *registerBtn; //注册按钮

@property (nonatomic, strong) UIPageControl *pageControl; //指示点点

@property (nonatomic, strong) UIButton *btnStart;//开始按钮

@end

static const int pageNum = 5;

@implementation IntroduceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = UIColorFromRGB(0xf6f4f9);
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    isLoaded_ = NO;
    isFirstLoad=[UserDefaultsUtils boolValueWithKey:@"firstLoad"];
    
    [self setupViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //是第一次才执行动画
    [self.firstPageView startAnimate];
    if (isFirstLoad) {
        if (!isLoaded_) {
            isLoaded_ = YES;
            [self startAnimation];
        }
    }
}

- (void)setupViews
{
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    _scrollView.pagingEnabled = YES;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.frame)*pageNum, CGRectGetHeight(self.view.frame));
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.delegate = self;
    [self.view addSubview:_scrollView];
    
    _firstPageView = [AnimationPageView animationView];
    _firstPageView.frame = CGRectMake(0, 0, CGRectGetWidth(_scrollView.frame), CGRectGetHeight(_scrollView.frame));
    [_scrollView addSubview:_firstPageView];
    
    _pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake((kMainFrameWidth - 100)/2, kMainFrameHeight-25-35, 100, 30)];
    _pageControl.backgroundColor = [UIColor clearColor];
    _pageControl.pageIndicatorTintColor=UIColorFromRGB(0xdbdce2);
    _pageControl.currentPageIndicatorTintColor=UIColorFromRGB(0x9ba3ae);
    _pageControl.numberOfPages = pageNum;
    [self.view addSubview:_pageControl];
    
//    float w = (kMainFrameWidth-30)/2;
//    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    loginBtn.frame = CGRectMake(10, kMainFrameHeight, w, 45);
    UIImage *image = [UIImage imageNamed:@"submit_btn"];
    image = [image stretchableImageWithLeftCapWidth:7 topCapHeight:7];
//    [loginBtn setBackgroundImage:image forState:UIControlStateNormal];
////    [loginBtn setBackgroundColor:UIColorFromRGB(0xff4544)];
//    
//    [loginBtn setTitle:@"登录" forState:UIControlStateNormal];
//    loginBtn.titleLabel.font = [UIFont systemFontOfSize:18.f];
////    [loginBtn setTitleColor:kDarkTextColor forState:UIControlStateNormal];
//    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    loginBtn.layer.cornerRadius=5.0f;
//    [loginBtn addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
//    self.loginBtn = loginBtn;
//    [self.view addSubview:self.loginBtn];
//    
//    UIButton *registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    registerBtn.frame = CGRectMake(kMainFrameWidth-10-w, kMainFrameHeight, w, 45);
//    [registerBtn setBackgroundImage:image forState:UIControlStateNormal];
////    [registerBtn setBackgroundColor:UIColorFromRGB(0xff4544)];
//    [registerBtn setTitle:@"注册" forState:UIControlStateNormal];
//    registerBtn.titleLabel.font = [UIFont systemFontOfSize:18.f];
//    [registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    registerBtn.layer.cornerRadius=5.0f;
//    [registerBtn addTarget:self action:@selector(registerAction:) forControlEvents:UIControlEventTouchUpInside];
//    self.registerBtn = registerBtn;
//    [self.view addSubview:self.registerBtn];
    
    UIButton *btnStart=[UIButton buttonWithType:UIButtonTypeCustom];
    btnStart.frame=CGRectMake((kMainFrameWidth-190)/2, kMainFrameHeight, 190, 45);
    [btnStart setBackgroundImage:image forState:UIControlStateNormal];
    [btnStart setTitle:@"开始" forState:UIControlStateNormal];
    btnStart.titleLabel.font=[UIFont systemFontOfSize:20.0f];
    btnStart.layer.cornerRadius=5.0f;
    [btnStart addTarget:self action:@selector(btnDidStart:) forControlEvents:UIControlEventTouchUpInside];
    self.btnStart=btnStart;
    [self.view addSubview:btnStart];
    
    //添加引导页
    NSArray *pageArr;
    if(IS_IPHONE_4){
        pageArr = @[@"introSecond_small", @"introThird_small", @"introFourth_small",@"introFive_small"];
    }else{
        pageArr = @[@"introSecondPage", @"introThirdPage", @"introFourthPage",@"introFivePage"];
    }
    
    for (int i=0; i<pageArr.count; i++) {
        UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake((i+1)*CGRectGetWidth(_scrollView.frame), 0, CGRectGetWidth(_scrollView.frame), CGRectGetHeight(_scrollView.frame))];
        imageV.image = [UIImage imageNamed:pageArr[i]];
//        imageV.contentMode = UIViewContentModeScaleAspectFill;
        [_scrollView addSubview:imageV];
    }
}

- (void)loginAction:(id)sender
{
    [AppUtils trackCustomEvent:@"login_event" args:nil];
    LoginViewController *controller = [[LoginViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)registerAction:(id)sender
{
    //点击注册按钮
    [AppUtils trackCustomEvent:@"register_event" args:nil];
    
    RegisterViewController *controller = [[RegisterViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)btnDidStart:(id)sender{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    QMIntroDetailViewController *introDetail=[[QMIntroDetailViewController alloc] init];
    [self.navigationController pushViewController:introDetail animated:YES];
}


- (void)startAnimation
{
//    CGRect loginBtnF = self.loginBtn.frame;
//    loginBtnF.origin.y = kMainFrameHeight-25-CGRectGetHeight(loginBtnF);
//    CGRect registerBtnF = self.registerBtn.frame;
//    registerBtnF.origin.y = kMainFrameHeight-25-CGRectGetHeight(loginBtnF);
    CGRect frmStart=self.btnStart.frame;
    frmStart.origin.y=kMainFrameHeight-25-45;
    
    
    CGRect pageControlF = self.pageControl.frame;
    pageControlF.origin.y = kMainFrameHeight-45-25-CGRectGetHeight(pageControlF)-5;
//    [self.firstPageView startAnimate];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//        weakSelf.loginBtn.frame = loginBtnF;
//        weakSelf.registerBtn.frame = registerBtnF;
        weakSelf.btnStart.frame=frmStart;
        weakSelf.pageControl.frame = pageControlF;
    } completion:^(BOOL finished) {
        [UserDefaultsUtils saveBoolValue:YES withKey:@"firstLoad"];
    }];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    // 设置页码
    _pageControl.currentPage = page;
    if (page==4) {
        [self startAnimation];
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    if (!decelerate && page==4) {
        [self startAnimation];
    }
}


@end
