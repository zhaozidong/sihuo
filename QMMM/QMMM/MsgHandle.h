//
//  MsgHandle.h
//  QMMM
//
//  Created by hanlu on 14-10-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseHandler.h"
#import "QMMsgHelper.h"
#import "MsgData.h"

@class GoodEntity;
@class EMMessage;

@interface MsgHandle : BaseHandler

+ (MsgHandle *)shareMsgHandler;

//加载本地消息
- (void)loadLocalMsgList:(contextBlock)context msgType:(MsgBigType)type;

//请求最新消息列表
- (void)getMsgList;

//获取评论与赞消息的未读数
- (void)getFavorAndCommentUnreadCount:(contextBlock)context;

//更新未读状态
- (void)updateMsgUnreadStatus:(contextBlock)context msgType:(MsgBigType)type unread:(BOOL)unread;

//删除消息
//- (void)deleteSystemMsg:(uint64_t)msgId success:(SuccessBlock)success failed:(FailedBlock)failed;

////检测是否还有系统消息未读
//- (void)checkSystemMsgUnreadCount:(contextBlock)context;

//检测本地是否有未读系统消息
- (void)checkLocalSystemMsgUnreadCount:(contextBlock)context;

//发送商品预览消息
- (void)sendGoodsPreviewMsg:(NSString *)chatter goodsInfo:(GoodEntity*)goodsInfo;

//发送商品消息
- (EMMessage *)sendGoodsMsg:(NSString *)chatter extData:(NSString *)extMessage;

//发送诈骗信息
- (EMMessage *)sendFraudMsg:(NSString *)chatter extData:(NSString *)extMessage;

//获取卖出商品后钱包的钱增加的消息
- (void)getWalletUnreadCount:(contextBlock)context;

@end
