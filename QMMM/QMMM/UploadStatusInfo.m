//
//  UploadStatusInfo.m
//  QMMM
//
//  Created by kingnet  on 15-3-4.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "UploadStatusInfo.h"
#import "UIImage+FixOrientation.h"

@implementation UploadStatusInfo

- (id)initWithFilePath:(NSString *)filePath
{
    self = [super init];
    if (self) {
        _filePath = filePath;
    }
    return self;
}

- (void)uploadImageProgress:(void(^)(float progress))uploading success:(SuccessBlock)success failed:(FailedBlock)failed
{
    self.status = UploadStatusBegan;
    NSData *data = [self getImgData:_filePath];
    if (data) {
        __weak typeof(self) weakSelf = self;
        [[GoodsHandler shareGoodsHandler]uploadGoodsPhotoData:data uploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
            weakSelf.status = UploadStatusUploading;
            if (uploading) {
                uploading((float) totalBytesWritten/totalBytesExpectedToWrite);
            }
        } success:^(id obj) {
            weakSelf.status = UploadStatusSuccess;
            if (success) {
                success(obj);
            }
        } failed:^(id obj) {
            weakSelf.status = UploadStatusFailed;
            if (failed) {
                failed(obj);
            }
        }];
    }
}

- (NSData *)getImgData:(NSString *)filePath
{
    NSData *data = nil;
    NSString *bigS= [filePath stringByReplacingOccurrencesOfString:@"_small" withString:@""];
    NSString *editPic=[filePath stringByReplacingOccurrencesOfString:@"_small" withString:@"_edit"];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    if ([fileMgr fileExistsAtPath:bigS]){
        UIImage *image=[[UIImage imageWithContentsOfFile:bigS] fixOrientation];
        if ([self fileSizeAtPath:bigS]>1) {
            data=UIImageJPEGRepresentation(image, 0.1);
        }else{
            data=UIImageJPEGRepresentation(image, 0.6);
        }
    }else if ([fileMgr fileExistsAtPath:editPic]){
        UIImage *image=[[UIImage imageWithContentsOfFile:editPic] fixOrientation];
        if ([self fileSizeAtPath:editPic]>1) {
            data=UIImageJPEGRepresentation(image, 0.1);
        }else{
            data=UIImageJPEGRepresentation(image, 0.6);
        }
    }
    return data;
}

- (float) fileSizeAtPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize]/(1024.0*1024);
    }
    return 0;
}



@end
