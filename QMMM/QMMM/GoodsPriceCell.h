//
//  GoodsPriceCell.h
//  QMMM
//
//  Created by kingnet  on 15-3-2.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsPriceCell : UITableViewCell

@property (nonatomic, assign) float price;

+ (GoodsPriceCell *)goodsPriceCell;

+ (CGFloat)cellHeight;

@end
