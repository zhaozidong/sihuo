//
//  ReportHandler.m
//  QMMM
//
//  Created by Derek on 15/3/10.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "ReportHandler.h"
#import "ReportApi.h"

@implementation ReportHandler{
    ReportApi *_reportAPI;
}

static ReportHandler *sharedInstance=nil;

- (instancetype)init
{
    if (self = [super init]) {
        _reportAPI = [[ReportApi alloc] init];
    }
    return self;
}

+ (ReportHandler *)sharedInstance
{
    static ReportHandler *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}


-(void)reportByInfo:(ReportInfo *)info success:(SuccessBlock)successBlock failure:(FailedBlock)failureBlock{

    [_reportAPI reportByInfo:info Context:^(id resultDic) {

        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = [resultDic objectForKey:@"d"];
            if (dataDic) {
                if (successBlock) {
                    successBlock(@"");
                }
            }
        }
        else{
            if (failureBlock) {
                failureBlock(resultDic[@"d"]);
            }
        }
    }];

}

@end
