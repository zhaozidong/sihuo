//
//  QMRecommendViewController.h
//  QMMM
//
//  Created by Derek.zhao on 14-12-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMRecommendViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView * tableView;

@end
