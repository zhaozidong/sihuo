//
//  QMGoodsFirstPlaceHolder.h
//  QMMM
//
//  Created by Derek.zhao on 14-12-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
    QMGoodsListPlaceHolderTypeNoGoods=0,
    QMGoodsListPlaceHolderTypeCloseContact,
    QMGoodsListPlaceHolderTypeClostLongTime,
    QMGoodsListPlaceHolderTypeInvite
}QMGoodsListPlaceHolderType;

@protocol QMGoodsFirstPlaceHoderDelegate <NSObject>

-(void)btnDidOpenContact:(UIButton *)sender;

-(void)btnDidInvite;

@end

@interface QMGoodsFirstPlaceHolder : UIView

@property(nonatomic,weak) id<QMGoodsFirstPlaceHoderDelegate> delegate;


-(id)initWithType:(QMGoodsListPlaceHolderType)type andTitle:(NSString *)title;




@end
