//
//  OrderDetailGoodsDescCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class OrderDataInfo;

extern NSString * const kOrderDetailGoodsDescCellIdentifier;

@interface OrderDetailGoodsDescCell : UITableViewCell

+ (UINib *)nib;

+ (CGFloat)cellHeight;

- (void)updateUI:(OrderDataInfo *)orderInfo;

@end
