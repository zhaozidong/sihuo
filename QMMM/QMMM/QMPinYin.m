//
//  QMPinYin.m
//  QMMM
//
//  Created by hanlu on 14-11-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMPinYin.h"
#import "getpinyin.h"

@implementation QMPinYin

+ (NSString *)getPinYin:(NSString *)value hasSymbol:(BOOL)hasSymbol {
    NSString *result = @"";
    if (value) {
        int i;
        NSString *kWhiteSpace = @" ";
        const char *utf8str = NULL;
        NSString *c;
        NSRange r;
        NSArray *a;
        r.length = 1;
        BOOL isNeedAddSpace = NO;
        for (i = 0; i < [value length]; i++) {
            r.location = i;
            c = [value substringWithRange:r];
            // NSLog(@"%d->[%@]", i, c);
            // 注意get_pinyin返回的如果有多音字则会用空格分离多个注音
            // 例如：get_pinyin("大"); 返回：da dai tai
            // 遇到多音字直接返回第一个注音
            utf8str = get_pinyin([c cStringUsingEncoding:NSUTF8StringEncoding]);
            if (utf8str != NULL) {
                c = [NSString stringWithCString:utf8str encoding:NSUTF8StringEncoding];
                a = [c componentsSeparatedByString:kWhiteSpace];
                if (a) {
                    if ([a count] > 0) {
                        c = [a objectAtIndex:0];
                    }// if
                }// if
                if ([result length] > 0) {
                    result = [result stringByAppendingString:kWhiteSpace];
                }// if
                result = [result stringByAppendingString:c];
                isNeedAddSpace = YES;
            } else {
                if (isNeedAddSpace) {
                    isNeedAddSpace = NO;
                    if (isLetterOrNumberChar(getFirstLetter(c))) {// 必须是字符才添加空格，标点符号不加空格
                        result = [result stringByAppendingString:kWhiteSpace];
                    }// if
                }// if
                if (hasSymbol) {
                    // 包含无法转换的字符和符号
                    result = [result stringByAppendingString:c];
                } else {
                    //NSLog(@"%d %d %d", getFirstLetter(@"1"),getFirstLetter(@"9"), getFirstLetter(@"0") );
                    if (isLetterOrNumberChar(getFirstLetter(c))) {
                        // 是字母的则添加
                        result = [result stringByAppendingString:[c lowercaseString]];
                    } else {
                        // 合并多个空格
                        if ([result length] > 0) {
                            r.location = [result length] - 1;
                            NSString *lastStr = [result substringWithRange:r];
                            if ([lastStr isEqualToString:kWhiteSpace] == NO) {
                                result = [result stringByAppendingString:kWhiteSpace];
                            }
                        }// if
                    }// if
                }// if
            }// if
        }// for
    } // if
    return result;
}

BOOL isLetterOrNumberChar(unichar c) {
    // 判断首字符是否在 a-z 97-122 A-Z 65-90的范围内 0-9的范围是48-57
    return ((c >= 97) && (c <= 122)) || ((c >= 65) && (c <= 90)) || ((c >= 48) && (c <=57));
}

unichar getFirstLetter(NSString *str) {
    unichar result = 0;
    if (str && ([str length] > 0)) {
        result = [str characterAtIndex:0];
    }// if
    return result;
}

@end
