//
//  QMGoodsActionCell.m
//  QMMM
//
//  Created by 韩芦 on 14-9-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsActionCell.h"
#import "GoodEntity.h"
#import "UIButton+Addition.h"

NSString * const kQMGoodsActionCellIndentifier = @"kQMGoodsActionCellIndentifier";

@interface QMGoodsActionCell () {
    //GoodEntity * _goodsInfo;
}

@end

@implementation QMGoodsActionCell

+ (id)cellAwakeFromNib
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMGoodsActionCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

+ (NSUInteger)heightForGoodsActionCell:(GoodEntity *)goodsInfo
{
    return 42;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Initialization code
    self.contentView.backgroundColor = [UIColor clearColor];
    
    [_favorBtn addTarget:self action:@selector(favorBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_commentBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_shareBtn addTarget:self action:@selector(shareBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIColor * textColor = UIColorFromRGB(0x99a0aa);
    
    [_favorBtn setTitleColor:textColor forState:UIControlStateNormal];
    [_favorBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    [_favorBtn setButtonType:BT_LeftImageRightTitle interGap:10.0];
    [_favorBtn setBackgroundColor:[UIColor clearColor]];
    
    [_commentBtn setTitleColor:textColor forState:UIControlStateNormal];
    [_commentBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    [_commentBtn setButtonType:BT_LeftImageRightTitle interGap:10.0];
    [_favorBtn setBackgroundColor:[UIColor clearColor]];
   
    [_shareBtn setTitleColor:textColor forState:UIControlStateNormal];
    [_shareBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    [_shareBtn setButtonType:BT_LeftImageRightTitle interGap:10.0];
    [_shareBtn setBackgroundColor:[UIColor clearColor]];
    
    [_lineSeprator setBackgroundColor:UIColorFromRGB(0xe1e1e1)];
    
    _roundBkgView.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(GoodEntity *)goodsInfo showFull:(BOOL)showFull
{
//    _goodsInfo = goodsInfo;
//    
//    NSString * favorCount = @"";
//    NSString * commentCount = @"";
    
//    if(goodsInfo.favorCounts) {
//        favorCount = [NSString stringWithFormat:@"%d", goodsInfo.favorCounts];
//    }
//    
//    if(goodsInfo.commentCounts) {
//        commentCount = [NSString stringWithFormat:@"%d", goodsInfo.commentCounts];
//    }
//    
//    [_favorBtn setTitle:favorCount forState:UIControlStateNormal];
//    if(goodsInfo.isFavor) {
//        [_favorBtn setTitleColor:kRedColor forState:UIControlStateNormal];
//        [_favorBtn setImage:[UIImage imageNamed:@"favor_press"] forState:UIControlStateNormal];
//        [_favorBtn setTitle:[AppUtils localizedProductString:@"QM_Text_favored_Title"] forState:UIControlStateNormal];
//    } else {
//        UIColor * textColor = UIColorFromRGB(0x5d5d5d);
//        [_favorBtn setTitleColor:textColor forState:UIControlStateNormal];
//        [_favorBtn setImage:[UIImage imageNamed:@"favor"] forState:UIControlStateNormal];
//        [_favorBtn setTitle:[AppUtils localizedProductString:@"QM_Text_favor_Title"] forState:UIControlStateNormal];
//    }
//    
//    [_commentBtn setTitle:[AppUtils localizedProductString:@"QM_Text_comment"] forState:UIControlStateNormal];
//    
//    [_shareBtn setTitle:[AppUtils localizedProductString:@"QM_Text_Share"] forState:UIControlStateNormal];
    
    NSString * favorCount = @"";
    NSString * commentCount = @"";
    
    if(goodsInfo.favorCounts) {
        favorCount = [NSString stringWithFormat:@"%d", goodsInfo.favorCounts];
    } else {
        favorCount = [AppUtils localizedProductString:@"QM_Text_favor_Title"];
    }
    
    if(goodsInfo.commentCounts) {
        commentCount = [NSString stringWithFormat:@"%d", goodsInfo.commentCounts];
    } else {
        commentCount = [AppUtils localizedProductString:@"QM_Text_comment"];
    }
    
    [_favorBtn setTitle:favorCount forState:UIControlStateNormal];
    if(goodsInfo.isFavor) {
        [_favorBtn setTitleColor:kRedColor forState:UIControlStateNormal];
        [_favorBtn setImage:[UIImage imageNamed:@"praise_icon_press"] forState:UIControlStateNormal];
    } else {
        UIColor * textColor = UIColorFromRGB(0x99a0aa);
        [_favorBtn setTitleColor:textColor forState:UIControlStateNormal];
        [_favorBtn setImage:[UIImage imageNamed:@"praise_icon"] forState:UIControlStateNormal];
    }
    
    [_commentBtn setTitle:commentCount forState:UIControlStateNormal];
    
    [_shareBtn setTitle:[AppUtils localizedProductString:@"QM_Text_Share"] forState:UIControlStateNormal];
    
    
    if(showFull) {
        _roundBkgView.hidden = YES;
        self.contentView.backgroundColor = [UIColor whiteColor];
    } else {
        _roundBkgView.hidden = NO;
        self.contentView.backgroundColor = [UIColor clearColor];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect bounds = self.contentView.bounds;
    CGFloat width = (bounds.size.width - 3) / 3;
    CGFloat height = bounds.size.height - 1;
    
    UIImage * image = [UIImage imageNamed:@"bottom_half_round_bkg"];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(2, 9, 5, 9)];
    _roundBkgView.frame = bounds;
    _roundBkgView.image = image;
    
    CGRect rect = _favorBtn.frame;
    rect.origin.x = bounds.origin.x;
    rect.origin.y = bounds.origin.y + 1;
    rect.size.width = width;
    rect.size.height = height;
    _favorBtn.frame = rect;
    
    rect = _leftLineSeprator.frame;
    rect.origin.x = _favorBtn.frame.origin.x + _favorBtn.frame.size.width;
    _leftLineSeprator.frame = rect;
    
    rect = _commentBtn.frame;
    rect.origin.x = _leftLineSeprator.frame.origin.x + _leftLineSeprator.frame.size.width;
    rect.origin.y = bounds.origin.y + 1;
    rect.size.width = width;
    rect.size.height = height;
    _commentBtn.frame = rect;
    
    rect = _rightLineSeprator.frame;
    rect.origin.x = _commentBtn.frame.origin.x + _commentBtn.frame.size.width;
    _rightLineSeprator.frame = rect;
    
    rect = _shareBtn.frame;
    rect.origin.x = _rightLineSeprator.frame.origin.x + _rightLineSeprator.frame.size.width;
    rect.origin.y = bounds.origin.y + 1;
    rect.size.width = width;
    rect.size.height = height;
    _shareBtn.frame = rect;
    
    _lineSeprator.frame = CGRectMake(bounds.origin.x, bounds.origin.y, bounds.size.width, 1);
}

#pragma mark - delegate

- (void)favorBtnClick:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(didFavorBtnClicked)]) {
        [_delegate didFavorBtnClicked];
    }
}

- (void)commentBtnClick:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(didCommentBtnClicked)]) {
        [_delegate didCommentBtnClicked];
    }
}

- (void)shareBtnClick:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(didShareBtnClicked)]) {
        [_delegate didShareBtnClicked];
    }
}

@end
