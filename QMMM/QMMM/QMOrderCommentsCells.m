//
//  QMOrderCommentsCells.m
//  QMMM
//
//  Created by kingnet  on 14-11-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMOrderCommentsCells.h"
#import "OrderDataInfo.h"

@interface QMOrderCommentsCells ()
@property (weak, nonatomic) IBOutlet UIImageView *commentsImageV; //评价的标志

@property (weak, nonatomic) IBOutlet UILabel *commentsNameLbl; //好、中、差
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *commentsLbl; //评价内容

@end
@implementation QMOrderCommentsCells

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cellsArr = [[NSBundle mainBundle]loadNibNamed:@"QMOrderCommentsCells" owner:self options:0];
        self.commentsLbl.text = @"";
        self.timeLbl.text = @"";
        self.commentsNameLbl.text = @"";
        self.timeLbl.textColor = kLightBlueColor;
        self.commentsLbl.textColor = kDarkTextColor;
        self.commentsLbl.numberOfLines = 0;
        self.commentsLbl.preferredMaxLayoutWidth = kMainFrameWidth-20;
    }
    return self;
}

- (void)updateUI:(OrderDataInfo *)orderInfo
{
    UIImage *image = nil;
    if (orderInfo.rate == 1) {
        self.commentsNameLbl.text = @"差评";
        self.commentsNameLbl.textColor = UIColorFromRGB(0x75797f);
        image = [UIImage imageNamed:@"comment_bad"];
    }
    else if (orderInfo.rate == 2){
        self.commentsNameLbl.text = @"中评";
        self.commentsNameLbl.textColor = UIColorFromRGB(0xffae00);
        image = [UIImage imageNamed:@"comment_normal"];
    }
    else if (orderInfo.rate == 3){
        self.commentsNameLbl.text = @"好评";
        self.commentsNameLbl.textColor = UIColorFromRGB(0xff3538);
        image = [UIImage imageNamed:@"comment_good"];
    }
    self.commentsImageV.image = image;
    
    self.commentsLbl.text = orderInfo.comments;
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:orderInfo.rate_ts];
    NSString * stringDate = [AppUtils stringFromDate:date];
    self.timeLbl.text = stringDate;
    UITableViewCell *cell = self.cellsArr[1];
    [cell.contentView setNeedsDisplay];
    [cell.contentView layoutIfNeeded];
}

- (CGFloat)commentsCellHeight:(NSString *)str
{
    //设置一个行高上限
    CGSize size = CGSizeMake(kMainFrameWidth-20,CGFLOAT_MAX);
    //计算实际frame大小，并将label的frame变成实际大小
    CGSize size01 = [str sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    return size01.height + 10.f;
}

@end
