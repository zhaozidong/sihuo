//
//  MyWalletCell.h
//  QMMM
//
//  Created by kingnet  on 14-11-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const kMyWalletIdentifier;

@interface MyWalletCell : UITableViewCell

@property (nonatomic, strong) NSString *leftText; //

@property (nonatomic, strong) NSString *iconStr; //

@property (nonatomic, assign) float money;

+ (UINib *)nib;

+ (CGFloat)cellHeight;

@end
