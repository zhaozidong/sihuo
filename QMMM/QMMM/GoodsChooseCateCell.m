//
//  GoodsChooseCateCell.m
//  QMMM
//
//  Created by kingnet  on 15-1-14.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "GoodsChooseCateCell.h"

@interface GoodsChooseCateCell ()
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end
@implementation GoodsChooseCateCell

- (void)awakeFromNib {
    // Initialization code
    self.textField.attributedPlaceholder = [AppUtils placeholderAttri:@"请选择分类"];
    self.textField.textColor = kDarkTextColor;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGesture];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (GoodsChooseCateCell *)chooseCateCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"GoodsChooseCateCell" owner:self options:0];
    return [array lastObject];
}

+ (CGFloat)cellHeight
{
    return 45.f;
}

- (void)setCateName:(NSString *)cateName
{
    self.textField.text = cateName;
}

- (void)handleTap:(UITapGestureRecognizer *)tapGesture
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapChooseCate)]) {
        [self.delegate didTapChooseCate];
    }
}

@end
