//
//  QMFilterView.m
//  QMMM
//
//  Created by kingnet  on 15-3-16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMFilterView.h"
#import "QMFilterItem.h"
#import <POP/POP.h>

NSUInteger const BASE_ITEM_TAG = 1000;

@interface QMFilterView ()
{
    __block BOOL isAnimating_; //是否正在进行动画
}
//下拉界面出现时的半透明黑色背景
@property (nonatomic, strong) UIControl * backgroundControl;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, assign) CGRect originalFrame;

@end
@implementation QMFilterView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGRect f = [[UIScreen mainScreen]bounds];
        self.originalFrame = CGRectMake(0, CGRectGetMaxY(self.frame)-CGRectGetHeight(f)*0.6, CGRectGetWidth(self.frame), CGRectGetHeight(f)*0.6);
        _selectedIndex = -1;
    }
    return self;
}

- (UIView *)containerView
{
    if (!_containerView) {
        _containerView = [[UIView alloc]initWithFrame:self.originalFrame];
        _containerView.backgroundColor = [UIColor whiteColor];
    }
    return _containerView;
}

- (UIControl *)backgroundControl
{
    if (!_backgroundControl) {
        CGRect frame = [[UIScreen mainScreen]bounds];
        _backgroundControl = [[UIControl alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.frame), CGRectGetWidth(self.frame), CGRectGetHeight(frame)-CGRectGetMaxY(self.frame))];
        _backgroundControl.backgroundColor = [UIColor blackColor];
        [_backgroundControl addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backgroundControl;
}

- (void)reload
{
    if (nil == self.dataSource) {
        return;
    }
    //移除掉所有的subview
    [self.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    NSInteger number = [self.dataSource numberOfItemForFilterView:self];
    number = number>5?5:number;
    float w = [[UIScreen mainScreen]bounds].size.width/number;
    for (int i=0; i<number; i++) {
        QMFilterItem *item = [QMFilterItem filterItem];
        item.frame = CGRectMake(w*i, 0, w, CGRectGetHeight(self.frame));
        item.index = i;
        item.title = [self.dataSource filterView:self titleForItem:item];
        item.tag = BASE_ITEM_TAG+i;
        [item addClickTarget:self action:@selector(itemClicked:)];
        [self addSubview:item];
    }
    //添加中间的细线
    for (int i=1; i< number; i++) {
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(i*w, 10, 1, CGRectGetHeight(self.frame)-2*10)];
        line.backgroundColor = kBorderColor;
        [self addSubview:line];
    }
    
    //给底部添加一条分割横线
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-1, CGRectGetWidth(self.frame), 1)];
    bottomLine.backgroundColor = kBorderColor;
    [self addSubview:bottomLine];
}

- (void)didMoveToSuperview
{
    [self reload];
}

- (void)itemClicked:(QMFilterItem *)item
{
    if (isAnimating_) {
        return;
    }
    if (self.selectedIndex == item.index) {
        //收回
        [self dismissWithCompletion:nil];
    }
    else{
        [AppUtils closeKeyboard];
        //展示view
        if (self.dataSource) {
            //如果有其他的item被选择了
            if (-1 != _selectedIndex) {
                [[self seletedItem] rotateArrowUp:NO];
            }
            [self.containerView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [obj removeFromSuperview];
            }];
            self.containerView.frame = self.originalFrame;
            _selectedIndex = item.index;
            [item rotateArrowUp:YES];
            [self showView:[self.dataSource filterView:self viewAtIndex:item.index]];
        }
    }
}

- (void)reloadItemAtIndex:(NSUInteger)index
{
    QMFilterItem *item = (QMFilterItem *)[self viewWithTag:BASE_ITEM_TAG+index];
    item.title = [self.dataSource filterView:self titleForItem:item];
}

- (void)showView:(UIView *)view
{
    isAnimating_ = YES;

    UIView *subView = view;
    CGRect viewFrame = subView.frame;
    
    if (CGRectGetHeight(viewFrame)>CGRectGetHeight(self.originalFrame)) {
        //如果加入的视图比默认设置的要高，则调整container的高度
        CGRect containerFrame = self.containerView.frame;
        containerFrame.size.height = CGRectGetHeight(viewFrame)+CGRectGetHeight(self.frame);
        containerFrame.origin.y = CGRectGetMaxY(self.frame)-containerFrame.size.height;
        self.containerView.frame = containerFrame;
    }
    
    subView.frame = CGRectMake(0, CGRectGetHeight(self.frame), CGRectGetWidth(self.containerView.frame), CGRectGetHeight(self.containerView.frame)-CGRectGetHeight(self.frame));

    [self.containerView addSubview:subView];
    
    self.backgroundControl.alpha = 0.0;
    if (nil == self.backgroundControl.superview) {
        [self.superview insertSubview:self.backgroundControl belowSubview:self];
    }
    if (nil == self.containerView.superview) {
        [self.superview insertSubview:self.containerView belowSubview:self];
    }
    
    //动画出现
    POPBasicAnimation *alphaAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    alphaAnimation.toValue = @(0.7);
    [self.backgroundControl pop_addAnimation:alphaAnimation forKey:@"alphaAnim"];
    
    //此处的kPOPLayerPositionY指的是view的中心点x、y的取值
    POPSpringAnimation *yAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    yAnimation.toValue = @(CGRectGetHeight(self.containerView.frame)/2+CGRectGetMinY(self.frame)); //
    yAnimation.springBounciness = 5;
    yAnimation.springSpeed = 2;
    yAnimation.completionBlock = ^(POPAnimation *anim, BOOL finish){
        isAnimating_ = NO;
    };
    [self.containerView.layer pop_addAnimation:yAnimation forKey:@"yAnim"];
}

- (void)dismissWithCompletion:(void(^)(void))completion
{
    [AppUtils closeKeyboard];
    
    //旋转箭头
    [[self seletedItem] rotateArrowUp:NO];
    
    _selectedIndex = -1;
    
    isAnimating_ = YES;
    
    [self.backgroundControl pop_removeAnimationForKey:@"alphaAnim"];
    [self.containerView.layer pop_removeAnimationForKey:@"yAnim"];
    
    POPBasicAnimation *yAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    yAnimation.toValue = @(CGRectGetHeight(self.containerView.frame)/2+CGRectGetMinY(self.frame)+15);
    yAnimation.completionBlock = ^(POPAnimation *anim, BOOL finish){
        if (finish) {
            POPBasicAnimation *alphaAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
            alphaAnimation.toValue = @(0.0);
            alphaAnimation.completionBlock = ^(POPAnimation *anim, BOOL finish){
                if (finish) {
                    [self.backgroundControl removeFromSuperview];
                }
            };
            [self.backgroundControl pop_addAnimation:alphaAnimation forKey:@"alphaAnim"];
            POPBasicAnimation *yAnimation2 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionY];
            yAnimation2.toValue = @(CGRectGetMaxY(self.frame)-(CGRectGetHeight(self.containerView.frame))/2);
            yAnimation2.completionBlock = ^(POPAnimation *anim, BOOL finish){
                if (finish) {
                    isAnimating_ = NO;
                    [self.containerView removeFromSuperview];
                    if (completion) {
                        completion();
                    }
                }
            };
            [self.containerView.layer pop_addAnimation:yAnimation2 forKey:@"yAnim2"];
        }
    };
    [self.containerView.layer pop_addAnimation:yAnimation forKey:@"yAnim"];
}

- (QMFilterItem *)seletedItem
{
    if (-1 == _selectedIndex) {
        return nil;
    }
    QMFilterItem *item = (QMFilterItem *)[self viewWithTag:BASE_ITEM_TAG+_selectedIndex];
    return item;
}

- (void)dismiss
{
    [self dismissWithCompletion:nil];
}

+ (CGFloat)viewHeight
{
    return 45.f;
}

@end
