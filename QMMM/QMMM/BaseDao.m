//
//  BaseDao.m
//  QMMM
//
//  Created by kingnet  on 14-9-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseDao.h"
#import "FMDatabase.h"

@implementation BaseDao

- (id)initWithTableName:(NSString *)tableName
{
    self = [super init];
    if (self) {
        if (![[QMDatabaseHelper sharedInstance] isTableExists:tableName]) {
            NSDictionary *dic_sql = [[QMDatabaseHelper sharedInstance] sqlForCreateTable];
            NSString *key = [NSString stringWithFormat:@"create%@Table", tableName];
            [self createTable:[dic_sql objectForKey:key]];
        }
    }
    return self;
}

- (void)createTable:(NSString *)sql
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance] openDatabase];
    if (![db executeUpdate:sql]) {
        DLog(@"Create table failed");
    }
    //[db close];
}

@end
