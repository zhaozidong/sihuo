//
//  SystemApi.h
//  QMMM
//
//  Created by kingnet  on 14-10-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseHandler.h"

@interface SystemApi : BaseHandler

- (void)getAppVersion:(contextBlock)context;

- (void)submitFeedback:(NSString *)content
                  type:(int)type
               context:(contextBlock)context;

- (void)reportDevInfoWithContext:(contextBlock)context;

- (void)reportUseTime:(NSString *)time Context:(contextBlock)context;

- (void)reportCurrentPage:(NSString *)page contex:(contextBlock)context;

@end
