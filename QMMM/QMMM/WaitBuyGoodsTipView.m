//
//  WaitBuyGoodsTipView.m
//  QMMM
//
//  Created by kingnet  on 15-1-9.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "WaitBuyGoodsTipView.h"

@interface WaitBuyGoodsTipView ()

@property (weak, nonatomic) IBOutlet UILabel *tipLbl;

@end

@implementation WaitBuyGoodsTipView

+ (WaitBuyGoodsTipView *)tipView
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"WaitBuyGoodsTipView" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.8];
    self.tipLbl.backgroundColor = [UIColor clearColor];
    self.tipLbl.text = @"";
    self.tipLbl.textColor = [UIColor whiteColor];
}

- (void)setTipStr:(NSString *)tipStr
{
    self.tipLbl.text = tipStr;
}

- (void)showWithMaxY:(float)maxY inView:(UIView *)inView completion:(void(^)(void))completion
{
    self.frame = CGRectMake(0, maxY-[[self class]viewHeight], kMainFrameWidth, [[self class]viewHeight]);
    self.alpha = 0.0;
    [inView addSubview:self];
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1.0;
    } completion:^(BOOL finished) {
        if (finished) {
            if (completion) {
                completion();
            }
        }
    }];
}

- (void)dismisWithCompletion:(void(^)(void))completion
{
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
            if (completion) {
                completion();
            }
        }
    }];
}

+ (CGFloat)viewHeight
{
    return 37.f;
}

@end
