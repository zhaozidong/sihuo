//
//  QMPickerView.m
//  QMMM
//
//  Created by kingnet  on 14-9-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMPickerView.h"

#define QMPickerViewHeight 206

@interface QMPickerView ()<UIPickerViewDataSource, UIPickerViewDelegate>
@property (nonatomic) UIPickerView *pickerView;
@property (nonatomic) UIControl *control;
@end
@implementation QMPickerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setupViews];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)dealloc
{
    self.dataArray = nil;
}

+ (void)showWithArray:(NSArray *)dataArray inView:(UIView *)containerView atIndex:(NSInteger)atIndex completion:(PickerBlock)competion
{
    [AppUtils closeKeyboard];
    
    QMPickerView *pickerView = [[QMPickerView alloc]initWithFrame:CGRectMake(0, kMainFrameHeight, kMainFrameWidth, QMPickerViewHeight)];
    pickerView.pickerBlock = [competion copy];
    pickerView.dataArray = dataArray;
    
    pickerView.control.frame = containerView.bounds;
    pickerView.control.center = containerView.center;
    [containerView addSubview:pickerView.control];
    [containerView addSubview:pickerView];

    [UIView animateWithDuration:.3f animations:^{
        CGFloat h = kMainFrameHeight;
        pickerView.frame = CGRectMake(0, h-QMPickerViewHeight, kMainFrameWidth, QMPickerViewHeight);
    } completion:^(BOOL finished) {
        [pickerView chooseWithIndex:atIndex];
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:.3f animations:^{
        self.frame = CGRectMake(0, kMainFrameHeight, kMainFrameWidth, QMPickerViewHeight);
    } completion:^(BOOL finished) {
        [self.control removeFromSuperview];
        [self removeFromSuperview];
        self.dataArray = nil;
    }];
}

- (void)setupViews
{
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:[self pickView]];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_pickerView);
    //设置位置
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[_pickerView]-0-|" options:0 metrics:0 views:views];
    [self addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_pickerView(162)]-0-|" options:0 metrics:0 views:views];
    [self addConstraints:constraints];
    
    //顶部的操作栏
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = UIColorFromRGB(0x595959);
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:view];
    
    views = NSDictionaryOfVariableBindings(_pickerView, view);
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view(44)]-0-[_pickerView]" options:0 metrics:0 views:views];
    [self addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[view]-0-|" options:0 metrics:0 views:views];
    [self addConstraints:constraints];
    
    UIButton *finishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [finishBtn setImage:[UIImage imageNamed:@"navRight_finish_btn"] forState:UIControlStateNormal];
    finishBtn.translatesAutoresizingMaskIntoConstraints = NO;
    [finishBtn addTarget:self action:@selector(finishAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:finishBtn];
    
    [view addConstraint:[NSLayoutConstraint constraintWithItem:finishBtn attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.f constant:0.f]];
    views = NSDictionaryOfVariableBindings(finishBtn);
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[finishBtn(40)]-10-|" options:0 metrics:0 views:views]];
    
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setImage:[UIImage imageNamed:@"xxx_btn"] forState:UIControlStateNormal];
    closeBtn.translatesAutoresizingMaskIntoConstraints = NO;
    [closeBtn addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:closeBtn];
    
    [view addConstraint:[NSLayoutConstraint constraintWithItem:closeBtn attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.f constant:0.f]];
    views = NSDictionaryOfVariableBindings(closeBtn);
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-10-[closeBtn(40)]" options:0 metrics:0 views:views]];
}

- (UIPickerView *)pickView
{
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc]init];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
        _pickerView.showsSelectionIndicator = YES;
        _pickerView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _pickerView;
}

- (UIControl *)control
{
    if (!_control) {
        _control = [[UIControl alloc]init];
        _control.backgroundColor = [UIColor clearColor];
        [_control addTarget:self action:@selector(tapControl:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _control;
}

- (void)tapControl:(id)sender
{
    self.pickerBlock(@"");
    [self dismiss];
}

- (void)finishAction:(id)sender
{
    NSString *string = [self.dataArray objectAtIndex:[self.pickerView selectedRowInComponent:0]];
 
    self.pickerBlock(string);
    [self dismiss];
}

- (void)closeAction:(id)sender
{
    self.pickerBlock(@"");
    [self dismiss];
}

- (void)chooseWithIndex:(NSInteger)index
{
    [self.pickerView selectRow:index inComponent:0 animated:NO];
}

#pragma mark - UIPickView datasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.dataArray) {
        return [self.dataArray count];
    }
    return 0;
}

#pragma mark - UIPickerView delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (self.dataArray) {
        return [self.dataArray objectAtIndex:row];
    }
    return @"";
}

//- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
//{
//    
//}

@end
