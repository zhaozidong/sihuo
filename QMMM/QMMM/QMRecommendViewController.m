//
//  QMRecommendViewController.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMRecommendViewController.h"
#import "MyGoodsList.h"
#import "GoodsHandler.h"
#import "QMGoodsController.h"
#import "QAudioPlayer.h"
#import "GoodEntity.h"
#import "UserDefaultsUtils.h"
#import "QMGoodsFirstPlaceHolder.h"
#import "GoodsDetailViewController.h"
#import "PersonPageViewController.h"
#import "QMShareViewContrller.h"
#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import "MJRefresh.h"
#import "MJRefreshConst.h"
#import "QMGoodsFootView.h"
#import "QMRecommendAdsCell.h"
#import "QMSpecialTopicsCell.h"
#import "ActivityInfo.h"
#import "QMCategoryCell.h"
#import "QMHotTopic1TableViewCell.h"
#import "QMHotTopic2TableViewCell.h"
#import "QMAllTopicViewController.h"

#import "SearchHistoryViewController.h"
#import "SearchCategoryViewController.h"
#import "QMGoodsSearchViewController.h"
#import "TopicGoodsForSearchViewController.h"
#import "TopicGoodsListViewController.h"

#import "ActivityDetailViewController.h"
#import "KeyWordEntity.h"
#import "SearchHistoryDao.h"
#import "UIScrollView+UzysCircularProgressPullToRefresh.h"


@interface QMRecommendViewController ()<QMGoodsControllerDelegate,QMRecommendAdsDelegate,QMSpecialTopicsDelegate,QMCategoryCellDelegate,QMHotTopicCellDelegate,UISearchBarDelegate,SearchHistoryViewControllerDelegate>{
    NSMutableArray * _goodsList;
    NSMutableDictionary * _goodslistDict;
    ActivityInfo *_actInfo;
    
    BOOL hidePrompt;
    QMShareViewContrller * _shareViewController;
    NSUInteger scrollFlag;
    UITextField *textField_;
    
    SearchHistoryViewController *historyViewController_;
    UIButton *cancelButton_;
    
}
@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic, strong) UIView *statusbarBgView; //在滑动时占位用，填充状态栏的颜色

@property (nonatomic, strong) UIColor *navBarBackgroundColor;  //导航栏的颜色

@property (nonatomic, strong) QMRecommendAdsCell *adsCell; //广告cell

//@property (nonatomic, strong) QMSpecialTopicsCell *topicsCell; //专题cell

@property (nonatomic, strong) QMCategoryCell *categoryCell;//分类cell

@property (nonatomic, strong) QMHotTopic1TableViewCell *hotTopic1;

@property (nonatomic, strong) QMHotTopic2TableViewCell *hotTopic2;

@end

@implementation QMRecommendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = kBackgroundColor;
    // Do any additional setup after loading the view.
//    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
//        self.edgesForExtendedLayout =UIRectEdgeNone;
//    }
//
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
//    self.title = [AppUtils localizedProductString:@"QM_Text_Recommend"];
    self.navigationItem.titleView=self.searchBar;
    
//    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(btnSearchClick:) title:nil size:CGSizeMake(44, 44) imageName:@"search"];

    [self registerObserver];
    
    //初始化tableView
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight) style:UITableViewStyleGrouped];
    _tableView.dataSource = self;
    _tableView.delegate = self;
//    _tableView.backgroundColor = UIColorFromRGB(0xf9f7fb);
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.allowsSelection = YES;
    _tableView.separatorColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.scrollsToTop=YES;
    _tableView.rowHeight=UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight=200;
    [self.view addSubview:_tableView];
    
    ((UIScrollView*)_tableView).delegate = self;
    
    if (self.rdv_tabBarController.tabBar.translucent) {
        UIEdgeInsets insets = UIEdgeInsetsMake(64,
                                               0,
                                               CGRectGetHeight(self.rdv_tabBarController.tabBar.frame),
                                               0);
        
        self.tableView.contentInset = insets;
        self.tableView.scrollIndicatorInsets = insets;
    }
    
    [self setupRefresh];
    
    [[GoodsHandler shareGoodsHandler].myGoodsList loadNextPage:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.navigationController.viewControllers.count < 2) {
        //是当前界面
        [[self rdv_tabBarController] setTabBarHidden:NO animated:YES];
    }
    
//    [self getTopData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    scrollFlag=0;
    [[QAudioPlayer sharedInstance] stopPlay];
    if (self.navigationController.viewControllers.count > 1) {
        //是当前界面
        [[self rdv_tabBarController] setTabBarHidden:YES animated:YES];
    }
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //防止在个人主页返回时，导航栏的颜色消失
    scrollFlag=2;
    [self.navigationController.navigationBar setBackgroundImage:nil
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = nil;
    self.navigationController.view.backgroundColor = self.navBarBackgroundColor;
    
}



#pragma mark UISearchBar

- (UISearchBar *)searchBar
{
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, 44)];
        _searchBar.delegate = self;
        
        UIImage* clearImg = [UIImage imageNamed:@"search_bg"];
        clearImg = [clearImg stretchableImageWithLeftCapWidth:7 topCapHeight:5];
        [_searchBar setBackgroundImage:clearImg];
        [_searchBar setSearchFieldBackgroundImage:clearImg forState:UIControlStateNormal];
        [_searchBar setBackgroundColor:[UIColor clearColor]];
        [_searchBar setImage:[UIImage imageNamed:@"search_icon"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
        [_searchBar setImage:[UIImage imageNamed:@"searchClearDel_icon"] forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
        
        for (UIView *subView in _searchBar.subviews) {
            for (UIView *view in subView.subviews) {
                if ([view isKindOfClass:[UITextField class]]) {
                    textField_ = (UITextField *)view;
                }
                if ([view isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                    [view removeFromSuperview];
                }
            }
        }
        
        if (textField_) {
            UIFont *font = [UIFont systemFontOfSize:15];
            NSDictionary *attrDic = @{NSForegroundColorAttributeName:UIColorFromRGB(0xffd4d4), NSFontAttributeName:font};
            NSAttributedString *attrStr = [[NSAttributedString alloc]initWithString:@"输入关键字搜索商品" attributes:attrDic];
            textField_.attributedPlaceholder = attrStr;
            textField_.textColor = [UIColor whiteColor];
        }
    }
    return _searchBar;
}

#pragma mark UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    //跳转到搜索页面
    //点击搜索按钮
    NSString *searchText = _searchBar.text;
    QMGoodsSearchViewController *controller = [[QMGoodsSearchViewController alloc]initWithKeyWords:searchText];
    [self.navigationController pushViewController:controller animated:YES];
    
    [self searchBarCancelButtonClicked:_searchBar];
    
    //存储搜索记录
    KeyWordEntity *entity = [[KeyWordEntity alloc]init];
    entity.keyWord = searchText;
    [[SearchHistoryDao sharedInstance]insert:entity];
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    //取消搜索
    [searchBar setShowsCancelButton:NO animated:YES];
    [textField_ resignFirstResponder];
    searchBar.text = @"";
    
    [historyViewController_ dismissWithCompletion:^{
        historyViewController_ = nil;
    }];
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    [searchBar setShowsCancelButton:YES animated:YES];
    self.navigationItem.leftBarButtonItems = nil;
    self.navigationItem.hidesBackButton = YES;
    
    if(kIOS7) {
        UIView *topView = _searchBar.subviews[0];
        for (UIView *subView in topView.subviews) {
            if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
                cancelButton_ = (UIButton*)subView;
                [cancelButton_ setTitle:[AppUtils localizedProductString:@"QM_Text_Cancel"] forState:UIControlStateNormal];
            }
        }
    } else {
        for(UIView *subView in _searchBar.subviews){
            if([subView isKindOfClass:[UIButton class]]){
                cancelButton_ = (UIButton*)subView;
                [cancelButton_ setTitle:[AppUtils localizedProductString:@"QM_Text_Cancel"] forState:UIControlStateNormal];
            }
        }
    }
    
    //显示搜索历史记录
    if (nil == historyViewController_) {
        historyViewController_ = [[SearchHistoryViewController alloc]init];
        historyViewController_.delegate = self;
        [self addChildViewController:historyViewController_];
        [historyViewController_ didMoveToParentViewController:self];
        [historyViewController_ showInView:self.view];
    }
    
    return YES;
}

#pragma mark SearchHistoryViewControllerDelegate
#pragma mark -
- (void)closeKeyBorad
{
    [textField_ resignFirstResponder];
    cancelButton_.enabled = YES;
}

//选中搜索历史的某一行
- (void)selectRow:(KeyWordEntity *)entity
{
    QMGoodsSearchViewController *controller = [[QMGoodsSearchViewController alloc]initWithKeyWords:entity.keyWord];
    [self.navigationController pushViewController:controller animated:YES];
    
    [self searchBarCancelButtonClicked:_searchBar];
}


- (UIView *)statusbarBgView
{
    if (!_statusbarBgView) {
        _statusbarBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, 20)];
        _statusbarBgView.backgroundColor = [UIColorFromRGB(0xff3232) colorWithAlphaComponent:0.9];
    }
    return _statusbarBgView;
}


#pragma mark UITableViewCell
- (QMRecommendAdsCell *)adsCell
{
    if (!_adsCell) {
        _adsCell = [QMRecommendAdsCell cellForGoodsAds];
        _adsCell.delegate = self;
    }
    return _adsCell;
}

//- (QMSpecialTopicsCell *)topicsCell
//{
//    if (!_topicsCell) {
//        _topicsCell = [QMSpecialTopicsCell cellForGoodsTopics];
//        _topicsCell.delegate = self;
//    }
//    return _topicsCell;
//}


- (QMCategoryCell *)categoryCell
{
    if (!_categoryCell) {
        _categoryCell=[QMCategoryCell categoryCell];
        _categoryCell.delegate=self;
        _categoryCell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return _categoryCell;
}

- (QMHotTopic1TableViewCell *)hotTopic1{
    if (!_hotTopic1) {
        _hotTopic1=[QMHotTopic1TableViewCell hotTopic1];
        _hotTopic1.delegate=self;
        _hotTopic1.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return _hotTopic1;
}

- (QMHotTopic2TableViewCell *)hotTopic2{
    if (!_hotTopic2) {
        _hotTopic2=[QMHotTopic2TableViewCell hotTopic2];
        _hotTopic2.delegate=self;
        _hotTopic2.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return _hotTopic2;
}


#pragma mark -ReloadData
-(void)reloadTableWithDict:(ActivityInfo *)actInfo{
    [self reloadGoodsList];
}

-(void)getTopData{
    __weak typeof(self) weakSelf=self;
    //获取广告和专题相关内容
    [[GoodsHandler shareGoodsHandler] getActivitiesInfo:^(id obj) {
        if (obj && [obj isKindOfClass:[ActivityInfo class]]) {
            //            NSLog(@"actInfo.activity=%@,actInfo.banners=%@",actInfo.activities,actInfo.banners);
            _actInfo=obj;
            [weakSelf.adsCell updateUiWithArr:_actInfo.banners];
            //            [weakSelf.topicsCell updateWithArr:_actInfo.activities];
            [weakSelf.categoryCell updateWithArr:_actInfo.categorys];
            [weakSelf.hotTopic1 updateWithArr:_actInfo.specialTopics];
            [weakSelf.hotTopic2 updateWithArr:_actInfo.specialTopics];
            //            [weakSelf reloadTableWithDict:obj];
        }else{
            NSLog(@"数据格式不正确");
        }
    } failed:^(id obj) {
        NSLog(@"获取广告和专题失败");
    }];
}

- (void)reloadGoodsList
{
    
//    if (!_actInfo || !_goodsList) {
//        return;
//    }
    
    NSMutableArray * tmpArray = [[NSMutableArray alloc] init];
    NSMutableDictionary * tmpDict = [[NSMutableDictionary alloc] init];
    
    // 先清空所有delegate
    for (int i = 0; i < _goodsList.count; i++) {
        QMGoodsController *goodsController = [_goodsList objectAtIndex:i];
        goodsController.delegate = nil;
    }
    
    //更新数据源
    MyGoodsList * myGoodsList = [GoodsHandler shareGoodsHandler].myGoodsList;
    
    @synchronized(myGoodsList.goodsList) {
        for (int i = 0; i < [myGoodsList.goodsList count]; i++) {
            GoodEntity * goodsInfo = [myGoodsList.goodsList objectAtIndex:i];
            
            QMGoodsController * controller = [_goodslistDict objectForKey:@(goodsInfo.gid)];
            if(nil == controller) {
                controller = [[QMGoodsController alloc] initWithGoodsInfo:goodsInfo];
            } else {
                [controller setGoodsInfo:goodsInfo];
            }
            controller.delegate = self;
            
            [tmpArray addObject:controller];
            [tmpDict setObject:controller forKey:@(goodsInfo.gid)];
        }
    }
    _goodsList = tmpArray;
    _goodslistDict = tmpDict;

    //刷新
    [_tableView reloadData];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - Pull Refresh

/**
 *  集成刷新控件
 */
- (void)setupRefresh
{
    //设置下拉刷新
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshActionHandler:^{
        [weakSelf headerRereshing];
    }];
    [self.tableView.tipLabel setText:[UzysTipLabel refreshTips]];
    [self.tableView triggerPullToRefresh];
    
    [self.tableView addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRereshing];
    }];
    
    self.tableView.footer.automaticallyRefresh = NO;
}

- (void)headerRereshing
{
    [self getTopData];
    
    MyGoodsList *myGoodsList = [GoodsHandler shareGoodsHandler].myGoodsList;
    if (!myGoodsList.loading) {
        [myGoodsList loadNextPage:YES];
    }
    
}

////////////////////// modify by zn /////////////
- (void)footerRereshing
{
    [AppUtils trackCustomEvent:@"event_loadingMore" args:nil];
    
    MyGoodsList * myGoodsList = [GoodsHandler shareGoodsHandler].myGoodsList;

    if (!myGoodsList.loading) {
        if(![myGoodsList loadNextPage:NO]) {
            [self.tableView.footer endRefreshing];
        }
    }
}

- (void)stopHeadRefresh
{
    if ([_tableView showPullToRefresh]) {
        [_tableView stopRefreshAnimation];
        [_tableView.tipLabel setText:[UzysTipLabel refreshTips]];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_goodsList count]+3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section<2) {
        return 1;
    }else if(section==2){
        return 2;
    }else{
        if([_goodsList count] > 0) {
            QMGoodsController * controller = [_goodsList objectAtIndex:section-3];
            return [controller tableView:tableView numberOfRowsInSection:section-3];
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = nil;
    if (indexPath.section==0) {
//        QMRecommendAdsCell *adsCell=[QMRecommendAdsCell cellForGoodsAds];
//        adsCell.delegate=self;
        return self.adsCell;
    }else if (indexPath.section==1){
            return self.categoryCell;
    }else if(indexPath.section==2){
    //        QMSpecialTopicsCell *topics=[QMSpecialTopicsCell cellForGoodsTopics];
    //        topics.delegate=self;

        if (indexPath.row==0) {
            return self.hotTopic1;
        }else{
            return self.hotTopic2;
        }
    }else{
        QMGoodsController * controller = [_goodsList objectAtIndex:indexPath.section-3];
        cell = [controller tableView:tableView cellForRowAtIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return cell;
}

#pragma mark - UITableViewDeletegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section<3) {
        return 200;
    }else{
        QMGoodsController * controller = [_goodsList objectAtIndex:indexPath.section-3];
        return [controller tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
//        return kMainFrameWidth/3*2;
        return kMainFrameWidth * 0.43;
    }else if(indexPath.section==2){
//        return 200;
        if (indexPath.row==0) {
            return kMainFrameWidth *0.46;
        }else{
            return kMainFrameWidth *0.23;
        }
    }else if(indexPath.section==1){
        return [QMCategoryCell cellHeight]+25;
    }
    else{
        QMGoodsController * controller = [_goodsList objectAtIndex:indexPath.section-3];
        return [controller tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    if (section==1 || section==2) {
//        return 35;
//    }
    if (section <= 2) {
        return CGFLOAT_MIN;
    }else if (section==3){
        return 35;
    }
    return 8.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == ([_goodsList count]-1)) {
        return 8.f;
    }
    return CGFLOAT_MIN;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==3) {
        UIView *view=[[UIView alloc] init];
        view.frame=CGRectMake(0, 0, kMainFrameWidth, 35);
//        view.backgroundColor=[UIColor yellowColor];
        
        UILabel *lblTitle=[[UILabel alloc] init];
        lblTitle.frame=CGRectMake(8, 5, kMainFrameWidth-8*2, 25);
//        lblTitle.backgroundColor=[UIColor redColor];
        lblTitle.textColor=UIColorFromRGB(0x99a0aa);
        lblTitle.font=[UIFont systemFontOfSize:12];
        lblTitle.textAlignment=NSTextAlignmentCenter;
        lblTitle.text=@"每日最新商品";

        [lblTitle sizeToFit];
        lblTitle.center=view.center;
        [view addSubview:lblTitle];
        
        UIView *viewLine1=[[UIView alloc] init];
        viewLine1.frame=CGRectMake(8,view.frame.size.height/2, (kMainFrameWidth-lblTitle.frame.size.width-2*8)/2-8,0.5);
        viewLine1.backgroundColor=UIColorFromRGB(0xc6d3e5);
        [view addSubview:viewLine1];
        
        UIView *viewLine2=[[UIView alloc] init];
        viewLine2.frame=CGRectMake(lblTitle.frame.origin.x+lblTitle.frame.size.width+8, view.frame.size.height/2, (kMainFrameWidth-lblTitle.frame.size.width-2*8)/2-8, 0.5);
        viewLine2.backgroundColor=UIColorFromRGB(0xc6d3e5);
        [view addSubview:viewLine2];
        
        return view;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section>2) {
        QMGoodsController * controller = [_goodsList objectAtIndex:indexPath.section-3];
        [controller tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2) {
        cell.backgroundColor = [UIColor clearColor];
    }
}

#pragma mark - RegisterObserver

- (void)registerObserver
{
    NSNotificationCenter * nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(processGoodsListLoadedNotify:) name:kGoodsListLoadedNotify object:nil];
    [nc addObserver:self selector:@selector(processGoodsListLoadStateNotify:) name:kGoodsListLoadStateNotify object:nil];
    [nc addObserver:self selector:@selector(processGoodsUpdateFavorNotify:) name:kGoodsUpdateFavorNotify object:nil];

    [nc addObserver:self selector:@selector(processNetworkErrorNotify:) name:kNotNetworkErrorNotity object:nil];
    [nc addObserver:self selector:@selector(processGoodsUpdateCommentNotify:) name:kAddGoodsCommentNotify object:nil];
    [nc addObserver:self selector:@selector(processDeleteGoodsNotify:) name:kDeleteGoodsNotify object:nil];
    [nc addObserver:self selector:@selector(processScrollToTopNotify:) name:kScrollToTopNotify object:nil];
}


#pragma mark - notification

- (void)processPublishGoodsNotify:(NSNotification *)notification
{
    MyGoodsList *myGoodsList = [GoodsHandler shareGoodsHandler].friendGoodsList;
    [myGoodsList loadNextPage:YES];
}

- (void)processGoodsListLoadedNotify:(NSNotification *)notification
{
    NSNumber * type = [notification.userInfo objectForKey:@"type"];
    if(type && [type intValue] == EGLT_RECOMMEND) {
        [self reloadGoodsList];
    }
}

- (void)processGoodsListLoadStateNotify:(NSNotification *)notification
{
    NSNumber * type = [notification.userInfo objectForKey:@"type"];
    if(type && [type intValue] == EGLT_RECOMMEND) {
        [self refreshStateViewUI];
    }
}

- (void)processGoodsUpdateFavorNotify:(NSNotification *)notification
{
    NSDictionary * dict = notification.userInfo;
    NSNumber * temp = [dict objectForKey:@"goodsId"];
    for (int i=0; i<_goodsList.count; i++) {
        QMGoodsController *goodsController = [_goodsList objectAtIndex:i];
        if ([temp intValue] == goodsController.goodsInfo.gid) {
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:3 inSection:i+3];
            [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        }
    }
    
//    NSDictionary * dict = notification.userInfo;
//    NSNumber * temp = [dict objectForKey:@"goodsId"];
//    NSNumber * type = [dict objectForKey:@"type"];
//    if(type && [type intValue] == EGLT_RECOMMEND) {
//        QMGoodsController * viewController = [_goodslistDict objectForKey:temp];
//        if(viewController) {
//            NSUInteger index = [_goodsList indexOfObject:viewController];
//            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:3 inSection:index];
//            [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
//            //            [_tableView reloadData];
//        }
//    }
}

- (void)processDeleteGoodsNotify:(NSNotification *)notification
{
    uint64_t goodsId = [[notification.userInfo objectForKey:@"goodsId"] unsignedLongLongValue];
    if(goodsId != 0) {
        QMGoodsController * viewController = [_goodslistDict objectForKey:@(goodsId)];
        [_goodslistDict removeObjectForKey:@(goodsId)];
        
        NSUInteger index = [_goodsList indexOfObject:viewController];
        if(index != NSNotFound) {
            [_goodsList removeObjectAtIndex:index];
            [_tableView deleteSections:[NSIndexSet indexSetWithIndex:index] withRowAnimation:UITableViewRowAnimationNone];
        } else {
            [_tableView reloadData];
        }
    }
}


- (void)processNetworkErrorNotify:(NSNotification *)notification
{
    [self stopHeadRefresh];
    
    ////////////////////// modify by zn /////////////
    if ([_tableView.footer isRefreshing]) {
        [self.tableView.footer endRefreshing];
    }
}

- (void)processGoodsUpdateCommentNotify:(NSNotification *)notification
{
    NSDictionary * dict = notification.userInfo;
    NSNumber * temp = [dict objectForKey:@"goodsId"];
    QMGoodsController * viewController = [_goodslistDict objectForKey:temp];
    if(viewController) {
        NSUInteger index = [_goodsList indexOfObject:viewController];
        if(NSNotFound != index) {
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:3 inSection:index+3];
            [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}


-(void)processScrollToTopNotify:(NSNotification *)notification{
    if (scrollFlag>1) {
        NSDictionary *dictInfo=notification.userInfo;
        UIViewController *vi=[dictInfo objectForKey:@"viewController"];
        if(self==vi){
            [_tableView setContentOffset:CGPointZero animated:YES];
//            [_tableView setContentOffset:CGPointMake(0, kMainTopHeight+10) animated:YES];
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//            [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }else{
        scrollFlag++;
    }
}


- (void)refreshStateViewUI
{
    MyGoodsList * myGoodsList = [GoodsHandler shareGoodsHandler].myGoodsList;
    
    if (myGoodsList.serverFirstPageLoading) {
        if (NO == [_tableView showPullToRefresh]) {
            [_tableView triggerPullToRefresh];
        }
    } else {
        [self stopHeadRefresh];
    }
    
    if (myGoodsList.loading && !myGoodsList.serverFirstPageLoading) {
//        if (![self.tableView.footer isRefreshing]) {
//            [self.tableView.footer beginRefreshing];
//        }
    }
    else{
        if ([self.tableView.footer isRefreshing]) {
            [self.tableView.footer endRefreshing];
        }
    }
}


#pragma mark - QMGoodsControllerDelegate

//点击商品图像
- (void)didGoodsImageClick:(QMGoodsController *)controller curIndex:(NSInteger)index
{
    [[QAudioPlayer sharedInstance] stopPlay];
    GoodsDetailViewController *viewController = [[GoodsDetailViewController alloc] initWithGoodsId:controller.goodsInfo.gid];
    
    [self.navigationController pushViewController:viewController animated:YES];
    
    //    QMFriendViewController * viewController = [[QMFriendViewController alloc] initWithNibName:@"QMFriendViewController" bundle:nil];
    //    [self.navigationController pushViewController:viewController animated:YES];
}

//点击用户头像
- (void)didGoodsHeadImageClick:(QMGoodsController *)controller userId:(uint64_t)userId
{
    [[QAudioPlayer sharedInstance] stopPlay];
    PersonPageViewController *personController = [[PersonPageViewController alloc]initWithUserId:userId];
    [self.navigationController pushViewController:personController animated:YES];
}

//点击赞
- (void)didGoodsFavorClick:(QMGoodsController *)controller
{
    [[GoodsHandler shareGoodsHandler] addFavor:controller.goodsInfo.gid sellerId:controller.goodsInfo.sellerUserId];
}

//点击评论
- (void)didGoodsCommentClick:(QMGoodsController *)controller
{
    [[QAudioPlayer sharedInstance] stopPlay];
    GoodsDetailViewController *viewController = [[GoodsDetailViewController alloc] initWithGoodsId:controller.goodsInfo.gid];
    viewController.needScrollToComments = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

//点击分享
- (void)didGoodsShareClick:(QMGoodsController *)controller
{
    if(nil == _shareViewController) {
        _shareViewController = [[QMShareViewContrller alloc] init];
    }
    [_shareViewController setGoodsInfo:controller.goodsInfo];
    [_shareViewController show];
}


-(void)didAdsClick:(BannerEntity *)entity{//点击广告图片
    [AppUtils trackCustomEvent:@"click_banner_event" args:nil];
    ActivityDetailViewController *controller = [[ActivityDetailViewController alloc]initWithEntity:entity];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)didSpecialTopiceClick:(ActivityEntity *)actInfo{//点击专题图片
//    NSString *eventName = [NSString stringWithFormat:@"click_%@", actInfo.tag];
//    [AppUtils trackCustomEvent:eventName args:nil];
//    SpecialGoodsViewController *controller = [[SpecialGoodsViewController alloc]initWithActivity:actInfo];
//    [self.navigationController pushViewController:controller animated:YES];
}

//- (void)btnSearchClick:(id)sender
//{
//    QMGoodsSearchViewController *searchViewController = [[QMGoodsSearchViewController alloc]init];
//    [self.navigationController pushViewController:searchViewController animated:YES];
//}

-(void)didTapCategoryCell:(CategoryEntity *)entity{
    //注意一下categoryId==-1的时候跳转到本地全部分类
    if (entity.categoryId == -1) { //更多分类
        SearchCategoryViewController *controller = [[SearchCategoryViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else{
        NSString *mtaEvent = [NSString stringWithFormat:@"click_category_%ld", (long)entity.categoryId];
        [AppUtils trackCustomEvent:mtaEvent args:nil];

        QMGoodsSearchViewController *controller = [[QMGoodsSearchViewController alloc]initWithCategoryId:(int)entity.categoryId];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

-(void)didTapHotTopicCell:(SpecialTopicEntity *)entity{
    //根据url跳转到指定的地址
//    NSLog(@"name=%@,type=%@,key=%@",entity.name,entity.type,entity.key);
    
    [AppUtils trackCustomEvent:entity.key args:nil];
    if ([entity.parseType isEqualToString:@"search"]) {
        TopicGoodsForSearchViewController *controller = [[TopicGoodsForSearchViewController alloc]initWithTitle:entity.name requestURL:entity.url];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else{
        TopicGoodsListViewController *controller = [[TopicGoodsListViewController alloc]initWithTitle:entity.name requestURL:entity.url isTrust:entity.isTrust];
        [self.navigationController pushViewController:controller animated:YES];
    }
}


@end
