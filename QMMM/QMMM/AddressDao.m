//
//  AddressDao.m
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "AddressDao.h"
#import "UserDao.h"
#import "FMDatabaseAdditions.h"
#import "UserInfoHandler.h"

@implementation AddressDao

+ (AddressDao *)sharedInstance
{
    static AddressDao *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (BOOL)insert:(BaseEntity *)entity
{
    AddressEntity *address = (AddressEntity *)entity;
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    if (address.isDefault) {
        NSString *s = [NSString stringWithFormat:@"UPDATE %@ SET isDefault = ? where uid = ? AND addressId <> ?", kAddressTable];
        [db executeUpdate:s, [NSNumber numberWithBool:NO], [NSNumber numberWithLongLong:address.uid], [NSNumber numberWithInt:address.addressId]];
    }
    
    NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO %@ (addressId, uid, provinceId, cityId, distritId, detailAddress, locationName, isDefault, consignee, phone) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", kAddressTable];
    
    return [db executeUpdate:insertSQL, [NSNumber numberWithInt:address.addressId], [NSNumber numberWithLongLong:address.uid], [NSNumber numberWithInt:address.provinceId], [NSNumber numberWithInt:address.cityId], [NSNumber numberWithInt:address.distritId],
            address.detailAddress, address.locationName, [NSNumber numberWithBool:address.isDefault], address.consignee, address.phone];
}

- (BOOL)update:(BaseEntity *)entity
{
    AddressEntity *address = (AddressEntity *)entity;
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    if (address.isDefault) {
        NSString *s = [NSString stringWithFormat:@"UPDATE %@ SET isDefault = ? where uid = ? AND addressId <> ?", kAddressTable];
        [db executeUpdate:s, [NSNumber numberWithBool:NO], [NSNumber numberWithLongLong:address.uid], [NSNumber numberWithInt:address.addressId]];
    }
    NSString *updateSQL = [NSString stringWithFormat:@"UPDATE %@ SET provinceId = ?, cityId = ?, distritId = ?, detailAddress = ?, locationName = ?, isDefault = ?, consignee = ?, phone = ? WHERE uid = ? AND addressId = ?", kAddressTable];
    return [db executeUpdate:updateSQL, [NSNumber numberWithInt:address.provinceId], [NSNumber numberWithInt:address.cityId], [NSNumber numberWithInt:address.distritId], address.detailAddress, address.locationName, [NSNumber numberWithBool:address.isDefault], address.consignee, address.phone, [NSNumber numberWithLongLong:address.uid], [NSNumber numberWithInt:address.addressId]];
}

- (BOOL)remove:(BaseEntity *)entity
{
    AddressEntity *address = (AddressEntity *)entity;
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *deletSQL = [NSString stringWithFormat:@"DELETE FROM %@ WHERE addressId = ?", kAddressTable];
    return [db executeUpdate:deletSQL, [NSNumber numberWithInt:address.addressId]];
}

- (NSMutableArray *)queryAll
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *selectSQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE uid = ?", kAddressTable];
    ;
    UserEntity *entity = [UserInfoHandler sharedInstance].currentUser;
    NSNumber *uid = [NSNumber numberWithLongLong:[entity.uid longLongValue]];
    FMResultSet *rs = [db executeQuery:selectSQL, uid];
    NSMutableArray *array = [NSMutableArray array];
    while ([rs next]) {
        AddressEntity *entity = [[AddressEntity alloc]initWithFMResultSet:rs];
        [array addObject:entity];
    }
    return array;
}

- (void)updateFirstRow
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE uid = ? ORDER BY ROWID ASC LIMIT 1", kAddressTable];
    UserEntity *entity = [UserInfoHandler sharedInstance].currentUser;
    NSNumber *uid = [NSNumber numberWithLongLong:[entity.uid longLongValue]];
    FMResultSet *rs = [db executeQuery:querySQL, uid];
    while ([rs next]) {
        AddressEntity *entity = [[AddressEntity alloc]initWithFMResultSet:rs];
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE %@ SET isDefault = ? WHERE addressId = ?", kAddressTable];
        [db executeUpdate:updateSQL, [NSNumber numberWithBool:YES], [NSNumber numberWithInt:entity.addressId]];
        break;
    }
}

- (int)getAddressCount
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    UserEntity *entity = [UserInfoHandler sharedInstance].currentUser;
    NSNumber *uid = [NSNumber numberWithLongLong:[entity.uid longLongValue]];

    return [db intForQuery:@"SELECT COUNT(*) FROM Address WHERE uid = ?", uid];
}

@end
