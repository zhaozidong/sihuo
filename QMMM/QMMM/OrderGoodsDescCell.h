//
//  OrderGoodsDescCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodDataInfo.h"

@class OrderSimpleInfo;

extern NSString * const kOrderGoodsDescCellIdentifier;

@interface OrderGoodsDescCell : UITableViewCell

+ (UINib *)nib;

- (void)updateUI:(OrderSimpleInfo *)orderInfo cellType:(EControllerType)cellType;

+ (CGFloat)cellHeight;

@end
