//
//  QMHotTopic1TableViewCell.h
//  QMMM
//
//  Created by Derek on 15/3/17.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityInfo.h"

@protocol QMHotTopicCellDelegate <NSObject>

-(void)didTapHotTopicCell:(SpecialTopicEntity *)entity;

@end



@interface QMHotTopic1TableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *topicImg1;

@property (strong, nonatomic) IBOutlet UIImageView *topicImg2;

@property (strong, nonatomic) IBOutlet UIImageView *topicImg3;

@property (weak, nonatomic) id<QMHotTopicCellDelegate>delegate;

+(QMHotTopic1TableViewCell *)hotTopic1;

-(void)updateWithArr:(NSArray *)arr;


@end
