//
//  QMDataCenter.h
//  QMMM
//
//  Created by hanlu on 14-10-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@class FMDatabase;
@class KNThread;
@class GoodsDataHelper;
@class QMChatUserHelper;
@class QMContactHelper;
@class QMFriendHelper;
@class QMMsgHelper;

@interface QMDataCenter : NSObject

@property (nonatomic, strong) KNThread * workThread;

@property (nonatomic, strong) GoodsDataHelper * goodsDataHelper;

@property (nonatomic, strong) QMChatUserHelper * chatUserHelper;

@property (nonatomic, strong) QMContactHelper * contactHelper;

@property (nonatomic, strong) QMFriendHelper * friendHelper;

@property (nonatomic, strong) QMMsgHelper * msgHelper;

+ (QMDataCenter *)sharedDataCenter;

+ (void)freeDataCenter;

//启动数据中心
- (void)start;

//停止
- (void)stop;

//是否启动
- (BOOL)isStarted;

@end
