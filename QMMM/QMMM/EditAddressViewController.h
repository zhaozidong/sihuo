//
//  EditAddressViewController.h
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@class AddressEntity;
typedef void(^EditAddressBlock)(AddressEntity *entity);
@interface EditAddressViewController : BaseViewController
@property (nonatomic, copy) EditAddressBlock editAddressBlock;
- (id)initWithBlock:(EditAddressBlock)editBlock;

- (id)initWithAddress:(AddressEntity *)addressEntity editBlock:(EditAddressBlock)editBlock;

@end
