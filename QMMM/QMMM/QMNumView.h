//
//  QMNumViewV2.h
//  QMMM
//
//  Created by hanlu on 14-10-31.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMNumView : UIView

@property (nonatomic, strong) UIImageView * bkgImageView;

@property (nonatomic, strong) UILabel * numLabel;

//modify by zn
@property (nonatomic, assign) CGFloat width;

@property (nonatomic, assign) BOOL isRedDot; //是否当小红点用，上面没数字

- (void)setNum:(NSUInteger)num;

@end
