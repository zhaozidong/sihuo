//
//  QMOrderDetailRightTextV2Cell.h
//  QMMM
//
//  Created by hanlu on 14-11-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMOrderDetailRightTextV2CellIdentifier;

@interface QMOrderDetailRightTextV2Cell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * nameLabel;

@property (nonatomic, strong) IBOutlet UILabel * valueLabel;

+ (id)cellForOrderDetailRightTextV2;

@end
