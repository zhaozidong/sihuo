//
//  UserRightsManager.m
//  QMMM
//
//  Created by Derek on 15/1/21.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "UserRightsManager.h"
#import <AddressBook/AddressBook.h>
#import "QMAuthorizationGuide.h"
#import "QMOpenContactViewController.h"

@interface UserRightsManager ()<UIAlertViewDelegate>

@end
@implementation UserRightsManager

static UserRightsManager *sharedInstance=nil;

+(id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if(sharedInstance == nil)
        {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;
        }
    }
    return nil;
}

+(UserRightsManager *)sharedInstance{
    @synchronized(self)
    {
        if(nil == sharedInstance)
        {
            [self new];
        }
    }
    return sharedInstance;
}


-(void)getContactRights:(successBlock)successCallBack failure:(failureBlock)failureCallBack {
    ABAddressBookRef _addressBook = NULL;
    if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined)
        {
//            __block QMAuthorizationGuide *guide=[[QMAuthorizationGuide alloc] initWithType:QMAuthorizationGuideTypeAddressBook];
//            guide.frame=CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight);
//            [[UIApplication sharedApplication].keyWindow addSubview:guide];
            
            ABAddressBookRequestAccessWithCompletion(_addressBook, ^(bool granted, CFErrorRef error) {
                dispatch_async(dispatch_get_main_queue(), ^{
//                    [guide removeFromSuperview];
                    if (granted) {//allow
                        [AppUtils trackCustomEvent:@"evnet_uploadContact" args:nil];
                        if (successCallBack) {
                            successCallBack(QMRightTypeAuthorized);
                        }
                    }else{//not allow
                        if (successCallBack) {
                            successCallBack(QMRightTypeDenied);
                        }
                    }
                });
            });
        }else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){//allow
            if (successCallBack) {
                successCallBack(QMRightTypeAuthorized);
            }
        }else if(ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied){//now allow
            if (successCallBack) {
                successCallBack(QMRightTypeDenied);
            }
        }
    }else{
        if (failureCallBack) {
            failureCallBack();
        }
    }
}


-(void)getCameraRights:(successBlock)successCallBack failure:(failureBlock)failureCallBack{
    
    
    
    
    
    
    
}

//获取麦克风权限
-(void)getMicroPhoneRights:(successBlock)successCallBack failure:(failureBlock)failureCallBack{
    if ([[AVAudioSession sharedInstance] respondsToSelector:@selector(recordPermission)]) {//iOS8
        switch ([[AVAudioSession sharedInstance] recordPermission]) {
            case AVAudioSessionRecordPermissionGranted:
                if (successCallBack) {
                    successCallBack(QMRightTypeAuthorized);
                }
                break;
            case AVAudioSessionRecordPermissionDenied:
                if (successCallBack) {
                    successCallBack(QMRightTypeDenied);
                }
                break;
            case AVAudioSessionRecordPermissionUndetermined:{
                // This is the initial state before a user has made any choice
                // You can use this spot to request permission here if you want
                
                [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
                    if (granted) {
                        //NSLog(@"Permission granted");
                        if (successCallBack) {
                            successCallBack(QMRightTypeAuthorized);
                        }
                    }
                    else {
                        //NSLog(@"Permission denied");
                        if (successCallBack) {
                            successCallBack(QMRightTypeDenied);
                        }
                    }
                }];}
                break;
            default:
                if (failureCallBack) {
                    failureCallBack();
                }
                break;
        }
    }else{//iOS7
        [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
            if (granted) {
                //NSLog(@"Permission granted");
                if (successCallBack) {
                    successCallBack(QMRightTypeAuthorized);
                }
            }
            else {
                //NSLog(@"Permission denied");
                if (successCallBack) {
                    successCallBack(QMRightTypeDenied);
                }
            }
        }];
    }
}


-(BOOL)getPushNotificationRights{
    UIApplication *application=[UIApplication sharedApplication];
    BOOL enabled;
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        enabled=[application isRegisteredForRemoteNotifications];
    }else{
        UIRemoteNotificationType types=[application enabledRemoteNotificationTypes];
        enabled=types & UIRemoteNotificationTypeAlert;
    }
    return enabled;
}

- (void)showNotifyTip:(UIViewController *)viewCtl
{
    //已经开启了改权限
    if ([self getPushNotificationRights]) {
        return;
    }
    
    //获得类名
    NSString *className = NSStringFromClass([viewCtl class]);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *dict = [defaults objectForKey:kEverShowNotifyTip];
    id temp = [dict objectForKey:className];
    if ((nil == dict) || (nil == temp) || (NO == [temp boolValue])) {
        NSMutableDictionary *mbDict = [NSMutableDictionary dictionary];
        if (dict) {
            [mbDict setDictionary:dict];
        }
        
        [mbDict setObject:@(YES) forKey:className];
        [defaults setObject:mbDict forKey:kEverShowNotifyTip];
        [defaults synchronize];
    
        if ([[[UIDevice currentDevice] systemVersion] floatValue]>=8) {
            UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:@"私货的系统提醒未打开" message:@"为了避免您错过商品的交易提醒，请开启系统推送通知。" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"马上开启", nil];
            alerView.tag = 1000;
            [alerView show];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"私货的系统提醒未打开" message:@"为了避免您错过商品的交易提醒，请在设置—>通知—>私货里开启系统推送通知。" delegate:nil cancelButtonTitle:[AppUtils localizedCommonString:@"QM_Button_IKnow"] otherButtonTitles:nil];
            [alertView show];
        }
    }
}

#pragma mark - UIAlerViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex == buttonIndex) {
        return;
    }
    
    if (alertView.tag == 1000) {
        //跳转到系统设置界面
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

@end
