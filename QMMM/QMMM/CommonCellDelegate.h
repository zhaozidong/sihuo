//
//  CommonCellDelegate.h
//  QMMM
//
//  Created by kingnet  on 14-11-3.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CommonCellDelegate <NSObject>

- (void)didEditing:(NSIndexPath *)indexPath;

@end
