//
//  EditAddressViewController.m
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "EditAddressViewController.h"
#import "EditAddressCellInfo.h"
#import "ButtonAccessoryView.h"
#import "ChooseCityView.h"
#import "AddressHandler.h"
#import "AddressEntity.h"
#import "BackNavigationController.h"

@interface EditAddressViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic) UITableView *tableView;
@property (nonatomic, strong) EditAddressCellInfo *cellInfo;
@property (nonatomic, strong) AddressHandler *addressHandler;
@property (nonatomic, strong) AddressEntity *addressEntity; //得到的结果
@property (nonatomic, strong) AddressEntity *oldEntity; //上一个界面得到
@property (nonatomic, assign) BOOL isUpdate; //是否为修改
@end

@implementation EditAddressViewController

- (instancetype)initWithBlock:(EditAddressBlock)editBlock
{
    self = [super init];
    if (self) {
        self.isUpdate = NO;
        self.editAddressBlock = [editBlock copy];
        self.addressEntity = [[AddressEntity alloc]init];
    }
    return self;
}

- (instancetype)initWithAddress:(AddressEntity *)addressEntity editBlock:(EditAddressBlock)editBlock
{
    self = [super init];
    if (self) {
        self.isUpdate = YES;
        self.editAddressBlock = [editBlock copy];
        self.oldEntity = addressEntity;
        self.addressEntity = [[AddressEntity alloc]init];
        self.addressEntity.provinceId = self.oldEntity.provinceId;
        self.addressEntity.cityId = self.oldEntity.cityId;
        self.addressEntity.distritId = self.oldEntity.distritId;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.isUpdate) {
        self.navigationItem.title = @"更新收货地址";
    }
    else{
        self.navigationItem.title = @"添加收货地址";
    }
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(saveAction:) title:nil size:CGSizeMake(44.f, 44.f) imageName:@"navRight_finish_btn"];
  
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    // Do any additional setup after loading the view.
    self.cellInfo = [[EditAddressCellInfo alloc]init];
    self.addressHandler = [[AddressHandler alloc]init];
    [self setupViews];
    if (self.isUpdate) {
        [self updateUI];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self registerObserver];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self removeObserver];
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)registerObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processKeyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)removeObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [nc removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [nc removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)processKeyboardWillHide:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0 ;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    [UIView animateWithDuration:animationDuration animations:^{
        UIEdgeInsets contentInsets = self.tableView.contentInset;
        contentInsets = UIEdgeInsetsMake(contentInsets.top, 0.0, 0.0, 0.0);
        self.tableView.contentInset = contentInsets;
        self.tableView.scrollIndicatorInsets = contentInsets;
    }];
}

- (void)processKeyboardWillShow:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0 ;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = self.tableView.contentInset;
    contentInsets = UIEdgeInsetsMake(contentInsets.top, 0.0, (keyboardSize.height), 0.0);
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.tableView.contentInset = contentInsets;
        self.tableView.scrollIndicatorInsets = contentInsets;
    } completion:^(BOOL finished) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.cellInfo.editingIndexPath];
        [self.tableView scrollRectToVisible:cell.frame animated:YES];
    }];
}

- (void)setupViews
{
    [self.view addSubview:self.tableView];
}

- (void)updateUI
{
    if (self.oldEntity) {
        self.cellInfo.buyerName = self.oldEntity.consignee;
        self.cellInfo.buyerPhone = self.oldEntity.phone;
        self.cellInfo.address = self.oldEntity.locationName;
        self.cellInfo.detailAddress = self.oldEntity.detailAddress;
        self.cellInfo.isDefault = self.oldEntity.isDefault;
    }
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer == self.navigationController.interactivePopGestureRecognizer) {
        [self backAction:nil];
        return NO;
    }
    return YES;
}

- (void)backAction:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[AppUtils localizedCommonString:@"QM_Alert_Discard"] message:nil delegate:self cancelButtonTitle:[AppUtils localizedCommonString:@"QM_Button_Discard"] otherButtonTitles:[AppUtils localizedCommonString:@"QM_Button_GoOn"], nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex == buttonIndex) {
        self.navigationController.interactivePopGestureRecognizer.delegate = (BackNavigationController *)self.navigationController;
        [self.navigationController popViewControllerAnimated:YES];
    }
}

//保存地址
- (void)saveAction:(id)sender
{
    [AppUtils closeKeyboard];
    
    if (self.cellInfo.buyerName.length == 0) {
        [AppUtils showAlertMessage:@"收货人姓名不能为空"];
        return;
    }
    if (![AppUtils checkPhoneNumber:self.cellInfo.buyerPhone]) {
        [AppUtils showAlertMessage:@"手机号格式不正确"];
        return;
    }
    if (self.cellInfo.address.length == 0) {
        [AppUtils showAlertMessage:@"省市区不能为空"];
        return;
    }
    if (self.cellInfo.detailAddress.length == 0) {
        [AppUtils showAlertMessage:@"详细地址不能为空"];
        return;
    }
    self.addressEntity.consignee = self.cellInfo.buyerName;
    self.addressEntity.phone = self.cellInfo.buyerPhone;
    self.addressEntity.locationName = self.cellInfo.address;
    self.addressEntity.detailAddress = self.cellInfo.detailAddress;
    self.addressEntity.isDefault = self.cellInfo.isDefault;

    [AppUtils showProgressMessage:@"正在提交"];
    EditAddressViewController __weak * weakSelf = self;
    if (self.isUpdate) {
        self.addressEntity.addressId = self.oldEntity.addressId;
        [self.addressHandler updateAddress:self.addressEntity success:^(id obj) {
            [AppUtils dismissHUD];
            [weakSelf finishAdd:(AddressEntity *)obj];
        } failed:^(id obj) {
            [AppUtils showErrorMessage:(NSString *)obj];
        }];
    }
    else{
        [self.addressHandler addNewAddress:self.addressEntity success:^(id obj) {
            [AppUtils dismissHUD];
            [weakSelf finishAdd:(AddressEntity *)obj];
        } failed:^(id obj) {
            [AppUtils showErrorMessage:(NSString *)obj];
        }];
    }
}

- (void)finishAdd:(AddressEntity *)entity
{
    self.editAddressBlock(entity);
    self.navigationController.interactivePopGestureRecognizer.delegate = (BackNavigationController *)self.navigationController;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Getter
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
    }
    return _tableView;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section ==0) {
        return 2;
    }
    else if (section == 1){
        return 2;
    }
    else if (section == 2){
        return 1;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [EditAddressCellInfo cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return self.cellInfo.cellsArray[0];
        }
        else if (indexPath.row == 1){
            return self.cellInfo.cellsArray[1];
        }
    }
    else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            return self.cellInfo.cellsArray[2];
        }
        else if(indexPath.row == 1){
            return self.cellInfo.cellsArray[3];
        }
    }
    else if(indexPath.section == 2){
        return self.cellInfo.cellsArray[4];
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - UITableView delegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 14.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 14.f)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            EditAddressViewController __weak * weakSelf = self;
            [ChooseCityView showInView:self.view provinceId:self.addressEntity.provinceId cityId:self.addressEntity.cityId distritId:self.addressEntity.distritId completion:^(int provinceID, int cityID, int districtID, NSString *result) {
                if (![result isEqualToString:@""]) {
                    weakSelf.cellInfo.address = result;
                    weakSelf.addressEntity.provinceId = provinceID;
                    weakSelf.addressEntity.cityId = cityID;
                    weakSelf.addressEntity.distritId = districtID;
                }
            }];
        }
    }
    if (indexPath.section == 2) {
        self.cellInfo.isDefault = !self.cellInfo.isDefault;
    }
}

@end
