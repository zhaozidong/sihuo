//
//  GoodsCategoryCell.m
//  QMMM
//
//  Created by kingnet  on 15-1-14.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "GoodsCategoryCell.h"
#import "QMGoodsCateInfo.h"

NSString * kGoodsCategoryCellIdentifier = @"kGoodsCategoryCellIdentifier";

@interface GoodsCategoryCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;

@end

@implementation GoodsCategoryCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"GoodsCategoryCell" bundle:[NSBundle mainBundle]];
}

- (void)awakeFromNib {
    // Initialization code
    self.nameLbl.text = @"";
    self.nameLbl.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(QMGoodsCateInfo *)category
{
    self.nameLbl.text = category.name;
}

+ (CGFloat)cellHeight
{
    return 45.f;
}

@end
