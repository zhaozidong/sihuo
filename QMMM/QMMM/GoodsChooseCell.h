//
//  GoodsChooseCell.h
//  QMMM
//
//  Created by kingnet  on 15-3-2.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsChooseCell : UITableViewCell

+ (GoodsChooseCell *)chooseCell;

+ (NSArray *)leftTextArr;

@property (nonatomic, strong)NSString *content; //填充的内容
@property (nonatomic, strong)NSString *leftText;

@property (nonatomic, strong)UIColor *contentColor;

@end
