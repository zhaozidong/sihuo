//
//  QMMsgV2ViewController.h
//  QMMM
//
//  Created by hanlu on 14-11-10.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMMsgV2ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView * tableView;

@end
