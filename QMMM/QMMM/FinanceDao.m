//
//  FinanceDao.m
//  QMMM
//
//  Created by kingnet  on 14-9-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "FinanceDao.h"
#import "UserDao.h"

@implementation FinanceDao

+ (FinanceDao *)sharedInstance
{
    static FinanceDao *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (BOOL)insert:(BaseEntity *)entity
{
    FinanceDataInfo *finance = (FinanceDataInfo *)entity;
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO %@ (_id, uid, desc, amount, orderId, payOrderId, comments, type, status, ts) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", kFinanceTable];
    return [db executeUpdate:insertSQL, [NSNumber numberWithInt:finance._id], [NSNumber numberWithLongLong:finance.uid], finance.desc, [NSNumber numberWithInt:finance.amount], finance.orderId, finance.payOrderId, finance.comments, [NSNumber numberWithInt:finance.type], [NSNumber numberWithInt:finance.status], [NSNumber numberWithInt:finance.ts]];
}

- (NSMutableArray *)queryAll
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *selectSQL = [NSString stringWithFormat:@"SELECT * FROM %@", kFinanceTable];
    
    FMResultSet *rs = [db executeQuery:selectSQL];
    NSMutableArray *array = [NSMutableArray array];
    while ([rs next]) {
        FinanceDataInfo *finance = [[FinanceDataInfo alloc]initWithFMResultSet:rs];
        [array addObject:finance];
    }
    return array;
}

- (BOOL)removeAll
{
    FMDatabase *db = [[QMDatabaseHelper sharedInstance]openDatabase];
    NSString *deleteSQL = [NSString stringWithFormat:@"delete from %@", kFinanceTable];
  
    BOOL rs = [db executeUpdate:deleteSQL];
    return rs;
}

@end
