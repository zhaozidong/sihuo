//
//  UIImage+Additions.m
//  QM
//
//  Copyright (c) 2011年 SNDA Corporation. All rights reserved.
//

#import "UIImage+Additions.h"
#import <CommonCrypto/CommonDigest.h>

@implementation UIImage (Additions)

- (id)thumbnailImage:(CGSize)size
{
    return [self thumbnailImage:size cornerRadius:0.0f];
}

- (id)thumbnailImage:(CGSize)size cornerRadius:(CGFloat)radius
{
    // 创建缩略图
    if (self == nil || size.width <= 0 || size.height <= 0)
        return nil;
    
    // 计算图片按照屏幕缩放比例的大小
    CGSize imageSize = [self size];
    CGFloat imageScale = [self scale];
    CGFloat screenScale = [UIScreen mainScreen].scale;
    if (imageScale != screenScale) {
        imageSize.width *= imageScale / screenScale;
        imageSize.height *= imageScale / screenScale;
    }
    radius = radius > 0 ? radius : 0.0f;
    if (!CGSizeEqualToSize(imageSize, size) || (radius != 0.0f)) {

        UIGraphicsBeginImageContextWithOptions(size, NO, screenScale);
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        // 创建圆角
        if (radius != 0.0f) {
            CGFloat minValue = size.width < size.height ? size.width / 2 : size.height / 2;
            radius = radius < minValue ? radius : minValue;
            
            CGContextBeginPath(context);
            CGFloat midX = size.width / 2;
            CGFloat midY = size.height / 2;
            CGContextMoveToPoint(context, 0, midY);
            CGContextAddArcToPoint(context, 0, 0, midX, 0, radius);
            CGContextAddArcToPoint(context, size.width, 0, size.width, midY, radius);
            CGContextAddArcToPoint(context, size.width, size.height, midX, size.height, radius);
            CGContextAddArcToPoint(context, 0, size.height, 0, midY, radius);
            CGContextClosePath(context);
            CGContextClip(context);
        }
        
        // 计算合适的缩放比例
        CGFloat scaleX = size.width / imageSize.width;
        CGFloat scaleY = size.height / imageSize.height;
        CGFloat scale = MAX(scaleX, scaleY);
        
        CGFloat width = imageSize.width * scale;
        CGFloat height = imageSize.height * scale;

        CGFloat dwidth = (size.width - width) / 2;
        CGFloat dheight = (size.height - height) / 2;
        
        // 缩放绘图
        CGRect rect = CGRectMake(dwidth, dheight, width, height);
        [self drawInRect:rect];
        
        // 生成缩放完成后的图片
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return newImage;
    }
    
    return self;
}

+ (id)thumbnailImage:(UIImage *)image size:(CGSize)size
{
    return [image thumbnailImage:size cornerRadius:0.0f];
}

+ (id)thumbnailImage:(UIImage *)image size:(CGSize)size cornerRadius:(CGFloat)radius
{
    return [image thumbnailImage:size cornerRadius:radius];
}

@end
