//
//  EditInfoViewController.h
//  QMMM
//  修改我的资料处的信息
//  Created by kingnet  on 15-1-22.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, EditInfoType){
    QMEditNickname,
    QMEditEmail
};

@interface EditInfoViewController : BaseViewController

@property (nonatomic, assign) EditInfoType editInfoType;

@property (nonatomic, strong) NSString *text; //初始化进来时显示的内容

- (id)initWithType:(EditInfoType)type;

@end
