//
//  SearchHistoryViewController.h
//  QMMM
//
//  Created by kingnet  on 15-1-13.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@class KeyWordEntity;

@protocol SearchHistoryViewControllerDelegate <NSObject>

- (void)closeKeyBorad;

- (void)selectRow:(KeyWordEntity *)entity;

@end

@interface SearchHistoryViewController : BaseViewController

@property (nonatomic, weak) id<SearchHistoryViewControllerDelegate> delegate;

- (void)showInView:(UIView *)view;

- (void)showInViewTop:(UIView *)view;

- (void)dismissWithCompletion:(void(^)(void))completion;

@end
