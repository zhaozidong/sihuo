//
//  KeyWordEntity.m
//  QMMM
//
//  Created by kingnet  on 15-1-13.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "KeyWordEntity.h"
#import "FMResultSet.h"

@implementation KeyWordEntity

- (id)initWithFMResultSet:(FMResultSet *)resultSet
{
    self = [super init];
    if (self) {
        self.wordId = [resultSet intForColumn:@"id"];
        self.keyWord = [resultSet stringForColumn:@"keyWord"];
    }
    return self;
}

@end
