//
//  UITextField+Additions.m
//  QMMM
//
//  Created by kingnet  on 15-3-18.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "UITextField+Additions.h"

@implementation UITextField (Additions)

- (BOOL)shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDot = NO;
    if ([self.text rangeOfString:@"."].location != NSNotFound) {
        isHaveDot = YES;
    }
    
    //如果输入的是小数点
    if ([string isEqualToString:@"."]) {
        //第一位是小数点时不可以
        if (self.text == nil || [self.text isEqualToString:@""]) {
            return NO;
        }
        else{
            //如果输入过小数点则不能再输入了
            if (isHaveDot) {
                return NO;
            }
        }
    }
    
    
    //如果第一位输入的是0，再次输入的不是点
    if ([self.text isEqualToString:@"0"] && ![string isEqualToString:@"."]) {
        return NO;
    }
    
    //判读有小数点时
    if (isHaveDot) {
        NSRange ran=[self.text rangeOfString:@"."];
        NSInteger tt=range.location-ran.location;
        if (tt<=2) {
            return YES;
        }
        else{
            return NO;
        }
    }
    else{
        return YES;
    }
}

@end
