//
//  OrderTimeCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kOrderTimeCellIdentifier;

@class OrderSimpleInfo;
@interface OrderTimeCell : UITableViewCell

+ (UINib *)nib;

- (void)updateUI:(OrderSimpleInfo *)orderInfo;

+ (CGFloat)cellHeight;

@end
