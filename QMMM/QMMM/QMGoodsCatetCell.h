//
//  QMGoodsCatetCell.h
//  QMMM
//
//  Created by hanlu on 14-9-19.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * kQMGoodsCatetCellIdentifier;

@class QMGoodsCateInfo;

@interface QMGoodsCatetCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * name;

@property (nonatomic, strong) IBOutlet UILabel * desc;

@property (nonatomic, strong) IBOutlet UIImageView * cateImageView;

+ (UINib *)nib;

- (void)updateUI:(QMGoodsCateInfo *)cateInfo;

+ (CGFloat)cellHeight;

@end
