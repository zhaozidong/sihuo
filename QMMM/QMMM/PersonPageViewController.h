//
//  PersonPageViewController.h
//  QMMM
//
//  Created by kingnet  on 15-1-22.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@interface PersonPageViewController : BaseViewController

- (instancetype)initWithUserId:(uint64_t )uid;

@end
