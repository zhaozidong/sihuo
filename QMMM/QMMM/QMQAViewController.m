//
//  QMQAViewController.m
//  QMMM
//
//  Created by Derek on 15/3/11.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMQAViewController.h"
#import <WebKit/WebKit.h>
#import "QMWebViewDelegate.h"

@interface QMQAViewController ()<UIWebViewDelegate>{
    NSString *strURL;
}

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) QMWebViewDelegate *webViewDelegate;

@end

@implementation QMQAViewController

-(id)initWithURL:(NSString *)URL{
    self=[super init];
    if (self) {
        strURL=URL;
    }
    return self;
}

-(id)initWithType:(QMQAType)type{
    self=[super init];
    if (self) {
        NSString *strTemp;
        switch (type) {
            case QMQATypeSiHuo:
                strTemp=@"sihuo";
                break;
            case QMQATypeAddFriend:
                strTemp=@"addFriend";
                break;
            case QMQATypeKeXinDu:
                strTemp=@"kexindu";
                break;
            case QMQATypeYingXiangLi:
                strTemp=@"yingxiangli";
                break;
            case QMQATypeTotalVolume:
                strTemp=@"totalVolume";
                break;
            case QMQATypeHaoPingLv:
                strTemp=@"haopinglv";
                break;
            case QMQATypeAddPhotos:
                strTemp=@"addPhotos";
                break;
            case QMQATypeDescribing:
                strTemp=@"describing";
                break;
            case QMQATypeMakePrice:
                strTemp=@"makePrice";
                break;
            case QMQATypeAudio:
                strTemp=@"audio";
                break;
            case QMQATypeCondition:
                strTemp=@"condition";
                break;
            case QMQATypeHowToBuy:
                strTemp=@"howToBuy";
                break;
            case QMQATypeCash:
                strTemp=@"cash";
                break;
            case QMQATypeHongBao:
                strTemp=@"hongbao";
                break;
            case QMQATypePrivateTrade:
                strTemp=@"privateTrade";
                break;
            case QMQATypeJustWantCheap:
                strTemp=@"justWantCheap";
                break;
            case QMQATypeReportInTime:
                strTemp=@"reportInTime";
                break;
            case QMQATypeBelieveTrade:
                strTemp=@"believeTrade";
            default:
                strTemp=@"";
                break;
        }
        strURL=[NSString stringWithFormat:@"/?c=index&a=faq#%@",strTemp];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"帮助";
    
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    _webView.delegate = self.webViewDelegate;
    _webView.multipleTouchEnabled = NO;
    _webView.scalesPageToFit = YES;
    [self.view addSubview:self.webView];
    
    
    NSString *finalURL=[@"http://" stringByAppendingString:[[AppUtils serverHost] stringByAppendingString:strURL]];
    
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:finalURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [_webView loadRequest:request];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=NO;
}

@end
