//
//  QMSelectExpressCoViewController.m
//  QMMM
//
//  Created by hanlu on 14-10-9.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMSelectExpressCoViewController.h"
#import "QMSelectExpressCell.h"

@interface QMSelectExpressCoViewController () <UITableViewDataSource, UITableViewDelegate, QMSelectExpressDelegate> {
    NSMutableArray * _expressArray;
    NSMutableArray * _recommendArray;
}

@end

@implementation QMSelectExpressCoViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)initNavBar
{
    self.title = [AppUtils localizedProductString:@"QM_Text_Select_Express"];
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(btnBackClick:)];
//    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(btnOkClick:) title:@"确定" size:CGSizeMake(44.f, 44.f) imageName:@""];
}

- (void)initExpressList
{
    _expressArray = [[NSMutableArray alloc] init];
    _recommendArray = [[NSMutableArray alloc] init];
    
    NSString * filePath = [[NSBundle mainBundle] pathForResource:@"express" ofType:@"plist"];
    NSDictionary * dict = [NSDictionary dictionaryWithContentsOfFile:filePath];
    for (int i = 1; i <= [dict count]; i ++) {
        NSString * key = [NSString stringWithFormat:@"Item%d", i];
        NSDictionary * temp = [dict objectForKey:key];
        NSString * flagString = [temp objectForKey:@"flag"];
        if([flagString length] > 0) {
            [_recommendArray addObject:temp];
        } else {
            [_expressArray addObject:temp];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = kBackgroundColor;
    
    [self initNavBar];
    
    [self initExpressList];
    
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = [UIColor clearColor];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = [AppUtils localizedProductString:@"QM_Text_Select_Express"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)btnBackClick:(id)sender
{
    _tableView.delegate = nil;
    _tableView.dataSource = nil;
    [AppUtils dismissHUD];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)btnOkClick:(id)sender
{
    NSString * expressCo = @""; //快递公司编号
    NSString * expressName = @""; //快递公司名字
    
    NSArray * array = [_tableView visibleCells];
    QMSelectExpressCell * recommendCell = [array objectAtIndex:0];
    QMSelectExpressCell * expressCell = [array objectAtIndex:1];
    NSArray * selectArray = recommendCell.collectionView.indexPathsForSelectedItems;
    if(selectArray && [selectArray count] > 0) {
        NSIndexPath * indexPath = [selectArray objectAtIndex:0];
        NSDictionary * dict = [_recommendArray objectAtIndex:indexPath.row];
        expressCo = [dict objectForKey:@"code"];
        expressName = [dict objectForKey:@"desc"];
    } else {
        selectArray = expressCell.collectionView.indexPathsForSelectedItems;
        if(selectArray && [selectArray count] > 0) {
            NSIndexPath * indexPath = [selectArray objectAtIndex:0];
            NSDictionary * dict = [_expressArray objectAtIndex:indexPath.row];
            expressCo = [dict objectForKey:@"code"];
            expressName = [dict objectForKey:@"desc"];
        }
    }
    
    if([expressName length] == 0) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:nil message:@"请选择物流公司" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
        return;
    } else {
        if (self.selectBlock) {
            self.selectBlock(expressCo, expressName);
        }
        
        [self btnBackClick:nil];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QMSelectExpressCell * expresscell = [tableView dequeueReusableCellWithIdentifier:kQMSelectExpressCellIdentifier];
    if(nil == expresscell) {
        expresscell = [QMSelectExpressCell cellforSelectExpress];
    }
    
    if(0 == indexPath.section) {
        [expresscell updateUI:_recommendArray];
    } else {
        [expresscell updateUI:_expressArray];
    }
    expresscell.delegate = self;
    
    [expresscell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return expresscell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section >= 2) {
        return nil;
    }
    
    CGRect screenRect = [UIScreen mainScreen].bounds;
    UIView * headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenRect.size.width, 30)];
    headView.backgroundColor = [UIColor clearColor];
    
    CGRect frame = CGRectInset(headView.bounds, 10, 0);
    UILabel * label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:12.0];
    label.textColor = UIColorFromRGB(0xa9a9a9);
    if(section == 0) {
        label.text = [AppUtils localizedProductString:@"QM_Text_Select_Express"];
    } else {
        label.text = [AppUtils localizedProductString:@"QM_Text_Express"];
    }
    [headView addSubview:label];
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) {
        return 45;
    } else {
        return 301;
    }
}

#pragma mark - QMSelectExpressDelegate

- (void)didSelectedItem:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray * array = [_tableView visibleCells];
    QMSelectExpressCell * recommendCell = [array objectAtIndex:0];
    QMSelectExpressCell * expressCell = [array objectAtIndex:1];
    if([collectionView isEqual:recommendCell.collectionView]) {
         NSArray * selectArray = expressCell.collectionView.indexPathsForSelectedItems;
        if(selectArray && [selectArray count] > 0) {
            [expressCell.collectionView deselectItemAtIndexPath:[selectArray objectAtIndex:0] animated:YES];
        }
        
        [self performSelector:@selector(btnOkClick:) withObject:nil afterDelay:0.5];
    } else {
        NSArray * selectArray = recommendCell.collectionView.indexPathsForSelectedItems;
        if(selectArray && [selectArray count] > 0) {
            [recommendCell.collectionView deselectItemAtIndexPath:[selectArray objectAtIndex:0] animated:YES];
        }
        
        [self performSelector:@selector(btnOkClick:) withObject:nil afterDelay:0.5];
    }
}

- (void)didCallBtnClick:(NSString *)phoneNum
{
    NSString * validPhoneNum = [phoneNum stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *telProtocol = nil;
    if ([self telpromptSupported])
        telProtocol = @"telprompt:";
    else
        telProtocol = @"tel:";
    
    NSURL *testURL = [NSURL URLWithString:telProtocol];
    if([[UIApplication sharedApplication] canOpenURL:testURL]) {
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", telProtocol, validPhoneNum]];
        [[UIApplication sharedApplication] openURL:telURL];
    } else {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:nil message:@"此设备不支持拨打系统电话" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
    }
}

- (BOOL)telpromptSupported
{
    static int telpromptSupported = 0;
    if (telpromptSupported == 0) {
        NSURL *testURL = [NSURL URLWithString:@"telprompt:"];
        telpromptSupported = [[UIApplication sharedApplication] canOpenURL:testURL] ? 1 : -1;
    }
    return telpromptSupported == 1;
}

@end
