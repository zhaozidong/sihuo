//
//  UIImage+Additions.h
//  QM
//
//  Copyright (c) 2011年 SNDA Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "NSData+TTAdditions.h"

@interface UIImage (Additions)

- (id)thumbnailImage:(CGSize)size;
- (id)thumbnailImage:(CGSize)size cornerRadius:(CGFloat)radius;
+ (id)thumbnailImage:(UIImage *)image size:(CGSize)size;
+ (id)thumbnailImage:(UIImage *)image size:(CGSize)size cornerRadius:(CGFloat)radius;

@end
