//
//  QMProbablyKnowCell.h
//  QMMM
//
//  Created by Derek on 15/1/20.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendInfo.h"
#import "QMRoundHeadView.h"

static NSString *QMPROBABLY=@"QMProbablyKnowCell";


@protocol QMAddNewFriendDelegate <NSObject>

-(void)addNewFriend:(FriendInfo *)friendInfo;

@end







@interface QMProbablyKnowCell : UITableViewCell


@property (strong, nonatomic) IBOutlet QMRoundHeadView *headView;

@property (strong, nonatomic) IBOutlet UILabel *lblName;

@property (strong, nonatomic) IBOutlet UILabel *lblDesc;

@property (strong, nonatomic) IBOutlet UIButton *btnNewFriend;

@property (nonatomic, weak) id<QMAddNewFriendDelegate> delegate;

+ (id)cellForProbablyKnow;

-(void)updateWithFriendInfo:(FriendInfo *)friendInfo;

@end
