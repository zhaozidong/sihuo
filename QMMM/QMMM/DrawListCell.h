//
//  DrawListCell.h
//  QMMM
//
//  Created by kingnet  on 14-9-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kDrawListCellIdentifier;

@class FinanceDataInfo;

@interface DrawListCell : UITableViewCell

+ (DrawListCell *)drawListCell;

- (void)updateUI:(FinanceDataInfo *)dataInfo;

+ (CGFloat)rowHeight;

@end
