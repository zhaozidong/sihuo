//
//  QMTopNavView.h
//  QMMM
//
//  Created by kingnet  on 14-10-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QMTopNavViewDelegate <NSObject>

- (void)filterButtonIndex:(NSInteger)index;

@end
@interface QMTopNavView : UIView

@property (nonatomic, weak) id<QMTopNavViewDelegate> delegate;

@property (nonatomic, assign) NSInteger selectedIndex; //当前选择的index

- (id)initWithFrame:(CGRect)frame titles:(NSArray *)titles images:(NSArray *)images;

- (void)setTitleNum:(int)num atIndex:(NSInteger)index;

@end
