//
//  UploadProgress.m
//  QMMM
//
//  Created by kingnet  on 15-1-20.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "UploadProgress.h"

@interface UploadProgress ()
{
    UIColor *_currentWaterColor;
    UIImageView *_errorImgV;
    UILabel *_tipLabel;
}
@end
@implementation UploadProgress
@synthesize cornerRadius = _cornerRadius;
@synthesize value = _value;
@synthesize isFailed = _isFailed;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

//初始化
- (void)initialize
{
    self.backgroundColor = [UIColor clearColor];
    _currentWaterColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    _errorImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"errorImg_bkg"]];
    _errorImgV.frame = CGRectMake((self.frame.size.width-21)/2, (self.frame.size.height-21)/2-5, 21, 21);
    [self addSubview:_errorImgV];
    
    _tipLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_errorImgV.frame), self.frame.size.width, 21)];
    _tipLabel.text = @"点击重新上传";
    _tipLabel.textColor = [UIColor whiteColor];
    _tipLabel.font = [UIFont systemFontOfSize:10.f];
    _tipLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_tipLabel];
    
    _errorImgV.hidden = YES;
    _tipLabel.hidden = YES;
}

- (void)setCornerRadius:(float)cornerRadius
{
    if (_cornerRadius != cornerRadius) {
        _cornerRadius = cornerRadius;
        self.layer.cornerRadius = _cornerRadius;
        self.layer.masksToBounds = YES;
    }
}

- (void)setValue:(float)value
{
    if(value < 0.0) value = 0.0;
    if(value > 1.0) value = 1.0;
    dispatch_async(dispatch_get_main_queue(), ^{
        _value = value;
        [self setNeedsDisplay];
    });
}

- (void)setIsFailed:(BOOL)isFailed
{
    _isFailed = isFailed;
    _errorImgV.hidden = !_isFailed;
    _tipLabel.hidden = !_isFailed;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _errorImgV.frame = CGRectMake((self.frame.size.width-21)/2, (self.frame.size.height-21)/2-5, 21, 21);
    _tipLabel.frame = CGRectMake(0, CGRectGetMaxY(_errorImgV.frame), self.frame.size.width, 21);
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGMutablePathRef path = CGPathCreateMutable();
    
    //画水
    CGContextSetLineWidth(context, 1);
    CGContextSetFillColorWithColor(context, [_currentWaterColor CGColor]);
    
    CGContextFillRect(context, CGRectMake(0, 0, rect.size.width, rect.size.height*(1.0-_value))); //画一个方框
    
    CGContextAddPath(context, path);
    CGContextFillPath(context);
    CGContextDrawPath(context, kCGPathStroke);
    CGPathRelease(path);
}


@end
