//
//  NSObject+UIWebView.m
//  QMMM
//
//  Created by kingnet  on 14-10-10.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "UIWebView+Additions.h"
#import "VersionInfo.h"

@implementation UIWebView (Additions)

- (void)loadRemoteUrl:(NSString *)url
{
    NSString *userAgent = [NSString stringWithFormat:@"Sihuo v%@ (iphone; iOS %@)", [VersionInfo currentVer], [[UIDevice currentDevice]systemVersion]];
    
    NSDictionary *dictionary = @{@"UserAgent": userAgent};
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    
    NSURL *mainURL;
    NSURLCache *urlCache = [NSURLCache sharedURLCache];
    //设置每个请求的缓存大小为5K
    [urlCache setMemoryCapacity:5*1024];
    NSString * encodingString = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    mainURL = [[NSURL alloc] initWithString:encodingString];
    
    NSMutableURLRequest *request =[NSMutableURLRequest
                                   requestWithURL:mainURL
                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                   timeoutInterval:60.0f];
    
    [request setValue:userAgent forHTTPHeaderField:@"User-Agent"];
    //从请求中获取缓存输出
    NSCachedURLResponse *response =  [urlCache cachedResponseForRequest:request];
    //判断是否有缓存
    if (response != nil){
        [request setCachePolicy:NSURLRequestReturnCacheDataDontLoad];
    }
    [self loadRequest:request];
}

- (void)removeWebViewDoubleTapGestureRecognizer
{
    [self goThroughSubViewFrom:self];
}

- (void)goThroughSubViewFrom:(UIView *)view {
    for (UIView *v in [view subviews])
    {
        if (v != view)
        {
            [self goThroughSubViewFrom:v];
        }
    }
    for (UIGestureRecognizer *reco in [view gestureRecognizers])
    {
        if ([reco isKindOfClass:[UITapGestureRecognizer class]])
        {
            if ([(UITapGestureRecognizer *)reco numberOfTapsRequired] == 2)
            {
                [view removeGestureRecognizer:reco];
            }
        }
    }
}

@end
