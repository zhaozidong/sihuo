//
//  LookLifePhotoTipView.m
//  QMMM
//
//  Created by kingnet  on 15-1-29.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "LookLifePhotoTipView.h"
#import <POP/POP.h>

@interface LookLifePhotoTipView ()
@property (weak, nonatomic) IBOutlet UILabel *tipLbl;

@end

@implementation LookLifePhotoTipView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.tipLbl.backgroundColor = [UIColor clearColor];
    self.tipLbl.numberOfLines = 9;
    self.tipLbl.textColor = [UIColor whiteColor];
}

+ (LookLifePhotoTipView *)photoTipView
{
    LookLifePhotoTipView *view = [[[NSBundle mainBundle]loadNibNamed:@"LookLifePhotoTipView" owner:self options:0] lastObject];
    view.frame = CGRectMake(kMainFrameWidth, 0, CGRectGetWidth(view.frame), kMainFrameHeight);
    return view;
}

- (void)showInView:(UIView *)inView
{
    [inView addSubview:self];
    POPSpringAnimation *posAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionX];
    posAnimation.springBounciness = 5;
    int x = kMainFrameWidth-20;
    posAnimation.toValue = @(x);
    [self.layer pop_addAnimation:posAnimation forKey:@"posAnim"];
}

- (void)dismissWithCompletion:(void(^)(void))completion
{
    [self.layer pop_removeAnimationForKey:@"posAnim"];
    
    POPBasicAnimation *alphaAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    alphaAnimation.toValue = @(0);
    alphaAnimation.completionBlock = ^(POPAnimation *anim, BOOL finish){
        if (finish) {
            [self removeFromSuperview];
            if (completion) {
                completion();
            }
        }
    };
    [self pop_addAnimation:alphaAnimation forKey:@"posAnim"];
}

@end
