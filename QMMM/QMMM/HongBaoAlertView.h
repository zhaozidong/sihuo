//
//  HongBaoAlertView.h
//  QMMM
//
//  Created by kingnet  on 15-3-26.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^HongBaoAlertBlock)(void);

@interface HongBaoAlertView : UIView

@property (nonatomic, assign) int money;

@property (nonatomic, copy) HongBaoAlertBlock alertBlock;

- (id)initWithMoney:(int)money;

- (void)show;

@end
