//
//  ActivityDetailViewController.h
//  QMMM
//  发现页的活动详情页
//  Created by kingnet  on 15-1-4.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseViewController.h"
#import "ActivityInfo.h"
@interface ActivityDetailViewController : BaseViewController

- (id)initWithUrl:(NSString *)url;

- (id)initWithEntity:(BannerEntity *)entity;

@end
