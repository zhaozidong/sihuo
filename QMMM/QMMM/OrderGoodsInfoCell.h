//
//  OrderGoodsInfoCell.h
//  QMMM
//
//  Created by kingnet  on 14-9-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class GoodEntity;

@interface OrderGoodsInfoCell : UITableViewCell
+ (OrderGoodsInfoCell *)goodsInfoCell;
- (void)updateUI:(GoodEntity *)entity;
- (void)setTotalPrice:(float)totalPrice;

+ (CGFloat)cellHeight;

@end
