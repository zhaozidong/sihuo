//
//  UserEntity.m
//  QMMM
//
//  Created by kingnet  on 14-8-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "UserEntity.h"
#import "NSString+Empty.h"

@implementation UserEntity

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        id lifePhotos = [dict objectForKey:@"life_photo"];
        if ([lifePhotos isKindOfClass:[NSString class]]) {
            self.life_photo = lifePhotos;
        }
        else if ([lifePhotos isKindOfClass:[NSArray class]]){
            [self lifePhotoStr:lifePhotos];
        }
        
        id gender = [dict objectForKey:@"gender"];
        if (gender && [gender isKindOfClass:[NSString class]]) {
            self.sex = gender;
        }
        else{
            self.sex = @"";
        }
        
        id temp = [dict objectForKey:@"uid"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.uid = temp;
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            self.uid = [temp stringValue];
        }
        
        temp = [dict objectForKey:@"username"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.username = temp;
        }
        else{
            self.username = @"";
        }
        
        temp = [dict objectForKey:@"nickname"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.nickname = temp;
        }
        else{
            self.nickname = @"";
        }
        
        temp = [dict objectForKey:@"avatar"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.avatar = temp;
        }
        else{
            self.avatar = @"";
        }
        
        temp = [dict objectForKey:@"motto"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.motto = temp;
        }
        else{
            self.motto = @"";
        }
        
        temp = [dict objectForKey:@"gender"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.sex = temp;
        }
        else{
            self.sex = @"";
        }
        
        temp = [dict objectForKey:@"mobile"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.mobile = temp;
        }
        else{
            self.mobile = @"";
        }
        
        temp = [dict objectForKey:@"location"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.location = temp;
        }
        else{
            self.location = @"";
        }
        
        temp = [dict objectForKey:@"reg_ts"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.reg_ts = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            self.reg_ts = [temp intValue];
        }
        
        temp = [dict objectForKey:@"login_ts"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.login_ts = [temp intValue];
        }
        else if(temp && [temp isKindOfClass:[NSNumber class]]){
            self.login_ts = [temp intValue];
        }
        
        temp = [dict objectForKey:@"update_ts"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.update_ts = [temp intValue];
        }
        else if(temp && [temp isKindOfClass:[NSNumber class]]){
            self.update_ts = [temp intValue];
        }
        
        temp = [dict objectForKey:@"email"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.email = temp;
        }
        else{
            self.email = @"";
        }
    }
    return self;
}

- (id)initWithUserEntity:(UserEntity *)entity
{
    self = [super init];
    if (self) {
        self.uid = entity.uid;
        self.username = entity.username;
        self.nickname = entity.nickname;
        self.avatar = entity.avatar;
        self.motto = entity.motto;
        self.sex = entity.sex;
        self.mobile = entity.mobile;
        self.location = entity.location;
        self.reg_ts = entity.reg_ts;
        self.login_ts = entity.login_ts;
        self.update_ts = entity.update_ts;
        self.life_photo = entity.life_photo;
        self.email = entity.email;
    }
    return self;
}

- (NSArray *)lifePhotos
{
    if ([self.life_photo isEqualToString:@""]) {
        return [NSArray array];
    }
    return [self.life_photo componentsSeparatedByString:@","];
}

- (void)lifePhotoStr:(NSArray *)array
{
    if (array == nil || array.count == 0) {
        self.life_photo = @"";
    }
    else{
        self.life_photo = [array componentsJoinedByString:@","];
    }
}

- (NSDictionary *)toDictionary
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.uid forKey:@"uid"];
    [dict setObject:self.username forKey:@"username"];
    [dict setObject:self.nickname forKey:@"nickname"];
    [dict setObject:self.avatar forKey:@"avatar"];
    [dict setObject:self.motto forKey:@"motto"];
    [dict setObject:self.mobile forKey:@"mobile"];
    [dict setObject:self.location forKey:@"location"];
    [dict setObject:[NSNumber numberWithInt:self.reg_ts] forKey:@"reg_ts"];
    [dict setObject:[NSNumber numberWithInt:self.login_ts] forKey:@"login_ts"];
    [dict setObject:[NSNumber numberWithInt:self.update_ts] forKey:@"update_ts"];
    [dict setObject:self.life_photo forKey:@"life_photo"];
    [dict setObject:self.sex forKey:@"sex"];
    [dict setObject:self.email forKey:@"email"];
    
    return dict;
}

- (BOOL)isFirstLogin
{
    return self.login_ts == 0;
}

@end
