//
//  ReportApi.h
//  QMMM
//
//  Created by Derek on 15/3/10.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseHandler.h"
#import "ReportInfo.h"
@interface ReportApi : BaseHandler


-(void)reportByInfo:(ReportInfo *)info Context:(contextBlock)context;



@end
