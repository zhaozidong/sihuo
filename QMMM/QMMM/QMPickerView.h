//
//  QMPickerView.h
//  QMMM
//
//  Created by kingnet  on 14-9-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PickerBlock)(NSString *string);

@interface QMPickerView : UIView
@property (nonatomic, copy) PickerBlock pickerBlock;
@property (nonatomic, strong) NSArray *dataArray; //数据源

+ (void)showWithArray:(NSArray *)dataArray inView:(UIView *)containerView atIndex:(NSInteger)atIndex completion:(PickerBlock)competion;

@end
