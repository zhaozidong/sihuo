//
//  GoodsApi.h
//  QMMM
//
//  Created by 韩芦 on 14-9-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseHandler.h"
#import "Constants.h"

@interface GoodsApi : BaseHandler

+ (GoodsApi *)shareInstance;

//获取商品列表
//dire: 拉数据方向（1上，2下)
//lastGoodsId: 最后一个ID
- (void)getRecommendGoodslist:(uint)type top:(BOOL)bTop lastGoodsId:(uint64_t)lastGoodsId context:(contextBlock)context;

//上传音频
- (void)upLoadAudio:(NSData*)data context:(contextBlock)context;

//下载音频
- (void)downloadAudio:(NSString *)audioPath context:(contextBlock)context;

//上传商品图片
- (void)uploadGoodsImage:(NSData *)data context:(contextBlock)context;

//发布商品
- (void)releaseGoodsWithDes:(NSString *)description
                      price:(float)price
                     number:(int)number
                   category:(int)category
                     photos:(NSArray *)photos
                  ori_price:(float)oriPrice
                      isNew:(int)isNew
                  audioTime:(int)audioTime
                   audioKey:(NSString *)audioKey
                   location:(NSString *)location
                    address:(NSString *)address
                    freight:(float)freight
                    context:(contextBlock)context;

//商品详情
- (void)goodsDetailsWithId:(uint64_t)goodsId prisNum:(NSInteger)prisNum context:(contextBlock)context;
//评论列表
- (void)commentListWithId:(uint64_t)goodsId offset:(uint64_t)offset context:(contextBlock)context;

//删除评论
- (void)deleteCommitWithGoodsId:(NSString *)gId commitId:(NSString *)cId context:(contextBlock)context;
//点赞
- (void)addfavor:(uint64_t)goodsId sellerId:(uint64_t)sellerId context:(contextBlock)context;

//发表评论
- (void)sendComment:(uint64_t)goodsId pid:(uint64_t)pid comments:(NSString *)comments context:(contextBlock)context;

//搜索商品
- (void)searchGoods:(NSDictionary *)dict context:(contextBlock)context;

//订单列表
- (void)getOrderList:(uint)type orderId:(NSString *)orderId context:(contextBlock)context;

//订单详情
- (void)getOrderDetail:(uint)type orderId:(NSString *)orderId context:(contextBlock)context;

//卖家发货
- (void)deliverGoods:(NSString *)orderId expressCo:(NSString *)express_co expressNum:(NSString *)express_num context:(contextBlock)context;

//评分
- (void)doScores:(NSString *)orderId scores:(CGFloat)scores comment:(NSString *)comment context:(contextBlock)context;

//发布的商品列表
//- (void)getGoodsSimpleList:()

//喜欢的商品列表
- (void)getFavorList:(NSString *)goodsId context:(contextBlock)context;

//已购买商品列表
- (void)getBoughtList:(NSString *)goodsId context:(contextBlock)context;

//更新商品信息处拉去商品信息
- (void)getGoodsInfoWithId:(uint64_t)goodsId context:(contextBlock)context;

//更新商品信息
- (void)updateGoodsWithId:(uint64_t)goodsId
                      desc:(NSString *)desc
                              price:(float)price
                             number:(int)number
                           category:(int)category
                             photos:(NSArray *)photos
                          ori_price:(float)oriPrice
                              isNew:(int)isNew
                          audioTime:(int)audioTime
                           audioKey:(NSString *)audioKey
                           location:(NSString *)location
                            address:(NSString *)address
                             freight:(float)freight
                            context:(contextBlock)context;

//删除商品
- (void)updateGoodsStaWithId:(uint64_t)goodsId type:(int)type context:(contextBlock)context;

//提交订单
- (void)submitOrder:(int)addressId goodsId:(uint64_t)goodsId paymentType:(int)paymentType num:(int)num remainPay:(float)remainPay context:(contextBlock)context;

//删除订单
- (void)deleteOrder:(NSString *)orderId type:(int)type context:(contextBlock)context;

//取消订单
- (void)cancelOrder:(NSString *)orderId context:(contextBlock)context;

//获取活动信息
- (void)getActivitiesInfo:(contextBlock)context;

//获取所有往期专题
- (void)getAllTopic:(contextBlock)context;

/**
 获取专题商品列表
 @param top 1下拉 0上拉
 @param key 分页Key，首次传0，服务器端给的
 @param tag 活动类型
 **/
- (void)getSpecialGoodsList:(uint)top key:(uint64_t)key tag:(NSString *)tag context:(contextBlock)context;

/**
 推荐商品列表
 **/
- (void)topicsGoodsList:(NSString *)requestURL context:(contextBlock)context;

/**
 获取商品分类
 **/
- (void)getCategory:(contextBlock)context;


//获取引导页商品列表
- (void)getIntroGoodsList:(contextBlock)context;

/**
 根据描述获取所属分类
 **/
- (void)getCategoryByDesc:(NSString *)desc context:(contextBlock)context;

/**
 上传单张图片带进度
 **/
- (void)uploadGoodsPhotoData:(NSData *)imgData uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))uploadProgressBlock context:(contextBlock)context;

/**************************退款相关*****************************/
/**
 卖家关闭订单并退款
 **/
- (void)sellerRefund:(NSString *)orderId context:(contextBlock)context;

/**
 买家申请退款
 **/
- (void)applyRefund:(NSString *)orderId amount:(float)amout reason:(NSString *)reason context:(contextBlock)context;

/**
 卖家同意退款
 **/
- (void)acceptRefund:(NSString *)orderId context:(contextBlock)context;

/**
 卖家拒绝退款
 **/
- (void)rejectRefund:(NSString *)orderId reason:(NSString *)reason context:(contextBlock)context;

/*****************************红包兑换商品**************************/
/**
 兑换商品
 **/
- (void)exchangeGoods:(int)num goodsId:(NSString *)goodsId addressId:(int)addressId context:(contextBlock)context;

@end
