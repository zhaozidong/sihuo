//
//  QMAllTopicCell.m
//  QMMM
//
//  Created by Derek on 15/3/19.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMAllTopicCell.h"
#import "UIImageView+QMWebCache.h"

@implementation QMAllTopicCell{
    BannerEntity *_entity;
}


+(QMAllTopicCell *)allTopicCell{
    return [[[NSBundle mainBundle] loadNibNamed:@"QMAllTopicCell" owner:self options:nil] lastObject];
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self= [[[NSBundle mainBundle] loadNibNamed:@"QMAllTopicCell" owner:self options:nil] lastObject];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
    _imgView.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    tap.numberOfTapsRequired=1;
    [_imgView addGestureRecognizer:tap];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(BannerEntity *)entity{
    _entity=entity;
    [_imgView qm_setImageWithURL:entity.imgSrc placeholderImage:nil completed:^(UIImage *image, NSError *error, QMImageCacheType cacheType, NSURL *imageURL) {
        if (error) {
            NSLog(@"加载专题图片失败");
        }
    }];
}

-(void)tap:(UIGestureRecognizer *)gesture{
    if (_delegate && [_delegate respondsToSelector:@selector(didTapTopicImg:)]) {
        [_delegate didTapTopicImg:_entity];
    }
}
@end
