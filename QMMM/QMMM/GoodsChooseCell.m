//
//  GoodsChooseCell.m
//  QMMM
//
//  Created by kingnet  on 15-3-2.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "GoodsChooseCell.h"

@interface GoodsChooseCell ()
@property (weak, nonatomic) IBOutlet UILabel *contentLbl;

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@end
@implementation GoodsChooseCell

+ (GoodsChooseCell *)chooseCell
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"GoodsChooseCell" owner:self options:0];
    return [array lastObject];
}

+ (NSArray *)leftTextArr
{
    return @[@"分       类", @"新旧程度", @"数       量", @"位       置"];
}

- (void)awakeFromNib {
    // Initialization code
    self.contentLbl.textColor = kDarkTextColor;
    self.leftLabel.textColor = kDarkTextColor;
    self.contentLbl.text = @"";
    self.leftLabel.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setContent:(NSString *)content
{
    _content = content;
    self.contentLbl.text = _content;
}

- (void)setLeftText:(NSString *)leftText
{
    _leftText = leftText;
    self.leftLabel.text = _leftText;
}

- (void)setContentColor:(UIColor *)contentColor
{
    _contentColor = contentColor;
    self.contentLbl.textColor = _contentColor;
}

@end
