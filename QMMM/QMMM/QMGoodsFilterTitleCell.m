//
//  QMGoodsFilterTitleCell.m
//  QMMM
//
//  Created by hanlu on 14-9-19.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsFilterTitleCell.h"

NSString * const kGoodsFilterTitleCellIdentifier = @"kGoodsFilterTitleCellIdentifier";

@implementation QMGoodsFilterTitleCell

+(id)cellForGoodsFilterTitleCell
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMGoodsFilterTitleCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
