//
//  QMFillExpressNumCell.h
//  QMMM
//
//  Created by Shinancao on 14/11/22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMFillExpressNumCellIdentifier;

@interface QMFillExpressNumCell : UITableViewCell

+ (id)cellForFillExpressNum;

- (void)setExpressName:(NSString *)expressName;

@end
