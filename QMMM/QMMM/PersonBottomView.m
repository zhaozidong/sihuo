//
//  PersonBottomView.m
//  QMMM
//
//  Created by kingnet  on 15-1-27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "PersonBottomView.h"
#import "UserDataInfo.h"
#import <POP/POP.h>

@interface PersonBottomView ()
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (weak, nonatomic) IBOutlet UIButton *addFriendBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonWidthCst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonLeadCst;

@end

@implementation PersonBottomView

+ (CGFloat)viewHeight
{
    return 50.f;
}

+ (PersonBottomView *)bottomView
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"PersonBottomView" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    UIColor *color = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tabbar_bg"]];
    self.backgroundColor = color;
    [self.chatBtn setTitleColor:kDarkTextColor forState:UIControlStateNormal];
    [self.chatBtn setTitle:@"私聊" forState:UIControlStateNormal];
    self.chatBtn.backgroundColor = [UIColor whiteColor];
    self.chatBtn.layer.cornerRadius = 2.f;
    self.chatBtn.layer.borderWidth = 0.5f;
    self.chatBtn.layer.borderColor = [kDarkTextColor CGColor];
    self.chatBtn.hidden = YES;
    [self.chatBtn addTarget:self action:@selector(chatAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.addFriendBtn setTitleColor:kRedColor forState:UIControlStateNormal];
    [self.addFriendBtn setTitle:@"加为好友" forState:UIControlStateNormal];
    self.addFriendBtn.backgroundColor = [UIColor whiteColor];
    self.addFriendBtn.layer.cornerRadius = 2.f;
    self.addFriendBtn.layer.borderWidth = 0.5f;
    self.addFriendBtn.layer.borderColor = [kRedColor CGColor];
    self.addFriendBtn.hidden = YES;
    [self.addFriendBtn addTarget:self action:@selector(addFriendAction:) forControlEvents:UIControlEventTouchUpInside];
    
    int width = floor((kMainFrameWidth-18)/2);
    self.buttonWidthCst.constant = width;
    [self layoutIfNeeded];
}

- (void)updateUI:(PersonRelation *)relation
{
    if (relation.isFriends) { //是一度好友
        self.chatBtn.hidden = NO;
        int leadGap = floor((kMainFrameWidth-self.buttonWidthCst.constant)/2);
        self.buttonLeadCst.constant = leadGap;
        [self layoutIfNeeded];
    }
    else if (relation.is2ndFriends){
        //二度好友
        self.chatBtn.hidden = NO;
        self.addFriendBtn.hidden = NO;
    }
}

- (void)chatAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapChat)]) {
        [self.delegate didTapChat];
    }
}

- (void)addFriendAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapAddFriend)]) {
        [self.delegate didTapAddFriend];
    }
}

- (void)showWithCompletion:(void(^)(void))completion
{
    POPBasicAnimation *decayAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    int Y = kMainFrameHeight-[[self class]viewHeight]+25;
    decayAnimation.toValue = @(Y);
    if (completion) {
        decayAnimation.completionBlock = ^(POPAnimation *anim, BOOL finish){
            if (finish) {
                completion();
            }
        };
    }
    [self.layer pop_addAnimation:decayAnimation forKey:@"decayAnim"];
}

- (void)dismissWithCompletion:(void(^)(void))completion
{
    POPBasicAnimation *decayAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionY];
    int Y = kMainFrameHeight+25;
    decayAnimation.toValue = @(Y);
    if (completion) {
        decayAnimation.completionBlock = ^(POPAnimation *anim, BOOL finish){
            if (finish) {
                completion();
            }
        };
    }
    [self.layer pop_addAnimation:decayAnimation forKey:@"decayAnim"];
}

@end
