//
//  OrderDetailPriceCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderDetailPriceCell.h"
#import "OrderDataInfo.h"

NSString * const kOrderDetailPriceCellIdentifier = @"kOrderDetailPriceCellIdentifier";

@interface OrderDetailPriceCell ()
@property (weak, nonatomic) IBOutlet UILabel *priceLbl; //订单总价

@end

@implementation OrderDetailPriceCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"OrderDetailPriceCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.priceLbl.text = @"";
    self.priceLbl.textColor = kRedColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(OrderDataInfo *)orderInfo
{
    self.priceLbl.text = [NSString stringWithFormat:@"¥ %.2f", orderInfo.payAmount];
}

+ (CGFloat)cellHeight
{
    return 45.f;
}

@end
