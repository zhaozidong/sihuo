//
//  QMNewFriendCell.m
//  QMMM
//
//  Created by hanlu on 14-10-31.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMNewFriendCell.h"
#import <CoreText/CoreText.h>
#import "MsgData.h"
#import "NSString+JSONCategories.h"

NSString * const kQMNewFriendCellIdentifier = @"kQMNewFriendCellIdentifier";

@interface QMNewFriendCell () {
    MsgData * _msgData;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statusLblWCst;
@end

@implementation QMNewFriendCell

+ (id)cellForNewFriend
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMNewFriendCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIImage * image = [UIImage imageNamed:@"ok_btn"];
    image = [image stretchableImageWithLeftCapWidth:5 topCapHeight:6];
    
    [_ctnBtn setTitle:@"接受" forState:UIControlStateNormal];
    [_ctnBtn setBackgroundImage:image forState:UIControlStateNormal];
    [_ctnBtn addTarget:self action:@selector(ctnBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [_statusLabel setText:@"已添加"];
    [_statusLabel setTextColor:UIColorFromRGB(0xb7bfc9)];
    [_statusLabel setBackgroundColor:[UIColor clearColor]];
    
    [_desclabel setTextColor:kLightBlueColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(MsgData *)msgData
{
    _msgData = msgData;
    
    NSDictionary * dict = [msgData.extData toDict];
    
    if(msgData.msgType == EMT_INVITE_FRIEND) {
        uint64_t userId = 0;
        id tmp = [dict objectForKey:@"req_uid"];
        if([tmp isKindOfClass:[NSString class]]) {
            userId = (uint64_t)[tmp longLongValue];
        } else if([tmp isKindOfClass:[NSNumber class]]) {
            userId = [tmp unsignedLongLongValue];
        }
        
        tmp = [dict objectForKey:@"req_remark"];
        if(tmp && [tmp length] > 0) {
            _nameLabel.text = tmp;
        } else {
            _nameLabel.text = [NSString stringWithFormat:@"%llu", userId];
        }
        
        tmp = [dict objectForKey:@"status"];
        if(0 == [tmp intValue]) {
            [_ctnBtn setHidden:NO];
            [_statusLabel setHidden:YES];
        } else {
            [_statusLabel setHidden:NO];
            [_ctnBtn setHidden:YES];
        }
        
        NSString * remark = @"";
        tmp = [dict objectForKey:@"by_remark"];
        if(tmp && [tmp length] > 0) {
            remark = tmp;
        }
        else {
            tmp = [dict objectForKey:@"nickname"];
            remark = tmp;
        }
        
        NSRange range = [msgData.msgContent rangeOfString:remark];
        
        //属性字符串设置属性
        NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:msgData.msgContent];
        [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(0, [attributeString length])];

        [attributeString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x5381db) range:range];
        _desclabel.attributedText = attributeString;
        self.statusLblWCst.constant = 50.f;
        [self.contentView layoutIfNeeded];
        
    } else if(msgData.msgType == EMT_ARGEE_FRIEND || msgData.msgType == EMT_CONTACT_COMING) {
        
        uint64_t userId = 0;
        id tmp = [dict objectForKey:@"uid"];
        if([tmp isKindOfClass:[NSString class]]) {
            userId = (uint64_t)[tmp longLongValue];
        } else if([tmp isKindOfClass:[NSNumber class]]) {
            userId = [tmp unsignedLongLongValue];
        }
        
        NSString *remark = [dict objectForKey:@"remark"];
        NSString *nickname = [dict objectForKey:@"nickname"];
        
        if (remark.length > 0 && nickname.length > 0) {
            NSString *s = [NSString stringWithFormat:@"%@ (%@)", remark, nickname];
            NSMutableAttributedString *attriStr = [[NSMutableAttributedString alloc]initWithString:s];
            
            [attriStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.f] range:NSMakeRange(s.length-2-nickname.length, nickname.length+2)];
            [attriStr addAttribute:NSForegroundColorAttributeName value:kLightBlueColor range:NSMakeRange(s.length-2-nickname.length, nickname.length+2)];
            _nameLabel.attributedText = attriStr;
        }
        else{
            if (remark.length > 0) {
                _nameLabel.text = remark;
            }
            else if (nickname.length > 0) {
                _nameLabel.text = nickname;
            }
        }

        _desclabel.text = msgData.msgContent;
//        _desclabel.attributedText = nil;
        _ctnBtn.hidden = YES;
        _statusLabel.hidden = YES;
        self.statusLblWCst.constant = 0.f;
        [self.contentView layoutIfNeeded];
    }
//    _nameLabel.text = @"王月月";
//    _desclabel.text = @"TA是 石晓楠 的朋友，想和你成为朋友";
//    
//    NSString * displayString = @"TA是 石晓楠 的朋友，想和你成为朋友";
//    NSRange range = [displayString rangeOfString:@"石晓楠"];
//    
//    //属性字符串设置属性
//    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:displayString];
//    [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(0, [attributeString length])];
//    [attributeString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xb7bfc9) range:NSMakeRange(0, [attributeString length])];
//    [attributeString addAttribute:NSForegroundColorAttributeName value:kRedColor range:range];
//    _desclabel.attributedText = attributeString;
}

#pragma mark - 

- (void)ctnBtnClick:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(didAgreeBtnClick:)]) {
        [_delegate didAgreeBtnClick:_msgData];
    }
}

@end
