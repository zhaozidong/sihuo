//
//  QMNoGoodsPlaceHolderView.m
//  QMMM
//
//  Created by Derek on 15/3/30.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMNoGoodsPlaceHolderView.h"


@implementation QMNoGoodsPlaceHolderView{
    UIButton *_btnInvite;
    UIButton *_btnExchange;
    UIImageView *_imgView;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(instancetype)init{
    self=[super init];
    if (self) {
        
        UIImage *image=[UIImage imageNamed:@"placeHolderInviteNew"];
        _imgView=[[UIImageView alloc] initWithImage:image];
        _imgView.frame=CGRectMake(0, 0, image.size.width, image.size.height);
        [self addSubview:_imgView];
        
        
        UIImage *imgBg=[UIImage imageNamed:@"placeHolderNoGoodsBtnBg"];
        _btnInvite=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_btnInvite setTitle:@"去接好友" forState:UIControlStateNormal];
        [_btnInvite setTitleColor:UIColorFromRGB(0xf2602d) forState:UIControlStateNormal];
        _btnInvite.titleLabel.font=[UIFont systemFontOfSize:15.0f];
        [_btnInvite setBackgroundImage:[imgBg resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)] forState:UIControlStateNormal];
        _btnInvite.layer.cornerRadius=10.0f;
        _btnInvite.tag=1;
        [_btnInvite addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btnInvite];
        
        
//        _btnExchange=[UIButton buttonWithType:UIButtonTypeRoundedRect];
//        [_btnExchange setTitle:@"兑换礼品" forState:UIControlStateNormal];
//        [_btnExchange setTitleColor:UIColorFromRGB(0xf2602d) forState:UIControlStateNormal];
//        _btnExchange.titleLabel.font=[UIFont systemFontOfSize:15.0f];
//        [_btnExchange setBackgroundImage:[imgBg resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)] forState:UIControlStateNormal];
//        _btnExchange.layer.cornerRadius=10.0f;
//        _btnExchange.tag=2;
//        [_btnExchange addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:_btnExchange];
        
        self.backgroundColor=[UIColor whiteColor];
    }
    return self;
}

-(void)layoutSubviews{
    _imgView.center=CGPointMake(self.center.x, self.frame.size.height*0.45);
    
//    _btnInvite.frame=CGRectMake((kMainFrameWidth-2*100-30)/2, CGRectGetMaxY(_imgView.frame)+20, 100, 40);
    
//    _btnExchange.frame=CGRectMake(CGRectGetMaxX(_btnInvite.frame)+30, CGRectGetMaxY(_imgView.frame)+20, 100, 40);
    
    _btnInvite.frame=CGRectMake((kMainFrameWidth-100)/2, CGRectGetMaxY(_imgView.frame)+20, 100 ,40 );
    
}

-(void)btnClick:(id)sender{
    if (_delegate && [_delegate respondsToSelector:@selector(btnDidClickNoGoods:)]) {
        [_delegate btnDidClickNoGoods:sender];
    }
}


@end
