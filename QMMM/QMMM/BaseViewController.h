//
//  BaseViewController.h
//  QMMM
//
//  Created by kingnet  on 14-8-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

/**
 向下滑动时使键盘消失，默认为YES
 **/
@property (nonatomic) BOOL canSwipCloseKeyboard;

/**
 点击空白处使键盘消失，默认为YES
 **/
@property (nonatomic) BOOL canTapCloseKeyboard;
/**
 导航栏的返回事件
 **/
- (void)backAction:(id)sender;


@end
