//
//  QMRoundHeadView.m
//  QMMM
//
//  Created by kingnet  on 14-10-22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMRoundHeadView.h"
#import "UIImageView+QMWebCache.h"

@interface QMRoundHeadView ()
@property (nonatomic, assign) float borderWidth;

@property (nonatomic, strong) UIColor *borderColor;

@property (nonatomic, assign) float cornerRadius;

@property (nonatomic, assign) float margin;
@end

@implementation QMRoundHeadView
@synthesize borderWidth = _borderWidth;
@synthesize borderColor = _borderColor;
@synthesize cornerRadius = _cornerRadius;
@synthesize margin = _margin;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _initView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self _initView];
    }
    return self;
}

- (void)_initView
{
    imageView_ = [[UIImageView alloc]initWithFrame:self.bounds];
    [self addSubview:imageView_];
    
    self.backgroundColor = [UIColor whiteColor];
    self.layer.masksToBounds = YES;
    
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer * tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headBtnClicked)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGestureRecognizer];
}

- (void)setBorderWidth:(float)borderWidth
{
    _borderWidth = borderWidth;
    self.layer.borderWidth = _borderWidth;
}

- (void)setBorderColor:(UIColor *)borderColor
{
    _borderColor = borderColor;
    self.layer.borderColor = [_borderColor CGColor];
}

- (void)setCornerRadius:(float)cornerRadius
{
    _cornerRadius = cornerRadius;
    self.layer.cornerRadius = cornerRadius;
}

- (void)setImageUrl:(NSString *)imageUrl
{
    _imageUrl = imageUrl;
    if (_imageUrl) {
        UIImage *placeholder = [UIImage imageNamed:@"headPlaceholder"];
        [imageView_ qm_setImageWithURL:_imageUrl placeholderImage:placeholder];
    }
}

- (void)setSmallImageUrl:(NSString *)smallImageUrl
{
    _smallImageUrl = smallImageUrl;
    if (_smallImageUrl) {
        NSString *smallPath = [AppUtils smallImagePath:_smallImageUrl width:80];
        UIImage *placeholder = [UIImage imageNamed:@"headPlaceholder_small"];
        if([smallPath length] > 0) {
            [imageView_ qm_setImageWithURL:smallPath placeholderImage:placeholder];
        } else {
            imageView_.image = placeholder;
        }
    }
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    imageView_.image = image;
}

- (void)setMargin:(float)margin
{
    _margin = margin;
    if (_margin > 0) {
         imageView_.frame = CGRectInset(self.bounds, _margin, _margin);
        if (_cornerRadius > 0) {
            imageView_.layer.cornerRadius = CGRectGetWidth(imageView_.frame)/2.0;
            imageView_.layer.masksToBounds = YES;
        }
    }
}

- (void)setClickEnable:(BOOL)clickEnable
{
    _clickEnable = clickEnable;
    self.userInteractionEnabled = _clickEnable;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    imageView_.frame = self.bounds;
    if (_margin > 0) {
        imageView_.frame = CGRectInset(self.bounds, _margin, _margin);
    }
}

- (void)setHeadViewStyle:(QMRoundHeadViewStyle)headViewStyle
{
    self.borderColor = kBorderColor;
    self.cornerRadius = CGRectGetWidth(self.frame)/2.0;
    
    if (headViewStyle == QMRoundHeadViewLarge) { //大头像
        CGRect frame = self.frame;
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, 70, 70);
        self.borderWidth = 2.0;
        imageView_.image = [UIImage imageNamed:@"headPlaceholder"];
    }
    else{ //小头像
        CGRect frame = self.frame;
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, 40, 40);
        self.borderWidth = 0.5;
        self.margin = 2.0;
        imageView_.image = [UIImage imageNamed:@"headPlaceholder_small"];
    }
}

- (void)headBtnClicked
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickHeadViewWithUid:)]) {
        [self.delegate clickHeadViewWithUid:self.userId];
    }
}

@end
