//
//  DrawSuccessViewController.m
//  QMMM
//
//  Created by Shinancao on 14/11/9.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "DrawSuccessViewController.h"

@interface DrawSuccessViewController ()

@end

@implementation DrawSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"提现结果";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    [self setupViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupViews
{
    UIView *view;
    if (self.isWin) {
        view = [[[NSBundle mainBundle]loadNibNamed:@"DrawWinView" owner:self options:0] lastObject];
        UILabel *label = (UILabel *)[view viewWithTag:100];
        label.numberOfLines = 2;
    }
    else{
        view = [[[NSBundle mainBundle] loadNibNamed:@"DrawSuccessView" owner:self options:0] lastObject];
    }
    [self.view addSubview:view];
    view.backgroundColor = [UIColor clearColor];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSDictionary *views = NSDictionaryOfVariableBindings(view);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-(0)-[view]-(0)-|" options:0 metrics:0 views:views]];
    float top = kMainTopHeight + 54;
    NSDictionary *metrics = @{@"top":@(top)};
    if (self.isWin) {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[view(198)]" options:0 metrics:metrics views:views]];
        UILabel *label = (UILabel *)[view viewWithTag:100];
        label.text = [NSString stringWithFormat:@"您打败了全国98%%的用户，享受到0手续费的福利，实际到账为%.2f元", self.amount];
    }
    else{
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[view(56)]" options:0 metrics:metrics views:views]];
    }
}

@end
