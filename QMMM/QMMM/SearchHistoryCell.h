//
//  SearchHistoryCell.h
//  QMMM
//
//  Created by kingnet  on 15-1-13.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * kSearchHistoryCellIdentifier;

@class KeyWordEntity;

@interface SearchHistoryCell : UITableViewCell

+ (UINib *)nib;

+ (CGFloat)cellHeight;

- (void)updateUI:(KeyWordEntity *)entity;

@end
