//
//  CommentListCell.h
//  QMMM
//
//  Created by kingnet  on 14-9-18.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsBaseTableViewCell.h"
#import "QMClickNameView.h"

@class CommentEntity;

extern NSString * const kCommentListCell;

@interface CommentListCell : UITableViewCell

@property (nonatomic, weak) id<QMClickNameViewDelegate> delegate;

@property (nonatomic, assign) BOOL isFirst;

+ (UINib *)nib;

+ (CommentListCell *)cellFromNib;

- (void)updateUI:(CommentEntity *)entity;;

@end
