//
//  GoodsDetailViewController.m
//  QMMM
//
//  Created by kingnet  on 14-10-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsDetailViewController.h"
#import "QMGoodsFavorlistCell.h"
#import "CommentListCell.h"
#import "GoodsDetailToolBar.h"
#import "GoodEntity.h"
#import "GoodsHandler.h"
#import "CommentEntity.h"
#import "YFInputBar.h"
#import "BuyGoodsViewController.h"
#import "UserDefaultsUtils.h"
#import "KL_ImagesZoomController.h"
#import "QMShareViewContrller.h"
#import "ChatViewController.h"
#import "UserInfoHandler.h"
#import "UserEntity.h"
#import "PersonPageViewController.h"
#import "ImageBrowerView.h"
#import "QMGoodsController.h"
#import "QMClearView.h"
#import "MsgHandle.h"
#import "QAudioPlayer.h"
#import "MJRefresh.h"
#import "MJRefreshConst.h"
#import "EditGoodsPage1ViewController.h"
#import "GoodDataInfo.h"
#import "NoCommentTipCell.h"
#import "QMFailPrompt.h"
#import "WaitBuyGoodsTipView.h"
#import "QMReportViewController.h"
#import "OrderDetailFooterView.h"
#import "QMQAViewController.h"

@interface GoodsDetailViewController ()<UITableViewDataSource, UITableViewDelegate, QMClickNameViewDelegate, GoodsDetailToolBarDelegate, QMGoodsControllerDelegate, YFInputBarDelegate, UIActionSheetDelegate, UIAlertViewDelegate, QMFailPromptDelegate, OrderDetailFooterViewDelegate>
{
    QMGoodsController *goodsController_;
    QMShareViewContrller *shareViewController_;
    BOOL isLoaded_;
    __block uint64_t maxId_; //评论的最大id
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic) QMGoodsFavorlistCell *goodsFavorListCell;
@property (nonatomic) CommentListCell *commentListCell;
@property (nonatomic) NoCommentTipCell *noCommentTipCell;
@property (nonatomic) GoodsDetailToolBar *toolBar;
@property (nonatomic, strong) GoodEntity *entity;
@property (nonatomic, strong) NSMutableArray *commentList;
@property (nonatomic) YFInputBar *inputBar;
@property (nonatomic, strong) CommentEntity *curTapComment; //用户点击要回复的评论
@property (nonatomic, strong) KL_ImagesZoomController *imageZoomView;

@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@property (nonatomic, strong) QMFailPrompt *failPrompt;

@end

@implementation GoodsDetailViewController

- (id)initWithGoodsId:(uint16_t)goodsId
{
    self = [super init];
    if (self) {
        self.goodsId = goodsId;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = [AppUtils localizedProductString:@"QM_Text_Detail"];
    self.navigationItem.leftBarButtonItems  = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(moreAction:) title:nil size:CGSizeMake(44.f, 44.f) imageName:@"more_btn"];
    
    self.commentList = [NSMutableArray array];
    
    [self setupViews];
    [self addRefreshFooterView];
    [self getGoodsDetails];
    [self getCommentList];
    
    isLoaded_ = NO;
    maxId_ = 0;
    
    //统计浏览商品次数
    [AppUtils trackCustomEvent:@"browse_goods_event" args:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    _tableView.delegate = nil;
    _tableView.dataSource = nil;
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(oneTapHandler:) name:kShowImageOneTapNotity object:nil];
   
    //注册键盘通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[QAudioPlayer sharedInstance] stopPlay];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kShowImageOneTapNotity object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kNotNetworkErrorNotity object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!isLoaded_) {
        isLoaded_ = YES;
        if (self.needScrollToComments) {
            [self.inputBar becomeFirstResponder];
            self.needScrollToComments = NO;
        }
    }
}


#pragma mark -
- (void)getGoodsDetails
{
    [AppUtils showProgressMessage:@"加载中"];
    GoodsDetailViewController __weak *wealSelf = self;
   
    [[GoodsHandler shareGoodsHandler]goodsDetailsWithId:self.goodsId prisNum:10 success:^(id obj) {
        [AppUtils dismissHUD];
        wealSelf.entity = (GoodEntity *)obj;
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
        [wealSelf loadGoodsInfoFailed:obj];
    }];
}

- (void)setEntity:(GoodEntity *)entity
{
    if (entity && !_entity) {
        _entity = entity;
        self.tableView.hidden = NO;
        goodsController_ = [[QMGoodsController alloc] initWithGoodsInfo:nil];
        goodsController_.delegate = self;
        [goodsController_ setShowFull:YES];
        [goodsController_ setGoodsInfo:_entity];
        
        //刷新点赞
        [self.goodsFavorListCell updateUI:_entity];
        
        //刷新商品信息
        [self.tableView reloadData];
        
        UserEntity *user = [UserInfoHandler sharedInstance].currentUser;
        //当是自己时
        if (_entity.sellerUserId == [user.uid longLongValue]) {
            [self.toolBar removeFromSuperview];
            CGRect frame = self.tableView.frame;
            self.tableView.frame = CGRectMake(frame.origin.x, frame.origin.y, CGRectGetWidth(frame), CGRectGetHeight(self.view.frame));
//            self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(moreAction:) title:nil size:CGSizeMake(44.f, 44.f) imageName:@"more_btn"];
        }
        else{
            self.toolBar.hidden = NO;
            //判断购买按钮是否可用
            if (_entity.number == 0) {
                self.toolBar.canPurchase = NO;
            }
            else if (_entity.number - _entity.lockNum == 0){ //待售罄
                //添加提示
                WaitBuyGoodsTipView *tipView = [WaitBuyGoodsTipView tipView];
                [tipView showWithMaxY:(kMainFrameHeight-50) inView:self.view completion:^{
                    tipView.tipStr = @"";
                }];
                
                self.toolBar.canPurchase = NO;
                
                //tableview底部加上一个insets
                UIEdgeInsets insets = self.tableView.contentInset;
                self.tableView.contentInset = UIEdgeInsetsMake(insets.top, insets.left, [WaitBuyGoodsTipView viewHeight], insets.right);
            }
        }

        //下载音频
        [[GoodsHandler shareGoodsHandler]downloadAudio:_entity.msgAudio complete:nil];
    }
}

- (void)loadGoodsInfoFailed:(NSString *)tip
{
    //拿到错误代码
    NSRange range = [tip rangeOfString:@"(" options:NSBackwardsSearch];
    if (range.location != NSNotFound) {
        NSString *str = [tip substringFromIndex:range.location+1];
        NSString *str1 = [str substringToIndex:str.length-1];
        if ([str1 intValue] == 3064 || [str1 intValue] == 3068) {//商品被删除了
            NSString *tip = @"";
            if ([str1 intValue] == 3064) {
                tip = @"商品已经被删除啦";
            }
            else if ([str1 intValue] == 3068){
                tip = @"商品已经被下架啦";
            }
            self.failPrompt =[[QMFailPrompt alloc]initWithFailType:QMFailTypeLoadFail labelText:tip  buttonType:QMFailButtonTypeNone buttonText:nil];
            self.failPrompt.frame = CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
            [self.view addSubview:self.failPrompt];
            
            [self performSelector:@selector(backAction:) withObject:nil afterDelay:1.0];
        }
    }
    //超时或异常
    else{
        self.failPrompt =[[QMFailPrompt alloc]initWithFailType:QMFailTypeLoadFail labelText:@"哎呦，加载失败了" buttonType:QMFailButtonTypeNone buttonText:nil];
        self.failPrompt.delegate = self;
        self.failPrompt.frame = CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
        [self.view addSubview:self.failPrompt];
    }
}

//
- (void)getCommentList
{
    GoodsDetailViewController __weak *weakSelf = self;
    [[GoodsHandler shareGoodsHandler]commentListWithId:self.goodsId offset:maxId_ success:^(id obj) {
        [weakSelf stopRefreshing];
        NSDictionary *dict = (NSDictionary *)obj;
        NSArray *models = [dict objectForKey:@"list"];
        maxId_ = [[dict objectForKey:@"maxId"] unsignedLongLongValue];
        [weakSelf addComments:models];
    } failed:^(id obj) {
        [weakSelf stopRefreshing];
    }];
}

- (void)addComments:(NSArray *)comments
{
    if (comments.count >0) {
        [_commentList addObjectsFromArray:comments];
        
        [self.tableView reloadData];
        
        if (self.needScrollToComments) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:NSNotFound inSection:2] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
    }
    else{
        [self removeFooterView];
    }
}

#pragma mark - View

- (void)setupViews
{
    [self.view addSubview:[self tableView]];
    
    [self.view addSubview:[self toolBar]];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(_toolBar);
    NSArray *contraits = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[_toolBar]-0-|" options:0 metrics:0 views:views];
    [self.view addConstraints:contraits];
    
    contraits = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_toolBar]-0-|" options:0 metrics:0 views:views];
    [self.view addConstraints:contraits];
    
    self.tableView.hidden = YES;
    self.toolBar.hidden = YES;
    
    OrderDetailFooterView *footer = [OrderDetailFooterView footerView];
    footer.frame = CGRectMake(0, 0, CGRectGetWidth(_tableView.frame), [OrderDetailFooterView viewHeight]);
    footer.delegate = self;
    _tableView.tableFooterView = footer;
    
    //添加键盘上部的输入框
    [self.view addSubview:[self inputBar]];
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-50.f) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[CommentListCell nib] forCellReuseIdentifier:kCommentListCell];
        [_tableView registerNib:[QMGoodsFavorlistCell nib] forCellReuseIdentifier:kQMGoodsFavorlistCellIndentifier];
        [_tableView registerNib:[QMClearView nib] forCellReuseIdentifier:@"kFooterIdentifier"];
        UINib *nib = [UINib nibWithNibName:@"WhiteTableViewCell" bundle:nil];
        [_tableView registerNib:nib forCellReuseIdentifier:@"kWhiteTableViewCell"];
    }
    return _tableView;
}

- (YFInputBar *)inputBar
{
    if (!_inputBar) {
        _inputBar = [[YFInputBar alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY([UIScreen mainScreen].bounds), CGRectGetWidth([UIScreen mainScreen].bounds), 50.f)];
        _inputBar.delegate = self;
        _inputBar.clearInputWhenSend = NO;
        _inputBar.resignFirstResponderWhenSend = YES;
    }
    return _inputBar;
}

- (GoodsDetailToolBar *)toolBar
{
    if (!_toolBar) {
        _toolBar = [GoodsDetailToolBar toolBar];
        _toolBar.translatesAutoresizingMaskIntoConstraints = NO;
        _toolBar.delegate = self;
    }
    return _toolBar;
}

- (QMGoodsFavorlistCell *)goodsFavorListCell
{
    if (!_goodsFavorListCell) {
        _goodsFavorListCell = [self.tableView dequeueReusableCellWithIdentifier:kQMGoodsFavorlistCellIndentifier];
        _goodsFavorListCell.delegate = self;
    }
    return _goodsFavorListCell;
}

- (CommentListCell *)commentListCell
{
    if (!_commentListCell) {
        _commentListCell = [CommentListCell cellFromNib];
        _commentListCell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _commentListCell.frame = CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), CGRectGetHeight(_commentListCell.frame));
    }
    return _commentListCell;
}

- (NoCommentTipCell *)noCommentTipCell
{
    if (!_noCommentTipCell) {
        _noCommentTipCell = [NoCommentTipCell tipCell];
    }
    return _noCommentTipCell;
}

- (void)removeFooterView
{
    [self stopRefreshing];
    [self.tableView removeFooter];
}

- (void)addRefreshFooterView
{
    __weak typeof(self) weakSelf = self;
    [self.tableView addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRereshing];
    }];
}

- (void)footerRereshing
{
    [self getCommentList];
}

- (void)stopRefreshing
{
    if ([self.tableView.footer isRefreshing]) {
        [self.tableView.footer endRefreshing];
    }
}

//是自己时的更多button
- (void)moreAction:(id)sender
{
    UserEntity *user = [UserInfoHandler sharedInstance].currentUser;
    if (self.entity.sellerUserId == [user.uid longLongValue]) {//是自己的商品显示编辑和删除
        NSString *str = @"下架";
        if (self.entity.status == 0) { //被下架了
            str = @"上架";
        }
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"编辑", str, @"删除", nil];
        actionSheet.destructiveButtonIndex = 2;
        actionSheet.tag=1000;
        [actionSheet showInView:self.view];
    }else{//别人的商品显示举报
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"举报", nil];
        actionSheet.tag = 1001;
        [actionSheet showInView:self.view];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.cancelButtonIndex == buttonIndex) {
        return;
    }
    if (actionSheet.tag==1000) {
        if (buttonIndex == 0) {
                [self editGoods];//编辑
        }
        else if (buttonIndex == 1){
            if(self.entity.status == -1) {
                return;
            }
            if(self.entity.status == 1) {
                //下架
                UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:[AppUtils localizedProductString:@"QM_Alert_DropGoods"] message:nil delegate:self cancelButtonTitle:[AppUtils localizedCommonString:@"QM_Button_Cancel"] otherButtonTitles:[AppUtils localizedCommonString:@"QM_Button_OK"], nil];
                alertView.tag = 1001;
                [alertView show];
            }
            else{
                //上架
                [self dropGoods:NO];
            }
        }
        else if (buttonIndex == 2){
            //删除
            [self deleteGoods];
        }
    }else if (actionSheet.tag==1001){
        if (buttonIndex==0){//举报
            QMReportViewController *report=[[QMReportViewController alloc] initWithGoodsId:[NSString stringWithFormat:@"%llu",self.goodsId] andUserId:[NSString stringWithFormat:@"%llu",self.entity.sellerUserId]];
            [self.navigationController pushViewController:report animated:YES];
        }
    }
}

- (void)editGoods
{
    __weak typeof(self) weakSelf = self;
    EditGoodsPage1ViewController *controller = [[EditGoodsPage1ViewController alloc]initWithType:QMUpdateGoods];
    controller.goodsId = self.goodsId;
    controller.updateGoodsBlock = ^(GoodEntity *entity){
        [weakSelf.entity assignValue:entity];
        [weakSelf.tableView reloadData];
    };
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)dropGoods:(BOOL)bDrop
{
    GoodsSimpleInfo *goods = [[GoodsSimpleInfo alloc]init];
    goods.gid = self.entity.gid;
    goods.desc = self.entity.desc;
    goods.photo = self.entity.picturelist.rawArray[0];
    goods.price = self.entity.price;
    goods.publish_time = self.entity.pub_time;
    goods.update_time = self.entity.update_time;
    goods.num = self.entity.number;
    goods.orig_price = self.entity.orig_price;
    goods.status = self.entity.status;
    [[GoodsHandler shareGoodsHandler] dropGoodsWithId:goods bdrop:bDrop success:^(id obj) {
        if(bDrop) {
            [AppUtils showSuccessMessage:@"下架完成"];
        } else {
            [AppUtils showSuccessMessage:@"上架完成"];
        }
        [self performSelector:@selector(backAction:) withObject:nil afterDelay:1.0];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)deleteGoods
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:[AppUtils localizedProductString:@"QM_Alert_DeleteGoods"] message:nil delegate:self cancelButtonTitle:[AppUtils localizedCommonString:@"QM_Button_Cancel"] otherButtonTitles:@"确定", nil];
    alertView.tag = 1000;
    [alertView show];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex == buttonIndex) {
        return;
    }
    if (alertView.tag == 1000) {
        //删除该商品
        [AppUtils showProgressMessage:@"正在提交"];
        [[GoodsHandler shareGoodsHandler]deleteGoodsWithId:self.goodsId success:^(id obj) {
            [AppUtils showSuccessMessage:@"删除完成"];
            //回到上一个界面
            [self performSelector:@selector(backAction:) withObject:nil afterDelay:1.0];
        } failed:^(id obj) {
            [AppUtils showErrorMessage:(NSString *)obj];
        }];
    } else if(alertView.tag == 1001) {
        //下架
        [self dropGoods:YES];
    }
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.inputBar resignFirstResponder];
}

#pragma mark - UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [goodsController_ tableView:tableView heightForRowAtIndexPath:indexPath];
    }
    else if (indexPath.section == 1){
//        return [QMGoodsFavorlistCell cellHeight];
        return [QMGoodsFavorlistCell heightWithGoodsEntity:self.entity];
    }
    else{
        if (self.commentList.count==0){
            if (indexPath.row == 0) {
                return 44.f;
            }
            else{
                return 5.f;
            }
        }
        else if (indexPath.row == [self.commentList count]) {
            return 5.f;
        }
        else{
            CommentEntity *comment = [self.commentList objectAtIndex:indexPath.row];
            [self.commentListCell updateUI:comment];
            [self.commentListCell setNeedsLayout];
            [self.commentListCell layoutIfNeeded];
            CGFloat height = [self.commentListCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            height += 1;
            return height;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return [goodsController_ tableView:tableView numberOfRowsInSection:section];
    }
    else if (section == 1){
        return self.entity.favorCounts == 0 ? 0 : 1;
    }
    else{
        if (self.entity.favorCounts>0 || self.commentList.count>0) {
           return [self.commentList count]+1;
        }
        else if (self.commentList.count==0 ){
            return 2; //一个cell是底部的blank cell，一个cell是提示cell
        }
        else{
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [goodsController_ tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    else if (indexPath.section == 1){
        return self.goodsFavorListCell;
    }
    else{
        if (self.commentList.count==0) {
            if (indexPath.row == 0) {
                if (self.entity.favorCounts <= 0) {
                    self.noCommentTipCell.tip = @"还没有人来评论/赞，快来抢沙发呀~";
                }
                else{
                    self.noCommentTipCell.tip = @"还没有人来评论，快来抢沙发呀~";
                }
                return self.noCommentTipCell;
            }
            else if (indexPath.row == 1){
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"kWhiteTableViewCell"];
                return cell;
            }
        }
        else{
            if (indexPath.row == [self.commentList count]) {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"kWhiteTableViewCell"];
                return cell;
            }
            else{
                CommentListCell *cell = [tableView dequeueReusableCellWithIdentifier:kCommentListCell];
                CommentEntity *comment = [self.commentList objectAtIndex:indexPath.row];
                [cell updateUI:comment];
                cell.delegate = self;
                
                cell.isFirst = (indexPath.row == 0);
                return cell;
            }
        }
    }
    return nil;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //删除本地数据
        NSString *title=[self tableView:_tableView titleForDeleteConfirmationButtonForRowAtIndexPath:indexPath];
        
        if ([title isEqualToString:@"删除"]) {
            __weak typeof(self) weakSelf=self;
            CommentEntity *comment = [self.commentList objectAtIndex:indexPath.row];
            [[GoodsHandler shareGoodsHandler] deleteCommit:[NSString stringWithFormat:@"%llu",comment.cid] goodsId:[NSString stringWithFormat:@"%llu",_entity.gid] success:^(id obj) {
                [AppUtils showSuccessMessage:@"删除成功"];
                NSLog(@"删除评论成功");
                [self.commentList removeObjectAtIndex:indexPath.row];
                _entity.commentCounts --;
                [[GoodsHandler shareGoodsHandler] decreaseCommentCount:_entity.gid];
                [weakSelf.tableView reloadData];
                return;
            } failed:^(id obj) {
                [AppUtils showErrorMessage:@"删除失败"];
                NSLog(@"删除评论失败 commitId=%llu,goodsId=%llu",comment.cid,_entity.gid);
            }];
        }else{
            [self.tableView setEditing:NO animated:YES];
            CommentEntity *comment = [self.commentList objectAtIndex:indexPath.row];
            QMReportViewController *report=[[QMReportViewController alloc] initWithUserId:[NSString stringWithFormat:@"%llu",comment.cUserId]];
            [self.navigationController pushViewController:report animated:YES];
        }
    }
}


-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section>1 && self.commentList.count>0) {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserEntity *user = [UserInfoHandler sharedInstance].currentUser;
    if ([user.uid longLongValue]==_entity.sellerUserId) {//自己的商品全部可以删除
        return  @"删除";
    }else{//别人的商品
        //如果是我的评论就显示删除，否则显示举报
        if (self.commentList && self.commentList.count>0) {
            CommentEntity *comment = [self.commentList objectAtIndex:indexPath.row];
            if (comment.cUserId==[user.uid longLongValue]) {
                return  @"删除";
            }else{
                return @"举报";
            }
        }
    }
    return nil;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    
    
}



#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1) {
        return CGFLOAT_MIN;
    }
    
    return 8.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"kFooterIdentifier"];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2) { //评论列表
        if (self.commentList.count > 0 && indexPath.row < self.commentList.count) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            self.curTapComment = [self.commentList objectAtIndex:indexPath.row];
            NSString *placeholder = [NSString stringWithFormat:@"回复%@:", self.curTapComment.cNickname];
            self.inputBar.textView.placeholder = placeholder;
            [self.inputBar becomeFirstResponder];
        }
    }
}

#pragma mark - Send Comments
- (void)inputBar:(YFInputBar *)inputBar sendBtnPress:(UIButton *)sendBtn withInputString:(NSString *)str
{
    NSString *temp = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (str && ![str isKindOfClass:[NSNull class]] && str.length>0 && ![temp isEqualToString:@""]) {
        [AppUtils showProgressMessage:@"正在发送"];
        NSString *content = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        uint64_t pid = 0;
        if (self.curTapComment) {//是回复
            content = [NSString stringWithFormat:@"%@", str];
            pid = self.curTapComment.cid;
        }
        GoodsDetailViewController __weak *weakSelf = self;
        YFInputBar __weak *weakInputBar = inputBar;
        __block YFInputBar *blockBar = weakInputBar;
        [[GoodsHandler shareGoodsHandler]sendComment:self.goodsId pid:pid comments:content success:^(id obj) {
            [AppUtils showSuccessMessage:@"发送成功"];
            _entity.commentCounts ++;
            
            //在最后插入一条评论
            [weakSelf insertOneComment:(CommentEntity *)obj];
            [blockBar clearText];
        } failed:^(id obj) {
            [AppUtils showErrorMessage:(NSString *)obj];
        }];
    }
    else{
        [AppUtils showAlertMessage:@"评论的内容不能为空"];
        return;
    }
}

- (void)insertOneComment:(CommentEntity *)entity
{
    if (entity) {
        [self.commentList addObject:entity];
        [self.tableView reloadData];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.commentList.count-1 inSection:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

#pragma mark - NSNotification Handler
- (void)oneTapHandler:(NSNotification *)notification
{
    self.navigationController.navigationBarHidden = NO;
    GoodsDetailViewController __weak *weakSelf = self;
    
    [UIView animateWithDuration:0.2 animations:^{
        weakSelf.imageZoomView.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [weakSelf.imageZoomView removeFromSuperview];
            weakSelf.imageZoomView = nil;
        }
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    if (!_tapGesture) {
        _tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
        _tapGesture.numberOfTapsRequired = 1;
    }
    [self.view addGestureRecognizer:_tapGesture];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    [self.view removeGestureRecognizer:_tapGesture];
}

- (void)tapAction:(UITapGestureRecognizer *)gesture
{
    [self.inputBar resignFirstResponder];
}

#pragma mark - QMClickNameView Delegate
- (void)clickNameViewWithUid:(uint64_t)userId
{
    PersonPageViewController *personController = [[PersonPageViewController alloc]initWithUserId:userId];
    [self.navigationController pushViewController:personController animated:YES];
}

#pragma mark - GoodsDetailToolbar Delegate
- (void)didTapChat
{
    NSString *title = [NSString stringWithFormat:@"%qu", _entity.sellerUserId];
    if([title length] > 0 && ![title isEqualToString:[UserInfoHandler sharedInstance].currentUser.uid]) {
        
        //模拟一条消息写到消息数据库
        [[MsgHandle shareMsgHandler] sendGoodsPreviewMsg:title goodsInfo:_entity];
        
        ChatViewController *chatController = [[ChatViewController alloc] initWithChatter:title];
        if([_entity.sellerNick length] > 0) {
            chatController.title = _entity.sellerNick;
        }
        [self.navigationController pushViewController:chatController animated:YES];
    }
}

- (void)didTapPurchase
{
    UserEntity *user = [UserInfoHandler sharedInstance].currentUser;
    NSString *sellerId = [NSString stringWithFormat:@"%qu", self.entity.sellerUserId];
    if ([user.uid isEqualToString:sellerId]) {
        [AppUtils showAlertMessage:[AppUtils localizedProductString:@"QM_HUD_Tip_SelfGoods"]];
    }
    else{
        //统计点击购买
        [AppUtils trackCustomEvent:@"purchase_click_event" args:nil];
        
        BuyGoodsViewController *controller = [[BuyGoodsViewController alloc]initWithGoodsID:self.entity.gid];

        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - GoodsDetailControl Delegate
//点击商品图像
- (void)didGoodsImageClick:(QMGoodsController *)controller curIndex:(NSInteger)index
{
    [self.inputBar resignFirstResponder];
    self.navigationController.navigationBarHidden = YES;
    if (!self.imageZoomView) {
        self.imageZoomView = [[KL_ImagesZoomController alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height )imgViewSize:CGSizeZero];
    }
    self.imageZoomView.alpha = 0.0;
    [self.view addSubview:self.imageZoomView];
    GoodsDetailViewController __weak *weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        weakSelf.imageZoomView.alpha = 1.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [weakSelf.imageZoomView updateImageDate:[weakSelf imageArr] smallImgs:[weakSelf smallImageArr] selectIndex:index];
        }
    }];
}

//goods图片地址
- (NSArray *)imageArr {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (NSString *urlStr in self.entity.picturelist.rawArray) {
        [arr addObject:[AppUtils getAbsolutePath:urlStr]];
    }
    return arr;
}

- (NSArray *)smallImageArr {
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for (NSString *urlStr in self.entity.picturelist.rawArray) {
        NSString *temp = [AppUtils thumbPath:urlStr sizeType:QMImageOneSize];
        [arr addObject:[AppUtils getAbsolutePath:temp]];
    }
    return arr;
}

//点击用户头像
- (void)didGoodsHeadImageClick:(QMGoodsController *)controller userId:(uint64_t)userId
{
    [AppUtils closeKeyboard];
    PersonPageViewController *personController = [[PersonPageViewController alloc]initWithUserId:userId];
    [self.navigationController pushViewController:personController animated:YES];
}

//点击赞
- (void)didGoodsFavorClick:(QMGoodsController *)controller
{
    //修改数据源
    uint64_t userId = (uint64_t)[[UserInfoHandler sharedInstance].currentUser.uid longLongValue];
    [_entity addFavor:userId];
    
    [self.goodsFavorListCell updateUI:_entity];
    
    //刷新UI
    [_tableView reloadData];
    
    //网络请求
    [[GoodsHandler shareGoodsHandler] addFavor:controller.goodsInfo.gid sellerId:controller.goodsInfo.sellerUserId];
}

//点击评论
- (void)didGoodsCommentClick:(QMGoodsController *)controller
{
    self.curTapComment = nil;
    self.inputBar.textView.placeholder = @"输入文字信息";
    [self.inputBar becomeFirstResponder];
}

//点击分享
- (void)didGoodsShareClick:(QMGoodsController *)controller
{
    [self.inputBar resignFirstResponder];
    if(nil == shareViewController_) {
        shareViewController_ = [[QMShareViewContrller alloc] init];
    }
    [shareViewController_ setGoodsInfo:controller.goodsInfo];
    [shareViewController_ show];
}

#pragma mark - QMFailPromptDelegate
- (void)placeHolderButtonClick:(id)sender
{
    [self.failPrompt removeFromSuperview];
    //重新请求一次数据
    [self getGoodsDetails];
    [self getCommentList];
}

#pragma mark - OrderDetailFooterView Delegate
- (void)didTapGuarantBtn
{
    //跳转到QA界面
    QMQAViewController *controller = [[QMQAViewController alloc]initWithType:QMQATypeHowToBuy];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
