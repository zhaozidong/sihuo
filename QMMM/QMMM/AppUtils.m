//
//  AppUtils.m
//  QMMM
//
//  Created by kingnet  on 14-8-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "AppUtils.h"
#import <CommonCrypto/CommonDigest.h>
#import "NSString+URLEncode.h"
#import "APIConfig.h"
#import "SVProgressHUD.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <sys/utsname.h>
#import "CloNetworkUtil.h"
#import "NSDate+Category.h"
#import "UserDefaultsUtils.h"
#import <AdSupport/AdSupport.h>

@implementation AppUtils

/******************* 获取本地化文本内容 *************/
+(NSString *)localizedCommonString:(NSString *)key
{
    return [[NSBundle mainBundle]localizedStringForKey:key value:nil table:@"Common"];
}

+(NSString *)localizedProductString:(NSString *)key
{
    return [[NSBundle mainBundle]localizedStringForKey:key value:nil table:@"Product"];
}

+(NSString *)localizedPersonString:(NSString *)key
{
    return [[NSBundle mainBundle]localizedStringForKey:key value:nil table:@"Person"];
}

/****************** System Utils ******************/
+ (NSString *)md5FromString:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (int)strlen(cStr), result); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

+ (NSString *)signWithDictionary:(NSDictionary *)dictionary method:(NSString *)method
{
    //将key排序
    NSArray *keys = [dictionary allKeys];
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSComparisonResult result = [obj1 compare:obj2];
        return result;
    }];
    //拼接key-value
    NSMutableString *msString = [[NSMutableString alloc] initWithString:@""];
    for (NSString *key in sortedArray) {
        if ([msString isEqualToString:@""]) {
            [msString appendFormat:@"%@=%@", key, [dictionary objectForKey:key]];
        }
        else{
            [msString appendFormat:@"&%@=%@", key, [dictionary objectForKey:key]];
        }
    }

    NSString *string = [[method stringByAppendingString:msString] stringByAppendingString:kAppKey];
    string = [string urlencode];
    string = [[self class]md5FromString:string];
    
    return string;
}

+ (void)closeKeyboard
{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}

+ (void)showAlertMessage:(NSString *)msg
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[[self class]localizedCommonString:@"QM_Tip_Title"] message:msg delegate:self cancelButtonTitle:[[self class]localizedCommonString:@"QM_Button_IKnow"] otherButtonTitles:nil, nil];
    [alertView show];
}

+ (void)hideStatusBar:(BOOL)hidden viewController:(UIViewController *)viewController
{
//    if ([viewController respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
//        // iOS 7
//        [viewController performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
//    } else {
//        // iOS 6
//        [[UIApplication sharedApplication] setStatusBarHidden:hidden withAnimation:UIStatusBarAnimationSlide];
//    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:hidden withAnimation:UIStatusBarAnimationSlide];
}

+(NSArray *)createBackButtonWithTarget:(id)target selector:(SEL)selector
{
    UIButton *barButton = [UIButton buttonWithType:UIButtonTypeCustom];
    barButton.frame = CGRectMake(0, 0, 44.f, 44.f);
    UIImage *image = [UIImage imageNamed:@"back_btn"];
    [barButton setImage:image forState:UIControlStateNormal];
    [barButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]initWithCustomView:barButton];
    
    NSArray *items = @[negativeSpacer, barButtonItem];
    return items;
}

+(NSArray *)createRightButtonWithTarget:(id)target selector:(SEL)selector title:(NSString *)title size:(CGSize)size imageName:(NSString *)imageName
{
    UIButton *barButton = [UIButton buttonWithType:UIButtonTypeCustom];
    barButton.frame = CGRectMake(0, 0, size.width, size.height);
    if (imageName && ![imageName isEqualToString:@""]) {
        UIImage *image = [UIImage imageNamed:imageName];
        [barButton setImage:image forState:UIControlStateNormal];
        UIImage *onImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_on", imageName]];
        if (onImage) {
            [barButton setImage:onImage forState:UIControlStateHighlighted];
        }
    }
    
    if (title!=nil && ![title isEqualToString:@""]) {
        [barButton setTitle:title forState:UIControlStateNormal];
    }
    barButton.titleLabel.font = [UIFont fontWithName:kFontName size:17.f];
    [barButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [barButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]initWithCustomView:barButton];
    
    NSArray *items = @[negativeSpacer, barButtonItem];
    return items;
}

+(UILabel *)titleLable:(NSString *)title
{
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , 130, 50)];
    titleLabel.backgroundColor = [UIColor clearColor];  //设置Label背景透明
    titleLabel.font = [UIFont boldSystemFontOfSize:18];  //设置文本字体与大小
    titleLabel.textColor = kRedColor;  //设置文本颜色
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;  //设置标题
    
    return titleLabel;
}

+ (NSAttributedString *)placeholderAttri:(NSString *)placeholder
{
    UIFont *font = [UIFont systemFontOfSize:15];
    return [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: kPlaceholderColor, NSFontAttributeName:font}];
}

+ (NSDictionary *)systemInfo
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    NSString *osver = [[UIDevice currentDevice]systemVersion];
    [dic setObject:osver forKey:@"osver"];
//    NSString *udid = [UIDevice currentDevice].advertisingIdentifier.UUIDString;
    NSString *udid = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    [dic setObject:udid forKey:@"did"];
    NSString *appver = [[NSBundle mainBundle]objectForInfoDictionaryKey:@"CFBundleVersion"];
    [dic setObject:appver forKey:@"appver"];
    NSString *deviceType = [[self class]platformType];
    [dic setObject:deviceType forKey:@"model"];
    [dic setObject:@"apple" forKey:@"mrf"];
    CGRect frame = [[UIScreen mainScreen]bounds];
    CGFloat scale_screen = [UIScreen mainScreen].scale;
    NSString *res = [NSString stringWithFormat:@"%d*%d", (int)(frame.size.width*scale_screen), (int)(frame.size.height*scale_screen)];
    [dic setObject:res forKey:@"res"];
    CTTelephonyNetworkInfo *netInfo = [[CTTelephonyNetworkInfo alloc]init];
    CTCarrier*ctcarrier = [netInfo subscriberCellularProvider];
    NSString *carrier = [ctcarrier carrierName];
    if (carrier == nil) {
        carrier = @"";
    }
    [dic setObject:carrier forKey:@"carrier"];
    CloNetworkUtil *networkUtil = [[CloNetworkUtil alloc] init];
    NSString *nettype = [networkUtil getNetWorkType];
    [dic setObject:nettype forKey:@"nettype"];
   
    return dic;
}


+ (NSString *) platformType
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}

/********************* Verification Utils **********************/
+ (BOOL)checkPhoneNumber:(NSString *)phoneNumber{
    
    NSString * MOBILE = @"^1\\d{10}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    BOOL res1 = [regextestmobile evaluateWithObject:phoneNumber];
    return res1;
}

+ (BOOL)checkExpressNumber:(NSString *)expressNumber{
    NSString *expressRegEx = @"^[a-zA-Z0-9-]{8,30}$";
    NSPredicate *expressText = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", expressRegEx];
    return [expressText evaluateWithObject:expressNumber];
}

+ (BOOL)emailValid:(NSString *)text
{
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    return [emailTest evaluateWithObject:text];
}

+ (NSString *)getAbsolutePath:(NSString *)path
{
    if (path && [path isKindOfClass:[NSString class]]) {
        NSString *host = [UserDefaultsUtils valueWithKey:kCdnhost];
        return [host stringByAppendingString:path];
    }
    else{
        return path;
    }
}

+ (NSString *)smallImagePath:(NSString *)path width:(int)width
{
    if (nil == path) {
        return @"";
    }
    NSString *temp = path;
    NSRange range = [temp rangeOfString:@"." options:NSBackwardsSearch];
    if(range.location != NSNotFound) {
        NSString *prefix = [temp substringToIndex:range.location];
        NSString *formate = [NSString stringWithFormat:@"_%dx%d", width, width];
        NSString *suffix = [formate stringByAppendingString:[temp substringFromIndex:range.location]];
        NSString *smallPath = [prefix stringByAppendingString:suffix];
        return smallPath;
    } else {
        return @"";
    }
}

+ (NSString *)thumbPath:(NSString *)path sizeType:(QMImageSizeType)sizeType
{
//    float width = [UIScreen mainScreen].bounds.size.width * [UIScreen mainScreen].scale;
    float width = [UIScreen mainScreen].bounds.size.width;
    if (sizeType == QMImageHalfSize) {
        float w = width/2;
        return [self smallImagePath:path width:w];
    }
    else if(sizeType == QMImageQuarterSize){
        float w = width/4;
        return [self smallImagePath:path width:w];
    }
    else if (sizeType == QMImageOneSize){
        float w = width;
        return [self smallImagePath:path width:w];
    }
    //是不规定的类型返回原path
    else{
        return path;
    }
}

/********************* Cookie Utils ****************************/
+ (void)saveCookies:(NSDictionary *)headerFields
{
    NSString *host = [@"http://" stringByAppendingString:[[self class]serverHost]];
    NSArray *cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:headerFields forURL:[NSURL URLWithString:host]];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    [cookieStorage setCookies:cookies forURL:[NSURL URLWithString:host] mainDocumentURL:nil];
    NSData *cookiesData = [NSKeyedArchiver archivedDataWithRootObject: [cookieStorage cookies]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject: cookiesData forKey: @"sessionCookies"];
    [defaults synchronize];
}

+ (void)loadCookies
{
    NSData *cookiesdata = [[NSUserDefaults standardUserDefaults] objectForKey: @"sessionCookies"];
    if ([cookiesdata length]) {
        NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData: cookiesdata];
        NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for (NSHTTPCookie *cookie in cookies){
            [cookieStorage setCookie: cookie];
        }
    }
}

+ (void)clearCookies
{
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *each in cookieStorage.cookies) {
        [cookieStorage deleteCookie:each];
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"sessionCookies"];
    [defaults synchronize];
}

+ (NSString *)getTokenValue
{
    [[self class]loadCookies];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    if ([[cookieStorage cookies] count]>0) {
        for (NSHTTPCookie *each in cookieStorage.cookies) {
            if ([each.name isEqualToString:@"token"]) {
                return each.value;
            }
        }
    }
    return @"";
}

/********************* SVProgressHUD **********************/
+ (void)showSuccessMessage:(NSString *)message
{
//    [GiFHUD dismiss];
    [SVProgressHUD setOffsetFromCenter:UIOffsetMake(0, -64)];
    [SVProgressHUD showSuccessWithStatus:message];
}

+ (void)showErrorMessage:(NSString *)message
{
//    [GiFHUD dismiss];
    [SVProgressHUD setOffsetFromCenter:UIOffsetMake(0, -64)];
    [SVProgressHUD showErrorWithStatus:message];
}

+ (void)showProgressMessage:(NSString *) message
{
    [SVProgressHUD setOffsetFromCenter:UIOffsetMake(0, -64)];
    [SVProgressHUD showWithStatus:message maskType:SVProgressHUDMaskTypeClear];

//    [GiFHUD setGifWithImageName:@"loading.gif"];
//    [GiFHUD show];
}

+ (void)dismissHUD
{
    [SVProgressHUD dismiss];
//    [GiFHUD dismiss];
}

/*********************** Format date **********************/
+(NSString *)stringFromDate:(NSDate *)date
{
    // 规则：
    // 0当天则只显示时间: NSDateFormatterShortStyle    下午4:52
    // 1昨天
    // 2星期 （只显示最近四天的）
    // 3超过的则显示 NSDateFormatterShortStyle 11-9-17
    
    //[d timeIntervalSinceNow];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // NSDateFormatterShortStyle    下午4:52
    // kCFDateFormatterMediumStyle  下午4:53:23
    // kCFDateFormatterLongStyle    格林尼治标准时间+0800下午4时55分03秒
    // kCFDateFormatterFullStyle    中国标准时间下午4时55分43秒
    //[dateFormatter setTimeStyle:kCFDateFormatterFullStyle];
    // NSDateFormatterShortStyle 11-9-17
    // NSDateFormatterMediumStyle 2011-9-17
    // NSDateFormatterLongStyle 2011年9月17日
    // NSDateFormatterFullStyle 2011年9月17日星期六
    //[dateFormatter setDateStyle:NSDateFormatterFullStyle];
    
    // 注意与语言不是一码事，指的是区域设置
//    NSLocale *curLocale = [NSLocale currentLocale];
//    [dateFormatter setLocale:curLocale];// 设置为当前区域
//    
//    NSInteger seconds = [NSDate secondsBetweenDate:date andDate:[NSDate date]];
//    if(seconds < 60) {
//        return @"刚刚";
//    } else if(seconds < 60 * 60) {
//        return [NSString stringWithFormat:@"%d分钟前", (int)(seconds / 60)];
//    } else if(seconds < 60 * 60 * 24) {
//        return [NSString stringWithFormat:@"%d小时前", (int)(seconds / (60 * 60))];
//    } else if(seconds < 60 * 60 * 24 * 3) {
//        return [NSString stringWithFormat:@"%d天前", (int)(seconds / (60 * 60 * 24))];
//    } else {
//        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//        return [dateFormatter stringFromDate:date];
//    }
    return [date formattedTime];
}

+(NSString *)stringForOrderDate:(NSDate *)date
{
    NSLocale *curLocale = [NSLocale currentLocale];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:curLocale];// 设置为当前区域
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [dateFormatter stringFromDate:date];
}

+(NSString *)stringForPublishDate:(NSDate *)date
{
    NSLocale *curLocale = [NSLocale currentLocale];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:curLocale];// 设置为当前区域
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter stringFromDate:date];
}

+(NSString *)stringForUserLibraryPath
{
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    return [pathArray objectAtIndex:0];
}

+(NSString *)stringForUserDocumentPath
{
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [pathArray objectAtIndex:0];
}

#pragma mark - Save Image
+(NSString *)getTempPath:(NSString*)filename{
    
    NSString *tempPath = NSTemporaryDirectory();
    
    return [tempPath stringByAppendingPathComponent:filename];
}

//返回path
+ (NSString *)saveData:(NSData *)imgData imgName:(NSString *)imgName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dirPath = [[self class] getTempPath:@"cacheImg"];
    BOOL isDir = NO;
    BOOL isDirExist = [fileManager fileExistsAtPath:dirPath isDirectory:&isDir];
    if (!(isDirExist && isDir)) {
        isDirExist = [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    if (isDirExist) {
        NSString *filePath = [[self class] getTempPath:[NSString stringWithFormat:@"cacheImg/%@", imgName]];
        if([fileManager createFileAtPath:filePath contents:imgData attributes:nil]){
            return filePath;
        }
        else{
            return @"";
        }
    }
    else{
        return @"";
    }
}

//获取Cache目录的路径
+ (NSString *)getCacheDirectory
{
    NSArray *arrPaths=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath=[[arrPaths objectAtIndex:0] stringByAppendingString:@"/goods_audio/"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:cachePath]) {
        NSError *error=nil;
        [[NSFileManager defaultManager] createDirectoryAtPath:cachePath withIntermediateDirectories:YES attributes:nil error:&error];
    }
    return cachePath;
}

//获取文件名
+ (NSString *)getAudioFileName:(NSString *)strPath{
    if(strPath && [[strPath substringFromIndex:[strPath length]-3]isEqualToString:@"m4a"]){
        return [strPath lastPathComponent];
    }
    return nil;
}

//获取域名
+ (NSString *)serverHost
{
#ifdef DEBUG
    return @"test.sihuo.com";
#else
    if (kIsTestRelease) {
        return @"test.sihuo.com";
    }
    else{
        return @"sihuo.com";
    }
#endif
}

+ (NSString *)uploadServerHost
{
#ifdef DEBUG
    return @"test.sihuo.com";
#else
    if (kIsTestRelease) {
        return @"test.sihuo.com";
    }
    else{
        return @"upload.sihuo.com";
    }
#endif
}

//MTA统计
+ (void)trackCustomEvent:(NSString *)event args:(NSArray *)args
{
#ifndef DEBUG
    if (!kIsTestRelease) {
        if (args) {
            [MTA trackCustomEvent:event args:args];
        }
        else{
            [MTA trackCustomEvent:event args:[NSArray arrayWithObject:@"arg0"]];
        }
    }
#endif
}

+ (void)trackCustomKeyValueEvent:(NSString *)event props:(NSDictionary *)props
{
#ifndef DEBUG
    if (!kIsTestRelease) {
        if (props && [props isKindOfClass:[NSDictionary class]]) {
            [MTA trackCustomKeyValueEvent:event props:props];
        }
    }
#endif
}

/********************** Get Freight *******************/
+ (float)freightForAddress:(NSString *)address
{
    NSDictionary *dict = @{@"浙江":@(10), @"江苏":@(10), @"上海":@(10), @"安徽":@(12), @"四川":@(12), @"黑龙江":@(12), @"陕西":@(12), @"天津":@(12), @"山东":@(12), @"河南":@(12), @"山西":@(12), @"云南":@(12), @"福建":@(12), @"贵州":@(12), @"青海":@(12), @"河北":@(12), @"甘肃":@(12), @"吉林":@(12), @"江西":@(12), @"北京":@(12), @"重庆":@(12), @"辽宁":@(12), @"广东":@(12), @"湖南":@(12), @"宁夏":@(12), @"海南":@(12), @"湖北":@(12)};
    if (address && address.length > 0) {
        NSString *subStr = [address substringToIndex:2];
        NSNumber *price = [dict objectForKey:subStr];
        if (price) {
            return [price floatValue];
        }
    }
    return 20.f;
}

//拿到mm:ss
+ (NSArray *)getTimeWithInterval:(NSTimeInterval )timeInterval{
    NSInteger ti = (NSInteger)timeInterval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    NSArray *resultArr = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%02li", (long)hours],[NSString stringWithFormat:@"%02li", (long)minutes], [NSString stringWithFormat:@"%02li", (long)seconds], nil];
    return resultArr;
}

/*********************** encode decode *************************************/
+( NSString *)DisEncodeUTF8ToChina:(NSString *)encodeStr
{
    return [encodeStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

+( NSString *)EncodeChinaToUTF8:(NSString *)encodeStr
{
    return [[NSString stringWithFormat:@"%@",encodeStr]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

/********************** System Call ****************************************/
+ (void)systemCall:(NSString *)phoneNum
{
    NSString * validPhoneNum = [phoneNum stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *telProtocol = nil;
    if ([self telpromptSupported])
        telProtocol = @"telprompt:";
    else
        telProtocol = @"tel:";
    
    NSURL *testURL = [NSURL URLWithString:telProtocol];
    if([[UIApplication sharedApplication] canOpenURL:testURL]) {
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", telProtocol, validPhoneNum]];
        [[UIApplication sharedApplication] openURL:telURL];
    } else {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:nil message:@"此设备不支持拨打系统电话" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
    }
}

+ (BOOL)telpromptSupported
{
    static int telpromptSupported = 0;
    if (telpromptSupported == 0) {
        NSURL *testURL = [NSURL URLWithString:@"telprompt:"];
        telpromptSupported = [[UIApplication sharedApplication] canOpenURL:testURL] ? 1 : -1;
    }
    return telpromptSupported == 1;
}

@end
