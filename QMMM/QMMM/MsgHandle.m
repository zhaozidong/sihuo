//
//  MsgHandle.m
//  QMMM
//
//  Created by hanlu on 14-10-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MsgHandle.h"
#import "MsgApi.h"
#import "QMDataCenter.h"
#import "UserInfoHandler.h"
#import "UserEntity.h"
#import "QMFriendHelper.h"
#import "GoodEntity.h"
#import "NSDictionary+JSONCategories.h"
#import "GoodsChatHistoryDao.h"

@interface MsgHandle () {
    MsgApi * _msgApi;
    QMMsgHelper * _msgHelper;
    QMFriendHelper * _friendHelper;
}

@end

@implementation MsgHandle

static MsgHandle* _shareInstance = NULL;

+ (MsgHandle *)shareMsgHandler
{
    @synchronized([MsgHandle class]) {
        if (nil == _shareInstance) {
            _shareInstance = [[MsgHandle alloc] init];
        }
        return _shareInstance;
    }
}

- (id)init
{
    self = [super init];
    if(self) {
        _msgApi = [[MsgApi alloc] init];
        
        _msgHelper = [QMDataCenter sharedDataCenter].msgHelper;
        
        _friendHelper = [QMDataCenter sharedDataCenter].friendHelper;
    }
    return self;
}

- (void)loadLocalMsgList:(contextBlock)context msgType:(MsgBigType)type
{
    [_msgHelper asyncLoadMsgList:^(id result) {
        if(context) {
            context(result);
        }
    } type:type];
}

//获取系统消息
- (void)getMsgList
{
    __block uint preMsgCount = 0;
    __block NSString * keyname = [NSString stringWithFormat:@"%@_preMsgCount", [UserInfoHandler sharedInstance].currentUser.uid];
    NSNumber * num = [[NSUserDefaults standardUserDefaults] objectForKey:keyname];
    if(num) {
        preMsgCount = [num unsignedIntValue];
    }
    
    __weak MsgHandle * weakMsgHandle = self;
    
    //发起请求
    [_msgApi getMsgList:preMsgCount context:^(id result) {
        if(result && [result isKindOfClass:[NSDictionary class]]) {
            if(0 == [[result objectForKey:@"s"] intValue]) {
                id temp = [result objectForKey:@"d"];
                if(temp && [temp isKindOfClass:[NSArray class]]) {
                    
                    BOOL bHasFriensMsg = NO;
                    BOOL bHasWalletMsg = NO;
                    BOOL bHasTradeMsg = NO;
                    
                    NSMutableArray * ret = [[NSMutableArray alloc] init];
                    for (NSDictionary * dict in temp) {
                        MsgData * info = [[MsgData alloc] initWithDictionary:dict];
                        [ret addObject:info];
                        
                        if(info.msgType == EMT_ARGEE_FRIEND || info.msgType == EMT_CONTACT_COMING || info.msgType == EMT_INVITE_FRIEND) {
                            bHasFriensMsg = YES;
                        }
                        
                        if (info.msgType == EMT_SELLER_GET_MONEY) {
                            bHasWalletMsg = YES;
                        }
                        
                        if (info.msgType == EMT_GOODS_PAYED || info.msgType == EMT_GOODS_DELIVERED || info.msgType == EMT_GOODS_RECEIVED) {
                            bHasTradeMsg = YES;
                        }
                    }
                    
                    //存储count
                    [[NSUserDefaults standardUserDefaults] setObject:@([ret count]) forKey:keyname];
                    
                    //数据库
                    if([ret count] > 0) {
                        
                        //消息入库
                        [_msgHelper asyncAddMsgList:^(id obj) {
                            //UI
                            [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshMsgListNotify object:nil];
                            
                            //未读
                            [[NSNotificationCenter defaultCenter] postNotificationName:kFlushMsgUnreadCountNotify object:nil userInfo:nil];
                        } list:ret];
                        
                        if(bHasFriensMsg) {
                             [[NSNotificationCenter defaultCenter] postNotificationName:kFriendListChangedNotify object:nil];
                        }
                        if (bHasWalletMsg) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:kWalletMsgNotify object:nil];
                        }
                        if (bHasTradeMsg) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:kTradeMsgNotify object:nil];
                        }
                    }
                    
                    //删除消息
                    uint deleteCount = (uint)[ret count] + preMsgCount;
                    if(deleteCount > 0) {
                        [weakMsgHandle deleteMsg:deleteCount success:^(id obj) {
                            if([obj boolValue]) {
                                [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:keyname];
                            }
                        } failed:^(id obj) {
                            [[NSUserDefaults standardUserDefaults] setObject:@(deleteCount) forKey:keyname];
                        }];
                    }
                }
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:kGetMsgListFailedNotify object:result[@"d"]];
            }
        }
    }];
}

//获取评论与赞消息的未读数
- (void)getFavorAndCommentUnreadCount:(contextBlock)context
{
    [_msgHelper asyncGetMsgUnreadCountList:^(id result) {
        if(context) {
            context(result);
        }
    }];
}

//获取卖出商品后钱包的钱增加的消息
- (void)getWalletUnreadCount:(contextBlock)context
{
    [_msgHelper asyncGetWalletUnreadCount:^(id result) {
        if (context) {
            context(result);
        }
    }];
}

//更新未读状态
- (void)updateMsgUnreadStatus:(contextBlock)context msgType:(MsgBigType)type unread:(BOOL)unread
{
    [_msgHelper asyncUpdateMsgUnreadStatusByType:nil type:type unRead:unread];
}

//删除消息
- (void)deleteMsg:(uint)preMsgCount success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [_msgApi deleteMsg:preMsgCount context:^(id result) {
        if(0 == [[result objectForKey:@"s"] intValue]) {
            id temp = [result objectForKey:@"d"];
            if([temp isKindOfClass:[NSNumber class]]) {
                if([temp boolValue]) {
                    if(success) {
                        success(@(YES));
                    }
                } else {
                    if(failed) {
                        failed(@(NO));
                    }
                }
            }
        } else {
            if(failed) {
                failed(@(NO));
            }
        }
    }];
}

////检测是否有系统消息未读
//- (void)checkSystemMsgUnreadCount:(contextBlock)context
//{
//    if([[QMDataCenter sharedDataCenter] isStarted]) {
//        [self getSystemMsgList:1 success:^(id success) {
//            if([success count] > 0) {
//                //通知刷新
//                [[NSNotificationCenter defaultCenter] postNotificationName:kReloadSystemMsgListNotify object:nil];
//                
//                //通知刷新未读数
//                NSDictionary * dict = @{@"systemMsgunreadCount":@(1)};
//                [[NSNotificationCenter defaultCenter] postNotificationName:kFlushMsgUnreadCountNotify object:nil userInfo:dict];
//            }
//            
//            if(context) {
//                context(success);
//            }
//            
//        } failed:^(id failed) {
//            
//        }];
//    }
//}

//检测本地是否有未读消息
- (void)checkLocalSystemMsgUnreadCount:(contextBlock)context
{
    if([[QMDataCenter sharedDataCenter] isStarted]) {
        [_msgHelper asyncGetMsgUnreadCount:^(id result) {
            if(context) {
                context(result);
            }
        }];
    }
}

//发送商品预览消息
- (void)sendGoodsPreviewMsg:(NSString *)chatter goodsInfo:(GoodEntity*)goodsInfo
{
    //判断是否已经发送过
    GoodsChatHistoryDao *dao = [GoodsChatHistoryDao sharedInstance];
    if ([dao isHaveGoodsId:goodsInfo.gid]) {
        return;
    }
    NSString *selfId = [UserInfoHandler sharedInstance].currentUser.uid;
    
    if([chatter length] > 0 && ![chatter isEqualToString:selfId]) {
/**********预览商品的cell不用自动出现在买家的界面了*****************/
//        id  chatManager = [[EaseMob sharedInstance] chatManager];
//        EMChatText *chatText = [[EMChatText alloc] initWithText:@""];
//        EMTextMessageBody *textBody = [[EMTextMessageBody alloc] initWithChatObject:chatText];
//        EMMessage *message = [[EMMessage alloc] initWithReceiver:chatter bodies:@[textBody]];
//        [message setFrom:selfId];
//        [message setIsGroup:NO];
//        [message setIsReadAcked:YES];
//        [message setTo:chatter];
        
//        NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
//        NSString *messageID = [NSString stringWithFormat:@"%.0f", interval];
//        UInt64 t = (UInt64)([[NSDate date] timeIntervalSince1970]*1000);
//        NSString *messageID = [NSString stringWithFormat:@"%llu", t];
//        [message setMessageId:messageID];
        
        //扩展消息
        NSDictionary * params = @{@"gid":@(goodsInfo.gid), @"desc":goodsInfo.desc, @"pic":[goodsInfo.picturelist.rawArray objectAtIndex:0], @"price":[NSString stringWithFormat:@"%.2f", goodsInfo.price], @"oid":@""};
        
//        NSDictionary * dict = @{@"tpl":@(EMT_CUSTOM_GOODS), @"arg":[params toString]};
//        [message setExt:dict];
//        
//        [chatManager importMessage:message
//                       append2Chat:YES];
        
        [self sendGoodsMsg:chatter extData:[params toString]];
        [dao insertGoodsId:goodsInfo.gid];
    }
}

//发送商品消息
- (EMMessage *)sendGoodsMsg:(NSString *)chatter extData:(NSString *)extMessage
{
    EMMessage * retMessage = nil;
    
    NSString *selfId = [UserInfoHandler sharedInstance].currentUser.uid;
    
    if([chatter length] > 0 && ![chatter isEqualToString:selfId] && [extMessage length] > 0) {
        EMChatText *chatText = [[EMChatText alloc] initWithText:@""];
        EMTextMessageBody *textBody = [[EMTextMessageBody alloc] initWithChatObject:chatText];
        EMMessage *message = [[EMMessage alloc] initWithReceiver:chatter bodies:@[textBody]];
        [message setFrom:selfId];
        [message setIsGroup:NO];
        [message setIsReadAcked:YES];
        [message setTo:chatter];
        
//        NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
//        NSString *messageID = [NSString stringWithFormat:@"%.0f", interval];
        UInt64 t = (UInt64)([[NSDate date] timeIntervalSince1970]*1000);
        NSString *messageID = [NSString stringWithFormat:@"%llu", t];
        [message setMessageId:messageID];
        
        //扩展消息
        NSDictionary * dict = @{@"tpl":@(EMT_GOODS_SEND), @"arg":extMessage};
        [message setExt:dict];
        
        retMessage = [[EaseMob sharedInstance].chatManager asyncSendMessage:message progress:nil];
    }
    
    return retMessage;
}


//发送防诈骗信息
- (EMMessage *)sendFraudMsg:(NSString *)chatter extData:(NSString *)extMessage{
    
    EMMessage * retMessage = nil;
    
    NSString *selfId = [UserInfoHandler sharedInstance].currentUser.uid;
    
    if([chatter length] > 0 && ![chatter isEqualToString:selfId] && [extMessage length] > 0) {
        EMChatText *chatText = [[EMChatText alloc] initWithText:@""];
        EMTextMessageBody *textBody = [[EMTextMessageBody alloc] initWithChatObject:chatText];
        EMMessage *message = [[EMMessage alloc] initWithReceiver:chatter bodies:@[textBody]];
        [message setFrom:selfId];
        [message setIsGroup:NO];
        [message setIsReadAcked:YES];
        [message setTo:chatter];
        
        //扩展消息
        NSDictionary * dict = @{@"tpl":@(EMT_RECEIVE_FRAUD_TEXT), @"arg":extMessage};
        [message setExt:dict];
        
        retMessage = [[EaseMob sharedInstance].chatManager asyncSendMessage:message progress:nil];
        
    }
    
    
    return retMessage;
}



@end
