//
//  FriendInfo.m
//  QMMM
//
//  Created by hanlu on 14-11-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "FriendInfo.h"
#import "FMResultSet.h"
#import "QMPinYin.h"

@implementation FriendInfo

- (id)initWithFMResultSet:(FMResultSet *)resultSet
{
    self = [super init];
    if(self) {
        _uid = [resultSet unsignedLongLongIntForColumnIndex:0];
        _nickname = [resultSet stringForColumnIndex:1];
        _icon = [resultSet stringForColumnIndex:2];
        _cellphone = [resultSet stringForColumnIndex:3];
        _pinyin = [resultSet stringForColumnIndex:4];
        _remark = [resultSet stringForColumnIndex:5];
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self && dict) {
        id tmp = [dict objectForKey:@"uid"];
        if([tmp isKindOfClass:[NSString class]]) {
            _uid = (uint64_t)[tmp longLongValue];
        } else if([tmp isKindOfClass:[NSNumber class]]) {
            _uid = [tmp unsignedLongLongValue];
        }
        
        tmp = [dict objectForKey:@"remark"];
        if([tmp isKindOfClass:[NSString class]]) {
            _remark = tmp;
        } else {
            _remark = @"";
        }
        
        tmp = [dict objectForKey:@"nickname"];
        if ([tmp isKindOfClass:[NSString class]]) {
            _nickname = tmp;
        }
        else{
            _nickname = @"";
        }
        
        tmp = [dict objectForKey:@"avatar"];
        if([tmp isKindOfClass:[NSString class]]) {
            _icon = tmp;
        } else {
            _icon = @"";
        }
        
        tmp = [dict objectForKey:@"mobile"];
        if([tmp isKindOfClass:[NSString class]]) {
            _cellphone = tmp;
        } else {
            _cellphone = @"";
        }
        
        tmp = [dict objectForKey:@"tips"];
        if([tmp isKindOfClass:[NSString class]]) {
            _tips = tmp;
        } else {
            _tips = @"";
        }
        
        if (_remark.length > 0) {
            self.showname = _remark;
        }
        else{
            self.showname = _nickname;
        }
        self.pinyin = [QMPinYin getPinYin:_showname hasSymbol:YES];
    }
    return self;
    
}

- (void)updateRemark:(NSString *)remark
{
    _remark = remark;
    if (_remark.length > 0) {
        self.showname = _remark;
    }
    else{
        self.showname = _nickname;
    }
    self.pinyin = [QMPinYin getPinYin:_showname hasSymbol:YES];
}

@end

@implementation FriendSectionInfo

- (id)initWithSection:(NSString *)letter pos:(NSUInteger)pos length:(NSUInteger)length
{
    self = [super init];
    if(self) {
        _letter = letter;
        _pos = pos;
        _length = length;
    }
    return self;
}

@end
