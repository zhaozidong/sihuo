//
//  UploadImg.m
//  Channel
//
//  Created by deling on 12-11-27.
//  Copyright (c) 2012年 zn. All rights reserved.
//

#import "UploadImg.h"
#import "QMHttpClient.h"

@implementation UploadImg
@synthesize viewController;
@synthesize popController;
@synthesize image;

-(void)uploadImg:(UIViewController *)vc uploadBlock:(UploadBlock)uploadBlock
{
    self.viewController = vc;
    self.uploadBlock = uploadBlock;
    //在这里呼出下方菜单按钮项
    UIActionSheet *myActionSheet = [[UIActionSheet alloc]
                                    initWithTitle:[AppUtils localizedCommonString:@"QM_Title_PhotoIntro"]
                                    delegate:self
                                    cancelButtonTitle:[AppUtils localizedCommonString:@"QM_Button_Cancel"] destructiveButtonTitle:nil
                                    otherButtonTitles: [AppUtils localizedCommonString:@"QM_Text_TakePhoto"], [AppUtils localizedCommonString:@"QM_Text_ChooseFromAlbum"],nil];

    [myActionSheet showInView:viewController.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //呼出的菜单按钮点击后的响应
    if (buttonIndex == actionSheet.cancelButtonIndex)
    {
        return ;
    }
    switch (buttonIndex)
    {
        case 0:  //打开照相机拍照
            [self performSelector:@selector(takePhoto) withObject:nil afterDelay:0.3];
            break;
        case 1:  //打开本地相册
            [self openAlbum];
            break;
    }
}

//开始拍照
-(void)takePhoto
{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            //设置拍照后的图片可被编辑
            picker.allowsEditing = YES;
            picker.sourceType = sourceType;
        
        if ([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            [self.viewController presentViewController:picker animated:YES completion:nil];
        }
        else{
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
            self.popController = popover;
             [self.popController presentPopoverFromRect:CGRectMake(0, 0, 300, 300) inView:viewController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        }else
        {
            NSLog(@"模拟其中无法打开照相机,请在真机中使用");
    }
}

//打开图片库
- (void)openAlbum
{
    
    self.photoPickController = [[UIImagePickerController alloc] init];
    
    self.photoPickController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    self.photoPickController.delegate = self;
    
    //设置选择后的图片可被编辑

    self.photoPickController.allowsEditing = YES;
    
    if ([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self.viewController presentViewController:self.photoPickController animated:YES completion:nil];
    }
    else{
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:self.photoPickController];
        self.popController = popover;
        [self.popController presentPopoverFromRect:CGRectMake(0, 0, 300, 300) inView:viewController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController1 animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    viewController1.navigationItem.title = [AppUtils localizedCommonString:@"QM_Text_ChoosePhoto"];
    if ([navigationController.viewControllers count]>1) {
        if (viewController1.navigationItem.leftBarButtonItems == nil) {
            viewController1.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(back:)];
        }
    }
    else{
        viewController1.navigationItem.leftBarButtonItems = nil;
    }
    viewController1.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(cancel:) title:[AppUtils localizedCommonString:@"QM_Button_Cancel"] size:CGSizeMake(44.f, 44.f) imageName:nil];
}

- (void)cancel:(id)sender
{
    [self.photoPickController dismissViewControllerAnimated:YES completion:nil];
}

- (void)back:(id)sender
{
    [self.photoPickController popViewControllerAnimated:YES];
}

//当选择一张图片后进入这里
-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *chooseType = [info objectForKey:UIImagePickerControllerMediaType];
    
    //当选择的类型是图片
    
    if ([chooseType isEqualToString:@"public.image"])
        
    {
        //获得视图白框里的内容
        UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
        if (img == nil) {
            img = [info objectForKey:UIImagePickerControllerOriginalImage];
        }
        NSData *rawData = UIImageJPEGRepresentation(img, 0.1);
        image = [UIImage imageWithData:rawData];
        
//        CGSize s = CGSizeMake(70.0, 70.0);
//        image = [self scaleToSize:image size:s];
        //关闭相册界面
        //[picker dismissModalViewControllerAnimated:YES];
        if ([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            [picker dismissViewControllerAnimated:YES completion:nil];
        }
        else{
            [self.popController dismissPopoverAnimated:YES];
        }
        
        self.uploadBlock(image);
    }
    else{
        [AppUtils showErrorMessage:@"只支持图片类型"];
    }
}
- (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size{
    //创建一个bitmap的上下文，并把他设成当前正使用的上下文
    UIGraphicsBeginImageContextWithOptions(size, YES, 0.0);
    //绘制改变大小的图片
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    //从当前的上下文创建一个改变大小的后的图片
    UIImage *scaledImage=UIGraphicsGetImageFromCurrentImageContext();
    //使当前的上下文 出堆栈
    UIGraphicsEndImageContext();
    //返回新的改变大小后的图片
    return scaledImage;
}


#pragma mark - Save Photo
-(NSString *)getTempPath:(NSString*)filename{
    
    NSString *tempPath = NSTemporaryDirectory();
    
    return [tempPath stringByAppendingPathComponent:filename];
}

//返回path
- (NSString *)saveData:(NSData *)imgData imgName:(NSString *)imgName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *dirPath = [self getTempPath:@"cacheImg"];
    BOOL isDir = NO;
    BOOL isDirExist = [fileManager fileExistsAtPath:dirPath isDirectory:&isDir];
    if (!(isDirExist && isDir)) {
        isDirExist = [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    if (isDirExist) {
        NSString *filePath = [self getTempPath:[NSString stringWithFormat:@"cacheImg/%@", imgName]];
        if([fileManager createFileAtPath:filePath contents:imgData attributes:nil]){
            return filePath;
        }
        else{
            return @"";
        }
    }
    else{
        return @"";
    }
}


@end
