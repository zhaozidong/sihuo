//
//  QMChatUserHelper.m
//  QMMM
//
//  Created by hanlu on 14-10-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMFriendHelper.h"
#import "QMSqlite.h"
#import "FriendInfo.h"
#import "QMDatabaseHelper.h"

@implementation QMFriendHelper

- (void)setWorkThread:(NSThread *)aThread
{
    @synchronized(self) {
        if (_workThread != aThread) {
            _workThread = aThread;
        }
    }
}

- (NSArray *)loadFriendList:(FMDatabase *)database
{
    NSMutableArray * list = [[NSMutableArray alloc] initWithCapacity:1];
    
    if(database) {
        @autoreleasepool {
            FMResultSet * resultSet = [database executeQuery:@"SELECT * FROM qmFriendList;"];
            while ([resultSet next]) {
                FriendInfo * info = [[FriendInfo alloc] initWithFMResultSet:resultSet];
                [list addObject:info];
            }
        }
    }
    
    return list;

}

- (void)addFriendList:(FMDatabase *)database list:(NSArray *)userList
{
    if(database && [userList count] > 0) {
         NSArray * array = [NSArray arrayWithArray:userList];
        for (FriendInfo * data in array) {
            [self updateFriend:database userInfo:data];
        }
    }
}


- (void)updateFriend:(FMDatabase *)database userInfo:(FriendInfo *)userInfo
{
    if(database && userInfo) {
        [database executeUpdateWithFormat:@"REPLACE INTO qmFriendList (userId, nickname, icon, cellphone, pinyin, remark) VALUES (%qu, %@, %@, %@, %@, %@);", userInfo.uid, userInfo.nickname, userInfo.icon, userInfo.cellphone, userInfo.pinyin, userInfo.remark];
    }
}

- (void)deleteFriend:(FMDatabase *)database userId:(uint64_t)userId
{
    if(database && userId) {
        [database executeUpdateWithFormat:@"DELETE FROM qmFriendList WHERE userId = %qu", userId];
    }
}

- (void)deleteAllFriend:(FMDatabase *)database
{
    if(database) {
        [database executeUpdate:@"DELETE FROM qmFriendList;"];
    }
}

- (void)asyncLoadFriendList:(contextBlock)context
{
    if(_workThread && context) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:bContext, nil];
        [self performSelector:@selector(threadLoadFriendList:)
                     onThread:_workThread
                   withObject:array
                waitUntilDone:NO];
    }
    else {
        if (context) {
            context(nil);
        }
    }
}
- (void)threadLoadFriendList:(NSArray *)params
{
    if(params && [params count] > 0) {
        contextBlock context = [params objectAtIndex:0];
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        NSArray * result = [self loadFriendList:helper.database];
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(result);
            });
        }
    }

}

- (void)asyncAddFriendList:(contextBlock)context list:(NSArray *)userList
{
    if(_workThread && userList) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:userList, bContext, nil];
        [self performSelector:@selector(threadAddFriendList:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadAddFriendList:(NSArray *)params
{
    if([params count] >= 1) {
        NSArray * data = [params objectAtIndex:0];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self addFriendList:helper.database list:data];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}

- (void)asyncUpdateFriend:(contextBlock)context userInfo:(FriendInfo *)userInfo
{
    if(_workThread && userInfo) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:userInfo, bContext, nil];
        [self performSelector:@selector(threadUpdateFriend:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadUpdateFriend:(NSArray *)params
{
    if(params && [params count] >= 1) {
        FriendInfo * userInfo = (FriendInfo *)[params objectAtIndex:0];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self updateFriend:helper.database userInfo:userInfo];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
        
    }
}

- (void)asyncDeleteFriend:(contextBlock)context userId:(uint64_t)userId
{
    if(_workThread && userId) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:@(userId), bContext, nil];
        [self performSelector:@selector(threadDeleteFriend:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
-(void)threadDeleteFriend:(NSArray *)params
{
    if(params && [params count] > 0) {
        uint64_t userId = [[params objectAtIndex:0] unsignedLongLongValue];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self deleteFriend:helper.database userId:userId];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
        
    }
}

- (void)asyncDeleteAllFriend:(contextBlock)context
{
    if(_workThread) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:bContext, nil];
        [self performSelector:@selector(threadDeleteAllFriend:) onThread:_workThread withObject:array waitUntilDone:NO];
    }

}
-(void)threadDeleteAllFriend:(NSArray *)params
{
    if(params) {
        contextBlock context = nil;
        if([params count] > 0) {
            context = [params objectAtIndex:0];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self deleteAllFriend:helper.database];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
        
    }
}

@end
