//
//  DrawSuccessViewController.h
//  QMMM
//
//  Created by Shinancao on 14/11/9.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@interface DrawSuccessViewController : BaseViewController

@property (nonatomic, assign) BOOL isWin; //是否中奖

@property (nonatomic, assign) float amount; //中奖时实际到账的钱数

@end
