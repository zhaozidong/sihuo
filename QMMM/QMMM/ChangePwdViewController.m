//
//  ChangePwdViewController.m
//  QMMM
//
//  Created by kingnet  on 14-10-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "ChangePwdViewController.h"
#import "ChangePwdCellControl.h"
#import "UserInfoHandler.h"

@interface ChangePwdViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ChangePwdCellControl *infoControl;

@end

@implementation ChangePwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"修改密码";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(submitAction:) title:nil size:CGSizeMake(44, 44) imageName:@"navRight_finish_btn"];
    
    self.infoControl = [[ChangePwdCellControl alloc]init];
    
    [self.view addSubview:self.tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)submitAction:(id)sender
{
    if (self.infoControl.oldPwd.length == 0) {
        [AppUtils showAlertMessage:@"请输入旧密码"];
        return;
    }
 
    if (self.infoControl.pwd.length <6 || self.infoControl.pwd.length > 20) {
        [AppUtils showAlertMessage:@"新密码长度应为6-20"];
        return;
    }
    
    [AppUtils showProgressMessage:@"正在提交"];
    ChangePwdViewController __weak * weakSelf = self;
    [[UserInfoHandler sharedInstance] executeChangePwdWithPwd:self.infoControl.pwd oldPwd:self.infoControl.oldPwd success:^(id obj) {
        [AppUtils showSuccessMessage:@"修改成功"];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

#pragma mark - UIView
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.rowHeight = [ChangePwdCellControl rowHeight];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
    }
    return _tableView;
}

#pragma mark - UITablView Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.infoControl.cellsArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.infoControl.cellsArray objectAtIndex:indexPath.row];
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 14.f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
