//
//  UserDataInfo.m
//  QMMM
//
//  Created by kingnet  on 14-10-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "UserDataInfo.h"

@implementation UserDataInfo

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        id temp = [dict objectForKey:@"uid"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            self.uid = [dict objectForKey:@"uid"];
        }
        
        temp = [dict objectForKey:@"nickname"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.nickname = [dict objectForKey:@"nickname"];
        }
        else{
            self.nickname = @"";
        }
        
        temp = [dict objectForKey:@"life_photo"];
        if (temp && [temp isKindOfClass:[NSArray class]]) {
            self.lifePhotos = [NSArray arrayWithArray:temp];
        }
        
        temp = [dict objectForKey:@"avatar"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            self.avatar = [dict objectForKey:@"avatar"];
        }
        else{
            self.avatar = @"";
        }
        
        temp = [dict objectForKey:@"mobile"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            self.mobile = [dict objectForKey:@"mobile"];
        }
        else{
            self.mobile = @"";
        }
        
        temp = [dict objectForKey:@"remark"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            self.remark = temp;
        }
        else{
            self.remark = @"";
        }
    }
    return self;
}

- (void)assignValue:(UserDataInfo *)userInfo
{
    self.uid = userInfo.uid;
    self.nickname = userInfo.nickname;
    self.lifePhotos = userInfo.lifePhotos;
    self.positive = userInfo.positive;
    self.total = userInfo.total;
    self.avatar = userInfo.avatar;
    self.mobile = userInfo.mobile;
    self.remark = userInfo.remark;
}

@end

@implementation PersonPageUserInfo

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        id temp = [dict objectForKey:@"userinfo"];
        if (temp && [temp isKindOfClass:[NSDictionary class]]) {
            self.userInfo = [[UserDataInfo alloc]initWithDictionary:temp];
        }
        
        temp = [dict objectForKey:@"order_rate"]; //获取好中差的字典
        if (temp && [temp isKindOfClass:[NSDictionary class]]) {
            id temp2 = [temp objectForKey:@"positive"];
            if (temp2 && [temp2 isKindOfClass:[NSNumber class]]) {
                self.userInfo.positive = [temp2 intValue];
            }
            
            temp2 = [temp objectForKey:@"total"];
            if (temp2 && [temp2 isKindOfClass:[NSNumber class]]) {
                self.userInfo.total = [temp2 intValue];
            }
        }
        
        temp = [dict objectForKey:@"relation"];
        if (temp && [temp isKindOfClass:[NSDictionary class]]) {
            self.relation = [[PersonRelation alloc] initWithDictionary:temp];
        }
        
        temp = [dict objectForKey:@"reliability"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.reliability = [temp intValue];
        }
        
        temp = [dict objectForKey:@"influence"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.influence = [temp intValue];
        }
    }
    return self;
}

- (void)assignValue:(PersonPageUserInfo *)userInfo
{
    self.userInfo = [[UserDataInfo alloc]init];
    [self.userInfo assignValue:userInfo.userInfo];
    self.relation = [[PersonRelation alloc]init];
    [self.relation assignValue:userInfo.relation];
    self.reliability = userInfo.reliability;
    self.influence = userInfo.influence;
}

@end

@implementation PersonInfluence

- (instancetype)initWithInfluence:(int)influence reliability:(int)reliability influenceBeat:(NSString *)influenceBeat reliabilityBeat:(NSString *)reliabilityBeat uid:(NSString *)uid
{
    self = [super init];
    if (self) {
        self.influence = influence;
        self.reliability = reliability;
        self.uid = uid;
        self.influenceBeat = influenceBeat;
        self.reliabilityBeat = reliabilityBeat;
    }
    return self;
}

- (void)assignValue:(PersonInfluence *)influence
{
    self.uid = influence.uid;
    self.influence = influence.influence;
    self.reliability = influence.reliability;
    self.influenceBeat = influence.influenceBeat;
    self.reliabilityBeat = influence.reliabilityBeat;
}

@end

@implementation PersonOrderStatusNum

- (instancetype)initWithSalerNum:(int)salerNum buyerNum:(int)buyerNum
{
    self = [super init];
    if (self) {
        self.salerNum = salerNum;
        self.buyerNum = buyerNum;
    }
    return self;
}

- (void)assignValue:(PersonOrderStatusNum *)orderStatus
{
    self.salerNum = orderStatus.salerNum;
    self.buyerNum = orderStatus.buyerNum;
}

@end

@implementation PersonRelation

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        id temp = [dict objectForKey:@"friend"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.isFriends = [temp boolValue];
        }
        
        temp = [dict objectForKey:@"bis_friend"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.is2ndFriends = [temp boolValue];
        }
        
        temp = [dict objectForKey:@"is_from_app"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.isFromApp = [temp boolValue];
        }
        
        temp = [dict objectForKey:@"whose_friend"];
        if (temp && [temp isKindOfClass:[NSArray class]]) {
            NSMutableArray *users = [NSMutableArray array];
            for (NSDictionary *dic in temp) {
                UserDataInfo *userInfo = [[UserDataInfo alloc]initWithDictionary:dic];
                [users addObject:userInfo];
            }
            self.friendsArr = [NSArray arrayWithArray:users];
        }
    }
    return self;
}

- (void)assignValue:(PersonRelation *)relation
{
    self.isFriends = relation.isFriends;
    self.is2ndFriends = relation.is2ndFriends;
    self.isFromApp = relation.isFromApp;
    self.friendsArr = relation.friendsArr;
}

@end