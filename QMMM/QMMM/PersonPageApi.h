//
//  PersonPageApi.h
//  QMMM
//
//  Created by kingnet  on 14-10-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseHandler.h"

@interface PersonPageApi : BaseHandler

/**
 获取个人主页的已售出商品
 **/
- (void)getSoldOutGoods:(NSString *)goodsId
                    uid:(NSString *)uid
                context:(contextBlock)context;

/**
 获取在售的商品
 **/
- (void)getSellingGoods:(uint64_t)goodsId
                    uid:(NSString *)uid
                context:(contextBlock)context;

/**
 获取用户信息
 **/
- (void)getUserInfo:(NSString *)uid
            context:(contextBlock)context;

/**
 关注或取消关注
 **/
- (void)addWatch:(NSString *)uid
          follow:(int)follow
    context:(contextBlock)context;

/**
 获取关注度和可信度
 **/
- (void)getWatchNumAndFlyNum:(NSString *)uid
                     context:(contextBlock)context;

/**
 申请加为好友
 **/
- (void)addFriendId:(NSString *)uid
            context:(contextBlock)context;

/**
 获取评价列表  
 **/
- (void)getEvaluateList:(NSString *)uid
                orderId:(uint64_t)orderId
                context:(contextBlock)context;

@end
