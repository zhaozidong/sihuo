//
//  QMPaySuccessViewController.h
//  QMMM
//
//  Created by Derek.zhao on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMPaySuccessViewController : UIViewController


-(id)initWithOrderId:(NSString *)orderId;

@end
