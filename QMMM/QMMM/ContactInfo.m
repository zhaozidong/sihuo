//
//  ContactInfo.m
//  QMMM
//
//  Created by hanlu on 14-10-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "ContactInfo.h"
#import "QMPinYin.h"

@implementation ContactInfo

- (id)initWithRecordRef:(ABRecordRef)record
{
    self = [super init];
    if(self) {
        self.personName = (__bridge_transfer NSString *)ABRecordCopyCompositeName(record);
        
        self.pinyinPersonName = [QMPinYin getPinYin:self.personName hasSymbol:YES];
        
        _phoneList = [[NSMutableArray alloc] init];
        
        CFTypeRef phoneNumbers = ABRecordCopyValue(record, kABPersonPhoneProperty);
        if(phoneNumbers) {
            CFIndex phoneCount = ABMultiValueGetCount(phoneNumbers);
            for (CFIndex i = 0; i < phoneCount; i ++) {
                NSString * phone = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(phoneNumbers, i);
                phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
                phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
                if([phone length] >= 11) {
                    phone = [phone substringWithRange:NSMakeRange([phone length] - 11, 11)];
                    if([phone characterAtIndex:0] == '1') {
                        [_phoneList addObject:phone];
                    }
                }
            }
            CFRelease(phoneNumbers);
        }
    }
    return self;
}

@end