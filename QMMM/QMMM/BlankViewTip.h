//
//  BlankViewTip.h
//  QMMM
//  APP的列表页的占位view
//  Created by kingnet  on 14-11-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlankViewTip : UIView

- (id)initWithMsg:(NSString *)msg imageName:(NSString *)imageName;

- (void)showInView:(UIView *)containerView;

@end
