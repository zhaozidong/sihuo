//
//  QMGoodsCateInfo.h
//  QMMM
//
//  Created by kingnet  on 15-1-12.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "BaseEntity.h"

extern const int kDefaultCategoryId;

@interface QMGoodsCateInfo : BaseEntity

@property (nonatomic, assign) NSUInteger cateId; //类别id

@property (nonatomic, strong) NSString * name; //类别名称

@property (nonatomic, strong) NSArray * childCate; //类别下的子类别

@property (nonatomic, strong) NSString * iconURL; //icon路径

- (id)initWithDictionary:(NSDictionary *)dict;

- (NSString *)childDesc;

@end
