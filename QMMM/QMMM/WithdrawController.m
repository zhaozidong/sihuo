//
//  WithdrawController.m
//  QMMM
//
//  Created by kingnet  on 14-9-27.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "WithdrawController.h"
#import "FinanceDataInfo.h"
#import "FinanceHandler.h"
#import "DrawListController.h"
#import "WithDrawInputCell.h"
#import "DrawSuccessViewController.h"
#import "QMInputAlert.h"
#import "TempFlag.h"

@interface WithdrawController ()<UITableViewDataSource, UITableViewDelegate, QMInputAlertDelegate, WithDrawInputCellDelegate>
{
    NSString *pwd_;
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) WithDrawInputCell *accountNoCell; //支付宝帐号
@property (nonatomic, strong) WithDrawInputCell *accountNameCell; //开户姓名
@property (nonatomic, strong) WithDrawInputCell *drawMoneyCell; //取现金额
@property (nonatomic, strong) UILabel *getMoneyLbl; //实际到账Lable

@end

@implementation WithdrawController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"提现";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    [self.view addSubview:self.tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self registerObserver];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self removeObserver];
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)registerObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processKeyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)removeObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [nc removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [nc removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)processKeyboardWillHide:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0 ;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];

    [UIView animateWithDuration:animationDuration animations:^{
        UIEdgeInsets contentInsets = self.tableView.contentInset;
        contentInsets = UIEdgeInsetsMake(contentInsets.top, 0, 0, 0);
        self.tableView.contentInset = contentInsets;
        self.tableView.scrollIndicatorInsets = contentInsets;
    }];
}

- (void)processKeyboardWillShow:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0 ;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];

    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = self.tableView.contentInset;
    contentInsets = UIEdgeInsetsMake(contentInsets.top, 0.0, (keyboardSize.height), 0.0);
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.tableView.contentInset = contentInsets;
        self.tableView.scrollIndicatorInsets = contentInsets;
    } completion:^(BOOL finished) {

    }];
}

#pragma mark - UIButton Action
+ (void)alertWithTitle:(NSString *)title msg:(NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:msg delegate:self cancelButtonTitle:@"哦，知道了" otherButtonTitles:nil];
    [alert show];
}

- (void)getMoneyAction:(id)sender
{
    [AppUtils closeKeyboard];
    
    if (self.accountNoCell.text.length == 0) {
        [AppUtils showAlertMessage:@"支付宝帐号不能为空"];
        return;
    }
    
    if (self.accountNameCell.text.length == 0) {
        [AppUtils showAlertMessage:@"支付宝用户名不能为空"];
        return;
    }
    
    if (self.drawMoneyCell.text.length == 0) {
        [AppUtils showAlertMessage:@"请输入提现金额"];
        return;
    }
    FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
    float amount = [self.drawMoneyCell.text floatValue];
    if ([kWithdrawFrom isEqualToString:kBalance]) {
        if (amount > financeInfo.amount) {
            [AppUtils showAlertMessage:[NSString stringWithFormat:@"您当前的账户余额为%.2f",financeInfo.amount]];
            return;
        }
    }
    else{
        if (amount > financeInfo.hongbao) {
            [AppUtils showAlertMessage:[NSString stringWithFormat:@"您当前的红包金额为%.2f",financeInfo.hongbao]];
            return;
        }
        //判断是否为整百
        if ((amount == 0) || ((int)amount % 100) >0 || (amount > (int)amount)) {
            [AppUtils showAlertMessage:@"只能整百提现哦~"];
            return;
        }
    }
    
    if (amount < 10) {
        [AppUtils showAlertMessage:@"提现金额至少10元"];
        return;
    }
    
    [self removeObserver];
    
    [QMInputAlert showWithPlaceholder:@"请输入私货的登录密码" alertType:QMInputAlertTypeSecurity delegate:self];
}

#pragma mark - UIView
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorColor = kBorderColor;
        //footerView
        UIView *footView = [[[NSBundle mainBundle]loadNibNamed:@"WithdrawFootView" owner:nil options:0] lastObject];
        footView.frame = CGRectMake(0, 0, CGRectGetWidth(footView.frame), 70);
        _tableView.tableFooterView = footView;
        for (UIButton *button in footView.subviews) {
            if ([button isKindOfClass:[UIButton class]]) {
                [button addTarget:self action:@selector(getMoneyAction:) forControlEvents:UIControlEventTouchUpInside];
                break;
            }
        }
    }
    return _tableView;
}

- (WithDrawInputCell *)accountNoCell
{
    if (!_accountNoCell) {
        _accountNoCell = [WithDrawInputCell inputCell];
        _accountNoCell.isShowRightView = YES;
        _accountNoCell.placeholder = @"您的支付宝帐号";
        FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
        //待定
        if (financeInfo.payee!=nil && ![financeInfo.payee isEqualToString:@""]) {
            _accountNoCell.text = financeInfo.accountNo;
            _accountNoCell.textFieldEnabled = NO;
        }
    }
    return _accountNoCell;
}

- (WithDrawInputCell *)accountNameCell
{
    if (!_accountNameCell) {
        _accountNameCell = [WithDrawInputCell inputCell];
        _accountNameCell.placeholder = @"您的支付宝用户名";
        FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
        if (financeInfo.payee!=nil && ![financeInfo.payee isEqualToString:@""]) {
            _accountNameCell.text = financeInfo.payee;
            _accountNameCell.textFieldEnabled = NO;
        }
    }
    return _accountNameCell;
}

- (WithDrawInputCell *)drawMoneyCell
{
    if (!_drawMoneyCell) {
        _drawMoneyCell = [WithDrawInputCell inputCell];
        if ([kWithdrawFrom isEqualToString:kBalance]) {
            _drawMoneyCell.placeholder = @"金额(最低提现金额为10.00元)";
        }
        else{
            _drawMoneyCell.placeholder = @"金额(需整百提现)";
            FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
            int money = ((int)financeInfo.hongbao/100)*100;
            _drawMoneyCell.text = [NSString stringWithFormat:@"%d", money];
        }
        _drawMoneyCell.delegate = self;
        _drawMoneyCell.keboardType = UIKeyboardTypeDecimalPad;
    }
    return _drawMoneyCell;
}

#pragma mark - QMInputAlertDelegate
- (BOOL)okWithInputStr:(NSString *)str inputAlert:(QMInputAlert *)inputAlert
{
    if (str.length == 0) {
        [AppUtils showErrorMessage:@"密码不能为空"];
        return NO;
    }
    pwd_ = str;
    [self registerObserver];
    [AppUtils showProgressMessage:@"正在提交"];

    NSString *payee = self.accountNameCell.text;
    NSString *accountNo = self.accountNoCell.text;
    
    //提交体现
    float amount = [self.drawMoneyCell.text floatValue];
    [[FinanceHandler sharedInstance] drawMoney:pwd_ money:amount payee:payee accountNo:accountNo success:^(id obj) {
        [AppUtils dismissHUD];
        NSDictionary *dic = (NSDictionary *)obj;
        if (dic && [dic isKindOfClass:[NSDictionary class]]) {
            NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
            [viewControllers removeObject:self];
            
            DrawSuccessViewController *controller = [[DrawSuccessViewController alloc]init];
//            if ([dic[@"rate"] floatValue] == 0) {
//                controller.isWin = YES;
//                controller.amount = [dic[@"net_money"] floatValue];
//            }
//            else{
                controller.isWin = NO;
//            }
            [viewControllers addObject:controller];
            [self.navigationController setViewControllers:viewControllers animated:YES];
        }
        else{
            [[self class] alertWithTitle:@"提交失败" msg:obj];
        }
    } failed:^(id obj) {
        [AppUtils dismissHUD];
        [[self class] alertWithTitle:@"提交失败" msg:(NSString *)obj];
    }];
    return YES;
}

- (void)cancelInput
{
    pwd_ = @"";
    [self registerObserver];
}

#pragma mark - UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 2;
    }
    else{
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [WithDrawInputCell rowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return self.accountNoCell;
        }
        if (indexPath.row == 1) {
            return self.accountNameCell;
        }
    }
    else if (indexPath.section == 1){
        return self.drawMoneyCell;
    }
    return nil;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    return 75.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view =[[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 40.f)];
    view.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(15.f, 0, CGRectGetWidth(tableView.frame)-15.f, 40.f)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:12.f];
    label.textColor = kDarkTextColor;
    [view addSubview:label];
    if (section == 0) {
        label.text = [AppUtils localizedPersonString:@"QM_Text_SetAccountTip"];
    }
    else{
        FinanceUserInfo *financeInfo = [FinanceHandler sharedInstance].financeUserInfo;
        if ([kWithdrawFrom isEqualToString:kBalance]) {
            label.text = [NSString stringWithFormat:@"可用现金: %.2f", financeInfo.amount];
        }
        else{
            int money = ((int)financeInfo.hongbao/100)*100;
            label.text = [NSString stringWithFormat:@"可用红包: %d元", money];
        }
    }
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 1) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 75.f)];
        view.backgroundColor = [UIColor clearColor];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, CGRectGetWidth(view.frame)-15, 50.f)];
        [view addSubview:label];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = UIColorFromRGB(0x9198A3);
        label.font = [UIFont systemFontOfSize:10.f];
        label.numberOfLines = 3;

        NSString *costs = @"0手续费";
        NSString *tips = @"1-3个工作日";
        NSString *text = [NSString stringWithFormat:@"使用私货可进行%@提现；确认提现后，余额将在%@内转到您的支付宝账户", costs, tips];
        NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:text];
        NSRange range = [text rangeOfString:costs];
        [attributedStr addAttribute:NSForegroundColorAttributeName value:kRedColor range:NSMakeRange(range.location, costs.length)];
        range = [text rangeOfString:tips];
        [attributedStr addAttribute:NSForegroundColorAttributeName value:kRedColor range:NSMakeRange(range.location, tips.length)];
        label.attributedText = attributedStr;
        
        //实际到账Lable
        if (self.getMoneyLbl == nil) {
            self.getMoneyLbl = [[UILabel alloc]initWithFrame:CGRectMake(15.f, CGRectGetMaxY(label.frame)+5, CGRectGetWidth(tableView.frame)-15-10, 20)];
            self.getMoneyLbl.backgroundColor = [UIColor clearColor];
            self.getMoneyLbl.textColor = kDarkTextColor;
            self.getMoneyLbl.font = [UIFont systemFontOfSize:15.f];
            self.getMoneyLbl.textAlignment = NSTextAlignmentRight;
        }
        [view addSubview:self.getMoneyLbl];
        return view;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - 
- (void)inputText:(NSString *)text
{
    float inputAmount = [text floatValue];
//    float costs = inputAmount*0.03;
//    float money = inputAmount - costs;
    self.getMoneyLbl.text = [NSString stringWithFormat:@"实际到账: %.2f元", inputAmount];
}

@end
