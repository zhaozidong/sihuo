//
//  BaseViewController.m
//  QMMM
//
//  Created by kingnet  on 14-8-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()
@property (nonatomic, strong) UISwipeGestureRecognizer *swipGesture;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    self.view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight)];
    self.view.backgroundColor = kBackgroundColor;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.canSwipCloseKeyboard = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//用于导航栏的返回
- (void)backAction:(id)sender
{
    [AppUtils dismissHUD];
    if ([self.navigationController.viewControllers count] < 2) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setCanSwipCloseKeyboard:(BOOL)canSwipCloseKeyboard
{
    _canSwipCloseKeyboard = canSwipCloseKeyboard;
    if (_canSwipCloseKeyboard) {
        //添加滑动手势，以便使键盘消失
        if (!self.swipGesture) {
            self.swipGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwip:)];
            self.swipGesture.direction = UISwipeGestureRecognizerDirectionDown;
        }
        [self.view addGestureRecognizer:self.swipGesture];
    }
    else{
        if (self.swipGesture) {
            [self.view removeGestureRecognizer:self.swipGesture];
        }
    }
}

- (void)setCanTapCloseKeyboard:(BOOL)canTapCloseKeyboard
{
    _canTapCloseKeyboard = canTapCloseKeyboard;
    if (_canTapCloseKeyboard) {
        //添加滑动手势，以便使键盘消失
        if (!self.tapGesture) {
            self.tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
            self.tapGesture.numberOfTapsRequired = 1;
            self.tapGesture.numberOfTouchesRequired = 1;
        }
        [self.view addGestureRecognizer:self.tapGesture];
    }
    else{
        if (self.tapGesture) {
            [self.view removeGestureRecognizer:self.tapGesture];
        }
    }
}

- (void)handleSwip:(UISwipeGestureRecognizer *)gesture
{
    [AppUtils closeKeyboard];
}

- (void)handleTap:(UITapGestureRecognizer *)gesture
{
    [AppUtils closeKeyboard];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSString *className = NSStringFromClass([self class]);
    [MobClick beginLogPageView:className];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSString *className = NSStringFromClass([self class]);
    [MobClick endLogPageView:className];
}
@end
