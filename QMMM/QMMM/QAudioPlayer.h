//
//  QAudioPlayer.h
//  QMMM
//
//  Created by Derek.zhao on 14-11-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "QMGoodsAudioView.h"


//typedef void(^finishBlock)(BOOL isFinished) ;

@interface QAudioPlayer : NSObject<AVAudioRecorderDelegate,AVAudioPlayerDelegate>{
    AVAudioPlayer *audioPlayer;
    AVAudioRecorder *audioRecorder;
    NSString *strAudioPath;
    
//    finishBlock finishBlk;
}

+(QAudioPlayer *) sharedInstance;
-(void)playAudioWithPath:(NSString *)strPath withRelatedView:(id)relatedView;
-(void)pausePlay;
-(void)stopPlay;
-(BOOL)deleteAudio;
-(BOOL)isPlaying;
-(NSString *)getTimeForAudio;



@end
