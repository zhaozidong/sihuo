//
//  SettingsJumpCell.m
//  QMMM
//
//  Created by kingnet  on 14-11-5.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "SettingsJumpCell.h"

NSString * const kSettingsJumpCellIdentifier = @"kSettingsJumpCellIdentifier";

@interface SettingsJumpCell ()
@property (weak, nonatomic) IBOutlet UILabel *leftText;

@end

@implementation SettingsJumpCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"SettingsJumpCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.leftText.text = @"";
    self.leftText.textColor = kDarkTextColor;
    self.leftText.font = [UIFont systemFontOfSize:15.f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (NSArray *)items
{
    return @[@"意见反馈", @"给我评分", @"用户协议", @"推荐给朋友"];
}

+ (CGFloat)cellHeight
{
    return 45.f;
}

+ (NSInteger)cellCount
{
    return 3;
}

- (void)updateUI:(NSInteger)index
{
    if (index < [[self class]items].count) {
        self.leftText.text = [[[self class]items] objectAtIndex:index];
    }
}

@end
