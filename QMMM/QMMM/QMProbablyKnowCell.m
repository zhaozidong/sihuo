//
//  QMProbablyKnowCell.m
//  QMMM
//
//  Created by Derek on 15/1/20.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMProbablyKnowCell.h"


@interface QMProbablyKnowCell(){
    FriendInfo *_friendInfo;
}

@end



@implementation QMProbablyKnowCell


+ (id)cellForProbablyKnow
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMProbablyKnowCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code

    }
    return self;
}


- (void)awakeFromNib {
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.headView.headViewStyle=QMRoundHeadViewSmall;
    self.headView.clickEnable = NO;
    UIImage *image=[UIImage imageNamed:@"border_btn_bkg"];
    [_btnNewFriend setBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(10, 7, 10, 7)] forState:UIControlStateNormal];
    [_btnNewFriend setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [_btnNewFriend addTarget:self action:@selector(btnDidNewFriend:) forControlEvents:UIControlEventTouchUpInside];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateWithFriendInfo:(FriendInfo *)friendInfo{
    _friendInfo=friendInfo;
    [self.headView setImageUrl:friendInfo.icon];
    self.lblName.text=friendInfo.nickname;
    self.lblDesc.text=friendInfo.tips;
}


-(void)layoutSubviews{
    
//    _head.frame=CGRectMake(0, 5, 50, 50);
    
    if ([self respondsToSelector:@selector(setSeparatorInset:)])
        [self setSeparatorInset:UIEdgeInsetsZero];
    
    if ([self respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [self setPreservesSuperviewLayoutMargins:NO];;
    }
    
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
    CGRect frame;
    _btnNewFriend.frame=CGRectMake(kMainFrameWidth-70, 10, 60, 32);
    frame=_lblDesc.frame;
    frame.size.width=kMainFrameWidth-140;
//    _lblDesc.frame=CGRectMake(50, 40, kMainFrameWidth-160, 30);
    _lblDesc.frame=frame;
    
}

-(void)btnDidNewFriend:(id)sender{
    if (_delegate && [_delegate respondsToSelector:@selector(addNewFriend:)]) {
        [_delegate addNewFriend:_friendInfo];
    }
}

@end
