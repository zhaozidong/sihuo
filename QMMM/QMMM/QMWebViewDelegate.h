//
//  QMWebViewDelegate.h
//  QMMM
//
//  Created by kingnet  on 15-2-6.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JSMethodDelegate <NSObject>

@optional
/**
 邀请好友
 协议: protocol://inviteFriends
 **/
- (void)inviteFriends;

/**
 发布商品
 协议: protocol://publishGoods/from
 **/
- (void)publishGoods:(NSString *)from;

/**
 设置界面的title
 协议: protocol://pageTitle/名称
 **/
- (void)setTitle:(NSString *)title;

/**
 复制
 协议: protocol://copyContent/复制的内容
 **/
- (void)copyContent:(NSString *)content;

/**
 邀请全部
 协议: protocol://inviteAllFriends
 **/
- (void)inviteAllFriends;

/**
 邀请朋友
 **/
- (void)shareToSMS;

/**
 返回上一层
 **/
- (void)back;

/**
 跳转到个人主页
 **/
- (void)personPage:(uint64_t)userId;

/**
 跳转到商品详情
 **/
- (void)goodsDetail:(uint64_t)goodsId;

/**
 跳转到购买界面
 **/
- (void)buyHongBaoGoods:(int)num price:(float)price goodsId:(int)goodsId;

/**
 弹出带输入框的键盘
 **/
- (void)keyboard;

@end
@interface QMWebViewDelegate : NSObject<UIWebViewDelegate>

{
    struct{
        unsigned int delegateInviteFriends:1;
        unsigned int delegatePublishGoods:1;
        unsigned int delegatePageTitle:1;
        unsigned int delegateCopy:1;
        unsigned int delegateInviteAllFriends:1;
        unsigned int delegateBack:1;
        unsigned int delegatePersonPage:1;
        unsigned int delegateGoodsDetail:1;
        unsigned int delegateBuyHongBaoGoods:1;
        unsigned int delegateKeyboard:1;
        
    } _methodFlags;
}

@property (nonatomic, weak) id<JSMethodDelegate> methodDelegate;

- (id)initWithDelegate:(id<JSMethodDelegate>) delegate;

@end
