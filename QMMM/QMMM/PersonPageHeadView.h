//
//  PersonPageHeadView.h
//  QMMM
//
//  Created by kingnet  on 14-11-10.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PersonPageHeadViewDelegate <NSObject>

- (void)didTapHeadView;

- (void)didTapFriendBtn;

@end

@class UserDataInfo, PersonRelation;

@interface PersonPageHeadView : UIView

@property (nonatomic) BOOL touching;
@property (nonatomic) float offsetY;

@property (weak, nonatomic) IBOutlet UIImageView *img_banner;

@property (nonatomic, weak) id<PersonPageHeadViewDelegate> delegate;

+ (PersonPageHeadView *)headView;

+ (CGFloat )headViewHeight;

- (void)updateUI:(UserDataInfo *)userDataInfo;

- (void)updateFriendButton:(PersonRelation *)relation;

@end
