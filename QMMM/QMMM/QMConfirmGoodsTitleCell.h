//
//  QMConfirmGoodsTitleCell.h
//  QMMM
//
//  Created by hanlu on 14-11-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMConfirmGoodsTitleCellIdentifier;

@interface QMConfirmGoodsTitleCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView * flagImageView;

@property (nonatomic, strong) IBOutlet UILabel * firstLabel;

@property (nonatomic, strong) IBOutlet UILabel * secondLabel;

@property (nonatomic, strong) IBOutlet UILabel * thirdLabel;

+ (id)cellForConfirmGoodsTitle;

@end
