//
//  QMPayFailViewController.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMPayFailViewController.h"
#import "MyOrderInfo.h"
#import "QMPaySuccessViewController.h"

#import "BackNavigationController.h"

@interface QMPayFailViewController ()<QMPayOrderDelegate, UIGestureRecognizerDelegate>{
    float _sihuoPay;
    float _aliPay;
    NSString *_orderId;
}

@end

@implementation QMPayFailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title=@"支付失败";
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout =UIRectEdgeNone;
    }
    self.view.backgroundColor = kBackgroundColor;
    
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = (BackNavigationController *)self.navigationController;
}

- (void)backAction:(id)sender{
    int count = (int)self.navigationController.viewControllers.count;
    NSMutableArray *mbArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [mbArray removeObjectsInRange:NSMakeRange(count-2, 2)];
    self.navigationController.interactivePopGestureRecognizer.delegate = (BackNavigationController *)self.navigationController;
    [self.navigationController setViewControllers:mbArray animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setOrderId:(NSString *)orderId sihuoPay:(float)sihuoPay andAliPay:(float)aliPay {
    _sihuoPay=sihuoPay;
    _aliPay=aliPay;
    _orderId=orderId;
}

- (IBAction)rePay:(id)sender {
    if (_orderId) {
        MyOrderInfo *orderInfo=[[MyOrderInfo alloc] initWithOrderId:_orderId andRemain:_sihuoPay];
        orderInfo.delegate=self;
        [orderInfo payOrder];
    }else{
        DLog(@"支付参数为空");
    }
}

-(void)paySuccessForOrderId:(NSString *)orderId{//在支付失败界面只需要实现支付成功的回调
    if (orderId) {
        QMPaySuccessViewController *paySuccess=[[QMPaySuccessViewController alloc] initWithOrderId:orderId];
        [self.navigationController pushViewController:paySuccess animated:YES];
    }else{
        DLog(@"订单id为空");
    }
}

- (void)payFail:(NSString *)tip
{
    [AppUtils showErrorMessage:tip];
}

#pragma mark - Gesture Delegate
-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer == self.navigationController.interactivePopGestureRecognizer) {
        [self backAction:nil];
        return NO;
    }
    return YES;
}

@end
