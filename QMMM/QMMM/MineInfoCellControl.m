//
//  MineInfoCellControl.m
//  QMMM
//
//  Created by kingnet  on 14-9-28.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MineInfoCellControl.h"
#import "UserEntity.h"
#import "QMRoundHeadView.h"

@interface MineInfoCellControl ()<UITextFieldDelegate, QMRoundHeadViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nickNameTf;
@property (weak, nonatomic) IBOutlet UITextField *phoneTf;
@property (weak, nonatomic) IBOutlet UITextField *emailTf;
@property (weak, nonatomic) IBOutlet UILabel *sihuoNumLbl;
@property (weak, nonatomic) IBOutlet QMRoundHeadView *headViewBtn;

@end
@implementation MineInfoCellControl

- (instancetype)init
{
    if (self = [super init]) {
        self.cellsArray = [[NSBundle mainBundle]loadNibNamed:@"MineInfoCells" owner:self options:0];
        self.nickNameTf.delegate = self;
        self.phoneTf.delegate = self;
        self.emailTf.delegate = self;
        self.phoneTf.enabled = NO;
        self.sihuoNumLbl.text = @"";
        self.emailTf.attributedPlaceholder = [AppUtils placeholderAttri:@"未填写"];
        self.nickNameTf.attributedPlaceholder = [AppUtils placeholderAttri:@"未填写"];
        self.phoneTf.attributedPlaceholder = [AppUtils placeholderAttri:@"未填写"];
        self.headViewBtn.headViewStyle = QMRoundHeadViewSmall;
        self.headViewBtn.delegate = self;
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

+ (CGFloat)rowHeightWithIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            return 60.;
        }
    }
    return 45.f;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [AppUtils closeKeyboard];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.nickNameTf) {
        self.editingIndexPath = [NSIndexPath indexPathForRow:2 inSection:1];
    }
    if (textField == self.emailTf) {
        self.editingIndexPath = [NSIndexPath indexPathForRow:4 inSection:1];
    }
}

- (void)updateUI:(UserEntity *)entity
{
    if (![entity.nickname isEqualToString:@""]) {
        self.nickNameTf.text = entity.nickname;
    }
    
    self.sihuoNumLbl.text = entity.uid;

    self.phoneTf.text = entity.mobile;
    
    if (![entity.email isEqualToString:@""]) {
        self.emailTf.text = entity.email;
    }
    
    [self.headViewBtn setImageUrl:entity.avatar];
}

- (void)updateUserAvarta:(UIImage *)image
{
    self.headViewBtn.image = image;
}

- (NSString *)nickName
{
    
    NSString *text = [self.nickNameTf.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (self.nickNameTf.text == nil || self.nickNameTf.text.length == 0 || text.length == 0) {
        return @"";
    }
    return self.nickNameTf.text;
}

- (NSString *)email
{
    if (self.emailTf.text == nil) {
        return @"";
    }
    return self.emailTf.text;
}

- (void)textDidChange:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter]postNotificationName:kMineInfoTextChangeNotify object:nil];
}

- (void)clickHeadViewWithUid:(uint64_t)userId
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickHeadViewWithUid:)]) {
        [self.delegate clickHeadViewWithUid:userId];
    }
}

@end
