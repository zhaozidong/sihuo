//
//  SoldOutGoodsEntity.m
//  QMMM
//
//  Created by kingnet  on 14-10-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "SoldOutGoodsEntity.h"

@implementation EvaluateEntity

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.uid = [dict objectForKey:@"uid"];
        if (!self.uid || [self.uid isKindOfClass:[NSNull class]]) {
            self.uid = @"";
        }
        
        id temp = [dict objectForKey:@"comments"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.comment = [dict objectForKey:@"comments"];
        }
        
        temp = [dict objectForKey:@"avatar"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.avatar = [dict objectForKey:@"avatar"];
        }
        else{
            self.avatar = @"";
        }
        
        temp = [dict objectForKey:@"comment_ts"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.comment_ts = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            self.comment_ts = [temp intValue];
        }
        
        temp = [dict objectForKey:@"rate"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.rate = [temp intValue];
        }
        else if(temp && [temp isKindOfClass:[NSString class]]){
            self.rate = [temp intValue];
        }
        
        temp = [dict objectForKey:@"amount"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.amout = [temp floatValue];
        }
        else if(temp && [temp isKindOfClass:[NSString class]]){
            self.amout = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"nickname"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.nickname = [dict objectForKey:@"nickname"];
        }
        else{
            self.nickname = @"";
        }
    }
    return self;
}

@end

@implementation SoldOutGoodsEntity

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        id temp = [dict objectForKey:@"goods_id"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.goodsId = [temp longLongValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.goodsId = [temp longLongValue];
        }
        
        temp = [dict objectForKey:@"photo"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.photo = [dict objectForKey:@"photo"];
        }
        else{
            self.photo = @"";
        }
        
        temp = [dict objectForKey:@"goods_desc"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.desc = [dict objectForKey:@"goods_desc"];
        }
        else{
            self.desc = @"";
        }
        
        temp = [dict objectForKey:@"price"];
        if(temp && [temp isKindOfClass:[NSNumber class]]){
            self.price = [temp floatValue];
        }
        else if(temp && [temp isKindOfClass:[NSString class]]){
            self.price = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"comments"];
        NSMutableArray *tempArr = [NSMutableArray array];
        if (temp && [temp isKindOfClass:[NSArray class]]) {
            for (NSDictionary *dic in temp) {
                if ([dic isKindOfClass:[NSDictionary class]]) {
                    EvaluateEntity *entity = [[EvaluateEntity alloc]initWithDictionary:dic];
                    [tempArr addObject:entity];
                }
            }
        }
        self.evaluateArr = [NSArray arrayWithArray:tempArr];
    }
    return self;
}

@end
