//
//  BaseHandler.m
//  QMMM
//
//  Created by kingnet  on 14-8-30.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseHandler.h"
#import "APIConfig.h"
#import "QMHttpClient.h"
#import "VersionInfo.h"

@implementation BaseHandler

NSString * const kServerErrorTip = @"ServerErrorTip";
NSString * const kHTTPTreatSuccess = @"OK";
NSString * const kHTTPTreatFailed = @"NO";
NSString * const kNotNetWorkError = @"kNotNetWorkError";

+ (NSString *)requestUrlWithPath:(NSString *)path
{//&format=msgpack
    NSString *ver = [NSString stringWithFormat:@"&v=%@", [VersionInfo currentVer]];
    return [[@"http://" stringByAppendingString:[[AppUtils serverHost] stringByAppendingString:path]]stringByAppendingString:ver];
}

+ (NSString *)httpsRequestUrlWithPath:(NSString *)path
{//&format=msgpack
    NSString *ver = [NSString stringWithFormat:@"&v=%@", [VersionInfo currentVer]];
    NSString *headStr = @"";
#ifdef DEBUG
    headStr = @"http://";
#else
    if (kIsTestRelease) {
        headStr = @"http://";
    }
    else{
        headStr = @"https://";
    }
#endif
    return [[headStr stringByAppendingString:[[AppUtils serverHost] stringByAppendingString:path]]stringByAppendingString:ver];
}

+ (NSString *)uploadFileWithPath:(NSString *)path
{
    NSString *ver = [NSString stringWithFormat:@"&v=%@", [VersionInfo currentVer]];
    return [[@"http://" stringByAppendingString:[[AppUtils uploadServerHost] stringByAppendingString:path]]stringByAppendingString:ver];
}

- (void)sendSMSWithMobile:(NSString *)mobile type:(NSString *)type success:(SuccessBlock)success failed:(FailedBlock)failed
{
    NSString *path = [[self class]httpsRequestUrlWithPath:API_SMS];
    
    NSDictionary *params = @{@"type":type, @"mobile":mobile};
    
    [[QMHttpClient defaultClient]requestWithPath:path method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error = nil;
        
        NSString *msgStr = (NSString *)[self parseData:responseObject error:&error];
        
        if (msgStr) {
            if (success) {
                success(msgStr);
            }
        }
        else{
            DLog(@"send sms error:%@", error.userInfo[NSLocalizedDescriptionKey]);
            NSString *errMsg = error.userInfo[kServerErrorTip];
            failed(errMsg);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error:%@", error.description);
        NSString *errMsg = [AppUtils localizedCommonString:@"QM_Alert_Server"];
        failed(errMsg);
    }];
}

- (id)parseData:(NSData *)data error:(NSError *__autoreleasing *)error
{
    NSError *err = nil;

//    NSDictionary *dataDic = [MPMessagePackReader readData:data error:&err];
    NSString *resString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    DLog(@"restring:%@", resString);

    NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&err];
    
    if (err) {//解析出错
        if (error) {

            NSDictionary *userInfo = @{NSLocalizedDescriptionKey:err.description, kServerErrorTip:[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            *error = [NSError errorWithDomain:err.domain code:err.code userInfo:userInfo];
        }
        return nil;
    }
    else{
        if (![dataDic isKindOfClass:[NSDictionary class]]) {//不是NSDictionary
            if (error) {
                NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"http response not a nsdictionary", kServerErrorTip:[AppUtils localizedCommonString:@"QM_Alert_Server"]};
                *error = [NSError errorWithDomain:kAppErrorDomain code:QMNotDictionaryError userInfo:userInfo];
            }
            return nil;
        }
        else{
            if ([[dataDic objectForKey:@"c"] intValue] == 0) {//
                id result = [dataDic objectForKey:@"d"];
                return result;
            }
            else{//返回错误提示
                if (error) {
                    NSString *tip = [NSString stringWithFormat:@"%@(%@)", [dataDic objectForKey:@"d"], [dataDic objectForKey:@"c"]];
                    NSDictionary *userInfo = @{NSLocalizedDescriptionKey:@"http response c != 0", kServerErrorTip:tip};
                    *error = [NSError errorWithDomain:kAppErrorDomain code:QMCNotZeroError userInfo:userInfo];
                }
                return nil;
            }
        }
    }
}

+ (NSString *)audioPath
{
    NSString * savePath = [[AppUtils stringForUserLibraryPath] stringByAppendingPathComponent:@"Caches/goods_audio/"];
    
    BOOL isDirectory = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (!([fileManager fileExistsAtPath:savePath isDirectory:&isDirectory] && isDirectory)) {
        [fileManager createDirectoryAtPath:savePath withIntermediateDirectories:YES attributes:nil error:nil];
    }

    return savePath;
}

- (void)getBaiDuLatitude:(double)latitude longitude:(double)longitude success:(SuccessBlock)success failed:(FailedBlock)failed
{
    NSString *coords = [NSString stringWithFormat:@"%f,%f", longitude, latitude];
    NSDictionary *params = @{@"coords":coords};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:API_BAIDU_COORDS method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *err = nil;
            NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:&err];
            if (!err) {
                if ([[dataDic objectForKey:@"status"] intValue] == 0) {
                    NSArray *array = [dataDic objectForKey:@"result"];
                    NSDictionary *dic = array[0];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (success) {
                            success(dic);
                        }
                    });
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (failed) {
                            failed(@"百度请求错误");
                        }
                    });
                }
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (failed) {
                        failed(err.localizedDescription);
                    }
                });
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (failed) {
                    failed(error.localizedDescription);
                }
            });
        }];
    });
}

- (void)getDetailLoc:(double )latitude longitude:(double )longitude success:(SuccessBlock)success failed:(FailedBlock)failed
{
    NSString *coords = [NSString stringWithFormat:@"%f,%f", latitude, longitude];
    NSDictionary *params = @{@"location":coords};

    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient]requestWithPath:API_DETAIL_LOC method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *err = nil;
            NSDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:&err];
            if (!err) {
                if ([[dataDic objectForKey:@"status"] intValue] == 0) {
                    NSDictionary *dic = [dataDic objectForKey:@"result"];
                    NSDictionary *dic2 = [dic objectForKey:@"addressComponent"];
                    NSString *res = [NSString stringWithFormat:@"%@%@%@", dic2[@"province"], dic2[@"city"], dic2[@"district"]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (success) {
                            success(res);
                        }
                    });
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (failed) {
                            failed(@"百度请求错误");
                        }
                    });
                }
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (failed) {
                        failed(err.localizedDescription);
                    }
                });
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (failed) {
                    failed(error.localizedDescription);
                }
            });
        }];

    });
}

- (void)batchUploadImageData:(NSArray *)datas type:(NSString *)type context:(contextBlock)context
{
    __block NSString *path = [[self class]uploadFileWithPath:[NSString stringWithFormat:API_UPLOAD_FILE, type]];
    
    __block contextBlock bContext = context;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSMutableArray *mutableOperations = [NSMutableArray array];
        for (NSData *data in datas) {
            NSInputStream *inputStreamForFile = [NSInputStream inputStreamWithData:data];
            NSURL *serverURL = [NSURL URLWithString:path];
            // 上传大小
            NSMutableURLRequest *request;
            request = [NSMutableURLRequest requestWithURL:serverURL];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBodyStream:inputStreamForFile];
            [request setValue:@"image/jpg" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%ld", (unsigned long)[data length]] forHTTPHeaderField:@"Content-Length"];
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            [mutableOperations addObject:operation];
        }
        
        NSArray *operations = [AFURLConnectionOperation batchOfRequestOperations:mutableOperations progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations) {
            NSLog(@"%lu of %lu complete", (unsigned long)numberOfFinishedOperations, (unsigned long)totalNumberOfOperations);
        } completionBlock:^(NSArray *operations) {
            NSMutableArray *mbPhotoKeys = [NSMutableArray array];
            for (AFHTTPRequestOperation *opt in operations) {
                NSError *jsonErr = nil;
                if (opt.response) {
                    NSDictionary *result= [NSJSONSerialization JSONObjectWithData:opt.responseData options:NSJSONReadingAllowFragments error:&jsonErr];
                    if (!jsonErr) {
                        if ([[result objectForKey:@"c"] intValue] == 0) {
                            NSDictionary *dataDic = [result objectForKey:@"d"];
                            if ([dataDic isKindOfClass:[NSDictionary class]]) {
                                NSString *key = dataDic[@"asset_key"];
                                [mbPhotoKeys addObject:key];
                            }
                            else{
                                [mbPhotoKeys addObject:@""];
                                DLog(@"batchupload goods image error:%@", result);
                            }
                        }
                        else{
                            [mbPhotoKeys addObject:@""];
                            DLog(@"batchupload goods image error:%@", [result objectForKey:@"d"]);
                        }
                    }
                    else{
                        [mbPhotoKeys addObject:@""];
                        DLog(@"batchupload goods image error:%@",jsonErr.localizedDescription);
                    }
                }
                else{
                    DLog(@"batchupload goods image error: response is nil");
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(mbPhotoKeys);
                }
            });
        }];
        [[NSOperationQueue mainQueue] addOperations:operations waitUntilFinished:NO];
    });
}

- (void)processResponse:(id)responseObject error:(NSError *)error operation:(AFHTTPRequestOperation *)operation context:(contextBlock)context
{
    if (responseObject) {//请求成功了
        NSError *err = nil;
        id result = [self parseData:responseObject error:&err];
        if (result) {
            NSDictionary *dic = @{@"s":@(0), @"d":result};
            dispatch_async(dispatch_get_main_queue(), ^{
                if (context) {
                    context(dic);
                }
            });
        }
        else{
            NSDictionary *dic = @{@"s":@(-1), @"d":err.userInfo[kServerErrorTip]};
            dispatch_async(dispatch_get_main_queue(), ^{
                if (context) {
                    context(dic);
                }
            });
        }
    }
    else if (error){ //http请求失败了
        NSDictionary *dic;
        if (error.code == -1001) {
            dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
        }
        else if(error.code == QMNotNetworkError){
            dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Network"]};
        }
        else{
            dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (context) {
                context(dic);
            }
        });
    }
}

@end
