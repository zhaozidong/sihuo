//
//  UserInfoHandler.m
//  QMMM
//
//  Created by kingnet  on 14-9-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "UserInfoHandler.h"
#import "UserInfoApi.h"
#import "UserEntity.h"
#import "UserDao.h"
#import "UserDefaultsUtils.h"
#import "QMDatabaseHelper.h"
#import "UserBasicInfo.h"
#import "QMChatUserHelper.h"
#import "QMDataCenter.h"
#import "ContactorInfo.h"
#import "UserDataInfo.h"

@interface UserInfoHandler ()
{
    UserInfoApi *userInfoApi_;
    
    NSMutableDictionary * _userDict;
}
@end

@implementation UserInfoHandler

@synthesize currentUser = _currentUser;

- (id)init
{
    self = [super init];
    if (self) {
        userInfoApi_ = [[UserInfoApi alloc]init];
        
        _userDict = [[NSMutableDictionary alloc] init];
    }
    return self;
}

+ (UserInfoHandler *)sharedInstance
{
    static UserInfoHandler *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

/****************** current user *********************/
- (UserEntity *)currentUser
{
    if (_currentUser == nil) {
        
        NSString *uid = [UserDefaultsUtils valueWithKey:kUserId];
        UserDao *dao = [UserDao sharedInstance];
        _currentUser = (UserEntity *)[dao queryById:uid];
        
    }
    return _currentUser;
}

- (void)setCurrentUser:(UserEntity *)currentUser
{
    //保存uid
    [UserDefaultsUtils saveValue:currentUser.uid forKey:kUserId];
    //保存当前的状态
    [UserDefaultsUtils saveBoolValue:YES withKey:kIsLogin];
    
    //设置当前的用户
    NSDictionary *userDic = [currentUser toDictionary];
    _currentUser = [[UserEntity alloc]initWithDictionary:userDic];
}

- (void)getNewerHongBao
{
    //判断是否是第一次登录，如果是的话请求红包接口
    if ([_currentUser isFirstLogin]) {
        [self newerHongBao:^(id obj) {
            [[NSNotificationCenter defaultCenter]postNotificationName:kNewerHongbaoNotify object:obj];
        } failed:^(id obj) {
            
        }];
    }
}


/*********************** methods **********************/

- (void)uploadLifePhotoData:(NSData *)imgData uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))uploadProgressBlock success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ uploadLifePhotoData:imgData uploadProgressBlock:uploadProgressBlock context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            NSDictionary *dataDic = (NSDictionary *)resultDic[@"d"];
            NSString *key = dataDic[@"asset_key"];
            if (success) {
                success(key);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)uploadAvatarData:(NSData *)imgData success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ uploadAvatarData:imgData context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            NSDictionary *dataDic = (NSDictionary *)resultDic[@"d"];
            NSString *key = dataDic[@"asset_key"];
            if (success) {
                success(key);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)uploadLifePhotos:(NSArray *)photos success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ uploadLifePhotos:photos context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            NSDictionary *dataDic = resultDic[@"d"];
            
            NSString *errMsg = [AppUtils localizedCommonString:@"QM_Alert_Error"];
            
            UserEntity *user = [[UserEntity alloc]initWithDictionary:dataDic];
            
            UserDao *dao = [UserDao sharedInstance];
            UserEntity *oldEntity = (UserEntity *)[dao queryById:user.uid];
            if (oldEntity) {
                [dao removeClientEntityWithId:user.uid];
            }
            if ([dao insert:user]) {
                //登录成功
                [self setCurrentUser:user];
                [[NSNotificationCenter defaultCenter]postNotificationName:kUserInfoUpdateNotify object:nil];
                if (success) {
                    success(kHTTPTreatSuccess);
                }
            }
            else{
                DLog(@"insert user error");
                if(failed) failed(errMsg);
            }

        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)updateUserInfo:(UserEntity *)entity success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ updateUserAvatar:entity.avatar nickName:entity.nickname email:entity.email motto:entity.motto location:entity.location context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            NSDictionary *dataDic = resultDic[@"d"];
            
            
            NSString *errMsg = [AppUtils localizedCommonString:@"QM_Alert_Error"];
            
            UserEntity *user = [[UserEntity alloc]initWithDictionary:dataDic];
            
            UserDao *dao = [UserDao sharedInstance];
            UserEntity *oldEntity = (UserEntity *)[dao queryById:user.uid];
            if (oldEntity) {
                [dao removeClientEntityWithId:user.uid];
            }
            if ([dao insert:user]) {
                //登录成功
                [self setCurrentUser:user];
                
                if (success) {
                    [[NSNotificationCenter defaultCenter]postNotificationName:kUserInfoUpdateNotify object:nil];
                    success(kHTTPTreatSuccess);
                }
            }
            else{
                DLog(@"insert user error");
                if(failed) failed(errMsg);
            }
            
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)submitLocation:(NSString *)location success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ updateUserAvatar:@"" nickName:@"" email:@"" motto:@"" location:location context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)uploadContacts:(NSArray *)contacts success:(SuccessBlock)success failed:(FailedBlock)failed
{
    NSMutableDictionary * mbDict = [[NSMutableDictionary alloc] init];
    for (ContactorInfo * info in contacts) {
        NSString * cellPhone = [info.cellPhone stringByReplacingOccurrencesOfString:@"-" withString:@""];
        cellPhone= [cellPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
        [mbDict setObject:info.name forKey:cellPhone];
    }
//    for (APContact *contact in contacts) {
//        NSString *name = [NSString stringWithFormat:@"%@%@", contact.lastName, contact.firstName];
//        for (NSString *phone in contact.phones) {
//            NSString *p = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
//            [mbDict setObject:name forKey:p];
//        }
//    }
    
    [userInfoApi_ uploadContacts:mbDict context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)executeLoginWithUserName:(NSString *)userName password:(NSString *)password success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ executeLoginWithUserName:userName password:password context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = resultDic[@"d"];
            NSString *errMsg = [AppUtils localizedCommonString:@"QM_Alert_Error"];
            UserEntity *user = [[UserEntity alloc]initWithDictionary:dataDic];
            //创建当前用户的数据库
            [[QMDatabaseHelper sharedInstance] createDatabase:user.uid];
            
            UserDao *dao = [UserDao sharedInstance];
            UserEntity *oldEntity = (UserEntity *)[dao queryById:user.uid];
            if (oldEntity) {
                [dao removeClientEntityWithId:user.uid];
            }
            if ([dao insert:user]) {
                //登录成功
                [self setCurrentUser:user];
                [self getNewerHongBao];
                
                if (success) {
                    success(kHTTPTreatSuccess);
                }
            }
            else{
                DLog(@"insert user error");
                if(failed) failed(errMsg);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)executeRegisterWithMobile:(NSString *)mobile password:(NSString *)password checkCode:(NSString *)checkCode avatar:(NSString *)avatar nickname:(NSString *)nickname success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ executeRegisterWithMobile:mobile password:password checkCode:checkCode avatar:avatar nickname:nickname context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = resultDic[@"d"];
            NSString *errMsg = [AppUtils localizedCommonString:@"QM_Alert_Error"];
            UserEntity *user = [[UserEntity alloc]initWithDictionary:dataDic];
            [[QMDatabaseHelper sharedInstance]createDatabase:user.uid];
            UserDao *dao = [UserDao sharedInstance];
            
            //存数据库
            if ([dao insert:user]) {
                //注册成功
                [self setCurrentUser:user];
                [self getNewerHongBao];
                
                if (success) {
                    success(kHTTPTreatSuccess);
                }
            }
            else{
                DLog(@"save user error.");
                if (failed) {
                    failed(errMsg);
                }
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)executeChangePwdWithPwd:(NSString *)pwd oldPwd:(NSString *)oldPwd success:(SuccessBlock)success failed:(FailedBlock)failed
{
    UserEntity *user = [self currentUser];
    [userInfoApi_ executeChangePwdWithPwd:pwd oldPwd:oldPwd mobile:user.mobile context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)sendSMSWithSuccess:(SuccessBlock)success failed:(FailedBlock)failed
{
    UserEntity *user = [self currentUser];
    [self sendSMSWithMobile:user.mobile type:@"change_password" success:success failed:failed];
}

- (void)executeFindPwdWithNewPwd:(NSString *)pwd checkCode:(NSString *)checkCode phoneNum:(NSString *)phoneNum success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ executeFindPwdWithNewPwd:pwd checkCode:checkCode phoneNum:phoneNum context:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = resultDic[@"d"];
            NSString *errMsg = [AppUtils localizedCommonString:@"QM_Alert_Error"];
            UserEntity *user = [[UserEntity alloc]initWithDictionary:dataDic];
            [[QMDatabaseHelper sharedInstance]createDatabase:user.uid];
            UserDao *dao = [UserDao sharedInstance];
            
            UserEntity *oldEntity = (UserEntity *)[dao queryById:user.uid];
            if (oldEntity) {
                [dao removeClientEntityWithId:user.uid];
            }
            if ([dao insert:user]) {
                //找回密码成功
                [self setCurrentUser:user];
                [self getNewerHongBao];
                
                if (success) {
                    success(kHTTPTreatSuccess);
                }
            }
            else{
                DLog(@"insert user error");
                if(failed) failed(errMsg);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)registerDeviceToken:(NSString *)deviceToken
{
    [userInfoApi_ registerDeviceToken:deviceToken context:^(id result) {
        if ([result[@"s"] intValue] == 0) {
//            NSDictionary *dataDic = result[@"d"];
//            NSLog(dataDic);
//            NSString * string = [NSString stringWithFormat:@"s=%@", result[@"s"]];
//            TTAlertNoTitle(string);
        } else {
//            NSString * string = [NSString stringWithFormat:@"d=%@", result[@"d"]];
//            TTAlertNoTitle(string);
        }
    }];
}

- (void)loadUserBasicInfo:(SuccessBlock)sucess failed:(FailedBlock)failed
{
    [[QMDataCenter sharedDataCenter].chatUserHelper asyncLoadChatUserList:^(id result) {
        if(result && [result count] > 0) {
            NSArray * array = [NSArray arrayWithArray:result];
            for (UserBasicInfo * info in array) {
                [self updateBasicUserInfo:info];
            }
            
            if([array count] > 0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kReloadChatUserListNotify object:nil];
            }
        }
    }];
}

- (void)getUserBasicInfo:(NSArray *)userlist success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ getUserBasicInfo:userlist context:^(id result) {
        if (result && [result isKindOfClass:[NSDictionary class]]) {
            if ([result[@"s"] intValue] == 0) {
                id temp = [result objectForKey:@"d"];
                if (temp && [temp isKindOfClass:[NSDictionary class]]) {
                    UserBasicInfo * info = [[UserBasicInfo alloc] initWithDictionary:temp];
                    //数据库缓存
                    [[QMDataCenter sharedDataCenter].chatUserHelper asyncUpdateChatUser:nil userInfo:info];
                    
                    //更新内存
                    [self updateBasicUserInfo:info];
                    
                    NSDictionary * dict = @{@"userId":@(info.userId)};
                    [[NSNotificationCenter defaultCenter] postNotificationName:kReloadChatUserListNotify object:nil userInfo:dict];
                    
                } else if(temp && [temp isKindOfClass:[NSArray class]]) {
                    //多个用户
                    NSMutableArray * resultArray = [[NSMutableArray alloc] init];
                    for (NSDictionary * dict in temp) {
                        UserBasicInfo * info = [[UserBasicInfo alloc] initWithDictionary:dict];
                        [resultArray addObject:info];
                    }
                    
                    //数据库缓存
                    [[QMDataCenter sharedDataCenter].chatUserHelper asyncAddChatUserList:nil list:resultArray];
                    
                    //更新内存
                    for (UserBasicInfo * info in resultArray) {
                        [self updateBasicUserInfo:info];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:kReloadChatUserListNotify object:nil];
                }
            }
            else{
//                if (failed) {
//                    failed(result[@"d"]);
//                }
                DLog(@"-------- getUserBasicInfo failed...----------");
            }
        }
    }];
}

- (UserBasicInfo *)getBasicUserInfo:(uint64_t)userId
{
    return [_userDict objectForKey:@(userId)];
}

- (void)updateBasicUserInfo:(UserBasicInfo *)userInfo
{
    if(userInfo) {
        UserBasicInfo * info  = [self getBasicUserInfo:userInfo.userId];
        if(nil == info) {
            [_userDict setObject:userInfo forKey:@(userInfo.userId)];
        } else {
            info.userId = userInfo.userId;
            info.nickname = userInfo.nickname;
            info.icon = userInfo.icon;
            info.remark = userInfo.remark;
        }
    }
}

- (void)batchUploadLifePhoto:(NSArray *)filePaths success:(SuccessBlock)success failed:(FailedBlock)failed
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSArray *tmpArr = [filePaths copy];
        NSMutableArray *fileData = [NSMutableArray arrayWithCapacity:tmpArr.count];
        for (NSString *s in tmpArr) {
            //把小图片路径换成大图片路径
            NSData *data;
            NSRange range = [s rangeOfString:@"_small.jpg"];
            if (range.location != NSNotFound) {
                NSString *bigS = [s stringByReplacingOccurrencesOfString:@"_small.jpg" withString:@".jpg"];
                data = [NSData dataWithContentsOfFile:bigS];
            }
            //如果不是小图就将该path下的图片传上去
            else{
                data = [NSData dataWithContentsOfFile:s];
            }
            [fileData addObject:data];
        }
        [super batchUploadImageData:fileData type:@"life_photo" context:^(id resultDic) {
            NSArray *keys = (NSArray *)resultDic;
            if (keys.count != tmpArr.count) {
                DLog(@"keys num error!");
                if (failed) {
                    failed(@"keys num error!");
                }
            }
            else{
                NSMutableDictionary *keysAndFilePaths = [NSMutableDictionary dictionary];
                for (int i=0; i<keys.count; i++) {
                    [keysAndFilePaths setObject:keys[i] forKey:tmpArr[i]];
                }
                if (success) {
                    success(keysAndFilePaths);
                }
            }
        }];
    });
}

- (void)getOrderStateNum:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ getOrderStateNum:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            NSDictionary *dataDic = resultDic[@"d"];
            if (dataDic && [dataDic isKindOfClass:[NSDictionary class]]) {
                int salerNum = [dataDic[@"seller_num"] intValue]; //待发货
                int buyerNum = [dataDic[@"buyer_num"] intValue]; //待确认收货
                PersonOrderStatusNum *status = [[PersonOrderStatusNum alloc]initWithSalerNum:salerNum buyerNum:buyerNum];
                if (success) {
                    success(status);
                }
            }
            else{
                if (failed) {
                    failed(kHTTPTreatFailed);
                }
            }
        }
        else{
            if (failed) {
                failed(kHTTPTreatFailed);
            }
        }
    }];
}

- (void)userLogout:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ userLogout:^(id resultDic) {
        if ([resultDic[@"s"] intValue] == 0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)smsVerify:(NSString *)type mobile:(NSString *)mobile captcha:(NSString *)captcha success:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ smsVerify:type mobile:mobile captcha:captcha context:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            if (success) {
                success(kHTTPTreatSuccess);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

- (void)newerHongBao:(SuccessBlock)success failed:(FailedBlock)failed
{
    [userInfoApi_ newerHongBao:^(id resultDic) {
        if ([resultDic[@"s"]intValue] == 0) {
            NSDictionary *dataDic = [resultDic objectForKey:@"d"];
            id temp = [dataDic objectForKey:@"money"];
            if (temp && success) {
                success(temp);
            }
        }
        else{
            if (failed) {
                failed(resultDic[@"d"]);
            }
        }
    }];
}

@end
