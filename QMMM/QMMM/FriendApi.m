//
//  FriendApi.m
//  QMMM
//
//  Created by hanlu on 14-11-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "FriendApi.h"
#import "QMHttpClient.h"
#import "APIConfig.h"

@implementation FriendApi

- (void)getFriendList:(contextBlock)context
{
    __block  contextBlock bContext = context;
    NSString *urlS = [NSString stringWithFormat:API_GET_FRIEND_LIST, @"uid,nickname,remark,avatar,mobile"];
    __block NSString * url = [[self class] httpsRequestUrlWithPath:urlS];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [super processResponse:responseObject error:nil operation:operation context:bContext];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            DLog(@"--------getFriendList fail : %@", url);
            
            [super processResponse:nil error:error operation:operation context:bContext];
            
        }];
    });

}

- (void)inviteFriend:(NSString *)friendList isInviteAll:(BOOL)isInviteAll context:(contextBlock)context
{
    if (isInviteAll) {
        //统计邀请好友次数
        [AppUtils trackCustomEvent:@"inviteOneFriend_event" args:nil];
    }
    else{
        [AppUtils trackCustomEvent:@"inviteAllFriend_event" args:nil];
    }
    
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] httpsRequestUrlWithPath:API_INVITE_FRIEND];
    NSDictionary * params = @{@"mobile":friendList, @"all":@(isInviteAll)};
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [super processResponse:responseObject error:nil operation:operation context:bContext];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [super processResponse:nil error:error operation:operation context:bContext];
            DLog(@"--------inviteFriend fail : %@", url);
            
        }];
    });

}

- (void)acceptFriend:(uint64_t)fid token:(NSString *)token context:(contextBlock)context
{
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_ACCEPT_FRIEND];
    NSDictionary * params = @{@"fid":@(fid), @"token":token};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [super processResponse:responseObject error:nil operation:operation context:bContext];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            DLog(@"--------acceptFriend fail : %@", url);
            
            [super processResponse:nil error:error operation:operation context:bContext];
            
        }];
    });

}

- (void)modifyFriendRemark:(uint64_t)fid remark:(NSString *)remark context:(contextBlock)context
{
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_MODIFY_FRIEND_REMARK];
    NSDictionary * params = @{@"fid":@(fid), @"remark":remark};
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [super processResponse:responseObject error:nil operation:operation context:bContext];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            DLog(@"--------modifyFriendRemark fail : %@", url);
            
            [super processResponse:nil error:error operation:operation context:bContext];
            
        }];
    });
}

- (void)getProbablyKnowList:(contextBlock)context{
    __block  contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_GET_PROBABLYKNOW];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestGet parameters:nil prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                if (bContext) {
                    bContext(result);
                }
            }
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"getProbablyKnowList failure");
        }];
    });
}


@end
