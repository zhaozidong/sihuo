//
//  OrderGoodsPriceCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class OrderSimpleInfo;

extern NSString * const kOrderGoodsPriceCellIdentifier;

@interface OrderGoodsPriceCell : UITableViewCell

+ (UINib *)nib;

+ (CGFloat)cellHeight;

- (void)updateUI:(OrderSimpleInfo *)orderInfo;

@end
