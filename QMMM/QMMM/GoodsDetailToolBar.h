//
//  GoodsDetailToolBar.h
//  QMMM
//
//  Created by kingnet  on 14-9-18.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GoodsDetailToolBarDelegate <NSObject>

- (void)didTapChat; //聊天
- (void)didTapPurchase; //购买

@end

@interface GoodsDetailToolBar : UIView

@property (nonatomic, weak) id<GoodsDetailToolBarDelegate> delegate;
@property (nonatomic, assign) BOOL canPurchase;
@property (nonatomic, assign) BOOL canChat; //如果是自己则不显示聊天button

+ (GoodsDetailToolBar *)toolBar;

@end
