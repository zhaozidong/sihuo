//
//  QMReportGoodViewController.m
//  QMMM
//
//  Created by sh on 15/2/27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMReportViewController.h"

@interface QMReportViewController ()

@end

@implementation QMReportViewController


- (id)initWithType:(QMReportViewType)type
{
    self = [super init];
    if (self) {
        self.reportType = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];


    self.navigationItem.title = [AppUtils localizedProductString:@"QM_Text_Report"];
    self.navigationItem.leftBarButtonItems  = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
