//
//  QMReportGoodViewController.h
//  QMMM
//
//  Created by sh on 15/2/27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface QMReportViewController : BaseViewController

typedef NS_ENUM(NSInteger, QMReportViewType){
    QMReportPerson, //举报个人
    QMReportGood //举报商品
};

@property (nonatomic, assign) QMReportViewType reportType;

@end
