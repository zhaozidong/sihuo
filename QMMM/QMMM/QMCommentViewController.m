//
//  QMCommentViewController.m
//  QMMM
//
//  Created by hanlu on 14-11-11.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMCommentViewController.h"
#import "MsgHandle.h"
#import "QMCommentMsgCell.h"
#import "GoodsDetailViewController.h"
#import "NSString+JSONCategories.h"
#import "QMFailPrompt.h"

@implementation QMLoadingMoreHeadView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [self addSubview:_indicatorView];
        
        _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 49, frame.size.height)];
        [_textLabel setFont:[UIFont systemFontOfSize:12.0f]];
        [_textLabel setText:@"正在加载"];
        [self addSubview:_textLabel];
        
         self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat startX = (self.bounds.size.width - _indicatorView.bounds.size.width - _textLabel.bounds.size.width - 8) / 2;
    
    CGRect frame = _indicatorView.frame;
    frame.origin.x = startX;
    _indicatorView.frame = frame;
    
    frame = _textLabel.frame;
    frame.origin.x = _indicatorView.frame.origin.x + _indicatorView.frame.size.width + 8;
    _textLabel.frame = frame;
}

- (void)startLoading
{
    [_indicatorView startAnimating];
}

- (void)stopLoading
{
    [_indicatorView stopAnimating];
}

@end

@interface QMCommentViewController () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate> {
    NSMutableArray * _dataSource;
     BOOL _isBlankPage;
}

@end

@implementation QMCommentViewController

- (void)initNavBar
{
    self.title = [AppUtils localizedProductString:@"QM_Text_comment"];
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backBtnClick:)];
}

- (void)initTableView
{
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    if(kIOS7) {
        self.tableView.separatorInset = UIEdgeInsetsZero;
    }
    self.tableView.backgroundColor = kBackgroundColor;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initNavBar];
    
    [self initTableView];
    
    _dataSource = [[NSMutableArray alloc] init];
    
    __weak UITableView * weakTableView = self.tableView;
    __weak NSMutableArray * weakDataSource = _dataSource;
    __weak QMCommentViewController *weakSelf=self;
    
    [[MsgHandle shareMsgHandler] loadLocalMsgList:^(id result) {
        [weakDataSource removeAllObjects];
        if([result count] > 0) {
            [weakDataSource addObjectsFromArray:result];
        }
        
        if([weakDataSource count] == 0) {
            weakTableView.hidden=YES;
            _isBlankPage = YES;
            [weakSelf showPromptViewWithType:QMFailTypeNOData andText:@"还没有人评论过你的商品"];
            
        }else{  //消息数量为0就不加载数据
            [weakTableView reloadData];
        }

    } msgType:EMT_COMMENT];
    
    [[MsgHandle shareMsgHandler] updateMsgUnreadStatus:nil msgType:EMT_COMMENT unread:NO];
}

-(void)showPromptViewWithType:(QMFailType)failType andText:(NSString *)text{//显示提示信息
    QMFailPrompt *fail=[[QMFailPrompt alloc]initWithFailType:failType labelText:text buttonType:QMFailButtonTypeNone buttonText:nil];
    fail.frame=CGRectMake(0, kMainTopHeight, kMainFrameWidth, kMainFrameHeight-kMainTopHeight);
    [self.view addSubview:fail];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    _tableView.delegate = nil;
    _tableView.dataSource = nil;
    [_dataSource removeAllObjects];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)backBtnClick:(id)sender
{
//    _tableView.delegate = nil;
//    _tableView.dataSource = nil;
//    [_dataSource removeAllObjects];
    [AppUtils dismissHUD];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:kFlushMsgUnreadCountNotify object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(_isBlankPage) {
        return 0;
    } else {
        return [_dataSource count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell * retCell = nil;
    if(!_isBlankPage) {
        QMCommentMsgCell * cell = [tableView dequeueReusableCellWithIdentifier:kQMCommentMsgCellIdentifier];
        if(nil == cell) {
            cell = [QMCommentMsgCell cellForCommentMsg];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        MsgData * msgdata = [_dataSource objectAtIndex:indexPath.section];
        [cell updateUI:msgdata];
        
        retCell = cell;
    }
    
    return retCell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    MsgData * msgdata = [_dataSource objectAtIndex:indexPath.section];
//    return [QMCommentMsgCell heightForCommentMsg:msgdata];
    
    if(_isBlankPage) {
        if(kIOS7) {
            return _tableView.bounds.size.height - 64;
        } else {
            return _tableView.bounds.size.height;
        }
    } else {
        return 110;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(_isBlankPage) {
        return CGFLOAT_MIN;
    } else {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(!_isBlankPage) {
        MsgData * msgdata = [_dataSource objectAtIndex:indexPath.section];
        NSDictionary * dict = [msgdata.extData toDict];
        id  obj = [dict objectForKey:@"gid"];
        uint64_t goodsId = 0;
        if([obj isKindOfClass:[NSString class]]) {
            goodsId = (uint64_t)[obj longLongValue];
        } else if([obj isKindOfClass:[NSNumber class]]) {
            goodsId = [obj unsignedLongLongValue];
        }
        
        if(goodsId) {
            GoodsDetailViewController * viewController = [[GoodsDetailViewController alloc] initWithGoodsId:goodsId];
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    NSLog(@"contentoffset.y = %f", _tableView.contentOffset.y);
    
//    CGFloat contentOffsetY = _tableView.contentOffset.y;
//    CGFloat contentInsetY = -_tableView.contentInset.top;
//    
//    if(_tableView.contentSize.height > _tableView.bounds.size.height || contentOffsetY < contentInsetY) {
//        NSLog(@"contentOffsetY = %f", contentOffsetY);
//    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}



@end
