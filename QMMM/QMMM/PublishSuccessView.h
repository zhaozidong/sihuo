//
//  PublishSuccessView.h
//  QMMM
//
//  Created by kingnet  on 14-10-2.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PublishSuccessViewDelegate <NSObject>

- (void)shareToWeiXin;

- (void)shareToWeiXinFriends;

- (void)shareToWeiBo;

- (void)shareToQQZone;

- (void)copyUrl;

- (void)inviteMoreFriends;

- (void)checkGoods; //查看刚发布完的商品



@end
@interface PublishSuccessView : UIView

@property (nonatomic, weak) id<PublishSuccessViewDelegate> delegate;

@property (nonatomic, assign) NSUInteger toNum;

+ (PublishSuccessView *)publishView;

@end
