//
//  CommentEntity.m
//  QMMM
//
//  Created by kingnet  on 14-9-19.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "CommentEntity.h"


@implementation CommentEntity

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        id temp = [dict objectForKey:@"id"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.cid = [temp unsignedLongLongValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.cid = [temp longLongValue];
        }
        
        temp = [dict objectForKey:@"gid"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.gid = [temp unsignedLongLongValue];
        }
        
        temp = [dict objectForKey:@"uid"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.cUserId = (uint64_t)[temp longLongValue];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            self.cUserId = [temp unsignedLongLongValue];
        }
        
        temp = [dict objectForKey:@"pid"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.pid = [temp longLongValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.pid = [temp longLongValue];
        }
        
        temp = [dict objectForKey:@"puid"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.puid = [temp longLongValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.puid = (uint64_t)[temp longLongValue];
        }
        
        self.comments = [dict objectForKey:@"comments"];
        if (!self.comments && [self.comments isKindOfClass:[NSNull class]]) {
            self.comments = @"";
        }
        
        temp = [dict objectForKey:@"comment_ts"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.comments_ts = [temp intValue];
        }
        
        self.cNickname = [dict objectForKey:@"nickname"];
        if (!self.cNickname || [self.cNickname isKindOfClass:[NSNull class]]) {
            self.cNickname = @"";
        }
        
        self.pnickname = [dict objectForKey:@"pnickname"];
        if (!self.pnickname || [self.pnickname isKindOfClass:[NSNull class]]) {
            self.pnickname = @"";
        }
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"cid:%qd, gid:%qd, cUserId:%qd, pid:%qd, comments:%@, comments_ts:%d, cNickname:%@ pnickname:%@", self.cid, self.gid, self.cUserId, self.pid, self.comments, self.comments_ts, self.cNickname, self.pnickname];
}

@end
