//
//  NSDate+Additions.m
//  QMMM
//
//  Created by hanlu on 14-9-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "NSDate+Additions.h"

@implementation NSDate(Addition)

+ (NSInteger)secondsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    NSInteger result = 0;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    if (calendar) {
        [calendar rangeOfUnit:NSSecondCalendarUnit startDate:&fromDate
                     interval:NULL forDate:fromDateTime];
        [calendar rangeOfUnit:NSSecondCalendarUnit startDate:&toDate
                     interval:NULL forDate:toDateTime];
        
        NSDateComponents *difference = [calendar components:NSSecondCalendarUnit
                                                   fromDate:fromDate toDate:toDate options:0];
        if (difference) {
            result = [difference second];
        }
    }
    return result;
}

@end
