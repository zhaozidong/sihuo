//
//  AppInfo.h
//  QMMM
//  系统信息，主要用于分享本APP
//  Created by kingnet  on 14-11-6.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseEntity.h"

@interface AppInfo : BaseEntity

@property (nonatomic, strong)NSString *appTitle; //分享的title

@property (nonatomic, strong)NSString *appDesc; //分享的描述

@property (nonatomic, strong)NSString *linker; //分享的连接（下载地址）

@property (nonatomic, strong)UIImage *appImage; //app的icon

- (id)initWithAPPTitle:(NSString *)appTitle appDesc:(NSString *)appDesc appLinker:(NSString *)appLinker appImage:(UIImage *)appImage;

+ (AppInfo *)shareToWeiXinApp;

@end
