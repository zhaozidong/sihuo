//
//  QMFailPrompt.m
//  QMMM
//
//  Created by Derek.zhao on 14-12-9.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMFailPrompt.h"

@implementation QMFailPrompt{
    
    NSString *_failMessage;
    UIImageView *_imagePrompt;
    QMFailType _failType;
    UILabel *_lblMessage;
    QMErrorShowType _showType;
    UIButton *_button;
    QMFailButtonType _buttonType;
    
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

//- (void)drawRect:(CGRect)rect {
//
//    
//}


-(void)addImageView{
    
    switch (_failType) {
        case QMFailTypeNOData:
            _imagePrompt=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"errorLoadFail"]];
            break;
        case QMFailTypeLoadFail:
            _imagePrompt=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"errorLoadFail"]];
            break;
        case QMFailTypeNoNetWork:
            _imagePrompt=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"errorNoNetWork"]];
        default:
            break;
    }
    [self addSubview:_imagePrompt];
}


//-(id)initWithFailType:(QMFailType)failType andLabelText:(NSString *)failMessage andHasButton:(BOOL)hasButton{
//    self=[super init];
//    if (!self) {
//        return nil;
//    }
//    _failType=failType;
//    _failMessage=failMessage;
//    _hasButton=hasButton;
//    [self addImageView];
//
//    
//    _lblMessage=[[UILabel alloc] init];
//    _lblMessage.backgroundColor=[UIColor clearColor];
//    _lblMessage.textAlignment=NSTextAlignmentCenter;
//    _lblMessage.textColor=[UIColor lightGrayColor];
//    _lblMessage.font = [UIFont systemFontOfSize:15.f];
//    [_lblMessage sizeToFit];
//    _lblMessage.text=failMessage;
//    [self addSubview:_lblMessage];
//    
//    if (hasButton) {
//        _button=[[UIButton alloc] init];
//        _button.frame=CGRectMake(0, 0, 80, 30);
//        [_button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//        [_button setTitle:@"重新加载" forState:UIControlStateNormal];
//        UIImage *image=[[UIImage imageNamed:@"btnReload_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 16)];
//        [_button setBackgroundImage:image forState:UIControlStateNormal];
//        [_button addTarget:self action:@selector(btnDidClick:) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:_button];
//    }
//    
//    self.backgroundColor=kBackgroundColor;
//    return self;
//}


-(id)initWithFailType:(QMFailType)failType labelText:(NSString *)labelText buttonType:(QMFailButtonType)buttonType buttonText:(NSString *)buttonText{
    self=[super init];
    if (self) {
        _failType=failType;
        _failMessage=labelText;
        _buttonType=buttonType;
        [self addImageView];
        
        _lblMessage=[[UILabel alloc] init];
        _lblMessage.backgroundColor=[UIColor clearColor];
        _lblMessage.textAlignment=NSTextAlignmentCenter;
        _lblMessage.textColor=[UIColor lightGrayColor];
        _lblMessage.font = [UIFont systemFontOfSize:15.f];
        [_lblMessage sizeToFit];
        _lblMessage.text=_failMessage;
        [self addSubview:_lblMessage];
        
        switch (buttonType) {
            case QMFailButtonTypeNone:
                break;
            case QMFailButtonTypeFail:
            {
                _button=[[UIButton alloc] init];
                _button.frame=CGRectMake(0, 0, 80, 30);
                [_button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                [_button setTitle:@"重新加载" forState:UIControlStateNormal];
                UIImage *image=[[UIImage imageNamed:@"btnReload_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 16)];
                [_button setBackgroundImage:image forState:UIControlStateNormal];
                [_button addTarget:self action:@selector(btnDidClick:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:_button];
                break;
            }
            case QMFailButtonTypeNormal:
            {
                _button=[[UIButton alloc] init];
                _button.frame=CGRectMake(0, 0, 80, 30);
                _button.titleLabel.font=[UIFont systemFontOfSize:15.0f];
                [_button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                [_button setTitle:buttonText forState:UIControlStateNormal];
                [_button addTarget:self action:@selector(btnDidClick:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:_button];
                break;
            }
            default:
                break;
        }
    }
    return self;
}



-(void)btnDidClick:(UIButton *)sender{
    if (_delegate && [_delegate respondsToSelector:@selector(placeHolderButtonClick:)]) {
        [_delegate placeHolderButtonClick:sender];
    }
}


- (void)layoutSubviews
{
//    CGFloat width=self.frame.size.width - 30*2;
//    CGRect frame=CGRectMake(0, 0, 130 , 130);
//    _imagePrompt.frame=frame;
    
    CGPoint center;
    center.y =self.bounds.size.height*0.35;
    center.x=self.bounds.size.width/2;
    _imagePrompt.center=center;
    

    _lblMessage.frame=CGRectMake(0, 0, self.bounds.size.width, 50);
    center.y=_imagePrompt.frame.origin.y+_imagePrompt.frame.size.height+15;
    _lblMessage.center=center;

//    if (_hasButton) {
//        _button.frame=CGRectMake(0, 0, 80, 30);
//        center.y=_lblMessage.frame.origin.y+_lblMessage.frame.size.height+20;
//        _button.center=center;
//    }
    
    if (_buttonType==QMFailButtonTypeNormal || _buttonType==QMFailButtonTypeFail) {
        _button.frame=CGRectMake(0, 0, 100, 40);
        center.y=CGRectGetMaxY(_lblMessage.frame)+20;
        _button.center=center;
    }
}

@end
