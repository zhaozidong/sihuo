//
//  MyOrderList.m
//  QMMM
//
//  Created by hanlu on 14-9-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "MyOrderList.h"
#import "OrderDataInfo.h"
#import "GoodsApi.h"
#import "GoodsHandler.h"
#import "GoodsDataHelper.h"

@interface MyOrderList () {
    
    // 服务端订单列表
    NSMutableArray      *_serverOrderList;
    // 本地缓存订单列表
    NSMutableArray      *_localOrderList;
    
    // 加载状态
    NSInteger _serverLoadState;
    NSInteger _localLoadState;
    
    // 是否已加载过
    BOOL _hasServerListLoaded;
    BOOL _hasLocalListLoaded;
    
    // 是否是从服务端拉取第一页（可能是第一次或重新拉取第一页）
    BOOL _serverFirstPageLoading;
    
    //缓存类型
    EControllerType _cacheType;
}
@end

@implementation MyOrderList

//缓存对象类型
- (id)initWithType:(EControllerType)type
{
    self = [super init];
    if(self) {
        _serverOrderList = [[NSMutableArray alloc] initWithCapacity:kDefaultGoodsSize];
        _localOrderList = [[NSMutableArray alloc] initWithCapacity:kDefaultGoodsSize];
        
        _hasServerListLoaded = NO;
        _hasLocalListLoaded = NO;
        
        _cacheType = type;
    }
    return self;
}

#pragma mark -

- (NSArray *)cacheList
{
    if (_hasServerListLoaded)
        return _serverOrderList;
    else
        return _localOrderList;
}

- (BOOL)loading
{
    return _localLoadState == kTTGoodsLoadStateLoading || _serverLoadState == kTTGoodsLoadStateLoading;
}

- (BOOL)endOfAll
{
    if (_hasServerListLoaded)
        return _serverLoadState == kTTGoodsLoadStateLoadEnd;
    else
        return NO;
}

- (BOOL)hasAnyLoaded
{
    @synchronized(self) {
        return _hasServerListLoaded || _hasLocalListLoaded;
    }
}

- (BOOL)loadFailed
{
    return _localLoadState == kTTGoodsLoadStateLoadFail || _serverLoadState == kTTGoodsLoadStateLoadFail;
}

- (void)deleteGoods:(uint64_t)goodsId
{
    for (int i = 0; i < [_serverOrderList count]; i ++) {
        GoodsSimpleInfo *info = [_serverOrderList objectAtIndex:i];
        if(info.gid == goodsId) {
            [_serverOrderList removeObjectAtIndex:i];
            break;
        }
    }
    
    for (int i = 0; i < [_localOrderList count]; i ++) {
        GoodsSimpleInfo *info = [_localOrderList objectAtIndex:i];
        if (info.gid == goodsId) {
            [_localOrderList removeObjectAtIndex:i];
            break;
        }
    }
}

#pragma mark -

- (BOOL)loadNextPage:(BOOL)firstPage
{
    BOOL resultLocal = NO;
    BOOL resultServer = NO;
    
    @synchronized(self) {
        if (firstPage) { //拉取首页
            if (!_hasServerListLoaded && !_hasLocalListLoaded) {
                //本地与服务端都未拉取过
                resultLocal = [self loadNextPageFromLocal:YES];
            }
            //曾拉取过，则重新从服务端拉取
            resultServer = [self loadNextPageFromServer:YES];
        }
        else {//非首页
            if (!_hasServerListLoaded) { //且未从服务端拉取过，则再次从本地读取
                if (_localLoadState != kTTGoodsLoadStateLoading &&
                    _localLoadState != kTTGoodsLoadStateLoadEnd) {
                    resultLocal = [self loadNextPageFromLocal:NO];
                }
            }
            
            //如果服务端数据未拉取完毕，则继续分页拉取
            if (_serverLoadState != kTTGoodsLoadStateLoading &&
                _serverLoadState != kTTGoodsLoadStateLoadEnd) {
                if (!_hasServerListLoaded)
                    resultServer = [self loadNextPageFromServer:YES];
                else
                    resultServer = [self loadNextPageFromServer:NO];
            }
        }
    }
    
    return resultLocal || resultServer;
}

- (void)changeLoadState:(NSInteger)localState serverState:(NSInteger)serverState
{
    BOOL bStateChanged = NO;
    
    if (_localLoadState != localState) {
        bStateChanged = YES;
        _localLoadState = localState;
    }
    if (_serverLoadState != serverState) {
        bStateChanged = YES;
        _serverLoadState = serverState;
    }
    
    if (bStateChanged) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyCacheListLoadStateNotify object:nil];
    }
}

- (void)changeLocalLoadState:(NSInteger)newState
{
    [self changeLoadState:newState serverState:_serverLoadState];
}

- (void)changeServerLoadState:(NSInteger)newState
{
    [self changeLoadState:_localLoadState serverState:newState];
}

#pragma mark - server goodslist load logic

- (BOOL)loadNextPageFromServer:(BOOL)firstPage
{
    NSString *lastGoodsId = @"0";
    
    @synchronized(_serverOrderList) {
        if (_serverLoadState == kTTGoodsLoadStateLoading) { //正在加载
            DLog(@"loadNextPageFromServer fail, it's still loading...");
            return NO;
        }
        
        if (!firstPage && _serverLoadState == kTTGoodsLoadStateLoadEnd) {//分页加载完毕
            DLog(@"getNextPageGoodsListFromServer fail, it's end");
            return NO;
        }
        
        _serverFirstPageLoading = firstPage;
        
        [self changeLoadState:_localLoadState serverState:kTTGoodsLoadStateLoading];
        
        if (!_serverFirstPageLoading && _serverOrderList.count > 0) {
            if(_cacheType == ECT_ORDER_MGR || _cacheType == ECT_BUYED_GOODS) {
                OrderSimpleInfo * info = [_serverOrderList lastObject];
                lastGoodsId = info.order_id;
            } else {
                GoodsSimpleInfo * info = [_serverOrderList lastObject];
                lastGoodsId = [NSString stringWithFormat:@"%qu", info.gid];
            }
        }
        else if(_localOrderList.count > 0){
            if(_cacheType == ECT_ORDER_MGR || _cacheType == ECT_BUYED_GOODS) {
                OrderSimpleInfo * info = [_localOrderList lastObject];
                lastGoodsId = info.order_id;
            } else {
                GoodsSimpleInfo * info = [_localOrderList lastObject];
                lastGoodsId = [NSString stringWithFormat:@"%qu", info.gid];
            }
        }
    }
    
    if(_cacheType == ECT_ORDER_MGR) {
        //必填
        [[GoodsApi shareInstance] getOrderList:1 orderId:lastGoodsId context:^(id result) {
            [self loadNextPageFromServerCallback:result];
        }];
    } else if(_cacheType == ECT_BUYED_GOODS) { //已买的
        [[GoodsApi shareInstance] getOrderList:2 orderId:lastGoodsId context:^(id result) {
            [self loadNextPageFromServerCallback:result];
        }];
    } else if(_cacheType == ECT_GOODS_MGR) {
        [[GoodsApi shareInstance] getBoughtList:lastGoodsId context:^(id result) {
            [self loadNextPageFromServerCallback:result];
        }];
    } else if(_cacheType == ECT_FAVOR_GOODS) {
        [[GoodsApi shareInstance] getFavorList:lastGoodsId context:^(id result) {
            [self loadNextPageFromServerCallback:result];
        }];
    }
    
    return YES;
}

- (void)loadNextPageFromServerCallback:(id)params
{
    BOOL bLocalListChanged = NO;
    BOOL bServerListChanged = NO;
    
    @synchronized(_serverOrderList) {
        if(_serverLoadState != kTTGoodsLoadStateLoading) {
            NSLog(@"loadNextPageFromServerCallback ignore, it's not loading...");
            return;
        }
        
        if([[params objectForKey:@"s"] intValue] == 0) {
            id result = [params objectForKey:@"d"];
            
            if(result && [result isKindOfClass:[NSArray class]]) {
                bServerListChanged = YES;
                
                NSArray * tempArray = [NSArray arrayWithArray:result];
                
                // 如果是从服务端拉到首页数据，清空本地缓存记录
                BOOL isNew = !_hasServerListLoaded || _serverFirstPageLoading;
                _hasServerListLoaded = YES;
                if (isNew) {
                    @synchronized(_localOrderList) {
                        bLocalListChanged = [_localOrderList count] != 0;
                        [_localOrderList removeAllObjects];
                        _hasLocalListLoaded = NO;
                        [self changeLoadState:kTTGoodsLoadStateNone serverState:_serverLoadState];
                        
                        // 从数据库中删除所有数据
                        [[GoodsHandler shareGoodsHandler].goodsHelper asyncDeleteAllMyList:nil type:_cacheType];
                        //[[GoodsHandler shareGoodsHandler].goodsHelper asyncDeleteAllRecommendList:nil];
                    }
                    // TODO: 这里以后是否可以考虑做合并处理？
                    // 清空Goods列表
                    [_serverOrderList removeAllObjects];
                }
                
                // 保存数据到数据库
                if([tempArray count] > 0) {
                    [[GoodsHandler shareGoodsHandler].goodsHelper asyncAddMyList:nil type:_cacheType list:tempArray];
                }
                
                // 加入servergoods缓存列表
                for (NSDictionary * dict in tempArray) {
                    if(_cacheType == ECT_ORDER_MGR || _cacheType == ECT_BUYED_GOODS) {
                        OrderSimpleInfo * orderInfo = [[OrderSimpleInfo alloc] initWithDictionary:dict];
                        [_serverOrderList addObject:orderInfo];
                    } else {
                        GoodsSimpleInfo * info = [[GoodsSimpleInfo alloc] initWithDictionary:dict];
                        [_serverOrderList addObject:info];
                    }
                }
                
                _serverFirstPageLoading = NO;
                
                //为空，则表示服务端已分页加载到最后一页了
                BOOL bEnd = [tempArray count] == 0;
                [self changeLoadState:_localLoadState serverState:bEnd ? kTTGoodsLoadStateLoadEnd : kTTGoodsLoadStateLoadDone];
            } else {
                _hasServerListLoaded = YES;
                _serverFirstPageLoading = NO;
                [self changeLoadState:_localLoadState serverState:kTTGoodsLoadStateLoadFail];
            }
            
        } else {
            [AppUtils showErrorMessage:[params objectForKey:@"d"]];
            //失败
            _hasServerListLoaded = YES;
            _serverFirstPageLoading = NO;
            [self changeLoadState:_localLoadState serverState:kTTGoodsLoadStateLoadFail];
        }
    }
    
    // 通知UI
    if (bLocalListChanged || bServerListChanged) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kMyCacheListLoadedNotify object:nil];
    }
}

#pragma mark - local goodslist load logic

- (BOOL)loadNextPageFromLocal:(BOOL)firstPage
{
    if (_hasServerListLoaded) {
        NSLog(@"loadNextPageFromLocal fail, goods already load from server...");
        return NO;
    }
    
    NSString * lastGoodsId = @"0";
    
    @synchronized(_localOrderList) {
        if (_localLoadState == kTTGoodsLoadStateLoading) {
            NSLog(@"loadNextPageFromLocal fail, it's still loading...");
            return NO;
        }
        
        if (_localLoadState == kTTGoodsLoadStateLoadEnd) {
            NSLog(@"loadNextPageFromLocal fail, it's end");
            return NO;
        }
        
        [self changeLocalLoadState:kTTGoodsLoadStateLoading];
        
        if (_localOrderList.count > 0) {
            if(_cacheType == ECT_ORDER_MGR || _cacheType == ECT_BUYED_GOODS) {
                OrderSimpleInfo * info = _localOrderList.lastObject;
                lastGoodsId = info.order_id;
            } else {
                GoodsSimpleInfo * info = _localOrderList.lastObject;
                lastGoodsId = [NSString stringWithFormat:@"%qu", info.gid];
            }
        }
    }
    
    // 从数据库加载
    [[GoodsHandler shareGoodsHandler].goodsHelper asyncLoadMyList:^(id result) {
        [self loadNextPageFromLocalCallback:result];
    } type:_cacheType fromId:lastGoodsId count:kDefaultGoodsSize];

//    [[GoodsHandler shareGoodsHandler].goodsHelper asyncLoadRecommendList:^(id result) {
//        [self loadNextPageFromLocalCallback:result];
//    } fromId:lastGoodsId count:kDefaultGoodsSize];
    
    return YES;
}

- (void)loadNextPageFromLocalCallback:(id)result
{
    @synchronized(_localOrderList) {
        if (_localLoadState != kTTGoodsLoadStateLoading || _hasServerListLoaded) {
            NSLog(@"getLocalFeedListFromDBCallback ignore, it's not loading...");
            return;
        }
        
        if (!_hasLocalListLoaded) {
            _hasLocalListLoaded = result != nil;
        }
        
        if(result && [result isKindOfClass:[NSArray class]]) {
            BOOL bEnd = NO;
            for (int i = 0; i < [result count]; i++) {
                id data = [result objectAtIndex:i];
                // 最后一条为null说明本地没有更多缓存了
                if([data isMemberOfClass:[NSNull class]]) {
                    bEnd = YES;
                    break;
                }
                
                
                [_localOrderList addObject:data];
            }
            
//            //按updatetime排序
//            if(_cacheType == ECT_GOODS_MGR) {
//                [_localOrderList sortUsingFunction:goodsSimpeInfoSortByUpdateTime context:nil];
//            }
 
            [self changeLocalLoadState:bEnd ? kTTGoodsLoadStateLoadEnd : kTTGoodsLoadStateLoadDone];
        } else {
            [self changeLocalLoadState:kTTGoodsLoadStateLoadFail];
        }
    }
    
    // 通知UI
    [[NSNotificationCenter defaultCenter] postNotificationName:kMyCacheListLoadedNotify object:nil];
}

//static NSInteger goodsSimpeInfoSortByUpdateTime(id info, id info2, void *reverse) {
//    
//    GoodsSimpleInfo * simpleInfo = info;
//    GoodsSimpleInfo * simpleInfo2 = info2;
//    
//    if(simpleInfo.update_time > simpleInfo2.update_time) {
//        return NSOrderedAscending;
//    } else if(simpleInfo.update_time == simpleInfo2.update_time) {
//        return NSOrderedSame;
//    } else {
//        return NSOrderedDescending;
//    }
//}

- (void)updateSimpleGoodsInfo:(GoodsSimpleInfo *)info
{
    if(info) {
        NSDictionary * dict = [info toDictionary];
        [[GoodsHandler shareGoodsHandler].goodsHelper asyncupdateGoodsSimpleInfo:nil type:ECT_GOODS_MGR dict:dict];
    }
}

@end
