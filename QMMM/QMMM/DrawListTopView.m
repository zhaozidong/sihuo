//
//  DrawListTopView.m
//  QMMM
//
//  Created by kingnet  on 15-2-5.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "DrawListTopView.h"
#import "FinanceDataInfo.h"

@interface DrawListTopView ()
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UIButton *rulesBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineHeightCst;
@property (weak, nonatomic) IBOutlet UILabel *textLbl;

@end
@implementation DrawListTopView

+ (DrawListTopView *)topView
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"DrawListTopView" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.moneyLbl.textColor = kDarkTextColor;
    self.moneyLbl.text = @"";
    self.textLbl.text = @"";
    self.rulesBtn.hidden = YES;
    self.lineHeightCst.constant = 0.5;
}

- (void)updateUI:(FinanceUserInfo *)info isBalance:(BOOL)isBalance
{
    if (isBalance) {
        self.moneyLbl.text = [NSString stringWithFormat:@"¥ %.2f", info.amount];
        self.textLbl.text = @"现金";
    }
    else{
        self.moneyLbl.text = [NSString stringWithFormat:@"¥ %.2f", info.hongbao];
        self.rulesBtn.hidden = NO;
        self.textLbl.text = @"红包";
    }
}

+ (CGFloat)viewHeight
{
    return 85.f;
}

- (IBAction)clickRulesAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapRulesBtn)]) {
        [self.delegate didTapRulesBtn];
    }
}

@end
