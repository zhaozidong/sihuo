//
//  OrderStatusCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodDataInfo.h"

@class OrderSimpleInfo;

extern NSString * const kOrderStatusCellIdentifier;

@protocol OrderStatusCellDelegate <NSObject>

- (void)didTapButton:(NSString *)orderId;

- (void)didTapCheckExpress:(NSString *)orderId;

@end
@interface OrderStatusCell : UITableViewCell

@property (nonatomic, weak) id<OrderStatusCellDelegate> delegate;

+ (UINib *)nib;

- (void)updateUI:(OrderSimpleInfo *)orderInfo cellType:(EControllerType)cellType;

+ (CGFloat)cellHeight;

@end
