//
//  QMContactHelper.m
//  QMMM
//
//  Created by hanlu on 14-10-21.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMContactHelper.h"
#import "QMSqlite.h"
#import "ContactorInfo.h"
#import "QMDatabaseHelper.h"

@implementation QMContactHelper

- (void)setWorkThread:(NSThread *)aThread
{
    @synchronized(self) {
        if (_workThread != aThread) {
            _workThread = aThread;
        }
    }
}


//[database executeUpdate:@"CREATE TABLE IF NOT EXISTS qmContactList (" \
// "cellPhone TEXT PRIMARY KEY NOT NULL, " \
// "name TEXT);"];

- (NSArray *)loadContactList:(FMDatabase *)database
{
    NSMutableArray * list = [[NSMutableArray alloc] initWithCapacity:1];
    
    if(database) {
        @autoreleasepool {
            FMResultSet * resultSet = [database executeQuery:@"SELECT * FROM qmContactList;"];
            while ([resultSet next]) {
                ContactorInfo * info = [[ContactorInfo alloc] initWithFMResult:resultSet];
                [list addObject:info];
            }
        }
    }
    
    return list;
}

- (void)addContactList:(FMDatabase *)database list:(NSArray *)userList
{
    if(database && [userList count] > 0) {
        NSArray * array = [NSArray arrayWithArray:userList];
        for (ContactorInfo * data in array) {
            [self updateContact:database userInfo:data];
        }
    }
}

- (void)updateContact:(FMDatabase *)database userInfo:(ContactorInfo *)userInfo
{
    if(database && userInfo) {
        [database executeUpdateWithFormat:@"REPLACE INTO qmContactList (cellPhone, name) VALUES (%@, %@);", userInfo.cellPhone, userInfo.name];
    }
}

- (void)deleteContact:(FMDatabase *)database cellPhone:(NSString *)cellPhone
{
    if(database && [cellPhone length] > 0) {
        [database executeUpdateWithFormat:@"DELETE FROM qmContactList WHERE cellPhone = %@", cellPhone];
    }
}

- (void)deleteAllContact:(FMDatabase *)database
{
    if(database) {
        [database executeUpdate:@"DELETE FROM qmContactList;"];
    }
}

- (void)asyncLoadContactList:(contextBlock)context
{
    if(_workThread && context) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:bContext, nil];
        [self performSelector:@selector(threadLoadContactList:)
                     onThread:_workThread
                   withObject:array
                waitUntilDone:NO];
    }
    else {
        if (context) {
            context(nil);
        }
    }
}
- (void)threadLoadContactList:(NSArray *)params
{
    if(params && [params count] > 0) {
        contextBlock context = [params objectAtIndex:0];
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        NSArray * result = [self loadContactList:helper.database];
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(result);
            });
        }
    }
}

- (void)asyncAddContactList:(contextBlock)context list:(NSArray *)userList
{
    if(_workThread && userList) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:userList, bContext, nil];
        [self performSelector:@selector(threadAddContactList:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadAddContactList:(NSArray *)params
{
    if([params count] >= 1) {
        NSArray * data = [params objectAtIndex:0];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self addContactList:helper.database list:data];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}


- (void)asyncUpdateContact:(contextBlock)context userInfo:(ContactorInfo *)userInfo
{
    if(_workThread && userInfo) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:userInfo, bContext, nil];
        [self performSelector:@selector(threadUpdateContact:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadUpdateContact:(NSArray *)params
{
    if(params && [params count] >= 1) {
        ContactorInfo * userInfo = (ContactorInfo *)[params objectAtIndex:0];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self updateContact:helper.database userInfo:userInfo];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
        
    }
}


- (void)asyncDeleteContact:(contextBlock)context cellPhone:(NSString *)cellPhone
{
    if(_workThread && [cellPhone length] > 0) {
        __strong contextBlock bContext = context;
        NSArray * array = [NSArray arrayWithObjects:cellPhone, bContext, nil];
        [self performSelector:@selector(threadDeleteContact:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
-(void)threadDeleteContact:(NSArray *)params
{
    if(params && [params count] > 0) {
        NSString * cellPhone = [params objectAtIndex:0];
        contextBlock context = nil;
        if([params count] > 1) {
            context = [params objectAtIndex:1];
        }
        
        QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
        [self deleteContact:helper.database cellPhone:cellPhone];
        
        if(context) {
            dispatch_async(dispatch_get_main_queue(), ^{
                context(@(YES));
            });
        }
    }
}

- (void)asyncDeleteAllContact:(contextBlock)context
{
    if(_workThread) {
        NSArray * array = nil;
        if(context) {
            __strong contextBlock bContext = context;
            array = [NSArray arrayWithObjects:bContext, nil];
        }
        [self performSelector:@selector(threadDeleteAllContact:) onThread:_workThread withObject:array waitUntilDone:NO];
    }
}
- (void)threadDeleteAllContact:(NSArray *)params
{
    contextBlock context = nil;
    if([params count] > 0) {
        context = [params objectAtIndex:0];
    }
    
    QMDatabaseHelper * helper = [QMDatabaseHelper sharedInstance];
    [self deleteAllContact:helper.database];
    
    if(context) {
        dispatch_async(dispatch_get_main_queue(), ^{
            context(@(YES));
        });
    }
}

@end
