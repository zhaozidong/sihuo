//
//  QMDataCenter.m
//  QMMM
//
//  Created by hanlu on 14-10-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMDataCenter.h"
#import "FMDatabaseAdditions.h"
#import "QMSqlite.h"
#import "KNThread.h"
#import "QMDatabaseHelper.h"
#import "GoodsDataHelper.h"
#import "QMChatUserHelper.h"
#import "QMContactHelper.h"
#import "QMFriendHelper.h"
#import "QMMsgHelper.h"

@interface QMDataCenter () <KNThreadDelegate> {
     BOOL _isWorkThreadStarted;
}

@end

@implementation QMDataCenter

static QMDataCenter* _shareInstance = NULL;

+ (QMDataCenter *)sharedDataCenter
{
    @synchronized([QMDataCenter class]) {
        if (nil == _shareInstance) {
            _shareInstance = [[QMDataCenter alloc] init];
        }
        return _shareInstance;
    }
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (_shareInstance == nil) {
            _shareInstance = [super allocWithZone:zone];
            return _shareInstance;
        }
    }
    return nil;
}

- (id)init
{
    self = [super init];
    if(self) {
        _isWorkThreadStarted = NO;
        
        _goodsDataHelper = [[GoodsDataHelper alloc] init];
        _chatUserHelper = [[QMChatUserHelper alloc] init];
        _contactHelper = [[QMContactHelper alloc] init];
        _friendHelper = [[QMFriendHelper alloc] init];
        _msgHelper = [[QMMsgHelper alloc] init];
        
        [self initWorkThread];
    }
    return self;
}

- (void)initWorkThread
{
     _workThread = [[KNThread alloc] initWithDelegate:self];
    [_goodsDataHelper setWorkThread:_workThread];
    [_chatUserHelper setWorkThread:_workThread];
    [_contactHelper setWorkThread:_workThread];
    [_friendHelper setWorkThread:_workThread];
    [_msgHelper setWorkThread:_workThread];
}

- (void)start
{
    @synchronized(self) {
        if (_isWorkThreadStarted)
            return;
        
        if(nil == _workThread) {
            [self initWorkThread];
        }
        [_workThread start];
        
        _isWorkThreadStarted = YES;
    }
}

- (void)stop
{
    @synchronized(self) {
        if (!_workThread)
            return;
        
        _isWorkThreadStarted = NO;
        
        [_workThread kill:YES];
        
        self.workThread = nil;
    }
}

- (BOOL)isStarted
{
    return _isWorkThreadStarted;
}

+ (void)freeDataCenter
{
    if(_shareInstance) {
        _shareInstance = nil;
    }
}

#pragma mark - KNThreadDelegate

- (void)threadStart:(KNThread *)thread
{
    if (thread) {
        [thread setName:[NSString stringWithFormat:@"%@ Work Thread", NSStringFromClass([self class])]];
    }
    
    QMDatabaseHelper *helper = [QMDatabaseHelper sharedInstance];
    [helper.openDatabase open];
    
    [self initAllTables:helper.openDatabase];
    
    //    DLog(@"goods work thread begin");
    //
    ////    // 创建数据库帮助对象，以备线程中其他对象使用
    ////    QMDatabaseHelper *helper = [QMDatabaseHelper sharedInstance];
    ////    [helper putIntoCurrentThread];
    ////    if (![helper open]) {
    ////        DLog(@"open error!");
    ////    }
    //    // TODO: 处理 open 不成功的情况
    //    DLog(@"goods work thread end");
}

- (void)threadStop:(KNThread *)thread
{
    // 移除并释放数据库帮助对象
    //[TTDatabaseHelper removeFromCurrentThread];
}


- (void)initAllTables:(FMDatabase *)database
{
#if DEBUG
    database.logsErrors = YES;
#endif
    
    // 处理数据库结构升级
    int oldVersion = [database userDatabaseVersion];
    if (oldVersion > 0) {
        if (oldVersion < CURRENT_DB_VERSION){
            [self databaseNeedUpdate:database oldVersion:oldVersion];
        }
    }
    else{//新用户
        // 处理数据库建表、表结构升级的逻辑
        //发现页列表
        [database executeUpdate:@"CREATE TABLE IF NOT EXISTS qmRecommendGoods ( "\
         "id INTEGER PRIMARY KEY, "\
         "picture TEXT,"\
         "desc TEXT,"\
         "price INTEGER,"\
         "orig_price INTEGER,"\
         "cate_type INTEGER,"\
         "city TEXT,"\
         "msgAudio TEXT,"\
         "msgAudioSecond INTEGER,"\
         "pub_time INTEGER NOT NULL,"\
         "sellerUserId INTEGER,"\
         "sellerHeadIcon TEXT,"\
         "sellerNick TEXT,"\
         "favorlist TEXT,"\
         "favorCounts INTEGER,"\
         "commentCounts INTEGER,"\
         "isFavor INTEGER,"\
         "isFriend INTEGER,"\
         "whoseFriend TEXT,"\
         "friendId INTEGER,"\
         "num INTEGER,"\
         "fNum INTEGER,"\
         "usage INTEGER,"\
         "status INTEGER,"\
         "update_time INTEGER,"\
         "freight INTEGER,"\
         "isSpecialGoods INTEGER);"];
        
        //首页列表
        [database executeUpdate:@"CREATE TABLE IF NOT EXISTS qmFriendGoods ( "\
         "id INTEGER PRIMARY KEY, "\
         "picture TEXT,"\
         "desc TEXT,"\
         "price INTEGER,"\
         "orig_price INTEGER,"\
         "cate_type INTEGER,"\
         "city TEXT,"\
         "msgAudio TEXT,"\
         "msgAudioSecond INTEGER,"
         "pub_time INTEGER NOT NULL,"\
         "sellerUserId INTEGER,"\
         "sellerHeadIcon TEXT,"\
         "sellerNick TEXT,"\
         "favorlist TEXT,"\
         "favorCounts INTEGER,"\
         "commentCounts INTEGER,"\
         "isFavor INTEGER,"\
         "isFriend INTEGER,"\
         "whoseFriend TEXT,"\
         "friendId INTEGER,"\
         "num INTEGER,"\
         "fNum INTEGER,"\
         "usage INTEGER,"\
         "status INTEGER,"\
         "update_time INTEGER,"\
         "freight INTEGER,"\
         "isSpecialGoods INTEGER);"];
        
        //订单列表
        [database executeUpdate:@"CREATE TABLE IF NOT EXISTS qmOrderList ("\
         "id TEXT PRIMARY KEY, "\
         "data TEXT);"];
        
        [database executeUpdate:@"CREATE TABLE IF NOT EXISTS qmPubGoodsList ("\
         "id INTEGER PRIMARY KEY, "\
         "data TEXT);"];
        
        
        [database executeUpdate:@"CREATE TABLE IF NOT EXISTS qmBoughtGoodsList ("\
         "id INTEGER PRIMARY KEY, "\
         "data TEXT);"];
        
        
        [database executeUpdate:@"CREATE TABLE IF NOT EXISTS qmFavorList ("\
         "id INTEGER PRIMARY KEY, "\
         "data TEXT);"];
        
        [database executeUpdate:@"CREATE TABLE IF NOT EXISTS qmSystemMsgList ("\
         "id INTEGER PRIMARY KEY, "\
         "msgtype INTEGER NOT NULL, "\
         "data TEXT, " \
         "status INTEGER);"];
        
        //消息列表界面
        [database executeUpdate:@"CREATE TABLE IF NOT EXISTS qmChatUserList (" \
         "userId INTEGER PRIMARY KEY NOT NULL, " \
         "nickname TEXT, "\
         "icon TEXT, "\
         "extData TEXT, "\
         "remark TEXT);"];
        
        [database executeUpdate:@"CREATE TABLE IF NOT EXISTS qmContactList (" \
         "cellPhone TEXT PRIMARY KEY NOT NULL, " \
         "name TEXT);"];
        
        //朋友列表页
        [database executeUpdate:@"CREATE TABLE IF NOT EXISTS qmFriendList (" \
         "userId INTEGER PRIMARY KEY NOT NULL, " \
         "nickname TEXT, "\
         "icon TEXT, "\
         "cellphone TEXT, "\
         "pinyin TEXT, "\
         "remark TEXT);"];
        
        [database executeUpdate:@"CREATE TABLE IF NOT EXISTS qmMsgList ("\
         "id TEXT PRIMARY KEY, "\
         "msgtype INTEGER NOT NULL, "\
         "msgcontent TEXT, " \
         "msgtime INTEGER, " \
         "extdata TEXT," \
         "msgpos INTEGER, "\
         "unread INTEGER);"];
    }
    
    // 标记当前数据库版本号
    if (oldVersion < CURRENT_DB_VERSION)
        [database setUserDatabaseVersion:CURRENT_DB_VERSION];
}

- (void)databaseNeedUpdate:(FMDatabase *)database oldVersion:(int)oldVersion
{
    //根据版本号，判断是否需要升级
    if (![database columnExists:@"num" inTableWithName:@"qmRecommendGoods"])
    {
        [database executeUpdate:@"ALTER TABLE qmRecommendGoods ADD COLUMN num INTEGER"];
    }
    if (![database columnExists:@"num" inTableWithName:@"qmFriendGoods"])
    {
        [database executeUpdate:@"ALTER TABLE qmFriendGoods ADD COLUMN num INTEGER"];
    }
    if (![database columnExists:@"fNum" inTableWithName:@"qmRecommendGoods"]) {
        [database executeUpdate:@"ALTER TABLE qmRecommendGoods ADD COLUMN fNum INTEGER"];
    }
    if (![database columnExists:@"usage" inTableWithName:@"qmRecommendGoods"]) {
        [database executeUpdate:@"ALTER TABLE qmRecommendGoods ADD COLUMN usage INTEGER"];
    }
    if (![database columnExists:@"status" inTableWithName:@"qmRecommendGoods"]) {
        [database executeUpdate:@"ALTER TABLE qmRecommendGoods ADD COLUMN status INTEGER"];
    }
    if (![database columnExists:@"update_time" inTableWithName:@"qmRecommendGoods"]) {
        [database executeUpdate:@"ALTER TABLE qmRecommendGoods ADD COLUMN update_time INTEGER"];
    }
    if (![database columnExists:@"fNum" inTableWithName:@"qmFriendGoods"]) {
        [database executeUpdate:@"ALTER TABLE qmFriendGoods ADD COLUMN fNum INTEGER"];
    }
    if (![database columnExists:@"usage" inTableWithName:@"qmFriendGoods"]) {
        [database executeUpdate:@"ALTER TABLE qmFriendGoods ADD COLUMN usage INTEGER"];
    }
    if (![database columnExists:@"status" inTableWithName:@"qmFriendGoods"]) {
        [database executeUpdate:@"ALTER TABLE qmFriendGoods ADD COLUMN status INTEGER"];
    }
    if (![database columnExists:@"update_time" inTableWithName:@"qmFriendGoods"]) {
        [database executeUpdate:@"ALTER TABLE qmFriendGoods ADD COLUMN update_time INTEGER"];
    }
    
    if (![database columnExists:@"freight" inTableWithName:@"qmFriendGoods"]) {
        [database executeUpdate:@"ALTER TABLE qmFriendGoods ADD COLUMN freight INTEGER"];
    }
    if (![database columnExists:@"freight" inTableWithName:@"qmRecommendGoods"]) {
        [database executeUpdate:@"ALTER TABLE qmRecommendGoods ADD COLUMN freight INTEGER"];
    }
    
    if (![database columnExists:@"isSpecialGoods" inTableWithName:@"qmFriendGoods"]) {
        [database executeUpdate:@"ALTER TABLE qmFriendGoods ADD COLUMN isSpecialGoods INTEGER"];
    }
    if (![database columnExists:@"isSpecialGoods" inTableWithName:@"qmRecommendGoods"]) {
        [database executeUpdate:@"ALTER TABLE qmRecommendGoods ADD COLUMN isSpecialGoods INTEGER"];
    }
    
    if (![database columnExists:@"remark" inTableWithName:@"qmFriendList"]) {
        [database executeUpdate:@"ALTER TABLE qmFriendList ADD COLUMN remark TEXT"];
    }
    
    if (![database columnExists:@"remark" inTableWithName:@"qmChatUserList"]) {
        [database executeUpdate:@"ALTER TABLE qmChatUserList ADD COLUMN remark TEXT"];
    }
}

@end
