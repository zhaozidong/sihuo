//
//  QMPayFailViewController.h
//  QMMM
//
//  Created by Derek.zhao on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMPayFailViewController : UIViewController

- (IBAction)rePay:(id)sender;

- (void)setOrderId:(NSString *)orderId sihuoPay:(float)sihuoPay andAliPay:(float)aliPay;

@end
