//
//  UserProtocolView.m
//  QMMM
//
//  Created by kingnet  on 14-11-15.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "UserProtocolView.h"

@interface UserProtocolView ()
@property (weak, nonatomic) IBOutlet UIButton *userProtocolBtn;
@property (weak, nonatomic) IBOutlet UIButton *checkUserProtocolBtn;

@end

@implementation UserProtocolView

+ (UserProtocolView *)protocolView
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"UserProtocolView" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    [self.userProtocolBtn setTitleColor:kBlueColor forState:UIControlStateNormal];
    [self.checkUserProtocolBtn setTitleColor:kGrayTextColor forState:UIControlStateNormal];
    self.checkUserProtocolBtn.selected = YES;
}
- (IBAction)checkUserProtocolAction:(id)sender {
    self.checkUserProtocolBtn.selected = !self.checkUserProtocolBtn.selected;
}
- (IBAction)userProtocolAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickUserProtocol)]) {
        [self.delegate clickUserProtocol];
    }
}

- (BOOL)isCheckUserPro
{
    return self.checkUserProtocolBtn.selected;
}

+ (CGSize)viewSize
{
    return CGSizeMake(240, 30.f);
}

@end
