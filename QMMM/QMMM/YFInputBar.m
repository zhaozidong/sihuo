//
//  YFInputBar.m
//  test
//
//  Created by 杨峰 on 13-11-10.
//  Copyright (c) 2013年 杨峰. All rights reserved.
//

#import "YFInputBar.h"
#import "AppDelegate.h"
@implementation YFInputBar


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor grayColor];
        
        self.frame = CGRectMake(0, CGRectGetMinY(frame), CGRectGetWidth(self.frame), CGRectGetHeight(frame));
        minHeight_ = CGRectGetHeight(self.frame);
        UIImage *rawBackground = [UIImage imageNamed:@"messageToolbarBg"];
        UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:1 topCapHeight:3];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:background];
        imageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        self.textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        self.textView.tag = 10000;
        self.sendBtn.tag = 10001;
        self.sendBtn.enabled = NO;
       
        [self addSubview:imageView];
        [self addSubview:self.textView];
        [self addSubview:self.sendBtn];
        
        //注册键盘通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    _originalFrame = frame;
}

//_originalFrame的set方法  因为会调用setFrame  所以就不在此做赋值；
-(void)setOriginalFrame:(CGRect)originalFrame
{
    self.frame = CGRectMake(0, CGRectGetMinY(originalFrame), CGRectGetWidth(originalFrame), CGRectGetHeight(originalFrame));
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark get方法实例化输入框／btn
- (HPGrowingTextView *)textView
{
    if (!_textView) {
        _textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(8, (CGRectGetHeight(self.frame)-34)/2, CGRectGetWidth(self.frame)-60-8, 34)];
        _textView.isScrollable = NO;
        _textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
        
        _textView.minNumberOfLines = 1;
        _textView.maxNumberOfLines = 3;
        // you can also set the maximum height in points with maxHeight
        // textView.maxHeight = 200.0f;
        _textView.returnKeyType = UIReturnKeySend; //just as an example
        _textView.enablesReturnKeyAutomatically = YES;
        _textView.font = [UIFont systemFontOfSize:15.0f];
        _textView.delegate = self;
        _textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
        _textView.backgroundColor = [UIColor whiteColor];
        _textView.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
        _textView.layer.borderWidth = 0.65f;
        _textView.layer.cornerRadius = 5.0f;
    }
    return _textView;
}

-(UIButton *)sendBtn
{
    if (!_sendBtn) {
        _sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sendBtn setTitle:[AppUtils localizedCommonString:@"QM_Text_Send"] forState:UIControlStateNormal];
        [_sendBtn setBackgroundColor:[UIColor clearColor]];
        [_sendBtn setFrame:CGRectMake(CGRectGetWidth(self.frame)-60, (CGRectGetHeight(self.frame)-30)/2, 60, 30)];
        [_sendBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_sendBtn setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateDisabled];

        _sendBtn.titleLabel.font = [UIFont systemFontOfSize:18.f];
        _sendBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        [_sendBtn addTarget:self action:@selector(sendBtnPress:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sendBtn;
}

#pragma mark - TextView Delegate
- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    CGRect r = self.frame;
    float diff = 0, h = height+16; //height为里面文本框的高度
    if (h < minHeight_) {
        diff = self.frame.size.height - minHeight_;
    }
    else{
        diff = (self.frame.size.height - h);
    }
    r.size.height -= diff;
    r.origin.y += diff;
    self.frame = r;
    CGRect rr = self.sendBtn.frame;
    rr.origin.y -= diff;
    self.sendBtn.frame = rr;
}

- (BOOL)growingTextViewShouldReturn:(HPGrowingTextView *)growingTextView
{
    [self sendBtnPress:nil];
    return NO;
}

- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView
{
    if ([growingTextView.text isEqualToString:@""]) {
        self.sendBtn.enabled = NO;
    }
    else{
        self.sendBtn.enabled =  YES;
    }
}

- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location >= kWordLimitNum) {
        return NO;
    }
    if ([text isEqualToString:@"\n"]) {
        [self sendBtnPress:nil];
        return NO;
    }
    return YES;
}

#pragma mark selfDelegate method

-(void)sendBtnPress:(UIButton*)sender
{
    if (self.delegate&&[self.delegate respondsToSelector:@selector(inputBar:sendBtnPress:withInputString:)]) {
        [self.delegate inputBar:self sendBtnPress:sender withInputString:self.textView.text];
    }
    if (self.clearInputWhenSend) {
        self.textView.text = @"";
    }
    if (self.resignFirstResponderWhenSend) {
        [self resignFirstResponder];
    }
}

#pragma mark keyboardNotification

- (void)keyboardWillShow:(NSNotification*)notification{
    CGRect _keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
//    NSLog(@"%f-%f-%f-%f",_keyboardRect.origin.y,_keyboardRect.size.height,[self getHeighOfWindow]-CGRectGetMaxY(self.frame),CGRectGetMinY(self.frame));

    //如果self在键盘之下 才做偏移
    if ([self convertYToWindow:CGRectGetMaxY(self.originalFrame)]>=_keyboardRect.origin.y)
    {
        //没有偏移 就说明键盘没出来，使用动画
        if (self.frame.origin.y== self.originalFrame.origin.y) {
            
            [UIView animateWithDuration:0.3
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 self.transform = CGAffineTransformMakeTranslation(0, -_keyboardRect.size.height+[self getHeighOfWindow]-CGRectGetMaxY(self.originalFrame));
                             } completion:nil];
        }
        else
        {
            self.transform = CGAffineTransformMakeTranslation(0, -_keyboardRect.size.height+[self getHeighOfWindow]-CGRectGetMaxY(self.originalFrame));
        }
    }
    else
    {
        
    }
    
}

- (void)keyboardWillHide:(NSNotification*)notification{

    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         float height = CGRectGetHeight(self.frame);
                         self.transform = CGAffineTransformMakeTranslation(0, height-minHeight_);
                         _originalFrame = CGRectMake(0, [self getHeighOfWindow], CGRectGetWidth(_originalFrame), minHeight_);
                     } completion:nil];
}

#pragma  mark ConvertPoint
//将坐标点y 在window和superview转化  方便和键盘的坐标比对
-(float)convertYFromWindow:(float)Y
{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    CGPoint o = [appDelegate.window convertPoint:CGPointMake(0, Y) toView:self.superview];
    return o.y;
    
}
-(float)convertYToWindow:(float)Y
{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    CGPoint o = [self.superview convertPoint:CGPointMake(0, Y) toView:appDelegate.window];
    return o.y;
    
}
-(float)getHeighOfWindow
{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    return appDelegate.window.frame.size.height;
}

-(BOOL)resignFirstResponder
{
    [self.textView resignFirstResponder];
    return [super resignFirstResponder];
}

- (BOOL)becomeFirstResponder
{
    [self.textView becomeFirstResponder];
    return [super becomeFirstResponder];
}

- (void)clearText
{
    self.textView.text = @"";
    self.center = CGPointMake(self.center.x, [self getHeighOfWindow]+CGRectGetHeight(self.frame)/2);
    _originalFrame = CGRectMake(0, [self getHeighOfWindow], CGRectGetWidth(_originalFrame), minHeight_);
}

@end
