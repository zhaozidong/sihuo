//
//  SellingGoodsDescCell.h
//  QMMM
//
//  Created by kingnet  on 14-12-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kSellingGoodsDescCellIdentifier;

@class GoodsSimpleInfo;
@interface SellingGoodsDescCell : UITableViewCell

+ (UINib *)nib;

+ (CGFloat)cellHeight;

- (void)updateUI:(GoodsSimpleInfo *)goodsInfo;

@end
