//
//  GoodsItem.h
//  QMMM
//
//  Created by kingnet  on 15-3-16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class GoodsSimpleInfo;

@protocol GoodsItemDelegate <NSObject>

- (void)clickGoodsItem:(GoodsSimpleInfo *)goodsInfo;

@end

@interface GoodsItem : UIView

@property (nonatomic, strong) GoodsSimpleInfo *goodsInfo;

@property (nonatomic, weak) id<GoodsItemDelegate> delegate;

+ (GoodsItem *)goodsItem;

+ (CGSize)itemSize;

+ (CGFloat)margin;

@end
