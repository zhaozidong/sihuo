//
//  QMNewFriendViewController.h
//  QMMM
//
//  Created by hanlu on 14-10-31.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMNewFriendViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView * tableView;

@end
