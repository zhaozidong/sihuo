//
//  ImageBrowerView.m
//  QMMM
//
//  Created by kingnet  on 14-10-22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "ImageBrowerView.h"
#import "ImageBrowerCell.h"

@implementation ImageBrowerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _initView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self _initView];
    }
    return self;
}

- (void)_initView
{
    tableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height, self.frame.size.width) style:UITableViewStylePlain];
    tableView_.delegate = self;
    tableView_.dataSource = self;
    tableView_.center = CGPointMake(self.frame.size.width * 0.5, self.frame.size.height * 0.5);
    tableView_.showsVerticalScrollIndicator = NO;
    tableView_.transform = CGAffineTransformMakeRotation(-M_PI / 2);
    tableView_.pagingEnabled = YES;
    tableView_.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView_.backgroundView = nil;
    tableView_.backgroundColor = [UIColor clearColor];
    tableView_.scrollsToTop = NO;
    [self addSubview:tableView_];
    
    pageControl_ = [[UIPageControl alloc]init];
    pageControl_.center = CGPointMake(self.frame.size.width*0.5, 20);
    pageControl_.bounds = CGRectMake(0, 0, 150, 150);
    pageControl_.enabled = NO;
    pageControl_.hidden = YES;
    [self addSubview:pageControl_];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    tableView_.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    tableView_.center = CGPointMake(self.frame.size.width * 0.5, self.frame.size.height * 0.5);
}

- (void)updateImages:(NSArray *)images selectIndex:(NSInteger)index
{
    self.imgs = images;
    [tableView_ reloadData];
    if (index >=0 && index < self.imgs.count) {
        NSInteger row = MAX(index, 0);
        [tableView_ scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
    pageControl_.hidden = YES;
    pageBackgroundView_.hidden = YES;
    if (images.count>1) {
        pageControl_.hidden = NO;
        pageControl_.numberOfPages = [self.imgs count];
        pageControl_.currentPage = index;
        CGSize pageSize = [pageControl_ sizeForNumberOfPages:self.imgs.count];
        if (nil == pageBackgroundView_) {
            pageBackgroundView_ = [[UIView alloc]initWithFrame:CGRectZero];
            pageBackgroundView_.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.1];
            [self insertSubview:pageBackgroundView_ belowSubview:pageControl_];
        }
        pageBackgroundView_.bounds = CGRectMake(0, 0, pageSize.width+10, 16);
        pageBackgroundView_.center = CGPointMake(self.frame.size.width*0.5, 20);
        pageBackgroundView_.layer.cornerRadius = 8;
        pageBackgroundView_.layer.masksToBounds = YES;
        pageBackgroundView_.layer.borderColor = [[UIColor clearColor]CGColor];
        pageBackgroundView_.hidden = NO;
    }
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    NSIndexPath *index = [tableView_ indexPathForRowAtPoint:scrollView.contentOffset];
    pageControl_.currentPage = index.row;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSIndexPath *index = [tableView_ indexPathForRowAtPoint:scrollView.contentOffset];
    pageControl_.currentPage = index.row;
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.imgs count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ImageBrowerCell *cell = [tableView dequeueReusableCellWithIdentifier:kImageBrowerCellIdty];
    if (!cell) {
        cell = [[ImageBrowerCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kImageBrowerCellIdty];
       // cell.contentView.transform = CGAffineTransformMakeRotation(M_PI / 2);
    }
    
    cell.size = self.bounds.size;
    NSString *imgStr = [self.imgs objectAtIndex:indexPath.row];
    [cell setImageName:imgStr];
    if (indexPath.row+1 < [self.imgs count]) {
        NSString *s = [self.imgs objectAtIndex:indexPath.row+1];
        [cell downloadNextImage:s];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.frame.size.width;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickeImageAtIndex:)]) {
        [self.delegate clickeImageAtIndex:indexPath.row];
    }
}
@end
