//
//  MsgApi.h
//  QMMM
//
//  Created by hanlu on 14-10-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "BaseHandler.h"

@interface MsgApi : BaseHandler

//获取系统消息列表
- (void)getMsgList:(uint)preGetCount context:(contextBlock)context;

//删除消息
- (void)deleteMsg:(uint)preMsgCount context:(contextBlock)context;

@end
