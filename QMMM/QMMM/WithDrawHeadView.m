//
//  WithDrawHeadView.m
//  QMMM
//
//  Created by kingnet  on 14-11-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "WithDrawHeadView.h"

@interface WithDrawHeadView ()
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;

@end

@implementation WithDrawHeadView

+ (WithDrawHeadView *)headView
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"WithdrawHeadView" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.moneyLbl.backgroundColor = [UIColor clearColor];
    self.moneyLbl.text = @"";
}

- (void)setMoney:(float)money
{
    self.moneyLbl.text = [NSString stringWithFormat:@"¥%.2f", money];
}

+ (CGFloat)headViewHeight
{
    return 216.f;
}

@end
