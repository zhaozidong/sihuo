//
//  QMCategoryCell.m
//  QMMM
//
//  Created by Derek on 15/3/16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMCategoryCell.h"
#import "QMCategoryCellDetail.h"


@interface QMCategoryCell()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>{
    NSArray *_arrCategory;
}

@end





@implementation QMCategoryCell

+ (QMCategoryCell *)categoryCell
{
    return [[[NSBundle mainBundle]loadNibNamed:@"QMCategoryCell" owner:self options:0] lastObject];
}

- (void)awakeFromNib {
    // Initialization code
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.scrollEnabled = YES;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.collectionView registerClass:[QMCategoryCellDetail class] forCellWithReuseIdentifier:@"categoryDetailCell"];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight{
//    return kMainFrameWidth*0.6;
    if (IS_IPHONE_4) {
        return ((kMainFrameWidth-6)/4)*2+3;
    }else if (IS_IPHONE_5) {
        return ((kMainFrameWidth-6)/4-9)*2+3;
    }else if (IS_IPHONE_6) {
        return ((kMainFrameWidth-6)/4-20)*2+3;
    }else if(IS_IPHONE_6P){
        return ((kMainFrameWidth-6)/4-30)*2+3;
    }
    return 50;
}

- (void)updateWithArr:(NSArray *)arr{
    _arrCategory=arr;
    [self.collectionView reloadData];
}



#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 8;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    UICollectionViewCell *collectionCell;;
//    collectionCell=[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
//    if (!collectionCell) {
//        collectionCell=[[UICollectionViewCell alloc] init];
//    }
    
    
    QMCategoryCellDetail *collectionCell=[collectionView dequeueReusableCellWithReuseIdentifier:@"categoryDetailCell" forIndexPath:indexPath];
    
    collectionCell.backgroundColor=[UIColor whiteColor];
    
    if (indexPath.row<=[_arrCategory count]-1) {
        CategoryEntity *category=[_arrCategory objectAtIndex:indexPath.row];
        [collectionCell updateWithCategory:category];
    }
    
    return collectionCell;
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int width=(int)(kMainFrameWidth-6)/4;
    if (IS_IPHONE_4) {
        return CGSizeMake(width, width);
    }else if (IS_IPHONE_5) {
        return CGSizeMake(width, width-9);
    }else if(IS_IPHONE_6){
        return CGSizeMake(width, width-20);
    }else if(IS_IPHONE_6P){
        return CGSizeMake(width, width-30);
    }
    return CGSizeMake(width, width-30);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    //一排中每块之间的距离
    //    CGSize size = [PhotoCell cellSize];
    //    CGFloat w = (CGRectGetWidth(collectionView.frame)-(size.width*5+8*2))/4;
    //    float iosV = [[[UIDevice currentDevice] systemVersion] floatValue];
    //    if (iosV >= 8.0) {
    //        return w;
    //    }
    return 1;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1.f; //每行之间的距离
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(1.f, 1.f, 1.f, 1.f);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"indexPath.section=%ld,indexPath.row=%ld",(long)indexPath.section,(long)indexPath.row);
    if (_delegate && [_delegate respondsToSelector:@selector(didTapCategoryCell:)]) {
        CategoryEntity *category=[_arrCategory objectAtIndex:indexPath.row];
        [_delegate didTapCategoryCell:category];
    }
}


@end
