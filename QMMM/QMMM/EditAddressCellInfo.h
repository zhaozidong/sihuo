//
//  EditAddressCellInfo.h
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EditAddressCellInfo : NSObject
@property (nonatomic, strong) NSArray *cellsArray;
@property (nonatomic, strong) NSString *buyerName;
@property (nonatomic, strong) NSString *buyerPhone;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *detailAddress;
@property (nonatomic, assign) BOOL isDefault; //是否为默认地址
@property (nonatomic, strong) NSIndexPath *editingIndexPath; //正在编辑的文本框

+(CGFloat)cellHeight;

@end
