//
//  QMOrderCommentsCells.h
//  QMMM
//
//  Created by kingnet  on 14-11-25.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@class OrderDataInfo;
@interface QMOrderCommentsCells : NSObject

@property (nonatomic, strong) NSArray *cellsArr;

- (void)updateUI:(OrderDataInfo *)orderInfo;

- (CGFloat)commentsCellHeight:(NSString *)str;

@end
