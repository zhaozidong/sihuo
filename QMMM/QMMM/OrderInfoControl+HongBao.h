//
//  OrderInfoControl+HongBao.h
//  QMMM
//
//  Created by kingnet  on 15-3-27.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "OrderInfoControl.h"

@interface OrderInfoControl (HongBao)

- (void)setLabelText;

- (void)resetGoodsNum;  //在价格超出钱数时，将数量减一

@end
