//
//  QMSqlite.h
//  QMMM
//
//  Created by 韩芦 on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMResultSet.h"

/**
	FMDatabase附加分类
 */
@interface FMDatabase (TTAdditions)

/**
	获取数据库用户版本，用于判断数据库结构是否需要更新
	@returns 数据库用户版本号
 */
- (int)userDatabaseVersion;
/**
	设置数据库用户版本号
	@param version 新的数据库用户版本号
 */
- (void)setUserDatabaseVersion:(int)version;

/**
	是否空数据库
	@returns sqlite_master中不存在任何TABLE则返回YES，否则返回NO
 */
- (BOOL)isEmpty;

@end
