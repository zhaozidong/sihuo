//
//  WithDrawTextField.m
//  QMMM
//
//  Created by kingnet  on 14-11-8.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "WithDrawTextField.h"

@implementation WithDrawTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.font = [UIFont systemFontOfSize:15.f];
    self.borderStyle = UITextBorderStyleNone;
    self.backgroundColor = [UIColor whiteColor];
    self.textColor = kDarkTextColor;
}

- (CGRect)rightViewRectForBounds:(CGRect)bounds
{
    CGRect rect = [super rightViewRectForBounds:bounds];
    rect.origin.x = CGRectGetWidth(self.bounds)-rect.size.width-15;
    return rect;
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    CGRect rect = [super placeholderRectForBounds:bounds];
    rect.origin.x = 15.f;
    return rect;
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    CGRect rect = [super textRectForBounds:bounds];
    rect.origin.x = 15.f;
    return rect;
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    CGRect rect = [super editingRectForBounds:bounds];
    rect.origin.x = 15.f;
    if (self.rightView) {
        rect.size.width = rect.size.width - CGRectGetWidth(self.rightView.frame) +5.f;
    }
    return rect;
}

@end
