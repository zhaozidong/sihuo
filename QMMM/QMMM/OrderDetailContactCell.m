//
//  OrderDetailContactCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderDetailContactCell.h"
#import "OrderDataInfo.h"

NSString * const kOrderDetailContactCellIdentifier = @"kOrderDetailContactCellIdentifier";

@interface OrderDetailContactCell ()
@property (weak, nonatomic) IBOutlet UILabel *sellerNameLbl; //卖家姓名
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatLeftCst;

@end
@implementation OrderDetailContactCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"OrderDetailContactCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.sellerNameLbl.text = @"";
    self.sellerNameLbl.textColor = kDarkTextColor;
    self.callBtn.hidden = YES;
    self.chatLeftCst.constant = 15.f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight
{
    return 45.f;
}

- (IBAction)contactAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapContactSeller)]) {
        [self.delegate didTapContactSeller];
    }
}

- (IBAction)callAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapCallSeller)]) {
        [self.delegate didTapCallSeller];
    }
}

- (void)updateUI:(OrderDataInfo *)orderInfo bSeller:(BOOL)bSeller
{
    if (bSeller) {
        self.sellerNameLbl.text = [NSString stringWithFormat:@"联系买家:%@", orderInfo.user_nick];
    }
    else{
        self.sellerNameLbl.text = [NSString stringWithFormat:@"联系卖家:%@", orderInfo.user_nick];
    }
    if (orderInfo.mobile.length > 0) {
        self.callBtn.hidden = NO;
        self.chatLeftCst.constant = 64.f;
    }
}


@end
