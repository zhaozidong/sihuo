//
//  PersonLoadingCell.h
//  QMMM
//
//  Created by kingnet  on 15-1-26.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonLoadingCell : UITableViewCell

+ (PersonLoadingCell *)loadingCell;

- (void)startAnimate;

- (void)stopAnimate;

@end
