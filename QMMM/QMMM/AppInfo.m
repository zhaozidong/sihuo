//
//  AppInfo.m
//  QMMM
//
//  Created by kingnet  on 14-11-6.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "AppInfo.h"

@implementation AppInfo

- (id)initWithAPPTitle:(NSString *)appTitle appDesc:(NSString *)appDesc appLinker:(NSString *)appLinker appImage:(UIImage *)appImage
{
    if (self = [super init]) {
        self.appTitle = appTitle;
        self.appDesc = appDesc;
        self.linker = appLinker;
        self.appImage = appImage;
    }
    return self;
}

+ (AppInfo *)shareToWeiXinApp
{
    return nil;
}

@end
