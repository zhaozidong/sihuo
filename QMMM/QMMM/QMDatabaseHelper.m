//
//  QMDatabaseHelper.m
//  QMMM
//
//  Created by kingnet  on 14-9-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMDatabaseHelper.h"
#import "FMDatabaseAdditions.h"
#import "UserDao.h"
@implementation QMDatabaseHelper

NSString * const kUserTable = @"User";
NSString * const kAddressTable = @"Address";
NSString * const kFinanceTable = @"Finance";
NSString * const kSearchHistory = @"SearchHistory";
NSString * const kGoodsChatHistory = @"GoodsChatHistory";

- (id)init
{
    if (self = [super init]) {
        
    }
    return self;
}

+ (QMDatabaseHelper *)sharedInstance
{
    static QMDatabaseHelper *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (FMDatabase *)openDatabase
{
    if (_database == nil) {
        DLog(@"database is nil");
    }
    if ([_database open]) {
        return _database;
    }
    return nil;
}

//- (void)removeAllData
//{
//    NSDictionary *dic_sql = [self sql];
//}

- (void)createDatabase:(NSString *)uid
{
    NSString *homedir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject];
    NSString *dbpath = [homedir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_qmmm.sqlite", uid]];
    DLog(@"dbpath:%@", dbpath);
    _database = [FMDatabase databaseWithPath:dbpath];
    
    [self createAllTable];
}

- (BOOL)isTableExists:(NSString *)tableName
{
    NSString *existsSql = [NSString stringWithFormat:@"select count(name) as countNum from sqlite_master where type = 'table' and name = '%@'", tableName];
    
    FMResultSet *rs = [[self openDatabase] executeQuery:existsSql];
    
	if ([rs next]) {
		NSInteger count = [rs intForColumn:@"countNum"];
		if (count == 1) {
			DLog(@"%@ is existed.",tableName);
			return YES;
		}
        DLog(@"%@ is not exist.",tableName);
	}
	[rs close];
    
    return NO;
}

//创建所用到的表，目前有User、Address、Finance
- (void)createAllTable
{
    //创建所需的表
    if (![self isTableExists:kUserTable]) {
        NSDictionary *dic_sql = [self sqlForCreateTable];
        NSString *key = [NSString stringWithFormat:@"create%@Table", kUserTable];
        if (![_database executeUpdate:[dic_sql objectForKey:key]]) {
            DLog(@"Create User table failed");
        }
    }
    if (![self isTableExists:kAddressTable]) {
        NSDictionary *dic_sql = [self sqlForCreateTable];
        NSString *key = [NSString stringWithFormat:@"create%@Table", kAddressTable];
        if (![_database executeUpdate:[dic_sql objectForKey:key]]) {
            DLog(@"Create Address table failed");
        }
    }
    if (![self isTableExists:kFinanceTable]) {
        NSDictionary *dic_sql = [self sqlForCreateTable];
        NSString *key = [NSString stringWithFormat:@"create%@Table", kFinanceTable];
        if (![_database executeUpdate:[dic_sql objectForKey:key]]) {
            DLog(@"Create Finance table failed");
        }
    }
    if (![self isTableExists:kSearchHistory]) {
        NSDictionary *dic_sql = [self sqlForCreateTable];
        NSString *key = [NSString stringWithFormat:@"create%@Table", kSearchHistory];
        if (![_database executeUpdate:[dic_sql objectForKey:key]]) {
            DLog(@"Create SearchHistory table failed");
        }
    }
    if (![self isTableExists:kGoodsChatHistory]) {
        NSDictionary *dic_sql = [self sqlForCreateTable];
        NSString *key = [NSString stringWithFormat:@"create%@Table", kGoodsChatHistory];
        if (![_database executeUpdate:[dic_sql objectForKey:key]]) {
            DLog(@"Create GoodsChatHistory table failed");
        }
    }
}

- (void)closeDB
{
    dispatch_async(dispatch_get_main_queue(), ^{
		if([_database close]) {
			_database = nil;
		}
	});
}

- (BOOL)deleteDatabase:(NSString *)uid
{
    NSString* docsdir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *dbpath = [docsdir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_qmmm.sqlite", uid]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager removeItemAtPath:dbpath error:nil];
}

- (NSDictionary *)sqlForCreateTable
{
    NSString *sqlFilePath = [[NSBundle mainBundle] pathForResource:@"sql" ofType:@"plist"];
    NSDictionary *dic_sql = [NSDictionary dictionaryWithContentsOfFile:sqlFilePath];
    return dic_sql;
}

- (NSString *)createInsertSQL:(NSString *)tableName
{
    FMDatabase *db = [self openDatabase];
    FMResultSet *rs = [db getTableSchema:tableName];
    NSMutableArray *columnArr = [NSMutableArray array];
    while ([rs next]) {
        NSString *name = [rs stringForColumn:@"name"];
        if (![name isEqualToString:@"id"]) {
            [columnArr addObject:name];
        }
    }
    
    NSString *prefix = [NSString stringWithFormat:@"insert into %@ (", tableName];
    NSMutableString *middle = [[NSMutableString alloc] initWithString:@""];
    for (NSString *colName in columnArr) {
        if ([colName isEqualToString:[columnArr firstObject]]) {
            [middle appendString:colName];
        }
        else{
            [middle appendFormat:@", %@", colName];
        }
        
    }
    
    NSMutableString *suffix = [[NSMutableString alloc] initWithString:@") values ("];
    for (NSString *colName in columnArr) {
        if ([colName isEqualToString:[columnArr lastObject]]) {
            [suffix appendFormat:@":%@)", colName];
        }
        else{
            [suffix appendFormat:@":%@, ", colName];
        }
    }
    
    NSString *insertSQL = [[prefix stringByAppendingString:middle] stringByAppendingString:suffix];
    
    return insertSQL;
}

@end
