//
//  QMGoodsCatetCell.m
//  QMMM
//
//  Created by hanlu on 14-9-19.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGoodsCatetCell.h"
#import "QMGoodsCateInfo.h"
#import "UIImageView+QMWebCache.h"

NSString * kQMGoodsCatetCellIdentifier = @"kQMGoodsCatetCellIdentifier";

@implementation QMGoodsCatetCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"QMGoodsCatetCell" bundle:[NSBundle mainBundle]];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    self.name.text = @"";
    self.desc.text = @"";
    self.name.textColor = kDarkTextColor;
    self.desc.textColor = kLightBlueColor;
    self.cateImageView.image = [UIImage imageNamed:@"goodsPlaceholder_small"];
    self.cateImageView.layer.cornerRadius = (self.cateImageView.bounds.size.width)/2;
    self.cateImageView.layer.borderWidth = 0;
    self.cateImageView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(QMGoodsCateInfo *)cateInfo
{
    _name.text = cateInfo.name;
    _desc.text = [cateInfo childDesc];
    if (cateInfo.iconURL.length > 0) {
        [self.cateImageView qm_setImageWithURL:cateInfo.iconURL placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_small"]];
    }
}

+ (CGFloat)cellHeight
{
    return 80.f;
}

@end
