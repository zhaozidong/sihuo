//
//  FeedbackViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-6.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "FeedbackViewController.h"
#import "UIPlaceHolderTextView.h"
#import "SystemHandler.h"

@interface FeedbackViewController ()<UITextViewDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) UIPlaceHolderTextView *textView;

@property (nonatomic, strong) UIButton *sendBtn; //发送按钮

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"意见反馈";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    self.navigationItem.rightBarButtonItems = [AppUtils createRightButtonWithTarget:self selector:@selector(sendAction:) title:nil size:CGSizeMake(44, 44) imageName:@"navRight_finish_btn"];
    [self setupViews];

    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    //拿到发送button
    UIBarButtonItem *buttonItem = [self.navigationItem.rightBarButtonItems objectAtIndex:1];
    self.sendBtn = (UIButton *)buttonItem.customView;
    self.sendBtn.enabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.textView becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.textView resignFirstResponder];
}

- (void)setupViews
{
    self.textView = [[UIPlaceHolderTextView alloc]initWithFrame:CGRectMake(0, 14+kMainTopHeight, CGRectGetWidth(self.view.frame), 222)];
    self.textView.placeholder = @"加入吐槽大军";
    [self.textView setPlaceholderTextColor:UIColorFromRGB(0xcbd1d8)];
    self.textView.textColor = kDarkTextColor;
    self.textView.font = [UIFont systemFontOfSize:15.f];
    self.textView.returnKeyType = UIReturnKeySend;
    self.textView.delegate = self;
    self.textView.enablesReturnKeyAutomatically = YES;
    [self.view addSubview:self.textView];
}

- (void)sendAction:(id)sender
{
    NSString *text = [self.textView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (self.textView.text==nil || [self.textView.text isEqualToString:@""] || text.length==0) {
        [AppUtils showAlertMessage:@"请输入您的反馈内容"];
        return;
    }
    if (self.textView.text.length > 200) {
        [AppUtils showAlertMessage:@"最多输入200个字，请简明扼要哦~"];
        return;
    }
    
    [self.textView resignFirstResponder];
    __weak typeof(self) weakSelf = self;
    [AppUtils showProgressMessage:[AppUtils localizedCommonString:@"QM_HUD_Send"]];
    [[SystemHandler sharedInstance]submitFeedback:self.textView.text type:0 success:^(id obj) {
        [AppUtils dismissHUD];
        [weakSelf sendOk];
    } failed:^(id obj) {
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (void)sendOk
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:[AppUtils localizedCommonString:@"QM_Tip_Title"] message:[AppUtils localizedPersonString:@"QM_Alert_GetFeedback"] delegate:self cancelButtonTitle:[AppUtils localizedCommonString:@"QM_Button_Good"] otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        [self sendAction:nil];
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    if (textView.text == nil || textView.text.length == 0) {
        self.sendBtn.enabled = NO;
    }
    else{
        self.sendBtn.enabled = YES;
    }
}

@end
