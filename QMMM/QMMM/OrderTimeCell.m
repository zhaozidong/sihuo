//
//  OrderTimeCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderTimeCell.h"
#import "OrderDataInfo.h"

NSString * const kOrderTimeCellIdentifier = @"kOrderTimeCellIdentifier";

@interface OrderTimeCell ()
@property (weak, nonatomic) IBOutlet UILabel *orderNumLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

@end
@implementation OrderTimeCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"OrderTimeCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.orderNumLbl.textColor = kLightBlueColor;
    self.timeLbl.textColor = kLightBlueColor;
    self.orderNumLbl.text = @"";
    self.timeLbl.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(OrderSimpleInfo *)orderInfo
{
    self.orderNumLbl.text = [NSString stringWithFormat:@"订单号：%@", orderInfo.order_id];
    NSString * time = [AppUtils stringForOrderDate:[NSDate dateWithTimeIntervalSince1970:orderInfo.order_pub_time]];
    self.timeLbl.text = [NSString stringWithFormat:@"下单时间：%@", time];
}

+ (CGFloat)cellHeight
{
    return 30.f;
}

@end
