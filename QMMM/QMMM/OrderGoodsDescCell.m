//
//  OrderGoodsDescCell.m
//  QMMM
//
//  Created by kingnet  on 14-12-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "OrderGoodsDescCell.h"
#import "OrderDataInfo.h"
#import "UIImageView+QMWebCache.h"
#import "HTCopyableLabel.h"

NSString * const kOrderGoodsDescCellIdentifier = @"kOrderGoodsDescCellIdentifier";

@interface OrderGoodsDescCell ()<HTCopyableLabelDelegate>
{
    NSString *expressNum_;
}
@property (weak, nonatomic) IBOutlet UIImageView *goodsImgV;
@property (weak, nonatomic) IBOutlet UILabel *recieverLbl; //收货人/商品描述
@property (weak, nonatomic) IBOutlet UILabel *phoneLbl; //电话/快递公司
@property (weak, nonatomic) IBOutlet HTCopyableLabel *addressLbl; //收货地址/快递单号

@end

@implementation OrderGoodsDescCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"OrderGoodsDescCell" bundle:nil];
}

- (void)awakeFromNib {
    // Initialization code
    self.recieverLbl.textColor = kDarkTextColor;
    self.phoneLbl.textColor = kLightBlueColor;
    self.addressLbl.textColor = kLightBlueColor;
    self.addressLbl.numberOfLines = 2;
    self.addressLbl.preferredMaxLayoutWidth = kMainFrameWidth - 120;
    self.addressLbl.copyableLabelDelegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(OrderSimpleInfo *)orderInfo cellType:(EControllerType)cellType
{
    //已卖掉的
    if (cellType == ECT_ORDER_MGR) {
        self.recieverLbl.text = [NSString stringWithFormat:@"收货人: %@", orderInfo.recipients];
        self.phoneLbl.text = [NSString stringWithFormat:@"手机号: %@", orderInfo.cellPhone];
        self.addressLbl.text = [NSString stringWithFormat:@"收货地址: %@", orderInfo.recipients_address];
        self.addressLbl.copyingEnabled = NO;
    }
    //已买到的
    else{
        self.recieverLbl.text = orderInfo.goods_desc;
        if (orderInfo.express_name.length > 0) {
            self.phoneLbl.text = [NSString stringWithFormat:@"快递公司：%@", orderInfo.express_name];
        }
        else{
            self.phoneLbl.text = @"快递公司：暂无快递公司";
        }
        if (orderInfo.express_num.length > 0) {
            self.addressLbl.text = [NSString stringWithFormat:@"快递单号：%@", orderInfo.express_num];
            expressNum_ = orderInfo.express_num;
        }
        else{
            self.addressLbl.text = @"快递单号：暂无快递单号";
            self.addressLbl.copyingEnabled = NO;
        }
    }
    
    NSString * imagePath = [AppUtils thumbPath:orderInfo.goods_image sizeType:QMImageQuarterSize];
    [self.goodsImgV qm_setImageWithURL:imagePath placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_middle"]];
}

+ (CGFloat)cellHeight
{
    return 110.f;
}

#pragma mark - HTCopyableLabelDelegate
- (NSString *)stringToCopyForCopyableLabel:(HTCopyableLabel *)copyableLabel
{
    if (expressNum_ && expressNum_.length>0) {
        return expressNum_;
    }
    return @"";
}

- (CGRect)copyMenuTargetRectInCopyableLabelCoordinates:(HTCopyableLabel *)copyableLabel
{
    return copyableLabel.bounds;
}

@end
