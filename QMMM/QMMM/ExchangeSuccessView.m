//
//  ExchangeSuccessView.m
//  QMMM
//
//  Created by kingnet  on 15-3-26.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "ExchangeSuccessView.h"

@interface ExchangeSuccessView ()
@property (weak, nonatomic) IBOutlet UIButton *inviteBtn;
@property (weak, nonatomic) IBOutlet UIButton *exchangeBtn;

@property (weak, nonatomic) IBOutlet UILabel *hongbaoLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonMarginCst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonMarginCst2;
@end
@implementation ExchangeSuccessView

+ (ExchangeSuccessView *)successView
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"ExchangeSuccessView" owner:self options:0];
    return [array lastObject];
}

+ (CGFloat)viewHeight
{
    return 290;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.hongbaoLbl.text = @"";
    [self.inviteBtn setBackgroundColor:UIColorFromRGB(0x5381db)];
    [self.exchangeBtn setBackgroundColor:UIColorFromRGB(0xff4544)];
    self.inviteBtn.layer.cornerRadius = 5.f;
    self.exchangeBtn.layer.cornerRadius = 5.f;
    self.backgroundColor = [UIColor clearColor];
    self.buttonMarginCst.constant = (kMainFrameWidth-290)/3;
    self.buttonMarginCst2.constant = (kMainFrameWidth-290)/3;
    [self layoutIfNeeded];
}

- (void)setHongbaoMoney:(float)hongbaoMoney
{
    self.hongbaoLbl.text = [NSString stringWithFormat:@"当前红包余额：¥%.2f", hongbaoMoney];
}

- (IBAction)inviteAction:(id)sender {
    if (self.delegate) {
        [self.delegate didTapInvite];
    }
}

- (IBAction)goOnExchangeAction:(id)sender {
    if (self.delegate) {
        [self.delegate didTapExchange];
    }
}


@end
