//
//  PersonEvaluateCell.h
//  QMMM
//
//  Created by kingnet  on 14-11-14.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class PersonEvaluateEntity;

@protocol PersonEvaluateCellDelegate <NSObject>

- (void)didTapEvaluateCell:(PersonEvaluateEntity *)evaluate;

@end

extern NSString * const kPersonEvaluateCellIdentifier;

@interface PersonEvaluateCell : UITableViewCell

@property (nonatomic, strong) PersonEvaluateEntity *evaluate; //评价

@property (nonatomic, weak) id<PersonEvaluateCellDelegate> delegate;

+ (UINib *)nib;

+ (PersonEvaluateCell *)evaluateCell;

- (void)updateUI:(PersonEvaluateEntity *)evaluate;

@end
