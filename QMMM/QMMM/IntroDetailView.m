//
//  IntroDetailView.m
//  QMMM
//
//  Created by Derek on 15/1/8.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "IntroDetailView.h"
#import "QMGoodsAudioView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface IntroDetailView(){
    UIView *view;
    UIImageView *_imageBg;
    NSString *_strImgPath;
    NSString *_audioPath;
    NSString *_strPrice;
    NSString *_strDiscount;
    NSString *_strDesc;
    NSInteger _audioTime;
    
    NSInteger initType;
    
    GoodEntity *_entity;
    
}
@property (strong,nonatomic)UIImageView *imageView;
@property (strong,nonatomic)QMGoodsAudioView *audioView;
@property (strong,nonatomic)UILabel *lblPrice;
@property (strong,nonatomic)UILabel *lblDiscount;
@property (strong,nonatomic)UILabel *lblDescription;

@end

@implementation IntroDetailView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id)initWithDict:(NSDictionary *)dictInfo{
    self=[super init];
    if (self) {
        if (dictInfo) {
            initType=0;
            
            if ([dictInfo objectForKey:@"price"]) {
                _strPrice=[NSString stringWithFormat:@"￥%.2f",[[dictInfo objectForKey:@"price"] floatValue]];
            }
            if ([dictInfo objectForKey:@"desc"]) {
                _strDesc=[dictInfo objectForKey:@"desc"];
            }
            if ([dictInfo objectForKey:@"picture"]) {
                _strImgPath=[dictInfo objectForKey:@"picture"];
            }
            if ([dictInfo objectForKey:@"audio"] && [dictInfo objectForKey:@"audio_time"]) {
                _audioPath=[dictInfo objectForKey:@"audio"];
                _audioTime=[[dictInfo objectForKey:@"audio_time"] integerValue];
            }
            if ([dictInfo objectForKey:@"price"] && [dictInfo objectForKey:@"orginal_price"]) {
                _strDiscount=[NSString stringWithFormat:@"%d折",(int)([[dictInfo objectForKey:@"price"] floatValue]/[[dictInfo objectForKey:@"orginal_price"] floatValue]*10)];
            }
            
            [self setUpDetail];
        }
    }
    return self;
}

-(id)initWithGoodEntity:(GoodEntity *)entity{
    self=[super init];
    if (self) {
//        NSLog(@"dict=%@",dict);
        if (entity) {
            initType=1;
            _entity=entity;
            
            _strPrice=[NSString stringWithFormat:@"￥%.2f",entity.price];
            _strDesc=entity.desc;
            _strDiscount=[NSString stringWithFormat:@"%d折",(int)(entity.price/entity.orig_price)*10];
            _strImgPath=entity.picturelist.rawArray[0];
            
            [self setUpDetail];
            
        }
    }
    return self;
}

-(void)setUpDetail{
    self.clipsToBounds=YES;
    
    view=[[UIView alloc] init];
    view.frame=CGRectMake(14, 0, self.bounds.size.width-2*14, self.bounds.size.height);
    view.backgroundColor=[UIColor whiteColor];
    view.clipsToBounds=YES;
    view.layer.cornerRadius=10.0f;
    [self addSubview:view];
    
    
    _lblPrice=[[UILabel alloc] init];
    _lblPrice.frame=CGRectMake(8, view.frame.size.height-60, 100,30);
    //            _lblPrice.backgroundColor=[UIColor blueColor];
    _lblPrice.textColor=UIColorFromRGB(0xf5292c);
    _lblPrice.font=[UIFont systemFontOfSize:18.0f];
    _lblPrice.text=_strPrice;
    [view addSubview:_lblPrice];
    
    _lblDescription=[[UILabel alloc] init];
    _lblDescription.frame=CGRectMake(8, view.frame.size.height-40, view.frame.size.width-2*8, 20);
    //            _lblDescription.backgroundColor=[UIColor orangeColor];
    _lblDescription.textColor=UIColorFromRGB(0x4f4f4f);
    _lblDescription.font=[UIFont systemFontOfSize:14.0f];
    _lblDescription.text=_strDesc;
    [view addSubview:_lblDescription];
    
    
    UIImage *image=[UIImage imageNamed:@"intro_discount_bg"];
    image=[image stretchableImageWithLeftCapWidth:20 topCapHeight:0];
    
    _imageBg=[[UIImageView alloc] init];
    _imageBg.frame=CGRectMake(view.frame.size.width-60, view.frame.size.height-75, 60,30);
    _imageBg.image=image;
    [view addSubview:_imageBg];
    
    //            NSLog(@"price=%f,orginalPrice=%f",entity.price,entity.orig_price);
    //            NSLog(@"strtemp=%@",strTemp);
    NSMutableAttributedString *attributeStr=[[NSMutableAttributedString alloc] initWithString:_strDiscount];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,1)];
    [attributeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:26.0f] range:NSMakeRange(0,1)];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(1,1)];
    [attributeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0f] range:NSMakeRange(1,1)];
    
    //            [attributeStr addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x99a0aa) range:NSMakeRange([text length],[titleText length]-[text length])];
    
    
    
    _lblDiscount=[[UILabel alloc] init];
    _lblDiscount.frame=CGRectMake(view.frame.size.width-40, view.frame.size.height-75, 80, 80);
    _lblDiscount.backgroundColor=[UIColor clearColor];
    _lblDiscount.textColor=[UIColor whiteColor];
    _lblDiscount.font=[UIFont systemFontOfSize:26.0f];
    _lblDiscount.attributedText=attributeStr;
    [view addSubview:_lblDiscount];
    
    
    _imageView=[[UIImageView alloc] init];
    [view addSubview:_imageView];
    NSString *strPath=_strImgPath;
    if (initType==0) {
        _imageView.image=[UIImage imageNamed:strPath];
        
//        UIImage *largeImage=[UIImage imageNamed:strPath];
//        if (kISSmall && largeImage.size.width>view.frame.size.width && largeImage.size.height>view.frame.size.height) {
//            CGRect cropRect=CGRectMake((largeImage.size.width-view.frame.size.width*2)/2,(largeImage.size.height-(view.frame.size.height-90)*2)/2,view.frame.size.width*2*10, (view.frame.size.height-90)*2*10);
//            
//            CGImageRef imageRef = CGImageCreateWithImageInRect([largeImage CGImage], cropRect);
//            _imageView.image=[UIImage imageWithCGImage:imageRef];
//            CGImageRelease(imageRef);
//        }else{
//            _imageView.image=largeImage;
//        }

    }else{
        if (strPath) {
            NSURL *imageUrl = [NSURL URLWithString:[AppUtils getAbsolutePath:strPath]];
            [_imageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_big"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error) {
                    NSLog(@"图片加载失败");
                    _imageView.image=[UIImage imageNamed:@"goodImgFailed_big"];
                }
            }];
        }
    }
    

    if (_audioPath) {
        _audioView=[[QMGoodsAudioView alloc] initWithFrame:CGRectMake(view.frame.size.width/2-60/2,view.frame.size.height-130,60,60)];
        _audioView.backgroundColor=[UIColor clearColor];
        if (initType==0) {
            [_audioView updateWithLocalPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath],_audioPath] andAudioTime:_audioTime];
            //[_audioView updateWithLocalPath:_audioPath andAudioTime:_audioTime];
        }else{
            [_audioView updateUI:_entity];
        }
        [view addSubview:_audioView];
    }

    
}

-(void)layoutSubviews{
    view.frame=CGRectMake(14, 0, self.bounds.size.width-2*14, self.bounds.size.height);
    _lblPrice.frame=CGRectMake(8, view.frame.size.height-70, 100,30);
    _lblDescription.frame=CGRectMake(10, view.frame.size.height-35, view.frame.size.width-2*8, 20);
    _imageBg.frame=CGRectMake(view.frame.size.width-60, view.frame.size.height-75, 60, 30);
    _lblDiscount.frame=CGRectMake(view.frame.size.width-40, view.frame.size.height-75, 50, 30);
    _imageView.frame=CGRectMake(0, 0, view.frame.size.width, view.frame.size.height-90);
    _audioView.frame=CGRectMake(view.frame.size.width/2-60/2,view.frame.size.height-130, 60, 60);
}
@end
