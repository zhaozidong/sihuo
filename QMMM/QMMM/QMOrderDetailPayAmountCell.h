//
//  QMOrderDetailPayAmountCell.h
//  QMMM
//
//  Created by hanlu on 14-9-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMOrderDetailPayAmountCellIdentifier;

@interface QMOrderDetailPayAmountCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * namelabel;

@property (nonatomic, strong) IBOutlet UILabel * valuelabel;

@property (nonatomic, strong) IBOutlet UILabel * onelabel;

@property (nonatomic, strong) IBOutlet UILabel * twolabel;

@property (nonatomic, strong) IBOutlet UILabel * threelabel;

@property (nonatomic, strong) IBOutlet UILabel * fourlabel;

@property (nonatomic, strong) IBOutlet UILabel * fivelablel;

+(id)cellForOrderDetailPayAmount;

- (void)updateUI:(CGFloat)ammount accountPay:(CGFloat)accountPay onlinePay:(CGFloat)onlinePay;

@end
