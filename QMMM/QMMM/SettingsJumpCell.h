//
//  SettingsJumpCell.h
//  QMMM
//
//  Created by kingnet  on 14-11-5.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kSettingsJumpCellIdentifier;

@interface SettingsJumpCell : UITableViewCell

+ (CGFloat)cellHeight;

+ (NSInteger)cellCount;

+ (UINib *)nib;

- (void)updateUI:(NSInteger)index;

@end
