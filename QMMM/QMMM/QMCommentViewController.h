//
//  QMCommentViewController.h
//  QMMM
//
//  Created by hanlu on 14-11-11.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>


@interface QMLoadingMoreHeadView : UIView

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView * indicatorView;

@property (nonatomic, strong) IBOutlet UILabel * textLabel;

- (void)startLoading;

- (void)stopLoading;

@end

@interface QMCommentViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView * tableView;

@property (nonatomic, strong) IBOutlet QMLoadingMoreHeadView * headView;

@end

