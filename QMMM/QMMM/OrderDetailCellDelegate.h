//
//  OrderDetailCellDelegate.h
//  QMMM
//
//  Created by kingnet  on 14-12-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OrderDetailCellDelegate <NSObject>

- (void)didTapContactSeller; //联系卖家

- (void)didTapCallExpress; //拨打物流电话

- (void)didTapCheckExpress; //查看物流信息

- (void)didTapPay; //付款

- (void)didTapModifyPrice; //修改价格

- (void)didTapDeliver; //发货

- (void)didTapConfirmReceive; //确认收货

- (void)payTimeIsUp; //付款的时间已到

- (void)didTapCallSeller; //拨打卖家/买家电话

- (void)didTapContactService; //联系客服

- (void)didTapRejectRefund; //拒绝退款

- (void)didTapAgreeRefund;  //接受退款

@end