//
//  QMInputAlert.h
//  QMMM
//
//  Created by kingnet  on 14-9-27.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    QMInputAlertTypeSecurity = 0,  //输入密码类型
    QMInputAlertTypeFloat = 1,  //输入的是金钱（允许两位小数）
    QMInputAlertTypeText = 2    //输入的是文本
}QMInputAlertType;

@class QMInputAlert;

@protocol QMInputAlertDelegate <NSObject>

/**
 @param str 输入的内容
 @return 是否取消对话框
 **/
- (BOOL)okWithInputStr:(NSString *)str inputAlert:(QMInputAlert *)inputAlert;

- (void)cancelInput;

@end
@interface QMInputAlert : UIView

@property (nonatomic, strong) NSString *placeholder;

@property (nonatomic, weak) id<QMInputAlertDelegate> delegate;

@property (nonatomic, assign) QMInputAlertType alertType;

+ (void)showWithPlaceholder:(NSString *)placeholder alertType:(QMInputAlertType)alertType delegate:(id<QMInputAlertDelegate>)delegate;

@end
