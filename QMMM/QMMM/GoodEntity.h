//
//  GoodEntity.h
//  QMMM
//
//  Created by 韩芦 on 14-9-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseEntity.h"
#import "GoodDataInfo.h"

@class FMResultSet;

@interface GoodEntity : BaseEntity

- (id)initWithFMResultSet:(FMResultSet *)resultSet;

- (id)initWithDictionary:(NSDictionary *)dict;

- (id)initWithGoodsEntity:(GoodEntity *)entity;

- (void)assignValue:(GoodEntity *)goodInfo;

- (NSString *)description;

//good_id
@property (nonatomic, assign) uint64_t gid;

//商品图片列表，以“，”分隔
@property (nonatomic, strong) GoodsPictureList* picturelist;

//商品描述
@property (nonatomic, copy) NSString * desc;

//商品价格
@property (nonatomic, assign) float price;

//商品原价
@property (nonatomic, assign) float orig_price;

//商品类别
@property (nonatomic, assign) int cate_type;

//卖家所在城市
@property (nonatomic, copy) NSString * city;

//卖家所在位置的经纬度，格式：(纬度，经度)
@property (nonatomic, copy) NSString *location;

//卖家语音消息地址
@property (nonatomic, copy) NSString * msgAudio;

//语音时长
@property (nonatomic, assign) uint  msgAudioSeconds;

//发布时间
@property (nonatomic, assign) int pub_time;

//卖家用户Id
@property (nonatomic, assign) uint64_t sellerUserId;

//卖家头像
@property (nonatomic, copy) NSString * sellerHeadIcon;

//卖家昵称
@property (nonatomic, strong) NSString * sellerNick;

//赞列表
@property (nonatomic, strong) FavorList * favorList;

//赞总数
@property (nonatomic, assign) int favorCounts;

//评论总是
@property (nonatomic, assign) int commentCounts;

//自己是否赞
@property (nonatomic,assign) BOOL isFavor;

//好友关系, 是否为1度好友
//@property (nonatomic, assign) BOOL is_friend;
@property (nonatomic, assign) int is_friend;

//是谁的好友
@property (nonatomic, strong) NSString * whose_friend;

//好友id
@property (nonatomic, strong) NSString * friendId;

//商品数量
@property (nonatomic, assign) int number;

@property (nonatomic, assign) int isNew;

//二度好友的数量
@property (nonatomic, assign) int fNum;

//商品上/下架状态 0被下架的商品 1正常的商品 -1被删除的商品 -2被举报的商品
@property (nonatomic, assign) int status;

//更新时间
@property (nonatomic, assign) int update_time;

//运费
@property (nonatomic, assign) float freight;

//是否为精选的
@property (nonatomic, assign) BOOL isSpecialGoods;

//被锁的数量
@property (nonatomic, assign) int lockNum;

///当前的系统时间
@property (nonatomic, assign) int curSysTime;

//可信度
@property (nonatomic, assign) int trust;

- (void)addFavor:(uint64_t)userId;

+ (NSArray *)usageDesc;

//- (NSString *)getNewPrecent;
@end

typedef NS_ENUM(NSInteger, QMEditGoodsType){
    QMEditNewGoods = 4000, //编辑商品
    QMUpdateGoods //更新商品
};




