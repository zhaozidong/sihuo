//
//  AnimationPageView.m
//  QMMM
//
//  Created by kingnet  on 14-11-20.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "AnimationPageView.h"

@interface AnimationPageView ()
@property (weak, nonatomic) IBOutlet UIView *whiteBgView;
@property (weak, nonatomic) IBOutlet UIImageView *introRedBg;
@property (weak, nonatomic) IBOutlet UIImageView *whiteLu;
@property (weak, nonatomic) IBOutlet UIImageView *colorLu;
@property (weak, nonatomic) IBOutlet UIImageView *slogan;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colorLuCst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *whiteLuCst;

@end

const int kAnimateDuration = 1;

@implementation AnimationPageView

+ (AnimationPageView *)animationView
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"AnimationPageView" owner:self options:0];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    UIImage *image = [UIImage imageNamed:@"introRedBg"];
    self.introRedBg.image = [image stretchableImageWithLeftCapWidth:4 topCapHeight:4];
    self.whiteLu.alpha = 0;
    self.introRedBg.alpha = 0;
    self.slogan.alpha = 0;
}

- (void)startAnimate
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:kAnimateDuration delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        weakSelf.colorLu.alpha = 0;
        weakSelf.whiteBgView.alpha = 0;
        weakSelf.introRedBg.alpha = 1;
        weakSelf.whiteLu.alpha = 1;
        weakSelf.colorLuCst.constant = (CGRectGetHeight(self.frame)/2-93-60);
        weakSelf.whiteLuCst.constant = (CGRectGetHeight(self.frame)/2-93-48);
        [weakSelf updateConstraintsIfNeeded];
        [weakSelf layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (finished) {
            [weakSelf.colorLu removeFromSuperview];
            [weakSelf.whiteBgView removeFromSuperview];
            [UIView animateWithDuration:kAnimateDuration animations:^{
                weakSelf.slogan.alpha = 1;
            }];
        }
    }];
}


@end
