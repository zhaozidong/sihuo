//
//  QMGetLocation.m
//  QMMM
//
//  Created by kingnet  on 14-12-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMGetLocation.h"
#import <MapKit/MapKit.h>
#import "JZLocationConverter.h"
#import "INTULocationManager.h"

@implementation QMGetLocation

+ (void)getCurrentLoc:(GetLocationBlock)locationBlock curPointBlock:(GetCurrentPointBlock)curPointBlock
{
    //确保拿到的位置描述是汉字
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObject:@"zh-Hans"] forKey:@"AppleLanguages"];
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity timeout:10.0 block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        if (status == INTULocationStatusSuccess) {
            if (curPointBlock) {
                curPointBlock(currentLocation, nil);
            }
            CLLocationCoordinate2D gcjPt = [JZLocationConverter wgs84ToGcj02:currentLocation.coordinate];
            CLLocation *loc = [[CLLocation alloc]initWithLatitude:gcjPt.latitude longitude:gcjPt.longitude];
            CLGeocoder *geocoder = [[CLGeocoder alloc]init];
            [geocoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error) {
                if ([placemarks count]>0 && error == nil) {
                    CLPlacemark *placemark = [placemarks objectAtIndex:0];
                    NSDictionary *locDic = placemark.addressDictionary;
                    NSString *countryCode = [locDic objectForKey:@"CountryCode"];
                    NSString *address = @"";
                    //是中国
                    if ([countryCode isEqualToString:@"CN"]) {
                        NSString *state = [locDic objectForKey:@"State"]; //省
                        NSString *city = [locDic objectForKey:@"City"]; //市
                        NSString *subLocality = [locDic objectForKey:@"SubLocality"]; //具体的区
                        NSString *reCity = [city stringByReplacingOccurrencesOfString:@"市辖区" withString:@""];
                        if (!city || [state isEqualToString:reCity]) { //是直辖市
                            address = [NSString stringWithFormat:@"%@•%@", state, subLocality];
                        }
                        else{
                            address = [NSString stringWithFormat:@"%@•%@", state, city];
                        }
                    }
                    //国外
                    else{
                        address = [locDic objectForKey:@"Country"];
                    }
                    
                    if (locationBlock) {
                        locationBlock(currentLocation, address, nil);
                    }
                }
                else if ([placemarks count]==0 && error==nil){
                    if (locationBlock) {
                        //未获取到当前的位置
                        locationBlock(currentLocation, @"", nil);
                    }
                }
                else if (error!=nil){
                    if (locationBlock) {
                        //获取当前位置出错
                        locationBlock(currentLocation, nil, error);
                    }
                }
            }];
        }
        else{
            NSString *errorTip = @"请求位置出错，请查看error code!";
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey:errorTip};
            NSError *error = [NSError errorWithDomain:kAppErrorDomain code:status userInfo:userInfo];
            if (locationBlock) {
                locationBlock(nil, nil, error);
            }
        }
    }];
}

+ (void)getCurrentPoint:(GetCurrentPointBlock)pointBlock
{
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity timeout:10.0 block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        if (status == INTULocationStatusSuccess) {
            if (pointBlock) {
                pointBlock(currentLocation, nil);
            }
        }
        else{
            NSString *errorTip = @"请求位置出错，请查看error code!";
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey:errorTip};
            NSError *error = [NSError errorWithDomain:kAppErrorDomain code:status userInfo:userInfo];
            if (pointBlock) {
                pointBlock(nil, error);
            }
        }
    }];
}

@end
