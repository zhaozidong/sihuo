//
//  QMRemarkContactCell.m
//  QMMM
//
//  Created by hanlu on 14-11-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMRemarkContactCell.h"

 NSString * const kQMRemarkContactCellIdentifier = @"kQMRemarkContactCellIdentifier";

@implementation QMRemarkContactCell

+ (id)cellForRemarkContact
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMRemarkContactCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
