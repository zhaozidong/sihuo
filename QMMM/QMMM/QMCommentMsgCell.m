//
//  QMCommentMsgCell.m
//  QMMM
//
//  Created by hanlu on 14-11-11.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "QMCommentMsgCell.h"
#import "MsgData.h"
#import "NSString+JSONCategories.h"
#import "UIImageView+QMWebCache.h"

#define MARGIN 10

NSString * const kQMCommentMsgCellIdentifier = @"kQMCommentMsgCellIdentifier";

@implementation QMCommentMsgCell

+ (id)cellForCommentMsg
{
    NSArray * array = [[NSBundle mainBundle] loadNibNamed:@"QMCommentMsgCell" owner:nil options:nil];
    return [array objectAtIndex:0];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [_timeLabel setTextColor:UIColorFromRGB(0x9198a3)];
    [_nameLabel setTextColor:UIColorFromRGB(0xfe595A)];
    
    [_descLabel setNumberOfLines:0];
    _goodsImageView.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateUI:(MsgData *)data
{
    NSDictionary * dict = [data.extData toDict];
    
    NSString * temp = [dict objectForKey:@"nickname"];
    if([temp length] > 0) {
        _nameLabel.text = temp;
    } else {
        _nameLabel.text = @"";
    }
    
    _timeLabel.text = [AppUtils stringFromDate:[NSDate dateWithTimeIntervalSince1970:data.msgTime]];
    
    temp = [dict objectForKey:@"pic"];
    if([temp length] > 0) {
        NSString *tempUrl = [AppUtils thumbPath:temp sizeType:QMImageQuarterSize];
        [_goodsImageView qm_setImageWithURL:tempUrl placeholderImage:[UIImage imageNamed:@"goodsPlaceholder_middle"]];
    } else {
        _goodsImageView.image = [UIImage imageNamed:@"goodsPlaceholder_middle"];
    }
    
    NSString * displayString = @"";
    NSRange redRange = NSMakeRange(NSNotFound, 0);
    
    temp = [dict objectForKey:@"comments"];
    if(data.msgType == EMT_GOODS_REPLY_COMMENT) {
        NSString * replyNick = [dict objectForKey:@"parent_nickname"];
        if([replyNick length] > 0) {
            displayString = [NSString stringWithFormat:@"回复 %@ : %@", replyNick, temp];
            redRange = [displayString rangeOfString:replyNick];
        } else {
            displayString = [NSString stringWithFormat:@"回复 %@ : %@", replyNick, @""];
        }
    } else {
        displayString = temp;
    }
    
    _descLabel.text = @"";
//    _descLabel.backgroundColor = [UIColor redColor];
    if([displayString length] > 0) {
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init] ;
        [paragraphStyle setLineBreakMode:NSLineBreakByCharWrapping];
        [paragraphStyle setLineSpacing:6.0];
        
        NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] initWithString:displayString];
        [attributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.0] range:NSMakeRange(0, [attributeString length])];
        [attributeString addAttribute:NSForegroundColorAttributeName value:kDarkTextColor range:NSMakeRange(0, [attributeString length])];
        [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attributeString length])];
        
        if(redRange.location != NSNotFound) {
            [attributeString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xfe595A) range:redRange];
//            [attributeString addAttribute:NSLinkAttributeName value:@"http://www.baidu.com" range:redRange];
        }
        
        //textSize
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat margin = MARGIN * 2  + 90 + 15;
        CGRect rect = [attributeString boundingRectWithSize:CGSizeMake(width - margin, 10000) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        if(rect.size.height > 38) {
            rect.size.height = 38;
        }
        CGRect frame = _descLabel.frame;
        frame.size.height = rect.size.height;
        _descLabel.frame = frame;
        
        [paragraphStyle setLineBreakMode:NSLineBreakByTruncatingTail];
        
        _descLabel.attributedText = attributeString;

    }
}

@end
