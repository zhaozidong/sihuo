//
//  OrderDetailRefundForSellerCell.h
//  QMMM
//
//  Created by kingnet  on 15-3-12.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailCellDelegate.h"

@class OrderDataInfo;
@interface OrderDetailRefundForSellerCell : UITableViewCell

@property (nonatomic, weak) id<OrderDetailCellDelegate> delegate;

+ (OrderDetailRefundForSellerCell *)refundForSellderCell;

- (void)updateUI:(OrderDataInfo *)orderInfo;

@end
