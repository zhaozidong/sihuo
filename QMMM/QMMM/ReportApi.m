//
//  ReportApi.m
//  QMMM
//
//  Created by Derek on 15/3/10.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "ReportApi.h"
#import "APIConfig.h"
#import "QMHttpClient.h"

@implementation ReportApi

-(void)reportByInfo:(ReportInfo *)info Context:(contextBlock)context{
    
    __block contextBlock bContext = context;
    __block NSString * url = [[self class] requestUrlWithPath:API_REPORT];
    
    NSDictionary *params;
    if (info.type==QMReportTypeGoods) {
        params=@{@"accused_id": info.userId,@"type":@"2",@"goods_id":info.goodsID,@"reason":info.reason,@"photo":info.photoKey};
    }else{
        params=@{@"accused_id":info.userId,@"type":@"1",@"reason":info.reason,@"photo":info.photoKey};
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [[QMHttpClient defaultClient] requestWithPath:url method:QMHttpRequestPost parameters:params prepareExecute:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError *error = nil;
            id result = [self parseData:responseObject error:&error];
            if (result) {
                NSDictionary *dic = @{@"s":@0, @"d":result};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
            } else {
                NSDictionary *dic = @{@"s":@(-1), @"d":error.userInfo[kServerErrorTip]};
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (bContext) {
                        bContext(dic);
                    }
                });
                DLog(@"submit feedback error:%@",error.userInfo[NSLocalizedDescriptionKey]);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"--------submit feedback fail : %@", url);
            
            NSDictionary *dic;
            if (error.code == -1001) {
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_timeout"]};
            }
            else{
                dic = @{@"s":@(-1), @"d":[AppUtils localizedCommonString:@"QM_Alert_Server"]};
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bContext) {
                    bContext(dic);
                }
            });
        }];
    });
}


@end
