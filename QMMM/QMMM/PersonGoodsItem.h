//
//  PersonGoodsItem.h
//  QMMM
//
//  Created by kingnet  on 14-11-12.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@class GoodsSimpleInfo;

@protocol PersonGoodsItemDelegate <NSObject>

- (void)clickGoodsItem:(GoodsSimpleInfo *)goodsInfo;

@end

@interface PersonGoodsItem : UIView

@property (nonatomic, strong) GoodsSimpleInfo *goodsInfo;

@property (nonatomic, weak) id<PersonGoodsItemDelegate> delegate;

+ (PersonGoodsItem *)goodsItem;

+ (CGSize)itemSize;

+ (CGFloat)margin;

@end
