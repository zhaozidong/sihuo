//
//  NSThread+TTAdditions.h
//  ttnew

//  Copyright 2011年 SNDA Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSThread (TTAdditions)

/**
	获取线程变量（for Foundation对象）
	@param key 变量key名称
	@returns 变量对象
 */
- (id)threadValueForKey:(id)key;
/**
	设置线程变量（for Foundation对象）
	@param key 变量key名称
	@param value 变量对象
 */
- (void)setThreadValueForKey:(id)key value:(id)value;
/**
	移除线程变量（for Foundation对象）
	@param key 变量key名称
 */
- (void)removeThreadValueForKey:(id)key;

/**
    获取线程变量（for Core Foundation对象）
    @param key 变量key名称
    @returns 变量对象
 */
- (const void *)threadValueForCFKey:(const void *)key;
/**
    设置线程变量（for Core Foundation对象）
    @param key 变量key名称
    @param value 变量对象
 */
- (void)setThreadValueForCFKey:(const void *)key value:(const void *)value;
/**
    移除线程变量（for Core Foundation对象）
    @param key 变量key名称
 */
- (void)removeThreadValueForCFKey:(const void *)key;

@end
