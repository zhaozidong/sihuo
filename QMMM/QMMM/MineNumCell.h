//
//  MineNumCell.h
//  QMMM
//
//  Created by kingnet  on 15-3-19.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MineNumCellDelegate <NSObject>

- (void)didTapFlyNumLbl:(NSString *)flyNum flyBeat:(NSString *)flyBeat; //点击可信度

- (void)didTapInfluenceNumLbl:(NSString *)influenceNum influenceBeat:(NSString *)influenceBeat; //点击影响力

@end

@class PersonInfluence;
@interface MineNumCell : UITableViewCell

@property (nonatomic, weak) id<MineNumCellDelegate> delegate;

@property (nonatomic, strong) PersonInfluence *personInfluence;;

+ (MineNumCell *)mineNumCell;

- (void)updateNum:(PersonInfluence *)influence;

@end
