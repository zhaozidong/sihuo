//
//  FillUserInfoViewController.m
//  QMMM
//
//  Created by kingnet  on 14-9-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "FillUserInfoViewController.h"
#import "UploadImg.h"
#import "UserInfoHandler.h"
#import "UserEntity.h"
#import "AppDelegate.h"
#import "QMTextField.h"
#import "QMSubmitButton.h"
#import "SystemHandler.h"

@interface FillUserInfoViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIAlertViewDelegate>
{
    UploadImg * uploadImg_;
    NSString * phoneNum_;
    NSString * checkCode_;
    NSString * pwd_;
    NSString * avatarKey_;
    NSData * avatarData_;
}

@property (nonatomic, strong) UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIView *headerView;

@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (strong, nonatomic) IBOutlet UITableViewCell *nicknameCell;
@property (weak, nonatomic) IBOutlet QMTextField *nicknameTf;
@property (weak, nonatomic) IBOutlet QMSubmitButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *avatarBtn;

@end

@implementation FillUserInfoViewController

- (id)initWithPhoneNum:(NSString *)phoneNum checkCode:(NSString *)checkCode pwd:(NSString *)pwd
{
    self = [super init];
    if (self) {
        phoneNum_ = phoneNum;
        checkCode_ = checkCode;
        pwd_ = pwd;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"注册(2/2)";
    self.navigationItem.leftBarButtonItems = [AppUtils createBackButtonWithTarget:self selector:@selector(backAction:)];
    
    [[SystemHandler sharedInstance] reportRegisterStep:@"update_profile"];
    
    self.canTapCloseKeyboard = YES;
    
    [self setupView];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerObserver];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeObserver];
}

- (void)setupView
{
    //加载xib
    [[NSBundle mainBundle]loadNibNamed:@"FillUserInfoView" owner:self options:0];
    
    //设置tableView
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) style:UITableViewStylePlain];
    _tableView.scrollEnabled = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.rowHeight = 45.f;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kBorderColor;
    _tableView.tableFooterView = self.footerView;
    _tableView.tableHeaderView = self.headerView;
    [self.view addSubview:_tableView];
    self.footerView.backgroundColor = [UIColor clearColor];
    self.headerView.backgroundColor = [UIColor clearColor];
    
    self.submitBtn.enabled = NO;
    [self.submitBtn setTitle:@"完成并进入私货" forState:UIControlStateNormal];
    
    UIImageView *imgV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"person_icon"]];
    self.nicknameTf.leftView = imgV;
    self.nicknameTf.attributedPlaceholder = [AppUtils placeholderAttri:@"请输入您的名字"];
    self.nicknameTf.delegate = self;
}

#pragma mark - 
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.nicknameCell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

#pragma mark -
- (void)doRegister
{
    NSString *text = [self.nicknameTf.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (self.nicknameTf.text == nil || self.nicknameTf.text.length == 0 || text.length ==0) {
        [AppUtils showAlertMessage:[AppUtils localizedPersonString:@"QM_Text_NickName"]];
        return;
    }
    if ([self.nicknameTf.text rangeOfString:@" "].location != NSNotFound) {
        [AppUtils showAlertMessage:@"昵称中不能有空格滴"];
        return;
    }
    
    //统计点击注册完成按钮的次数
    [AppUtils trackCustomEvent:@"register_edit_userInfo" args:nil];
    
    [AppUtils showProgressMessage:[AppUtils localizedPersonString:@"QM_HUD_SigningUp"]];
    
    __weak typeof(self) weakSelf = self;
    
    [[UserInfoHandler sharedInstance] executeRegisterWithMobile:phoneNum_ password:pwd_ checkCode:checkCode_  avatar:@"" nickname:self.nicknameTf.text success:^(id obj) {
        //统计注册成功次数
        [AppUtils trackCustomEvent:@"register_ok" args:nil];
        [[SystemHandler sharedInstance] reportRegisterStep:@"enter_app"];
        //上传头像
        if (nil != avatarData_) {
            [[UserInfoHandler sharedInstance] uploadAvatarData:avatarData_ success:^(id obj) {
                [AppUtils dismissHUD];
                
                NSString *key = (NSString *)obj;
                UserEntity *curUser = [UserInfoHandler sharedInstance].currentUser;
                UserEntity *entity = [[UserEntity alloc]initWithUserEntity:curUser];
                entity.avatar = key;
                [[UserInfoHandler sharedInstance]updateUserInfo:entity success:^(id obj) {
                    
                    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                    [appDelegate startApp];
                    
                } failed:^(id obj) {
                    [AppUtils showErrorMessage:(NSString *)obj];
                    [weakSelf.avatarBtn setBackgroundImage:[UIImage imageNamed:@"avatarbtn_normal"] forState:UIControlStateNormal];
                    [weakSelf.avatarBtn setBackgroundImage:[UIImage imageNamed:@"avatarbtn_hightlight"] forState:UIControlStateHighlighted];
                }];
            } failed:^(id obj) {
                [AppUtils showErrorMessage:(NSString *)obj];
                [weakSelf.avatarBtn setBackgroundImage:[UIImage imageNamed:@"avatarbtn_normal"] forState:UIControlStateNormal];
                [weakSelf.avatarBtn setBackgroundImage:[UIImage imageNamed:@"avatarbtn_hightlight"] forState:UIControlStateHighlighted];
            }];
        }
        else{
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate startApp];
        }
        
    } failed:^(id obj) {
        //拿到错误代码
        [AppUtils showErrorMessage:(NSString *)obj];
    }];
}

- (IBAction)didTapFillInfo:(id)sender {
    [AppUtils closeKeyboard];
    
    //判断头像是否上传了
    if (nil == avatarData_) {
        UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:nil message:@"上传真实头像很有用哦" delegate:self cancelButtonTitle:@"跳过" otherButtonTitles:@"上传试试吧", nil];
        [alerView show];
        return;
    }
    [self doRegister];
}

- (IBAction)didTapAddAvarta:(id)sender {
    [AppUtils closeKeyboard];
    
    if (!uploadImg_) {
        uploadImg_ = [[UploadImg alloc]init];
    }
    
    UIButton *button = (UIButton *)sender;
    
    [uploadImg_ uploadImg:self uploadBlock:^(UIImage *img) {
        button.layer.cornerRadius = CGRectGetWidth(button.frame)/2.f;
        button.layer.masksToBounds = YES;
        button.layer.borderWidth = 2.f;
        button.layer.borderColor = [kBorderColor CGColor];
        [button setBackgroundImage:img forState:UIControlStateNormal];
        [button setBackgroundImage:img forState:UIControlStateHighlighted];
        
        //记录下data
        avatarData_ = UIImageJPEGRepresentation(img, 0.9);
        
        //统计上传头像成功
        [AppUtils trackCustomEvent:@"register_uploadAvatar_ok" args:nil];
    }];
}

- (void)textDidChange:(UITextField *)textField
{
    if (self.nicknameTf.text!=nil && self.nicknameTf.text.length>0) {
        self.submitBtn.enabled = YES;
    }
    else{
        self.submitBtn.enabled = NO;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self didTapFillInfo:nil];
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex == buttonIndex) {
        [self doRegister];
    }
    else{
        [self didTapAddAvarta:nil];
    }
}

- (void)registerObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(processKeyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(processKeyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)removeObserver
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [nc removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [nc removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)processKeyboardWillHide:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0 ;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    frame.size.height = kMainFrameHeight;
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.view.frame = frame;
    }];
}

- (void)processKeyboardWillShow:(NSNotification *)notification
{
    NSTimeInterval animationDuration = 0;
    
    //获取键盘显示的位置，动画时长等
    [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGRect keyboardInViewF = [self.view convertRect:keyboardRect toView:self.view];
    CGRect textFieldF = [self.submitBtn.superview convertRect:self.submitBtn.frame toView:self.view];
    
    if (CGRectGetMinY(keyboardInViewF)-CGRectGetMaxY(textFieldF) < 0) {
        CGRect frame = self.view.frame;
        frame.origin.y = CGRectGetMinY(keyboardInViewF)-CGRectGetMaxY(textFieldF);
        frame.size.height = frame.size.height-(CGRectGetMinY(keyboardInViewF)-CGRectGetMaxY(textFieldF));
        [UIView animateWithDuration:animationDuration animations:^{
            self.view.frame = frame;
        }];
    }
}

@end
