//
//  PhotoCell.m
//  QMMM
//
//  Created by Shinancao on 14-9-21.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "PhotoCell.h"
#import "UIImage+IF.h"
#import "UIImageView+QMWebCache.h"
#import "UploadProgress.h"
#import "UserInfoHandler.h"
#import "UIImage+FixOrientation.h"

NSString * const kPhotoCellIdentifier = @"kPhotoCellIdentifier";

@interface PhotoCell ()
@property (weak, nonatomic) IBOutlet UILabel *mainPhotoLbl;
@property (weak, nonatomic) IBOutlet UIImageView *photoImgV;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UploadProgress *progressView;

@end
@implementation PhotoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle]loadNibNamed:@"PhotoCell" owner:self options:nil] lastObject];
    if (self) {
        self.contentView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.isHaveAddBtn = NO;
    self.isMainPhoto = NO;
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = [kBorderColor CGColor];
    self.progressView.hidden = YES;
    self.progressView.cornerRadius = 0.f;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.contentView.frame = self.bounds;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    self.photoImgV.image = _image;
}

- (void)setFilePath:(NSString *)filePath
{
    _filePath = filePath;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:_filePath]) {
        UIImage *tmpImage = [UIImage imageWithContentsOfFile:_filePath];
        self.photoImgV.image = tmpImage;
    }
    else{
        UIImage *placeholder = [UIImage imageNamed:@"goodsPlaceholder_small"];
        NSString *smallPath = [AppUtils thumbPath:_filePath sizeType:QMImageQuarterSize];
        [self.photoImgV qm_setImageWithURL:smallPath placeholderImage:placeholder];
    }
}

- (void)setIsHaveAddBtn:(BOOL)isHaveAddBtn
{
    _isHaveAddBtn = isHaveAddBtn;
    if (isHaveAddBtn) {
        self.photoImgV.hidden = YES;
        self.mainPhotoLbl.hidden = YES;
        self.addBtn.hidden = NO;
    }
    else{
        self.photoImgV.hidden = NO;
        self.addBtn.hidden = YES;
        if (_isMainPhoto) {
            self.mainPhotoLbl.hidden = NO;
        }
    }
}

- (void)setIsMainPhoto:(BOOL)isMainPhoto
{
    _isMainPhoto = isMainPhoto;
    if (!_isHaveAddBtn && _isMainPhoto) {
        self.mainPhotoLbl.hidden = NO;
    }
    else{
        self.mainPhotoLbl.hidden = YES;
    }
}

+(CGSize)cellSize
{
    return CGSizeMake(65.f, 65.f);
}

- (IBAction)addPhotoAction:(id)sender {
    if (self.addPhotoBlock) {
        self.addPhotoBlock();
    }
}

- (void)setProgress:(float)progress
{
    self.progressView.value = progress;
}

- (void)prepareUpload
{
    self.progressView.isFailed = NO;
    self.progressView.hidden = NO;
    self.progressView.value = 0.f;
}

- (void)uploadSuccess
{
    self.progressView.value = 1.0;
    self.progressView.hidden = YES;
    self.progressView.isFailed = NO;
}

- (void)uploadFailed
{
    self.progressView.value = 0.0;
    self.progressView.isFailed = YES;
}

@end
