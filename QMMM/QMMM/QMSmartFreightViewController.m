//
//  QMSmartFreightViewController.m
//  QMMM
//
//  Created by Derek on 15/1/6.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "QMSmartFreightViewController.h"
#import "QMSmartFreight.h"


@interface QMSmartFreightViewController ()

@end

@implementation QMSmartFreightViewController
-(void)loadView{
    QMSmartFreight *smartFreight=[[QMSmartFreight alloc] init];
    smartFreight.frame=CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight);
    self.view=smartFreight;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor clearColor];
    
    UIButton *btnClose=[[UIButton alloc] init];
    btnClose.frame=CGRectMake(kMainFrameWidth-35, 15, 30, 30);
    [btnClose setImage:[UIImage imageNamed:@"smartFreight_close"] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(btnDidClose:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnClose];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)btnDidClose:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
