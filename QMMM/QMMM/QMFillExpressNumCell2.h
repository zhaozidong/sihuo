//
//  QMFillExpressNumCell2.h
//  QMMM
//
//  Created by Shinancao on 14/11/22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kQMFillExpressNumCell2Identifier;

@interface QMFillExpressNumCell2 : UITableViewCell

@property (nonatomic, strong) NSString *expressNum;

+ (QMFillExpressNumCell2 *)cell2ForFillExpressNum;

@end
