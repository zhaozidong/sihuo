//
//  UploadStatusInfo.h
//  QMMM
//  单张上传进度的封装
//  Created by kingnet  on 15-3-4.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GoodsHandler.h"

typedef enum
{
    UploadStatusBegan = 0, /*开始*/
    UploadStatusUploading = 1, /*上传中*/
    UploadStatusFailed = 2, /*上传失败*/
    UploadStatusSuccess = 3 /*上传成功*/
}
UploadStatus;

@interface UploadStatusInfo : NSObject

@property (nonatomic, assign) UploadStatus status; //上传的状态

@property (nonatomic, strong) NSIndexPath *indexPath; //所在的索引

@property (nonatomic, strong) NSString *filePath;

- (id)initWithFilePath:(NSString *)filePath;

- (void)uploadImageProgress:(void(^)(float progress))uploading success:(SuccessBlock)success failed:(FailedBlock)failed;

@end
