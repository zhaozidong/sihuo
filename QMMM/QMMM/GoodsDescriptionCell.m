//
//  GoodsDescriptionCell.m
//  QMMM
//
//  Created by kingnet  on 14-9-18.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "GoodsDescriptionCell.h"
#import <QuartzCore/QuartzCore.h>
#import "GoodEntity.h"

NSString * const kGoodsDescriptionCellIdentifier = @"kGoodsDescriptionCellIdentifier";

@interface GoodsDescriptionCell ()
@property (weak, nonatomic) IBOutlet UIView *locationView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLbl;
@property (weak, nonatomic) IBOutlet UILabel *locationLbl;

@end

@implementation GoodsDescriptionCell

+ (GoodsDescriptionCell *)cellAwakeFromNib
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"GoodsDescriptionCell" owner:self options:nil];
    return [array lastObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.locationView.layer.masksToBounds = YES;
    self.locationView.layer.cornerRadius = CGRectGetHeight(self.locationView.frame)/2.f;
    self.locationView.backgroundColor = UIColorFromRGB(0xe8e8e8);
    self.descriptionLbl.backgroundColor = [UIColor clearColor];
    self.descriptionLbl.numberOfLines = 0;
    self.descriptionLbl.text = @"";
    self.locationLbl.text = @"";
    self.descriptionLbl.font = [UIFont fontWithName:kFontName size:12.f];
    self.descriptionLbl.textColor = UIColorFromRGB(0x777777);
    self.locationLbl.font = [UIFont fontWithName:kFontName size:10.f];
    self.locationLbl.textColor = UIColorFromRGB(0x7e7e7e);
}

- (void)updateUI:(GoodEntity *)goodEntity
{
    if (goodEntity) {
        NSString *newString = @"";
        if (goodEntity.isNew != -1) {
            newString = [[GoodEntity usageDesc] objectAtIndex:goodEntity.isNew];
        }
        NSString *temp = [newString stringByAppendingFormat:@"  %@", goodEntity.desc];
        NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:temp];
        [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, newString.length)];
        [attributeStr addAttribute:NSBackgroundColorAttributeName value:UIColorFromRGB(0xff8385) range:NSMakeRange(0, newString.length)];
        [attributeStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:kFontName size:10.f] range:NSMakeRange(0, newString.length)];
        
        self.descriptionLbl.attributedText = attributeStr;
        
        NSDate * date = [NSDate dateWithTimeIntervalSince1970:goodEntity.pub_time];
        NSString * stringDate = [AppUtils stringFromDate:date];
        NSString * locationText = [NSString stringWithFormat:@"%@ | %@", goodEntity.city, stringDate];
        
        self.locationLbl.text = locationText;
        
        [self setNeedsUpdateConstraints];
        [self setNeedsLayout];
    }
}

@end
