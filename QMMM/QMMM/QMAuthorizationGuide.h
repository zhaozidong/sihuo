//
//  QMAuthorizationGuide.h
//  QMMM
//
//  Created by Derek.zhao on 14-12-22.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
    QMAuthorizationGuideTypeAddressBook,
    QMAuthorizationGuideTypeLocation,
    QMAuthorizationGuideTypeCamera
}QMAuthorizationGuideType;




@interface QMAuthorizationGuide : UIView

-(id)initWithType:(QMAuthorizationGuideType)type;

@end
