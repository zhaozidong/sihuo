//
//  AppDelegate.h
//  QMMM
//
//  Created by kingnet  on 14-8-29.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDVTabBarController.h"
#import "WXApi.h"
#import "MyOrderInfo.h"
#import "TGCameraNavigationController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, RDVTabBarControllerDelegate, WXApiDelegate, EMChatManagerDelegate, TGCameraDelegate>{
    BOOL hidePrompt;
}


@property (strong, nonatomic) UIWindow *window;
/**
 * tabbarcontroller 根视图
 */
@property (strong, nonatomic) RDVTabBarController *viewController;

@property (strong, nonatomic) MyOrderInfo *orderInfo;

//app内通知提醒 lable
@property (strong, nonatomic) UILabel *labelNotification;


/**
 开始使用App
 **/
- (void)startApp;

/**
 用户退出登录
 **/
- (void)logout;

@end
