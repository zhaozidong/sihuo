//
//  TTIconMenuBar.h
//  AWIconSheet
//
//  Created by hanlu on 14-1-13.
//  Copyright (c) 2014年 Narcissus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IconMenuItemData : NSObject
@property (nonatomic, retain) NSString * imageName;
@property (nonatomic, retain) NSString * titleName;
- (id)initWithName:(NSString *)aImageName title:(NSString *)aTitleName;
@end

@interface TTIconMenuItem : UIButton
@property (nonatomic,assign)int index;
+(CGFloat)heightForMenuItem;
+(CGFloat)widthForMenuItem;
@end

@protocol TTIconMenuBarDelegate <NSObject>
@required
- (int)numberOfItemsInMenuBar;
- (TTIconMenuItem*)menuItemForMenuAtIndex:(NSInteger)index;
- (void)didTapOnItemAtIndex:(NSInteger)index;
@end

@interface TTIconMenuBar : UIView
@property (nonatomic, retain) UIButton * cancelBtn;
@property (nonatomic, retain) id<TTIconMenuBarDelegate> iconMenuBarDelegate;

- (id)initWithIconMenuBarDelegate:(id<TTIconMenuBarDelegate>)delgate itemCount:(NSUInteger)count;
- (void)show;
- (void)dismiss;

@end

