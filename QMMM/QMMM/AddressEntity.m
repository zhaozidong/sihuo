//
//  AddressEntity.m
//  QMMM
//
//  Created by kingnet  on 14-9-24.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "AddressEntity.h"
#import "FMResultSet.h"

@implementation AddressEntity

- (instancetype)initWithDictionary:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        id temp = [dict objectForKey:@"id"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.addressId = [[dict objectForKey:@"id"] intValue];
        }
        temp = [dict objectForKey:@"uid"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.uid = [[dict objectForKey:@"uid"] intValue];
        }
        temp = [dict objectForKey:@"province"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.provinceId = [[dict objectForKey:@"province"]intValue];
        }
      
        temp = [dict objectForKey:@"city"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.cityId = [[dict objectForKey:@"city"] intValue];
        }
        temp = [dict objectForKey:@"district"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.distritId = [[dict objectForKey:@"district"] intValue];
        }
        temp = [dict objectForKey:@"address"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.detailAddress = [dict objectForKey:@"address"];
        }
        temp = [dict objectForKey:@"is_default"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.isDefault = [[dict objectForKey:@"is_default"] boolValue];
        }
        NSMutableString *mbStr = [[NSMutableString alloc]initWithString:@""];
        temp = [dict objectForKey:@"province_name"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            [mbStr appendString:[dict objectForKey:@"province_name"]];
        }
        temp = [dict objectForKey:@"city_name"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            if (![temp isEqualToString:@"县"] && ![temp isEqualToString:@"市辖区"] && ![temp isEqualToString:@"省直辖"]) {
                [mbStr appendString:[dict objectForKey:@"city_name"]];
            }
        }
        temp = [dict objectForKey:@"district_name"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            [mbStr appendString:[dict objectForKey:@"district_name"]];
        }
        self.locationName = mbStr;
        
        temp = [dict objectForKey:@"name"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.consignee = [dict objectForKey:@"name"];
        }
        
        temp = [dict objectForKey:@"mobile"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.phone = [dict objectForKey:@"mobile"];
        }
    }
    return self;
}

- (id)initWithFMResultSet:(FMResultSet *)rs
{
    self = [super init];
    if (self) {
        self.addressId = [rs intForColumnIndex:1];
        self.uid = [rs unsignedLongLongIntForColumnIndex:2];
        self.provinceId = [rs intForColumnIndex:3];
        self.cityId = [rs intForColumnIndex:4];
        self.distritId = [rs intForColumnIndex:5];
        self.detailAddress = [rs stringForColumnIndex:6];
        self.locationName = [rs stringForColumnIndex:7];
        self.isDefault = [rs boolForColumnIndex:8];
        self.consignee = [rs stringForColumnIndex:9];
        self.phone = [rs stringForColumnIndex:10];
    }
    return self;
}

- (void)assignWithEntity:(AddressEntity *)entity
{
    self.addressId = entity.addressId;
    self.uid = entity.uid;
    self.provinceId = entity.provinceId;
    self.cityId = entity.cityId;
    self.distritId = entity.distritId;
    self.detailAddress = entity.detailAddress;
    self.locationName = entity.locationName;
    self.isDefault = entity.isDefault;
    self.consignee = entity.consignee;
    self.phone = entity.phone;
}

@end
