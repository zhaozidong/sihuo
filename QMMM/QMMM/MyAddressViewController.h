//
//  MyAddressViewController.h
//  QMMM
//  收货地址
//  Created by kingnet  on 14-9-23.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger,AddressInitType){
    AddressInitTypeMine,
    AddressInitTypeBuy
};




@class AddressEntity;

typedef void(^AddressBlock)(AddressEntity *entity);

@interface MyAddressViewController : BaseViewController
@property (nonatomic, copy) AddressBlock addressBlock;
@property (nonatomic, assign) int addressId;
@property (nonatomic, assign) BOOL needSelected; //是不需要选中的
@property (nonatomic, assign) AddressInitType initType;

- (id)initWithAddressId:(int)addressId addressBlock:(AddressBlock)addressBlock;

@end
