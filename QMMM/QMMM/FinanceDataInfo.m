//
//  FinanceDataInfo.m
//  QMMM
//
//  Created by kingnet  on 14-9-27.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "FinanceDataInfo.h"
#import "FMResultSet.h"

@implementation FinanceDataInfo

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        id temp = [dict objectForKey:@"id"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self._id = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            self._id = [temp intValue];
        }
        
        temp = [dict objectForKey:@"uid"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.uid = [temp longLongValue];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            self.uid = [temp longLongValue];
        }
        
        temp = [dict objectForKey:@"desc"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.desc = [dict objectForKey:@"desc"];
        }
        
        temp = [dict objectForKey:@"amount"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.amount = [temp floatValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.amount = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"order_id"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            self.orderId = [dict objectForKey:@"order_id"];
        }
        
        temp = [dict objectForKey:@"pay_order_id"];
        if (temp && ![temp isKindOfClass:[NSNull class]]) {
            self.payOrderId = [dict objectForKey:@"pay_order_id"];
        }
        
        temp = [dict objectForKey:@"comments"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.comments = [dict objectForKey:@"comments"];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            self.comments = [dict objectForKey:@"comments"];
        }
        
        temp = [dict objectForKey:@"type"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.type = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            self.type = [temp intValue];
        }
        
        temp = [dict objectForKey:@"status"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.status = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            self.status = [temp intValue];
        }
        
        temp = [dict objectForKey:@"ts"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.ts = [temp intValue];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            self.ts = [temp intValue];
        }
    }
    return self;
}

- (id)initWithFMResultSet:(FMResultSet *)rs
{
    self = [super init];
    if (self) {
        self._id = [rs intForColumnIndex:1];
        self.uid = [rs unsignedLongLongIntForColumnIndex:2];
        self.desc = [rs stringForColumnIndex:3];
        self.amount = [rs intForColumnIndex:4];
        self.orderId = [rs stringForColumnIndex:5];
        self.payOrderId = [rs stringForColumnIndex:6];
        self.comments = [rs stringForColumnIndex:7];
        self.type = [rs intForColumnIndex:8];
        self.status = [rs intForColumnIndex:9];
        self.ts = [rs intForColumnIndex:10];
    }
    return self;
}

@end

@implementation FinanceUserInfo

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        id temp = [dict objectForKey:@"amount"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.amount = [temp floatValue];
        }
        else if (temp && [temp isKindOfClass:[NSNumber class]]){
            self.amount = [temp floatValue];
        }
        
        temp = [dict objectForKey:@"payee"];
        
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.payee = [dict objectForKey:@"payee"];
        }
        
        temp = [dict objectForKey:@"account_no"];
        if (temp && [temp isKindOfClass:[NSString class]]) {
            self.accountNo = [dict objectForKey:@"account_no"];
        }
        
        temp = [dict objectForKey:@"hongbao"];
        if (temp && [temp isKindOfClass:[NSNumber class]]) {
            self.hongbao = [temp floatValue];
        }
        else if (temp && [temp isKindOfClass:[NSString class]]){
            self.hongbao = [temp floatValue];
        }
    }
    return self;
}

@end