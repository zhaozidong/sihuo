//
//  MyOrderList.h
//  QMMM
//
//  Created by hanlu on 14-9-26.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GoodDataInfo.h"

@interface MyOrderList : NSObject

// 已加载的订单列表
@property (nonatomic, strong) NSArray *cacheList;

// 是否正在加载中
@property (nonatomic, assign) BOOL loading;
// 是否正在从服务器加载首次中
@property (nonatomic, assign) BOOL serverFirstPageLoading;
// 所有内容都加载了
@property (nonatomic, assign) BOOL endOfAll;
// 是否有任意内容加载了
@property (nonatomic, assign) BOOL hasAnyDidLoaded;
//是否加载失败了
@property (nonatomic, assign) BOOL loadFailed;

// 加载下一页内容
// firstPage: 是否拉取第一页
// 注意：拉取第一页将强制从服务端拉取所页，并清除之前缓存的所有信息
- (BOOL)loadNextPage:(BOOL)firstPage;

//缓存对象类型
- (id)initWithType:(EControllerType)type;

- (void)updateSimpleGoodsInfo:(GoodsSimpleInfo *)info;

- (void)deleteGoods:(uint64_t)goodsId;

@end
