//
//  TimerButton.h
//  QMMM
//
//  Created by kingnet  on 14-9-13.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimerButton : UIButton

- (void)startTiming;  //开始计时
- (void)endTiming; //结束计时

@end
