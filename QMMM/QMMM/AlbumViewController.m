//
//  AlbumViewController.m
//  QMMM
//
//  Created by kingnet  on 14-11-1.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "AlbumViewController.h"
#import "QMSubmitButton.h"
#import "AGImagePickerController.h"
#import "AGIPCGridItem.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImage+IF.h"

@interface AlbumViewController ()<AGImagePickerControllerDelegate>
{
    AGImagePickerController *ipc;
}
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet QMSubmitButton *makeSureBtn;

@end

@implementation AlbumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupViews];
    self.selectedPhotos = [NSMutableArray arrayWithCapacity:self.totalCount];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (kIOS7) {
        [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(0xff3232)];
    }
    else{
        [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setBackgroundColor:UIColorFromRGB(0xff3232)];
    }
}

- (void)setupViews
{
    __block AlbumViewController *blockSelf = self;
    
    ipc = [[AGImagePickerController alloc] initWithDelegate:self];
    ipc.navigationBarHidden = NO;
    //设置导航栏颜色
    if (kIOS7) {
        [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(0x1d1d1d)];
    }
    else{
        [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setBackgroundColor:UIColorFromRGB(0x1d1d1d)];
    }
    //[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    ipc.didFailBlock = ^(NSError *error) {
        NSLog(@"Fail. Error: %@", error);
        
        if (error == nil) {
            [blockSelf.selectedPhotos removeAllObjects];
            NSLog(@"User has cancelled.");
            
        } else {
            
            // We need to wait for the view controller to appear first.
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            });
        }
        
    };
    ipc.didFinishBlock = ^(NSArray *info) {
        [blockSelf.selectedPhotos setArray:info];
        
        NSLog(@"Info: %@", info);
        
    };
    // Show saved photos on top
    ipc.shouldShowSavedPhotosOnTop = NO;
    ipc.shouldChangeStatusBarStyle = NO;
    ipc.maximumNumberOfPhotosToBeSelected = self.totalCount;
    
    self.footerView = [[[NSBundle mainBundle] loadNibNamed:@"AlbumFooterView" owner:self options:0] lastObject];
    if (kIOS7) {
        self.footerView.backgroundColor = [UIColorFromRGB(0x1d1d1d) colorWithAlphaComponent:0.9];
    }
    else{
        self.footerView.backgroundColor = UIColorFromRGB(0x1d1d1d);
    }
    self.footerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.footerView];
    _makeSureBtn.titleLabel.font = [UIFont systemFontOfSize:20.f];
    [_makeSureBtn setTitle:[NSString stringWithFormat:@"确定（0/%d）", self.totalCount] forState:UIControlStateNormal];
    [_makeSureBtn addTarget:self action:@selector(makeSureAction:) forControlEvents:UIControlEventTouchUpInside];
    NSDictionary *views = NSDictionaryOfVariableBindings(_footerView);
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-(0)-[_footerView]-(0)-|" options:0 metrics:0 views:views];
    [self.view addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_footerView]-(0)-|" options:0 metrics:0 views:views];
    [self.view addConstraints:constraints];
    
    [ipc willMoveToParentViewController:self];
    ipc.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:ipc.view];
    views = [NSDictionary dictionaryWithObjects:@[ipc.view, _footerView] forKeys:@[@"ipcView", @"_footerView"]];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-(0)-[ipcView]-(0)-|" options:0 metrics:0 views:views];
    [self.view addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[ipcView]-(0)-[_footerView]" options:0 metrics:0 views:views];
    [self.view addConstraints:constraints];
    [self addChildViewController:ipc];
    [ipc didMoveToParentViewController:self];
}

#pragma mark -
#pragma mark - AGImagePickerControllerDelegate
- (NSUInteger)agImagePickerController:(AGImagePickerController *)picker
         numberOfItemsPerRowForDevice:(AGDeviceType)deviceType
              andInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (deviceType == AGDeviceTypeiPad)
    {
        if (UIInterfaceOrientationIsLandscape(interfaceOrientation))
            return 7;
        else
            return 6;
    } else {
        if (UIInterfaceOrientationIsLandscape(interfaceOrientation))
            return 5;
        else
            return 4;
    }
}

- (BOOL)agImagePickerController:(AGImagePickerController *)picker shouldDisplaySelectionInformationInSelectionMode:(AGImagePickerControllerSelectionMode)selectionMode
{
    return (selectionMode == AGImagePickerControllerSelectionModeSingle ? NO : YES);
}

- (BOOL)agImagePickerController:(AGImagePickerController *)picker shouldShowToolbarForManagingTheSelectionInSelectionMode:(AGImagePickerControllerSelectionMode)selectionMode
{
    return (selectionMode == AGImagePickerControllerSelectionModeSingle ? NO : YES);
}

- (AGImagePickerControllerSelectionBehaviorType)selectionBehaviorInSingleSelectionModeForAGImagePickerController:(AGImagePickerController *)picker
{
    return AGImagePickerControllerSelectionBehaviorTypeRadio;
}

- (void)backActionTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)makeSureAction:(id)sender
{
    [AppUtils showProgressMessage:@"请稍候"];
    __weak typeof(self) weakSelf = self;
    dispatch_queue_t myQueue = dispatch_queue_create(kMyGlobalQueueId, NULL);
    dispatch_async(myQueue, ^{
        //写入文件
        NSMutableArray *mbArray = [NSMutableArray arrayWithCapacity:ipc.selection.count];
        @autoreleasepool {
            for (int i=0; i<ipc.selection.count; i++) {
                ALAsset *asset = (ALAsset *)ipc.selection[i];
                ALAssetRepresentation* representation = [asset defaultRepresentation];
                // Retrieve the image orientation from the ALAsset
                UIImageOrientation orientation = UIImageOrientationUp;
                NSNumber* orientationValue = [asset valueForProperty:@"ALAssetPropertyOrientation"];
                if (orientationValue != nil) {
                    orientation = [orientationValue intValue];
                }
                UIImage *img = [UIImage imageWithCGImage:[[asset defaultRepresentation]fullResolutionImage] scale:[representation scale] orientation:orientation];
                
                //保存原大图
                NSData *imgData = [weakSelf compressImage:img];
                UInt64 t = (UInt64)([[NSDate date] timeIntervalSince1970]*1000);
                NSString *timeSp = [NSString stringWithFormat:@"%llu", t];
                [AppUtils saveData:imgData imgName:[NSString stringWithFormat:@"%@.jpg", timeSp]];
                
                UIImage *thumbnail = [UIImage imageWithCGImage:asset.thumbnail];
                imgData = UIImageJPEGRepresentation(thumbnail, 0.7);
                NSString *smallImgPath = [AppUtils saveData:imgData imgName:[NSString stringWithFormat:@"%@_small.jpg", timeSp]];
                [mbArray addObject:smallImgPath];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppUtils dismissHUD];
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(choosePhotosFromAlbum:)]) {
                [weakSelf.delegate choosePhotosFromAlbum:mbArray];
            }
        });
    });
}

- (void)agImagePickerController:(AGImagePickerController *)picker didTapAgGridItem:(AGIPCGridItem *)gridItem
{
    [_makeSureBtn setTitle:[NSString stringWithFormat:@"确定（%ld/%d）", (unsigned long)[picker.selection count], self.totalCount] forState:UIControlStateNormal];
}

#pragma mark - Helper
- (NSData *)compressImage:(UIImage *)image
{
    NSArray *compressScale = @[@(0.5), @(0.1), @(0.05), @(0.01), @(0.001)];
    int i;
    for (i=0; i<compressScale.count; i++) {
        NSData *imageData = UIImageJPEGRepresentation(image, [compressScale[i] floatValue]);
        if (imageData.length < 1024.0f*1024.0f) {
            return imageData;
        }
    }
    if (i == compressScale.count) {
        NSData *imageData = UIImageJPEGRepresentation(image, 0.0001);
        return imageData;
    }
    return nil;
}

@end
