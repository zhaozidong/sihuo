//
//  UzysTipLabel.h
//  QMMM
//  下拉时的提示文字
//  Created by kingnet  on 15-4-16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UzysTipLabel : UILabel

@property (nonatomic,assign) BOOL isObserving;
@property (nonatomic,weak) UIScrollView *scrollView;

+ (NSString *)refreshTips;

@end
