//
//  UzysTipLabel.m
//  QMMM
//
//  Created by kingnet  on 15-4-16.
//  Copyright (c) 2015年 kingnet . All rights reserved.
//

#import "UzysTipLabel.h"
#import "UIScrollView+UzysCircularProgressPullToRefresh.h"

@implementation UzysTipLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.textAlignment = NSTextAlignmentCenter;
        self.textColor = kDarkTextColor;
        self.font = [UIFont systemFontOfSize:14.f];
        self.adjustsFontSizeToFitWidth = YES;
        self.minimumScaleFactor = .1f;
    }
    return self;
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"contentOffset"])
    {
        [self scrollViewDidScroll:[[change valueForKey:NSKeyValueChangeNewKey] CGPointValue]];
    }
}

- (void)scrollViewDidScroll:(CGPoint)contentOffset
{
    self.center = CGPointMake(self.center.x, contentOffset.y/2);
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    if (self.superview && newSuperview == nil) {
        UIScrollView *scrollView = (UIScrollView *)self.superview;
        if (scrollView.showPullToRefresh) {
            if (self.isObserving) {
                [scrollView removeObserver:self forKeyPath:@"contentOffset"];
                self.isObserving = NO;
            }
        }
    }
}

+ (NSString *)refreshTips
{
    static NSArray *array = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        NSString *strplistPath = [[NSBundle mainBundle]pathForResource:@"refreshTips" ofType:@"plist"];
        array = [[NSArray alloc]initWithContentsOfFile:strplistPath];
    });
    //随机取其中一句话返回
    if (array && array.count >0) {
        NSInteger index = arc4random()%array.count;
        return array[index];
    }
    return @"";
}

@end
