//
//  LCVoiceTip.m
//  QMMM
//
//  Created by kingnet  on 14-11-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import "LCVoiceTip.h"

@interface LCVoiceTip ()
@property (weak, nonatomic) IBOutlet UIImageView *backgroudImgV;
@property (weak, nonatomic) IBOutlet UILabel *tipLbl;
@property (nonatomic, strong) NSString *tip;
@end
@implementation LCVoiceTip

- (id)init
{
    self = [[[NSBundle mainBundle]loadNibNamed:@"LCVoiceTip" owner:self options:0] lastObject];
    if (self) {
        self.backgroudImgV.image = [[UIImage imageNamed:@"lcvoicetip_bg"]stretchableImageWithLeftCapWidth:14 topCapHeight:12];
    }
    return self;
}

- (void)setTip:(NSString *)tip
{
    self.tipLbl.text = tip;
}

+ (LCVoiceTip *)sharedInstance
{
    static LCVoiceTip *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

+ (void)showWithText:(NSString *)text
{
    LCVoiceTip *tip = [LCVoiceTip sharedInstance];
    tip.tip = text;
    if (tip.superview == nil) {
        UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
        tip.frame = CGRectMake(kMainFrameWidth/2 - tip.frame.size.width/2, kMainTopHeight + 10, tip.frame.size.width, tip.frame.size.height);
        [keyWindow addSubview:tip];
    }
}


+(void) hide{
    LCVoiceTip *tip = [LCVoiceTip sharedInstance];
    if (tip.superview) {
        [tip removeFromSuperview];
    }
}

@end
