//
//  LCVoice.h
//  LCVoiceHud
//
//  Created by 郭历成 on 13-6-21.
//  Contact titm@tom.com
//  Copyright (c) 2013年 Wuxiantai Developer Team.(http://www.wuxiantai.com) All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "amrFileCodec.h"

@interface LCVoice : NSObject

@property(nonatomic) float recordTime;

@property (nonatomic, strong) NSString *audioLocPath;

-(void) startRecord;
-(NSURL *) stopRecord;
-(void) cancelled;

//-(void) play:(NSData*) data;
-(void) stopPlay;

+(NSTimeInterval) getAudioTime:(NSData *) data;

-(void)setCurRecordTime:(int)time;

@end
