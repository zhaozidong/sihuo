//
//  LCVoiceTip.h
//  QMMM
//
//  Created by kingnet  on 14-11-4.
//  Copyright (c) 2014年 kingnet . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCVoiceTip : UIView

+ (void)showWithText:(NSString *)text;

+(void) hide;

@end
